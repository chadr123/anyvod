#!/bin/bash

. ../../installer/common/functions.sh

version=`cat version`
fileName="AnyVOD-${version}_linux_amd64.deb"
installRoot="usr/bin"
installDirectory="AnyVOD"
installPath="${installRoot}/${installDirectory}"
escapedInstallPath=`echo "$installPath" | sed 's/\//\\\\\//g'`

cd ../../package
check

if [ -d AnyVOD ] ; then
  rm -rf AnyVOD
  check
fi

mkdir -p AnyVOD/${installPath}
check

mkdir -p AnyVOD/usr/share/applications
check

cp ../installer/linux/dcple-com-AnyVOD.desktop AnyVOD/usr/share/applications
check

sed -i "s/%BIN_PATH%/\/${escapedInstallPath}\/client/g" client/AnyVODClient.sh
check

cp -a client licenses AnyVOD/${installPath}
check

cd AnyVOD/${installRoot}
check

strip "${installDirectory}/client/AnyVODClient_bin"
check

ln -s "${installDirectory}/client/AnyVODClient.sh" AnyVODClient
check

cd -
check

cp ../images/orgs/icon_64x64.png "AnyVOD/${installPath}/client/icon.png"
check

cp -a ../installer/linux/deb/* AnyVOD
check

sed -i "s/%VERSION%/${version}/g" AnyVOD/DEBIAN/control
check

dpkg-deb --build AnyVOD ${fileName}
check

exit 0
