#!/bin/bash

. ../../installer/common/functions.sh

if [ -d ../../package ] ; then
  rm -rf ../../package
  check
fi

cd ../../client/AnyVODClient/scripts/mac
check

./build.sh
check

cd ../../../../installer/mac
check

./package.sh
check

exit 0
