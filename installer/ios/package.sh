#!/bin/bash

. ../../installer/common/functions.sh

version=`cat ../linux/version`
version_qt=`cat ../../client/AnyVODClient/scripts/linux/version`
qt_path=`../../client/AnyVODClient/scripts/linux/qt_path.sh`

cd ../../package
check

cp ../client/AnyVODMobileClient-build-ios/AnyVOD.ipa ./AnyVODMobile-${version}_ios.ipa
check

exit 0
