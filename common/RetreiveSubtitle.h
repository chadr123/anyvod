﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "types.h"

#include <stdint.h>

class RetreiveSubtitle
{
public:
    RetreiveSubtitle();

    bool getLyrics(const wstring &filePath, wstring *ret);
    bool existSubtitle(const wstring &filePath, const string &fileName, string *url);

protected:
    virtual bool retreiveFromAlSongServer(const string &data, wstring *ret) = 0;
    virtual bool retreiveFromGOMServer(const string &md5, wstring *ret) = 0;
    virtual void getMD5(uint8_t *buf, size_t size, string *ret) = 0;
    virtual FILE* openFile(const wstring &filePath) = 0;

protected:
    uint32_t unsynchsafe(uint32_t synchsafe);
    void escapeURL(const string &fileName, string *ret);

private:
    bool readBody(const wstring &filePath, uint8_t *buf, size_t size);
    void constructData(const string &md5, string *ret);
    bool extractLyrics(const wstring &data, wstring *ret);
    bool extractExistSubtitle(const wstring &data);
    void escape(const wstring &data, wstring *ret);
    void replace(wstring &data, const wstring &oldVal, const wstring &newVal);
    bool findDataMP3(FILE *file);
    int32_t calcFramePosMP3(uint32_t header);
    bool readAudioHeaderMP3(FILE *file, uint32_t *header, int32_t *read, bool *sig);
    bool readBodyMP3(FILE *file, uint8_t *buf, size_t size);
    bool getAtomHeader(FILE *file, uint32_t start, uint32_t *size, char type[5]);
    bool findAtom(FILE *file, uint32_t start, uint32_t end, const vector_string &types, uint32_t *nextStart, uint32_t *atomStart, uint32_t *atomSize);

    bool parseMP3(FILE *file, uint8_t *buf, size_t size);
    bool parseOGG(FILE *file, uint8_t *buf, size_t size);
    bool parseFLAC(FILE *file, uint8_t *buf, size_t size);
    bool parseWMA(FILE *file, uint8_t *buf, size_t size);
    bool parseM4A(FILE *file, uint8_t *buf, size_t size);
    bool assumeMP3(const wstring &filePath, FILE *file, uint8_t *buf, size_t size);
    int64_t findPattern(FILE *file, const uint8_t *pattern, size_t size);
    bool readFirst(FILE *file, uint8_t *buf, size_t size);

private:
    static const int AUDIO_BODY_SIZE;
    static const int MOVIE_BODY_SIZE;

protected:
    static const char *ALSONG_SERVER;
    static const char *ALSONG_URL;
    static const char *ALSONG_CLIENT_ID;

    static const char *GOM_SERVER;
    static const char *GOM_URL;

    static const uint8_t MP3_SIG[3];
    static const uint8_t OGG_SIG[4];
    static const uint8_t FLAC_SIG[4];
    static const uint8_t WMA_SIG[16];
    static const uint8_t M4A_SIG[7];

    static const uint8_t OGG_DATA_SIG[7];
    static const uint8_t WMA_DATA_SIG[16];
};
