/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "stdafx.h"
#include "Analyser.h"

static Analyser *g_instance = nullptr;

extern "C" ANALYSER_API Analyser* GetAnalyserInstance()
{
  return g_instance;

}

Analyser::Analyser(HMODULE handle)
:m_handle(handle)
{

}

Analyser::~Analyser()
{

}

HMODULE
Analyser::GetHandle()
{
  return this->m_handle;

}

void
Analyser::SetInstance(Analyser *instance)
{
  g_instance = instance;

}

Analyser*
Analyser::GetInstance()
{
  return GetAnalyserInstance();

}
