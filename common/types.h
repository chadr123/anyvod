﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <vector>
#include <map>
#include <string>
#include <stdio.h>
#include <stdint.h>

using namespace std;

#include "size.h"
#include "packet_types.h"
#include "size_types.h"

class Analyser;

enum FILE_ITEM_TYPE
{
  FT_DIR,
  FT_FILE,

  FT_COUNT

};

enum FILE_START_TYPE
{
  FST_LOG,
  FST_ENV,
  FST_SUBTITLE,
  FST_NORMAL,

  FST_COUNT

};

enum INVOKER_TYPE
{
  IT_HEAD = 0,
  IT_UPDATEMOVIEINFO = 0,
  IT_REBUILDMOVIEMETA,

  IT_COUNT

};

enum CLIENT_STATE
{
  CS_HEAD = 0,
  CS_LOGOUT = 0,
  CS_LOGIN,
  CS_FILELIST,
  CS_SENDING_STREAM,

  CS_COUNT

};

enum ANALYSED_DATA_TYPE
{
  ADT_HEADER = 0,
  ADT_FIRST,
  ADT_MIDDLE,
  ADT_LAST,

  ADT_COUNT

};

enum ERROR_TYPE
{
  ET_NONE = 0,
  ET_INVALID_ID,
  ET_INVALID_PASS,
  ET_INVALID_TICKET,
  ET_EXCEED_OFFSET,
  ET_NOT_EXIST_PATH,
  ET_NOT_EXIST_FILENAME,
  ET_CANNOT_CREATE_TICKET,
  ET_ALREADY_LOGINED,
  ET_NOT_LOGINED,
  ET_TRANSMIT_NOT_STARTED,
  ET_TRANSMIT_ALREADY_STARTED,
  ET_TRANSMIT_ALREADY_TRANSMITING,
  ET_TRANSMIT_ALREADY_STOPPED,
  ET_CANNOT_READ_FILE,
  ET_NOT_SUPPORTED_FORMAT,
  ET_CANNOT_START_TRANSMISSION,
  ET_NO_PERMISSION,
  ET_NOT_ADMIN,
  ET_NOT_EXIST_LOG,
  ET_NOT_EXIST_ENV,
  ET_NOT_EXIST_SCHEDULER,
  ET_SCHEDULE_RUNNING,
  ET_SCHEDULER_NOT_RUNNING,
  ET_CANNOT_START_SCHEDULER,
  ET_NOT_EXIST_SCHEDULE,
  ET_CANNOT_WRITE_FILE,
  ET_INVALID_SCRIPT,
  ET_CANNOT_DELETE_FILE,
  ET_CANNOT_DELETE_CURRENT_LOG,
  ET_CANNOT_ACCESS_DATABASE,
  ET_ALREADY_EXIST_ENTITY,
  ET_ENTITY_NOT_FOUND,
  ET_CLIENT_NOT_FOUND,
  ET_CANNOT_DELETE_PUBLIC_GROUP,
  ET_EXIST_ASSOCIATED_USER_GROUP,
  ET_CANNOT_DELETE_ASSOCIATED_PUBLIC_GROUP,
  ET_CANNOT_MODIFY_PUBLIC_GROUP,
  ET_COMPRESSION_ERROR,
  ET_NOT_SUBTITLE,
  ET_ALREADY_JOINED,
  ET_INVALID_SOCKET_OPERATION,
  ET_CANNOT_OPEN_FILE,
  ET_UNKNOWN,

  ET_COUNT

};

const wchar_t * const ERROR_TYPE_MSG[ET_COUNT+1] =
{
  L"메시지 시작",
  L"아이디가 존재하지 않습니다.",
  L"암호가 올바르지 않습니다.",
  L"티켓이 올바르지 않습니다.",
  L"오프셋이 범위를 초과했습니다.",
  L"존재하지 않는 경로입니다.",
  L"존재하지 않는 파일명 입니다.",
  L"티켓을 생성 할 수 없습니다.",
  L"이미 로그인 되어있습니다.",
  L"로그인 되어있지 않습니다.",
  L"전송이 시작되지 않았습니다.",
  L"이미 전송이 시작 되었습니다.",
  L"전송 중입니다.",
  L"전송이 이미 정지 되었습니다.",
  L"파일을 읽을 수 없습니다.",
  L"지원하지 않는 파일 종류입니다.",
  L"전송을 시작 할 수 없습니다.",
  L"권한이 없습니다.",
  L"관리자가 아닙니다.",
  L"로그가 존재하지 않습니다",
  L"환경파일이 존재하지 않습니다.",
  L"스케줄파일이 존재하지 않습니다.",
  L"스케줄이 실행 중입니다.\n스케줄이 종료 될 때까지 잠시만 기다려 주십시오.",
  L"스케줄러가 정지 되어있습니다.",
  L"스케줄러를 시작 할 수 없습니다.",
  L"존재하지 않는 스케줄입니다.",
  L"파일에 쓸 수 없습니다.",
  L"스크립트가 유효하지 않습니다.",
  L"파일을 삭제 할 수 없습니다.",
  L"가장 최근 로그는 삭제 할 수 없습니다.",
  L"데이터 베이스에 접근 할 수 없습니다.",
  L"이미 존재하는 항목입니다.",
  L"항목을 찾을 수 없습니다.",
  L"접속자를 찾을 수 없습니다.",
  L"공용 그룹은 삭제 할 수 없습니다.",
  L"그룹에 속해 있는 사용자가 존재할 경우 삭제가 불가능 합니다.",
  L"공용 그룹의 권한은 삭제가 불가능 합니다.",
  L"공용 그룹은 수정 할 수 없습니다.",
  L"압축을 진행할 수 없습니다.",
  L"자막 파일이 아닙니다.",
  L"이미 참여하고 있습니다.",
  L"잘못 된 소켓을 이용하여 패킷을 전송 했습니다.",
  L"알 수 없는 오류 입니다.",
  L"파일을 열 수 없습니다.",
  L"메시지 끝"

};

struct TIME
{
  TIME()
  {

  }

  TIME(int _min, int _max)
  {
    min = _min;
    max = _max;

  }

  int min;
  int max;

};

struct FILE_CLOSER
{
    FILE_CLOSER(FILE *file)
    {
      _file = file;
    }

    ~FILE_CLOSER()
    {
      if (_file)
        fclose(_file);
    }

    FILE *_file;
};

typedef vector<TIME> VECTOR_TIME;
typedef VECTOR_TIME::iterator VECTOR_TIME_ITERATOR;
typedef VECTOR_TIME::const_iterator VECTOR_TIME_CONST_ITERATOR;

struct KEY_VALUE
{
  wstring key;
  wstring value;

};

typedef vector<KEY_VALUE> VECTOR_KEY_VALUE;
typedef VECTOR_KEY_VALUE::iterator VECTOR_KEY_VALUE_ITERATOR;

typedef vector<string> vector_string;
typedef vector<wstring> vector_wstring;
typedef vector<wchar_t> vector_wchar;

struct ANYVOD_FILE_ITEM
{
  FILE_ITEM_TYPE type;

  unsigned int totalTime;
  unsigned int bitRate;
  unsigned long long totalSize;
  unsigned int totalFrame;
  wstring fileName;
  wstring title;
  wstring movieType;
  bool permission;

};

struct ANYVOD_TIME
{
  short year;
  short month;
  short weekday;
  short day;
  short hour;
  short minute;
  short second;
  short milliseconds;

};

struct ANYVOD_USER_INFO
{
  unsigned long long pid;
  wstring id;
  wstring pass;
  wstring name;
  bool admin;

};

typedef vector<ANYVOD_USER_INFO> VECTOR_ANYVOD_USER_INFO;
typedef VECTOR_ANYVOD_USER_INFO::iterator VECTOR_ANYVOD_USER_INFO_ITERATOR;

struct ANYVOD_GROUP_INFO
{
  unsigned long long pid;
  wstring name;

};

typedef vector<ANYVOD_GROUP_INFO> VECTOR_ANYVOD_GROUP_INFO;
typedef VECTOR_ANYVOD_GROUP_INFO::iterator VECTOR_ANYVOD_GROUP_INFO_ITERATOR;

struct ANYVOD_USER_GROUP_INFO
{
  unsigned long long pid;
  wstring userID;
  wstring groupName;

};

typedef vector<ANYVOD_USER_GROUP_INFO> VECTOR_ANYVOD_USER_GROUP_INFO;
typedef VECTOR_ANYVOD_USER_GROUP_INFO::iterator VECTOR_ANYVOD_USER_GROUP_INFO_ITERATOR;

struct ANYVOD_CLIENT_STATUS
{
  unsigned long long totalRecvBytes;
  unsigned long long totalSentBytes;
  unsigned int recvBytesPerSec;
  unsigned int sentBytesPerSec;
  wstring ip;
  wstring id;
  ANYVOD_PACKET_TYPE lastPacketType;
  CLIENT_STATE clientState;
  unsigned short port;
  size_t recvBufCount;
  size_t sendBufCount;
  bool isStream;
  string ticket;

};

typedef vector<ANYVOD_CLIENT_STATUS> VECTOR_ANYVOD_CLIENT_STATUS;
typedef VECTOR_ANYVOD_CLIENT_STATUS::iterator VECTOR_ANYVOD_CLIENT_STATUS_ITERATOR;

struct ANYVOD_FILE_GROUP_INFO
{
  unsigned long long pid;
  wstring groupName;

};

typedef vector<ANYVOD_FILE_GROUP_INFO> VECTOR_ANYVOD_FILE_GROUP_INFO;
typedef VECTOR_ANYVOD_FILE_GROUP_INFO::iterator VECTOR_ANYVOD_FILE_GROUP_INFO_ITERATOR;

struct ANYVOD_STATEMENT_STATUS
{
  string sql;
  unsigned int totalCount;
  unsigned int usingCount;

};

typedef vector<ANYVOD_STATEMENT_STATUS> VECTOR_ANYVOD_STATEMENT_STATUS;
typedef VECTOR_ANYVOD_STATEMENT_STATUS::iterator VECTOR_ANYVOD_STATEMENT_STATUS_ITERATOR;

struct ANYVOD_ANALYSED_DATA_BLOCK
{
  ANYVOD_ANALYSED_DATA_BLOCK()
  {
    blockOffset = 0;
    size = 0;

  }

  long long blockOffset;
  long long size;

};

typedef vector<ANYVOD_ANALYSED_DATA_BLOCK> VECTOR_ANYVOD_ANALYSED_DATA_BLOCK;
typedef VECTOR_ANYVOD_ANALYSED_DATA_BLOCK::iterator VECTOR_ANYVOD_ANALYSED_DATA_BLOCK_ITERATOR;

struct ANYVOD_MOVIE_INFO
{
  unsigned long long totalSize;
  unsigned int bitRate;
  unsigned char bitRateMultiplier;
  wstring movieType;
  wstring codecVideo;

};

typedef map<wstring, Analyser*> MAP_ANYVOD_ANALYSER;
typedef MAP_ANYVOD_ANALYSER::iterator MAP_ANYVOD_ANALYSER_ITERATOR;

const wstring VIDEO_EXTENSION = L"3g2 3gp 3gp2 3gpp adf apng amr amv asf avi avs bmv brp cdg cdxl cif dav dif divx "
                                L"dpg dv dvr-ms evo flm flv ffm gdv gif gxf hnm idf ifv imx ipu ivr ivf k3g kux lvf m1v m2t m2ts m2v m4b m4p m4v moflex mj2 mjp2 mjpg mkv "
                                L"mods mov mp2v mp4 mpe mpeg mpg mpv2 msp mts mve mvi mxg mxf nsr nsv nut ogm psp ogv qcif qt ram rgb rm rmvb roq "
                                L"sdr2 ser sga ssif skm son swf tp tpr trp ts ty ty+ vb viv vob vqe vqf vql webm wm wmp wmv wtv xbin xl y4m yop yuv";
const wstring AUDIO_EXTENSION = L"302 722 aa3 aac aax ac3 adp ads adx aea afc aix aif amr ape apl apm ast au avr bit binka brstm bfstm bcstm "
                                L"caf cdata daud dss dtk dts dtshd eac3 fap flac fsb fwse g722 g723_1 g729 genh gsm hca hcom ircam latm lbc loas m1a m2a m4a mac mca mka "
                                L"mlp mmf mod mp2 mp3 mpa mpc mtaf msf musx nist nsp opus ogg oma omg paf pcm pvf ra rco rso rsd sbg sf shn sph str ss2 svs tak tco thd "
                                L"tta tun vag wav wma wsd wv";
const wstring SUBTITLE_EXTENSION = L"ass aqt idx js jss lrc pjs sami smi sub sup srt stl ssa svag rt vag vpk vtt xvag";
const wstring PLAYLIST_EXTENSION = L"cue m3u m3u8 pls asx wvx wax wmx wpl b4s";
const wstring CAPTURE_FORMAT_EXTENSION = L"bmp jpg png tiff";
const wstring FONT_EXTENSION = L"ttf";

const uint32_t UTF8_BOM = 0xEFBBBF00;
