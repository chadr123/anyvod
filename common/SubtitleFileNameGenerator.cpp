﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#define _SECURE_SCL 0

#include "types.h"
#include "SubtitleFileNameGenerator.h"

#include <string>
#include <algorithm>
#include <iterator>

SubtitleFileNameGenerator::SubtitleFileNameGenerator()
{
    this->m_sepList.push_back(L' ');
    this->m_sepList.push_back(L'_');
    this->m_sepList.push_back(L'-');
    this->m_sepList.push_back(L'.');
    this->m_sepList.push_back(L'+');
    this->m_sepList.push_back(L'~');

    this->m_appended.push_back(L"eng");
    this->m_appended.push_back(L"kor");
    this->m_appended.push_back(L"(eng)");
    this->m_appended.push_back(L"(kor)");

    this->splitString(SUBTITLE_EXTENSION, L' ', &this->m_extList);
}

void SubtitleFileNameGenerator::getFileNames(const wstring &src, vector_wstring *ret)
{
    wstring fileBaseName;

    this->getFileBaseName(src, true, &fileBaseName);
    this->getFileNamesSub(fileBaseName, ret);

    for (size_t i = 0; i < this->m_sepList.size(); i++)
    {
        wstring fileName = fileBaseName;

        for (size_t j = 0; j < this->m_sepList.size(); j++)
        {
            wchar_t oldSep = this->m_sepList[j];
            wchar_t newSep = this->m_sepList[i];

            if (oldSep != newSep)
                replace(fileName.begin(), fileName.end(), oldSep, newSep);
        }

        if (fileName != fileBaseName)
            this->getFileNamesSub(fileName, ret);

        for (size_t j = 0; j < this->m_appended.size(); j++)
        {
            fileName = fileBaseName;
            fileName += this->m_sepList[i];

            this->getFileNamesWithExt(fileName + this->m_appended[j], ret);
            this->getFileNamesWithExt(this->m_appended[j] + fileName, ret);
        }
    }
}

void SubtitleFileNameGenerator::getFileNamesWithExt(const wstring &src, vector_wstring *ret)
{
    wstring fileBaseName;

    this->getFileBaseName(src, true, &fileBaseName);

    for (size_t i = 0; i < this->m_extList.size(); i++)
    {
        wstring ext = L"." + this->m_extList[i];

        this->getFileNameByExt(fileBaseName, ext, ret);
    }

    this->getFileNamesWithExtNoBaseName(src, ret);
}

void SubtitleFileNameGenerator::getFileNamesWithExtNoBaseName(const wstring &src, vector_wstring *ret)
{
    wstring fileBaseName;

    this->getFileBaseName(src, false, &fileBaseName);

    for (size_t i = 0; i < this->m_extList.size(); i++)
    {
        wstring ext = L"." + this->m_extList[i];

        this->getFileNameByExt(fileBaseName, ext, ret);
    }
}

void SubtitleFileNameGenerator::getFileNamesSub(const wstring &fileBaseName, vector_wstring *ret)
{
    for (size_t i = 0; i < this->m_extList.size(); i++)
    {
        vector_wstring list;
        vector_wstring uniqueList;
        wstring ext = L"." + this->m_extList[i];

        this->getFileNameByExt(fileBaseName, ext, &list);

        for (size_t j = 0; j < this->m_sepList.size(); j++)
          this->getFileNameBySimilar(fileBaseName, ext, this->m_sepList[j], &list);

        for (size_t j = 0; j < this->m_sepList.size(); j++)
        {
            for (size_t k = 0; k < this->m_sepList.size(); k++)
            {
                wstring fileName = fileBaseName;
                wchar_t oldSep = this->m_sepList[j];
                wchar_t newSep = this->m_sepList[k];

                if (oldSep != newSep)
                {
                    replace(fileName.begin(), fileName.end(), oldSep, newSep);

                    if (fileName != fileBaseName)
                        this->getFileNameBySimilar(fileName, ext, newSep, &list);
                }
            }
        }

        sort(list.begin(), list.end());
        unique_copy(list.begin(), list.end(), back_inserter(uniqueList));

        ret->insert(ret->end(), uniqueList.begin(), uniqueList.end());
    }
}

void SubtitleFileNameGenerator::getFileNameByExt(const wstring &fileBaseName, const wstring &ext, vector_wstring *ret)
{
    ret->push_back(fileBaseName + ext);
}

void SubtitleFileNameGenerator::getFileNameBySimilar(const wstring &fileBaseName, const wstring &ext, const wchar_t sep, vector_wstring *ret)
{
    vector_wstring tokens;
    wstring prevName;
    wstring nextName;

    this->splitString(fileBaseName, sep, &tokens);

    for (size_t i = 0; i < tokens.size(); i++)
    {
        wstring newName;

        if (!prevName.empty())
            newName = prevName + sep;

        newName += tokens[i];
        ret->push_back(newName + ext);

        prevName = newName;
    }

    for (int i = (int)tokens.size()-1; i >= 0 ; i--)
    {
        wstring newName;

        if (!nextName.empty())
            newName = sep + nextName;

        newName = tokens[i] + newName;
        ret->push_back(newName + ext);

        nextName = newName;
    }
}

void SubtitleFileNameGenerator::splitString(const wstring &src, const wchar_t delem, vector_wstring *ret)
{
    size_t start = 0;
    size_t end = src.find_first_of(delem);

    if (end == wstring::npos)
    {
        ret->push_back(src);
    }
    else
    {
        wstring substr;

        do
        {
            substr = src.substr(start, end - start);

            if (substr.size() > 0)
                ret->push_back(substr);

             start = end + 1;

             end = src.find_first_of(delem, start);

        } while (end != wstring::npos);

        substr = src.substr(start, src.size() - start);

        if (substr.size() > 0)
            ret->push_back(substr);
    }
}

void SubtitleFileNameGenerator::getFileBaseName(const wstring &fullPath, bool withExt, wstring *retFileName)
{
    int index = (int)fullPath.find_last_of(L'\\');

    if ((size_t)index == wstring::npos)
        index = (int)fullPath.find_last_of(L'/');

    if ((size_t)index == wstring::npos)
        index = -1;

    size_t extIndex = fullPath.find_last_of(L'.');

    if (extIndex == wstring::npos || !withExt)
      extIndex = fullPath.size();

    retFileName->assign(fullPath, index + 1, extIndex - index - 1);
}
