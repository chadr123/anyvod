/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "size.h"
#include "types.h"

class Analyser
{
public:
  struct FILE_CLOSER
  {
    FILE_CLOSER(HANDLE file)
    {
      _file = file;

    }

    ~FILE_CLOSER()
    {
      CloseHandle(_file);

    }

    HANDLE _file;

  };

public:
  Analyser(HMODULE handle);
  virtual ~Analyser();

  virtual HMODULE GetHandle();
  virtual void GetName(wchar_t ret[MAX_ANALYSER_NAME_SIZE]) = 0;
  virtual void GetDescription(wchar_t ret[MAX_ANALYSER_DESC_SIZE]) = 0;
  virtual bool Analyse(const wstring &path, VECTOR_ANYVOD_ANALYSED_DATA_BLOCK *ret) = 0;

  static void SetInstance(Analyser *instance);
  static Analyser* GetInstance();

private:
  HMODULE m_handle;

};
