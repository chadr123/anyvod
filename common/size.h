﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "size_types.h"

const int MAX_UTF8_SIZE = 4;
const int UTF8_BOM_SIZE = 3;

const int MAX_ID_CHAR_SIZE = 20 * MAX_UTF8_SIZE;
const int MAX_PASS_CHAR_SIZE = 20 * MAX_UTF8_SIZE;
const int MAX_TICKET_CHAR_SIZE = 38;

const int MAX_FILENAME_CHAR_SIZE = 260 * MAX_UTF8_SIZE;
const int MAX_PATH_CHAR_SIZE = 512 * MAX_UTF8_SIZE;
const int MAX_FILEPATH_CHAR_SIZE = MAX_PATH_CHAR_SIZE + MAX_FILENAME_CHAR_SIZE;
const int MAX_TITLE_CHAR_SIZE = 256 * MAX_UTF8_SIZE;
const int MAX_MOVIETYPE_CHAR_SIZE = 32 * MAX_UTF8_SIZE;

const int MAX_MOVIE_TYPE_CHAR_SIZE = 50;
const int MAX_CODEC_NAME_CHAR_SIZE = 50;

const int MAX_SCHEDULE_FIELD_CHAR_SIZE = 50;
const int MAX_SCHEDULE_NAME_CHAR_SIZE = 50;
const int MAX_SCHEDULE_SCRIPT_CHAR_SIZE = MAX_SCHEDULE_FIELD_CHAR_SIZE * TI_COUNT + MAX_SCHEDULE_NAME_CHAR_SIZE;

const int MAX_STREAM_SIZE = 4096 * 3;
const int MAX_META_STREAM_SIZE = (int)(1.001 * (MAX_STREAM_SIZE + 12) + 1);

const int MAX_NAME_CHAR_SIZE = 50 * MAX_UTF8_SIZE;
const int MAX_IP_CHAR_SIZE = 15;//IPv4
const int MAX_GROUP_NAME_CHAR_SIZE = MAX_ID_CHAR_SIZE + 0;//그룹 이름 크기는 최소 아이디 크기보다 크거나 같아야한다.
const int MAX_SQL_CHAR_SIZE = 4096;

const int MAX_ANALYSER_NAME_CHAR_SIZE = 50;
const int MAX_ANALYSER_DESC_CHAR_SIZE = 100 * MAX_UTF8_SIZE;

const int MAX_URL_CHAR_SIZE = 4096;

const int MAX_ID_SIZE = MAX_ID_CHAR_SIZE + 1;
const int MAX_PASS_SIZE = MAX_PASS_CHAR_SIZE + 1;
const int MAX_TICKET_SIZE = MAX_TICKET_CHAR_SIZE + 1;

const int MAX_FILENAME_SIZE = MAX_FILENAME_CHAR_SIZE + 1;
const int MAX_PATH_SIZE = MAX_PATH_CHAR_SIZE + 1;
const int MAX_FILEPATH_SIZE = MAX_FILEPATH_CHAR_SIZE + 1;
const int MAX_TITLE_SIZE = MAX_TITLE_CHAR_SIZE + 1;
const int MAX_MOVIETYPE_SIZE = MAX_MOVIETYPE_CHAR_SIZE + 1;

const int MAX_MOVIE_TYPE_SIZE = MAX_MOVIE_TYPE_CHAR_SIZE + 1;
const int MAX_CODEC_NAME_SIZE = MAX_CODEC_NAME_CHAR_SIZE + 1;

const int MAX_SCHEDULE_FIELD_SIZE = MAX_SCHEDULE_FIELD_CHAR_SIZE + 1;
const int MAX_SCHEDULE_NAME_SIZE = MAX_SCHEDULE_NAME_CHAR_SIZE + 1;
const int MAX_SCHEDULE_SCRIPT_SIZE = MAX_SCHEDULE_SCRIPT_CHAR_SIZE + 1;

const int MAX_NAME_SIZE = MAX_NAME_CHAR_SIZE + 1;
const int MAX_IP_SIZE = MAX_IP_CHAR_SIZE + 1;
const int MAX_GROUP_NAME_SIZE = MAX_GROUP_NAME_CHAR_SIZE + 1;
const int MAX_SQL_SIZE = MAX_SQL_CHAR_SIZE + 1;

const int MAX_ANALYSER_NAME_SIZE = MAX_ANALYSER_NAME_CHAR_SIZE + 1;
const int MAX_ANALYSER_DESC_SIZE = MAX_ANALYSER_DESC_CHAR_SIZE + 1;

const int MAX_URL_SIZE = MAX_URL_CHAR_SIZE + 1;
