﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "MemBuffer.h"

#include <memory.h>

MemBuffer::MemBuffer()
:m_Buf(nullptr), m_Size(0), m_Front(0), m_Rear(0)
{

}

MemBuffer::MemBuffer(size_t nSize)
:m_Buf(nullptr), m_Size(0), m_Front(0), m_Rear(0)
{
  this->Create(nSize);

}


MemBuffer::~MemBuffer()
{
  this->Destroy();

}

bool
MemBuffer::pushBack(char c)
{
  if (this->IsFull())
    return false;

  this->m_Buf[this->m_Rear++] = c;
  this->m_Rear = this->m_Rear % this->m_Size;

  return true;

}


bool
MemBuffer::popFront(char& c)
{
  if (this->IsEmpty())
    return false;

  c = this->m_Buf[this->m_Front++];
  this->m_Front = this->m_Front % this->m_Size;

  return true;
}


void
MemBuffer::Destroy(void)
{
  if (this->m_Buf)
  {
    delete[] this->m_Buf;

    this->m_Buf = nullptr;

  }

  this->m_Size = 0;
  this->m_Front = 0;
  this->m_Rear = 0;

}


void
MemBuffer::Clear(void)
{
  memset(this->m_Buf, 0x00, this->m_Size);

  this->m_Front = 0;
  this->m_Rear = 0;

}

bool
MemBuffer::Create(size_t nSize)
{
  nSize++;

  this->m_Buf = new char [nSize];
  memset(this->m_Buf, 0x00, nSize);

  if (this->m_Buf)
  {
    this->m_Front = 0;
    this->m_Rear = 0;
    this->m_Size = nSize;

    return true;

  }

  return false;

}

bool
MemBuffer::IsEmpty(void) const
{
  return this->m_Front == this->m_Rear;

}

bool
MemBuffer::IsFull(void) const
{
  return (this->m_Rear + 1) % this->m_Size == this->m_Front;

}

size_t
MemBuffer::GetReadableSize(void) const
{
  size_t nRet = 0;

  if (this->IsEmpty())
    return nRet;

  if (this->m_Front < this->m_Rear)
  {
    nRet = this->m_Rear - this->m_Front;

  }
  else
  {
    nRet = this->m_Size - this->m_Front + this->m_Rear;

  }

  return nRet;

}

size_t
MemBuffer::GetWritableSize(void) const
{
  size_t nRet = 0;

  if (this->IsFull())
    return nRet;

  if (this->IsEmpty())
    return this->GetSize();

  if (this->m_Front < this->m_Rear)
  {
    nRet = (this->m_Size-1) - this->m_Rear + this->m_Front;

  }
  else
  {
    nRet = this->m_Front - this->m_Rear - 1;

  }

  return nRet;

}

size_t
MemBuffer::GetSize(void) const
{
  return this->m_Size - 1;

}

size_t
MemBuffer::GetFrontPos(void) const
{
  return this->m_Front;

}

size_t
MemBuffer::GetRearPos(void) const
{
  return this->m_Rear;

}

bool
MemBuffer::Resize(size_t nSize)
{
  if (this->m_Size == nSize)
    return true;

  char *pBuf = new char [nSize];
  size_t nCopySize = (nSize > this->m_Size ? this->m_Size : nSize);

  if (pBuf)
  {
    memcpy(pBuf, this->m_Buf, nCopySize);

    delete [] this->m_Buf;

    this->m_Front = 0;
    this->m_Rear = 0;
    this->m_Size = nSize;
    this->m_Buf = pBuf;

    return true;

  }

  return false;

}

size_t
MemBuffer::Read(char *pBuf, size_t nSize, bool bNoMove)
{
  size_t nRet;
  size_t nOldPos = this->m_Front;


  for (nRet = 0; nRet < nSize; nRet++)
  {
    if (!this->popFront(pBuf[nRet]))
      break;

  }

  if (bNoMove)
  {
    this->m_Front = nOldPos;

  }

  return nRet;

}

size_t
MemBuffer::Write(const char *pBuf, size_t nSize, bool bNoMove)
{
  size_t nRet;
  size_t nOldPos = this->m_Rear;

  for (nRet = 0; nRet < nSize; nRet++)
  {
    if (!this->pushBack(pBuf[nRet]))
      break;
  }

  if (bNoMove)
  {
    this->m_Rear = nOldPos;

  }

  return nRet;

}

