﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#define _SECURE_SCL 0

#include "RetreiveSubtitle.h"
#include "network.h"

#include <wctype.h>
#include <string>
#include <algorithm>
#include <stdio.h>
#include <memory.h>

using namespace std;

const int RetreiveSubtitle::AUDIO_BODY_SIZE = 163840;
const int RetreiveSubtitle::MOVIE_BODY_SIZE = 1048576;
const char *RetreiveSubtitle::ALSONG_SERVER = "lyrics.alsong.co.kr";
const char *RetreiveSubtitle::ALSONG_URL = "/alsongwebservice/service1.asmx";
const char *RetreiveSubtitle::ALSONG_CLIENT_ID = "aed58bdb83dd73e48e262852f33148923e1d3af3b2663712645643d2d8d1e863771d52c283e04e97e87611038bf375e788c886cf7a6e3eecb1b44580b690422687c1ba078fe26f1b203c99a231b6f43fdc1283be9dd61bd311d1d0f0a37002cd800a8e6aa658880cca3a4cf5915e003d35d17d2172ea45837c77574b6a9883ef";

const char *RetreiveSubtitle::GOM_SERVER = "playerapi.gomlab.com";
const char *RetreiveSubtitle::GOM_URL = "/subtitle/jamakcheck.html";

const uint8_t RetreiveSubtitle::MP3_SIG[3] = {'I', 'D', '3'};
const uint8_t RetreiveSubtitle::OGG_SIG[4] = {'O', 'g', 'g', 'S'};
const uint8_t RetreiveSubtitle::FLAC_SIG[4] = {'f', 'L', 'a', 'C'};
const uint8_t RetreiveSubtitle::WMA_SIG[16] = {0x30, 0x26, 0xB2, 0x75, 0x8E, 0x66, 0xCF, 0x11, 0xA6, 0xD9, 0x00, 0xAA, 0x00, 0x62, 0xCE, 0x6C};
const uint8_t RetreiveSubtitle::M4A_SIG[7] = {'f', 't', 'y', 'p', 'M', '4', 'A'};

const uint8_t RetreiveSubtitle::OGG_DATA_SIG[7] = {0x05, 'v', 'o', 'r', 'b', 'i', 's'};
const uint8_t RetreiveSubtitle::WMA_DATA_SIG[16] = {0x36, 0x26, 0xB2, 0x75, 0x8E, 0x66, 0xCF, 0x11, 0xA6, 0xD9, 0x00, 0xAA, 0x00, 0x62, 0xCE, 0x6C};

RetreiveSubtitle::RetreiveSubtitle()
{

}

bool RetreiveSubtitle::getLyrics(const wstring &filePath, wstring *ret)
{
    uint8_t *buf = new uint8_t[AUDIO_BODY_SIZE];
    string md5;
    string sendData;
    wstring recvData;

    if (!this->readBody(filePath, buf, AUDIO_BODY_SIZE))
    {
        delete[] buf;
        return false;
    }

    this->getMD5(buf, AUDIO_BODY_SIZE, &md5);
    this->constructData(md5, &sendData);

    delete[] buf;

    if (!this->retreiveFromAlSongServer(sendData, &recvData))
        return false;

    return this->extractLyrics(recvData, ret);
}

bool RetreiveSubtitle::existSubtitle(const wstring &filePath, const string &fileName, string *url)
{
    FILE *file = this->openFile(filePath);
    FILE_CLOSER closer(file);
    string md5;
    wstring recvData;

    if (file == nullptr)
      return false;

    uint8_t *buf = new uint8_t[MOVIE_BODY_SIZE];

    if (fread(buf, 1, MOVIE_BODY_SIZE, file) != (size_t)MOVIE_BODY_SIZE)
    {
        delete[] buf;
        return false;
    }

    this->getMD5(buf, MOVIE_BODY_SIZE, &md5);

    delete[] buf;

    if (!this->retreiveFromGOMServer(md5, &recvData))
        return false;

    if (this->extractExistSubtitle(recvData))
    {
        string urlFileName;

        this->escapeURL(fileName, &urlFileName);

        url->append("http://");
        url->append(GOM_SERVER);
        url->append("/index.gom?type=subtitle_search&gomReq=1&keyword=");
        url->append(md5);
        url->append("&fn2=");
        url->append(urlFileName);

        return true;
    }

    return false;
}

int64_t RetreiveSubtitle::findPattern(FILE *file, const uint8_t *pattern, size_t size)
{
    if (size == 0)
        return -1;

    while (!feof(file))
    {
        uint8_t bytes[4096 * 2];
        size_t read;

        read = fread(bytes, 1, sizeof(bytes), file);

        if (read == 0)
            break;

        for (size_t i = 0; i < read - size; i++)
        {
            uint8_t *cur = &bytes[i];

            if (memcmp(pattern, cur, size) != 0)
                continue;

            size_t offset = read - (i + size);

            fseek(file, (long)(-1 * offset), SEEK_CUR);

            return ftell(file);
        }
    }

    return -1;
}

bool RetreiveSubtitle::readAudioHeaderMP3(FILE *file, uint32_t *header, int32_t *read, bool *sig)
{
    uint16_t sync;

    *header = 0;
    *read = 0;
    *sig = false;

    if (fread(&sync, 1, 2, file) != 2)
        return false;

    *read += 2;
    sync = ntohs(sync);

    if ((sync & 0xffe0) == 0xffe0)
    {
        if (fread(header, 1, 2, file) != 2)
            return false;

        *read += 2;

        *header = ntohs(*header);
        *header = ((uint32_t)sync << 16) | *header;

        *sig = true;
    }
    else
    {
        *sig = false;
    }

    return true;
}

bool RetreiveSubtitle::findDataMP3(FILE *file)
{
    do
    {
        long cur = ftell(file);
        long next = cur + 1;
        uint32_t header = 0;
        int32_t read = 0;
        bool sig = false;

        if (!this->readAudioHeaderMP3(file, &header, &read, &sig))
            return false;

        if (sig)
        {
            int32_t frameLen = this->calcFramePosMP3(header);

            if (frameLen < 0)
            {
                fseek(file, next, SEEK_SET);
                continue;
            }

            fseek(file, frameLen - read, SEEK_CUR);

            if (!this->readAudioHeaderMP3(file, &header, &read, &sig))
                return false;

            if (sig)
            {
                if (this->calcFramePosMP3(header) >= 0)
                {
                    fseek(file, cur, SEEK_SET);
                    return true;
                }
            }

            fseek(file, next, SEEK_SET);
        }
        else
        {
            fseek(file, -1, SEEK_CUR);
        }

    } while (true);

    return false;
}

int32_t RetreiveSubtitle::calcFramePosMP3(uint32_t header)
{
    int version = (header & 0x00180000) >> 19;
    int layer = (header & 0x00060000) >> 17;
    int bitRate = (header & 0xf000) >> 12;
    int sampleRate = (header & 0x0c00) >> 10;
    bool padding = (header & 0x0200) >> 9 ? true : false;
    const int sampleRateTable[][4] =
    {
        {22050, 24000, 16000, 0},
        {44100, 48000, 32000, 0}
    };

    const int bitRateTable[][16] =
    {
        {0, 32000, 40000, 48000, 56000, 64000, 80000, 96000, 112000, 128000, 160000, 192000, 224000, 256000, 320000, 0},
        {0,  8000, 16000, 24000, 32000, 40000, 48000, 56000,  64000,  80000,  96000, 112000, 128000, 144000, 160000, 0}
    };

    if (version != 2 && version != 3) // MPEG vesion 1 && 2
        return -1;

    if (layer != 1) // Layer 3
        return -1;

    if (bitRate == 0 || bitRate == 15)
        return -1;

    if (sampleRate == 3)
        return -1;

    int paddingSize = padding ? 1 : 0;
    int bitRateSelector = version == 2 ? 1 : 0;

    return 144 * bitRateTable[bitRateSelector][bitRate] / sampleRateTable[version - 2][sampleRate] + paddingSize;
}

bool RetreiveSubtitle::readBodyMP3(FILE *file, uint8_t *buf, size_t size)
{
    if (!this->findDataMP3(file))
        return false;

    if (fread(buf, 1, size, file) != size)
        return false;

    return true;
}

bool RetreiveSubtitle::getAtomHeader(FILE *file, uint32_t start, uint32_t *size, char type[5])
{
    uint32_t atomSize;

    fseek(file, start, SEEK_SET);

    if (fread(&atomSize, 1, sizeof(atomSize), file) != sizeof(atomSize))
        return false;

    atomSize = ntohl(atomSize);

    if (fread(type, 1, 4, file) != 4)
        return false;

    if (atomSize == 0)//파일 끝을 알리는 맨 마지막 atom
    {
        long totalSize;
        long orgPos = ftell(file);

        fseek(file, 0, SEEK_END);
        totalSize = ftell(file);
        fseek(file, orgPos, SEEK_SET);

        *size = uint32_t(totalSize - start);
    }
    else if (atomSize == 1)
    {
        //확장 atom은 미지원
        return false;
    }
    else
    {
        *size = atomSize;
    }

    return true;
}

bool RetreiveSubtitle::findAtom(FILE *file, uint32_t start, uint32_t end, const vector_string &types, uint32_t *nextStart, uint32_t *atomStart, uint32_t *atomSize)
{
    char byte4[5] = {0, };

    while (true)
    {
        if (end != 0 && start >= end)
        {
            *nextStart = start;

            break;
        }

        if (this->getAtomHeader(file, start, atomSize, byte4))
        {
            for (size_t i = 0; i < types.size(); i++)
            {
                if (strcmp(byte4, types[i].c_str()) == 0)
                {
                    *nextStart = start;
                    *nextStart += sizeof(uint32_t) * 2;

                    *atomStart = start;

                    return true;
                }
            }
        }
        else
        {
            return false;
        }

        start += *atomSize;
    }

    return false;
}

bool RetreiveSubtitle::parseMP3(FILE *file, uint8_t *buf, size_t size)
{
    uint16_t version;
    uint32_t id3Size;
    uint8_t flag;

    fseek(file, sizeof(MP3_SIG), SEEK_SET);

    do
    {
        if (fread(&version, 1, 2, file) != 2)
            return false;

        if (fread(&flag, 1, 1, file) != 1)
            return false;

        if (fread(&id3Size, 1, 4, file) != 4)
            return false;

        id3Size = ntohl(id3Size);
        id3Size = this->unsynchsafe(id3Size);

        fseek(file, id3Size, SEEK_CUR);

        uint8_t header[sizeof(MP3_SIG)] = {0, };

        if (fread(header, 1, sizeof(header), file) != sizeof(header))
            return false;

        if (memcmp(MP3_SIG, header, sizeof(header)) != 0)
        {
            if (memcmp(FLAC_SIG, header, sizeof(header)) == 0)
                return this->parseFLAC(file, buf, size);
            else
                break;
        }

    } while(true);

    fseek(file, -1 * (long)sizeof(MP3_SIG), SEEK_CUR);

    return this->readBodyMP3(file, buf, size);
}

bool RetreiveSubtitle::parseOGG(FILE *file, uint8_t *buf, size_t size)
{
    uint8_t tmp[4];
    int64_t pos;

    fseek(file, sizeof(OGG_SIG), SEEK_SET);

    pos = this->findPattern(file, OGG_DATA_SIG, sizeof(OGG_DATA_SIG));

    if (pos == -1)
        return false;

    if (fread(tmp, 1, 4, file) != 4)
        return false;

    if (fread(buf, 1, size, file) != size)
        return false;

    return true;
}

bool RetreiveSubtitle::parseFLAC(FILE *file, uint8_t *buf, size_t size)
{
    return this->readFirst(file, buf, size);
}

bool RetreiveSubtitle::parseWMA(FILE *file, uint8_t *buf, size_t size)
{
    uint8_t tmp[8];
    int64_t pos;

    fseek(file, sizeof(WMA_SIG), SEEK_SET);

    pos = this->findPattern(file, WMA_DATA_SIG, sizeof(WMA_DATA_SIG));

    if (pos == -1)
        return false;

    if (fread(tmp, 1, 8, file) != 8)
        return false;

    if (fread(buf, 1, size, file) != size)
        return false;

    return true;
}

bool RetreiveSubtitle::parseM4A(FILE *file, uint8_t *buf, size_t size)
{
    uint32_t start;
    uint32_t nextStart;
    uint32_t atomStart;
    uint32_t atomSize;
    vector_string types;

    types.push_back("moov");
    types.push_back("moof");
    types.push_back("mfra");
    types.push_back("free");
    types.push_back("skip");
    types.push_back("pnot");

    start = 0;

    while (this->findAtom(file, start, 0, types, &nextStart, &atomStart, &atomSize))
        start = atomStart + atomSize;

    fseek(file, start, SEEK_SET);

    return fread(buf, 1, size, file) == size;
}

bool RetreiveSubtitle::assumeMP3(const wstring &filePath, FILE *file, uint8_t *buf, size_t size)
{
    wstring::size_type index = filePath.find_last_of(L'.');

    if (index != wstring::npos)
    {
        wstring ext = filePath.substr(index + 1);

        transform(ext.begin(), ext.end(), ext.begin(), towlower);

        if (ext == L"mp3")
        {
            fseek(file, 0, SEEK_SET);

            return this->readBodyMP3(file, buf, size);
        }
    }

    return false;
}

bool RetreiveSubtitle::readFirst(FILE *file, uint8_t *buf, size_t size)
{
    fseek(file, 0, SEEK_SET);

    return fread(buf, 1, size, file) == size;
}

bool RetreiveSubtitle::readBody(const wstring &filePath, uint8_t *buf, size_t size)
{
    FILE *file = this->openFile(filePath);
    FILE_CLOSER closer(file);

    if (file == nullptr)
        return false;

    uint8_t header[16] = {0, };

    if (fread(header, 1, 16, file) != 16)
        return false;

    if (memcmp(MP3_SIG, header, sizeof(MP3_SIG)) == 0)
        return this->parseMP3(file, buf, size);
    else if (memcmp(OGG_SIG, header, sizeof(OGG_SIG)) == 0)
        return this->parseOGG(file, buf, size);
    else if (memcmp(FLAC_SIG, header, sizeof(FLAC_SIG)) == 0)
        return this->parseFLAC(file, buf, size);
    else if (memcmp(WMA_SIG, header, sizeof(WMA_SIG)) == 0)
        return this->parseWMA(file, buf, size);
    else if (memcmp(M4A_SIG, &header[4], sizeof(M4A_SIG)) == 0)
        return this->parseM4A(file, buf, size);
    else
        return this->assumeMP3(filePath, file, buf, size);
}

bool RetreiveSubtitle::extractLyrics(const wstring &data, wstring *ret)
{
    wstring firstTag = L"<strLyric>";
    wstring::size_type start = data.find(firstTag);
    wstring::size_type end = data.find(L"</strLyric>");

    if (start == wstring::npos || end == wstring::npos)
        return false;

    start += firstTag.size();
    this->escape(data.substr(start, end - start), ret);

    return true;
}

bool RetreiveSubtitle::extractExistSubtitle(const wstring &data)
{
    return data.find(L"\"exist\"") != wstring::npos;
}

void RetreiveSubtitle::escape(const wstring &data, wstring *ret)
{
    wstring tmp = data;

    this->replace(tmp, L"&quot;", L"\"");
    this->replace(tmp, L"&amp;", L"&");
    this->replace(tmp, L"&lt;", L"<");
    this->replace(tmp, L"&gt;", L">");
    this->replace(tmp, L"&nbsp;", L" ");

    this->replace(tmp, L"<br>", L"\n");

    *ret = tmp;
}

void RetreiveSubtitle::replace(wstring &data, const wstring &oldVal, const wstring &newVal)
{
    wstring::size_type pos = 0;
    wstring::size_type offset = 0;

    while ((pos = data.find(oldVal, offset)) != wstring::npos)
    {
        data.replace(data.begin() + pos, data.begin() + pos + oldVal.size(), newVal);
        offset = pos + newVal.size();
    }
}

void RetreiveSubtitle::constructData(const string &md5, string *ret)
{
    ret->append("<?xml version='1.0' encoding='UTF-8'?>");
    ret->append("<SOAP-ENV:Envelope ");
    ret->append("xmlns:SOAP-ENV='http://www.w3.org/2003/05/soap-envelope' ");
    ret->append("xmlns:SOAP-ENC='http://www.w3.org/2003/05/soap-encoding' ");
    ret->append("xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' ");
    ret->append("xmlns:xsd='http://www.w3.org/2001/XMLSchema' ");
    ret->append("xmlns:ns2='ALSongWebServer/Service1Soap' ");
    ret->append("xmlns:ns1='ALSongWebServer' ");
    ret->append("xmlns:ns3='ALSongWebServer/Service1Soap12'>");
    ret->append("<SOAP-ENV:Body>");
    ret->append("<ns1:GetLyric7>");
    ret->append("<ns1:encData>");
    ret->append(ALSONG_CLIENT_ID);
    ret->append("</ns1:encData>");
    ret->append("<ns1:stQuery>");
    ret->append("<ns1:strChecksum>");
    ret->append(md5.c_str());
    ret->append("</ns1:strChecksum>");
    ret->append("<ns1:strVersion>3.46</ns1:strVersion>");
    ret->append("<ns1:strMACAddress></ns1:strMACAddress>");
    ret->append("<ns1:strIPAddress>255.255.255.0</ns1:strIPAddress>");
    ret->append("</ns1:stQuery>");
    ret->append("</ns1:GetLyric7>");
    ret->append("</SOAP-ENV:Body>");
    ret->append("</SOAP-ENV:Envelope>");
}

uint32_t RetreiveSubtitle::unsynchsafe(uint32_t synchsafe)
{
    uint32_t ret = 0;
    uint32_t mask = 0x7f000000;

    while (mask)
    {
        ret >>= 1;
        ret |= synchsafe & mask;
        mask >>= 8;
    }

    return ret;
}

void RetreiveSubtitle::escapeURL(const string &fileName, string *ret)
{
    char hex[] = "0123456789abcdef";

    for (string::size_type i = 0; i < fileName.size(); i++)
    {
        char tmp[4] = {0, };
        char c = fileName.at(i);

        tmp[0] = '%';
        tmp[1] = hex[(c >> 4) & 0x0f];
        tmp[2] = hex[c & 0x0f];

        ret->append(tmp);
    }
}
