/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#define PATCH 0
#define MINOR 2
#define MAJOR 2

#if defined QT_NO_DEBUG || defined NDEBUG
# define STRFILEVER "Release"
#else
# define STRFILEVER "Debug"
#endif

#define STRPRODUCTVER STRFILEVER

#define TRANSLATION "041204b0"

#define COMPANY "dcple.com"
#define COPYRIGHT "Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com) All rights reserved."
