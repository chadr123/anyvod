/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "ParentDlg.h"

struct ANYVOD_PACKET_USER_INFO;
union ANYVOD_PACKET;

// CUserManagementDlg 대화 상자입니다.

class CUserManagementDlg : public CParentDlg
{
	DECLARE_DYNAMIC(CUserManagementDlg)

public:
	CUserManagementDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CUserManagementDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_USER_MANAGEMENT_DIALOG };

public:
  CListCtrl& GetUserList();
  void AddUserToList(unsigned long long pid, ANYVOD_PACKET_USER_INFO &item);

private:
  void DeleteUserItems();
  void ToggleUserItemControls();
  bool ProcessPacket();
  void UpdateTotal();
  virtual bool HandlePacket(ANYVOD_PACKET *packet);
  virtual void InitControls();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:
  CStatic m_total;
  CButton m_close;
  CButton m_delete;
  CButton m_modify;
  CButton m_add;
  CButton m_refresh;
  CListCtrl m_userList;
  unsigned int m_itemCount;
  unsigned int m_itemCurCount;

  afx_msg void OnBnClickedButtonUserManagementRefresh();
  afx_msg void OnBnClickedButtonUserManagementAdd();
  afx_msg void OnBnClickedButtonUserManagementModify();
  afx_msg void OnBnClickedButtonUserManagementDelete();
  afx_msg void OnBnClickedButtonUserManagementClose();
  afx_msg void OnNMClickListUserManagementUserlist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnNMDblclkListUserManagementUserlist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnNMReturnListUserManagementUserlist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnLvnGetdispinfoListUserManagementUserlist(NMHDR *pNMHDR, LRESULT *pResult);
protected:
  virtual void OnCancel();
public:
  afx_msg void OnLvnItemchangedListUserManagementUserlist(NMHDR *pNMHDR, LRESULT *pResult);
};
