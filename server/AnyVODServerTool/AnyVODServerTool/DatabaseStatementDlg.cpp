/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

// DatabaseStatementDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "AnyVODServerTool.h"
#include "DatabaseStatementDlg.h"
#include "AnyVODServerToolDlg.h"
#include "ViewSQLDlg.h"
#include "Socket.h"
#include "Util.h"

// CDatabaseStatementDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDatabaseStatementDlg, CDialog)

CDatabaseStatementDlg::CDatabaseStatementDlg(CWnd* pParent /*=nullptr*/)
	: CParentDlg(CDatabaseStatementDlg::IDD, pParent),
  m_itemCount(0), m_itemCurCount(0), m_maxCount(0)
{
  this->m_itemCount = 0;
  this->m_itemCurCount = 0;
  this->m_maxCount = 0;

}

CDatabaseStatementDlg::~CDatabaseStatementDlg()
{
}

void CDatabaseStatementDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_STATIC_DATABASE_STATEMENT_TOTAL, m_total);
  DDX_Control(pDX, IDC_LIST_DATABASE_STATEMENT_STATEMENTLIST, m_statementList);
  DDX_Control(pDX, IDC_BUTTON_DATABASE_STATEMENT_VIEW_SQL, m_viewSQL);
  DDX_Control(pDX, IDC_STATIC_DATABASE_STATEMENT_MAX, m_max);
}


BEGIN_MESSAGE_MAP(CDatabaseStatementDlg, CDialog)
  ON_BN_CLICKED(IDC_BUTTON_DATABASE_STATEMENT_CLOSE, &CDatabaseStatementDlg::OnBnClickedButtonDatabaseStatementClose)
  ON_BN_CLICKED(IDC_BUTTON_DATABASE_STATEMENT_VIEW_SQL, &CDatabaseStatementDlg::OnBnClickedButtonDatabaseStatementViewSql)
  ON_BN_CLICKED(IDC_BUTTON_DATABASE_STATEMENT_REFRESH, &CDatabaseStatementDlg::OnBnClickedButtonDatabaseStatementRefresh)
  ON_NOTIFY(NM_CLICK, IDC_LIST_DATABASE_STATEMENT_STATEMENTLIST, &CDatabaseStatementDlg::OnNMClickListDatabaseStatementStatementlist)
  ON_NOTIFY(NM_DBLCLK, IDC_LIST_DATABASE_STATEMENT_STATEMENTLIST, &CDatabaseStatementDlg::OnNMDblclkListDatabaseStatementStatementlist)
  ON_NOTIFY(NM_RETURN, IDC_LIST_DATABASE_STATEMENT_STATEMENTLIST, &CDatabaseStatementDlg::OnNMReturnListDatabaseStatementStatementlist)
  ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_DATABASE_STATEMENT_STATEMENTLIST, &CDatabaseStatementDlg::OnLvnItemchangedListDatabaseStatementStatementlist)
  ON_WM_CLOSE()
END_MESSAGE_MAP()

void CDatabaseStatementDlg::InitControls()
{
  this->ToggleViewSQLControls();

  this->m_statementList.InsertColumn(0, L"SQL", LVCFMT_LEFT, 173);
  this->m_statementList.InsertColumn(1, L"총 개수", LVCFMT_RIGHT, 55);
  this->m_statementList.InsertColumn(2, L"사용 중", LVCFMT_RIGHT, 55);
  this->m_statementList.InsertColumn(3, L"미사용 중", LVCFMT_RIGHT, 65);
  this->m_statementList.SetExtendedStyle(LVS_EX_GRIDLINES| LVS_EX_FULLROWSELECT);
  this->m_statementList.SetImageList(&Util::GetIconImageList(), LVSIL_SMALL);

  this->UpdateTotal();

  this->OnBnClickedButtonDatabaseStatementRefresh();

}

bool CDatabaseStatementDlg::HandlePacket(ANYVOD_PACKET *packet)
{
  switch (ntohl(packet->s2c_header.header.type))
  {
  case PT_ADMIN_S2C_STATEMENT_STATUSLIST:
    {
      if (packet->s2c_header.success)
      {
        this->m_itemCurCount = 0;
        this->m_itemCount = ntohl(packet->admin_s2c_statement_statuslist.count);
        this->m_maxCount = ntohl(packet->admin_s2c_statement_statuslist.maxCount);

        this->DeleteStatementItems();

        this->UpdateTotal();

        return this->m_itemCount > 0;

      }

      break;

    }
  case PT_ADMIN_S2C_STATEMENT_STATUS_ITEM:
    {
      if (packet->s2c_header.success)
      {
        ANYVOD_PACKET_STATEMENT_STATUS &status = packet->admin_s2c_statement_status_item.status;
        wstring sql;
        LVITEM lvi;

        Util::ConvertAsciiToUnicode((char*)status.sql, &sql);

        lvi.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
        lvi.iItem = this->m_itemCurCount;
        lvi.iSubItem = 0;
        lvi.pszText = (LPWSTR)sql.c_str();
        lvi.lParam = (LPARAM)new wstring(sql);
        lvi.iImage = ICON_INDEX_SQL;

        this->m_statementList.InsertItem(&lvi);

        CString count;

        lvi.mask = LVIF_TEXT;
        lvi.iSubItem = 1;

        count.Format(L"%d 개", ntohl(status.totalCount));
        lvi.pszText = (LPWSTR)count.GetString();

        this->m_statementList.SetItem(&lvi);

        lvi.iSubItem = 2;

        count.Format(L"%d 개", ntohl(status.usingCount));
        lvi.pszText = (LPWSTR)count.GetString();

        this->m_statementList.SetItem(&lvi);

        lvi.iSubItem = 3;

        count.Format(L"%d 개", ntohl(status.totalCount) - ntohl(status.usingCount));
        lvi.pszText = (LPWSTR)count.GetString();

        this->m_statementList.SetItem(&lvi);

        this->m_itemCurCount++;

        if (this->m_itemCount == this->m_itemCurCount)
        {
          return false;

        }
        else
        {
          return true;

        }

      }

      break;

    }

  }

  return false;

}

void CDatabaseStatementDlg::DeleteStatementItems()
{
  for (int i = 0; i < this->m_statementList.GetItemCount(); i++)
  {
    delete (wstring*)this->m_statementList.GetItemData(i);

  }

  this->m_statementList.DeleteAllItems();

}

bool CDatabaseStatementDlg::ProcessPacket()
{
  bool nextProcess = false;
  bool success = false;
  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;
  Socket &socket = dlg->GetSocket();

  do
  {
    if (!Util::HandlePacket(socket, this, &nextProcess, &success))
    {
      this->EndDialog(IDCLOSE);

      break;

    }

  } while (nextProcess);

  return success;

}

CListCtrl& CDatabaseStatementDlg::GetStatementList()
{
  return this->m_statementList;

}

void CDatabaseStatementDlg::ToggleViewSQLControls()
{
  int count = this->m_statementList.GetSelectedCount();
  bool enable = count != 0;

  if (count > 1)
  {
    this->m_viewSQL.EnableWindow(false);

  }
  else
  {
    this->m_viewSQL.EnableWindow(enable);

  }

  if (this->GetFocus() == nullptr)
  {
    this->SetFocus();

  }

}

void CDatabaseStatementDlg::UpdateTotal()
{
  CString count;

  count.Format(L"%d 개", this->m_itemCount);
  this->m_total.SetWindowText(count);

  count.Format(L"%d 개", this->m_maxCount);
  this->m_max.SetWindowText(count);

}

// CDatabaseStatementDlg 메시지 처리기입니다.

void CDatabaseStatementDlg::OnBnClickedButtonDatabaseStatementClose()
{
  this->DeleteStatementItems();
  this->EndDialog(IDCANCEL);
}

void CDatabaseStatementDlg::OnBnClickedButtonDatabaseStatementViewSql()
{
  CViewSQLDlg dlg(this);

  dlg.DoModal();

}

void CDatabaseStatementDlg::OnBnClickedButtonDatabaseStatementRefresh()
{
  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;

  dlg->GetSocket().SendStatementList();
  this->ProcessPacket();

  this->ToggleViewSQLControls();
}

void CDatabaseStatementDlg::OnNMClickListDatabaseStatementStatementlist(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

  this->ToggleViewSQLControls();

  *pResult = 0;
}

void CDatabaseStatementDlg::OnNMDblclkListDatabaseStatementStatementlist(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

  this->OnBnClickedButtonDatabaseStatementViewSql();

  *pResult = 0;
}

void CDatabaseStatementDlg::OnNMReturnListDatabaseStatementStatementlist(NMHDR *pNMHDR, LRESULT *pResult)
{
  this->OnBnClickedButtonDatabaseStatementViewSql();

  *pResult = 0;
}

void CDatabaseStatementDlg::OnLvnItemchangedListDatabaseStatementStatementlist(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

  this->ToggleViewSQLControls();

  *pResult = 0;
}

void CDatabaseStatementDlg::OnClose()
{
  this->DeleteStatementItems();

  CParentDlg::OnClose();
}
