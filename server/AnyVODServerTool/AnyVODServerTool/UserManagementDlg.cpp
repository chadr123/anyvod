/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

// UserManagementDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "AnyVODServerTool.h"
#include "UserManagementDlg.h"
#include "UserManagementAddDlg.h"
#include "UserManagementModifyDlg.h"
#include "AnyVODServerToolDlg.h"
#include "Socket.h"
#include "Util.h"

// CUserManagementDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CUserManagementDlg, CDialog)

CUserManagementDlg::CUserManagementDlg(CWnd* pParent /*=nullptr*/)
	: CParentDlg(CUserManagementDlg::IDD, pParent), m_itemCount(0), m_itemCurCount(0)
{

}

CUserManagementDlg::~CUserManagementDlg()
{
}

void CUserManagementDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_LIST_USER_MANAGEMENT_USERLIST, m_userList);
  DDX_Control(pDX, IDC_STATIC_USER_MANAGEMENT_TOTAL, m_total);
  DDX_Control(pDX, IDC_BUTTON_USER_MANAGEMENT_CLOSE, m_close);
  DDX_Control(pDX, IDC_BUTTON_USER_MANAGEMENT_DELETE, m_delete);
  DDX_Control(pDX, IDC_BUTTON_USER_MANAGEMENT_MODIFY, m_modify);
  DDX_Control(pDX, IDC_BUTTON_USER_MANAGEMENT_ADD, m_add);
  DDX_Control(pDX, IDC_BUTTON_USER_MANAGEMENT_REFRESH, m_refresh);
}


BEGIN_MESSAGE_MAP(CUserManagementDlg, CDialog)
  ON_BN_CLICKED(IDC_BUTTON_USER_MANAGEMENT_REFRESH, &CUserManagementDlg::OnBnClickedButtonUserManagementRefresh)
  ON_BN_CLICKED(IDC_BUTTON_USER_MANAGEMENT_ADD, &CUserManagementDlg::OnBnClickedButtonUserManagementAdd)
  ON_BN_CLICKED(IDC_BUTTON_USER_MANAGEMENT_MODIFY, &CUserManagementDlg::OnBnClickedButtonUserManagementModify)
  ON_BN_CLICKED(IDC_BUTTON_USER_MANAGEMENT_DELETE, &CUserManagementDlg::OnBnClickedButtonUserManagementDelete)
  ON_BN_CLICKED(IDC_BUTTON_USER_MANAGEMENT_CLOSE, &CUserManagementDlg::OnBnClickedButtonUserManagementClose)
  ON_NOTIFY(NM_CLICK, IDC_LIST_USER_MANAGEMENT_USERLIST, &CUserManagementDlg::OnNMClickListUserManagementUserlist)
  ON_NOTIFY(NM_DBLCLK, IDC_LIST_USER_MANAGEMENT_USERLIST, &CUserManagementDlg::OnNMDblclkListUserManagementUserlist)
  ON_NOTIFY(NM_RETURN, IDC_LIST_USER_MANAGEMENT_USERLIST, &CUserManagementDlg::OnNMReturnListUserManagementUserlist)
  ON_NOTIFY(LVN_GETDISPINFO, IDC_LIST_USER_MANAGEMENT_USERLIST, &CUserManagementDlg::OnLvnGetdispinfoListUserManagementUserlist)
  ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_USER_MANAGEMENT_USERLIST, &CUserManagementDlg::OnLvnItemchangedListUserManagementUserlist)
END_MESSAGE_MAP()

CListCtrl& CUserManagementDlg::GetUserList()
{
  return this->m_userList;

}

void CUserManagementDlg::InitControls()
{
  this->UpdateTotal();

  this->m_userList.InsertColumn(0, L"아이디", LVCFMT_LEFT, 356);
  this->m_userList.InsertColumn(1, L"이름", LVCFMT_CENTER, 58);
  this->m_userList.InsertColumn(2, L"관리자", LVCFMT_CENTER, 50);

  this->m_userList.SetExtendedStyle(LVS_EX_GRIDLINES| LVS_EX_FULLROWSELECT);
  this->m_userList.SetImageList(&Util::GetIconImageList(), LVSIL_SMALL);

  this->ToggleUserItemControls();

  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;

  dlg->GetSocket().SendUserList();
  this->ProcessPacket();

}

void CUserManagementDlg::ToggleUserItemControls()
{
  bool enable = this->m_userList.GetSelectedCount() != 0;

  this->m_delete.EnableWindow(enable);
  this->m_modify.EnableWindow(enable);

  if (this->GetFocus() == nullptr)
  {
    this->SetFocus();

  }

}

void CUserManagementDlg::AddUserToList(unsigned long long pid, ANYVOD_PACKET_USER_INFO &item)
{
  ANYVOD_USER_INFO *info = new ANYVOD_USER_INFO;

  info->admin = item.admin != 0;
  Util::ConvertUTF8ToUnicode((char*)item.id, MAX_ID_SIZE, &info->id);
  Util::ConvertUTF8ToUnicode((char*)item.pass, MAX_PASS_SIZE, &info->pass);
  Util::ConvertUTF8ToUnicode((char*)item.name, MAX_NAME_SIZE, &info->name);
  info->pid = pid;

  LVITEM lvi;
  LPTSTR adminText = nullptr;

  if (info->admin)
  {
    adminText = L"예";

  }
  else
  {
    adminText = L"아니오";

  }

  lvi.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
  lvi.iItem = this->m_itemCurCount;
  lvi.iSubItem = 0;
  lvi.pszText = (LPWSTR)info->id.c_str();
  lvi.lParam = (LPARAM)info;
  lvi.iImage = I_IMAGECALLBACK;

  this->m_userList.InsertItem(&lvi);

  lvi.mask = LVIF_TEXT;
  lvi.iSubItem = 1;
  lvi.pszText = (LPWSTR)info->name.c_str();;

  this->m_userList.SetItem(&lvi);

  lvi.mask = LVIF_TEXT;
  lvi.iSubItem = 2;
  lvi.pszText = adminText;

  this->m_userList.SetItem(&lvi);

  this->m_itemCurCount++;

}

void CUserManagementDlg::UpdateTotal()
{
  CString count;

  count.Format(L"%d", this->m_itemCount);
  Util::NumberFormatting(&count);
  count += L" 개";
  this->m_total.SetWindowText(count);

}

void CUserManagementDlg::DeleteUserItems()
{
  for (int i = 0; i < this->m_userList.GetItemCount(); i++)
  {
    delete (ANYVOD_USER_INFO*)this->m_userList.GetItemData(i);

  }

  this->m_userList.DeleteAllItems();

}

bool CUserManagementDlg::HandlePacket(ANYVOD_PACKET *packet)
{
  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;

  switch (ntohl(packet->s2c_header.header.type))
  {
  case PT_ADMIN_S2C_USER_DELETE:
    {
      break;

    }
  case PT_ADMIN_S2C_USERLIST:
    {
      if (packet->s2c_header.success)
      {
        this->m_itemCount = ntohl(packet->admin_s2c_userlist.count);
        this->m_itemCurCount = 0;
        this->DeleteUserItems();

        this->UpdateTotal();

        return this->m_itemCount > 0;

      }

      break;

    }
  case PT_ADMIN_S2C_USER_ITEM:
    {
      if (packet->s2c_header.success)
      {
        this->AddUserToList(ntohll(packet->admin_s2c_user_item.pid),
          packet->admin_s2c_user_item.detail);

        if (this->m_itemCurCount == this->m_itemCount)
        {
          return false;

        }
        else
        {
          return true;

        }

      }

      break;

    }

  }

  return false;

}

bool CUserManagementDlg::ProcessPacket()
{
  bool nextProcess = false;
  bool success = false;
  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;
  Socket &socket = dlg->GetSocket();

  do
  {
    if (!Util::HandlePacket(socket, this, &nextProcess, &success))
    {
      this->DeleteUserItems();
      this->EndDialog(IDCLOSE);

      break;

    }

  } while (nextProcess);

  return success;

}

// CUserManagementDlg 메시지 처리기입니다.

void CUserManagementDlg::OnBnClickedButtonUserManagementRefresh()
{
  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;

  dlg->GetSocket().SendUserList();
  this->ProcessPacket();

  this->ToggleUserItemControls();

}

void CUserManagementDlg::OnBnClickedButtonUserManagementAdd()
{
  CUserManagementAddDlg dlg(this);
  INT_PTR result = dlg.DoModal();

  if (result == IDCLOSE)
  {
    this->DeleteUserItems();
    this->EndDialog(IDCLOSE);

  }
  else if (result == IDOK)
  {
    this->m_itemCount++;

    this->UpdateTotal();

    Util::ListControlScrollToEnd(this->m_userList);

  }

  this->ToggleUserItemControls();

}

void CUserManagementDlg::OnBnClickedButtonUserManagementModify()
{
  CUserManagementModifyDlg dlg(this);
  int count = this->m_userList.GetSelectedCount();

  for (int i = 0; i < count; i++)
  {
    POSITION pos = this->m_userList.GetFirstSelectedItemPosition();
    int item = this->m_userList.GetNextSelectedItem(pos);
    INT_PTR result = dlg.DoModal();

    if (result == IDCLOSE)
    {
      this->DeleteUserItems();
      this->EndDialog(IDCLOSE);

      break;

    }
    else if (result == IDABORT)
    {
      break;

    }

    this->m_userList.SetItemState(item, 0, LVIS_SELECTED | LVIS_FOCUSED);

  }

  this->ToggleUserItemControls();

}

void CUserManagementDlg::OnBnClickedButtonUserManagementDelete()
{
  POSITION pos = this->m_userList.GetFirstSelectedItemPosition();

  if (pos != NULL)
  {
    if (AfxMessageBox(L"사용자를 삭제 하시겠습니까?", MB_YESNO) == IDYES)
    {
      CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;

      while (pos)
      {
        int item = this->m_userList.GetNextSelectedItem(pos);
        ANYVOD_USER_INFO *info = (ANYVOD_USER_INFO*)this->m_userList.GetItemData(item);

        dlg->GetSocket().SendUserDelete(info->pid);

        if (this->ProcessPacket())
        {
          delete info;

          this->m_itemCount--;
          this->m_itemCurCount--;

          this->m_userList.DeleteItem(item);

          pos = this->m_userList.GetFirstSelectedItemPosition();

        }
        else
        {
          if (pos != NULL)
          {
            if (AfxMessageBox(L"사용자를 삭제 하는 도중 오류가 발생 했습니다. 계속 하시겠습니까?", MB_YESNO) == IDNO)
            {
              break;

            }

          }

        }

      }

      this->UpdateTotal();

    }

    this->ToggleUserItemControls();

  }
  else
  {
    AfxMessageBox(L"선택된 사용자가 없습니다.");

  }

}

void CUserManagementDlg::OnBnClickedButtonUserManagementClose()
{
  this->DeleteUserItems();
  this->EndDialog(IDOK);

}

void CUserManagementDlg::OnNMClickListUserManagementUserlist(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

  this->ToggleUserItemControls();

  *pResult = 0;
}

void CUserManagementDlg::OnNMDblclkListUserManagementUserlist(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

  this->OnBnClickedButtonUserManagementModify();

  *pResult = 0;
}

void CUserManagementDlg::OnNMReturnListUserManagementUserlist(NMHDR *pNMHDR, LRESULT *pResult)
{
  this->OnBnClickedButtonUserManagementModify();

  *pResult = 0;
}

void CUserManagementDlg::OnLvnGetdispinfoListUserManagementUserlist(NMHDR *pNMHDR, LRESULT *pResult)
{
  NMLVDISPINFO *pDispInfo = reinterpret_cast<NMLVDISPINFO*>(pNMHDR);
  ANYVOD_USER_INFO *info = (ANYVOD_USER_INFO*)pDispInfo->item.lParam;

  pDispInfo->item.iImage = info->admin ? ICON_INDEX_ADMIN : ICON_INDEX_USER;

  *pResult = 0;
}

void CUserManagementDlg::OnCancel()
{
  this->DeleteUserItems();

  CParentDlg::OnCancel();
}

void CUserManagementDlg::OnLvnItemchangedListUserManagementUserlist(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

  if (pNMLV->uOldState == 0)
  {
    this->ToggleUserItemControls();

  }

  *pResult = 0;
}
