/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

// GroupManagementDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "AnyVODServerTool.h"
#include "GroupManagementDlg.h"
#include "GroupManagementAddDlg.h"
#include "GroupManagementModifyDlg.h"
#include "AnyVODServerToolDlg.h"
#include "Socket.h"
#include "Util.h"

// CGroupManagementDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CGroupManagementDlg, CDialog)

CGroupManagementDlg::CGroupManagementDlg(CWnd* pParent /*=nullptr*/)
	: CParentDlg(CGroupManagementDlg::IDD, pParent), m_itemCount(0), m_itemCurCount(0)
{

}

CGroupManagementDlg::~CGroupManagementDlg()
{
}

void CGroupManagementDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_LIST_GROUP_MANAGEMENT_GROUPLIST, m_groupList);
  DDX_Control(pDX, IDC_STATIC_GROUP_MANAGEMENT_TOTAL, m_total);
  DDX_Control(pDX, IDC_BUTTON_GROUP_MANAGEMENT_CLOSE, m_close);
  DDX_Control(pDX, IDC_BUTTON_GROUP_MANAGEMENT_DELETE, m_delete);
  DDX_Control(pDX, IDC_BUTTON_GROUP_MANAGEMENT_MODIFY, m_modify);
  DDX_Control(pDX, IDC_BUTTON_GROUP_MANAGEMENT_ADD, m_add);
  DDX_Control(pDX, IDC_BUTTON_GROUP_MANAGEMENT_REFRESH, m_refresh);
}


BEGIN_MESSAGE_MAP(CGroupManagementDlg, CDialog)
  ON_BN_CLICKED(IDC_BUTTON_GROUP_MANAGEMENT_REFRESH, &CGroupManagementDlg::OnBnClickedButtonGroupManagementRefresh)
  ON_BN_CLICKED(IDC_BUTTON_GROUP_MANAGEMENT_ADD, &CGroupManagementDlg::OnBnClickedButtonGroupManagementAdd)
  ON_BN_CLICKED(IDC_BUTTON_GROUP_MANAGEMENT_MODIFY, &CGroupManagementDlg::OnBnClickedButtonGroupManagementModify)
  ON_BN_CLICKED(IDC_BUTTON_GROUP_MANAGEMENT_DELETE, &CGroupManagementDlg::OnBnClickedButtonGroupManagementDelete)
  ON_BN_CLICKED(IDC_BUTTON_GROUP_MANAGEMENT_CLOSE, &CGroupManagementDlg::OnBnClickedButtonGroupManagementClose)
  ON_NOTIFY(NM_CLICK, IDC_LIST_GROUP_MANAGEMENT_GROUPLIST, &CGroupManagementDlg::OnNMClickListGroupManagementGrouplist)
  ON_NOTIFY(NM_DBLCLK, IDC_LIST_GROUP_MANAGEMENT_GROUPLIST, &CGroupManagementDlg::OnNMDblclkListGroupManagementGrouplist)
  ON_NOTIFY(NM_RETURN, IDC_LIST_GROUP_MANAGEMENT_GROUPLIST, &CGroupManagementDlg::OnNMReturnListGroupManagementGrouplist)
  ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_GROUP_MANAGEMENT_GROUPLIST, &CGroupManagementDlg::OnLvnItemchangedListGroupManagementGrouplist)
END_MESSAGE_MAP()

CListCtrl& CGroupManagementDlg::GetGroupList()
{
  return this->m_groupList;

}

void CGroupManagementDlg::InitControls()
{
  this->UpdateTotal();

  this->m_groupList.InsertColumn(0, L"이름", LVCFMT_LEFT, 464);

  this->m_groupList.SetExtendedStyle(LVS_EX_GRIDLINES| LVS_EX_FULLROWSELECT);
  this->m_groupList.SetImageList(&Util::GetIconImageList(), LVSIL_SMALL);

  this->ToggleGroupItemControls();

  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;

  dlg->GetSocket().SendGroupList();
  this->ProcessPacket();

}

void CGroupManagementDlg::ToggleGroupItemControls()
{
  bool enable = this->m_groupList.GetSelectedCount() != 0;

  this->m_delete.EnableWindow(enable);
  this->m_modify.EnableWindow(enable);

  if (this->GetFocus() == nullptr)
  {
    this->SetFocus();

  }

}

void CGroupManagementDlg::AddGroupToList(unsigned long long pid, ANYVOD_PACKET_GROUP_INFO &item)
{
  ANYVOD_GROUP_INFO *info = new ANYVOD_GROUP_INFO;

  Util::ConvertUTF8ToUnicode((char*)item.name, MAX_GROUP_NAME_SIZE, &info->name);
  info->pid = pid;

  LVITEM lvi;

  lvi.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
  lvi.iItem = this->m_itemCurCount;
  lvi.iSubItem = 0;
  lvi.pszText = (LPWSTR)info->name.c_str();
  lvi.lParam = (LPARAM)info;
  lvi.iImage = ICON_INDEX_GROUP;

  this->m_groupList.InsertItem(&lvi);

  this->m_itemCurCount++;

}

void CGroupManagementDlg::UpdateTotal()
{
  CString count;

  count.Format(L"%d", this->m_itemCount);
  Util::NumberFormatting(&count);
  count += L" 개";
  this->m_total.SetWindowText(count);

}

void CGroupManagementDlg::DeleteGroupItems()
{
  for (int i = 0; i < this->m_groupList.GetItemCount(); i++)
  {
    delete (ANYVOD_GROUP_INFO*)this->m_groupList.GetItemData(i);

  }

  this->m_groupList.DeleteAllItems();

}

bool CGroupManagementDlg::HandlePacket(ANYVOD_PACKET *packet)
{
  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;

  switch (ntohl(packet->s2c_header.header.type))
  {
  case PT_ADMIN_S2C_GROUP_DELETE:
    {
      break;

    }
  case PT_ADMIN_S2C_GROUPLIST:
    {
      if (packet->s2c_header.success)
      {
        this->m_itemCount = ntohl(packet->admin_s2c_grouplist.count);
        this->m_itemCurCount = 0;
        this->DeleteGroupItems();

        this->UpdateTotal();

        return this->m_itemCount > 0;

      }

      break;

    }
  case PT_ADMIN_S2C_GROUP_ITEM:
    {
      if (packet->s2c_header.success)
      {
        this->AddGroupToList(ntohll(packet->admin_s2c_group_item.pid),
          packet->admin_s2c_group_item.detail);

        if (this->m_itemCurCount == this->m_itemCount)
        {
          return false;

        }
        else
        {
          return true;

        }

      }

      break;

    }

  }

  return false;

}

bool CGroupManagementDlg::ProcessPacket()
{
  bool nextProcess = false;
  bool success = false;
  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;
  Socket &socket = dlg->GetSocket();

  do
  {
    if (!Util::HandlePacket(socket, this, &nextProcess, &success))
    {
      this->DeleteGroupItems();
      this->EndDialog(IDCLOSE);

      break;

    }

  } while (nextProcess);

  return success;

}

// CGroupManagementDlg 메시지 처리기입니다.

void CGroupManagementDlg::OnBnClickedButtonGroupManagementRefresh()
{
  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;

  dlg->GetSocket().SendGroupList();
  this->ProcessPacket();

  this->ToggleGroupItemControls();

}

void CGroupManagementDlg::OnBnClickedButtonGroupManagementAdd()
{
  CGroupManagementAddDlg dlg(this);
  INT_PTR result = dlg.DoModal();

  if (result == IDCLOSE)
  {
    this->DeleteGroupItems();
    this->EndDialog(IDCLOSE);

  }
  else if (result == IDOK)
  {
    this->m_itemCount++;

    this->UpdateTotal();

    Util::ListControlScrollToEnd(this->m_groupList);

  }

  this->ToggleGroupItemControls();

}

void CGroupManagementDlg::OnBnClickedButtonGroupManagementModify()
{
  CGroupManagementModifyDlg dlg(this);
  int count = this->m_groupList.GetSelectedCount();

  for (int i = 0; i < count; i++)
  {
    POSITION pos = this->m_groupList.GetFirstSelectedItemPosition();
    int item = this->m_groupList.GetNextSelectedItem(pos);
    INT_PTR result = dlg.DoModal();

    if (result == IDCLOSE)
    {
      this->DeleteGroupItems();
      this->EndDialog(IDCLOSE);

      break;

    }
    else if (result == IDABORT)
    {
      break;

    }

    this->m_groupList.SetItemState(item, 0, LVIS_SELECTED | LVIS_FOCUSED);

  }

  this->ToggleGroupItemControls();

}

void CGroupManagementDlg::OnBnClickedButtonGroupManagementDelete()
{
  POSITION pos = this->m_groupList.GetFirstSelectedItemPosition();

  if (pos != NULL)
  {
    if (AfxMessageBox(L"그룹을 삭제 하시겠습니까?", MB_YESNO) == IDYES)
    {
      CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;

      while (pos)
      {
        int item = this->m_groupList.GetNextSelectedItem(pos);
        ANYVOD_GROUP_INFO *info = (ANYVOD_GROUP_INFO*)this->m_groupList.GetItemData(item);

        dlg->GetSocket().SendGroupDelete(info->pid);

        if (this->ProcessPacket())
        {
          delete info;

          this->m_itemCount--;
          this->m_itemCurCount--;

          this->m_groupList.DeleteItem(item);

          pos = this->m_groupList.GetFirstSelectedItemPosition();

        }
        else
        {
          if (pos != NULL)
          {
            if (AfxMessageBox(L"그룹을 삭제 하는 도중 오류가 발생 했습니다. 계속 하시겠습니까?", MB_YESNO) == IDNO)
            {
              break;

            }

          }

        }

      }

      this->UpdateTotal();

    }

    this->ToggleGroupItemControls();

  }
  else
  {
    AfxMessageBox(L"선택된 그룹이 없습니다.");

  }

}

void CGroupManagementDlg::OnBnClickedButtonGroupManagementClose()
{
  this->DeleteGroupItems();
  this->EndDialog(IDOK);

}

void CGroupManagementDlg::OnNMClickListGroupManagementGrouplist(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

  this->ToggleGroupItemControls();

  *pResult = 0;
}

void CGroupManagementDlg::OnNMDblclkListGroupManagementGrouplist(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

  this->OnBnClickedButtonGroupManagementModify();

  *pResult = 0;
}

void CGroupManagementDlg::OnNMReturnListGroupManagementGrouplist(NMHDR *pNMHDR, LRESULT *pResult)
{
  this->OnBnClickedButtonGroupManagementModify();

  *pResult = 0;
}

void CGroupManagementDlg::OnCancel()
{
  this->DeleteGroupItems();

  CParentDlg::OnCancel();
}

void CGroupManagementDlg::OnLvnItemchangedListGroupManagementGrouplist(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

  this->ToggleGroupItemControls();

  *pResult = 0;
}
