/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "ParentDlg.h"

union ANYVOD_PACKET;

// CGroupManagementAddDlg 대화 상자입니다.

class CGroupManagementAddDlg : public CParentDlg
{
	DECLARE_DYNAMIC(CGroupManagementAddDlg)

public:
	CGroupManagementAddDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CGroupManagementAddDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_GROUP_MANAGEMENT_ADD_DIALOG };

private:
  void ProcessPacket();
  virtual bool HandlePacket(ANYVOD_PACKET *packet);
  virtual void InitControls();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
  CEdit m_name;

  afx_msg void OnBnClickedButtonGroupManagementAddConfirmExist();
  afx_msg void OnBnClickedButtonGroupManagementAddAdd();
  afx_msg void OnBnClickedButtonGroupManagementAddCancel();
  virtual BOOL PreTranslateMessage(MSG* pMsg);
protected:
  virtual void OnOK();
  virtual void OnCancel();
};
