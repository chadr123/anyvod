/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

// AnyVODServerToolDlg.h : 헤더 파일
//

#pragma once

#include "ParentDlg.h"
#include "Socket.h"
#include "Common.h"

struct ANYVOD_FILE_ITEM;
enum FILE_ITEM_TYPE;

// CAnyVODServerToolDlg 대화 상자
class CAnyVODServerToolDlg : public CParentDlg
{
// 생성입니다.
public:
	CAnyVODServerToolDlg(CWnd* pParent = nullptr);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ANYVODSERVERTOOL_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.

public:
  Socket& GetSocket();
  ENV_INFO& GetEnv();

private:
  virtual BOOL PreTranslateMessage(MSG* pMsg);

private:
  Socket m_socket;
  wstring m_curPath;
  vector<uint8_t> m_logBuf;
  wstring m_envBuf;
  ENV_INFO m_envInfo;

private:
  bool ReadEnv();
  bool SplitFileLine(const wstring &fileName, VECTOR_KEY_VALUE *ret);
  void LoginToggleControls(bool login);
  void DeleteFileItems();
  void ProcessPacket();
  int AddToFileList(int startIndex, ANYVOD_FILE_ITEM *items, unsigned int count, FILE_ITEM_TYPE type);
  int AddToFileListSub(ANYVOD_FILE_ITEM &item, int startIndex, FILE_ITEM_TYPE type);
  virtual bool HandlePacket(ANYVOD_PACKET *packet);
  virtual void InitControls();

// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
  CButton m_fileListRefresh;
  CButton m_networkRefresh;
  CButton m_logRefresh;
  CStatic m_networkTotal;
  CStatic m_networkSent;
  CStatic m_networkRecv;
  CStatic m_networkSentTotal;
  CStatic m_networkRecvTotal;
  CListCtrl m_fileList;
  CComboBox m_path;
  CStatic m_logSize;
  CEdit m_log;
  CEdit m_env;

  afx_msg void OnMainMenuFileLogin();
  afx_msg void OnMainMenuFileLogout();
  afx_msg void OnMainMenuFileExit();
  afx_msg void OnMainMenuHelpAbout();
  afx_msg void OnNMDblclkListMainFilelist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnBnClickedButtonMainFilelistRefresh();
  afx_msg void OnCbnSelchangeComboMainPath();
  afx_msg void OnBnClickedButtonMainNetworkRefresh();
  afx_msg void OnNMReturnListMainFilelist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnBnClickedButtonMainLogRefresh();
  afx_msg void OnClose();
  afx_msg void OnMainMenuUserManagement();
  afx_msg void OnMainMenuUserStatus();
  afx_msg void OnMainMenuToolSchedule();
  afx_msg void OnMainMenuToolLog();
  afx_msg void OnMainMenuAuthGroupManagement();
  afx_msg void OnMainMenuAuthUserGroupAuth();
  afx_msg void OnMainMenuAuthFileAuth();
  afx_msg void OnNMRClickListMainFilelist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnMainFilelistMenuFileAuth();
  afx_msg void OnMainFilelistMenuViewDetail();
  afx_msg void OnMainMenuDatabaseStatement();
  afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
  afx_msg void OnMainMenuRebuildMetaData();
  afx_msg void OnMainMenuRebuildMediaData();
};
