/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

// ScheduleModifyDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "AnyVODServerTool.h"
#include "ScheduleModifyDlg.h"
#include "ScheduleDlg.h"
#include "AnyVODServerToolDlg.h"
#include "Socket.h"
#include "Util.h"

// CScheduleModifyDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CScheduleModifyDlg, CDialog)

CScheduleModifyDlg::CScheduleModifyDlg(CWnd* pParent /*=nullptr*/)
	: CParentDlg(CScheduleModifyDlg::IDD, pParent)
{

}

CScheduleModifyDlg::~CScheduleModifyDlg()
{
}

void CScheduleModifyDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_EDIT_SCHEDULE_MODIFY_MINUTE, m_minute);
  DDX_Control(pDX, IDC_EDIT_SCHEDULE_MODIFY_HOUR, m_hour);
  DDX_Control(pDX, IDC_EDIT_SCHEDULE_MODIFY_DAY, m_day);
  DDX_Control(pDX, IDC_EDIT_SCHEDULE_MODIFY_MONTH, m_month);
  DDX_Control(pDX, IDC_EDIT_SCHEDULE_MODIFY_WEEKDAY, m_weekday);
  DDX_Control(pDX, IDC_CHECK_SCHEDULE_MODIFY_MINUTE_ALL, m_minuteAll);
  DDX_Control(pDX, IDC_CHECK_SCHEDULE_MODIFY_HOUR_ALL, m_hourAll);
  DDX_Control(pDX, IDC_CHECK_SCHEDULE_MODIFY_DAY_ALL, m_dayAll);
  DDX_Control(pDX, IDC_CHECK_SCHEDULE_MODIFY_MONTH_ALL, m_monthAll);
  DDX_Control(pDX, IDC_CHECK_SCHEDULE_MODIFY_WEEKDAY_ALL, m_weekdayAll);
  DDX_Control(pDX, IDC_COMBO_SCHEDULE_MODIFY_TYPE, m_types);
  DDX_Control(pDX, IDC_BUTTON_SCHEDULE_MODIFY_STOP_MULTIPLE_MODIFY, m_stopMultiModify);
}


BEGIN_MESSAGE_MAP(CScheduleModifyDlg, CDialog)
  ON_BN_CLICKED(IDC_BUTTON_SCHEDULE_MODIFY_CLOSE, &CScheduleModifyDlg::OnBnClickedButtonScheduleModifyClose)
  ON_BN_CLICKED(IDC_BUTTON_SCHEDULE_MODIFY_MODIFY, &CScheduleModifyDlg::OnBnClickedButtonScheduleModifyModify)
  ON_BN_CLICKED(IDC_CHECK_SCHEDULE_MODIFY_MINUTE_ALL, &CScheduleModifyDlg::OnBnClickedCheckScheduleModifyMinuteAll)
  ON_BN_CLICKED(IDC_CHECK_SCHEDULE_MODIFY_HOUR_ALL, &CScheduleModifyDlg::OnBnClickedCheckScheduleModifyHourAll)
  ON_BN_CLICKED(IDC_CHECK_SCHEDULE_MODIFY_DAY_ALL, &CScheduleModifyDlg::OnBnClickedCheckScheduleModifyDayAll)
  ON_BN_CLICKED(IDC_CHECK_SCHEDULE_MODIFY_MONTH_ALL, &CScheduleModifyDlg::OnBnClickedCheckScheduleModifyMonthAll)
  ON_BN_CLICKED(IDC_CHECK_SCHEDULE_MODIFY_WEEKDAY_ALL, &CScheduleModifyDlg::OnBnClickedCheckScheduleModifyWeekdayAll)
  ON_BN_CLICKED(IDC_BUTTON_SCHEDULE_MODIFY_STOP_MULTIPLE_MODIFY, &CScheduleModifyDlg::OnBnClickedButtonScheduleModifyStopMultipleModify)
END_MESSAGE_MAP()

bool CScheduleModifyDlg::HandlePacket(ANYVOD_PACKET *packet)
{
  ANYVOD_PACKET_TYPE type = (ANYVOD_PACKET_TYPE)ntohl(packet->s2c_header.header.type);

  if (type == PT_ADMIN_S2C_SCHEDULE_MODIFY)
  {
    if (packet->s2c_header.success)
    {
      CScheduleDlg *dlg = (CScheduleDlg*)this->m_pParentWnd;
      CString times[TI_COUNT];
      CString type;

      Util::GetFieldTextAndValidate(this->m_minute, this->m_minuteAll, &times[TI_MINUTE]);
      Util::GetFieldTextAndValidate(this->m_hour, this->m_hourAll, &times[TI_HOUR]);
      Util::GetFieldTextAndValidate(this->m_day, this->m_dayAll, &times[TI_DAY]);
      Util::GetFieldTextAndValidate(this->m_month, this->m_monthAll, &times[TI_MONTH]);
      Util::GetFieldTextAndValidate(this->m_weekday, this->m_weekdayAll, &times[TI_WEEKDAY]);

      this->m_types.GetLBText(this->m_types.GetCurSel(), type);

      CListCtrl &list = dlg->GetScheduleList();
      POSITION pos = list.GetFirstSelectedItemPosition();
      int item = list.GetNextSelectedItem(pos);

      for (int i = TI_MINUTE; i < TI_COUNT; i++)
      {
        list.SetItemText(item, i+1, times[i].GetString());

      }

      list.SetItemText(item, 0, type.GetString());

      this->EndDialog(IDOK);

    }

  }
  else if (type == PT_ADMIN_S2C_SCHEDULE_TYPES)
  {
    if (packet->s2c_header.success)
    {
      Util::AddTypesToComboBox((char(*)[MAX_SCHEDULE_NAME_SIZE])packet->admin_s2c_schedule_types.types, &this->m_types);

    }

  }

  return false;

}

void CScheduleModifyDlg::ProcessPacket()
{
  bool nextProcess = false;
  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd->GetParent();
  Socket &socket = dlg->GetSocket();

  do
  {
    if (!Util::HandlePacket(socket, this, &nextProcess, nullptr))
    {
      this->EndDialog(IDCLOSE);

      break;

    }

  } while (nextProcess);

}

void
CScheduleModifyDlg::InitControls()
{
  CScheduleDlg *dlg = (CScheduleDlg*)this->m_pParentWnd;
  CListCtrl &list = dlg->GetScheduleList();
  POSITION pos = list.GetFirstSelectedItemPosition();

  if (list.GetSelectedCount() > 1)
  {
    this->m_stopMultiModify.ShowWindow(SW_SHOW);

  }
  else
  {
    this->m_stopMultiModify.ShowWindow(SW_HIDE);

  }

  if (pos != NULL)
  {
    int item = list.GetNextSelectedItem(pos);

    this->m_number = list.GetItemData(item);
    this->m_minute.SetLimitText(MAX_SCHEDULE_FIELD_CHAR_SIZE);
    this->m_hour.SetLimitText(MAX_SCHEDULE_FIELD_CHAR_SIZE);
    this->m_day.SetLimitText(MAX_SCHEDULE_FIELD_CHAR_SIZE);
    this->m_month.SetLimitText(MAX_SCHEDULE_FIELD_CHAR_SIZE);
    this->m_weekday.SetLimitText(MAX_SCHEDULE_FIELD_CHAR_SIZE);

    CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd->GetParent();

    dlg->GetSocket().SendScheduleTypes();
    this->ProcessPacket();

    CString text;

    text = list.GetItemText(item, 0);

    int index = this->m_types.FindStringExact(0, text);

    if (index == CB_ERR)
    {
      AfxMessageBox(L"수정할려는 스케줄 타입이 존재하지 않습니다.");

      this->EndDialog(IDCANCEL);

    }
    else
    {
      this->m_types.SetCurSel(index);

      text = list.GetItemText(item, 1);
      text.Trim();

      if (text == L"*")
      {
        this->m_minuteAll.SetCheck(BST_CHECKED);
        this->OnBnClickedCheckScheduleModifyMinuteAll();

      }
      else
      {
        this->m_minute.SetWindowText(text);

      }

      text = list.GetItemText(item, 2);
      text.Trim();

      if (text == L"*")
      {
        this->m_hourAll.SetCheck(BST_CHECKED);
        this->OnBnClickedCheckScheduleModifyHourAll();

      }
      else
      {
        this->m_hour.SetWindowText(text);

      }

      text = list.GetItemText(item, 3);
      text.Trim();

      if (text == L"*")
      {
        this->m_dayAll.SetCheck(BST_CHECKED);
        this->OnBnClickedCheckScheduleModifyDayAll();

      }
      else
      {
        this->m_day.SetWindowText(text);

      }

      text = list.GetItemText(item, 4);
      text.Trim();

      if (text == L"*")
      {
        this->m_monthAll.SetCheck(BST_CHECKED);
        this->OnBnClickedCheckScheduleModifyMonthAll();

      }
      else
      {
        this->m_month.SetWindowText(text);

      }

      text = list.GetItemText(item, 5);
      text.Trim();

      if (text == L"*")
      {
        this->m_weekdayAll.SetCheck(BST_CHECKED);
        this->OnBnClickedCheckScheduleModifyWeekdayAll();

      }
      else
      {
        this->m_weekday.SetWindowText(text);

      }

    }

  }
  else
  {
    AfxMessageBox(L"선택된 스케줄이 없습니다.");

    this->EndDialog(IDCANCEL);

  }

}

// CScheduleModifyDlg 메시지 처리기입니다.

void CScheduleModifyDlg::OnBnClickedButtonScheduleModifyClose()
{
  this->EndDialog(IDCANCEL);

}

void CScheduleModifyDlg::OnBnClickedButtonScheduleModifyModify()
{
  CString minute;
  CString hour;
  CString day;
  CString month;
  CString weekday;
  CString type;

  if (!Util::GetFieldTextAndValidate(this->m_minute, this->m_minuteAll, &minute) ||
    !Util::ValidateField(minute, TI_MINUTE))
    return;

  if (!Util::GetFieldTextAndValidate(this->m_hour, this->m_hourAll, &hour) ||
    !Util::ValidateField(hour, TI_HOUR))
    return;

  if (!Util::GetFieldTextAndValidate(this->m_day, this->m_dayAll, &day) ||
    !Util::ValidateField(day, TI_DAY))
    return;

  if (!Util::GetFieldTextAndValidate(this->m_month, this->m_monthAll, &month) ||
    !Util::ValidateField(month, TI_MONTH))
    return;

  if (!Util::GetFieldTextAndValidate(this->m_weekday, this->m_weekdayAll, &weekday) ||
    !Util::ValidateField(weekday, TI_WEEKDAY))
    return;

  this->m_types.GetLBText(this->m_types.GetCurSel(), type);

  CString total = minute + L" " + hour + L" " + day +
    L" " + month + L" " + weekday + L" " + type;

  string script;
  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd->GetParent();

  Util::ConvertUnicodeToAscii(total.GetString(), &script);

  dlg->GetSocket().SendScheduleModify(this->m_number, script);
  this->ProcessPacket();


}

void CScheduleModifyDlg::OnBnClickedCheckScheduleModifyMinuteAll()
{
  this->m_minute.EnableWindow(this->m_minuteAll.GetCheck() == BST_UNCHECKED);

}

void CScheduleModifyDlg::OnBnClickedCheckScheduleModifyHourAll()
{
  this->m_hour.EnableWindow(this->m_hourAll.GetCheck() == BST_UNCHECKED);

}

void CScheduleModifyDlg::OnBnClickedCheckScheduleModifyDayAll()
{
  this->m_day.EnableWindow(this->m_dayAll.GetCheck() == BST_UNCHECKED);

}

void CScheduleModifyDlg::OnBnClickedCheckScheduleModifyMonthAll()
{
  this->m_month.EnableWindow(this->m_monthAll.GetCheck() == BST_UNCHECKED);

}

void CScheduleModifyDlg::OnBnClickedCheckScheduleModifyWeekdayAll()
{
  this->m_weekday.EnableWindow(this->m_weekdayAll.GetCheck() == BST_UNCHECKED);

}

BOOL CScheduleModifyDlg::PreTranslateMessage(MSG* pMsg)
{
  return CDialog::PreTranslateMessage(pMsg);
}

void CScheduleModifyDlg::OnOK()
{
  this->OnBnClickedButtonScheduleModifyModify();

  //CParentDlg::OnOK();
}

void CScheduleModifyDlg::OnCancel()
{
  this->OnBnClickedButtonScheduleModifyClose();

  CParentDlg::OnCancel();
}

void CScheduleModifyDlg::OnBnClickedButtonScheduleModifyStopMultipleModify()
{
  this->EndDialog(IDABORT);

}
