/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

// AnyVODServerToolDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "AnyVODServerTool.h"
#include "AnyVODServerToolDlg.h"
#include "LoginDlg.h"
#include "ScheduleDlg.h"
#include "LogDlg.h"
#include "UserManagementDlg.h"
#include "ClientStatusDlg.h"
#include "GroupManagementDlg.h"
#include "UserGroupAuthDlg.h"
#include "FileAuthDlg.h"
#include "ViewFileDetailDlg.h"
#include "DatabaseStatementDlg.h"
#include "BuildNumber.h"
#include "Util.h"


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
public:
  CStatic m_version;
  CStatic m_copyRight;
  virtual BOOL OnInitDialog();
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_STATIC_ABOUT_VERSION, m_version);
  DDX_Control(pDX, IDC_STATIC_ABOUT_COPYRIGHT, m_copyRight);
}

BOOL CAboutDlg::OnInitDialog()
{
  CDialog::OnInitDialog();

  CString version;
  CString name;
  CString copyRight;

  AfxGetApp()->GetMainWnd()->GetWindowText(name);

  version.Format(L", Version %d.%d.%d.%d %S", MAJOR, MINOR, PATCH, BUILDNUM, STRFILEVER);
  version += ", Built on " __DATE__ " at " __TIME__;

  name += version;
  copyRight.Format(L"%S", COPYRIGHT);

  this->m_version.SetWindowText(name);
  this->m_copyRight.SetWindowText(copyRight);

  return TRUE;  // return TRUE unless you set the focus to a control
  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CAnyVODServerToolDlg 대화 상자




CAnyVODServerToolDlg::CAnyVODServerToolDlg(CWnd* pParent /*=nullptr*/)
	: CParentDlg(CAnyVODServerToolDlg::IDD, pParent), m_hIcon(AfxGetApp()->LoadIcon(IDR_MAINFRAME))
{

}

void CAnyVODServerToolDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_BUTTON_MAIN_FILELIST_REFRESH, m_fileListRefresh);
  DDX_Control(pDX, IDC_BUTTON_MAIN_NETWORK_REFRESH, m_networkRefresh);
  DDX_Control(pDX, IDC_BUTTON_MAIN_LOG_REFRESH, m_logRefresh);
  DDX_Control(pDX, IDC_STATIC_MAIN_NETWORK_TOTAL, m_networkTotal);
  DDX_Control(pDX, IDC_STATIC_MAIN_NETWORK_SENT, m_networkSent);
  DDX_Control(pDX, IDC_STATIC_MAIN_NETWORK_RECV, m_networkRecv);
  DDX_Control(pDX, IDC_STATIC_MAIN_NETWORK_SENT_TOTAL, m_networkSentTotal);
  DDX_Control(pDX, IDC_STATIC_MAIN_NETWORK_RECV_TOTAL, m_networkRecvTotal);
  DDX_Control(pDX, IDC_LIST_MAIN_FILELIST, m_fileList);
  DDX_Control(pDX, IDC_COMBO_MAIN_PATH, m_path);
  DDX_Control(pDX, IDC_STATIC_MAIN_LOG_SIZE, m_logSize);
  DDX_Control(pDX, IDC_EDIT_MAIN_LOG, m_log);
  DDX_Control(pDX, IDC_EDIT_MAIN_ENV, m_env);
}

BEGIN_MESSAGE_MAP(CAnyVODServerToolDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
  ON_COMMAND(ID_MAIN_MENU_FILE_LOGIN, &CAnyVODServerToolDlg::OnMainMenuFileLogin)
  ON_COMMAND(ID_MAIN_MENU_FILE_LOGOUT, &CAnyVODServerToolDlg::OnMainMenuFileLogout)
  ON_COMMAND(ID_MAIN_MENU_FILE_EXIT, &CAnyVODServerToolDlg::OnMainMenuFileExit)
  ON_COMMAND(ID_MAIN_MENU_HELP_ABOUT, &CAnyVODServerToolDlg::OnMainMenuHelpAbout)
  ON_NOTIFY(NM_DBLCLK, IDC_LIST_MAIN_FILELIST, &CAnyVODServerToolDlg::OnNMDblclkListMainFilelist)
  ON_BN_CLICKED(IDC_BUTTON_MAIN_FILELIST_REFRESH, &CAnyVODServerToolDlg::OnBnClickedButtonMainFilelistRefresh)
  ON_CBN_SELCHANGE(IDC_COMBO_MAIN_PATH, &CAnyVODServerToolDlg::OnCbnSelchangeComboMainPath)
  ON_BN_CLICKED(IDC_BUTTON_MAIN_NETWORK_REFRESH, &CAnyVODServerToolDlg::OnBnClickedButtonMainNetworkRefresh)
  ON_NOTIFY(NM_RETURN, IDC_LIST_MAIN_FILELIST, &CAnyVODServerToolDlg::OnNMReturnListMainFilelist)
  ON_BN_CLICKED(IDC_BUTTON_MAIN_LOG_REFRESH, &CAnyVODServerToolDlg::OnBnClickedButtonMainLogRefresh)
  ON_WM_CLOSE()
  ON_COMMAND(ID_MAIN_MENU_USER_MANAGEMENT, &CAnyVODServerToolDlg::OnMainMenuUserManagement)
  ON_COMMAND(ID_MAIN_MENU_USER_STATUS, &CAnyVODServerToolDlg::OnMainMenuUserStatus)
  ON_COMMAND(ID_MAIN_MENU_TOOL_SCHEDULE, &CAnyVODServerToolDlg::OnMainMenuToolSchedule)
  ON_COMMAND(ID_MAIN_MENU_TOOL_LOG, &CAnyVODServerToolDlg::OnMainMenuToolLog)
  ON_COMMAND(ID_MAIN_MENU_AUTH_GROUP_MANAGEMENT, &CAnyVODServerToolDlg::OnMainMenuAuthGroupManagement)
  ON_COMMAND(ID_MAIN_MENU_AUTH_USER_GROUP_AUTH, &CAnyVODServerToolDlg::OnMainMenuAuthUserGroupAuth)
  ON_COMMAND(ID_MAIN_MENU_AUTH_FILE_AUTH, &CAnyVODServerToolDlg::OnMainMenuAuthFileAuth)
  ON_NOTIFY(NM_RCLICK, IDC_LIST_MAIN_FILELIST, &CAnyVODServerToolDlg::OnNMRClickListMainFilelist)
  ON_COMMAND(ID_MAIN_FILELIST_MENU_FILE_AUTH, &CAnyVODServerToolDlg::OnMainFilelistMenuFileAuth)
  ON_COMMAND(ID_MAIN_FILELIST_MENU_VIEW_DETAIL, &CAnyVODServerToolDlg::OnMainFilelistMenuViewDetail)
  ON_COMMAND(ID_MAIN_MENU_DATABASE_STATEMENT, &CAnyVODServerToolDlg::OnMainMenuDatabaseStatement)
  ON_WM_CTLCOLOR()
  ON_COMMAND(ID_MAIN_MENU_REBUILD_META_DATA, &CAnyVODServerToolDlg::OnMainMenuRebuildMetaData)
  ON_COMMAND(ID_MAIN_MENU_REBUILD_MEDIA_DATA, &CAnyVODServerToolDlg::OnMainMenuRebuildMediaData)
END_MESSAGE_MAP()

bool CAnyVODServerToolDlg::SplitFileLine(const wstring &fileName, VECTOR_KEY_VALUE *ret)
{
  wifstream ifs(fileName.c_str());

  if (ifs.is_open())
  {
    ifs.imbue(locale(ifs.getloc(), new codecvt_utf8<wchar_t, 0x10ffff, consume_header>));

    while (ifs.good())
    {
      wstring line;

      getline(ifs, line);

      Util::TrimString(line, &line);

      if (line.size() == 0)
        continue;

      if (line.find_first_of(COMMENT_PREFIX) == 0)
        continue;

      size_t index = line.find_first_of(ITEM_DELIMITER);

      if (index == wstring::npos)
      {
        continue;

      }
      else
      {
        KEY_VALUE data;

        data.key = line.substr(0, index);
        data.value = line.substr(index + 1);

        ret->push_back(data);

      }

    }

    return true;

  }

  return false;

}

bool CAnyVODServerToolDlg::ReadEnv()
{
  VECTOR_KEY_VALUE ret;

  if (this->SplitFileLine(INIT_FILE_NAME, &ret))
  {
    if (ret.size() == 0)
    {
      return false;

    }
    else
    {
      for (size_t i = 0; i < ret.size(); i++)
      {
        KEY_VALUE &data = ret[i];

        transform(data.key.begin(), data.key.end(), data.key.begin(), towlower);
        transform(data.value.begin(), data.value.end(), data.value.begin(), towlower);

        if (data.key == SERVER_ADDRESS)
        {
          this->m_envInfo.server_address = data.value;

        }
        else if (data.key == SERVER_PORT)
        {
          if (swscanf(data.value.c_str(), L"%hu", &this->m_envInfo.server_port) <= 0)
          {
            this->m_envInfo.server_port = DEFAULT_NETWORK_PORT;

          }

        }

      }

      return true;

    }

  }
  else
  {
    return false;

  }

}

ENV_INFO& CAnyVODServerToolDlg::GetEnv()
{
  return this->m_envInfo;

}

void CAnyVODServerToolDlg::InitControls()
{
  this->m_log.SetWindowText(L"");
  this->m_logSize.SetWindowText(L"");
  this->m_networkTotal.SetWindowText(L"0 개");
  this->m_networkSent.SetWindowText(L"0 KB/s");
  this->m_networkRecv.SetWindowText(L"0 KB/s");
  this->m_networkSentTotal.SetWindowText(L"0 KB");
  this->m_networkRecvTotal.SetWindowText(L"0 KB");
  this->DeleteFileItems();
  this->m_path.ResetContent();
  this->m_env.SetWindowText(L"");

  this->m_log.SendMessage(EM_SETLANGOPTIONS, 0,
    (LPARAM)(this->m_log.SendMessage(EM_GETLANGOPTIONS, 0, 0) & ~IMF_AUTOFONT));
  this->m_env.SendMessage(EM_SETLANGOPTIONS, 0,
    (LPARAM)(this->m_env.SendMessage(EM_GETLANGOPTIONS, 0, 0) & ~IMF_AUTOFONT));

}

void CAnyVODServerToolDlg::LoginToggleControls(bool login)
{
  UINT loginItem = MF_BYCOMMAND;
  UINT logoutItem = MF_BYCOMMAND;

  if (login)
  {
    loginItem |= MF_DISABLED;
    logoutItem |= MF_ENABLED;

  }
  else
  {
    loginItem |= MF_ENABLED;
    logoutItem |= MF_DISABLED;

    this->InitControls();

  }

  CMenu *menu = this->GetMenu()->GetSubMenu(MENU_INDEX_FILE);

  menu->EnableMenuItem(ID_MAIN_MENU_FILE_LOGIN, loginItem);
  menu->EnableMenuItem(ID_MAIN_MENU_FILE_LOGOUT, logoutItem);

  menu = this->GetMenu()->GetSubMenu(MENU_INDEX_USER);

  menu->EnableMenuItem(ID_MAIN_MENU_USER_MANAGEMENT, logoutItem);
  menu->EnableMenuItem(ID_MAIN_MENU_USER_STATUS, logoutItem);

  menu = this->GetMenu()->GetSubMenu(MENU_INDEX_AUTH);

  menu->EnableMenuItem(ID_MAIN_MENU_AUTH_GROUP_MANAGEMENT, logoutItem);
  menu->EnableMenuItem(ID_MAIN_MENU_AUTH_USER_GROUP_AUTH, logoutItem);
  menu->EnableMenuItem(ID_MAIN_MENU_AUTH_FILE_AUTH, logoutItem);

  menu = this->GetMenu()->GetSubMenu(MENU_INDEX_TOOL);

  menu->EnableMenuItem(ID_MAIN_MENU_TOOL_SCHEDULE, logoutItem);
  menu->EnableMenuItem(ID_MAIN_MENU_TOOL_LOG, logoutItem);
  menu->EnableMenuItem(ID_MAIN_MENU_REBUILD_META_DATA, logoutItem);
  menu->EnableMenuItem(ID_MAIN_MENU_REBUILD_MEDIA_DATA, logoutItem);

  menu = this->GetMenu()->GetSubMenu(MENU_INDEX_DATABASE);

  menu->EnableMenuItem(ID_MAIN_MENU_DATABASE_STATEMENT, logoutItem);

  this->m_fileList.EnableWindow(login);
  this->m_fileListRefresh.EnableWindow(login);
  this->m_networkRefresh.EnableWindow(login);
  this->m_logRefresh.EnableWindow(login);
  this->m_log.EnableWindow(login);
  this->m_logSize.EnableWindow(login);
  this->m_networkTotal.EnableWindow(login);
  this->m_networkSent.EnableWindow(login);
  this->m_networkRecv.EnableWindow(login);
  this->m_networkSentTotal.EnableWindow(login);
  this->m_networkRecvTotal.EnableWindow(login);
  this->m_path.EnableWindow(login);
  this->m_env.EnableWindow(login);

}

Socket& CAnyVODServerToolDlg::GetSocket()
{
  return this->m_socket;

}

void CAnyVODServerToolDlg::DeleteFileItems()
{
  for (int i = 0; i < this->m_fileList.GetItemCount(); i++)
  {
    delete (ANYVOD_FILE_ITEM*)this->m_fileList.GetItemData(i);

  }

  this->m_fileList.DeleteAllItems();

}


int CAnyVODServerToolDlg::AddToFileList(int startIndex, ANYVOD_FILE_ITEM *items, unsigned int count, FILE_ITEM_TYPE type)
{
  for (unsigned int i = 0; i < count; i++)
  {
    ANYVOD_FILE_ITEM &item = items[i];

    if (item.type == type)
    {
      startIndex = this->AddToFileListSub(item, startIndex, type);

    }

  }

  return startIndex;

}

int CAnyVODServerToolDlg::AddToFileListSub(ANYVOD_FILE_ITEM &item, int startIndex, FILE_ITEM_TYPE type)
{
  ANYVOD_FILE_ITEM *toStore = new ANYVOD_FILE_ITEM;
  LVITEM lvi;

  lvi.mask = LVIF_TEXT | LVIF_PARAM | LVIF_IMAGE;
  lvi.iItem = startIndex;
  lvi.iSubItem = 0;
  lvi.pszText = (LPWSTR)item.fileName.c_str();
  lvi.lParam = (LPARAM)toStore;

  *toStore = item;

  SHFILEINFO sfi;

  if (type == FT_FILE)
  {
    wstring ext = item.fileName.substr(item.fileName.find_last_of(EXTENTION_SEPERATOR));

    transform(ext.begin(), ext.end(), ext.begin(), towlower);
    SHGetFileInfo(ext.c_str(), 0, &sfi, sizeof(SHFILEINFO), SHGFI_USEFILEATTRIBUTES | SHGFI_ICON | SHGFI_SMALLICON);

  }
  else
  {
    SHGetFileInfo(L"folder", FILE_ATTRIBUTE_DIRECTORY, &sfi, sizeof(SHFILEINFO), SHGFI_USEFILEATTRIBUTES | SHGFI_ICON | SHGFI_SMALLICON);

  }

  lvi.iImage = sfi.iIcon;

  DestroyIcon(sfi.hIcon);

  this->m_fileList.InsertItem(&lvi);

  CString fileSize;

  lvi.mask = LVIF_TEXT;
  lvi.iSubItem = 1;

  if (type == FT_FILE)
  {
    Util::FileSizeToString(item.totalSize, &fileSize);

    lvi.pszText = (LPWSTR)fileSize.GetString();

  }
  else
  {
    lvi.pszText = L"<DIR>";

  }

  this->m_fileList.SetItem(&lvi);

  startIndex++;

  return startIndex;

}

bool CAnyVODServerToolDlg::HandlePacket(ANYVOD_PACKET *packet)
{
  switch (ntohl(packet->s2c_header.header.type))
  {
  case PT_S2C_LOGOUT:
    {
      this->LoginToggleControls(false);

      this->m_socket.Disconnect();

      break;

    }
  case PT_S2C_FILELIST:
    {
      if (packet->s2c_header.success)
      {
        wstring path;
        int strIndex;

        Util::ConvertUTF8ToUnicode((char*)packet->s2c_filelist.path, MAX_PATH_SIZE, &path);
        strIndex = this->m_path.FindStringExact(0, path.c_str());

        if (strIndex == CB_ERR)
        {
          strIndex = this->m_path.AddString(path.c_str());

        }

        this->m_path.SetCurSel(strIndex);

        this->DeleteFileItems();

        int index = 0;

        if (path.size() != 1)
        {
          ANYVOD_FILE_ITEM item;

          item.fileName = PARENT_DIRECTORY;
          item.type = FT_DIR;

          index = this->AddToFileListSub(item, index, FT_DIR);

        }

        unsigned int count = ntohl(packet->s2c_filelist.count);
        ANYVOD_FILE_ITEM *items = new ANYVOD_FILE_ITEM[count];

        for (unsigned int i = 0; i < count; i++)
        {
          ANYVOD_FILE_ITEM &item = items[i];
          ANYVOD_PACKET_FILE_ITEM &src = packet->s2c_filelist.Items[i];

          item.bitRate = ntohl(src.bitRate);
          item.permission = src.permission != 0;
          item.totalSize = ntohll(src.totalSize);
          item.totalTime = ntohl(src.totalTime);
          item.type = (FILE_ITEM_TYPE)ntohl(src.type);
          Util::ConvertUTF8ToUnicode((char*)src.fileName, MAX_FILENAME_SIZE, &item.fileName);

        }

        index = this->AddToFileList(index, items, count, FT_DIR);
        this->AddToFileList(index, items, count, FT_FILE);

        delete[] items;

        this->m_curPath = path;

        this->m_fileList.SetColumnWidth(0, 476);

      }

      break;

    }
  case PT_ADMIN_S2C_NETWORK_STATISTICS:
    {
      if (packet->s2c_header.success)
      {
        CString totalClients;
        CString totalRecv;
        CString totalSent;
        CString recvPerSec;
        CString sentPerSec;

        totalClients.Format(L"%u", ntohl(packet->admin_s2c_network_statistics.totalClients));
        Util::NumberFormatting(&totalClients);
        totalClients += L" 개";

        totalRecv.Format(L"%llu", ntohll(packet->admin_s2c_network_statistics.totalRecv) / KILO);
        Util::NumberFormatting(&totalRecv);
        totalRecv += L" KB";

        totalSent.Format(L"%llu", ntohll(packet->admin_s2c_network_statistics.totalSent) / KILO);
        Util::NumberFormatting(&totalSent);
        totalSent += L" KB";

        recvPerSec.Format(L"%u", ntohl(packet->admin_s2c_network_statistics.recvPerSec) / KILO);
        Util::NumberFormatting(&recvPerSec);
        recvPerSec += L" KB/s";

        sentPerSec.Format(L"%u", ntohl(packet->admin_s2c_network_statistics.sentPerSec) / KILO);
        Util::NumberFormatting(&sentPerSec);
        sentPerSec += L" KB/s";

        this->m_networkTotal.SetWindowText(totalClients);
        this->m_networkRecvTotal.SetWindowText(totalRecv);
        this->m_networkSentTotal.SetWindowText(totalSent);
        this->m_networkRecv.SetWindowText(recvPerSec);
        this->m_networkSent.SetWindowText(sentPerSec);

      }

      break;

    }
  case PT_ADMIN_S2C_FILE_START:
    {
      if (packet->s2c_header.success)
      {
        FILE_START_TYPE type = (FILE_START_TYPE)ntohl(packet->admin_s2c_file_start.type);

        if (type == FST_LOG)
        {
          CString totalSize;

          totalSize.Format(L"%llu", ntohll(packet->admin_s2c_file_start.totalSize));
          Util::NumberFormatting(&totalSize);
          totalSize += L" Bytes";

          this->m_logSize.SetWindowText(totalSize);
          this->m_log.SetWindowText(L"");
          this->m_logBuf.clear();

        }
        else if (type == FST_ENV)
        {
          this->m_env.SetWindowText(L"");
          this->m_envBuf.clear();

        }
        else
        {
        }

        return true;

      }

      break;

    }
  case PT_ADMIN_S2C_FILE_STREAM:
    {
      if (packet->s2c_header.success)
      {
        FILE_START_TYPE type = (FILE_START_TYPE)ntohl(packet->admin_s2c_file_stream.type);
        uint32_t size = ntohl(packet->admin_s2c_file_stream.size);

        if (type == FST_LOG)
        {
          this->m_logBuf.insert(this->m_logBuf.end(),
            &packet->admin_s2c_file_stream.data[0], &packet->admin_s2c_file_stream.data[size]);

        }
        else if (type == FST_ENV)
        {
          wstring tmp;

          Util::ConvertUTF8ToUnicode((char*)packet->admin_s2c_file_stream.data, size, &tmp);

          this->m_envBuf.append(tmp);

        }
        else
        {
        }

        return true;

      }

      break;

    }
  case PT_ADMIN_S2C_FILE_END:
    {
      FILE_START_TYPE type = (FILE_START_TYPE)ntohl(packet->admin_s2c_file_end.type);

      if (type == FST_LOG)
      {
        wstring tmp;

        Util::ConvertUTF8ToUnicode((const char*)&this->m_logBuf[0], this->m_logBuf.size(), &tmp);
        Util::AppendContentsToEdit(tmp, this->m_log);

      }
      else if (type == FST_ENV)
      {
        Util::AppendContentsToEdit(this->m_envBuf, this->m_env);

      }
      else
      {
      }

      break;

    }
  case PT_ADMIN_S2C_REBUILD_META_DATA:
  case PT_ADMIN_S2C_REBUILD_MOVIE_DATA:
    {
      break;

    }
  case PT_ADMIN_S2C_LASTLOG:
  case PT_ADMIN_S2C_ENV:
    {
      if (packet->s2c_header.success)
      {
        return true;

      }

      break;

    }

  }

  return false;

}

void CAnyVODServerToolDlg::ProcessPacket()
{
  bool nextProcess = false;

  do
  {
    if (!Util::HandlePacket(this->m_socket, this, &nextProcess, nullptr))
    {
      this->LoginToggleControls(false);

      break;

    }

  } while (nextProcess);

}

// CAnyVODServerToolDlg 메시지 처리기

BOOL CAnyVODServerToolDlg::OnInitDialog()
{
	CParentDlg::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	this->LoginToggleControls(false);

  if (!this->m_socket.Create())
  {
    AfxMessageBox(L"소켓을 생성할 수 없습니다.");

    this->EndDialog(0);

  }

  if (!this->ReadEnv())
  {
    CString msg;

    msg.Format(L"환경 파일을 읽을 수 없습니다. : %s", INIT_FILE_NAME);

    AfxMessageBox(msg);

    this->EndDialog(0);

  }

  this->m_fileList.InsertColumn(0, L"파일명", LVCFMT_LEFT, 493);
  this->m_fileList.InsertColumn(1, L"크기", LVCFMT_CENTER, 60);
  this->m_fileList.SetExtendedStyle(LVS_EX_GRIDLINES| LVS_EX_FULLROWSELECT);
  this->m_fileList.SetImageList(&Util::GetShellImageList(), LVSIL_SMALL);

  this->m_log.SetLimitText(-1);
  this->m_env.SetLimitText(-1);

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CAnyVODServerToolDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CAnyVODServerToolDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CAnyVODServerToolDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

BOOL CAnyVODServerToolDlg::PreTranslateMessage(MSG* pMsg)
{
  if(pMsg->message == WM_KEYDOWN &&
    (pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE))
  {
    return FALSE;

  }

  return CParentDlg::PreTranslateMessage(pMsg);

}

HBRUSH CAnyVODServerToolDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
  HBRUSH hbr = CParentDlg::OnCtlColor(pDC, pWnd, nCtlColor);

  if (nCtlColor == CTLCOLOR_STATIC)
  {
    UINT id = pWnd->GetDlgCtrlID();

    if (id == IDC_EDIT_MAIN_LOG || id == IDC_EDIT_MAIN_ENV)
    {
      if (pWnd->GetStyle() & ES_READONLY && pWnd->IsWindowEnabled())
      {
        pDC->SetBkColor(RGB(255, 255, 255));
        hbr = (HBRUSH)::GetStockObject(WHITE_BRUSH);

      }

    }

  }

  return hbr;
}

void CAnyVODServerToolDlg::OnMainMenuFileLogin()
{
  CLoginDlg login(this);
  INT_PTR result = login.DoModal();

  if (result == IDOK)
  {
    this->LoginToggleControls(true);

    wstring path;

    path.append(1, DIRECTORY_SEPERATOR);

    this->m_socket.SendFileList(path);
    this->ProcessPacket();

    this->m_socket.SendNetworkStatistics();
    this->ProcessPacket();

    this->m_socket.SendLastLog();
    this->ProcessPacket();

    this->m_socket.SendEnv();
    this->ProcessPacket();

  }
  else if (result == IDCLOSE)
  {
    this->LoginToggleControls(false);

  }

}

void CAnyVODServerToolDlg::OnMainMenuFileLogout()
{
  this->m_socket.SendLogout();
  this->ProcessPacket();

}

void CAnyVODServerToolDlg::OnMainMenuFileExit()
{
  this->OnClose();

}

void CAnyVODServerToolDlg::OnMainMenuHelpAbout()
{
  CAboutDlg about;

  about.DoModal();

}

void CAnyVODServerToolDlg::OnNMDblclkListMainFilelist(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

  if (pNMItemActivate->iItem == -1)
    return;

  ANYVOD_FILE_ITEM *item = (ANYVOD_FILE_ITEM*)this->m_fileList.GetItemData(pNMItemActivate->iItem);

  if (item->type == FT_DIR)
  {
    CString text = this->m_fileList.GetItemText(pNMItemActivate->iItem, 0);
    wstring path = this->m_curPath;

    if (wcscmp(text.GetString(), PARENT_DIRECTORY.c_str()) == 0)
    {
      path = path.substr(0, path.find_last_of(DIRECTORY_SEPERATOR));

      if (path.size() == 0)
      {
        path.append(1, DIRECTORY_SEPERATOR);

      }

    }
    else
    {
      wstring dir;

      if (path[path.size()-1] != DIRECTORY_SEPERATOR)
      {
        path.append(1, DIRECTORY_SEPERATOR);

      }

      path.append(text.GetString());

    }

    this->m_socket.SendFileList(path);
    this->ProcessPacket();

  }

  *pResult = 0;
}

void CAnyVODServerToolDlg::OnBnClickedButtonMainFilelistRefresh()
{
  this->m_socket.SendFileList(this->m_curPath);
  this->ProcessPacket();

}

void CAnyVODServerToolDlg::OnCbnSelchangeComboMainPath()
{
  CString text;

  this->m_path.GetLBText(this->m_path.GetCurSel(), text);
  this->m_socket.SendFileList(text.GetString());
  this->ProcessPacket();
}

void CAnyVODServerToolDlg::OnBnClickedButtonMainNetworkRefresh()
{
  this->m_socket.SendNetworkStatistics();
  this->ProcessPacket();
}

void CAnyVODServerToolDlg::OnNMReturnListMainFilelist(NMHDR *pNMHDR, LRESULT *pResult)
{
  POSITION pos = this->m_fileList.GetFirstSelectedItemPosition();

  if (pos != NULL)
  {
    int item = this->m_fileList.GetNextSelectedItem(pos);
    NMITEMACTIVATE active;

    active.iItem = item;

    this->OnNMDblclkListMainFilelist((NMHDR*)&active, pResult);

  }

  *pResult = 0;
}

void CAnyVODServerToolDlg::OnBnClickedButtonMainLogRefresh()
{
  this->m_socket.SendLastLog();

  this->ProcessPacket();

}

void CAnyVODServerToolDlg::OnClose()
{
  this->m_socket.Disconnect();
  this->DeleteFileItems();

  this->EndDialog(0);

  CParentDlg::OnClose();
}

void CAnyVODServerToolDlg::OnMainMenuUserManagement()
{
  CUserManagementDlg dlg(this);

  if (dlg.DoModal() == IDCLOSE)
  {
    this->LoginToggleControls(false);

  }

}

void CAnyVODServerToolDlg::OnMainMenuUserStatus()
{
  CClientStatusDlg dlg(this);

  if (dlg.DoModal() == IDCLOSE)
  {
    this->LoginToggleControls(false);

  }

}

void CAnyVODServerToolDlg::OnMainMenuToolSchedule()
{
  CScheduleDlg dlg(this);

  if (dlg.DoModal() == IDCLOSE)
  {
     this->LoginToggleControls(false);

  }

}

void CAnyVODServerToolDlg::OnMainMenuToolLog()
{
  CLogDlg dlg(this);

  if (dlg.DoModal() == IDCLOSE)
  {
    this->LoginToggleControls(false);

  }

}

void CAnyVODServerToolDlg::OnMainMenuAuthGroupManagement()
{
  CGroupManagementDlg dlg(this);

  if (dlg.DoModal() == IDCLOSE)
  {
    this->LoginToggleControls(false);

  }

}

void CAnyVODServerToolDlg::OnMainMenuAuthUserGroupAuth()
{
  CUserGroupAuthDlg dlg(this);

  if (dlg.DoModal() == IDCLOSE)
  {
    this->LoginToggleControls(false);

  }

}

void CAnyVODServerToolDlg::OnMainMenuAuthFileAuth()
{
  CFileAuthDlg dlg(this);

  if (dlg.DoModal() == IDCLOSE)
  {
    this->LoginToggleControls(false);

  }

}

void CAnyVODServerToolDlg::OnNMRClickListMainFilelist(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

  if (pNMItemActivate->iItem == -1)
    return;

  ANYVOD_FILE_ITEM *info = (ANYVOD_FILE_ITEM*)this->m_fileList.GetItemData(pNMItemActivate->iItem);
  CMenu menu;
  CMenu *subMenu = nullptr;
  CPoint pt = pNMItemActivate->ptAction;
  int disable = MF_BYCOMMAND | MF_DISABLED;

  this->m_fileList.ClientToScreen(&pt);

  menu.LoadMenu(IDR_MAIN_FILELIST_MENU);
  subMenu = menu.GetSubMenu(0);

  if (info->type == FT_DIR || this->m_fileList.GetSelectedCount() > 1)
  {
    subMenu->EnableMenuItem(ID_MAIN_FILELIST_MENU_VIEW_DETAIL, disable);

    if (info->fileName == PARENT_DIRECTORY)
    {
      subMenu->EnableMenuItem(ID_MAIN_FILELIST_MENU_FILE_AUTH, disable);

    }

  }

  subMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, pt.x, pt.y, this);

  *pResult = 0;
}

void CAnyVODServerToolDlg::OnMainFilelistMenuFileAuth()
{
  POSITION pos = this->m_fileList.GetFirstSelectedItemPosition();

  if (pos != NULL)
  {
    vector_wstring fileNames;

    while (pos)
    {
      int item = this->m_fileList.GetNextSelectedItem(pos);
      ANYVOD_FILE_ITEM *info = (ANYVOD_FILE_ITEM*)this->m_fileList.GetItemData(item);

      fileNames.push_back(info->fileName);

    }

    CFileAuthDlg dlg(this, this->m_curPath, fileNames);

    if (dlg.DoModal() == IDCLOSE)
    {
      this->LoginToggleControls(false);

    }

  }
  else
  {
    AfxMessageBox(L"파일을 선택 해 주세요.");

  }

}

void CAnyVODServerToolDlg::OnMainFilelistMenuViewDetail()
{
  POSITION pos = this->m_fileList.GetFirstSelectedItemPosition();

  if (pos != NULL)
  {
    int item = this->m_fileList.GetNextSelectedItem(pos);
    ANYVOD_FILE_ITEM *info = (ANYVOD_FILE_ITEM*)this->m_fileList.GetItemData(item);

    CViewFileDetailDlg dlg(*info, this->m_curPath, this);

    dlg.DoModal();

  }
  else
  {
    AfxMessageBox(L"파일을 선택 해 주세요.");

  }

}

void CAnyVODServerToolDlg::OnMainMenuDatabaseStatement()
{
  CDatabaseStatementDlg dlg(this);

  if (dlg.DoModal() == IDCLOSE)
  {
    this->LoginToggleControls(false);

  }

}

void CAnyVODServerToolDlg::OnMainMenuRebuildMetaData()
{
  if (AfxMessageBox(L"메타데이터를 재 구성하기 위해서는 많은 시간이 걸립니다. 진행 하시겠습니까?", MB_YESNO) == IDYES)
  {
    this->m_socket.SendRebuildMetaData();

    this->ProcessPacket();

    AfxMessageBox(L"완료 되었습니다.");

  }

}

void CAnyVODServerToolDlg::OnMainMenuRebuildMediaData()
{
  if (AfxMessageBox(L"미디어 데이터를 재 구성하기 위해서는 많은 시간이 걸립니다. 진행 하시겠습니까?", MB_YESNO) == IDYES)
  {
    this->m_socket.SendRebuildMediaData();

    this->ProcessPacket();

    AfxMessageBox(L"완료 되었습니다.");

  }

}
