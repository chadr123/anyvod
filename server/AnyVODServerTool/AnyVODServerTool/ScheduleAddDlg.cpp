/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

// ScheduleAddDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "AnyVODServerTool.h"
#include "ScheduleDlg.h"
#include "ScheduleAddDlg.h"
#include "AnyVODServerToolDlg.h"
#include "Socket.h"
#include "Util.h"

// CScheduleAddDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CScheduleAddDlg, CDialog)

CScheduleAddDlg::CScheduleAddDlg(CWnd* pParent /*=nullptr*/)
	: CParentDlg(CScheduleAddDlg::IDD, pParent)
{

}

CScheduleAddDlg::~CScheduleAddDlg()
{
}

void CScheduleAddDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_EDIT_SCHEDULE_ADD_MINUTE, m_minute);
  DDX_Control(pDX, IDC_EDIT_SCHEDULE_ADD_HOUR, m_hour);
  DDX_Control(pDX, IDC_EDIT_SCHEDULE_ADD_DAY, m_day);
  DDX_Control(pDX, IDC_EDIT_SCHEDULE_ADD_MONTH, m_month);
  DDX_Control(pDX, IDC_EDIT_SCHEDULE_ADD_WEEKDAY, m_weekday);
  DDX_Control(pDX, IDC_CHECK_SCHEDULE_ADD_MINUTE_ALL, m_minuteAll);
  DDX_Control(pDX, IDC_CHECK_SCHEDULE_ADD_HOUR_ALL, m_hourAll);
  DDX_Control(pDX, IDC_CHECK_SCHEDULE_ADD_DAY_ALL, m_dayAll);
  DDX_Control(pDX, IDC_CHECK_SCHEDULE_ADD_MONTH_ALL, m_monthAll);
  DDX_Control(pDX, IDC_CHECK_SCHEDULE_ADD_WEEKDAY_ALL, m_weekdayAll);
  DDX_Control(pDX, IDC_COMBO_SCHEDULE_ADD_TYPE, m_types);
}


BEGIN_MESSAGE_MAP(CScheduleAddDlg, CDialog)
  ON_BN_CLICKED(IDC_BUTTON_SCHEDULE_ADD_CLOSE, &CScheduleAddDlg::OnBnClickedButtonScheduleAddClose)
  ON_BN_CLICKED(IDC_BUTTON_SCHEDULE_ADD_ADD, &CScheduleAddDlg::OnBnClickedButtonScheduleAddAdd)
  ON_BN_CLICKED(IDC_CHECK_SCHEDULE_ADD_MINUTE_ALL, &CScheduleAddDlg::OnBnClickedCheckScheduleAddMinuteAll)
  ON_BN_CLICKED(IDC_CHECK_SCHEDULE_ADD_HOUR_ALL, &CScheduleAddDlg::OnBnClickedCheckScheduleAddHourAll)
  ON_BN_CLICKED(IDC_CHECK_SCHEDULE_ADD_DAY_ALL, &CScheduleAddDlg::OnBnClickedCheckScheduleAddDayAll)
  ON_BN_CLICKED(IDC_CHECK_SCHEDULE_ADD_MONTH_ALL, &CScheduleAddDlg::OnBnClickedCheckScheduleAddMonthAll)
  ON_BN_CLICKED(IDC_CHECK_SCHEDULE_ADD_WEEKDAY_ALL, &CScheduleAddDlg::OnBnClickedCheckScheduleAddWeekdayAll)
END_MESSAGE_MAP()

void CScheduleAddDlg::InitControls()
{
  this->m_minute.SetLimitText(MAX_SCHEDULE_FIELD_CHAR_SIZE);
  this->m_hour.SetLimitText(MAX_SCHEDULE_FIELD_CHAR_SIZE);
  this->m_day.SetLimitText(MAX_SCHEDULE_FIELD_CHAR_SIZE);
  this->m_month.SetLimitText(MAX_SCHEDULE_FIELD_CHAR_SIZE);
  this->m_weekday.SetLimitText(MAX_SCHEDULE_FIELD_CHAR_SIZE);

  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd->GetParent();

  dlg->GetSocket().SendScheduleTypes();
  this->ProcessPacket();

  this->m_types.SetCurSel(0);

}

bool CScheduleAddDlg::HandlePacket(ANYVOD_PACKET *packet)
{
  ANYVOD_PACKET_TYPE type =  (ANYVOD_PACKET_TYPE)ntohl(packet->s2c_header.header.type);

  if (type == PT_ADMIN_S2C_SCHEDULE_ADD)
  {
    if (packet->s2c_header.success)
    {
      CScheduleDlg *dlg = (CScheduleDlg*)this->m_pParentWnd;
      CString times[TI_COUNT];
      CString type;
      char elements[TI_COUNT][MAX_SCHEDULE_FIELD_SIZE];

      Util::GetFieldTextAndValidate(this->m_minute, this->m_minuteAll, &times[TI_MINUTE]);
      Util::GetFieldTextAndValidate(this->m_hour, this->m_hourAll, &times[TI_HOUR]);
      Util::GetFieldTextAndValidate(this->m_day, this->m_dayAll, &times[TI_DAY]);
      Util::GetFieldTextAndValidate(this->m_month, this->m_monthAll, &times[TI_MONTH]);
      Util::GetFieldTextAndValidate(this->m_weekday, this->m_weekdayAll, &times[TI_WEEKDAY]);

      this->m_types.GetLBText(this->m_types.GetCurSel(), type);

      for (int i = 0; i < TI_COUNT; i++)
      {
        string tmp;

        Util::ConvertUnicodeToAscii(times[i].GetString(), &tmp);
        strncpy_s(elements[i], MAX_SCHEDULE_FIELD_SIZE, tmp.c_str(), MAX_SCHEDULE_FIELD_CHAR_SIZE);

      }

      ANYVOD_TIME time;
      string tmp;

      memset(&time, 0x00, sizeof(time));
      Util::ConvertUnicodeToAscii(type.GetString(), &tmp);

      dlg->AddScheduleToList(ntohl(packet->admin_s2c_schedule_add.index), (char*)tmp.c_str(), elements, false, time, time);

      this->EndDialog(IDOK);

    }

  }
  else if (type == PT_ADMIN_S2C_SCHEDULE_TYPES)
  {
    if (packet->s2c_header.success)
    {
      Util::AddTypesToComboBox((char(*)[MAX_SCHEDULE_NAME_SIZE])packet->admin_s2c_schedule_types.types, &this->m_types);

    }

  }

  return false;

}

void CScheduleAddDlg::ProcessPacket()
{
  bool nextProcess = false;
  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd->GetParent();
  Socket &socket = dlg->GetSocket();

  do
  {
    if (!Util::HandlePacket(socket, this, &nextProcess, nullptr))
    {
      this->EndDialog(IDCLOSE);

      break;

    }

  } while (nextProcess);

}

// CScheduleAddDlg 메시지 처리기입니다.

void CScheduleAddDlg::OnBnClickedButtonScheduleAddClose()
{
  this->EndDialog(IDCANCEL);

}

void CScheduleAddDlg::OnBnClickedButtonScheduleAddAdd()
{
  CString minute;
  CString hour;
  CString day;
  CString month;
  CString weekday;
  CString type;

  if (!Util::GetFieldTextAndValidate(this->m_minute, this->m_minuteAll, &minute) ||
    !Util::ValidateField(minute, TI_MINUTE))
    return;

  if (!Util::GetFieldTextAndValidate(this->m_hour, this->m_hourAll, &hour) ||
    !Util::ValidateField(hour, TI_HOUR))
    return;

  if (!Util::GetFieldTextAndValidate(this->m_day, this->m_dayAll, &day) ||
    !Util::ValidateField(day, TI_DAY))
    return;

  if (!Util::GetFieldTextAndValidate(this->m_month, this->m_monthAll, &month) ||
    !Util::ValidateField(month, TI_MONTH))
    return;

  if (!Util::GetFieldTextAndValidate(this->m_weekday, this->m_weekdayAll, &weekday) ||
    !Util::ValidateField(weekday, TI_WEEKDAY))
    return;

  this->m_types.GetLBText(this->m_types.GetCurSel(), type);

  CString total = minute + L" " + hour + L" " + day +
    L" " + month + L" " + weekday + L" " + type;
  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd->GetParent();
  string script;

  Util::ConvertUnicodeToAscii(total.GetString(), &script);

  dlg->GetSocket().SendScheduleAdd(script);
  this->ProcessPacket();


}

void CScheduleAddDlg::OnBnClickedCheckScheduleAddMinuteAll()
{
  this->m_minute.EnableWindow(this->m_minuteAll.GetCheck() == BST_UNCHECKED);

}

void CScheduleAddDlg::OnBnClickedCheckScheduleAddHourAll()
{
  this->m_hour.EnableWindow(this->m_hourAll.GetCheck() == BST_UNCHECKED);

}

void CScheduleAddDlg::OnBnClickedCheckScheduleAddDayAll()
{
  this->m_day.EnableWindow(this->m_dayAll.GetCheck() == BST_UNCHECKED);

}

void CScheduleAddDlg::OnBnClickedCheckScheduleAddMonthAll()
{
  this->m_month.EnableWindow(this->m_monthAll.GetCheck() == BST_UNCHECKED);

}

void CScheduleAddDlg::OnBnClickedCheckScheduleAddWeekdayAll()
{
  this->m_weekday.EnableWindow(this->m_weekdayAll.GetCheck() == BST_UNCHECKED);

}

BOOL CScheduleAddDlg::PreTranslateMessage(MSG* pMsg)
{
  return CDialog::PreTranslateMessage(pMsg);
}

void CScheduleAddDlg::OnOK()
{
  this->OnBnClickedButtonScheduleAddAdd();

  //CParentDlg::OnOK();
}

void CScheduleAddDlg::OnCancel()
{
  this->OnBnClickedButtonScheduleAddClose();

  CParentDlg::OnCancel();
}
