/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "ParentDlg.h"

union ANYVOD_PACKET;

// CLogViewDlg 대화 상자입니다.

class CLogViewDlg : public CParentDlg
{
	DECLARE_DYNAMIC(CLogViewDlg)

public:
	CLogViewDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CLogViewDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_LOG_VIEW_DIALOG };

private:
  vector<uint8_t> m_logBuf;

private:
  void ProcessPacket();
  virtual bool HandlePacket(ANYVOD_PACKET *packet);
  virtual void InitControls();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
  CStatic m_size;
  CEdit m_log;
  CButton m_close;

  afx_msg void OnBnClickedButtonLogViewClose();
  afx_msg void OnBnClickedButtonLogViewRefresh();
  afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};
