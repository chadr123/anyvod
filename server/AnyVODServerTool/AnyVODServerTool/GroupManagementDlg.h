/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "ParentDlg.h"

struct ANYVOD_PACKET_GROUP_INFO;
union ANYVOD_PACKET;

// CGroupManagementDlg 대화 상자입니다.

class CGroupManagementDlg : public CParentDlg
{
	DECLARE_DYNAMIC(CGroupManagementDlg)

public:
	CGroupManagementDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CGroupManagementDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_GROUP_MANAGEMENT_DIALOG };

public:
  CListCtrl& GetGroupList();
  void AddGroupToList(unsigned long long pid, ANYVOD_PACKET_GROUP_INFO &item);

private:
  void DeleteGroupItems();
  void ToggleGroupItemControls();
  bool ProcessPacket();
  void UpdateTotal();
  virtual bool HandlePacket(ANYVOD_PACKET *packet);
  virtual void InitControls();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:
  CStatic m_total;
  CButton m_close;
  CButton m_delete;
  CButton m_modify;
  CButton m_add;
  CButton m_refresh;
  CListCtrl m_groupList;
  unsigned int m_itemCount;
  unsigned int m_itemCurCount;

  afx_msg void OnBnClickedButtonGroupManagementRefresh();
  afx_msg void OnBnClickedButtonGroupManagementAdd();
  afx_msg void OnBnClickedButtonGroupManagementModify();
  afx_msg void OnBnClickedButtonGroupManagementDelete();
  afx_msg void OnBnClickedButtonGroupManagementClose();
  afx_msg void OnNMClickListGroupManagementGrouplist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnNMDblclkListGroupManagementGrouplist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnNMReturnListGroupManagementGrouplist(NMHDR *pNMHDR, LRESULT *pResult);
protected:
  virtual void OnCancel();
public:
  afx_msg void OnLvnKeydownListGroupManagementGrouplist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnLvnItemchangedListGroupManagementGrouplist(NMHDR *pNMHDR, LRESULT *pResult);
};
