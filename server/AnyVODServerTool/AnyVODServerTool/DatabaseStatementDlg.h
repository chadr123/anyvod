/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "ParentDlg.h"

union ANYVOD_PACKET;

// CDatabaseStatementDlg 대화 상자입니다.

class CDatabaseStatementDlg : public CParentDlg
{
	DECLARE_DYNAMIC(CDatabaseStatementDlg)

public:
	CDatabaseStatementDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CDatabaseStatementDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DATABASE_STATEMENT_DIALOG };

public:
  CListCtrl& GetStatementList();

private:
  unsigned int m_maxCount;
  unsigned int m_itemCount;
  unsigned int m_itemCurCount;

private:
  bool ProcessPacket();
  void UpdateTotal();
  void DeleteStatementItems();
  void ToggleViewSQLControls();
  virtual bool HandlePacket(ANYVOD_PACKET *packet);
  virtual void InitControls();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
  CStatic m_total;
  CListCtrl m_statementList;
  CButton m_viewSQL;
  CStatic m_max;

  afx_msg void OnBnClickedButtonDatabaseStatementClose();
  afx_msg void OnBnClickedButtonDatabaseStatementViewSql();
  afx_msg void OnBnClickedButtonDatabaseStatementRefresh();
  afx_msg void OnNMClickListDatabaseStatementStatementlist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnNMDblclkListDatabaseStatementStatementlist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnNMReturnListDatabaseStatementStatementlist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnLvnItemchangedListDatabaseStatementStatementlist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnClose();
};
