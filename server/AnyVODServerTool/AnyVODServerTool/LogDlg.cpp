/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

// LogDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "AnyVODServerTool.h"
#include "LogDlg.h"
#include "LogViewDlg.h"
#include "AnyVODServerToolDlg.h"
#include "Socket.h"
#include "Util.h"

// CLogDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CLogDlg, CDialog)

CLogDlg::CLogDlg(CWnd* pParent /*=nullptr*/)
	: CParentDlg(CLogDlg::IDD, pParent), m_itemCount(0), m_itemCurCount(0)
{

}

CLogDlg::~CLogDlg()
{
}

void CLogDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_LIST_LOG_LIST, m_logList);
  DDX_Control(pDX, IDC_BUTTON_LOG_DELETE, m_delete);
  DDX_Control(pDX, IDC_BUTTON_LOG_VIEW, m_view);
  DDX_Control(pDX, IDC_BUTTON_LOG_REFRESH, m_refresh);
  DDX_Control(pDX, IDC_STATIC_LOG_COUNT, m_count);
}


BEGIN_MESSAGE_MAP(CLogDlg, CDialog)
  ON_BN_CLICKED(IDC_BUTTON_LOG_CLOSE, &CLogDlg::OnBnClickedButtonLogClose)
  ON_NOTIFY(NM_DBLCLK, IDC_LIST_LOG_LIST, &CLogDlg::OnNMDblclkListLogList)
  ON_NOTIFY(NM_CLICK, IDC_LIST_LOG_LIST, &CLogDlg::OnNMClickListLogList)
  ON_BN_CLICKED(IDC_BUTTON_LOG_DELETE, &CLogDlg::OnBnClickedButtonLogDelete)
  ON_BN_CLICKED(IDC_BUTTON_LOG_VIEW, &CLogDlg::OnBnClickedButtonLogView)
  ON_BN_CLICKED(IDC_BUTTON_LOG_REFRESH, &CLogDlg::OnBnClickedButtonLogRefresh)
  ON_NOTIFY(NM_RETURN, IDC_LIST_LOG_LIST, &CLogDlg::OnNMReturnListLogList)
  ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_LOG_LIST, &CLogDlg::OnLvnItemchangedListLogList)
END_MESSAGE_MAP()

void CLogDlg::InitControls()
{
  this->ToggleLogItemControls();

  this->m_logList.InsertColumn(0, L"파일명", LVCFMT_LEFT, 234);
  this->m_logList.InsertColumn(1, L"크기", LVCFMT_CENTER, 60);
  this->m_logList.SetExtendedStyle(LVS_EX_GRIDLINES| LVS_EX_FULLROWSELECT);
  this->m_logList.SetImageList(&Util::GetShellImageList(), LVSIL_SMALL);

  this->UpdateTotal();

  this->OnBnClickedButtonLogRefresh();

}

bool CLogDlg::HandlePacket(ANYVOD_PACKET *packet)
{
  switch (ntohl(packet->s2c_header.header.type))
  {
  case PT_ADMIN_S2C_LOGLIST:
    {
      if (packet->s2c_header.success)
      {
        this->m_itemCurCount = 0;
        this->m_itemCount = ntohl(packet->admin_s2c_loglist.count);
        this->m_logList.DeleteAllItems();

        this->UpdateTotal();

        return this->m_itemCount > 0;

      }

      break;

    }
  case PT_ADMIN_S2C_LOG_ITEM:
    {
      if (packet->s2c_header.success)
      {
        wstring name;
        LVITEM lvi;

        Util::ConvertUTF8ToUnicode((char*)packet->admin_s2c_log_item.fileName, MAX_PATH_SIZE, &name);

        lvi.mask = LVIF_TEXT | LVIF_IMAGE;
        lvi.iItem = this->m_itemCurCount;
        lvi.iSubItem = 0;
        lvi.pszText = (LPWSTR)name.c_str();

        SHFILEINFO sfi;
        wstring ext = name.substr(name.find_last_of(EXTENTION_SEPERATOR));

        transform(ext.begin(), ext.end(), ext.begin(), towlower);
        SHGetFileInfo(ext.c_str(), 0, &sfi, sizeof(SHFILEINFO), SHGFI_USEFILEATTRIBUTES | SHGFI_ICON | SHGFI_SMALLICON);

        lvi.iImage = sfi.iIcon;

        DestroyIcon(sfi.hIcon);

        this->m_logList.InsertItem(&lvi);

        CString fileSize;

        lvi.mask = LVIF_TEXT;
        lvi.iSubItem = 1;

        Util::FileSizeToString(ntohll(packet->admin_s2c_log_item.fileSize), &fileSize);

        lvi.pszText = (LPWSTR)fileSize.GetString();

        this->m_logList.SetItem(&lvi);

        this->m_itemCurCount++;

        if (this->m_itemCount == this->m_itemCurCount)
        {
          return false;

        }
        else
        {
          return true;

        }

      }

      break;

    }
  case PT_ADMIN_S2C_LOG_DELETE:
    {
      break;

    }

  }

  return false;

}

bool CLogDlg::ProcessPacket()
{
  bool nextProcess = false;
  bool success = false;
  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;
  Socket &socket = dlg->GetSocket();

  do
  {
    if (!Util::HandlePacket(socket, this, &nextProcess, &success))
    {
      this->EndDialog(IDCLOSE);

      break;

    }

  } while (nextProcess);

  return success;

}

CListCtrl& CLogDlg::GetLogList()
{
  return this->m_logList;

}

void CLogDlg::ToggleLogItemControls()
{
  int count = this->m_logList.GetSelectedCount();
  bool enable = count != 0;

  this->m_delete.EnableWindow(enable);

  if (count > 1)
  {
    this->m_view.EnableWindow(false);

  }
  else
  {
    this->m_view.EnableWindow(enable);

  }

  if (this->GetFocus() == nullptr)
  {
    this->SetFocus();

  }

}

void CLogDlg::UpdateTotal()
{
  CString count;

  count.Format(L"총 %d 개", this->m_itemCount);
  this->m_count.SetWindowText(count);


}

// CLogDlg 메시지 처리기입니다.

void CLogDlg::OnBnClickedButtonLogClose()
{
  this->EndDialog(IDCANCEL);

}

void CLogDlg::OnNMDblclkListLogList(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

  this->OnBnClickedButtonLogView();

  *pResult = 0;
}

void CLogDlg::OnNMClickListLogList(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

  this->ToggleLogItemControls();

  *pResult = 0;
}

void CLogDlg::OnBnClickedButtonLogDelete()
{
  POSITION pos = this->m_logList.GetFirstSelectedItemPosition();

  if (pos != NULL)
  {
    if (AfxMessageBox(L"로그를 삭제 하시겠습니까?", MB_YESNO) == IDYES)
    {
      CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;

      while (pos)
      {
        int item = this->m_logList.GetNextSelectedItem(pos);
        CString fileName = this->m_logList.GetItemText(item, 0);

        dlg->GetSocket().SendLogDelete(fileName.GetString());

        if (this->ProcessPacket())
        {
          this->m_itemCount--;
          this->m_logList.DeleteItem(item);

          pos = this->m_logList.GetFirstSelectedItemPosition();

        }
        else
        {
          if (pos != NULL)
          {
            if (AfxMessageBox(L"로그를 삭제 하는 도중 오류가 발생 했습니다. 계속 하시겠습니까?", MB_YESNO) == IDNO)
            {
              break;

            }

          }

        }

      }

      this->UpdateTotal();
      this->ToggleLogItemControls();

    }

  }
  else
  {
    AfxMessageBox(L"선택된 로그가 없습니다.");

  }

}

void CLogDlg::OnBnClickedButtonLogView()
{
  CLogViewDlg dlg(this);

  if (dlg.DoModal() == IDCLOSE)
  {
    this->EndDialog(IDCLOSE);

  }

}

void CLogDlg::OnBnClickedButtonLogRefresh()
{
  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;

  dlg->GetSocket().SendLogList();
  this->ProcessPacket();

  this->ToggleLogItemControls();

}

void CLogDlg::OnNMReturnListLogList(NMHDR *pNMHDR, LRESULT *pResult)
{
  this->OnBnClickedButtonLogView();

  *pResult = 0;
}

void CLogDlg::OnLvnItemchangedListLogList(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

  if (pNMLV->uOldState == 0)
  {
    this->ToggleLogItemControls();

  }

  *pResult = 0;
}
