/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

// UserManagementModifyDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "AnyVODServerTool.h"
#include "UserManagementDlg.h"
#include "UserManagementModifyDlg.h"
#include "AnyVODServerToolDlg.h"
#include "Socket.h"
#include "Util.h"

// CUserManagementModifyDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CUserManagementModifyDlg, CDialog)

CUserManagementModifyDlg::CUserManagementModifyDlg(CWnd* pParent /*=nullptr*/)
	: CParentDlg(CUserManagementModifyDlg::IDD, pParent)
{

}

CUserManagementModifyDlg::~CUserManagementModifyDlg()
{
}

void CUserManagementModifyDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_STATIC_USER_MANAGEMENT_MODIFY_ID, m_id);
  DDX_Control(pDX, IDC_EDIT_USER_MANAGEMENT_MODIFY_NAME, m_name);
  DDX_Control(pDX, IDC_CHECK_USER_MANAGEMENT_MODIFY_ADMIN, m_admin);
  DDX_Control(pDX, IDC_EDIT_USER_MANAGEMENT_MODIFY_PASSWORD, m_password);
  DDX_Control(pDX, IDC_EDIT_USER_MANAGEMENT_MODIFY_CONFIRM_PASSWORD, m_confirmPassword);
  DDX_Control(pDX, IDC_CHECK_USER_MANAGEMENT_MODIFY_MODIFY_PASSWORD, m_modifyPassword);
  DDX_Control(pDX, IDC_BUTTON_USER_MANAGEMENT_STOP_MULTIPLE_MODIFY, m_stopMultiModify);
}


BEGIN_MESSAGE_MAP(CUserManagementModifyDlg, CDialog)
  ON_BN_CLICKED(IDC_BUTTON_USER_MANAGEMENT_MODIFY_MODIFY, &CUserManagementModifyDlg::OnBnClickedButtonUserManagementModifyModify)
  ON_BN_CLICKED(IDC_BUTTON_USER_MANAGEMENT_MODIFY_CANCEL, &CUserManagementModifyDlg::OnBnClickedButtonUserManagementModifyCancel)
  ON_BN_CLICKED(IDC_CHECK_USER_MANAGEMENT_MODIFY_MODIFY_PASSWORD, &CUserManagementModifyDlg::OnBnClickedCheckUserManagementModifyModifyPassword)
  ON_BN_CLICKED(IDC_BUTTON_USER_MANAGEMENT_STOP_MULTIPLE_MODIFY, &CUserManagementModifyDlg::OnBnClickedButtonUserManagementStopModify)
END_MESSAGE_MAP()

void CUserManagementModifyDlg::InitControls()
{
  CUserManagementDlg *dlg = (CUserManagementDlg*)this->m_pParentWnd;
  CListCtrl &list = dlg->GetUserList();
  POSITION pos = list.GetFirstSelectedItemPosition();

  if (list.GetSelectedCount() > 1)
  {
    this->m_stopMultiModify.ShowWindow(SW_SHOW);

  }
  else
  {
    this->m_stopMultiModify.ShowWindow(SW_HIDE);

  }

  if (pos != NULL)
  {
    int item = list.GetNextSelectedItem(pos);
    this->m_name.SetLimitText(MAX_NAME_CHAR_SIZE);
    this->m_password.SetLimitText(MAX_PASS_CHAR_SIZE);
    this->m_confirmPassword.SetLimitText(MAX_PASS_CHAR_SIZE);

    this->m_id.SetWindowText(list.GetItemText(item, 0));
    this->m_name.SetWindowText(list.GetItemText(item, 1));

    ANYVOD_USER_INFO *info =  (ANYVOD_USER_INFO*)list.GetItemData(item);

    this->m_admin.SetCheck(info->admin ? BST_CHECKED : BST_UNCHECKED);
    this->m_modifyPassword.SetCheck(BST_UNCHECKED);
    this->m_password.EnableWindow(false);
    this->m_confirmPassword.EnableWindow(false);

    this->m_pid = info->pid;

  }
  else
  {
    AfxMessageBox(L"선택된 사용자가 없습니다.");

    this->EndDialog(IDCANCEL);

  }

}

bool CUserManagementModifyDlg::HandlePacket(ANYVOD_PACKET *packet)
{
  ANYVOD_PACKET_TYPE type = (ANYVOD_PACKET_TYPE)ntohl(packet->s2c_header.header.type);

  if (type == PT_ADMIN_S2C_USER_MODIFY)
  {
    if (packet->s2c_header.success)
    {
      CUserManagementDlg *dlg = (CUserManagementDlg*)this->m_pParentWnd;
      CString name;
      bool admin = this->m_admin.GetCheck() == BST_CHECKED;;
      CListCtrl &list = dlg->GetUserList();
      POSITION pos = list.GetFirstSelectedItemPosition();
      int item = list.GetNextSelectedItem(pos);
      bool modifyPass = this->m_modifyPassword.GetCheck() == BST_CHECKED;

      this->m_name.GetWindowText(name);

      list.SetItemText(item, 1, name);

      if (admin)
      {
        list.SetItemText(item, 2, L"예");

      }
      else
      {
        list.SetItemText(item, 2, L"아니오");

      }

      ANYVOD_USER_INFO *info = (ANYVOD_USER_INFO*)list.GetItemData(item);

      info->admin = admin;

      if (modifyPass)
      {
        CString pass;

        this->m_password.GetWindowText(pass);
        info->pass = pass.GetString();

      }

      info->name = name;

      this->EndDialog(IDOK);

    }

  }

  return false;

}

void CUserManagementModifyDlg::ProcessPacket()
{
  bool nextProcess = false;
  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd->GetParent();
  Socket &socket = dlg->GetSocket();

  do
  {
    if (!Util::HandlePacket(socket, this, &nextProcess, nullptr))
    {
      this->EndDialog(IDCLOSE);

      break;

    }

  } while (nextProcess);

}

// CUserManagementModifyDlg 메시지 처리기입니다.

BOOL CUserManagementModifyDlg::PreTranslateMessage(MSG* pMsg)
{
  return CDialog::PreTranslateMessage(pMsg);
}

void CUserManagementModifyDlg::OnOK()
{
  this->OnBnClickedButtonUserManagementModifyModify();

  //CParentDlg::OnOK();
}

void CUserManagementModifyDlg::OnCancel()
{
  this->OnBnClickedButtonUserManagementModifyCancel();

  CParentDlg::OnCancel();
}

void CUserManagementModifyDlg::OnBnClickedButtonUserManagementModifyModify()
{
  CString pass;
  CString confirmPass;
  CString name;
  bool modifyPass = this->m_modifyPassword.GetCheck() == BST_CHECKED ;

  this->m_password.GetWindowText(pass);
  this->m_confirmPassword.GetWindowText(confirmPass);
  this->m_name.GetWindowText(name);

  pass.Trim();
  confirmPass.Trim();
  name.Trim();

  if (name.GetLength() == 0)
  {
    AfxMessageBox(L"이름을 입력해 주세요.");

    this->m_name.SetFocus();

    return;

  }

  if (modifyPass)
  {
    if (pass.GetLength() == 0)
    {
      AfxMessageBox(L"암호를 입력해 주세요.");

      this->m_password.SetFocus();

      return;

    }
    else if (confirmPass.GetLength() == 0)
    {
      AfxMessageBox(L"암호 확인을 입력해 주세요.");

      this->m_confirmPassword.SetFocus();

      return;


    }
    else if (pass != confirmPass)
    {
      AfxMessageBox(L"암호를 확인해 주세요.");

      this->m_password.SetFocus();

      return;


    }

  }
  else
  {
    pass.Empty();

  }

  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd->GetParent();
  CUserManagementDlg *pdlg = (CUserManagementDlg*)this->m_pParentWnd;
  ANYVOD_USER_INFO info;

  info.pass = pass.GetString();
  info.name = name.GetString();
  info.admin = this->m_admin.GetCheck() == BST_CHECKED;

  dlg->GetSocket().SendUserModify(this->m_pid, info);
  this->ProcessPacket();

}

void CUserManagementModifyDlg::OnBnClickedButtonUserManagementModifyCancel()
{
  this->EndDialog(IDCANCEL);

}


void CUserManagementModifyDlg::OnBnClickedCheckUserManagementModifyModifyPassword()
{
  bool modify = this->m_modifyPassword.GetCheck() == BST_CHECKED;

  this->m_password.EnableWindow(modify);
  this->m_confirmPassword.EnableWindow(modify);

}

void CUserManagementModifyDlg::OnBnClickedButtonUserManagementStopModify()
{
  this->EndDialog(IDABORT);

}
