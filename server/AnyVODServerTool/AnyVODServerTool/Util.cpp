/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "stdafx.h"
#include "Util.h"
#include "Socket.h"
#include "ParentDlg.h"
#include "Common.h"

void
Util::ConvertAsciiToUnicode(const string &ascii, wstring *ret)
{
  int nReqire = ::MultiByteToWideChar(CP_ACP, 0, ascii.c_str(), (int)ascii.size(), nullptr, 0);
  wchar_t *wszTmp = new wchar_t[nReqire+1];

  MultiByteToWideChar(CP_ACP, 0, ascii.c_str(), (int)ascii.size(), wszTmp, nReqire+1);

  wszTmp[nReqire] = L'\0';

  *ret = wszTmp;

  delete[] wszTmp;

}

void
Util::ConvertUnicodeToAscii(const wstring &unicode, string *ret)
{
  int nReqire = ::WideCharToMultiByte(CP_ACP, 0, unicode.c_str(), (int)unicode.size(), nullptr, 0, nullptr, nullptr);
  char *szTmp = new char[nReqire+1];

  WideCharToMultiByte(CP_ACP, 0, unicode.c_str(), (int)unicode.size(), szTmp, nReqire+1, nullptr, nullptr);

  szTmp[nReqire] = '\0';

  *ret = szTmp;

  delete[] szTmp;

}

void
Util::ConvertUnicodeToUTF8(const wstring &unicode, char *ret, int size)
{
  memset(ret, 0x00, size);

  WideCharToMultiByte(CP_UTF8, 0, unicode.c_str(), (int)unicode.size(), ret, size-MAX_UTF8_SIZE, nullptr, nullptr);

}

void
Util::ConvertUTF8ToUnicode(const char *utf8, int size, wstring *ret)
{
  int nReqire = ::MultiByteToWideChar(CP_UTF8, 0, utf8, size, nullptr, 0);
  wchar_t *wszTmp = new wchar_t[nReqire+1];

  MultiByteToWideChar(CP_UTF8, 0, utf8, size, wszTmp, nReqire+1);

  wszTmp[nReqire] = '\0';

  *ret = wszTmp;

  delete[] wszTmp;

}

void
Util::SplitString(const wstring &str, const wstring &delim, bool inserLastBlank, vector_wstring *ret)
{
  size_t start = 0;
  size_t end = str.find_first_of(delim);

  if (end == wstring::npos)
  {
    ret->push_back(str);

  }
  else
  {
    wstring substr;

    do
    {
      substr = str.substr(start, end - start);

      ret->push_back(substr);

      start = end + 1;

      end = str.find_first_of(delim, start);

    } while (end != wstring::npos);

    substr = str.substr(start, str.size() - start);

    if (substr.size() == 0)
    {
      if (inserLastBlank)
      {
        ret->push_back(substr);

      }

    }
    else
    {
      ret->push_back(substr);

    }

  }

}

void
Util::NumberFormatting(CString *ret)
{
  int index = ret->ReverseFind(L'.');

  if (index == -1)
  {
    index = ret->GetLength();

  }

  CString formatted;
  int i = index - 3;
  int k = 3;

  for (; i > 0; i -= 3, k += 3)
  {
    if (ret->GetAt(i) != L',')
    {
      ret->Insert(i, L',');

    }

  }

}

void
Util::PrintErrorMessage(ERROR_TYPE type)
{
  AfxMessageBox(ERROR_TYPE_MSG[type]);

}

bool
Util::HandlePacket(Socket &socket, CParentDlg *dlg, bool *nextProcess, bool *success)
{
  ANYVOD_PACKET *packet = nullptr;

  if (socket.BeginResult(&packet))
  {
    bool ret = dlg->HandlePacket(packet);

    if (nextProcess != nullptr)
    {
      *nextProcess = ret;

    }

    if (success != nullptr)
    {
      *success = packet->s2c_header.success != 0;

    }

    if (packet->s2c_header.success)
    {
      if (ntohl(packet->s2c_header.header.type) == PT_S2C_ERROR)
      {
        Util::PrintErrorMessage((ERROR_TYPE)ntohl(packet->s2c_error.type));

      }

      socket.EndResult();

      return true;

    }
    else
    {
      socket.EndResult();

      if (socket.BeginResult(&packet))
      {
        Util::PrintErrorMessage((ERROR_TYPE)ntohl(packet->s2c_error.type));

        socket.EndResult();

        return true;

      }
      else
      {
        AfxMessageBox(L"연결 끊김");

        return false;

      }

    }

  }
  else
  {
    AfxMessageBox(L"연결 끊김");

    return false;

  }

}

bool
Util::GetFieldTextAndValidate(CEdit &edit, CButton &chk, CString *ret)
{
  if (chk.GetCheck() == BST_CHECKED)
  {
    *ret = L"*";

    return true;

  }
  else
  {
    edit.GetWindowText(*ret);

    ret->Trim();

    if (ret->GetLength() == 0)
    {
      AfxMessageBox(L"내용을 입력 해 주세요.");

      edit.SetFocus();

      return false;

    }

    wstring tmp = *ret;

    size_t index = tmp.find_first_not_of(L"1234567890,-");

    if (index != wstring::npos)
    {
      CString msg;

      msg.Format(L"\"%c\"는 허용하지 않는 문자입니다.", ret->GetAt(index));

      AfxMessageBox(msg);

      edit.SetFocus();

      return false;

    }

    index = 0;

    while (true)
    {
      index = tmp.find_first_of(L",-", index+1);

      if (index == wstring::npos)
        break;

      bool valid = false;

      if (tmp[index] == L'-')
      {
        size_t next = tmp.find_first_of(L'-', index+1);

        if (next != wstring::npos)
        {
          wstring sub = tmp.substr(index, next-index);

          if (sub.find_first_of(L',') == wstring::npos)
          {
            AfxMessageBox(L"하이픈(-)사이에는 컴마(,)가 존재해야 합니다.");

            edit.SetFocus();

            return false;

          }

        }

      }

      if (index == 0)
      {
        valid = false;

      }
      else if (index == tmp.size()-1)
      {
        valid = false;

      }
      else if (!isdigit(tmp[index-1]) || !isdigit(tmp[index+1]))
      {
        valid = false;

      }
      else
      {
        valid = true;

      }

      if (!valid)
      {
        AfxMessageBox(L"컴마(,) 또는 하이픈(-)의 앞뒤에는 숫자가 존재해야 합니다.");

        edit.SetFocus();

        return false;

      }

    }

    return true;

  }

}

bool
Util::ValidateField(CString &field, TIME_INDEX index)
{
  wstring tmp = field;

  size_t start = 0;
  size_t end = tmp.find_first_of(L',', start);

  if (end == wstring::npos)
  {
    if (!Util::ValidateFieldSub(tmp, index))
      return false;

  }
  else
  {
    while ((end = tmp.find_first_of(L',', start)) != wstring::npos)
    {
      wstring timeToken = tmp.substr(start, end - start);

      if (!Util::ValidateFieldSub(timeToken, index))
        return false;

      start = end + 1;

    }

    wstring timeToken = tmp.substr(start);

    if (!Util::ValidateFieldSub(timeToken, index))
      return false;

  }

  return true;

}

bool
Util::ValidateFieldSub(wstring &token, TIME_INDEX index)
{
  if (token == L"*")
  {
    return true;

  }
  else
  {
    static TIME_VALUE limits;
    TIME time;
    size_t rangeEnd = token.find_first_of(L'-');
    TIME limit = limits.timeLimits[index];
    LPWSTR tmp = (LPWSTR)token.c_str();
    CString notNumber;

    notNumber.Format(L"%s는 숫자가 아닙니다.", tmp);

    if (rangeEnd == wstring::npos)
    {
      if (swscanf(token.c_str(), L"%d", &time.min) < 1)
      {
        AfxMessageBox(notNumber);

        return false;

      }

      time.max = time.min;

    }
    else
    {
      wstring rangeToken = token.substr(0, rangeEnd);

      if (swscanf(rangeToken.c_str(), L"%d", &time.min) < 1)
      {
        AfxMessageBox(notNumber);

        return false;

      }

      rangeToken = token.substr(rangeEnd + 1);

      if (swscanf(rangeToken.c_str(), L"%d", &time.max) < 1)
      {
        AfxMessageBox(notNumber);

        return false;

      }

      if (time.min > time.max)
      {
        CString msg;

        msg.Format(L"오른쪽 숫자가 왼쪽 숫자보다 클 수 없습니다.(%s)", tmp);

        AfxMessageBox(msg);

        return false;

      }

    }

    if (time.min >= limit.min && time.max <= limit.max)
    {
      return true;

    }
    else
    {
      CString msg;

      msg.Format(L"입력하신 %s 의 숫자가 범위를 초과합니다.\n%d~%d의 값을 입력 해 주세요.", tmp, limit.min, limit.max);

      AfxMessageBox(msg);

      return false;

    }

  }

}

void
Util::AddTypesToComboBox(char types[IT_COUNT][MAX_SCHEDULE_NAME_SIZE], CComboBox *ret)
{
  for (int i = IT_HEAD; i < IT_COUNT; i++)
  {
    wstring tmp;

    Util::ConvertAsciiToUnicode(types[i], &tmp);
    ret->AddString(tmp.c_str());

  }

}

void
Util::FileSizeToString(unsigned long long fileSize, CString *ret)
{
  if (fileSize < KILO * KILO)
  {
    ret->Format(L"%dKB", (int)floor(fileSize / (double)KILO + 0.5));

  }
  else if (fileSize < MEGA * KILO)
  {
    ret->Format(L"%dMB", (int)floor(fileSize / (double)MEGA + 0.5));

  }
  else
  {
    ret->Format(L"%.1f", (float)(fileSize / (double)GIGA));
    Util::NumberFormatting(ret);
    *ret += L"GB";

  }

}

CImageList&
Util::GetShellImageList()
{
  static IMAGE_CONSTRUCTOR img;

  return img.list;

}

CImageList&
Util::GetIconImageList()
{
  static ICON_IMAGE_CONSTRUCTOR img;

  return img.list;

}

void
Util::AppendContentsToEdit(wstring &contents, CEdit &edit)
{
  int len = edit.GetWindowTextLength();

  edit.SetSel(len, len);
  edit.ReplaceSel(contents.c_str());
  edit.LineScroll(edit.GetLineCount());

}

void
Util::TrimString(const wstring &src, wstring *ret)
{
  *ret = src;

  ret->erase(0, ret->find_first_not_of(L" \t\v\n\r"));
  ret->erase(ret->find_last_not_of(L" \t\v\n\r") + 1);

}

void
Util::ListControlScrollToEnd(CListCtrl &list)
{
  CRect rect;
  CSize size;

  list.GetItemRect(0, rect, LVIR_BOUNDS);

  size.cx = 0;
  size.cy = rect.Height() * list.GetItemCount();

  list.Scroll(size);

}

void
Util::TimeToString(unsigned int time, CString *ret)
{
  if (time < 60 )
    ret->Format(L"%d초", time);
  else if (time < 3600)
    ret->Format(L"%d분 %d초", time / 60, time % 60);
  else
    ret->Format(L"%d시간 %d분 %d초", time / 3600, time / 60 - ((time / 3600) * 60), time % 60);

}

void
Util::GetFittedText(CWnd &control, CString &orgText, CString *ret)
{
  CDC *dc = control.GetDC();
  CFont *font = dc->SelectObject(control.GetFont());
  CSize len = dc->GetTextExtent(orgText);
  CRect rect;
  int i = 1;

  control.GetClientRect(&rect);

  *ret = orgText;

  while (len.cx >= rect.right - rect.left)
  {
    CString left = orgText.Left(orgText.GetLength()/2 - i);
    CString right = orgText.Mid(orgText.GetLength()/2 + i);

    *ret = left + L"..." + right;

    len = dc->GetTextExtent(*ret);

    i++;

  }

  dc->SelectObject(font);

}

void
Util::AppendDirSeparator(wstring *path)
{
  if (path->size() == 0 || (*path)[path->size()-1] != DIRECTORY_SEPERATOR)
    path->append(1, DIRECTORY_SEPERATOR);

}

void
Util::GetFileNameFromPath(const wstring &path, wstring *ret)
{
  size_t endPos;
  size_t pos = path.find_last_of(DIRECTORY_SEPERATOR);

  if (pos == wstring::npos)
    pos = 0;
  else
    pos++;

  endPos = path.find_last_of(EXTENTION_SEPERATOR);

  if (endPos == wstring::npos)
    endPos = path.length();

  *ret = path.substr(pos, endPos - pos);

}
