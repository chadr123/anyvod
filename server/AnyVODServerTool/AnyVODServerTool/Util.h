/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "..\..\..\common\types.h"

class Socket;
class CParentDlg;

class Util
{
private:
  Util(){}
  ~Util(){}

  struct TIME_VALUE
  {
    TIME_VALUE()
    {
      timeLimits.push_back(TIME(0, 59));//minute
      timeLimits.push_back(TIME(0, 23));//hour
      timeLimits.push_back(TIME(1, 31));//day
      timeLimits.push_back(TIME(1, 12));//month
      timeLimits.push_back(TIME(0, 6));//weekday

    }

    VECTOR_TIME timeLimits;

  };

  struct IMAGE_CONSTRUCTOR
  {
    IMAGE_CONSTRUCTOR()
    {
      IImageList *handle = nullptr;

      SHGetImageList(SHIL_SMALL, IID_IImageList, (void**)&handle);

      list.Attach((HIMAGELIST)handle);

    }

    ~IMAGE_CONSTRUCTOR()
    {
      ImageList_Destroy(list.Detach());

    }

    CImageList list;

  };

  struct ICON_IMAGE_CONSTRUCTOR
  {
    ICON_IMAGE_CONSTRUCTOR()
    {
      HINSTANCE hinstShell32 = NULL;
      WCHAR lpszLibPath[MAX_PATH*2];

      GetSystemDirectoryW(lpszLibPath, MAX_PATH+1);
      wcscat(lpszLibPath, L"\\SHELL32.DLL");

      hinstShell32 = LoadLibraryW(lpszLibPath);

      list.Create(16, 16, ILC_COLOR32 | ILC_MASK, 0, 2);

      list.Add(LoadIcon(hinstShell32, MAKEINTRESOURCE(21)));
      list.Add(LoadIcon(hinstShell32, MAKEINTRESOURCE(269)));
      list.Add(LoadIcon(hinstShell32, MAKEINTRESOURCE(220)));
      list.Add(LoadIcon(hinstShell32, MAKEINTRESOURCE(18)));
      list.Add(LoadIcon(hinstShell32, MAKEINTRESOURCE(37)));
      list.Add(LoadIcon(hinstShell32, MAKEINTRESOURCE(194)));
      list.Add(LoadIcon(hinstShell32, MAKEINTRESOURCE(137)));
      list.Add(LoadIcon(hinstShell32, MAKEINTRESOURCE(301)));
      list.Add(LoadIcon(hinstShell32, MAKEINTRESOURCE(29)));

      FreeLibrary(hinstShell32);

    }

    ~ICON_IMAGE_CONSTRUCTOR()
    {
      list.DeleteImageList();

    }

    CImageList list;

  };

public:
  static void ConvertAsciiToUnicode(const string &ascii, wstring *ret);
  static void ConvertUnicodeToAscii(const wstring &unicode, string *ret);
  static void ConvertUnicodeToUTF8(const wstring &unicode, char *ret, int size);
  static void ConvertUTF8ToUnicode(const char *utf8, int size, wstring *ret);
  static bool HandlePacket(Socket &socket, CParentDlg *dlg, bool *nextProcess, bool *success);
  static void SplitString(const wstring &str, const wstring &delim, bool inserLastBlank, vector_wstring *ret);
  static void NumberFormatting(CString *ret);
  static bool GetFieldTextAndValidate(CEdit &edit, CButton &chk, CString *ret);
  static bool ValidateField(CString &field, TIME_INDEX index);
  static bool ValidateFieldSub(wstring &token, TIME_INDEX index);
  static void AddTypesToComboBox(char types[IT_COUNT][MAX_SCHEDULE_NAME_SIZE], CComboBox *ret);
  static void FileSizeToString(unsigned long long fileSize, CString *ret);
  static CImageList& GetShellImageList();
  static void AppendContentsToEdit(wstring &contents, CEdit &edit);
  static CImageList& GetIconImageList();
  static void PrintErrorMessage(ERROR_TYPE type);
  static void TrimString(const wstring &src, wstring *ret);
  static void ListControlScrollToEnd(CListCtrl &list);
  static void TimeToString(unsigned int time, CString *ret);
  static void GetFittedText(CWnd &control, CString &orgText, CString *ret);
  static void AppendDirSeparator(wstring *path);
  static void GetFileNameFromPath(const wstring &path, wstring *ret);

};
