/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "ParentDlg.h"

union ANYVOD_PACKET;

// CLogDlg 대화 상자입니다.

class CLogDlg : public CParentDlg
{
	DECLARE_DYNAMIC(CLogDlg)

public:
	CLogDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CLogDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_LOG_DIALOG };

public:
  CListCtrl& GetLogList();

private:
  unsigned int m_itemCount;
  unsigned int m_itemCurCount;

private:
  void ToggleLogItemControls();
  bool ProcessPacket();
  void UpdateTotal();
  virtual bool HandlePacket(ANYVOD_PACKET *packet);
  virtual void InitControls();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
  CListCtrl m_logList;
  CButton m_delete;
  CButton m_view;
  CButton m_refresh;
  CStatic m_count;

  afx_msg void OnBnClickedButtonLogClose();
  afx_msg void OnNMDblclkListLogList(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnNMClickListLogList(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnBnClickedButtonLogDelete();
  afx_msg void OnBnClickedButtonLogView();
  afx_msg void OnBnClickedButtonLogRefresh();
  afx_msg void OnNMReturnListLogList(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnLvnItemchangedListLogList(NMHDR *pNMHDR, LRESULT *pResult);
};
