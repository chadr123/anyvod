/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

// ClientStatusDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "AnyVODServerTool.h"
#include "ClientStatusDlg.h"
#include "AnyVODServerToolDlg.h"
#include "Socket.h"
#include "Util.h"

// CClientStatusDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CClientStatusDlg, CDialog)

CClientStatusDlg::CClientStatusDlg(CWnd* pParent /*=nullptr*/)
	: CParentDlg(CClientStatusDlg::IDD, pParent), m_itemCount(0), m_itemCurCount(0)
{

}

CClientStatusDlg::~CClientStatusDlg()
{
}

void CClientStatusDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_LIST_CLIENT_STATUS_STATUSLIST, m_statusList);
  DDX_Control(pDX, IDC_STATIC_CLIENT_STATUS_COUNT, m_count);
  DDX_Control(pDX, IDC_BUTTON_CLIENT_STATUS_LOGOUT, m_logout);
  DDX_Control(pDX, IDC_STATIC_CLIENT_STATUS_ID, m_id);
  DDX_Control(pDX, IDC_STATIC_CLIENT_STATUS_IP, m_ip);
  DDX_Control(pDX, IDC_STATIC_CLIENT_STATUS_STATE, m_state);
  DDX_Control(pDX, IDC_STATIC_CLIENT_STATUS_PORT, m_port);
  DDX_Control(pDX, IDC_STATIC_CLIENT_STATUS_TOTAL_RECV, m_totalRecv);
  DDX_Control(pDX, IDC_STATIC_CLIENT_STATUS_TOTAL_SENT, m_totalSent);
  DDX_Control(pDX, IDC_STATIC_CLIENT_STATUS_RECV, m_recv);
  DDX_Control(pDX, IDC_STATIC_CLIENT_STATUS_SENT, m_sent);
  DDX_Control(pDX, IDC_STATIC_CLIENT_STATUS_RECV_BUFFER_COUNT, m_recvBufferCount);
  DDX_Control(pDX, IDC_STATIC_CLIENT_STATUS_SENT_BUFFER_COUNT, m_sentBufferCount);
  DDX_Control(pDX, IDC_STATIC_CLIENT_STATUS_LAST_PACKET, m_lastPacket);
  DDX_Control(pDX, IDC_STATIC_CLIENT_STATUS_STREAM, m_stream);
}


BEGIN_MESSAGE_MAP(CClientStatusDlg, CDialog)
  ON_BN_CLICKED(IDC_BUTTON_CLIENT_STATUS_REFRESH, &CClientStatusDlg::OnBnClickedButtonClientStatusRefresh)
  ON_WM_CLOSE()
  ON_BN_CLICKED(IDC_BUTTON_CLIENT_STATUS_CLOSE, &CClientStatusDlg::OnBnClickedButtonClientStatusClose)
  ON_NOTIFY(NM_CLICK, IDC_LIST_CLIENT_STATUS_STATUSLIST, &CClientStatusDlg::OnNMClickListClientStatusStatuslist)
  ON_BN_CLICKED(IDC_BUTTON_CLIENT_STATUS_LOGOUT, &CClientStatusDlg::OnBnClickedButtonClientStatusLogout)
  ON_NOTIFY(NM_RETURN, IDC_LIST_CLIENT_STATUS_STATUSLIST, &CClientStatusDlg::OnNMReturnListClientStatusStatuslist)
  ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_CLIENT_STATUS_STATUSLIST, &CClientStatusDlg::OnLvnItemchangedListClientStatusStatuslist)
END_MESSAGE_MAP()

void CClientStatusDlg::InitControls()
{
  this->ToggleClientStatusItemControls();

  this->m_statusList.InsertColumn(0, L"아이디", LVCFMT_LEFT, 134);
  this->m_statusList.InsertColumn(1, L"상태", LVCFMT_LEFT, 78);
  this->m_statusList.InsertColumn(2, L"받은 트래픽", LVCFMT_RIGHT, 78);
  this->m_statusList.InsertColumn(3, L"보낸 트래픽", LVCFMT_RIGHT, 78);
  this->m_statusList.InsertColumn(4, L"마지막 패킷", LVCFMT_LEFT, 207);
  this->m_statusList.InsertColumn(5, L"스트림", LVCFMT_LEFT, 78);

  this->m_statusList.SetExtendedStyle(LVS_EX_GRIDLINES| LVS_EX_FULLROWSELECT);
  this->m_statusList.SetImageList(&Util::GetIconImageList(), LVSIL_SMALL);

  this->m_id.SetWindowText(L"-");
  this->m_ip.SetWindowText(L"-");
  this->m_state.SetWindowText(L"-");
  this->m_port.SetWindowText(L"-");
  this->m_totalRecv.SetWindowText(L"0 KB");
  this->m_totalSent.SetWindowText(L"0 KB");
  this->m_recv.SetWindowText(L"0 KB/s");
  this->m_sent.SetWindowText(L"0 KB/s");
  this->m_recvBufferCount.SetWindowText(L"0 개");
  this->m_sentBufferCount.SetWindowText(L"0 개");
  this->m_lastPacket.SetWindowText(L"-");
  this->m_stream.SetWindowText(L"-");

  this->UpdateTotal();

  this->OnBnClickedButtonClientStatusRefresh();

}

bool CClientStatusDlg::HandlePacket(ANYVOD_PACKET *packet)
{
  switch (ntohl(packet->s2c_header.header.type))
  {
  case PT_ADMIN_S2C_CLIENT_LOGOUT:
    {
      break;

    }
  case PT_ADMIN_S2C_CLIENT_STATUSLIST:
    {
      if (packet->s2c_header.success)
      {
        this->m_itemCurCount = 0;
        this->m_itemCount = ntohl(packet->admin_s2c_loglist.count);
        this->DeleteClientStatusItems();

        this->UpdateTotal();

        return this->m_itemCount > 0;

      }

      break;

    }
  case PT_ADMIN_S2C_CLIENT_STATUS_ITEM:
    {
      if (packet->s2c_header.success)
      {
        ANYVOD_CLIENT_STATUS *item = new ANYVOD_CLIENT_STATUS;
        ANYVOD_PACKET_CLIENT_STATUS &status = packet->admin_s2c_client_status_item.status;

        item->clientState = (CLIENT_STATE)ntohl(status.clientState);
        item->lastPacketType = (ANYVOD_PACKET_TYPE)ntohl(status.lastPacketType);

        Util::ConvertUTF8ToUnicode((char*)status.id, MAX_ID_SIZE, &item->id);
        Util::ConvertAsciiToUnicode((char*)status.ip, &item->ip);
        item->port = ntohs(status.port);
        item->recvBufCount = ntohl(status.recvBufCount);
        item->recvBytesPerSec = ntohl(status.recvBytesPerSec);
        item->totalRecvBytes = ntohll(status.totalRecvBytes);
        item->sendBufCount = ntohl(status.sendBufCount);
        item->sentBytesPerSec = ntohl(status.sentBytesPerSec);
        item->totalSentBytes = ntohll(status.totalSentBytes);
        item->isStream = status.isStream ? true : false;
        item->ticket = (char*)status.ticket;

        ITEM_KEY key;

        key.ticket = item->ticket;
        key.isStream = item->isStream;

        this->m_items.insert(make_pair(key, item));
        this->m_itemCurCount++;

        if (this->m_itemCount == this->m_itemCurCount)
        {
          this->InsertItems();

          return false;

        }
        else
        {
          return true;

        }

      }

      break;

    }

  }

  return false;

}

void CClientStatusDlg::InsertItems()
{
  map<ITEM_KEY, ANYVOD_CLIENT_STATUS*>::iterator i = this->m_items.begin();

  for (int index = 0; i != this->m_items.end(); i++, index++)
  {
    ANYVOD_CLIENT_STATUS *item = i->second;
    LVITEM lvi;

    lvi.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM | LVIF_INDENT;
    lvi.iItem = index;
    lvi.iSubItem = 0;
    lvi.pszText = (LPWSTR)item->id.c_str();
    lvi.iImage = item->isStream ? ICON_INDEX_STREAM : ICON_INDEX_CLIENT_STATUS;
    lvi.iIndent = item->isStream ? 1 : 0;
    lvi.lParam = (LPARAM)item;

    this->m_statusList.InsertItem(&lvi);

    lvi.mask = LVIF_TEXT;
    lvi.iSubItem = 1;
    lvi.pszText = (LPWSTR)CLIENT_STATE_MSG[item->clientState].GetString();

    this->m_statusList.SetItem(&lvi);

    CString data;

    data.Format(L"%llu", item->recvBytesPerSec / KILO);
    Util::NumberFormatting(&data);
    data += L" KB/s";

    lvi.iSubItem = 2;
    lvi.pszText = (LPWSTR)data.GetString();

    this->m_statusList.SetItem(&lvi);

    data.Format(L"%llu", item->sentBytesPerSec / KILO);
    Util::NumberFormatting(&data);
    data += L" KB/s";

    lvi.iSubItem = 3;
    lvi.pszText = (LPWSTR)data.GetString();

    this->m_statusList.SetItem(&lvi);

    lvi.iSubItem = 4;
    lvi.pszText = (LPWSTR)PACKET_TYPE_STRING[item->lastPacketType-1].GetString();

    this->m_statusList.SetItem(&lvi);

    lvi.iSubItem = 5;
    lvi.pszText = item->isStream ? L"예" : L"아니오";

    this->m_statusList.SetItem(&lvi);
  }

}

void CClientStatusDlg::DeleteClientStatusItems()
{
  for (int i = 0; i < this->m_statusList.GetItemCount(); i++)
  {
    delete (ANYVOD_CLIENT_STATUS*)this->m_statusList.GetItemData(i);

  }

  this->m_statusList.DeleteAllItems();
  this->m_items.clear();

}

bool CClientStatusDlg::ProcessPacket()
{
  bool nextProcess = false;
  bool success = false;
  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;
  Socket &socket = dlg->GetSocket();

  do
  {
    if (!Util::HandlePacket(socket, this, &nextProcess, &success))
    {
      this->EndDialog(IDCLOSE);

      break;

    }

  } while (nextProcess);

  return success;

}

void CClientStatusDlg::ToggleClientStatusItemControls()
{
  bool enable = this->m_statusList.GetSelectedCount() != 0;

  this->m_logout.EnableWindow(enable);
  this->m_id.EnableWindow(enable);
  this->m_ip.EnableWindow(enable);
  this->m_state.EnableWindow(enable);
  this->m_port.EnableWindow(enable);
  this->m_totalRecv.EnableWindow(enable);
  this->m_totalSent.EnableWindow(enable);
  this->m_recv.EnableWindow(enable);
  this->m_sent.EnableWindow(enable);
  this->m_recvBufferCount.EnableWindow(enable);
  this->m_sentBufferCount.EnableWindow(enable);
  this->m_lastPacket.EnableWindow(enable);
  this->m_stream.EnableWindow(enable);

  if (this->GetFocus() == nullptr)
  {
    this->SetFocus();

  }

}

void CClientStatusDlg::UpdateTotal()
{
  CString count;

  count.Format(L"총 %d 개", this->m_itemCount);
  this->m_count.SetWindowText(count);

}

void
CClientStatusDlg::ViewDetail()
{
  POSITION pos = this->m_statusList.GetFirstSelectedItemPosition();

  if (pos != NULL)
  {
    int item = this->m_statusList.GetNextSelectedItem(pos);
    ANYVOD_CLIENT_STATUS *status = (ANYVOD_CLIENT_STATUS*)this->m_statusList.GetItemData(item);

    this->m_id.SetWindowText(status->id.c_str());
    this->m_ip.SetWindowText(status->ip.c_str());

    this->m_state.SetWindowText(CLIENT_STATE_MSG[status->clientState]);

    CString tmp;

    tmp.Format(L"%hu", status->port);
    this->m_port.SetWindowText(tmp);

    tmp.Format(L"%llu", status->totalRecvBytes / KILO);
    Util::NumberFormatting(&tmp);
    tmp += L" KB";
    this->m_totalRecv.SetWindowText(tmp);

    tmp.Format(L"%llu", status->totalSentBytes / KILO);
    Util::NumberFormatting(&tmp);
    tmp += L" KB";
    this->m_totalSent.SetWindowText(tmp);

    tmp.Format(L"%u", status->recvBytesPerSec / KILO);
    Util::NumberFormatting(&tmp);
    tmp += L" KB/s";
    this->m_recv.SetWindowText(tmp);

    tmp.Format(L"%u", status->sentBytesPerSec / KILO);
    Util::NumberFormatting(&tmp);
    tmp += L" KB/s";
    this->m_sent.SetWindowText(tmp);

    tmp.Format(L"%u", status->recvBufCount);
    Util::NumberFormatting(&tmp);
    tmp += L" 개";
    this->m_recvBufferCount.SetWindowText(tmp);

    tmp.Format(L"%u", status->sendBufCount);
    Util::NumberFormatting(&tmp);
    tmp += L" 개";
    this->m_sentBufferCount.SetWindowText(tmp);

    this->m_lastPacket.SetWindowText(PACKET_TYPE_STRING[status->lastPacketType-1]);
    this->m_stream.SetWindowText(status->isStream ? L"예" : L"아니오");

  }

}

// CClientStatusDlg 메시지 처리기입니다.

void CClientStatusDlg::OnBnClickedButtonClientStatusRefresh()
{
  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;

  dlg->GetSocket().SendClientStatusList();
  this->ProcessPacket();

  this->ToggleClientStatusItemControls();
}

void CClientStatusDlg::OnClose()
{
  this->DeleteClientStatusItems();

  CParentDlg::OnClose();
}

void CClientStatusDlg::OnBnClickedButtonClientStatusClose()
{
  this->DeleteClientStatusItems();

  this->EndDialog(IDCANCEL);
}

void CClientStatusDlg::OnNMClickListClientStatusStatuslist(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

  this->ViewDetail();
  this->ToggleClientStatusItemControls();

  *pResult = 0;
}

void CClientStatusDlg::OnBnClickedButtonClientStatusLogout()
{
  POSITION pos = this->m_statusList.GetFirstSelectedItemPosition();

  if (pos != NULL)
  {
    if (AfxMessageBox(L"로그 아웃 하시겠습니까?", MB_YESNO) == IDYES)
    {
      CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;

      while (pos)
      {
        int item = this->m_statusList.GetNextSelectedItem(pos);
        ANYVOD_CLIENT_STATUS *info = (ANYVOD_CLIENT_STATUS*)this->m_statusList.GetItemData(item);

        dlg->GetSocket().SendClientLogout(info->ip, info->port);

        if (this->ProcessPacket())
        {
          delete info;

          this->m_itemCount--;
          this->m_itemCurCount--;

          this->m_statusList.DeleteItem(item);

          pos = this->m_statusList.GetFirstSelectedItemPosition();

        }
        else
        {
          if (pos != NULL)
          {
            if (AfxMessageBox(L"접속자를 로그 아웃 하는 도중 오류가 발생 했습니다. 계속 하시겠습니까?", MB_YESNO) == IDNO)
            {
              break;

            }

          }

        }

      }

      this->UpdateTotal();

    }

    this->ToggleClientStatusItemControls();

  }
  else
  {
    AfxMessageBox(L"선택된 접속자가 없습니다.");

  }
}

void CClientStatusDlg::OnNMReturnListClientStatusStatuslist(NMHDR *pNMHDR, LRESULT *pResult)
{
  this->OnBnClickedButtonClientStatusLogout();

  *pResult = 0;
}

void CClientStatusDlg::OnLvnItemchangedListClientStatusStatuslist(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

  if (pNMLV->uOldState == 0)
  {
    this->ViewDetail();
    this->ToggleClientStatusItemControls();

  }

  *pResult = 0;
}
