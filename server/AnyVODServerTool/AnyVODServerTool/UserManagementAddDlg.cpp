/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

// UserManagementAddDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "AnyVODServerTool.h"
#include "UserManagementDlg.h"
#include "UserManagementAddDlg.h"
#include "AnyVODServerToolDlg.h"
#include "Socket.h"
#include "Util.h"

// CUserManagementAddDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CUserManagementAddDlg, CDialog)

CUserManagementAddDlg::CUserManagementAddDlg(CWnd* pParent /*=nullptr*/)
	: CParentDlg(CUserManagementAddDlg::IDD, pParent)
{

}

CUserManagementAddDlg::~CUserManagementAddDlg()
{
}

void CUserManagementAddDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_EDIT_USER_MANAGEMENT_ADD_ID, m_id);
  DDX_Control(pDX, IDC_EDIT_USER_MANAGEMENT_ADD_NAME, m_name);
  DDX_Control(pDX, IDC_CHECK_USER_MANAGEMENT_ADD_ADMIN, m_admin);
  DDX_Control(pDX, IDC_EDIT_USER_MANAGEMENT_ADD_PASSWORD, m_password);
  DDX_Control(pDX, IDC_EDIT_USER_MANAGEMENT_ADD_CONFIRM_PASSWORD, m_confirmPassword);
}


BEGIN_MESSAGE_MAP(CUserManagementAddDlg, CDialog)
  ON_BN_CLICKED(IDC_BUTTON_USER_MANAGEMENT_ADD_CONFIRM_EXIST, &CUserManagementAddDlg::OnBnClickedButtonUserManagementAddConfirmExist)
  ON_BN_CLICKED(IDC_BUTTON_USER_MANAGEMENT_ADD_ADD, &CUserManagementAddDlg::OnBnClickedButtonUserManagementAddAdd)
  ON_BN_CLICKED(IDC_BUTTON_USER_MANAGEMENT_ADD_CANCEL, &CUserManagementAddDlg::OnBnClickedButtonUserManagementAddCancel)
END_MESSAGE_MAP()

void CUserManagementAddDlg::InitControls()
{
  this->m_id.SetLimitText(MAX_ID_CHAR_SIZE);
  this->m_name.SetLimitText(MAX_NAME_CHAR_SIZE);
  this->m_password.SetLimitText(MAX_PASS_CHAR_SIZE);
  this->m_confirmPassword.SetLimitText(MAX_PASS_CHAR_SIZE);

}

bool CUserManagementAddDlg::HandlePacket(ANYVOD_PACKET *packet)
{
  ANYVOD_PACKET_TYPE type = (ANYVOD_PACKET_TYPE)ntohl(packet->s2c_header.header.type);

  if (type == PT_ADMIN_S2C_USER_ADD)
  {
    if (packet->s2c_header.success)
    {
      CUserManagementDlg *dlg = (CUserManagementDlg*)this->m_pParentWnd;
      ANYVOD_PACKET_USER_INFO info;
      CString id;
      CString pass;
      CString name;

      this->m_id.GetWindowText(id);
      this->m_password.GetWindowText(pass);
      this->m_name.GetWindowText(name);

      info.admin = this->m_admin.GetCheck() == BST_CHECKED;

      Util::ConvertUnicodeToUTF8(id.GetString(), (char*)info.id, MAX_ID_SIZE);
      Util::ConvertUnicodeToUTF8(pass.GetString(), (char*)info.pass, MAX_PASS_CHAR_SIZE);
      Util::ConvertUnicodeToUTF8(name.GetString(), (char*)info.name, MAX_NAME_CHAR_SIZE);

      dlg->AddUserToList(ntohll(packet->admin_s2c_user_add.pid), info);

      this->EndDialog(IDOK);

    }

  }
  else if (type == PT_ADMIN_S2C_USER_EXIST)
  {
    if (packet->s2c_header.success)
    {
      if (packet->admin_s2c_user_exist.exist)
      {
        AfxMessageBox(L"사용 불가능한 아이디 입니다.");

      }
      else
      {
        AfxMessageBox(L"사용 가능한 아이디 입니다.");

      }

    }

  }

  return false;

}

void CUserManagementAddDlg::ProcessPacket()
{
  bool nextProcess = false;
  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd->GetParent();
  Socket &socket = dlg->GetSocket();

  do
  {
    if (!Util::HandlePacket(socket, this, &nextProcess, nullptr))
    {
      this->EndDialog(IDCLOSE);

      break;

    }

  } while (nextProcess);

}

// CUserManagementAddDlg 메시지 처리기입니다.

BOOL CUserManagementAddDlg::PreTranslateMessage(MSG* pMsg)
{
  return CDialog::PreTranslateMessage(pMsg);
}

void CUserManagementAddDlg::OnOK()
{
  this->OnBnClickedButtonUserManagementAddAdd();

  //CParentDlg::OnOK();
}

void CUserManagementAddDlg::OnCancel()
{
  this->OnBnClickedButtonUserManagementAddCancel();

  CParentDlg::OnCancel();
}

void CUserManagementAddDlg::OnBnClickedButtonUserManagementAddConfirmExist()
{
  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd->GetParent();
  CString id;

  this->m_id.GetWindowText(id);

  id.Trim();

  if (id.GetLength() > 0)
  {
    dlg->GetSocket().SendUserExist(id);
    this->ProcessPacket();

  }
  else
  {
    AfxMessageBox(L"아이디를 입력해 주세요.");

    this->m_id.SetFocus();

  }

}

void CUserManagementAddDlg::OnBnClickedButtonUserManagementAddAdd()
{
  CString id;
  CString pass;
  CString confirmPass;
  CString name;

  this->m_id.GetWindowText(id);
  this->m_password.GetWindowText(pass);
  this->m_confirmPassword.GetWindowText(confirmPass);
  this->m_name.GetWindowText(name);

  id.Trim();
  pass.Trim();
  confirmPass.Trim();
  name.Trim();

  if (id.GetLength() == 0)
  {
    AfxMessageBox(L"아이디를 입력해 주세요.");

    this->m_id.SetFocus();

    return;

  }
  else if (name.GetLength() == 0)
  {
    AfxMessageBox(L"이름을 입력해 주세요.");

    this->m_name.SetFocus();

    return;


  }
  else if (pass.GetLength() == 0)
  {
    AfxMessageBox(L"암호를 입력해 주세요.");

    this->m_password.SetFocus();

    return;

  }
  else if (confirmPass.GetLength() == 0)
  {
    AfxMessageBox(L"암호 확인을 입력해 주세요.");

    this->m_confirmPassword.SetFocus();

    return;


  }
  else if (pass != confirmPass)
  {
    AfxMessageBox(L"암호를 확인해 주세요.");

    this->m_password.SetFocus();

    return;


  }

  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd->GetParent();
  ANYVOD_USER_INFO info;

  info.id = id.GetString();
  info.pass = pass.GetString();
  info.name = name.GetString();

  info.admin = this->m_admin.GetCheck() == BST_CHECKED;

  dlg->GetSocket().SendUserAdd(info);
  this->ProcessPacket();

}

void CUserManagementAddDlg::OnBnClickedButtonUserManagementAddCancel()
{
  this->EndDialog(IDCANCEL);

}

