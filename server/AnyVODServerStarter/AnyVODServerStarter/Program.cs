﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;

namespace AnyVODServerStarter
{
  internal class Program
  {
    private static void PrintUsage()
    {
      Console.WriteLine("Usage: {0} OPTIONS LOCATION [EMAIL SMTP PORT ID PASSWORD STARTTLS]", Process.GetCurrentProcess().ProcessName + ".exe");
      Console.WriteLine("");
      Console.WriteLine("OPTIONS  : Arguments that pass to server process.");
      Console.WriteLine("           Currently available options are \"start\" or \"\"(empty)");
      Console.WriteLine("LOCATION : Path of server executable.(relative path or absolute path)");
      Console.WriteLine("EMAIL    : Email address that is used to report crashing logs of server process.");
      Console.WriteLine("SMTP     : SMTP server address that is used to report crashing logs of server process.");
      Console.WriteLine("PORT     : SMTP server port that is used to report crashing logs of server process.");
      Console.WriteLine("ID       : Account of email that is used to report crashing logs of server process.");
      Console.WriteLine("PASSWORD : Password of email that is used to report crashing logs of server process.");
      Console.WriteLine("STARTTLS : Use StartTLS(\"true\" or \"false\")");

    }

    private static void Main(string[] args)
    {
      //옵션, 위치, 이메일
      if (args.Length < 2)
      {
        PrintUsage();

      }
      else
      {
        Assembly assembly = Assembly.GetExecutingAssembly();
        DateTime buildDate = new FileInfo(assembly.Location).LastWriteTime;

        Console.WriteLine(" AnyVOD Server Starter v" + assembly.GetName().Version + " " + FileVersionInfo.GetVersionInfo(assembly.Location).ProductVersion);
        Console.WriteLine(" Built on " + buildDate.ToShortDateString() + " at " + buildDate.ToLongTimeString());
        Console.WriteLine(" Author : DongRyeol Cha (chadr@dcple.com)");
        Console.WriteLine("-----------------------------------------");
        Console.WriteLine("To exit, type \"exit\"");

        Invoker invoker = new Invoker();

        invoker.Options = args[0];
        invoker.Location = args[1];

        if (args.Length > 2)
        {
          invoker.Email = args[2];
          invoker.SMTP = args[3];
          invoker.Port = Int32.Parse(args[4]);
          invoker.ID = args[5];
          invoker.Password = args[6];
          invoker.UseStartTLS = bool.Parse(args[7]);

        }

        Thread thread = new Thread(new ThreadStart(invoker.DoWork));

        thread.Start();

        string cmd;

        while (true)
        {
          cmd = Console.ReadLine();

          if (cmd == "exit")
          {
            invoker.RequestStop();

            thread.Join();

            break;

          }
          else
          {
            Console.WriteLine(">> Invalid command");

          }

          Console.Write("> ");

        }

      }

    }

  }

}
