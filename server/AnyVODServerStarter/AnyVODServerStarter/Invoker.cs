﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;

namespace AnyVODServerStarter
{
  internal class Invoker
  {
    private volatile bool m_continue = true;
    private string m_options = string.Empty;
    private string m_location = string.Empty;
    private string m_email = string.Empty;
    private string m_smtp = string.Empty;
    private int m_port = 0;
    private string m_id = string.Empty;
    private string m_password = string.Empty;
    private bool m_useStartTLS = false;

    public string Options
    {
      get { return this.m_options; }
      set { this.m_options = value; }

    }

    public string Location
    {
      get { return this.m_location; }
      set { this.m_location = value; }

    }

    public string Email
    {
      get { return this.m_email; }
      set { this.m_email = value; }

    }

    public string SMTP
    {
      get { return this.m_smtp; }
      set { this.m_smtp = value; }

    }

    public int Port
    {
      get { return this.m_port; }
      set { this.m_port = value; }

    }

    public string ID
    {
      get { return this.m_id; }
      set { this.m_id = value; }

    }

    public string Password
    {
      get { return this.m_password; }
      set { this.m_password = value; }

    }

    public bool UseStartTLS
    {
      get { return this.m_useStartTLS; }
      set { this.m_useStartTLS = value; }

    }

    public void RequestStop()
    {
      this.m_continue = false;

    }

    public void DoWork()
    {
      while (this.m_continue)
      {
        Process process = null;

        try
        {
          Process[] servers = Process.GetProcessesByName(Path.GetFileNameWithoutExtension(this.m_location));

          if (servers != null && servers.Length > 0)
          {
            Console.WriteLine("\n>> Server process already exist");
            Console.WriteLine(">> Connect to process");

            process = servers[0];

            if (process.Handle == (IntPtr)0)
            {
              Console.WriteLine(">> Invalid process handle");

              Process.GetCurrentProcess().Kill();

              return;

            }

            Console.WriteLine(">> Process handle 0x{0:x}", process.Handle);
            Console.Write("> ");

          }
          else
          {
            ProcessStartInfo sinfo = new ProcessStartInfo();

            sinfo.FileName = this.m_location;
            sinfo.Arguments = this.m_options;
            sinfo.WorkingDirectory = Path.GetDirectoryName(this.m_location);

            if (sinfo.WorkingDirectory == String.Empty)
              sinfo.WorkingDirectory = "." + Path.DirectorySeparatorChar;

            Console.WriteLine("\n>> Server process start");

            process = Process.Start(sinfo);

            Console.WriteLine(">> Server process started");
            Console.Write("> ");

          }

          while (this.m_continue)
          {
            if (process.WaitForExit(1))
            {
              //종료값을 검사해서 비정상 종료이면 재시작을 한다.
              try
              {
                if (process.ExitCode == 0) //정상종료
                {
                  Console.WriteLine("\n>> Server process has terminated gracefully");

                  Process.GetCurrentProcess().Kill();

                  return;

                }
                else
                {
                  long reason = 0;

                  if (this.IsNormalExitCode(process.ExitCode, ref reason))
                  {
                    Console.WriteLine("\n>> Server process has terminated normally with reason : 0x{0:x}", reason);
                    Console.WriteLine(">> Waiting 5 seconds until exit");

                    Thread.Sleep(5000);
                    Process.GetCurrentProcess().Kill();

                    return;

                  }
                  else
                  {
                    Console.WriteLine("\n>> Server process has terminated abnormally : {0:d}", process.ExitCode);

                    //비정상 종료
                    this.SendEmail(process);

                  }

                  break;

                }

              }
              catch (System.Exception)
              {
                Console.WriteLine("\n>> Server process has terminated abnormally : {0:d}", process.ExitCode);

                //비정상 종료
                this.SendEmail(process);

                break;

              }

            }

          }

        }
        catch (System.Exception ex)
        {
          Console.WriteLine("\n>> " + ex.Message);

          Process.GetCurrentProcess().Kill();

          return;

        }

      }

    }

    private bool IsNormalExitCode(int exitCode, ref long reason)
    {
      uint mask = 0xc0000000;
      uint reasonMask = 0xfff;

      if ((exitCode & mask) == mask)
      {
        reason = exitCode & reasonMask;

        return true;

      }

      return false;

    }

    private void SendEmail(Process process)
    {
      Console.WriteLine("\n>> Server process has crashed");

      if (this.m_email == string.Empty)
      {
        Console.WriteLine(">> But because of email address is not setted, Logs will be not reported");

      }
      else
      {
        Console.WriteLine(">> Sending Email to {0}", this.m_email);

        try
        {
          string dir = process.StartInfo.WorkingDirectory;
          string[] dumpFiles = Directory.GetFiles(dir, "*.dmp");
          string[] logFiles = Directory.GetFiles(dir, "*.log");

          Array.Sort(dumpFiles);
          Array.Sort(logFiles);

          //관리자에게 메일 발송
          MailMessage msg = new MailMessage(new MailAddress("anyvod_reporter@anyvod"), new MailAddress(this.m_email));

          msg.Subject = "AnyVOD Server crashed!!";
          msg.Body = "Crashed at " + DateTime.Now.ToString(CultureInfo.GetCultureInfoByIetfLanguageTag("en-US").DateTimeFormat) + "\r\n";
          msg.Body += "Exit Code : " + process.ExitCode.ToString() + " (0x" + process.ExitCode.ToString("x") + ")\r\n";

          if (dumpFiles.LongLength > 0)
          {
            msg.Body += "Dump files are attached\r\n";
            msg.Attachments.Add(new Attachment(dumpFiles[dumpFiles.LongLength - 1], MediaTypeNames.Application.Octet));

          }
          else
          {
            msg.Body += "Dump files are not attached\r\n";

          }

          if (logFiles.LongLength > 0)
          {
            msg.Body += "Log files are attached\r\n";
            msg.Attachments.Add(new Attachment(logFiles[logFiles.LongLength - 1], MediaTypeNames.Text.Plain));

          }
          else
          {
            msg.Body += "Log files are not attached\r\n";

          }

          SmtpClient client = new SmtpClient(this.m_smtp, this.m_port);

          client.Credentials = new NetworkCredential(this.m_id, this.m_password);
          client.EnableSsl = this.m_useStartTLS;

          client.Send(msg);

          Console.WriteLine(">> Email has sent to {0}", this.m_email);

        }
        catch (System.Exception ex)
        {
          Console.WriteLine(">> " + ex.Message);

        }

      }

      Console.Write("> ");

    }

  }

}
