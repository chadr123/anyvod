/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "stdafx.h"
#include "StatementCache.h"
#include "AutoLocker.h"
#include "Common.h"

StatementCache::StatementCache()
:m_maxCount(100)
{
  InitializeCriticalSectionAndSpinCount(&this->m_locker, MAX_SPIN_COUNT);

}

StatementCache::~StatementCache()
{
  this->Deinit();

  DeleteCriticalSection(&this->m_locker);

}

void
StatementCache::Init(CppSQLite3DB *db)
{
  this->m_db = db;

}

void
StatementCache::Deinit()
{
  MAP_STRING_VECTOR_STATEMENT_ITEM_ITERATOR i = this->m_container.begin();

  for (; i != this->m_container.end(); i++)
  {
    VECTOR_STATEMENT_ITEM *item = i->second;
    VECTOR_STATEMENT_ITEM_ITERATOR j = item->begin();

    for (; j != item->end(); j++)
    {
      STATEMENT_ITEM *statement = *j;

      if (statement == nullptr)
        continue;

      DeleteCriticalSection(&statement->locker);

      delete statement;

    }

    item->clear();

    delete item;

  }

  this->m_container.clear();

}

void
StatementCache::GetStatementStatus(VECTOR_ANYVOD_STATEMENT_STATUS *ret)
{
  AutoLocker locker(&this->m_locker);

  MAP_STRING_VECTOR_STATEMENT_ITEM_ITERATOR i = this->m_container.begin();

  for (; i != this->m_container.end(); i++)
  {
    VECTOR_STATEMENT_ITEM *item = i->second;
    VECTOR_STATEMENT_ITEM_ITERATOR j = item->begin();
    ANYVOD_STATEMENT_STATUS status;

    status.sql = i->first;
    status.totalCount = (unsigned int)item->size();
    status.usingCount = 0;

    for (; j != item->end(); j++)
    {
      STATEMENT_ITEM *statement = *j;

      if (TryEnterCriticalSection(&statement->locker))
      {
        LeaveCriticalSection(&statement->locker);

      }
      else
      {
        status.usingCount++;

      }

    }

    ret->push_back(status);

  }

}

StatementCache::STATEMENT_ITEM*
StatementCache::GetStatement(const string &sql)
{
  AutoLocker locker(&this->m_locker);

  MAP_STRING_VECTOR_STATEMENT_ITEM_ITERATOR i = this->m_container.find(sql);

  if (i == this->m_container.end())
  {
    //없음. 새로 만든다.
    VECTOR_STATEMENT_ITEM *item = new VECTOR_STATEMENT_ITEM;
    STATEMENT_ITEM *statement = new STATEMENT_ITEM;

    InitializeCriticalSectionAndSpinCount(&statement->locker, MAX_SPIN_COUNT);

    try
    {
      statement->statement = this->m_db->compileStatement(sql.c_str());

      item->push_back(statement);
      this->m_container.insert(make_pair(sql, item));

      EnterCriticalSection(&statement->locker);

      return statement;

    }
    catch (CppSQLite3Exception &e)
    {
      delete item;
      delete statement;

      throw e;

      return nullptr;

    }

  }
  else
  {
    VECTOR_STATEMENT_ITEM *item = i->second;
    VECTOR_STATEMENT_ITEM_ITERATOR j = item->begin();

    for (; j != item->end(); j++)
    {
      STATEMENT_ITEM *statement = *j;

      if (TryEnterCriticalSection(&statement->locker))
      {
        return statement;

      }

    }

    //여기까지 오면 가용 statement가 없다. 새로 만든다.
    if ((int)item->size() >= this->m_maxCount)
    {
      //최대 용량을 초과 했다. 아무거나 락 걸고 기다린다.
      STATEMENT_ITEM *statement = item->at(rand() % item->size());

      EnterCriticalSection(&statement->locker);

      return statement;

    }
    else
    {
      //새로 만들어서 넣고 락 걸어서 리턴 한다.
      STATEMENT_ITEM *statement = new STATEMENT_ITEM;

      InitializeCriticalSectionAndSpinCount(&statement->locker, MAX_SPIN_COUNT);
      statement->statement = this->m_db->compileStatement(sql.c_str());

      item->push_back(statement);

      EnterCriticalSection(&statement->locker);

      return statement;

    }

  }

}

void
StatementCache::ReleaseStatement(STATEMENT_ITEM *item)
{
  item->reset();

  LeaveCriticalSection(&item->locker);

}

void
StatementCache::SetMaxItemCount(int count)
{
  this->m_maxCount = count;

}

int
StatementCache::GetMaxItemCount() const
{
  return this->m_maxCount;

}
