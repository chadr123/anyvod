/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "stdafx.h"
#include "ScheduleInvoker.h"
#include "Server.h"
#include "Log.h"

ScheduleInvoker::ScheduleInvoker()
:m_server(nullptr)
{
  memset(&this->m_startedTime, 0x00, sizeof(this->m_startedTime));
  memset(&this->m_endedTime, 0x00, sizeof(this->m_endedTime));

}

ScheduleInvoker::~ScheduleInvoker()
{

}

void
ScheduleInvoker::SetServer(Server *server)
{
  this->m_server = server;

}

void
ScheduleInvoker::SetName(const wstring &name)
{
  this->m_name = name;

}

void
ScheduleInvoker::GetStartedTime(SYSTEMTIME *time) const
{
  *time = this->m_startedTime;

}

void
ScheduleInvoker::GetEndedTime(SYSTEMTIME *time) const
{
  *time = this->m_endedTime;

}

void
ScheduleInvoker::SetStartedTime(const SYSTEMTIME &time)
{
  this->m_startedTime = time;

}

void
ScheduleInvoker::SetEndedTime(const SYSTEMTIME &time)
{
  this->m_endedTime = time;

}

bool
ScheduleInvoker::Run()
{
  Log::GetInstance().Write(L"Schedule started : %s", this->m_name.c_str());

  GetLocalTime(&this->m_startedTime);

  this->Invoke(this->m_server);

  GetLocalTime(&this->m_endedTime);

  Log::GetInstance().Write(L"Schedule ended : %s", this->m_name.c_str());

  return false;

}
