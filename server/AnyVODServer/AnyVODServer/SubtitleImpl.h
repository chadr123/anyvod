/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <stdint.h>
#include "..\..\..\common\RetreiveSubtitle.h"

class SubtitleImpl : public RetreiveSubtitle
{
public:
  SubtitleImpl();

protected:
  virtual FILE* openFile(const wstring &filePath);
  virtual bool retreiveFromAlSongServer(const string &data, wstring *ret);
  virtual bool retreiveFromGOMServer(const string &md5, wstring *ret);
  virtual void getMD5(uint8_t *buf, size_t size, string *ret);

private:
  bool retreiveFromServer(const string &address, const string &url, const string &agent,
    const string &method, const string &header, const string &data, DWORD flag, wstring *ret);

};
