/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "stdafx.h"
#include "Log.h"
#include "AutoLocker.h"
#include "Common.h"
#include "Util.h"
#include "FindHandleCloser.h"

const int Log::MAX_LOG_BUFFER_SIZE = 1024;
Log Log::instance;

Log::Log()
:m_ofs(nullptr)
{
  InitializeCriticalSectionAndSpinCount(&this->m_lock, MAX_SPIN_COUNT);

}

Log::~Log()
{
  DeleteCriticalSection(&this->m_lock);

}

Log&
Log::GetInstance()
{
  return instance;

}

bool
Log::RestartLog(const wstring &filePath)
{
  AutoLocker locker(&this->m_lock);

  this->EndLog();

  return this->StartLog(filePath);

}

bool
Log::StartLog(const wstring &filePath)
{
  AutoLocker locker(&this->m_lock);
  wstring path;

  Util::SplitFilePath(filePath, &path, &wstring());

  if (!Util::CreateDirectories(path))
    return false;

  if (this->m_ofs == nullptr)
  {
    this->m_ofs = _wfopen(filePath.c_str(), L"a, ccs=UTF-8");

    if (this->m_ofs != nullptr)
    {
      DWORD size = 0;

      GetComputerNameExW(ComputerNamePhysicalDnsFullyQualified, nullptr, &size);

      wchar_t *buf = new wchar_t[size+1];

      memset(buf, 0x00, sizeof(char)*(size+1));

      GetComputerNameExW(ComputerNamePhysicalDnsFullyQualified, buf, &size);

      wstring time;

      this->GetTime(&time);

      fwprintf(this->m_ofs, L"%s\n", L"#---------------------------------------------------------------------");
      fwprintf(this->m_ofs, L"%s%s\n", L"# Logging started at ", time.c_str());
      fwprintf(this->m_ofs, L"%s%s\n", L"# System name : ", buf);
      fwprintf(this->m_ofs, L"%s\n", L"#---------------------------------------------------------------------");

      delete[] buf;

      this->m_fileName = filePath.substr(filePath.find_last_of(DIRECTORY_DELIMITER)+1);

      return true;

    }

  }

  return false;

}

void
Log::EndLog()
{
  AutoLocker locker(&this->m_lock);

  if (this->m_ofs != nullptr)
  {
    wstring time;

    this->GetTime(&time);

    fwprintf(this->m_ofs, L"%s\n", L"#---------------------------------------------------------------------");
    fwprintf(this->m_ofs, L"%s%s\n", L"# Logging ended at ", time.c_str());
    fwprintf(this->m_ofs, L"%s\n", L"#---------------------------------------------------------------------");

    fclose(this->m_ofs);
    this->m_ofs = nullptr;

  }

}

void
Log::GetFileName(wstring *ret)
{
  AutoLocker locker(&this->m_lock);

  *ret = this->m_fileName;

}

void
Log::GetNewLogDate(wstring *ret)
{
  SYSTEMTIME time;
  wstringstream timeString;

  GetLocalTime(&time);

  timeString << L"_" << time.wYear << L"-" << setw(2) << setfill(L'0') <<
    time.wMonth << L"-" << setw(2) << setfill(L'0') <<
    time.wDay << L"_" << setw(2) << setfill(L'0') <<
    time.wHour << L"-" << setw(2) << setfill(L'0') <<
    time.wMinute << L"-" << setw(2) << setfill(L'0') <<
    time.wSecond << L"." << setw(4) << setfill(L'0') <<
    time.wMilliseconds;

  *ret = timeString.str();

}

bool
Log::DeleteLog(const wstring &root, const wstring &fileName)
{
  wstring path = root;

  Util::AppendDirSeparator(&path);

  path += fileName;

  if (DeleteFileW(path.c_str()))
  {
    return true;

  }
  else
  {
    return false;

  }

}

bool
Log::GetLogNames(const wstring &root, VECTOR_FILE_ITEM *ret)
{
  wstring pattern = root;
  bool success = false;

  Util::AppendDirSeparator(&pattern);

  pattern += LOG_FILENAME_PREFIX;
  pattern += L"*";
  pattern += LOG_FILENAME_EXTENTION;

  WIN32_FIND_DATAW findData;
  HANDLE find = FindFirstFileW(pattern.c_str(), &findData);
  FindHandleCloser closer(find);

  if (find == INVALID_HANDLE_VALUE)
  {
    success = false;

  }
  else
  {
    do
    {
      if (IS_BIT_SET(findData.dwFileAttributes, FILE_ATTRIBUTE_DIRECTORY))
      {
        continue;

      }

      FILE_ITEM item;
      ULARGE_INTEGER fileSize;

      fileSize.HighPart = findData.nFileSizeHigh;
      fileSize.LowPart = findData.nFileSizeLow;

      item.fileName = findData.cFileName;
      item.fileSize = fileSize.QuadPart;

      ret->push_back(item);

    } while (FindNextFileW(find, &findData));

    if (GetLastError() != ERROR_NO_MORE_FILES)
    {
      success = false;

      ret->clear();

    }
    else
    {
      success = true;

    }

  }

  return success;

}

void
Log::GetTime(wstring *ret)
{
  SYSTEMTIME time;
  wstringstream timeString;

  GetLocalTime(&time);

  timeString << L"[" << time.wYear << L"/" << setw(2) << setfill(L'0') <<
    time.wMonth << L"/" << setw(2) << setfill(L'0') <<
    time.wDay << L" " << setw(2) << setfill(L'0') <<
    time.wHour << L":" << setw(2) << setfill(L'0') <<
    time.wMinute << L":" << setw(2) << setfill(L'0') <<
    time.wSecond << L"." << setw(4) << setfill(L'0') <<
    time.wMilliseconds << L"]";

  *ret = timeString.str();

}

wstring
Log::Write(const wchar_t* szFmt, ...)
{
  AutoLocker locker(&this->m_lock);

  wchar_t buf[MAX_LOG_BUFFER_SIZE];
  va_list vList;

  va_start(vList, szFmt);
  vswprintf(buf, MAX_LOG_BUFFER_SIZE-1, szFmt, vList);
  va_end(vList);

  if (this->m_ofs != nullptr)
  {
    wstring time;

    this->GetTime(&time);

    fwprintf(this->m_ofs, L"%s %s\n", time.c_str(), buf);

  }

  return wstring(buf);

}

void
Log::Flush()
{
  if (this->m_ofs != nullptr)
  {
    fflush(this->m_ofs);

  }

}
