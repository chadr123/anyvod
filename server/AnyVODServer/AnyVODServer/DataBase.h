/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "StatementCache.h"
#include "..\..\..\common\CppSQLite3.h"

class Server;

class DataBase
{
public:
  struct MOVIE_INFO
  {
    MOVIE_INFO()
    {
      totalSize = 0;
      totalFrame = 0;
      bitRate = 0;
      totalTime = 0;

    }

    long long totalSize;
    int totalFrame;
    int bitRate;
    int totalTime;
    wstring movieType;
    wstring codecVideo;
    wstring title;
    VECTOR_ANYVOD_ANALYSED_DATA_BLOCK metaDataBlocks;

  };

  enum DATABASE_FAIL_REASON
  {
    DFR_NONE,
    DFR_ALREADY_EXIST,
    DFR_NOT_FOUND,
    DFR_DATABASE_ERROR,
    DFR_ASSOCIATED_USER_GROUP,

  };

  DataBase();
  ~DataBase();

  static ERROR_TYPE FailResonToErrorType(DATABASE_FAIL_REASON reason);

  StatementCache& GetStatementCache();
  void GetPassword(const wstring &pass, wstring *ret) const;

  bool Open(const wstring &fileName, Server *server);
  void Close();
  bool Vacuum();

  bool DeleteMovieInfo(const wstring &filePath, DATABASE_FAIL_REASON *reason);
  bool GetMovieInfo(const wstring &filePath, MOVIE_INFO *info, DATABASE_FAIL_REASON *reason);
  bool UpdateMovieInfo(const wstring &root);
  bool RebuildMovieMetaData();
  bool RebuildMovieData();

  unsigned long long AddUser(const ANYVOD_USER_INFO &info, bool useTransaction, bool *success, DATABASE_FAIL_REASON *reason);
  bool DeleteUser(unsigned long long pid, DATABASE_FAIL_REASON *reason);
  bool ModifyUser(const ANYVOD_USER_INFO &info, DATABASE_FAIL_REASON *reason);
  bool GetUserInfos(VECTOR_ANYVOD_USER_INFO *ret, DATABASE_FAIL_REASON *reason);
  bool GetUserInfo(unsigned long long pid, ANYVOD_USER_INFO *ret, DATABASE_FAIL_REASON *reason);
  bool FindUser(const wstring &id, ANYVOD_USER_INFO *ret, DATABASE_FAIL_REASON *reason);
  bool ExistUser(const wstring &id, DATABASE_FAIL_REASON *reason);

  unsigned long long AddAdmin(unsigned long long userPID, bool useTransaction, bool *success, DATABASE_FAIL_REASON *reason);
  bool DeleteAdmin(unsigned long long userPID, DATABASE_FAIL_REASON *reason);
  bool IsAdmin(const wstring &userID, DATABASE_FAIL_REASON *reason);
  bool IsAdmin(unsigned long long userPID, DATABASE_FAIL_REASON *reason);

  unsigned long long AddGroup(const wstring &name, bool useTransaction, bool *success, DATABASE_FAIL_REASON *reason);
  bool DeleteGroup(const wstring &name, DATABASE_FAIL_REASON *reason);
  bool DeleteGroupWithAll(unsigned long long pid, DATABASE_FAIL_REASON *reason);
  bool ExistGroup(const wstring &name, DATABASE_FAIL_REASON *reason);
  bool ExistGroupSameUser(const wstring &name, DATABASE_FAIL_REASON *reason);
  bool ModifyGroup(const ANYVOD_GROUP_INFO &info, DATABASE_FAIL_REASON *reason);
  bool GetGroupInfos(VECTOR_ANYVOD_GROUP_INFO *ret, DATABASE_FAIL_REASON *reason);
  bool GetGroupInfo(unsigned long long pid, ANYVOD_GROUP_INFO *ret, DATABASE_FAIL_REASON *reason);

  unsigned long long AddUserToGroup(unsigned long long userPID, const wstring &groupName,
    bool useTransaction, bool *success, DATABASE_FAIL_REASON *reason);
  bool DeleteUserFromGroup(unsigned long long userPID, const wstring &groupName, DATABASE_FAIL_REASON *reason);
  bool DeleteUserGroup(const wstring &groupName, DATABASE_FAIL_REASON *reason);
  bool DeleteUserGroup(unsigned long long pid, DATABASE_FAIL_REASON *reason);
  bool ExistUserGroup(unsigned long long userPID, const wstring &groupName, DATABASE_FAIL_REASON *reason);
  bool GetUserGroupInfos(VECTOR_ANYVOD_USER_GROUP_INFO *ret, DATABASE_FAIL_REASON *reason);
  bool GetUserGroupInfo(unsigned long long pid, ANYVOD_USER_GROUP_INFO *ret, DATABASE_FAIL_REASON *reason);

  unsigned long long AddPermissionToUserGroup(const wstring &filePath, const wstring &groupName,
    bool useTransaction, bool *success, DATABASE_FAIL_REASON *reason);
  bool DeletePermissionFromUserGroup(const wstring &filePath, const wstring &groupName, DATABASE_FAIL_REASON *reason);
  bool DeletePermissionFromUserGroup(const wstring &groupName, DATABASE_FAIL_REASON *reason);
  bool DeletePermissionFromUserGroup(unsigned long long pid, DATABASE_FAIL_REASON *reason);
  bool ExistFileUserGroup(const wstring &filePath, const wstring &groupName, DATABASE_FAIL_REASON *reason);
  bool HasPermission(const wstring &filePath, const wstring &userID, DATABASE_FAIL_REASON *reason);
  bool HasDirectoryPermission(const wstring &path, const wstring &userID, DATABASE_FAIL_REASON *reason);
  bool GetFileGroupInfos(const wstring &filePath, VECTOR_ANYVOD_FILE_GROUP_INFO *ret, DATABASE_FAIL_REASON *reason);

private:
  bool UpdateMovieInfoInternal(const wstring &startPath, int tabSize);
  bool InstallDB();
  void GetUserInfoFromReader(CppSQLite3Query &reader, ANYVOD_USER_INFO *ret);
  void GetGroupInfoFromReader(CppSQLite3Query &reader, ANYVOD_GROUP_INFO *ret);
  void GetUserGroupInfoFromReader(CppSQLite3Query &reader, ANYVOD_USER_GROUP_INFO *ret);
  void GetFileGroupInfoFromReader(CppSQLite3Query &reader, ANYVOD_FILE_GROUP_INFO *ret);
  void GetMovieInfoFromReader(CppSQLite3Query &reader, MOVIE_INFO *ret);
  void GetMovieMetaFromReader(CppSQLite3Query &reader, ANYVOD_ANALYSED_DATA_BLOCK *ret);
  unsigned long long GetNextPIDValue(bool *success, DATABASE_FAIL_REASON *reason);
  bool DeterminePath(const wstring &filePath, vector_wstring *ret);

private:
  CppSQLite3DB m_db;
  StatementCache m_statCache;
  Server *m_server;
  CRITICAL_SECTION m_nextPIDLocker;

};
