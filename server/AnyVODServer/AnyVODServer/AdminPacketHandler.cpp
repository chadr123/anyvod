/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "stdafx.h"
#include "AdminPacketHandler.h"
#include "PacketHandler.h"
#include "Client.h"
#include "Server.h"
#include "DataBase.h"
#include "Util.h"
#include "ScheduleInvokerFactory.h"
#include "SubtitleImpl.h"
#include "..\..\..\common\SubtitleFileNameGenerator.h"
#include "..\..\..\common\packets.h"

AdminPacketHandler::AdminPacketHandler(Client *client, PacketHandler *parent)
:m_client(client), m_parent(parent)
{

}

AdminPacketHandler::~AdminPacketHandler()
{

}

bool
AdminPacketHandler::HandlePacket(const ANYVOD_PACKET &packet)
{
  bool success = true;
  ANYVOD_PACKET_TYPE type = (ANYVOD_PACKET_TYPE)ntohl(packet.c2s_header.header.type);

  switch (type)
  {
  case PT_ADMIN_C2S_NETWORK_STATISTICS:
    {
      success = this->SendNetworkStatistics(true);

      Log::GetInstance().Write(L"Send network statistics (IP : %s, Port : %hu, Socket : %u)",
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;

    }
  case PT_ADMIN_C2S_LASTLOG:
    {
      wstring fileName;
      wstring logPath = this->m_client->GetServer()->GetEnv().log_dir;

      Util::AppendDirSeparator(&logPath);
      Log::GetInstance().GetFileName(&fileName);
      logPath.append(fileName);

      Log::GetInstance().Flush();

      ifstream ifs;

      ifs.open(logPath.c_str(), ios::binary);

      if (ifs.is_open())
      {
        success = this->SendLastLog(true, ifs, fileName);

      }
      else
      {
        success = this->SendLastLog(false, ifs, fileName) && this->m_parent->SendError(ET_NOT_EXIST_LOG);

      }

      Log::GetInstance().Write(L"Send last log (IP : %s, Port : %hu, Socket : %u)",
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;

    }
  case PT_ADMIN_C2S_ENV:
    {
      wstring envPath = INIT_FILE_NAME;
      ifstream ifs;

      ifs.open(envPath.c_str(), ios::binary);

      if (ifs.is_open())
      {
        success = this->SendEnv(true, ifs, envPath);

      }
      else
      {
        success = this->SendEnv(false, ifs, envPath) && this->m_parent->SendError(ET_NOT_EXIST_ENV);

      }

      Log::GetInstance().Write(L"Send env (IP : %s, Port : %hu, Socket : %u)",
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;

    }
  case PT_ADMIN_C2S_SCHEDULELIST:
    {
      if (this->m_client->GetServer()->GetScheduler().IsRun())
      {
        success = this->SendScheduleList(true);

      }
      else
      {
        success = this->SendScheduleList(false) && this->m_parent->SendError(ET_SCHEDULER_NOT_RUNNING);

      }

      Log::GetInstance().Write(L"Send schedule list (IP : %s, Port : %hu, Socket : %u)",
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;

    }
  case PT_ADMIN_C2S_RESTART_SCHEDULER:
    {
      Log::GetInstance().Write(L"Restart scheduler is requested (IP : %s, Port : %hu, Socket : %u)",
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      success = this->SendRestartScheduler();

      Log::GetInstance().Write(L"Send restart scheduler complete (IP : %s, Port : %hu, Socket : %u)",
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;

    }
  case PT_ADMIN_C2S_SCHEDULE_STARTNOW:
    {
      unsigned int index = ntohl(packet.admin_c2s_schedule_startnow.index);

      Log::GetInstance().Write(L"Schedule startnow is requested : %d (IP : %s, Port : %hu, Socket : %u)",
        index,
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      success = this->SendScheduleStartNow(index);

      Log::GetInstance().Write(L"Send schedule startnow : %d (IP : %s, Port : %hu, Socket : %u)",
        index,
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;
    }
  case PT_ADMIN_C2S_SCHEDULE_TYPES:
    {
      success = this->SendScheduleTypes();

      Log::GetInstance().Write(L"Send schedule types (IP : %s, Port : %hu, Socket : %u)",
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;
    }
  case PT_ADMIN_C2S_SCHEDULE_DELETE:
    {
      unsigned int index = ntohl(packet.admin_c2s_schedule_delete.index);

      Log::GetInstance().Write(L"Schedule delete is requested : %d (IP : %s, Port : %hu, Socket : %u)",
        index,
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      success = this->SendScheduleDelete(index);

      Log::GetInstance().Write(L"Send schedule delete : %d (IP : %s, Port : %hu, Socket : %u)",
        index,
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;
    }
  case PT_ADMIN_C2S_SCHEDULE_ADD:
    {
      char script[MAX_SCHEDULE_SCRIPT_SIZE];

      strncpy_s(script, MAX_SCHEDULE_SCRIPT_SIZE, (char*)packet.admin_c2s_schedule_add.script, MAX_SCHEDULE_SCRIPT_CHAR_SIZE);

      Log::GetInstance().Write(L"Schedule add is requested (IP : %s, Port : %hu, Socket : %u)",
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      wstring tmp;

      Util::ConvertAsciiToUnicode(script, &tmp);
      success = this->SendScheduleAdd(tmp);

      Log::GetInstance().Write(L"Send schedule add (IP : %s, Port : %hu, Socket : %u)",
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;
    }
  case PT_ADMIN_C2S_SCHEDULE_MODIFY:
    {
      char script[MAX_SCHEDULE_SCRIPT_SIZE];
      unsigned int index = ntohl(packet.admin_c2s_schedule_modify.index);

      strncpy_s(script, MAX_SCHEDULE_SCRIPT_SIZE, (char*)packet.admin_c2s_schedule_modify.script, MAX_SCHEDULE_SCRIPT_CHAR_SIZE);

      Log::GetInstance().Write(L"Schedule modify is requested : %d (IP : %s, Port : %hu, Socket : %u)",
        index,
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      wstring tmp;

      Util::ConvertAsciiToUnicode(script, &tmp);
      success = this->SendScheduleModify(index, tmp);

      Log::GetInstance().Write(L"Send schedule modify : %d (IP : %s, Port : %hu, Socket : %u)",
        index,
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;
    }
  case PT_ADMIN_C2S_LOGLIST:
    {
      success = this->SendLogList();

      Log::GetInstance().Write(L"Send log list (IP : %s, Port : %hu, Socket : %u)",
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;
    }
  case PT_ADMIN_C2S_LOG_DELETE:
    {
      wstring fileName;
      wstring curLog;

      Log::GetInstance().GetFileName(&curLog);

      Util::ConvertUTF8ToUnicode((char*)packet.admin_c2s_log_delete.fileName, MAX_PATH_SIZE, &fileName);

      if (fileName == curLog)
      {
        success = this->SendLogDelete(false) && this->m_parent->SendError(ET_CANNOT_DELETE_CURRENT_LOG);

      }
      else
      {
        if (Log::DeleteLog(this->m_client->GetServer()->GetEnv().log_dir, fileName))
        {
          success = this->SendLogDelete(true);

        }
        else
        {
          success = this->SendLogDelete(false) && this->m_parent->SendError(ET_CANNOT_DELETE_FILE);

        }

      }

      Log::GetInstance().Write(L"Send log delete : %s (IP : %s, Port : %hu, Socket : %u)",
        fileName.c_str(),
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;
    }
  case PT_ADMIN_C2S_LOG_VIEW:
    {
      wstring fileName;
      wstring logPath = this->m_client->GetServer()->GetEnv().log_dir;

      Util::AppendDirSeparator(&logPath);
      Util::ConvertUTF8ToUnicode((char*)packet.admin_c2s_log_view.fileName, MAX_PATH_SIZE, &fileName);
      logPath.append(fileName);

      Log::GetInstance().Flush();

      ifstream ifs;

      ifs.open(logPath.c_str(), ios::binary);

      if (ifs.is_open())
      {
        success = this->SendLogView(true, ifs, fileName);

      }
      else
      {
        success = this->SendLogView(false, ifs, fileName) && this->m_parent->SendError(ET_CANNOT_OPEN_FILE);

      }

      Log::GetInstance().Write(L"Send log view : %s (IP : %s, Port : %hu, Socket : %u)",
        fileName.c_str(),
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;

    }
  case PT_ADMIN_C2S_USERLIST:
    {
      success = this->SendUserList();

      Log::GetInstance().Write(L"Send user list (IP : %s, Port : %hu, Socket : %u)",
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;

    }
  case PT_ADMIN_C2S_USER_ADD:
    {
      ANYVOD_USER_INFO info;

      info.admin = packet.admin_c2s_user_add.detail.admin != 0;
      Util::ConvertUTF8ToUnicode((char*)packet.admin_c2s_user_add.detail.id, MAX_ID_SIZE, &info.id);
      Util::ConvertUTF8ToUnicode((char*)packet.admin_c2s_user_add.detail.name, MAX_NAME_SIZE, &info.name);
      Util::ConvertUTF8ToUnicode((char*)packet.admin_c2s_user_add.detail.pass, MAX_PASS_SIZE, &info.pass);

      success = this->SendUserAdd(info);

      Log::GetInstance().Write(L"Send user add : %s (IP : %s, Port : %hu, Socket : %u)",
        info.id.c_str(),
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;

    }
  case PT_ADMIN_C2S_USER_MODIFY:
    {
      ANYVOD_USER_INFO info;

      info.admin = packet.admin_c2s_user_modify.detail.admin != 0;
      // 아이디는 사용 안함
      //Util::ConvertUTF8ToUnicode((char*)packet.admin_c2s_user_modify.detail.id, MAX_ID_SIZE, &info.id);
      Util::ConvertUTF8ToUnicode((char*)packet.admin_c2s_user_modify.detail.name, MAX_NAME_SIZE, &info.name);
      Util::ConvertUTF8ToUnicode((char*)packet.admin_c2s_user_modify.detail.pass, MAX_PASS_SIZE, &info.pass);
      info.pid = ntohll(packet.admin_c2s_user_modify.pid);

      success = this->SendUserModify(info);

      Log::GetInstance().Write(L"Send user modify : %llu (IP : %s, Port : %hu, Socket : %u)",
        info.pid,
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;

    }
  case PT_ADMIN_C2S_USER_DELETE:
    {
      uint64_t pid = ntohll(packet.admin_c2s_user_delete.pid);

      success = this->SendUserDelete(pid);

      Log::GetInstance().Write(L"Send user delete : %llu (IP : %s, Port : %hu, Socket : %u)",
        pid,
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;

    }
  case PT_ADMIN_C2S_USER_EXIST:
    {
      wstring id;

      Util::ConvertUTF8ToUnicode((char*)packet.admin_c2s_user_exist.id, MAX_ID_SIZE, &id);

      success = this->SendUserExist(id);

      Log::GetInstance().Write(L"Send user exist : %s (IP : %s, Port : %hu, Socket : %u)",
        id.c_str(),
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;

    }
  case PT_ADMIN_C2S_CLIENT_STATUSLIST:
    {
      success = this->SendClientStatusList();

      Log::GetInstance().Write(L"Send client status list (IP : %s, Port : %hu, Socket : %u)",
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;

    }
  case PT_ADMIN_C2S_CLIENT_LOGOUT:
    {
      char ip[MAX_IP_SIZE];
      wstring tmp;
      uint16_t port = ntohs(packet.admin_c2s_client_logout.port);

      strncpy_s(ip, MAX_IP_SIZE, (char*)packet.admin_c2s_client_logout.ip, MAX_IP_CHAR_SIZE);
      Util::ConvertAsciiToUnicode(ip, &tmp);

      success = this->SendClientLogout(tmp, port);

      Log::GetInstance().Write(L"Send client logout IP : %S, Port : %hu (IP : %s, Port : %hu, Socket : %u)",
        ip, port,
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;

    }
  case PT_ADMIN_C2S_GROUPLIST:
    {
      success = this->SendGroupList();

      Log::GetInstance().Write(L"Send group list (IP : %s, Port : %hu, Socket : %u)",
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;

    }
  case PT_ADMIN_C2S_GROUP_ADD:
    {
      ANYVOD_GROUP_INFO info;

      Util::ConvertUTF8ToUnicode((char*)packet.admin_c2s_group_add.detail.name, MAX_GROUP_NAME_SIZE, &info.name);

      success = this->SendGroupAdd(info);

      Log::GetInstance().Write(L"Send group add : %s (IP : %s, Port : %hu, Socket : %u)",
        info.name.c_str(),
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;

    }
  case PT_ADMIN_C2S_GROUP_MODIFY:
    {
      ANYVOD_GROUP_INFO info;

      Util::ConvertUTF8ToUnicode((char*)packet.admin_c2s_group_modify.detail.name, MAX_GROUP_NAME_SIZE, &info.name);
      info.pid = ntohll(packet.admin_c2s_group_modify.pid);

      success = this->SendGroupModify(info);

      Log::GetInstance().Write(L"Send group modify : %s (IP : %s, Port : %hu, Socket : %u)",
        info.name.c_str(),
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;

    }
  case PT_ADMIN_C2S_GROUP_DELETE:
    {
      uint64_t pid = ntohll(packet.admin_c2s_group_delete.pid);

      success = this->SendGroupDelete(pid);

      Log::GetInstance().Write(L"Send group delete : %llu (IP : %s, Port : %hu, Socket : %u)",
        pid,
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;

    }
  case PT_ADMIN_C2S_GROUP_EXIST:
    {
      wstring name;

      Util::ConvertUTF8ToUnicode((char*)packet.admin_c2s_group_exist.name, MAX_GROUP_NAME_SIZE, &name);

      success = this->SendGroupExist(name);

      Log::GetInstance().Write(L"Send group exist : %s (IP : %s, Port : %hu, Socket : %u)",
        name.c_str(),
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;

    }
  case PT_ADMIN_C2S_USER_GROUPLIST:
    {
      success = this->SendUserGroupList();

      Log::GetInstance().Write(L"Send user group list (IP : %s, Port : %hu, Socket : %u)",
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;

    }
  case PT_ADMIN_C2S_USER_GROUP_ADD:
    {
      wstring groupName;
      uint64_t pid = ntohll(packet.admin_c2s_user_group_add.userPID);

      Util::ConvertUTF8ToUnicode((char*)packet.admin_c2s_user_group_add.groupName, MAX_GROUP_NAME_SIZE, &groupName);

      success = this->SendUserGroupAdd(pid, groupName);

      Log::GetInstance().Write(L"Send user group add : %llu %s (IP : %s, Port : %hu, Socket : %u)",
        pid,
        groupName.c_str(),
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;

    }
  case PT_ADMIN_C2S_USER_GROUP_DELETE:
    {
      uint64_t pid = ntohll(packet.admin_c2s_user_group_delete.pid);
      success = this->SendUserGroupDelete(pid);

      Log::GetInstance().Write(L"Send user group delete : %llu (IP : %s, Port : %hu, Socket : %u)",
        pid,
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;

    }
  case PT_ADMIN_C2S_FILE_GROUPLIST:
    {
      wstring filePath;

      Util::ConvertUTF8ToUnicode((char*)packet.admin_c2s_file_grouplist.filePath, MAX_FILEPATH_SIZE, &filePath);

      success = this->SendFileGroupList(filePath);

      Log::GetInstance().Write(L"Send file group list (IP : %s, Port : %hu, Socket : %u)",
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;

    }
  case PT_ADMIN_C2S_FILE_GROUP_ADD:
    {
      wstring filePath;
      wstring groupName;

      Util::ConvertUTF8ToUnicode((char*)packet.admin_c2s_file_group_add.filePath, MAX_FILEPATH_SIZE, &filePath);
      Util::ConvertUTF8ToUnicode((char*)packet.admin_c2s_file_group_add.detail.groupName, MAX_GROUP_NAME_SIZE, &groupName);

      success = this->SendFileGroupAdd(filePath, groupName);

      Log::GetInstance().Write(L"Send file group add : %s %s (IP : %s, Port : %hu, Socket : %u)",
        filePath.c_str(),
        groupName.c_str(),
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;

    }
  case PT_ADMIN_C2S_FILE_GROUP_DELETE:
    {
      wstring filePath;
      wstring groupName;

      Util::ConvertUTF8ToUnicode((char*)packet.admin_c2s_file_group_delete.filePath, MAX_FILEPATH_SIZE, &filePath);
      Util::ConvertUTF8ToUnicode((char*)packet.admin_c2s_file_group_delete.detail.groupName, MAX_GROUP_NAME_SIZE, &groupName);

      success = this->SendFileGroupDelete(filePath, groupName);

      Log::GetInstance().Write(L"Send file group delete (IP : %s, Port : %hu, Socket : %u)",
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;

    }
  case PT_ADMIN_C2S_STATEMENT_STATUSLIST:
    {
      success = this->SendStatementStatusList();

      Log::GetInstance().Write(L"Send statement status list (IP : %s, Port : %hu, Socket : %u)",
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;

    }
  case PT_ADMIN_C2S_SUBTITLE_EXIST:
    {
      SubtitleFileNameGenerator gen;
      vector_wstring names;
      wstring filePath;
      wstring fileName;
      wstring dirPath;
      wstring subtitlePath;
      wstring movieDir = this->m_client->GetServer()->GetEnv().movie_dir;
      bool found = false;

      Util::ConvertUTF8ToUnicode((char*)packet.admin_c2s_subtitle_exist.filePath, MAX_PATH_SIZE, &filePath);
      replace(filePath.begin(), filePath.end(), UNIX_DIRECTORY_DELIMITER, DIRECTORY_DELIMITER);
      Util::SplitFilePath(filePath, &dirPath, &fileName);
      Util::AppendDirSeparator(&dirPath);

      gen.getFileNamesWithExt(fileName, &names);

      for (size_t i = 0; i < names.size(); i++)
      {
        ifstream ifs;

        Util::BindPath(movieDir, dirPath, &subtitlePath);
        Util::BindPath(subtitlePath, names[i], &subtitlePath);

        ifs.open(subtitlePath.c_str(), ios::binary);

        if (ifs.is_open())
        {
          success = this->SendSubtitleExist(true, true, names[i]);
          found = true;

          break;

        }

      }

      if (found == false)
      {
        names.clear();
        gen.getFileNames(fileName, &names);

        for (size_t i = 0; i < names.size(); i++)
        {
          ifstream ifs;

          Util::BindPath(movieDir, dirPath, &subtitlePath);
          Util::BindPath(subtitlePath, names[i], &subtitlePath);

          ifs.open(subtitlePath.c_str(), ios::binary);

          if (ifs.is_open())
          {
            success = this->SendSubtitleExist(true, true, names[i]);
            found = true;

            break;

          }

        }

        if (found == false)
        {
          SubtitleImpl subtitle;
          wstring ret;
          wstring moviePath;

          Util::BindPath(movieDir, filePath, &moviePath);

          if (subtitle.getLyrics(moviePath, &ret))
          {
            wstring lyricName;

            gen.getFileBaseName(fileName, true, &lyricName);
            lyricName += EXTENSION_SEPERATOR;
            lyricName += L"lrc";

            success = this->SendSubtitleExist(true, true, lyricName);

          }
          else
          {
            success = this->SendSubtitleExist(true, false, L"");

          }

        }

      }

      replace(filePath.begin(), filePath.end(), DIRECTORY_DELIMITER, UNIX_DIRECTORY_DELIMITER);

      Log::GetInstance().Write(L"Send subtitle exist : %s (IP : %s, Port : %hu, Socket : %u)",
        filePath.c_str(),
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;

    }
  case PT_ADMIN_C2S_REBUILD_META_DATA:
    {
      success = this->SendRebuildMetaData();

      Log::GetInstance().Write(L"Send rebuild metadata (IP : %s, Port : %hu, Socket : %u)",
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;

    }
  case PT_ADMIN_C2S_REBUILD_MOVIE_DATA:
    {
      success = this->SendRebuildMovieData();

      Log::GetInstance().Write(L"Send rebuild movie data (IP : %s, Port : %hu, Socket : %u)",
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;

    }
  default:
    {
      Log::GetInstance().Write(L"Invalid administrator packet. Plan to close (IP : %s, Port : %hu, Socket : %u)",
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      this->m_client->SetForceClose();

      break;

    }

  }

  return success;

}

bool
AdminPacketHandler::SendNetworkStatistics(bool success)
{
  ANYVOD_PACKET packet;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_NETWORK_STATISTICS);
  packet.s2c_header.header.type = PT_ADMIN_S2C_NETWORK_STATISTICS;
  packet.s2c_header.success = success;

  VECTOR_ANYVOD_CLIENT_STATUS ret;

  this->m_client->GetServer()->GetClientProcessor().GetClientStatus(&ret);

  packet.admin_s2c_network_statistics.totalClients = htonl((unsigned int)ret.size());

  unsigned long long totalSent = 0;
  unsigned long long totalRecv = 0;
  unsigned int recvPerSec = 0;
  unsigned int sentPerSec = 0;

  for (size_t i = 0; i < ret.size(); i++)
  {
    ANYVOD_CLIENT_STATUS status = ret[i];

    totalSent += status.totalSentBytes;
    totalRecv += status.totalRecvBytes;

    recvPerSec += status.recvBytesPerSec;
    sentPerSec += status.sentBytesPerSec;

  }

  packet.admin_s2c_network_statistics.totalSent = htonll(totalSent);
  packet.admin_s2c_network_statistics.totalRecv = htonll(totalRecv);

  packet.admin_s2c_network_statistics.recvPerSec = htonl(recvPerSec);
  packet.admin_s2c_network_statistics.sentPerSec = htonl(sentPerSec);

  return this->m_client->SendPostPacket(packet);

}

bool
AdminPacketHandler::SendFile(ifstream &ifs, FILE_START_TYPE type, const wstring &fileName)
{
  unsigned int totalSize = 0;
  bool sent = false;
  bool cutBOM = false;
  uint32_t bom = 0;

  ifs.seekg(0, ios::beg);
  ifs.read((char*)&bom, UTF8_BOM_SIZE);

  bom = ntohl(bom);
  cutBOM = bom == UTF8_BOM && (type == FST_ENV || type == FST_LOG);

  ifs.seekg(0, ios::end);
  totalSize = (unsigned int)ifs.tellg();

  if (cutBOM)
  {
    totalSize -= UTF8_BOM_SIZE;

  }

  if (!ifs.good())
  {
    sent = this->SendFileStart(false, totalSize, type, fileName) && this->m_parent->SendError(ET_CANNOT_READ_FILE);

    return sent;

  }
  else
  {
    sent = this->SendFileStart(true, totalSize, type, fileName);

    if (!sent)
    {
      return sent;

    }

    char buf[MAX_STREAM_SIZE];

    ifs.seekg(0, ios::beg);

    if (cutBOM)
    {
      ifs.read(buf, UTF8_BOM_SIZE);

    }

    while (ifs.good())
    {
      ifs.read(buf, MAX_STREAM_SIZE);

      if (ifs.bad())
      {
        return this->SendFileStream(false, buf, 0, type) && this->m_parent->SendError(ET_CANNOT_READ_FILE);

      }
      else
      {
        this->SendFileStream(true, buf, (unsigned int)ifs.gcount(), type);

      }

    }

    sent = this->SendFileEnd(true, type);

    return sent;

  }

}

bool
AdminPacketHandler::SendFileStart(bool success, unsigned int totalSize, FILE_START_TYPE type, const wstring &fileName)
{
  ANYVOD_PACKET packet;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_FILE_START);
  packet.s2c_header.header.type = PT_ADMIN_S2C_FILE_START;
  packet.s2c_header.success = success;

  packet.admin_s2c_file_start.totalSize = htonll((uint64_t)totalSize);
  packet.admin_s2c_file_start.type = htonl(type);

  Util::ConvertUnicodeToUTF8(fileName, (char*)packet.admin_s2c_file_start.path, MAX_PATH_SIZE);

  return this->m_client->SendPostPacket(packet);

}

bool
AdminPacketHandler::SendFileStream(bool success, const char buf[MAX_STREAM_SIZE], unsigned int size, FILE_START_TYPE type)
{
  ANYVOD_PACKET packet;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_FILE_STREAM);
  packet.s2c_header.header.type = PT_ADMIN_S2C_FILE_STREAM;
  packet.s2c_header.success = success;

  packet.admin_s2c_file_stream.type = htonl(type);

  if (success)
  {
    packet.admin_s2c_file_stream.size = htonl(size);

    memcpy(packet.admin_s2c_file_stream.data, buf, size);

  }

  return this->m_client->SendPostPacket(packet);

}

bool
AdminPacketHandler::SendFileEnd(bool success, FILE_START_TYPE type)
{
  ANYVOD_PACKET packet;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_FILE_END);
  packet.s2c_header.header.type = PT_ADMIN_S2C_FILE_END;
  packet.s2c_header.success = success;

  packet.admin_s2c_file_end.type = htonl(type);

  return this->m_client->SendPostPacket(packet);

}

bool
AdminPacketHandler::SendLastLog(bool success, ifstream &log, const wstring &fileName)
{
  ANYVOD_PACKET packet;
  bool sent = false;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_LASTLOG);
  packet.s2c_header.header.type = PT_ADMIN_S2C_LASTLOG;
  packet.s2c_header.success = success;

  sent = this->m_client->SendPostPacket(packet);

  if (success)
  {
    return this->SendFile(log, FST_LOG, fileName);

  }
  else
  {
    return sent;

  }

}

bool
AdminPacketHandler::SendEnv(bool success, ifstream &env, const wstring &fileName)
{
  ANYVOD_PACKET packet;
  bool sent = false;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_ENV);
  packet.s2c_header.header.type = PT_ADMIN_S2C_ENV;
  packet.s2c_header.success = success;

  sent = this->m_client->SendPostPacket(packet);

  if (success)
  {
    return this->SendFile(env, FST_ENV, fileName);

  }
  else
  {
    return sent;

  }

}

bool
AdminPacketHandler::SendScheduleList(bool success)
{
  ANYVOD_PACKET packet;
  bool sent = false;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_SCHEDULELIST);
  packet.s2c_header.header.type = PT_ADMIN_S2C_SCHEDULELIST;
  packet.s2c_header.success = success;

  Scheduler::VECTOR_JOB_STATUS jobs;

  this->m_client->GetServer()->GetScheduler().GetJobStatus(&jobs);

  packet.admin_s2c_schedulelist.count = htonl((unsigned int)jobs.size());

  sent = this->m_client->SendPostPacket(packet);

  if (success)
  {
    for (size_t i = 0; i < jobs.size(); i++)
    {
      Scheduler::JOB_STATUS &job = jobs[i];

      sent = this->SendScheduleItem(true, (unsigned int)i, job);

      if (!sent)
        break;

    }

  }

  return sent;

}

bool
AdminPacketHandler::SendScheduleItem(bool success, unsigned int number, const Scheduler::JOB_STATUS &job)
{
  ANYVOD_PACKET packet;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_SCHEDULE_ITEM);
  packet.s2c_header.header.type = PT_ADMIN_S2C_SCHEDULE_ITEM;
  packet.s2c_header.success = success;

  packet.admin_s2c_schedule_item.number = htonl(number);
  packet.admin_s2c_schedule_item.running = job.run;

  packet.admin_s2c_schedule_item.startedTime.year = htons(job.startedTime.wYear);
  packet.admin_s2c_schedule_item.startedTime.month = htons(job.startedTime.wMonth);
  packet.admin_s2c_schedule_item.startedTime.weekday = htons(job.startedTime.wDayOfWeek);
  packet.admin_s2c_schedule_item.startedTime.day = htons(job.startedTime.wDay);
  packet.admin_s2c_schedule_item.startedTime.hour = htons(job.startedTime.wHour);
  packet.admin_s2c_schedule_item.startedTime.minute = htons(job.startedTime.wMinute);
  packet.admin_s2c_schedule_item.startedTime.second = htons(job.startedTime.wSecond);
  packet.admin_s2c_schedule_item.startedTime.milliseconds = htons(job.startedTime.wMilliseconds);

  packet.admin_s2c_schedule_item.endedTime.year = htons(job.endedTime.wYear);
  packet.admin_s2c_schedule_item.endedTime.month = htons(job.endedTime.wMonth);
  packet.admin_s2c_schedule_item.endedTime.weekday = htons(job.endedTime.wDayOfWeek);
  packet.admin_s2c_schedule_item.endedTime.day = htons(job.endedTime.wDay);
  packet.admin_s2c_schedule_item.endedTime.hour = htons(job.endedTime.wHour);
  packet.admin_s2c_schedule_item.endedTime.minute = htons(job.endedTime.wMinute);
  packet.admin_s2c_schedule_item.endedTime.second = htons(job.endedTime.wSecond);
  packet.admin_s2c_schedule_item.endedTime.milliseconds = htons(job.endedTime.wMilliseconds);


  size_t start = 0;
  size_t end = 0;
  string tmp;

  for (int i = TI_MINUTE; ((end = job.script.find_first_of(L" \t", start)) != wstring::npos) || i < TI_COUNT; i++)
  {
    wstring element = job.script.substr(start, end - start);

    Util::ConvertUnicodeToAscii(element, &tmp);
    strncpy_s((char*)packet.admin_s2c_schedule_item.elements[i], MAX_SCHEDULE_FIELD_SIZE, tmp.c_str(), MAX_SCHEDULE_FIELD_CHAR_SIZE);

    start = job.script.find_first_not_of(L" \t", end);

  }

  Util::ConvertUnicodeToAscii(job.name, &tmp);
  strncpy_s((char*)packet.admin_s2c_schedule_item.name, MAX_SCHEDULE_NAME_SIZE, tmp.c_str(), MAX_SCHEDULE_NAME_CHAR_SIZE);

  return this->m_client->SendPostPacket(packet);

}

bool
AdminPacketHandler::SendRestartScheduler()
{
  ANYVOD_PACKET packet;
  unsigned int index = 0;
  Scheduler &scheduler = this->m_client->GetServer()->GetScheduler();
  bool success = !scheduler.RunningSchedules(&index);
  bool sent = false;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_RESTART_SCHEDULER);
  packet.s2c_header.header.type = PT_ADMIN_S2C_RESTART_SCHEDULER;
  packet.s2c_header.success = success;

  packet.admin_s2c_restart_scheduler.runningIndex = htonl(index);

  sent = this->m_client->SendPostPacket(packet);

  if (!success)
  {
    sent = this->m_parent->SendError(ET_SCHEDULE_RUNNING);

  }

  Log::GetInstance().Write(L"Stopping scheduler");

  scheduler.Stop();

  Log::GetInstance().Write(L"Scheduler stopped");

  Log::GetInstance().Write(L"Starting scheduler");

  if (scheduler.Start())
  {
    Log::GetInstance().Write(L"Scheduler started");

    return this->SendRestartSchedulerComplete(true);

  }
  else
  {
    Log::GetInstance().Write(L"Restarting scheduler failed");

    return this->SendRestartSchedulerComplete(false) && this->m_parent->SendError(ET_CANNOT_START_SCHEDULER);

  }

}

bool
AdminPacketHandler::SendRestartSchedulerComplete(bool success)
{
  ANYVOD_PACKET packet;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_RESTART_SCHEDULER_COMPLETE);
  packet.s2c_header.header.type = PT_ADMIN_S2C_RESTART_SCHEDULER_COMPLETE;
  packet.s2c_header.success = success;

  return this->m_client->SendPostPacket(packet);

}

bool
AdminPacketHandler::SendScheduleStartNow(unsigned int index)
{
  ANYVOD_PACKET packet;
  Scheduler::SCHEDULE_FAIL_REASON reason = Scheduler::SFR_NONE;
  bool success = this->m_client->GetServer()->GetScheduler().StartNow(index, &reason);
  bool sent = false;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_SCHEDULE_STARTNOW);
  packet.s2c_header.header.type = PT_ADMIN_S2C_SCHEDULE_STARTNOW;
  packet.s2c_header.success = success;

  sent = this->m_client->SendPostPacket(packet);

  if (!success)
  {
    if (reason == Scheduler::SFR_RUNNING)
    {
      sent = this->m_parent->SendError(ET_SCHEDULE_RUNNING);

    }
    else
    {
      sent = this->m_parent->SendError(ET_NOT_EXIST_SCHEDULE);

    }

  }

  return sent;

}

bool
AdminPacketHandler::SendScheduleTypes()
{
  ANYVOD_PACKET packet;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_SCHEDULE_TYPES);
  packet.s2c_header.header.type = PT_ADMIN_S2C_SCHEDULE_TYPES;
  packet.s2c_header.success = true;

  for (int i = IT_HEAD; i < IT_COUNT; i++)
  {
    string tmp;

    Util::ConvertUnicodeToAscii(ScheduleInvokerFactory::INVOKER_NAMES[i], &tmp);
    strncpy_s((char*)packet.admin_s2c_schedule_types.types[i], MAX_SCHEDULE_NAME_SIZE, tmp.c_str(), MAX_SCHEDULE_NAME_CHAR_SIZE);

  }

  return this->m_client->SendPostPacket(packet);

}

bool
AdminPacketHandler::SendScheduleDelete(unsigned int index)
{
  ANYVOD_PACKET packet;
  Scheduler::SCHEDULE_FAIL_REASON reason = Scheduler::SFR_NONE;
  bool success = this->m_client->GetServer()->GetScheduler().Delete(index, &reason);
  bool sent = false;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_SCHEDULE_DELETE);
  packet.s2c_header.header.type = PT_ADMIN_S2C_SCHEDULE_DELETE;
  packet.s2c_header.success = success;

  sent = this->m_client->SendPostPacket(packet);

  if (!success)
  {
    if (reason == Scheduler::SFR_RUNNING)
    {
      sent = this->m_parent->SendError(ET_SCHEDULE_RUNNING);

    }
    else if (reason == Scheduler::SFR_NOT_EXIST)
    {
      sent = this->m_parent->SendError(ET_NOT_EXIST_SCHEDULE);

    }
    else
    {
      sent = this->m_parent->SendError(ET_CANNOT_WRITE_FILE);

    }

  }

  return sent;

}

bool
AdminPacketHandler::SendScheduleAdd(const wstring &script)
{
  ANYVOD_PACKET packet;
  Scheduler::SCHEDULE_FAIL_REASON reason = Scheduler::SFR_NONE;
  bool success = false;
  bool sent = false;

  packet.admin_s2c_schedule_add.index = htonl(this->m_client->GetServer()->GetScheduler().Add(script, &success, &reason));

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_SCHEDULE_ADD);
  packet.s2c_header.header.type = PT_ADMIN_S2C_SCHEDULE_ADD;
  packet.s2c_header.success = success;

  sent = this->m_client->SendPostPacket(packet);

  if (!success)
  {
    if (reason == Scheduler::SFR_INVALID_SCRIPT)
    {
      sent = this->m_parent->SendError(ET_INVALID_SCRIPT);

    }
    else
    {
      sent = this->m_parent->SendError(ET_CANNOT_WRITE_FILE);

    }

  }

  return sent;

}

bool
AdminPacketHandler::SendScheduleModify(unsigned int index, const wstring &script)
{
  ANYVOD_PACKET packet;
  Scheduler::SCHEDULE_FAIL_REASON reason = Scheduler::SFR_NONE;
  bool success = this->m_client->GetServer()->GetScheduler().Modify(index, script, &reason);
  bool sent = false;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_SCHEDULE_MODIFY);
  packet.s2c_header.header.type = PT_ADMIN_S2C_SCHEDULE_MODIFY;
  packet.s2c_header.success = success;

  sent = this->m_client->SendPostPacket(packet);

  if (!success)
  {
    if (reason == Scheduler::SFR_INVALID_SCRIPT)
    {
      sent = this->m_parent->SendError(ET_INVALID_SCRIPT);

    }
    else if (reason == Scheduler::SFR_RUNNING)
    {
      sent = this->m_parent->SendError(ET_SCHEDULE_RUNNING);

    }
    else if (reason == Scheduler::SFR_NOT_EXIST)
    {
      sent = this->m_parent->SendError(ET_NOT_EXIST_SCHEDULE);

    }
    else
    {
      sent = this->m_parent->SendError(ET_CANNOT_WRITE_FILE);

    }

  }

  return sent;

}

bool
AdminPacketHandler::SendLogList()
{
  ANYVOD_PACKET packet;
  Log::VECTOR_FILE_ITEM fileItems;
  bool sent = false;
  bool success = Log::GetLogNames(this->m_client->GetServer()->GetEnv().log_dir, &fileItems);

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_LOGLIST);
  packet.s2c_header.header.type = PT_ADMIN_S2C_LOGLIST;
  packet.s2c_header.success = success;

  packet.admin_s2c_loglist.count = htonl((unsigned int)fileItems.size());

  sent = this->m_client->SendPostPacket(packet);

  if (success)
  {
    for (size_t i = 0; i < fileItems.size(); i++)
    {
      sent = this->SendLogItem(true, fileItems[i]);

      if (!sent)
        break;

    }

  }
  else
  {
    return this->m_parent->SendError(ET_NOT_EXIST_LOG);

  }

  return sent;

}

bool
AdminPacketHandler::SendLogItem(bool success, const Log::FILE_ITEM &item)
{
  ANYVOD_PACKET packet;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_LOG_ITEM);
  packet.s2c_header.header.type = PT_ADMIN_S2C_LOG_ITEM;
  packet.s2c_header.success = success;

  packet.admin_s2c_log_item.fileSize = htonll(item.fileSize);

  Util::ConvertUnicodeToUTF8(item.fileName, (char*)packet.admin_s2c_log_item.fileName, MAX_PATH_SIZE);

  return this->m_client->SendPostPacket(packet);

}

bool
AdminPacketHandler::SendLogDelete(bool success)
{
  ANYVOD_PACKET packet;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_LOG_DELETE);
  packet.s2c_header.header.type = PT_ADMIN_S2C_LOG_DELETE;
  packet.s2c_header.success = success;

  return this->m_client->SendPostPacket(packet);

}

bool
AdminPacketHandler::SendLogView(bool success, ifstream &log, const wstring &fileName)
{
  ANYVOD_PACKET packet;
  bool sent = false;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_LOG_VIEW);
  packet.s2c_header.header.type = PT_ADMIN_S2C_LOG_VIEW;
  packet.s2c_header.success = success;

  sent = this->m_client->SendPostPacket(packet);

  if (success)
  {
    return this->SendFile(log, FST_LOG, fileName);

  }
  else
  {
    return sent;

  }

}

bool
AdminPacketHandler::SendUserList()
{
  ANYVOD_PACKET packet;
  VECTOR_ANYVOD_USER_INFO userItems;
  bool sent = false;
  DataBase::DATABASE_FAIL_REASON reason;
  DataBase &db = this->m_client->GetServer()->GetDataBase();
  bool success = db.GetUserInfos(&userItems, &reason);

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_USERLIST);
  packet.s2c_header.header.type = PT_ADMIN_S2C_USERLIST;
  packet.s2c_header.success = success;

  packet.admin_s2c_userlist.count = htonl((unsigned int)userItems.size());

  sent = this->m_client->SendPostPacket(packet);

  if (success)
  {
    for (size_t i = 0; i < userItems.size(); i++)
    {
      sent = this->SendUserItem(true, userItems[i]);

      if (!sent)
        break;

    }

  }
  else
  {
    return this->m_parent->SendError(DataBase::FailResonToErrorType(reason));

  }

  return sent;

}

bool
AdminPacketHandler::SendUserItem(bool success, const ANYVOD_USER_INFO &item)
{
  ANYVOD_PACKET packet;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_USER_ITEM);
  packet.s2c_header.header.type = PT_ADMIN_S2C_USER_ITEM;
  packet.s2c_header.success = success;

  packet.admin_s2c_user_item.pid = htonll(item.pid);
  packet.admin_s2c_user_item.detail.admin = item.admin;

  Util::ConvertUnicodeToUTF8(item.id, (char*)packet.admin_s2c_user_item.detail.id, MAX_ID_SIZE);
  Util::ConvertUnicodeToUTF8(item.name, (char*)packet.admin_s2c_user_item.detail.name, MAX_NAME_SIZE);
  Util::ConvertUnicodeToUTF8(item.pass, (char*)packet.admin_s2c_user_item.detail.pass, MAX_PASS_SIZE);

  return this->m_client->SendPostPacket(packet);

}

bool
AdminPacketHandler::SendUserAdd(const ANYVOD_USER_INFO &info)
{
  ANYVOD_PACKET packet;
  DataBase::DATABASE_FAIL_REASON reason;
  bool success = false;
  bool sent = false;
  DataBase &db = this->m_client->GetServer()->GetDataBase();

  packet.admin_s2c_user_add.pid = htonll(db.AddUser(info, true, &success, &reason));

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_USER_ADD);
  packet.s2c_header.header.type = PT_ADMIN_S2C_USER_ADD;
  packet.s2c_header.success = success;

  sent = this->m_client->SendPostPacket(packet);

  if (!success)
  {
    sent = this->m_parent->SendError(DataBase::FailResonToErrorType(reason));

  }

  return sent;

}

bool
AdminPacketHandler::SendUserModify(const ANYVOD_USER_INFO &info)
{
  ANYVOD_PACKET packet;
  DataBase::DATABASE_FAIL_REASON reason;
  bool sent = false;
  DataBase &db = this->m_client->GetServer()->GetDataBase();
  bool success = db.ModifyUser(info, &reason);

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_USER_MODIFY);
  packet.s2c_header.header.type = PT_ADMIN_S2C_USER_MODIFY;
  packet.s2c_header.success = success;

  sent = this->m_client->SendPostPacket(packet);

  if (!success)
  {
    sent = this->m_parent->SendError(DataBase::FailResonToErrorType(reason));

  }

  return sent;

}

bool
AdminPacketHandler::SendUserDelete(unsigned long long pid)
{
  ANYVOD_PACKET packet;
  DataBase::DATABASE_FAIL_REASON reason;
  bool sent = false;
  DataBase &db = this->m_client->GetServer()->GetDataBase();
  ANYVOD_USER_INFO info;
  bool success = false;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_USER_DELETE);
  packet.s2c_header.header.type = PT_ADMIN_S2C_USER_DELETE;

  success = db.GetUserInfo(pid, &info, &reason);

  if (success)
  {
    success = db.DeleteUser(pid, &reason);

    packet.s2c_header.success = success;

    sent = this->m_client->SendPostPacket(packet);

    if (!success)
    {
      sent = this->m_parent->SendError(DataBase::FailResonToErrorType(reason));

    }

    this->m_client->GetServer()->GetClientProcessor().LogoutUser(info.id);

  }
  else
  {
    packet.s2c_header.success = false;

    sent = this->m_client->SendPostPacket(packet);
    sent = this->m_parent->SendError(DataBase::FailResonToErrorType(reason));

  }

  return sent;

}

bool
AdminPacketHandler::SendUserExist(const wstring &id)
{
  ANYVOD_PACKET packet;
  DataBase::DATABASE_FAIL_REASON reason;
  bool sent = false;
  DataBase &db = this->m_client->GetServer()->GetDataBase();
  bool exist = db.ExistUser(id, &reason) || db.ExistGroup(id, &reason);

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_USER_EXIST);
  packet.s2c_header.header.type = PT_ADMIN_S2C_USER_EXIST;

  if (reason == DataBase::DFR_DATABASE_ERROR)
  {
    packet.s2c_header.success = false;

  }
  else
  {
    packet.s2c_header.success = true;

    packet.admin_s2c_user_exist.exist = exist;

  }

  sent = this->m_client->SendPostPacket(packet);

  if (reason == DataBase::DFR_DATABASE_ERROR)
  {
    sent = this->m_parent->SendError(DataBase::FailResonToErrorType(reason));

  }

  return sent;

}

bool
AdminPacketHandler::SendClientStatusList()
{
  ANYVOD_PACKET packet;
  VECTOR_ANYVOD_CLIENT_STATUS items;
  bool sent = false;

  this->m_client->GetServer()->GetClientProcessor().GetClientStatus(&items);

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_CLIENT_STATUSLIST);
  packet.s2c_header.header.type = PT_ADMIN_S2C_CLIENT_STATUSLIST;
  packet.s2c_header.success = true;

  packet.admin_s2c_client_statuslist.count = htonl((unsigned int)items.size());

  sent = this->m_client->SendPostPacket(packet);

  for (size_t i = 0; i < items.size(); i++)
  {
    sent = this->SendClientStatusItem(true, items[i]);

    if (!sent)
      break;

  }

  return sent;

}

bool
AdminPacketHandler::SendClientStatusItem(bool success, const ANYVOD_CLIENT_STATUS &item)
{
  ANYVOD_PACKET packet;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_CLIENT_STATUS_ITEM);
  packet.s2c_header.header.type = PT_ADMIN_S2C_CLIENT_STATUS_ITEM;
  packet.s2c_header.success = success;

  packet.admin_s2c_client_status_item.status.clientState = htonl((int)item.clientState);
  packet.admin_s2c_client_status_item.status.lastPacketType = htonl((int)item.lastPacketType);
  packet.admin_s2c_client_status_item.status.port = htons(item.port);

  packet.admin_s2c_client_status_item.status.totalRecvBytes = htonll(item.totalRecvBytes);
  packet.admin_s2c_client_status_item.status.recvBufCount = htonl((unsigned int)item.recvBufCount);
  packet.admin_s2c_client_status_item.status.recvBytesPerSec = htonl(item.recvBytesPerSec);

  packet.admin_s2c_client_status_item.status.totalSentBytes = htonll(item.totalSentBytes);
  packet.admin_s2c_client_status_item.status.sendBufCount = htonl((unsigned int)item.sendBufCount);
  packet.admin_s2c_client_status_item.status.sentBytesPerSec = htonl(item.sentBytesPerSec);

  packet.admin_s2c_client_status_item.status.isStream = item.isStream;
  strncpy((char*)packet.admin_s2c_client_status_item.status.ticket, item.ticket.c_str(), MAX_TICKET_SIZE);

  Util::ConvertUnicodeToUTF8(item.id, (char*)packet.admin_s2c_client_status_item.status.id, MAX_ID_SIZE);

  string tmp;

  Util::ConvertUnicodeToAscii(item.ip, &tmp);
  strncpy_s((char*)packet.admin_s2c_client_status_item.status.ip, MAX_IP_SIZE, tmp.c_str(), MAX_IP_CHAR_SIZE);

  return this->m_client->SendPostPacket(packet);

}

bool
AdminPacketHandler::SendClientLogout(const wstring &ip, unsigned short port)
{
  ANYVOD_PACKET packet;
  bool sent = false;
  bool success = this->m_client->GetServer()->GetClientProcessor().LogoutClient(ip, port);

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_CLIENT_LOGOUT);
  packet.s2c_header.header.type = PT_ADMIN_S2C_CLIENT_LOGOUT;
  packet.s2c_header.success = success;

  sent = this->m_client->SendPostPacket(packet);

  if (!success)
  {
    sent = this->m_parent->SendError(ET_CLIENT_NOT_FOUND);

  }

  return sent;

}

bool
AdminPacketHandler::SendGroupList()
{
  ANYVOD_PACKET packet;
  VECTOR_ANYVOD_GROUP_INFO groupItems;
  bool sent = false;
  DataBase::DATABASE_FAIL_REASON reason;
  DataBase &db = this->m_client->GetServer()->GetDataBase();
  bool success = db.GetGroupInfos(&groupItems, &reason);

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_GROUPLIST);
  packet.s2c_header.header.type = PT_ADMIN_S2C_GROUPLIST;
  packet.s2c_header.success = success;

  packet.admin_s2c_grouplist.count = htonl((unsigned int)groupItems.size());

  sent = this->m_client->SendPostPacket(packet);

  if (success)
  {
    for (size_t i = 0; i < groupItems.size(); i++)
    {
      sent = this->SendGroupItem(true, groupItems[i]);

      if (!sent)
        break;

    }

  }
  else
  {
    return this->m_parent->SendError(DataBase::FailResonToErrorType(reason));

  }

  return sent;

}

bool
AdminPacketHandler::SendGroupItem(bool success, const ANYVOD_GROUP_INFO &item)
{
  ANYVOD_PACKET packet;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_GROUP_ITEM);
  packet.s2c_header.header.type = PT_ADMIN_S2C_GROUP_ITEM;
  packet.s2c_header.success = success;

  packet.admin_s2c_group_item.pid = htonll(item.pid);

  Util::ConvertUnicodeToUTF8(item.name, (char*)packet.admin_s2c_group_item.detail.name, MAX_GROUP_NAME_SIZE);

  return this->m_client->SendPostPacket(packet);

}

bool
AdminPacketHandler::SendGroupAdd(const ANYVOD_GROUP_INFO &info)
{
  ANYVOD_PACKET packet;
  DataBase::DATABASE_FAIL_REASON reason;
  bool success = false;
  bool sent = false;
  DataBase &db = this->m_client->GetServer()->GetDataBase();

  packet.admin_s2c_group_add.pid = htonll(db.AddGroup(info.name, true, &success, &reason));

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_GROUP_ADD);
  packet.s2c_header.header.type = PT_ADMIN_S2C_GROUP_ADD;
  packet.s2c_header.success = success;

  sent = this->m_client->SendPostPacket(packet);

  if (!success)
  {
    sent = this->m_parent->SendError(DataBase::FailResonToErrorType(reason));

  }

  return sent;

}

bool
AdminPacketHandler::SendGroupModify(const ANYVOD_GROUP_INFO &info)
{
  ANYVOD_PACKET packet;
  DataBase::DATABASE_FAIL_REASON reason;
  bool sent = false;
  DataBase &db = this->m_client->GetServer()->GetDataBase();
  bool success = false;
  ANYVOD_GROUP_INFO orgInfo;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_GROUP_MODIFY);
  packet.s2c_header.header.type = PT_ADMIN_S2C_GROUP_MODIFY;

  success = db.GetGroupInfo(info.pid, &orgInfo, &reason);

  if (success)
  {
    if (orgInfo.name != PUBLIC_GROUP_NAME)
    {
      db.ModifyGroup(info, &reason);

      packet.s2c_header.success = success;

      sent = this->m_client->SendPostPacket(packet);

      if (!success)
      {
        sent = this->m_parent->SendError(DataBase::FailResonToErrorType(reason));

      }

    }
    else
    {
      packet.s2c_header.success = false;

      sent = this->m_client->SendPostPacket(packet);
      sent = this->m_parent->SendError(ET_CANNOT_MODIFY_PUBLIC_GROUP);

    }

  }
  else
  {
    packet.s2c_header.success = false;

    sent = this->m_client->SendPostPacket(packet);
    sent = this->m_parent->SendError(DataBase::FailResonToErrorType(reason));

  }

  return sent;

}

bool
AdminPacketHandler::SendGroupDelete(unsigned long long pid)
{
  ANYVOD_PACKET packet;
  DataBase::DATABASE_FAIL_REASON reason;
  bool sent = false;
  DataBase &db = this->m_client->GetServer()->GetDataBase();
  ANYVOD_GROUP_INFO info;
  bool success = false;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_GROUP_DELETE);
  packet.s2c_header.header.type = PT_ADMIN_S2C_GROUP_DELETE;

  success = db.GetGroupInfo(pid, &info, &reason);

  if (success)
  {
    if (info.name != PUBLIC_GROUP_NAME)
    {
      success = db.DeleteGroupWithAll(pid, &reason);

      packet.s2c_header.success = success;

      sent = this->m_client->SendPostPacket(packet);

      if (!success)
      {
        sent = this->m_parent->SendError(DataBase::FailResonToErrorType(reason));

      }

    }
    else
    {
      packet.s2c_header.success = false;

      sent = this->m_client->SendPostPacket(packet);
      sent = this->m_parent->SendError(ET_CANNOT_DELETE_PUBLIC_GROUP);

    }

  }
  else
  {
    packet.s2c_header.success = false;

    sent = this->m_client->SendPostPacket(packet);
    sent = this->m_parent->SendError(DataBase::FailResonToErrorType(reason));

  }

  return sent;

}

bool
AdminPacketHandler::SendGroupExist(const wstring &name)
{
  ANYVOD_PACKET packet;
  DataBase::DATABASE_FAIL_REASON reason;
  bool sent = false;
  DataBase &db = this->m_client->GetServer()->GetDataBase();
  bool exist = db.ExistGroup(name, &reason);

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_GROUP_EXIST);
  packet.s2c_header.header.type = PT_ADMIN_S2C_GROUP_EXIST;

  if (reason == DataBase::DFR_DATABASE_ERROR)
  {
    packet.s2c_header.success = false;

  }
  else
  {
    packet.s2c_header.success = true;

    packet.admin_s2c_group_exist.exist = exist;

  }

  sent = this->m_client->SendPostPacket(packet);

  if (reason == DataBase::DFR_DATABASE_ERROR)
  {
    sent = this->m_parent->SendError(DataBase::FailResonToErrorType(reason));

  }

  return sent;

}

bool
AdminPacketHandler::SendUserGroupList()
{
  ANYVOD_PACKET packet;
  VECTOR_ANYVOD_USER_GROUP_INFO items;
  bool sent = false;
  DataBase::DATABASE_FAIL_REASON reason;
  DataBase &db = this->m_client->GetServer()->GetDataBase();
  bool success = db.GetUserGroupInfos(&items, &reason);

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_USER_GROUPLIST);
  packet.s2c_header.header.type = PT_ADMIN_S2C_USER_GROUPLIST;
  packet.s2c_header.success = success;

  packet.admin_s2c_user_grouplist.count = htonl((unsigned int)items.size());

  sent = this->m_client->SendPostPacket(packet);

  if (success)
  {
    for (size_t i = 0; i < items.size(); i++)
    {
      sent = this->SendUserGroupItem(true, items[i]);

      if (!sent)
        break;

    }

  }
  else
  {
    return this->m_parent->SendError(DataBase::FailResonToErrorType(reason));

  }

  return sent;

}

bool
AdminPacketHandler::SendUserGroupItem(bool success, const ANYVOD_USER_GROUP_INFO &item)
{
  ANYVOD_PACKET packet;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_USER_GROUP_ITEM);
  packet.s2c_header.header.type = PT_ADMIN_S2C_USER_GROUP_ITEM;
  packet.s2c_header.success = success;

  packet.admin_s2c_user_group_item.pid = htonll(item.pid);

  Util::ConvertUnicodeToUTF8(item.userID, (char*)packet.admin_s2c_user_group_item.detail.userID, MAX_ID_SIZE);
  Util::ConvertUnicodeToUTF8(item.groupName, (char*)packet.admin_s2c_user_group_item.detail.groupName, MAX_GROUP_NAME_SIZE);

  return this->m_client->SendPostPacket(packet);

}

bool
AdminPacketHandler::SendUserGroupAdd(unsigned long long userPID, const wstring &groupName)
{
  ANYVOD_PACKET packet;
  DataBase::DATABASE_FAIL_REASON reason;
  bool success = false;
  bool sent = false;
  DataBase &db = this->m_client->GetServer()->GetDataBase();

  packet.admin_s2c_user_group_add.pid = db.AddUserToGroup(userPID, groupName, true, &success, &reason);

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_USER_GROUP_ADD);
  packet.s2c_header.header.type = PT_ADMIN_S2C_USER_GROUP_ADD;
  packet.s2c_header.success = success;

  sent = this->m_client->SendPostPacket(packet);

  if (!success)
  {
    sent = this->m_parent->SendError(DataBase::FailResonToErrorType(reason));

  }

  return sent;

}

bool
AdminPacketHandler::SendUserGroupDelete(unsigned long long pid)
{
  ANYVOD_PACKET packet;
  DataBase::DATABASE_FAIL_REASON reason;
  bool sent = false;
  DataBase &db = this->m_client->GetServer()->GetDataBase();
  ANYVOD_USER_GROUP_INFO info;
  bool success = false;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_USER_GROUP_DELETE);
  packet.s2c_header.header.type = PT_ADMIN_S2C_USER_GROUP_DELETE;

  success = db.GetUserGroupInfo(pid, &info, &reason);

  if (success)
  {
    if (info.groupName != PUBLIC_GROUP_NAME)
    {
      success = db.DeleteUserGroup(pid, &reason);

      packet.s2c_header.success = success;

      sent = this->m_client->SendPostPacket(packet);

      if (!success)
      {
        sent = this->m_parent->SendError(DataBase::FailResonToErrorType(reason));

      }

    }
    else
    {
      packet.s2c_header.success = false;

      sent = this->m_client->SendPostPacket(packet);
      sent = this->m_parent->SendError(ET_CANNOT_DELETE_ASSOCIATED_PUBLIC_GROUP);

    }

  }
  else
  {
    packet.s2c_header.success = false;

    sent = this->m_client->SendPostPacket(packet);
    sent = this->m_parent->SendError(DataBase::FailResonToErrorType(reason));

  }

  return sent;

}

bool
AdminPacketHandler::SendFileGroupList(const wstring &filePath)
{
  ANYVOD_PACKET packet;
  VECTOR_ANYVOD_FILE_GROUP_INFO items;
  bool sent = false;
  DataBase::DATABASE_FAIL_REASON reason;
  DataBase &db = this->m_client->GetServer()->GetDataBase();
  wstring localPath;
  bool success = false;

  this->m_parent->ConvertToNativePath(filePath, &localPath);

  success = db.GetFileGroupInfos(localPath, &items, &reason);

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_FILE_GROUPLIST);
  packet.s2c_header.header.type = PT_ADMIN_S2C_FILE_GROUPLIST;
  packet.s2c_header.success = success;

  packet.admin_s2c_file_grouplist.count = htonl((unsigned int)items.size());

  sent = this->m_client->SendPostPacket(packet);

  if (success)
  {
    for (size_t i = 0; i < items.size(); i++)
    {
      sent = this->SendFileGroupItem(true, items[i]);

      if (!sent)
        break;

    }

  }
  else
  {
    return this->m_parent->SendError(DataBase::FailResonToErrorType(reason));

  }

  return sent;

}

bool
AdminPacketHandler::SendFileGroupItem(bool success, const ANYVOD_FILE_GROUP_INFO &item)
{
  ANYVOD_PACKET packet;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_FILE_GROUP_ITEM);
  packet.s2c_header.header.type = PT_ADMIN_S2C_FILE_GROUP_ITEM;
  packet.s2c_header.success = success;

  packet.admin_s2c_file_group_item.pid = htonll(item.pid);

  Util::ConvertUnicodeToUTF8(item.groupName, (char*)packet.admin_s2c_file_group_item.detail.groupName, MAX_GROUP_NAME_SIZE);

  return this->m_client->SendPostPacket(packet);

}

bool
AdminPacketHandler::SendFileGroupAdd(const wstring &filePath, const wstring &groupName)
{
  ANYVOD_PACKET packet;
  DataBase::DATABASE_FAIL_REASON reason;
  bool success = false;
  bool sent = false;
  DataBase &db = this->m_client->GetServer()->GetDataBase();
  wstring localPath;

  this->m_parent->ConvertToNativePath(filePath, &localPath);

  packet.admin_s2c_user_group_add.pid = htonll(db.AddPermissionToUserGroup(localPath, groupName, true, &success, &reason));

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_FILE_GROUP_ADD);
  packet.s2c_header.header.type = PT_ADMIN_S2C_FILE_GROUP_ADD;
  packet.s2c_header.success = success;

  sent = this->m_client->SendPostPacket(packet);

  if (!success)
  {
    sent = this->m_parent->SendError(DataBase::FailResonToErrorType(reason));

  }

  return sent;

}

bool
AdminPacketHandler::SendFileGroupDelete(const wstring &filePath, const wstring &groupName)
{
  ANYVOD_PACKET packet;
  DataBase::DATABASE_FAIL_REASON reason;
  bool sent = false;
  DataBase &db = this->m_client->GetServer()->GetDataBase();
  ANYVOD_FILE_GROUP_INFO info;
  bool success = false;
  wstring localPath;

  this->m_parent->ConvertToNativePath(filePath, &localPath);

  success = db.DeletePermissionFromUserGroup(localPath, groupName, &reason);

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_FILE_GROUP_DELETE);
  packet.s2c_header.header.type = PT_ADMIN_S2C_FILE_GROUP_DELETE;
  packet.s2c_header.success = success;

  sent = this->m_client->SendPostPacket(packet);

  if (!success)
  {
    sent = this->m_parent->SendError(DataBase::FailResonToErrorType(reason));

  }

  return sent;

}

bool
AdminPacketHandler::SendStatementStatusList()
{
  ANYVOD_PACKET packet;
  VECTOR_ANYVOD_STATEMENT_STATUS items;
  bool sent = false;
  StatementCache &cache = this->m_client->GetServer()->GetDataBase().GetStatementCache();

  cache.GetStatementStatus(&items);

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_STATEMENT_STATUSLIST);
  packet.s2c_header.header.type = PT_ADMIN_S2C_STATEMENT_STATUSLIST;
  packet.s2c_header.success = true;

  packet.admin_s2c_statement_statuslist.count = htonl((unsigned int)items.size());
  packet.admin_s2c_statement_statuslist.maxCount = htonl(cache.GetMaxItemCount());

  sent = this->m_client->SendPostPacket(packet);

  for (size_t i = 0; i < items.size(); i++)
  {
    sent = this->SendStatementStatusItem(true, items[i]);

    if (!sent)
      break;

  }

  return sent;

}

bool
AdminPacketHandler::SendStatementStatusItem(bool success, const ANYVOD_STATEMENT_STATUS &item)
{
  ANYVOD_PACKET packet;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_STATEMENT_STATUS_ITEM);
  packet.s2c_header.header.type = PT_ADMIN_S2C_STATEMENT_STATUS_ITEM;
  packet.s2c_header.success = success;

  packet.admin_s2c_statement_status_item.status.totalCount = htonl(item.totalCount);
  packet.admin_s2c_statement_status_item.status.usingCount = htonl(item.usingCount);

  strncpy_s((char*)packet.admin_s2c_statement_status_item.status.sql, MAX_SQL_SIZE, item.sql.c_str(), MAX_SQL_CHAR_SIZE);

  return this->m_client->SendPostPacket(packet);

}

bool
AdminPacketHandler::SendSubtitleExist(bool success, bool exist, const wstring &fileName)
{
  ANYVOD_PACKET packet;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_SUBTITLE_EXIST);
  packet.s2c_header.header.type = PT_ADMIN_S2C_SUBTITLE_EXIST;
  packet.s2c_header.success = success;

  packet.admin_s2c_subtitle_exist.exist = exist;

  Util::ConvertUnicodeToUTF8(fileName, (char*)packet.admin_s2c_subtitle_exist.fileName, MAX_PATH_SIZE);

  return this->m_client->SendPostPacket(packet);

}

bool
AdminPacketHandler::SendRebuildMetaData()
{
  ANYVOD_PACKET packet;
  bool success = false;
  DataBase &db = this->m_client->GetServer()->GetDataBase();

  success = db.RebuildMovieMetaData();

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_REBUILD_META_DATA);
  packet.s2c_header.header.type = PT_ADMIN_S2C_REBUILD_META_DATA;
  packet.s2c_header.success = success;

  return this->m_client->SendPostPacket(packet);

}

bool
AdminPacketHandler::SendRebuildMovieData()
{
  ANYVOD_PACKET packet;
  bool success = false;
  DataBase &db = this->m_client->GetServer()->GetDataBase();

  success = db.RebuildMovieData();

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_S2C_REBUILD_MOVIE_DATA);
  packet.s2c_header.header.type = PT_ADMIN_S2C_REBUILD_MOVIE_DATA;
  packet.s2c_header.success = success;

  return this->m_client->SendPostPacket(packet);

}
