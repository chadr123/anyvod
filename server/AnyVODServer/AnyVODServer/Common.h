/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "..\..\..\common\types.h"
#include "..\..\..\common\network.h"

#ifdef USE_VISTA
#define LOCK_OBJECT SRWLOCK
#define InitLock(lock, spin_count) InitializeSRWLock(lock)
#define DeleteLock(lock)
#define WriteLock AcquireSRWLockExclusive
#define ReadLock AcquireSRWLockShared
#define WriteUnlock ReleaseSRWLockExclusive
#define ReadUnlock ReleaseSRWLockShared

#define COUNTER_VAL LONGLONG
#define AddVal InterlockedExchangeAdd64
#define ChangeVal InterlockedExchange64
#else
#define LOCK_OBJECT CRITICAL_SECTION
#define InitLock(lock, spin_count) InitializeCriticalSectionAndSpinCount(lock, spin_count)
#define DeleteLock(lock) DeleteCriticalSection(lock);
#define WriteLock EnterCriticalSection
#define ReadLock EnterCriticalSection
#define WriteUnlock LeaveCriticalSection
#define ReadUnlock LeaveCriticalSection

#define COUNTER_VAL LONG
#define AddVal InterlockedExchangeAdd
#define ChangeVal InterlockedExchange
#endif

const int DEFAULT_BUFFERING_MULTIPLIER = 1;
const int MAX_RECV_REQUEST_COUNT = 1000;
const int MAX_SEND_REQUEST_COUNT = 1000;
const int MAX_COMPLETION_THREAD_COUNT = 32;
const int MAX_BACK_LOG = 5;
const int MAX_SPIN_COUNT = 4000;
const wchar_t INIT_FILE_NAME[] = L"setting.ini";
const wchar_t SCHEDULE_FILE_NAME[] = L"schedule.ini";
const wchar_t MOVIE_DIR[] = L"movie_dir";
const wchar_t MOVIE_EXTS[] = L"movie_exts";
const wchar_t LOG_DIR[] = L"log_dir";
const wchar_t DB_DIR[] = L"db_dir";
const wchar_t ANALYSER_DIR[] = L"analyser_dir";
const wchar_t USE_IPV6[] = L"use_ipv6";
const wchar_t NIC[] = L"nic";
const wchar_t NIC_STREAM[] = L"nic_stream";
const wchar_t NIC6[] = L"nic6";
const wchar_t NIC6_STREAM[] = L"nic6_stream";
const wchar_t NIC6_SCOPE_ID[] = L"nic6_scope_id";
const wchar_t NIC6_STREAM_SCOPE_ID[] = L"nic6_stream_scope_id";
const wchar_t LISTEN_PORT[] = L"listen_port";
const wchar_t LISTEN_STREAM_PORT[] = L"listen_stream_port";
const wchar_t BUFFERING_MULTIPLIER[] = L"buffering_multiplier";
const wchar_t COMMENT_PREFIX[] = L"//";
const wchar_t MOVIE_EXTS_DELIMITER = ';';
const wchar_t EXTENSION_DELIMITER = ' ';
const wchar_t ITEM_DELIMITER = '=';
const wchar_t EXTENSION_SEPERATOR = '.';
const wchar_t CURRENT_PATH = '.';
const wchar_t DIRECTORY_DELIMITER = '\\';
const wchar_t UNIX_DIRECTORY_DELIMITER = '/';
const wchar_t SQLITE3_FILENAME[] = L"server.db";
const wchar_t PUBLIC_GROUP_NAME[] = L"public";
const wchar_t SQL_FILENAME[] = L"database.sql";
const wchar_t LOG_FILENAME_PREFIX[] = L"log";
const wchar_t LOG_FILENAME_EXTENTION[] = L".txt";
const char LOCALE[] = "";
const char ANALYSER_INSTANCE_GETTER_FUNC_NAME[] = "GetAnalyserInstance";
const char MOVIE_INFO_TABLENAME[] = "movie_info";
const char MOVIE_META_TABLENAME[] = "movie_meta";
const char ACCOUNT_TABLENAME[] = "account";
const char DIR_INFO_TABLENAME[] = "dir_info";
const char GROUPS_TABLENAME[] = "groups";
const char USER_GROUP_TABLENAME[] = "user_group";
const char MOVIE_PERMISSION_TABLENAME[] = "movie_permission";
const char ADMINS_TABLENAME[] = "admins";
const char SEQUENCE_TABLENAME[] = "sequence";

#ifdef _WIN64
  const wchar_t ARCH_BITS[] = L"x64";
#else
  const wchar_t ARCH_BITS[] = L"x86";
#endif

enum IO_OPERATION_TYPE
{
  OP_RECV_HEADER,
  OP_RECV_BODY,
  OP_SEND

};

struct IO_CONTEXT : public WSAOVERLAPPED
{
  IO_CONTEXT()
  {
  }

  ~IO_CONTEXT()
  {
    delete[] buf.buf;

  }

  WSABUF buf;
  IO_OPERATION_TYPE op;

};

struct ENV_INFO
{
  ENV_INFO()
  {
    db_dir = L".\\db";
    log_dir = L".\\logs";
    analyser_dir = L".\\analyser";
    use_ipv6 = false;
    nic = DEFAULT_SERVER_ADDRESS;
    nic_stream = DEFAULT_SERVER_ADDRESS;
    nic6 = DEFAULT_SERVER_ADDRESS6;
    nic6_stream = DEFAULT_SERVER_ADDRESS6;
    nic6_scope_id = 0;
    nic6_stream_scope_id = 0;
    listen_port = DEFAULT_NETWORK_PORT;
    listen_stream_port = DEFAULT_NETWORK_STREAM_PORT;
    buffering_multiplier = DEFAULT_BUFFERING_MULTIPLIER;

  }

  wstring db_dir;
  wstring log_dir;
  wstring movie_dir;
  wstring analyser_dir;
  vector_wstring movie_exts;
  bool use_ipv6;
  wstring nic;
  wstring nic_stream;
  wstring nic6;
  wstring nic6_stream;
  ULONG nic6_scope_id;
  ULONG nic6_stream_scope_id;
  unsigned short listen_port;
  unsigned short listen_stream_port;
  int buffering_multiplier;

};

struct ERROR_CODE
{
  ERROR_CODE()
  {
    type = ET_NONE;
    errorCode = 0;

  }

  ERROR_TYPE type;
  DWORD errorCode;

};

#ifndef IS_BIT_SET
#define IS_BIT_SET(src, flag) ((src&flag)==flag)
#endif
