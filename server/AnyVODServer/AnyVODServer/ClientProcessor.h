/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "Client.h"
#include "Common.h"

class ClientProcessor
{
public:
  ClientProcessor();
  ~ClientProcessor();

  void AddClient(SOCKET socket, SOCKADDR_STORAGE *clientAddr, Server *server, HANDLE completionPort, bool isStream);
  void Clear();

  void LogoutUser(const wstring &id);
  bool LogoutClient(const wstring &ip, unsigned short port);
  void GetClientStatus(VECTOR_ANYVOD_CLIENT_STATUS *ret);
  bool ProcessClientTasks();
  void CollectDeadClient();
  bool FindUserByTicket(const string &ticket, Client::USER_INFO *userInfo);

private:
  typedef list<Client*> LIST_CLIENT;
  typedef LIST_CLIENT::iterator LIST_CLIENT_ITERATOR;

private:
  LIST_CLIENT m_clients;
  LOCK_OBJECT m_clientLocker;

};
