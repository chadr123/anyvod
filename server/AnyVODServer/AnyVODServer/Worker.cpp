/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "stdafx.h"
#include "Worker.h"
#include "Server.h"
#include "AutoLocker.h"
#include "Log.h"
#include "Util.h"

Worker::Worker(HANDLE completionPort, Server *server)
:m_completionPort(completionPort), m_server(server)
{

}

Worker::~Worker()
{

}

bool
Worker::Run()
{
  DWORD bytesTransferred = 0;
  IO_CONTEXT *context = nullptr;
  Client *client = nullptr;
  BOOL ret = GetQueuedCompletionStatus(this->m_completionPort, &bytesTransferred, (PULONG_PTR)&client, (LPWSAOVERLAPPED*)&context, 0);

  if (!(ret == FALSE && GetLastError() == WAIT_TIMEOUT))
  {
    if (context == nullptr)
    {
      Log::GetInstance().Write(L"Cannot get data from Completion port : \"%s\"",
        Util::SystemErrorToString(GetLastError()).c_str());

      return true;

    }

    AutoLocker autoLocker(client->GetLocker());
    bool closed = false;

    if (ret == FALSE)
    {
      DWORD flags;

      if (WSAGetOverlappedResult(client->GetSocket(), context, &bytesTransferred, FALSE, &flags) == FALSE)
      {
        if (WSAGetLastError() != NO_ERROR)
        {
          //이곳에서는 상대방이 closesocket을 하여 전송을 요청한 컨텍스트를 더이상
          //전송 할 수 없을 때 발생한다. 전송 요청을 한 후 완료되지 않았을 때 closesocket을
          //했다면 전송 요청한 수 만큼 발생한다.

         /* Log::GetInstance().Write("Network name is no longer available. Closing client pended (IP : %s, Port : %hu, Socket : %hu, Context : %p)",
            Util::AddressToString(client->GetSockAddr()).c_str(),
            Util::PortFromAddress(client->GetSockAddr()),
            client->GetSocket(),
            context);*/

          closed = true;

        }

      }

    }
    else if (bytesTransferred == 0)
    {
      Log::GetInstance().Write(L"Client connection closed. Deleting client pended (IP : %s, Port : %hu, Socket : %u)",
        Util::AddressToString(client->GetSockAddr()).c_str(),
        Util::PortFromAddress(client->GetSockAddr()),
        client->GetSocket());

      closed = true;

    }

    if (!client->ProcessContext(context->op, context, bytesTransferred, closed))
    {
      Log::GetInstance().Write(L"Cannot process context (IP : %s, Port : %hu, Socket : %u)",
        Util::AddressToString(client->GetSockAddr()).c_str(),
        Util::PortFromAddress(client->GetSockAddr()),
        client->GetSocket());

      closed = true;

    }

    if (closed)
    {
      client->SetToBeClosed();

    }

  }

  bool remain = false;

  this->m_server->GetClientProcessor().CollectDeadClient();
  remain = this->m_server->GetClientProcessor().ProcessClientTasks();

  if (ret == FALSE && context == nullptr && remain == false)
  {
    SleepEx(1, FALSE);

  }

  return true;

}
