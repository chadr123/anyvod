/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "stdafx.h"
#include "DataBase.h"
#include "Server.h"
#include "Util.h"
#include "Log.h"
#include "AutoLocker.h"
#include "StatementReseter.h"
#include "FindHandleCloser.h"
#include "..\..\..\common\Analyser.h"

DataBase::DataBase()
:m_server(nullptr)
{
  InitializeCriticalSectionAndSpinCount(&this->m_nextPIDLocker, MAX_SPIN_COUNT);

}

DataBase::~DataBase()
{
  this->Close();
  DeleteCriticalSection(&this->m_nextPIDLocker);

}

StatementCache&
DataBase::GetStatementCache()
{
  return this->m_statCache;

}

void
DataBase::GetUserInfoFromReader(CppSQLite3Query &reader, ANYVOD_USER_INFO *ret)
{
  ret->pid = reader.getInt64Field("PID");
  ret->id = reader.getStringField("ID");
  ret->name = reader.getStringField("Name");
  ret->pass = reader.getStringField("Pass");

}

void
DataBase::GetGroupInfoFromReader(CppSQLite3Query &reader, ANYVOD_GROUP_INFO *ret)
{
  ret->pid = reader.getInt64Field("PID");
  ret->name = reader.getStringField("Name");

}

void
DataBase::GetUserGroupInfoFromReader(CppSQLite3Query &reader, ANYVOD_USER_GROUP_INFO *ret)
{
  ret->pid = reader.getInt64Field("PID");
  ret->userID = reader.getStringField("UserID");
  ret->groupName = reader.getStringField("GroupName");

}

void
DataBase::GetFileGroupInfoFromReader(CppSQLite3Query &reader, ANYVOD_FILE_GROUP_INFO *ret)
{
  ret->pid = reader.getInt64Field("PID");
  ret->groupName = reader.getStringField("GroupName");

}

void
DataBase::GetMovieInfoFromReader(CppSQLite3Query &reader, MOVIE_INFO *ret)
{
  ret->bitRate = reader.getIntField("BitRate");
  ret->totalSize = reader.getInt64Field("TotalSize");
  ret->totalFrame = reader.getIntField("TotalFrame");
  ret->totalTime = reader.getIntField("TotalTime");
  ret->movieType = reader.getStringField("MovieType");
  ret->codecVideo = reader.getStringField("CodecVideo");
  ret->title = reader.getStringField("Title");

}

void
DataBase::GetMovieMetaFromReader(CppSQLite3Query &reader, ANYVOD_ANALYSED_DATA_BLOCK *ret)
{
  ret->blockOffset = reader.getInt64Field("Offset");
  ret->size = reader.getInt64Field("Size");

}

bool
DataBase::Open(const wstring &fileName, Server *server)
{
  try
  {
    wstring path;

    Util::SplitFilePath(fileName, &path, &wstring());

    if (!Util::CreateDirectories(path))
      return false;

    this->m_db.open(fileName.c_str());

    this->m_server = server;
    this->m_statCache.Init(&this->m_db);

    return this->InstallDB();

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Open DB file failed : %s", e.errorMessage());

    return false;

  }

}

void
DataBase::Close()
{
  this->m_statCache.Deinit();
  this->m_db.close();

}

bool DataBase::Vacuum()
{
  CppSQLite3Buffer query;

  try
  {
    Log::GetInstance().Write(L"DB vacuuming");

    query.format("vacuum");

    this->m_db.execQuery(query);

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"DB vacuuming failed : %s", e.errorMessage());

    return false;

  }

  Log::GetInstance().Write(L"Done DB vacuuming");

  return true;

}

ERROR_TYPE
DataBase::FailResonToErrorType(DATABASE_FAIL_REASON reason)
{
  ERROR_TYPE type = ET_UNKNOWN;

  switch (reason)
  {
  case DFR_ALREADY_EXIST:
    {
      type = ET_ALREADY_EXIST_ENTITY;

      break;

    }
  case DFR_NOT_FOUND:
    {
      type = ET_ENTITY_NOT_FOUND;

      break;

    }
  case DFR_DATABASE_ERROR:
    {
      type = ET_CANNOT_ACCESS_DATABASE;

      break;

    }
  case DFR_ASSOCIATED_USER_GROUP:
    {
      type = ET_EXIST_ASSOCIATED_USER_GROUP;

      break;

    }

  }

  return type;

}

bool
DataBase::InstallDB()
{
  CppSQLite3Buffer query;

  try
  {
    query.format("select count(*) from %Q", ACCOUNT_TABLENAME);

    this->m_db.execQuery(query);

    return true;

  }
  catch (CppSQLite3Exception)
  {
    wifstream ifs(SQL_FILENAME);

    ifs.imbue(locale(ifs.getloc(), new codecvt_utf8<wchar_t, 0x10ffff, consume_header>));

    if (ifs.is_open())
    {
      wstring buf;

      try
      {
        this->m_db.begin();

        while (ifs.good())
        {
          bool onlySpace = true;

          getline(ifs, buf);

          for (size_t i = 0; i < buf.size(); i++)
          {
            if (!iswspace(buf[i]))
            {
              onlySpace = false;

              break;

            }

          }

          if (!onlySpace)
          {
            this->m_db.execQuery(buf.c_str());

          }

        }

        this->m_db.commit();

        wcout << L">> " << Log::GetInstance().Write(L"DB installed") << endl;

        wcout << L">> " << Log::GetInstance().Write(L"Updating movie informations") << endl;

        if (this->UpdateMovieInfo(L""))
        {
          wcout << L">> " << Log::GetInstance().Write(L"Updating movie informations completed") << endl;

        }
        else
        {
          wcout << L">> " << Log::GetInstance().Write(L"Cannot update movie informations") << endl;

        }

        return true;

      }
      catch (CppSQLite3Exception &e)
      {
        Log::GetInstance().Write(L"Install DB failed : %s (%s)", e.errorMessage(), buf.c_str());

        try
        {
          this->m_db.rollback();

        }
        catch (CppSQLite3Exception &ee)
        {
          Log::GetInstance().Write(L"Rollback failed : %s", ee.errorMessage());

        }

        return false;

      }

    }
    else
    {
      Log::GetInstance().Write(L"Cannot open SQL script : %s", SQL_FILENAME);

      return false;

    }

  }

}

bool
DataBase::GetMovieInfo(const wstring &filePath, DataBase::MOVIE_INFO *info, DATABASE_FAIL_REASON *reason)
{
  bool success = false;

  try
  {
    CppSQLite3Buffer query("select Title, BitRate, TotalSize, TotalFrame, TotalTime, MovieType, CodecVideo "
      "from %Q where Path=?", MOVIE_INFO_TABLENAME);
    StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
    StatementReseter reset(&this->m_statCache, state);

    state->bind(1, filePath.c_str());

    CppSQLite3Query reader = state->execQuery();

    if (!reader.eof())
    {
      this->GetMovieInfoFromReader(reader, info);

      CppSQLite3Buffer mmquery("select Offset, Size from %Q where movie_info_PID=(select PID from %Q where Path=?)",
        MOVIE_META_TABLENAME, MOVIE_INFO_TABLENAME);
      StatementCache::STATEMENT_ITEM *mmstate = this->m_statCache.GetStatement((const char*)mmquery);
      StatementReseter mmreset(&this->m_statCache, mmstate);

      mmstate->bind(1, filePath.c_str());

      CppSQLite3Query mmreader = mmstate->execQuery();

      while (!mmreader.eof())
      {
        ANYVOD_ANALYSED_DATA_BLOCK block;

        this->GetMovieMetaFromReader(mmreader, &block);

        info->metaDataBlocks.push_back(block);

        mmreader.nextRow();

      }

      *reason = DFR_NONE;
      success = true;

    }
    else
    {
      *reason = DFR_NOT_FOUND;
      success = false;

    }

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Get movie info failed : %s", e.errorMessage());

    *reason = DFR_DATABASE_ERROR;
    success = false;

  }

  return success;

}

bool
DataBase::DeleteMovieInfo(const wstring &filePath, DATABASE_FAIL_REASON *reason)
{
  try
  {
    CppSQLite3Buffer getQuery("select PID from %Q where Path=?", MOVIE_INFO_TABLENAME);
    StatementCache::STATEMENT_ITEM *getState = this->m_statCache.GetStatement((const char*)getQuery);
    StatementReseter getReset(&this->m_statCache, getState);

    getState->bind(1, filePath.c_str());

    this->m_db.begin();

    long long pid = getState->execScalar64();

    if (pid > 0ll)
    {
      CppSQLite3Buffer miQuery("delete from %Q where PID=?", MOVIE_INFO_TABLENAME);
      StatementCache::STATEMENT_ITEM *miState = this->m_statCache.GetStatement((const char*)miQuery);
      StatementReseter miReset(&this->m_statCache, miState);

      miState->bind(1, pid);

      miState->execDML();

      CppSQLite3Buffer mpQuery("delete from %Q where movie_info_PID=?", MOVIE_PERMISSION_TABLENAME);
      StatementCache::STATEMENT_ITEM *mpState = this->m_statCache.GetStatement((const char*)mpQuery);
      StatementReseter mpReset(&this->m_statCache, mpState);

      mpState->bind(1, pid);

      mpState->execDML();

      CppSQLite3Buffer mmQuery("delete from %Q where movie_info_PID=?", MOVIE_META_TABLENAME);
      StatementCache::STATEMENT_ITEM *mmState = this->m_statCache.GetStatement((const char*)mmQuery);
      StatementReseter mmReset(&this->m_statCache, mmState);

      mmState->bind(1, pid);

      mmState->execDML();

      this->m_db.commit();

      Log::GetInstance().Write(L"Deleted %s", filePath.c_str());

      *reason = DFR_NONE;

      return true;

    }
    else
    {
      Log::GetInstance().Write(L"Get PID failed : %s", filePath.c_str());

      this->m_db.rollback();

      *reason = DFR_NOT_FOUND;

      return false;

    }

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Delete movie info failed : %s", e.errorMessage());

    try
    {
      this->m_db.rollback();

    }
    catch (CppSQLite3Exception &ee)
    {
      Log::GetInstance().Write(L"Rollback failed : %s", ee.errorMessage());

    }

    *reason = DFR_DATABASE_ERROR;

    return false;

  }

}

bool
DataBase::RebuildMovieMetaData()
{
  wstring movieDir = this->m_server->GetEnv().movie_dir;
  DWORD attr = GetFileAttributesW(movieDir.c_str());

  if (attr == INVALID_FILE_ATTRIBUTES)
  {
    Log::GetInstance().Write(L"Due to access to movie directory failed, can not rebuild movie metadata. : %s", movieDir.c_str());

    return false;

  }

  this->m_db.begin();

  try
  {
    Log::GetInstance().Write(L"Rebuilding movie metadata started");

    CppSQLite3Buffer deleteMetaQuery("delete from %Q", MOVIE_META_TABLENAME);
    StatementCache::STATEMENT_ITEM *deleteMetaState = this->m_statCache.GetStatement((const char*)deleteMetaQuery);
    StatementReseter deleteMetaReset(&this->m_statCache, deleteMetaState);

    deleteMetaState->execDML();

    CppSQLite3Buffer selectMovieInfoQuery("select Path, MovieType from %Q", MOVIE_INFO_TABLENAME);
    StatementCache::STATEMENT_ITEM *selectMovieInfoState = this->m_statCache.GetStatement((const char*)selectMovieInfoQuery);
    StatementReseter selectMovieInfoReset(&this->m_statCache, selectMovieInfoState);
    CppSQLite3Query reader = selectMovieInfoState->execQuery();

    while (!reader.eof())
    {
      wstring physicalPath;
      wstring logicalPath;
      wstring movieType;

      Util::AppendDirSeparator(&movieDir);

      logicalPath = reader.getStringField("Path");
      movieType = reader.getStringField("MovieType");

      Util::BindPath(movieDir, logicalPath, &physicalPath);

      VECTOR_ANYVOD_ANALYSED_DATA_BLOCK metaData;

      if (!movieType.empty())
      {
        Analyser *analyser = this->m_server->GetAnalyserLoader().GetAnalyser(movieType);

        if (analyser == nullptr)
        {
          Log::GetInstance().Write(L"Can not find analyser : %s", movieType.c_str());

        }
        else
        {
          if (!analyser->Analyse(physicalPath, &metaData))
          {
            Log::GetInstance().Write(L"Can not analyse movie : %s. Skipping...", physicalPath.c_str());

            metaData = VECTOR_ANYVOD_ANALYSED_DATA_BLOCK();

          }

        }

      }

      DATABASE_FAIL_REASON reason;
      bool success = false;

      for (size_t i = 0; i < metaData.size(); i++)
      {
        ANYVOD_ANALYSED_DATA_BLOCK &block = metaData[i];

        if (block.size <= 0)
          continue;

        unsigned long long movieMetaPID = this->GetNextPIDValue(&success, &reason);

        if (movieMetaPID > 0ull && success)
        {
          CppSQLite3Buffer mmQuery("insert into %Q (PID, movie_info_PID, Offset, Size) "
            "values (?, (select PID from %Q where Path=?), ?, ?)",
            MOVIE_META_TABLENAME, MOVIE_INFO_TABLENAME);
          StatementCache::STATEMENT_ITEM *mmState = this->m_statCache.GetStatement((const char*)mmQuery);
          StatementReseter mmReset(&this->m_statCache, mmState);

          mmState->bind(1, (long long)movieMetaPID);
          mmState->bind(2, logicalPath.c_str());
          mmState->bind(3, block.blockOffset);
          mmState->bind(4, block.size);

          mmState->execDML();

        }
        else
        {
          Log::GetInstance().Write(L"Cannot get next PID from %S", MOVIE_META_TABLENAME);

          this->m_db.rollback();

          return false;

        }

      }

      if (metaData.size() > 0)
        Log::GetInstance().Write(L"Rebuilt : %s", physicalPath.c_str());
      else
        Log::GetInstance().Write(L"Metadata are empty : %s", physicalPath.c_str());

      reader.nextRow();

    }

    Log::GetInstance().Write(L"Rebuilding movie metadata succeed");
    this->m_db.commit();

    return true;

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Rebuilding movie metadata failed : %s", e.errorMessage());
    this->m_db.rollback();

    return false;

  }

}

bool
DataBase::RebuildMovieData()
{  
  this->m_db.begin();

  try
  {
    Log::GetInstance().Write(L"Rebuilding movie data started");

    CppSQLite3Buffer deleteInfoQuery("delete from %Q", MOVIE_INFO_TABLENAME);
    StatementCache::STATEMENT_ITEM *deleteInfoState = this->m_statCache.GetStatement((const char*)deleteInfoQuery);
    StatementReseter deleteInfoReset(&this->m_statCache, deleteInfoState);

    deleteInfoState->execDML();

    CppSQLite3Buffer deleteMetaQuery("delete from %Q", MOVIE_META_TABLENAME);
    StatementCache::STATEMENT_ITEM *deleteMetaState = this->m_statCache.GetStatement((const char*)deleteMetaQuery);
    StatementReseter deleteMetaReset(&this->m_statCache, deleteMetaState);

    deleteMetaState->execDML();

    CppSQLite3Buffer deletePermQuery("delete from %Q", MOVIE_PERMISSION_TABLENAME);
    StatementCache::STATEMENT_ITEM *deletePermState = this->m_statCache.GetStatement((const char*)deletePermQuery);
    StatementReseter deletePermReset(&this->m_statCache, deletePermState);

    deletePermState->execDML();

    this->m_db.commit();

    if (this->UpdateMovieInfo(L""))
    {
      Log::GetInstance().Write(L"Rebuilding movie data succeed");

      return true;

    }
    else
    {
      Log::GetInstance().Write(L"Can not build movie data");      

      return false;

    }

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Rebuilding movie data failed : %s", e.errorMessage());
    this->m_db.rollback();

    return false;

  }

}

bool
DataBase::UpdateMovieInfo(const wstring &root)
{
  wstring movieDir = this->m_server->GetEnv().movie_dir;
  DWORD attr = GetFileAttributesW(movieDir.c_str());

  if (attr == INVALID_FILE_ATTRIBUTES)
  {
    Log::GetInstance().Write(L"Due to access to movie directory failed, can not update movie info. : %s", movieDir.c_str());

    return false;

  }

  try
  {
    Log::GetInstance().Write(L"Updating movie info started");

    CppSQLite3Buffer query("select Path from %Q", MOVIE_INFO_TABLENAME);
    StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
    StatementReseter reset(&this->m_statCache, state);
    CppSQLite3Query reader = state->execQuery();

    while (!reader.eof())
    {
      wstring physicalPath;
      wstring logicalPath;
      wstring filePath;

      Util::AppendDirSeparator(&movieDir);

      logicalPath = reader.getStringField("Path");

      Util::BindPath(movieDir, logicalPath, &physicalPath);
      Util::SplitFilePath(physicalPath, &filePath, &wstring());

      DWORD fileAttr = GetFileAttributesW(filePath.c_str());
      bool toBeDelete = false;

      if (fileAttr == INVALID_FILE_ATTRIBUTES)
      {
        toBeDelete = true;

      }
      else
      {
        if ((IS_BIT_SET(fileAttr, FILE_ATTRIBUTE_HIDDEN) ||
          IS_BIT_SET(fileAttr, FILE_ATTRIBUTE_SYSTEM)) && !Util::IsRoot(filePath))
        {
          toBeDelete = true;

        }
        else
        {
          fileAttr = GetFileAttributesW(physicalPath.c_str());

          if (fileAttr == INVALID_FILE_ATTRIBUTES)
          {
            toBeDelete = true;

          }
          else
          {
            if (IS_BIT_SET(fileAttr, FILE_ATTRIBUTE_HIDDEN) || IS_BIT_SET(fileAttr, FILE_ATTRIBUTE_SYSTEM))
            {
              toBeDelete = true;

            }
            else
            {
              toBeDelete = false;

            }

          }

        }

      }

      if (toBeDelete)
      {
        DataBase::DATABASE_FAIL_REASON reason;

        if (!this->DeleteMovieInfo(logicalPath, &reason))
        {
          return false;

        }

      }

      reader.nextRow();

    }

    if (this->UpdateMovieInfoInternal(root, 0))
    {
      Log::GetInstance().Write(L"Updating movie info succeed");

      return true;

    }
    else
    {
      Log::GetInstance().Write(L"Updating movie info failed");

      return false;

    }

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Updating movie info failed : %s", e.errorMessage());

    return false;

  }

}

bool
DataBase::UpdateMovieInfoInternal(const wstring &startPath, int tabSize)
{
  wstring tabs;
  wstring root = startPath;

  for (int i = 0; i < tabSize; i++)
  {
    tabs += L" ";

  }

  if (!(root.size() == 1 && root[0] == DIRECTORY_DELIMITER))
  {
    Util::AppendDirSeparator(&root);

  }

  wstring movieDir = this->m_server->GetEnv().movie_dir;
  wstring searchPath;

  Util::AppendDirSeparator(&movieDir);
  Util::BindPath(movieDir, root, &searchPath);

  searchPath.append(L"*");

  const ENV_INFO &env = this->m_server->GetEnv();
  WIN32_FIND_DATAW findData;
  HANDLE find = FindFirstFileW(searchPath.c_str(), &findData);
  FindHandleCloser closer(find);

  if (find == INVALID_HANDLE_VALUE)
  {
    Log::GetInstance().Write(L"FindFirstFileW failed : \"%s\" Path : %s",
      Util::SystemErrorToString(GetLastError()).c_str(), searchPath.c_str());

  }
  else
  {
    do
    {
      if (IS_BIT_SET(findData.dwFileAttributes, FILE_ATTRIBUTE_HIDDEN) ||
        IS_BIT_SET(findData.dwFileAttributes, FILE_ATTRIBUTE_SYSTEM))
      {
        continue;

      }

      if (IS_BIT_SET(findData.dwFileAttributes, FILE_ATTRIBUTE_DIRECTORY))
      {
        if (wcscmp(findData.cFileName, L".") == 0 || wcscmp(findData.cFileName, L"..") == 0)
        {
          continue;

        }

        if (!this->UpdateMovieInfoInternal(root + findData.cFileName, tabSize + 1))
        {
          return false;

        }

      }
      else
      {
        if (!Util::IsMatchExt(findData.cFileName, env.movie_exts))
          continue;

        wstring filePath = root + findData.cFileName;
        wstring physicalPath;

        Util::BindPath(movieDir, filePath, &physicalPath);

        this->m_db.begin();

        try
        {
          CppSQLite3Buffer getQuery("select LastWriteTime from %Q where Path=?", MOVIE_INFO_TABLENAME);
          StatementCache::STATEMENT_ITEM *getState = this->m_statCache.GetStatement((const char*)getQuery);
          StatementReseter getReset(&this->m_statCache, getState);

          getState->bind(1, filePath.c_str());

          CppSQLite3Query reader = getState->execQuery();
          MOVIE_INFO info;

          if (!reader.eof())
          {
            LARGE_INTEGER time;

            time.LowPart = findData.ftLastWriteTime.dwLowDateTime;
            time.HighPart = findData.ftLastWriteTime.dwHighDateTime;

            //업데이트
            if (time.QuadPart != reader.getInt64Field("LastWriteTime"))
            {
              if (!Util::GetMediaInfo(physicalPath, &info))
              {
                Log::GetInstance().Write(L"Cannot get media info : %s", physicalPath.c_str());

                this->m_db.rollback();

                continue;

              }

              VECTOR_ANYVOD_ANALYSED_DATA_BLOCK metaData;

              if (!info.movieType.empty())
              {
                Analyser *analyser = this->m_server->GetAnalyserLoader().GetAnalyser(info.movieType);

                if (analyser == nullptr)
                {
                  Log::GetInstance().Write(L"Can not find analyser : %s", info.movieType.c_str());

                }
                else
                {
                  if (!analyser->Analyse(physicalPath, &metaData))
                  {
                    Log::GetInstance().Write(L"Can not analyse movie : %s. Skipping...", physicalPath.c_str());

                    metaData = VECTOR_ANYVOD_ANALYSED_DATA_BLOCK();

                  }

                }

              }

              CppSQLite3Buffer updateQuery("update %Q set Title=?, TotalSize=?, TotalFrame=?, TotalTime=?, BitRate=?, MovieType=?, CodecVideo=?, LastWriteTime=? "
                "where Path=?", MOVIE_INFO_TABLENAME);
              StatementCache::STATEMENT_ITEM *updateState = this->m_statCache.GetStatement((const char*)updateQuery);
              StatementReseter updateReset(&this->m_statCache, updateState);
              LARGE_INTEGER lastFileTime;

              lastFileTime.LowPart = findData.ftLastWriteTime.dwLowDateTime;
              lastFileTime.HighPart = findData.ftLastWriteTime.dwHighDateTime;

              updateState->bind(1, info.title.c_str());
              updateState->bind(2, info.totalSize);
              updateState->bind(3, info.totalFrame);
              updateState->bind(4, info.totalTime);
              updateState->bind(5, info.bitRate);
              updateState->bind(6, info.movieType.c_str());
              updateState->bind(7, info.codecVideo.c_str());
              updateState->bind(8, lastFileTime.QuadPart);
              updateState->bind(9, filePath.c_str());

              updateState->execDML();

              CppSQLite3Buffer deleteMetaQuery("delete from %Q where movie_info_PID=(select PID from %Q where Path=?)",
                MOVIE_META_TABLENAME, MOVIE_INFO_TABLENAME);
              StatementCache::STATEMENT_ITEM *deleteMetaState = this->m_statCache.GetStatement((const char*)deleteMetaQuery);
              StatementReseter deleteMetaReset(&this->m_statCache, deleteMetaState);

              deleteMetaState->bind(1, filePath.c_str());

              deleteMetaState->execDML();

              DATABASE_FAIL_REASON reason;
              bool success = false;

              for (size_t i = 0; i < metaData.size(); i++)
              {
                ANYVOD_ANALYSED_DATA_BLOCK &block = metaData[i];

                if (block.size <= 0)
                  continue;

                unsigned long long movieMetaPID = this->GetNextPIDValue(&success, &reason);

                if (movieMetaPID > 0ull && success)
                {
                  CppSQLite3Buffer mmQuery("insert into %Q (PID, movie_info_PID, Offset, Size) "
                    "values (?, (select PID from %Q where Path=?), ?, ?)",
                    MOVIE_META_TABLENAME, MOVIE_INFO_TABLENAME);
                  StatementCache::STATEMENT_ITEM *mmState = this->m_statCache.GetStatement((const char*)mmQuery);
                  StatementReseter mmReset(&this->m_statCache, mmState);

                  mmState->bind(1, (long long)movieMetaPID);
                  mmState->bind(2, filePath.c_str());
                  mmState->bind(3, block.blockOffset);
                  mmState->bind(4, block.size);

                  mmState->execDML();

                }
                else
                {
                  Log::GetInstance().Write(L"%sCannot get next PID from %S", tabs.c_str(), MOVIE_META_TABLENAME);

                  this->m_db.rollback();

                  return false;

                }

              }

              Log::GetInstance().Write(L"%sUpdated : %s", tabs.c_str(), filePath.c_str());

            }

            this->m_db.commit();

          }
          else
          {
            if (!Util::GetMediaInfo(physicalPath, &info))
            {
              Log::GetInstance().Write(L"Cannot get media info : %s", physicalPath.c_str());

              this->m_db.rollback();

              continue;

            }

            VECTOR_ANYVOD_ANALYSED_DATA_BLOCK metaData;

            if (!info.movieType.empty())
            {
              Analyser *analyser = this->m_server->GetAnalyserLoader().GetAnalyser(info.movieType);

              if (analyser == nullptr)
              {
                Log::GetInstance().Write(L"Can not find analyser : %s", info.movieType.c_str());

              }
              else
              {
                if (!analyser->Analyse(physicalPath, &metaData))
                {
                  Log::GetInstance().Write(L"Can not analyse movie : %s. Skipping...", physicalPath.c_str());

                  metaData = VECTOR_ANYVOD_ANALYSED_DATA_BLOCK();

                }

              }

            }

            DATABASE_FAIL_REASON reason;
            bool success = false;
            unsigned long long movieInfoPID = this->GetNextPIDValue(&success, &reason);

            if (movieInfoPID > 0ull && success)
            {
              CppSQLite3Buffer miQuery("insert into %Q (PID, Path, Title, TotalSize, TotalFrame, TotalTime, BitRate, MovieType, CodecVideo, LastWriteTime) "
                "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", MOVIE_INFO_TABLENAME);
              StatementCache::STATEMENT_ITEM *miState = this->m_statCache.GetStatement((const char*)miQuery);
              StatementReseter miReset(&this->m_statCache, miState);
              LARGE_INTEGER lastFileTime;

              lastFileTime.LowPart = findData.ftLastWriteTime.dwLowDateTime;
              lastFileTime.HighPart = findData.ftLastWriteTime.dwHighDateTime;

              miState->bind(1, (long long)movieInfoPID);
              miState->bind(2, filePath.c_str());
              miState->bind(3, info.title.c_str());
              miState->bind(4, info.totalSize);
              miState->bind(5, info.totalFrame);
              miState->bind(6, info.totalTime);
              miState->bind(7, info.bitRate);
              miState->bind(8, info.movieType.c_str());
              miState->bind(9, info.codecVideo.c_str());
              miState->bind(10, lastFileTime.QuadPart);

              miState->execDML();

              for (size_t i = 0; i < metaData.size(); i++)
              {
                ANYVOD_ANALYSED_DATA_BLOCK &block = metaData[i];

                if (block.size <= 0)
                  continue;

                unsigned long long movieMetaPID = this->GetNextPIDValue(&success, &reason);

                if (movieMetaPID > 0ull && success)
                {
                  CppSQLite3Buffer mmQuery("insert into %Q (PID, movie_info_PID, Offset, Size) values (?, ?, ?, ?)",
                    MOVIE_META_TABLENAME);
                  StatementCache::STATEMENT_ITEM *mmState = this->m_statCache.GetStatement((const char*)mmQuery);
                  StatementReseter mmReset(&this->m_statCache, mmState);

                  mmState->bind(1, (long long)movieMetaPID);
                  mmState->bind(2, (long long)movieInfoPID);
                  mmState->bind(3, block.blockOffset);
                  mmState->bind(4, block.size);

                  mmState->execDML();

                }
                else
                {
                  Log::GetInstance().Write(L"%sCannot get next PID from %S", tabs.c_str(), MOVIE_META_TABLENAME);

                  this->m_db.rollback();

                  return false;

                }

              }

              unsigned long long permissionPID = this->GetNextPIDValue(&success, &reason);

              if (permissionPID > 0ll && success)
              {
                CppSQLite3Buffer maxQuery("select g.Name as g_Name, count(mp.PID) as count_PID from "
                  "%Q as mp, %Q as mi, %Q as g "
                  "where mi.Path like ? and mi.Path not like ? "
                  "and mi.PID=mp.movie_info_PID and g.PID=mp.groups_PID "
                  "group by mp.groups_PID order by count_PID desc, g.PermissionOrder asc limit 1;",
                  MOVIE_PERMISSION_TABLENAME, MOVIE_INFO_TABLENAME, GROUPS_TABLENAME);
                StatementCache::STATEMENT_ITEM *maxState = this->m_statCache.GetStatement((const char*)maxQuery);
                StatementReseter maxReset(&this->m_statCache, maxState);
                wstring groupName;
                wstring firstArg = root + L"%.%";
                wstring secondArg = root + L"%" + DIRECTORY_DELIMITER +  L"%";

                maxState->bind(1, firstArg.c_str());
                maxState->bind(2, secondArg.c_str());

                CppSQLite3Query nameReader = maxState->execQuery();

                groupName = nameReader.getStringField("g_Name", L"");

                if (groupName == L"")
                {
                  groupName = PUBLIC_GROUP_NAME;

                }

                CppSQLite3Buffer mpQuery("insert into %Q (PID, movie_info_PID, groups_PID) "
                  "values (?, ?, (select PID from %Q where Name=?))",
                  MOVIE_PERMISSION_TABLENAME, GROUPS_TABLENAME);
                StatementCache::STATEMENT_ITEM *mpState = this->m_statCache.GetStatement((const char*)mpQuery);
                StatementReseter mpReset(&this->m_statCache, mpState);

                mpState->bind(1, (long long)permissionPID);
                mpState->bind(2, (long long)movieInfoPID);
                mpState->bind(3, groupName.c_str());

                mpState->execDML();

                this->m_db.commit();

                Log::GetInstance().Write(L"%sInserted : %s", tabs.c_str(), filePath.c_str());

              }
              else
              {
                Log::GetInstance().Write(L"%sCannot get next PID from %S", tabs.c_str(), MOVIE_PERMISSION_TABLENAME);

                this->m_db.rollback();

                return false;

              }

            }
            else
            {
              Log::GetInstance().Write(L"%sCannot get next PID from %S", tabs.c_str(), MOVIE_INFO_TABLENAME);

              this->m_db.rollback();

              return false;

            }

          }

        }
        catch (CppSQLite3Exception &e)
        {
          Log::GetInstance().Write(L"%s%s", tabs.c_str(), e.errorMessage());
          Log::GetInstance().Write(L"%sInserting or Updating failed : %s", tabs.c_str(), root.c_str());

          try
          {
            this->m_db.rollback();

          }
          catch (CppSQLite3Exception &ee)
          {
            Log::GetInstance().Write(L"Rollback failed : %s", ee.errorMessage());

          }

          return false;

        }

      }

    } while (FindNextFileW(find, &findData));

    if (GetLastError() != ERROR_NO_MORE_FILES)
    {
      Log::GetInstance().Write(L"FindFirstFileW failed : \"%s\" Path : %s",
        Util::SystemErrorToString(GetLastError()).c_str(), root.c_str());

    }

  }

  return true;

}

unsigned long long
DataBase::AddAdmin(unsigned long long userPID, bool useTransaction, bool *success, DATABASE_FAIL_REASON *reason)
{
  unsigned long long pid = 0;

  try
  {
    if (useTransaction)
      this->m_db.begin();

    if (!this->IsAdmin(userPID, reason))
    {
      if (*reason == DFR_NOT_FOUND)
      {
        pid = this->GetNextPIDValue(success, reason);

        if (pid > 0ll && *success)
        {
          CppSQLite3Buffer query("insert into %Q (PID, account_PID) values (?, ?)", ADMINS_TABLENAME);
          StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
          StatementReseter reset(&this->m_statCache, state);

          state->bind(1, (long long)pid);
          state->bind(2, (long long)userPID);

          state->execDML();

          *reason = DFR_NONE;
          *success = true;

          if (useTransaction)
            this->m_db.commit();

        }
        else
        {
          *success = false;

          if (useTransaction)
            this->m_db.rollback();

        }

      }
      else
      {
        *success = false;

        if (useTransaction)
          this->m_db.rollback();

      }

    }
    else
    {
      *reason = DFR_ALREADY_EXIST;
      *success = false;

      if (useTransaction)
        this->m_db.rollback();

    }

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Add admin info failed : %s", e.errorMessage());

    *success = false;

    try
    {
      if (useTransaction)
        this->m_db.rollback();

    }
    catch (CppSQLite3Exception &ee)
    {
      Log::GetInstance().Write(L"Rollback failed : %s", ee.errorMessage());

    }

  }

  return pid;

}

bool
DataBase::DeleteAdmin(unsigned long long userPID, DATABASE_FAIL_REASON *reason)
{
  try
  {
    CppSQLite3Buffer query("delete from %Q where account_PID=?", ADMINS_TABLENAME);
    StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
    StatementReseter reset(&this->m_statCache, state);

    state->bind(1, (long long)userPID);

    if (state->execDML() > 0)
    {
      *reason = DFR_NONE;

      return true;

    }
    else
    {
      *reason = DFR_NOT_FOUND;

      return false;

    }

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Delete admin info failed : %s", e.errorMessage());

    return false;

  }

}

unsigned long long
DataBase::AddUser(const ANYVOD_USER_INFO &info, bool useTransaction, bool *success, DATABASE_FAIL_REASON *reason)
{
  unsigned long long pid = 0;

  try
  {
    if (useTransaction)
      this->m_db.begin();

    if (!this->ExistUser(info.id, reason) && !this->ExistGroup(info.id, reason))
    {
      if (*reason == DFR_NOT_FOUND)
      {
        pid = this->GetNextPIDValue(success, reason);

        if (pid > 0ll && *success)
        {
          CppSQLite3Buffer query("insert into %Q (PID, ID, Name, Pass) values (?, ?, ?, ?)", ACCOUNT_TABLENAME);
          StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
          StatementReseter reset(&this->m_statCache, state);
          wstring password;

          this->GetPassword(info.pass, &password);

          if (info.pass.empty() || !password.empty())
          {
            state->bind(1, (long long)pid);
            state->bind(2, info.id.c_str());
            state->bind(3, info.name.c_str());
            state->bind(4, password.c_str());

            state->execDML();

            this->AddGroup(info.id, !useTransaction, success, reason);

          }

          if (*success)
          {
            this->AddUserToGroup(pid, PUBLIC_GROUP_NAME, !useTransaction, success, reason);

            if (*success)
            {
              this->AddUserToGroup(pid, info.id, !useTransaction, success, reason);

              if (*success)
              {
                if (info.admin)
                {
                  this->AddAdmin(pid, !useTransaction, success, reason);

                }

                if (*success)
                {
                  *reason = DFR_NONE;
                  *success = true;

                  if (useTransaction)
                    this->m_db.commit();

                }
                else
                {
                  *success = false;

                  if (useTransaction)
                    this->m_db.rollback();

                }

              }
              else
              {
                *success = false;

                if (useTransaction)
                  this->m_db.rollback();

              }

            }
            else
            {
              *success = false;

              if (useTransaction)
                this->m_db.rollback();

            }

          }
          else
          {
            *success = false;

            if (useTransaction)
              this->m_db.rollback();

          }

        }
        else
        {
          *success = false;

          if (useTransaction)
            this->m_db.rollback();

        }

      }
      else
      {
        *success = false;

        if (useTransaction)
          this->m_db.rollback();

      }

    }
    else
    {
      *reason = DFR_ALREADY_EXIST;
      *success = false;

      if (useTransaction)
        this->m_db.rollback();


    }

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Add user info failed : %s", e.errorMessage());

    *reason = DFR_DATABASE_ERROR;
    *success = false;

    try
    {
      if (useTransaction)
        this->m_db.rollback();

    }
    catch (CppSQLite3Exception &ee)
    {
      Log::GetInstance().Write(L"Rollback failed : %s", ee.errorMessage());

    }

  }

  return pid;

}

bool
DataBase::DeleteUser(unsigned long long pid, DATABASE_FAIL_REASON *reason)
{
  try
  {
    this->m_db.begin();

    ANYVOD_USER_INFO info;

    if (this->GetUserInfo(pid, &info, reason))
    {
      this->DeletePermissionFromUserGroup(info.id, reason);

      if (*reason != DFR_DATABASE_ERROR)
      {
        if (this->DeleteUserFromGroup(pid, info.id, reason))
        {
          if (this->DeleteUserFromGroup(pid, PUBLIC_GROUP_NAME, reason))
          {
            if (this->DeleteGroup(info.id, reason))
            {
              if (this->IsAdmin(info.id, reason))
              {
                if (!this->DeleteAdmin(pid, reason))
                {
                  this->m_db.rollback();

                  return false;

                }

              }

              CppSQLite3Buffer query("delete from %Q where PID=?", ACCOUNT_TABLENAME);
              StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
              StatementReseter reset(&this->m_statCache, state);

              state->bind(1, (long long)pid);

              int count = state->execDML();

              if (count > 0)
              {
                this->m_db.commit();

                *reason = DFR_NONE;

                return true;

              }
              else
              {
                this->m_db.rollback();

                *reason = DFR_NOT_FOUND;

                return false;

              }

            }
            else
            {
              this->m_db.rollback();

              return false;

            }

          }
          else
          {
            this->m_db.rollback();

            return false;

          }

        }
        else
        {
          this->m_db.rollback();

          return false;

        }

      }
      else
      {
        this->m_db.rollback();

        return false;

      }

    }
    else
    {
      this->m_db.rollback();

      return false;

    }

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Delete user info failed : %s", e.errorMessage());

    try
    {
      this->m_db.rollback();

    }
    catch (CppSQLite3Exception &ee)
    {
      Log::GetInstance().Write(L"Rollback failed : %s", ee.errorMessage());

    }

    *reason = DFR_DATABASE_ERROR;

    return false;

  }

}

bool
DataBase::ModifyUser(const ANYVOD_USER_INFO &info, DATABASE_FAIL_REASON *reason)
{
  try
  {
    bool success = false;

    this->m_db.begin();

    if (info.pass.size() == 0)
    {
      //암호 업데이트 안함
      CppSQLite3Buffer query("update %Q set Name=? where PID=?", ACCOUNT_TABLENAME);
      StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
      StatementReseter reset(&this->m_statCache, state);

      state->bind(1, info.name.c_str());
      state->bind(2, (long long)info.pid);

      success = state->execDML() > 0;

    }
    else
    {
      CppSQLite3Buffer query("update %Q set Name=?, Pass=? where PID=?", ACCOUNT_TABLENAME);
      StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
      StatementReseter reset(&this->m_statCache, state);
      wstring password;

      this->GetPassword(info.pass, &password);

      if (info.pass.empty() || !password.empty())
      {
        state->bind(1, info.name.c_str());
        state->bind(2, password.c_str());
        state->bind(3, (long long)info.pid);

        success = state->execDML() > 0;

      }

    }

    if (success)
    {
      bool isAdmin = this->IsAdmin(info.pid, reason);

      if (*reason != DFR_DATABASE_ERROR)
      {
        if (info.admin && !isAdmin)
        {
          this->AddAdmin(info.pid, false, &success, reason);

        }
        else if (!info.admin && isAdmin)
        {
          success = this->DeleteAdmin(info.pid, reason);

        }

        if (success)
        {
          this->m_db.commit();

          *reason = DFR_NONE;

          return true;

        }
        else
        {
          this->m_db.rollback();

          return false;

        }

      }
      else
      {
        this->m_db.rollback();

        return false;

      }

    }
    else
    {
      *reason = DFR_NOT_FOUND;

      this->m_db.rollback();

      return false;

    }

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Modify user info failed : %s", e.errorMessage());

    try
    {
      this->m_db.rollback();

    }
    catch (CppSQLite3Exception &ee)
    {
      Log::GetInstance().Write(L"Rollback failed : %s", ee.errorMessage());

    }

    *reason = DFR_DATABASE_ERROR;

    return false;

  }

  return false;

}

bool
DataBase::GetUserInfo(unsigned long long pid, ANYVOD_USER_INFO *ret, DATABASE_FAIL_REASON *reason)
{
  try
  {
    CppSQLite3Buffer query("select PID, ID, Name, Pass from %Q where PID=?", ACCOUNT_TABLENAME);
    StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
    StatementReseter reset(&this->m_statCache, state);

    state->bind(1, (long long)pid);

    CppSQLite3Query reader = state->execQuery();

    if (!reader.eof())
    {
      this->GetUserInfoFromReader(reader, ret);

      ret->admin = this->IsAdmin(ret->id, reason);

      if (*reason == DFR_DATABASE_ERROR)
      {
        return false;

      }
      else
      {
        *reason = DFR_NONE;

        return true;

      }

    }
    else
    {
      *reason = DFR_NOT_FOUND;

      return false;

    }

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Get user info failed : %s", e.errorMessage());

    *reason = DFR_DATABASE_ERROR;

    return false;

  }

}

bool
DataBase::GetUserInfos(VECTOR_ANYVOD_USER_INFO *ret, DATABASE_FAIL_REASON *reason)
{
  try
  {
    CppSQLite3Buffer query("select PID, ID, Name, Pass from %Q", ACCOUNT_TABLENAME);
    StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
    StatementReseter reset(&this->m_statCache, state);
    CppSQLite3Query reader = state->execQuery();

    while (!reader.eof())
    {
      ANYVOD_USER_INFO info;

      this->GetUserInfoFromReader(reader, &info);

      info.admin = this->IsAdmin(info.id, reason);

      if (*reason == DFR_DATABASE_ERROR)
      {
        return false;

      }

      ret->push_back(info);

      reader.nextRow();

    }

    *reason = DFR_NONE;

    return true;

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Get user infos failed : %s", e.errorMessage());

    *reason = DFR_DATABASE_ERROR;

    return false;

  }

}

bool
DataBase::ExistUser(const wstring &id, DATABASE_FAIL_REASON *reason)
{
  try
  {
    CppSQLite3Buffer query("select exists(select 1 from %Q where ID=? limit 1)", ACCOUNT_TABLENAME);
    StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
    StatementReseter reset(&this->m_statCache, state);

    state->bind(1, id.c_str());

    if (state->execScalar() > 0)
    {
      *reason = DFR_NONE;

      return true;

    }
    else
    {
      *reason = DFR_NOT_FOUND;

      return false;

    }

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Exist user failed : %s", e.errorMessage());

    *reason = DFR_DATABASE_ERROR;

    return false;

  }

}

bool
DataBase::FindUser(const wstring &id, ANYVOD_USER_INFO *ret, DATABASE_FAIL_REASON *reason)
{
  try
  {
    CppSQLite3Buffer query("select PID, ID, Name, Pass from %Q where ID=?", ACCOUNT_TABLENAME);
    StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
    StatementReseter reset(&this->m_statCache, state);

    state->bind(1, id.c_str());

    CppSQLite3Query reader = state->execQuery();

    if (!reader.eof())
    {
      this->GetUserInfoFromReader(reader, ret);

      *reason = DFR_NONE;

      return true;

    }
    else
    {
      *reason = DFR_NOT_FOUND;

      return false;

    }

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Find user failed : %s", e.errorMessage());

    *reason = DFR_DATABASE_ERROR;

    return false;

  }

}

unsigned long long
DataBase::GetNextPIDValue(bool *success, DATABASE_FAIL_REASON *reason)
{
  unsigned long long pid = 0;
  AutoLocker locker(&this->m_nextPIDLocker);

  try
  {
    CppSQLite3Buffer query("insert into %Q (DUMMY) values ('d')", SEQUENCE_TABLENAME);
    StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
    StatementReseter reset(&this->m_statCache, state);

    state->execDML();

    pid = this->m_db.lastRowId();

    *reason = DFR_NONE;
    *success = true;

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Get next value failed : %s", e.errorMessage());

    *reason = DFR_DATABASE_ERROR;
    *success = false;

  }

  return pid;

}

bool
DataBase::GetFileGroupInfos(const wstring &filePath, VECTOR_ANYVOD_FILE_GROUP_INFO *ret, DATABASE_FAIL_REASON *reason)
{
  try
  {
    CppSQLite3Buffer query("select mp.PID, g.Name as GroupName from %Q as mp, %Q as g, %Q as mi "
    "where mp.movie_info_PID=mi.PID and mp.groups_PID=g.PID and mi.Path=?",
    MOVIE_PERMISSION_TABLENAME, GROUPS_TABLENAME, MOVIE_INFO_TABLENAME);
    StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
    StatementReseter reset(&this->m_statCache, state);

    state->bind(1, filePath.c_str());

    CppSQLite3Query reader = state->execQuery();

    while (!reader.eof())
    {
      ANYVOD_FILE_GROUP_INFO info;

      this->GetFileGroupInfoFromReader(reader, &info);

      if (*reason == DFR_DATABASE_ERROR)
      {
        return false;

      }

      ret->push_back(info);

      reader.nextRow();

    }

    *reason = DFR_NONE;

    return true;

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Get file group infos failed : %s", e.errorMessage());

    *reason = DFR_DATABASE_ERROR;

    return false;

  }

}

bool
DataBase::HasPermission(const wstring &filePath, const wstring &userID, DATABASE_FAIL_REASON *reason)
{
  bool isAdmin = this->IsAdmin(userID, reason);

  if (*reason != DFR_NONE)
  {
    return false;

  }
  else if (isAdmin)
  {
    *reason = DFR_NONE;

    return true;

  }

  try
  {
    CppSQLite3Buffer query("select exists(select 1 from %Q as a, %Q as g, %Q as ug, %Q as mi, %Q as mp "
      "where a.PID=ug.account_PID and g.PID=ug.groups_PID and mi.PID=mp.movie_info_PID and g.PID=mp.groups_PID and "
      "(g.Name=? or a.ID=?) and mi.Path=? limit 1)",
      ACCOUNT_TABLENAME, GROUPS_TABLENAME, USER_GROUP_TABLENAME,
      MOVIE_INFO_TABLENAME, MOVIE_PERMISSION_TABLENAME);
    StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
    StatementReseter reset(&this->m_statCache, state);

    state->bind(1, PUBLIC_GROUP_NAME);
    state->bind(2, userID.c_str());
    state->bind(3, filePath.c_str());

    if (state->execScalar() > 0)
    {
      *reason = DFR_NONE;

      return true;

    }
    else
    {
      *reason = DFR_NONE;

      return false;

    }

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Has permission failed : %s", e.errorMessage());

    *reason = DFR_DATABASE_ERROR;

    return false;

  }

}

bool
DataBase::HasDirectoryPermission(const wstring &path, const wstring &userID, DATABASE_FAIL_REASON *reason)
{
  bool isAdmin = this->IsAdmin(userID, reason);

  if (*reason != DFR_NONE)
  {
    return false;

  }
  else if (isAdmin)
  {
    *reason = DFR_NONE;

    return true;

  }

  try
  {
    CppSQLite3Buffer query("select exists(select 1 from %Q as a, %Q as g, %Q as ug, %Q as mi, %Q as mp "
      "where a.PID=ug.account_PID and g.PID=ug.groups_PID and mi.PID=mp.movie_info_PID and g.PID=mp.groups_PID and "
      "(g.Name=? or a.ID=?) and mi.Path like ? limit 1)",
      ACCOUNT_TABLENAME, GROUPS_TABLENAME, USER_GROUP_TABLENAME,
      MOVIE_INFO_TABLENAME, MOVIE_PERMISSION_TABLENAME);
    StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
    StatementReseter reset(&this->m_statCache, state);
    wstring arg = path + L"%";

    state->bind(1, PUBLIC_GROUP_NAME);
    state->bind(2, userID.c_str());
    state->bind(3, arg.c_str());

    if (state->execScalar() > 0)
    {
      *reason = DFR_NONE;

      return true;

    }
    else
    {
      *reason = DFR_NONE;

      return false;

    }

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Has directory permission failed : %s", e.errorMessage());

    *reason = DFR_DATABASE_ERROR;

    return false;

  }

}

bool
DataBase::IsAdmin(unsigned long long userPID, DATABASE_FAIL_REASON *reason)
{
  try
  {
    CppSQLite3Buffer query("select exists(select 1 from %Q where account_PID=? limit 1)", ADMINS_TABLENAME);
    StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
    StatementReseter reset(&this->m_statCache, state);

    state->bind(1, (long long)userPID);

    if (state->execScalar() > 0)
    {
      *reason = DFR_NONE;

      return true;

    }
    else
    {
      *reason = DFR_NOT_FOUND;

      return false;

    }

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"IsAdmin failed : %s", e.errorMessage());

    *reason = DFR_DATABASE_ERROR;

  }

  return false;

}

bool
DataBase::IsAdmin(const wstring &userID, DATABASE_FAIL_REASON *reason)
{
  try
  {
    CppSQLite3Buffer query("select exists(select 1 from %Q as a, %Q as ad where a.ID=? and a.PID=ad.account_PID limit 1)",
      ACCOUNT_TABLENAME, ADMINS_TABLENAME);
    StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
    StatementReseter reset(&this->m_statCache, state);

    state->bind(1, userID.c_str());

    if (state->execScalar() > 0)
    {
      *reason = DFR_NONE;

      return true;

    }
    else
    {
      *reason = DFR_NONE;

      return false;

    }

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"IsAdmin failed : %s", e.errorMessage());

    *reason = DFR_DATABASE_ERROR;

  }

  return false;

}

unsigned long long
DataBase::AddGroup(const wstring &name, bool useTransaction, bool *success, DATABASE_FAIL_REASON *reason)
{
  unsigned long long pid = 0;

  try
  {
    if (useTransaction)
      this->m_db.begin();

    if (!this->ExistGroup(name, reason))
    {
      if (*reason == DFR_NOT_FOUND)
      {
        pid = this->GetNextPIDValue(success, reason);

        if (pid > 0ll && *success)
        {
          CppSQLite3Buffer query("insert into %Q (PID, Name, PermissionOrder) values (?, ?, 0)", GROUPS_TABLENAME);
          StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
          StatementReseter reset(&this->m_statCache, state);

          state->bind(1, (long long)pid);
          state->bind(2, name.c_str());

          state->execDML();

          *reason = DFR_NONE;
          *success = true;

          if (useTransaction)
            this->m_db.commit();

        }
        else
        {
          *success = false;

          if (useTransaction)
            this->m_db.rollback();

        }

      }
      else
      {
        *success = false;

        if (useTransaction)
          this->m_db.rollback();

      }

    }
    else
    {
      *reason = DFR_ALREADY_EXIST;
      *success = false;

      if (useTransaction)
        this->m_db.rollback();

    }

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Add group failed : %s", e.errorMessage());

    *reason = DFR_DATABASE_ERROR;
    *success = false;

    try
    {
      if (useTransaction)
        this->m_db.rollback();

    }
    catch (CppSQLite3Exception &ee)
    {
      Log::GetInstance().Write(L"Rollback failed : %s", ee.errorMessage());

    }

  }

  return pid;

}

bool
DataBase::ExistGroupSameUser(const wstring &name, DATABASE_FAIL_REASON *reason)
{
  try
  {
    CppSQLite3Buffer query("select ug.PID from %Q as ug, %Q as g, %Q as a "
    "where g.PID=ug.groups_PID and a.PID=ug.account_PID and g.Name=?;",
      USER_GROUP_TABLENAME, GROUPS_TABLENAME, ACCOUNT_TABLENAME);
    StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
    StatementReseter reset(&this->m_statCache, state);

    state->bind(1, name.c_str());

    CppSQLite3Query reader = state->execQuery();

    if (!reader.eof())
    {
      *reason = DFR_NONE;

      return true;

    }
    else
    {
      *reason = DFR_NOT_FOUND;

      return false;

    }

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Exist group same user failed : %s", e.errorMessage());

    *reason = DFR_DATABASE_ERROR;

    return false;

  }

}

bool
DataBase::DeleteGroupWithAll(unsigned long long pid, DATABASE_FAIL_REASON *reason)
{
  try
  {
    this->m_db.begin();

    ANYVOD_GROUP_INFO info;

    if (this->GetGroupInfo(pid, &info, reason))
    {
      if (!this->ExistGroupSameUser(info.name, reason))
      {
        this->DeletePermissionFromUserGroup(info.name, reason);

        if (*reason != DFR_DATABASE_ERROR)
        {
          this->DeleteUserGroup(info.name, reason);

          if (*reason != DFR_DATABASE_ERROR)
          {
            if (this->DeleteGroup(info.name, reason))
            {
              this->m_db.commit();

              *reason = DFR_NONE;

              return true;

            }
            else
            {
              this->m_db.rollback();

              return false;

            }

          }
          else
          {
            this->m_db.rollback();

            return false;

          }

        }
        else
        {
          this->m_db.rollback();

          return false;

        }

      }
      else
      {
        this->m_db.rollback();

        *reason = DFR_ASSOCIATED_USER_GROUP;

        return false;

      }

    }
    else
    {
      this->m_db.rollback();

      return false;

    }

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Delete user info with all failed : %s", e.errorMessage());

    try
    {
      this->m_db.rollback();

    }
    catch (CppSQLite3Exception &ee)
    {
      Log::GetInstance().Write(L"Rollback failed : %s", ee.errorMessage());

    }

    *reason = DFR_DATABASE_ERROR;

    return false;

  }

}

bool
DataBase::DeleteGroup(const wstring &name, DATABASE_FAIL_REASON *reason)
{
  try
  {
    CppSQLite3Buffer query("delete from %Q where Name=?", GROUPS_TABLENAME);
    StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
    StatementReseter reset(&this->m_statCache, state);

    state->bind(1, name.c_str());

    if (state->execDML() > 0)
    {
      *reason = DFR_NONE;

      return true;

    }
    else
    {
      *reason = DFR_NOT_FOUND;

      return false;

    }

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Delete group failed : %s", e.errorMessage());

    *reason = DFR_DATABASE_ERROR;

    return false;

  }

}

bool
DataBase::ExistGroup(const wstring &name, DATABASE_FAIL_REASON *reason)
{
  try
  {
    CppSQLite3Buffer query("select PID from %Q where Name=?", GROUPS_TABLENAME);
    StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
    StatementReseter reset(&this->m_statCache, state);

    state->bind(1, name.c_str());

    CppSQLite3Query reader = state->execQuery();

    if (!reader.eof())
    {
      *reason = DFR_NONE;

      return true;

    }
    else
    {
      *reason = DFR_NOT_FOUND;

      return false;

    }

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Exist group failed : %s", e.errorMessage());

    *reason = DFR_DATABASE_ERROR;

    return false;

  }

}

unsigned long long
DataBase::AddUserToGroup(unsigned long long userPID, const wstring &groupName,
                         bool useTransaction, bool *success, DATABASE_FAIL_REASON *reason)
{
  unsigned long long pid = 0;

  try
  {
    if (useTransaction)
      this->m_db.begin();

    if (!this->ExistUserGroup(userPID, groupName, reason))
    {
      if (*reason == DFR_NOT_FOUND)
      {
        pid = this->GetNextPIDValue(success, reason);

        if (pid > 0ll && *success)
        {
          CppSQLite3Buffer query("insert into %Q (PID, account_PID, groups_PID) "
            "values (?, ?, (select PID from %Q where Name=?))",
            USER_GROUP_TABLENAME, GROUPS_TABLENAME);
          StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
          StatementReseter reset(&this->m_statCache, state);

          state->bind(1, (long long)pid);
          state->bind(2, (long long)userPID);
          state->bind(3, groupName.c_str());

          state->execDML();

          *reason = DFR_NONE;
          *success = true;

          if (useTransaction)
            this->m_db.commit();

        }
        else
        {
          *success = false;

          if (useTransaction)
            this->m_db.rollback();

        }


      }
      else
      {
        *success = false;

        if (useTransaction)
          this->m_db.rollback();

      }

    }
    else
    {
      *reason = DFR_ALREADY_EXIST;
      *success = false;

      if (useTransaction)
        this->m_db.rollback();

    }

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Add user to group failed : %s", e.errorMessage());

    *success = false;
    *reason = DFR_DATABASE_ERROR;

    try
    {
      if (useTransaction)
        this->m_db.rollback();

    }
    catch (CppSQLite3Exception &ee)
    {
      Log::GetInstance().Write(L"Rollback failed : %s", ee.errorMessage());

    }

  }

  return pid;

}

bool
DataBase::DeleteUserGroup(unsigned long long pid, DATABASE_FAIL_REASON *reason)
{
  try
  {
    CppSQLite3Buffer query("delete from %Q where PID=?", USER_GROUP_TABLENAME);
    StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
    StatementReseter reset(&this->m_statCache, state);

    state->bind(1, (long long)pid);

    if (state->execDML() > 0)
    {
      *reason = DFR_NONE;

      return true;

    }
    else
    {
      *reason = DFR_NOT_FOUND;

      return false;

    }

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Delete user group failed : %s", e.errorMessage());

    *reason = DFR_DATABASE_ERROR;

    return false;

  }

}

bool
DataBase::DeleteUserGroup(const wstring &groupName, DATABASE_FAIL_REASON *reason)
{
  try
  {
    CppSQLite3Buffer query("delete from %Q where groups_PID=(select PID from %Q where Name=?)",
      USER_GROUP_TABLENAME, GROUPS_TABLENAME);
    StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
    StatementReseter reset(&this->m_statCache, state);

    state->bind(1, groupName.c_str());

    if (state->execDML() > 0)
    {
      *reason = DFR_NONE;

      return true;

    }
    else
    {
      *reason = DFR_NOT_FOUND;

      return false;

    }

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Delete user group failed : %s", e.errorMessage());

    *reason = DFR_DATABASE_ERROR;

    return false;

  }

}

bool
DataBase::DeleteUserFromGroup(unsigned long long userPID, const wstring &groupName, DATABASE_FAIL_REASON *reason)
{
  try
  {
    CppSQLite3Buffer query("delete from %Q where account_PID=? "
      "and groups_PID=(select PID from %Q where Name=?)",
      USER_GROUP_TABLENAME, GROUPS_TABLENAME);
    StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
    StatementReseter reset(&this->m_statCache, state);

    state->bind(1, (long long)userPID);
    state->bind(2, groupName.c_str());

    if (state->execDML() > 0)
    {
      *reason = DFR_NONE;

      return true;

    }
    else
    {
      *reason = DFR_NOT_FOUND;

      return false;

    }

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Delete user from group failed : %s", e.errorMessage());

    *reason = DFR_DATABASE_ERROR;

    return false;

  }

}

bool
DataBase::GetUserGroupInfo(unsigned long long pid, ANYVOD_USER_GROUP_INFO *ret, DATABASE_FAIL_REASON *reason)
{
  try
  {
    CppSQLite3Buffer query("select ug.PID as PID, a.ID as UserID, g.Name as GroupName "
      "from %Q as ug, %Q as a, %Q as g where ug.account_PID=a.PID and ug.groups_PID=g.PID and ug.PID=?",
      USER_GROUP_TABLENAME, ACCOUNT_TABLENAME, GROUPS_TABLENAME);
    StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
    StatementReseter reset(&this->m_statCache, state);

    state->bind(1, (long long)pid);

    CppSQLite3Query reader = state->execQuery();

    if (!reader.eof())
    {
      this->GetUserGroupInfoFromReader(reader, ret);

    }

    *reason = DFR_NONE;

    return true;

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Get user group info failed : %s", e.errorMessage());

    *reason = DFR_DATABASE_ERROR;

    return false;

  }

}

bool
DataBase::GetUserGroupInfos(VECTOR_ANYVOD_USER_GROUP_INFO *ret, DATABASE_FAIL_REASON *reason)
{
  try
  {
    CppSQLite3Buffer query("select ug.PID as PID, a.ID as UserID, g.Name as GroupName "
      "from %Q as ug, %Q as a, %Q as g where ug.account_PID=a.PID and ug.groups_PID=g.PID",
      USER_GROUP_TABLENAME, ACCOUNT_TABLENAME, GROUPS_TABLENAME);
    StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
    StatementReseter reset(&this->m_statCache, state);
    CppSQLite3Query reader = state->execQuery();

    while (!reader.eof())
    {
      ANYVOD_USER_GROUP_INFO info;

      this->GetUserGroupInfoFromReader(reader, &info);

      ret->push_back(info);

      reader.nextRow();

    }

    *reason = DFR_NONE;

    return true;

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Get user group infos failed : %s", e.errorMessage());

    *reason = DFR_DATABASE_ERROR;

    return false;

  }

}

bool
DataBase::ExistUserGroup(unsigned long long userPID, const wstring &groupName, DATABASE_FAIL_REASON *reason)
{
  try
  {
    CppSQLite3Buffer query("select PID from %Q where account_PID=? "
      "and groups_PID=(select PID from %Q where Name=?)", USER_GROUP_TABLENAME, GROUPS_TABLENAME);
    StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
    StatementReseter reset(&this->m_statCache, state);

    state->bind(1, (long long)userPID);
    state->bind(2, groupName.c_str());

    CppSQLite3Query reader = state->execQuery();

    if (!reader.eof())
    {
      *reason = DFR_NONE;

      return true;

    }
    else
    {
      *reason = DFR_NOT_FOUND;

      return false;

    }

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Exist user group failed : %s", e.errorMessage());

    *reason = DFR_DATABASE_ERROR;

    return false;

  }

}

unsigned long long
DataBase::AddPermissionToUserGroup(const wstring &filePath, const wstring &groupName,
                                   bool useTransaction, bool *success, DATABASE_FAIL_REASON *reason)
{
  unsigned long long pid = 0;

  try
  {
    if (useTransaction)
      this->m_db.begin();

    vector_wstring paths;
    bool multi = this->DeterminePath(filePath, &paths);

    CppSQLite3Buffer query("insert into %Q (PID, movie_info_PID, groups_PID) "
      "values (?, (select PID from %Q where Path=?), (select PID from %Q where Name=?))",
      MOVIE_PERMISSION_TABLENAME, MOVIE_INFO_TABLENAME, GROUPS_TABLENAME);
    StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
    StatementReseter reset(&this->m_statCache, state);

    if (paths.size() > 0)
    {
      for (vector_wstring::iterator i = paths.begin(); i != paths.end(); i++)
      {
        wstring &path = *i;

        if (!this->ExistFileUserGroup(path, groupName, reason))
        {
          if (*reason == DFR_NOT_FOUND)
          {
            pid = this->GetNextPIDValue(success, reason);

            if (pid > 0ll && *success)
            {
              state->reset();

              state->bind(1, (long long)pid);
              state->bind(2, path.c_str());
              state->bind(3, groupName.c_str());

              state->execDML();

              *reason = DFR_NONE;
              *success = true;

            }
            else
            {
              *success = false;

            }

          }
          else
          {
            *success = false;

          }

        }
        else
        {
          if (multi)
          {
            *reason = DFR_NONE;
            *success = true;

          }
          else
          {
            *reason = DFR_ALREADY_EXIST;
            *success = false;

            break;

          }

        }

      }

    }
    else
    {
      *reason = DFR_NOT_FOUND;
      *success = false;

    }

    if (*success)
    {
      if (useTransaction)
        this->m_db.commit();

    }
    else
    {
      if (useTransaction)
        this->m_db.rollback();

    }

    if (multi)
    {
      pid = 0;

    }

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Add user permission to group failed : %s", e.errorMessage());

    *reason = DFR_DATABASE_ERROR;
    *success = false;

    try
    {
      if (useTransaction)
        this->m_db.rollback();

    }
    catch (CppSQLite3Exception &ee)
    {
      Log::GetInstance().Write(L"Rollback failed : %s", ee.errorMessage());

    }

  }

  return pid;

}

bool
DataBase::DeterminePath(const wstring &filePath, vector_wstring *ret)
{
  bool multi = false;
  wstring fullPath;
  DWORD attr = 0;

  Util::BindPath(this->m_server->GetEnv().movie_dir, filePath, &fullPath);
  attr = GetFileAttributesW(fullPath.c_str());

  if (IS_BIT_SET(attr, FILE_ATTRIBUTE_DIRECTORY))
  {
    //경로임
    wstring tmp = filePath;

    Util::AppendDirSeparator(&tmp);

    CppSQLite3Buffer query("select Path from %Q where Path like ?", MOVIE_INFO_TABLENAME);
    StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
    StatementReseter reset(&this->m_statCache, state);
    wstring arg = tmp + L"%";

    state->bind(1, arg.c_str());

    CppSQLite3Query reader = state->execQuery();

    while (!reader.eof())
    {
      ret->push_back(reader.getStringField("Path"));

      reader.nextRow();

    }

    return true;

  }
  else
  {
    //파일임
    ret->push_back(filePath);

    return false;

  }

}

bool
DataBase::DeletePermissionFromUserGroup(const wstring &filePath, const wstring &groupName, DATABASE_FAIL_REASON *reason)
{
  bool success = false;

  try
  {
    this->m_db.begin();

    vector_wstring paths;
    bool multi = this->DeterminePath(filePath, &paths);

    CppSQLite3Buffer query("delete from %Q where "
      "groups_PID=(select PID from %Q where Name=?) and movie_info_PID=(select PID from %Q where Path=?)",
      MOVIE_PERMISSION_TABLENAME, GROUPS_TABLENAME, MOVIE_INFO_TABLENAME);
    StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
    StatementReseter reset(&this->m_statCache, state);

    if (paths.size() > 0)
    {
      for (vector_wstring::iterator i = paths.begin(); i != paths.end(); i++)
      {
        wstring &path = *i;

        state->reset();

        state->bind(1, groupName.c_str());
        state->bind(2, path.c_str());

        bool deleted = state->execDML() > 0;

        if (multi)
        {
          *reason = DFR_NONE;

          success = true;

        }
        else
        {
          if (deleted)
          {
            *reason = DFR_NONE;

            success = true;

          }
          else
          {
            *reason = DFR_NOT_FOUND;

            success = false;

            break;

          }

        }

      }

    }
    else
    {
      *reason = DFR_NOT_FOUND;

      success = false;

    }

    if (success)
    {
      this->m_db.commit();

    }
    else
    {
      this->m_db.rollback();

    }

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Delete permission from user group failed : %s", e.errorMessage());

    *reason = DFR_DATABASE_ERROR;

    success = false;

    try
    {
      this->m_db.rollback();

    }
    catch (CppSQLite3Exception &ee)
    {
      Log::GetInstance().Write(L"Rollback failed : %s", ee.errorMessage());

    }

  }

  return success;

}

bool
DataBase::DeletePermissionFromUserGroup(unsigned long long pid, DATABASE_FAIL_REASON *reason)
{
  try
  {
    CppSQLite3Buffer query("delete from %Q where PID=?", MOVIE_PERMISSION_TABLENAME);
    StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
    StatementReseter reset(&this->m_statCache, state);

    state->bind(1, (long long)pid);

    if (state->execDML() > 0)
    {
      *reason = DFR_NONE;

      return true;

    }
    else
    {
      *reason = DFR_NOT_FOUND;

      return false;

    }

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Delete permission from user group failed : %s", e.errorMessage());

    *reason = DFR_DATABASE_ERROR;

    return false;

  }

}

bool
DataBase::DeletePermissionFromUserGroup(const wstring &groupName, DATABASE_FAIL_REASON *reason)
{
  try
  {
    CppSQLite3Buffer query("delete from %Q where groups_PID=(select PID from %Q where Name=?)",
      MOVIE_PERMISSION_TABLENAME, GROUPS_TABLENAME);
    StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
    StatementReseter reset(&this->m_statCache, state);

    state->bind(1, groupName.c_str());

    if (state->execDML() > 0)
    {
      *reason = DFR_NONE;

      return true;

    }
    else
    {
      *reason = DFR_NOT_FOUND;

      return false;

    }

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Delete permission from user group failed : %s", e.errorMessage());

    *reason = DFR_DATABASE_ERROR;

    return false;

  }

}

bool
DataBase::ExistFileUserGroup(const wstring &filePath, const wstring &groupName, DATABASE_FAIL_REASON *reason)
{
  try
  {
    CppSQLite3Buffer query("select PID from %Q where movie_info_PID=(select PID from %Q where Path=?) "
      "and groups_PID=(select PID from %Q where Name=?)",
      MOVIE_PERMISSION_TABLENAME, MOVIE_INFO_TABLENAME, GROUPS_TABLENAME);
    StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
    StatementReseter reset(&this->m_statCache, state);

    state->bind(1, filePath.c_str());
    state->bind(2, groupName.c_str());

    CppSQLite3Query reader = state->execQuery();

    if (!reader.eof())
    {
      *reason = DFR_NONE;

      return true;

    }
    else
    {
      *reason = DFR_NOT_FOUND;

      return false;

    }

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Exist user group failed : %s", e.errorMessage());

    *reason = DFR_DATABASE_ERROR;

    return false;

  }

}

bool
DataBase::ModifyGroup(const ANYVOD_GROUP_INFO &info, DATABASE_FAIL_REASON *reason)
{
  try
  {
    this->m_db.begin();

    if (!this->ExistGroup(info.name, reason))
    {
      CppSQLite3Buffer query("update %Q set Name=? where PID=?", GROUPS_TABLENAME);
      StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
      StatementReseter reset(&this->m_statCache, state);

      state->bind(1, info.name.c_str());
      state->bind(2, (long long)info.pid);

      if (state->execDML() > 0)
      {
        this->m_db.commit();

        *reason = DFR_NONE;

        return true;

      }
      else
      {
        *reason = DFR_NOT_FOUND;

        this->m_db.rollback();

        return false;

      }

    }
    else
    {
      *reason = DFR_ALREADY_EXIST;

      this->m_db.rollback();

      return false;

    }

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Modify group info failed : %s", e.errorMessage());

    try
    {
      this->m_db.rollback();

    }
    catch (CppSQLite3Exception &ee)
    {
      Log::GetInstance().Write(L"Rollback failed : %s", ee.errorMessage());

    }

    *reason = DFR_DATABASE_ERROR;

    return false;

  }

  return false;

}

bool
DataBase::GetGroupInfo(unsigned long long pid, ANYVOD_GROUP_INFO *ret, DATABASE_FAIL_REASON *reason)
{
  try
  {
    CppSQLite3Buffer query("select PID, Name from %Q where PID=?", GROUPS_TABLENAME);
    StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
    StatementReseter reset(&this->m_statCache, state);

    state->bind(1, (long long)pid);

    CppSQLite3Query reader = state->execQuery();

    if (!reader.eof())
    {
      this->GetGroupInfoFromReader(reader, ret);

      *reason = DFR_NONE;

      return true;

    }
    else
    {
      *reason = DFR_NOT_FOUND;

      return false;

    }

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Get group info failed : %s", e.errorMessage());

    *reason = DFR_DATABASE_ERROR;

    return false;

  }

}

bool
DataBase::GetGroupInfos(VECTOR_ANYVOD_GROUP_INFO *ret, DATABASE_FAIL_REASON *reason)
{
  try
  {
    CppSQLite3Buffer query("select PID, Name from %Q", GROUPS_TABLENAME);
    StatementCache::STATEMENT_ITEM *state = this->m_statCache.GetStatement((const char*)query);
    StatementReseter reset(&this->m_statCache, state);
    CppSQLite3Query reader = state->execQuery();

    while (!reader.eof())
    {
      ANYVOD_GROUP_INFO info;

      this->GetGroupInfoFromReader(reader, &info);

      ret->push_back(info);

      reader.nextRow();

    }

    *reason = DFR_NONE;

    return true;

  }
  catch (CppSQLite3Exception &e)
  {
    Log::GetInstance().Write(L"Get group infos failed : %s", e.errorMessage());

    *reason = DFR_DATABASE_ERROR;

    return false;

  }

}

void
DataBase::GetPassword(const wstring &pass, wstring *ret) const
{
  string tmp;
  string password;

  Util::ConvertUnicodeToAscii(pass, &tmp);
  Util::GetSHA256((uint8_t*)tmp.c_str(), tmp.size(), &password);
  Util::ConvertAsciiToUnicode(password, ret);

}
