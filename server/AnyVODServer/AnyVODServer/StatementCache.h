/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "..\..\..\common\types.h"
#include "..\..\..\common\CppSQLite3.h"

class StatementCache
{
public:
  struct STATEMENT_ITEM
  {
    friend class StatementCache;

    inline int execDML() { return statement.execDML(); }
    inline CppSQLite3Query execQuery() { return statement.execQuery(); }
    inline int execScalar() { return statement.execScalar(); }
    inline long long execScalar64() { return statement.execScalar64(); }

    inline void bind(int index, const wchar_t* value) { statement.bind(index, value); }
    inline void bind(int index, const int value) { statement.bind(index, value); }
    inline void bind(int index, const long long value) { statement.bind(index, value); }
    inline void bind(int index, const double value) { statement.bind(index, value); }
    inline void bind(int index, const unsigned char* value, int len) { statement.bind(index, value, len); }
    inline void bindNull(int index) { statement.bindNull(index); }

    inline void reset() { statement.reset(); }
    inline void finalize() { statement.finalize(); }

  private:
    CppSQLite3Statement statement;
    CRITICAL_SECTION locker;

  };

  StatementCache();
  ~StatementCache();

  void Init(CppSQLite3DB *db);
  void Deinit();

  STATEMENT_ITEM* GetStatement(const string &sql);
  void ReleaseStatement(STATEMENT_ITEM *item);

  void GetStatementStatus(VECTOR_ANYVOD_STATEMENT_STATUS *ret);

  void SetMaxItemCount(int count);
  int GetMaxItemCount() const;

private:
  typedef vector<STATEMENT_ITEM*> VECTOR_STATEMENT_ITEM;
  typedef VECTOR_STATEMENT_ITEM::iterator VECTOR_STATEMENT_ITEM_ITERATOR;

  typedef map<string, VECTOR_STATEMENT_ITEM*> MAP_STRING_VECTOR_STATEMENT_ITEM;
  typedef MAP_STRING_VECTOR_STATEMENT_ITEM::iterator MAP_STRING_VECTOR_STATEMENT_ITEM_ITERATOR;

private:
  CppSQLite3DB *m_db;
  CRITICAL_SECTION m_locker;
  MAP_STRING_VECTOR_STATEMENT_ITEM m_container;
  int m_maxCount;

};
