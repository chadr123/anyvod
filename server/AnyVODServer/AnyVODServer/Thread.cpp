/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "stdafx.h"
#include "Thread.h"

Thread::Thread()
:m_workContinue(true), m_thread(0)
{

}

Thread::~Thread()
{
  this->Stop();
  this->Clear();

}

bool
Thread::Start()
{
  if (!this->IsRun())
  {
    this->m_thread = (HANDLE)_beginthreadex(nullptr, 0, ThreadMain, this, 0, nullptr);

    if (this->m_thread == 0)
    {
      return false;

    }

    return true;

  }
  else
  {
    return false;

  }

}

void
Thread::Stop()
{
  this->m_workContinue = false;

  WaitForSingleObject(this->m_thread, INFINITE);

  this->m_workContinue = true;

}

bool
Thread::IsRun() const
{
  DWORD stat = WaitForSingleObject(this->m_thread, 1);

  return stat != WAIT_OBJECT_0 && stat != WAIT_FAILED;

}

void
Thread::Wait() const
{
  WaitForSingleObject(this->m_thread, INFINITE);

}

HANDLE
Thread::GetThreadHandle() const
{
  return this->m_thread;

}

void
Thread::Clear()
{
  CloseHandle(this->m_thread);
  this->m_thread = 0;

}

unsigned int __stdcall
Thread::ThreadMain(void *lpArgs)
{
  Thread *thread = (Thread*)lpArgs;

  while (thread->m_workContinue)
  {
    if (!thread->Run())
      break;

  }

  thread->Clear();

  return 0;

}
