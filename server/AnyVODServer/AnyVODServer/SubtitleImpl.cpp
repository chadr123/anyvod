/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "stdafx.h"
#include "SubtitleImpl.h"
#include "Util.h"

SubtitleImpl::SubtitleImpl()
{

}

FILE* SubtitleImpl::openFile(const wstring &filePath)
{
  return _wfopen(filePath.c_str(), L"rb");

}

bool SubtitleImpl::retreiveFromServer(const string &address, const string &url, const string &agent,
                                      const string &method, const string &header, const string &data,
                                      DWORD flag, wstring *ret)
{
  bool success = false;
  HINTERNET internet = NULL;
  HINTERNET http = NULL;
  HINTERNET request = NULL;

  internet = InternetOpenA(agent.c_str(), INTERNET_OPEN_TYPE_PRECONFIG, nullptr, nullptr, 0);

  if (internet != NULL)
  {
    http = InternetConnectA(internet, address.c_str(), INTERNET_DEFAULT_HTTP_PORT, "", "", INTERNET_SERVICE_HTTP, 0, 0);

    if (http != NULL)
    {
      request = HttpOpenRequestA(http, method.c_str(), url.c_str(), nullptr, nullptr, nullptr, flag, 0);

      if (request != NULL)
      {
        if (HttpSendRequestA(request, header.c_str(), (DWORD)header.size(), (void*)data.c_str(), (DWORD)data.size()))
        {
          DWORD read;

          while (true)
          {
            DWORD size;

            if (!InternetQueryDataAvailable(request, &size, 0, 0))
            {
              success = false;

              break;
            }

            if (size == 0)
              break;

            char *buf = new char[size];

            if (!InternetReadFile(request, buf, size, &read))
            {
              delete[] buf;
              success = false;

              break;

            }

            wstring fragment;

            Util::ConvertUTF8ToUnicode(buf, read, &fragment);

            ret->append(fragment.c_str());

            delete[] buf;

            success = true;

          }

        }

      }

    }

  }

  if (request != NULL)
    InternetCloseHandle(request);

  if (http != NULL)
    InternetCloseHandle(http);

  if (internet != NULL)
    InternetCloseHandle(internet);

  return success;

}

bool SubtitleImpl::retreiveFromAlSongServer(const string &data, wstring *ret)
{
  string header;

  header.append("Content-Type: application/soap+xml; charset=utf-8\r\n");
  header.append("SOAPAction: ALSongWebServer/GetLyric7\r\n");  

  return this->retreiveFromServer(ALSONG_SERVER, ALSONG_URL, "gSOAP/2.7", "POST", header, data, 0, ret);

}

bool SubtitleImpl::retreiveFromGOMServer(const string &md5, wstring *ret)
{
  string header;
  string data;

  header.append("Content-Type: ");
  header.append("multipart/form-data; boundary=--MULTI-PARTS-FORM-DATA-BOUNDARY");
  header.append("\r\n");

  data.append("----MULTI-PARTS-FORM-DATA-BOUNDARY\r\n");
  data.append("Content-Disposition: form-data; name=\"moviekey\"\r\n");
  data.append("\r\n");
  data.append(md5 + "\r\n");

  data.append("----MULTI-PARTS-FORM-DATA-BOUNDARY--");

  return this->retreiveFromServer(GOM_SERVER, GOM_URL, "GenericHTTPClient", "POST", header, data, 0, ret);

}

void SubtitleImpl::getMD5(uint8_t *buf, size_t size, string *ret)
{
  Util::GetMD5(buf, size, ret);

}
