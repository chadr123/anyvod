/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "DataBase.h"
#include "..\..\..\common\types.h"

class Util
{
private:
  Util(){}
  ~Util(){}

public:
  static bool IsMatchExt(const wstring &fileName, const vector_wstring &exts);
  static void SplitFilePath(const wstring &fullPath, wstring *retPath, wstring *retFileName);
  static bool IsRoot(const wstring &path);
  static bool GetMediaInfo(const wstring &filePath, DataBase::MOVIE_INFO *info);
  static void TrimString(const wstring &src, wstring *ret);
  static void GetLocalTimeToLargeInteger(const SYSTEMTIME *time, ULARGE_INTEGER *integer);
  static void AppendDirSeparator(wstring *path);
  static void BindPath(const wstring &first, const wstring &second, wstring *ret);
  static void ConvertAsciiToUnicode(const string &ascii, wstring *ret);
  static void ConvertUnicodeToAscii(const wstring &unicode, string *ret);
  static void ConvertUnicodeToUTF8(const wstring &unicode, char *ret, int size);
  static void ConvertUTF8ToUnicode(const char *utf8, int size, wstring *ret);
  static wstring SystemErrorToString(DWORD error);
  static wstring GetModuleVersion(HMODULE module, bool getStringVersion);
  static bool DeterminMovieMetaDataRange(long long startOffset, int size, VECTOR_ANYVOD_ANALYSED_DATA_BLOCK &metaData);
  static void SplitString(const wstring &src, const wchar_t delem, vector_wstring *ret);
  static bool CreateDirectories(const wstring &path);
  static const wstring AddressToString(const SOCKADDR_STORAGE &addr);
  static unsigned short PortFromAddress(const SOCKADDR_STORAGE &addr);
  static void GetMD5(uint8_t *buf, size_t size, string *ret);
  static void GetSHA256(uint8_t *buf, size_t size, string *ret);

private:
  inline static bool DeterminMovieMetaDataRangeSub(ANYVOD_ANALYSED_DATA_BLOCK &prevBlock, ANYVOD_ANALYSED_DATA_BLOCK &block, long long startOffset, int size, long long *accumOffset);
  static void GetHashValue(ALG_ID alg, uint8_t *buf, size_t size, string *ret);

};
