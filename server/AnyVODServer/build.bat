call toolchain.bat

%MSBuildPath% /t:Rebuild /p:Configuration=Release /p:Platform=Win32 /m /fl AnyVODServer.sln
if %errorlevel% neq 0 goto end

%MSBuildPath% /t:Rebuild /p:Configuration=Release /p:Platform=x64 /m /fl AnyVODServer.sln
if %errorlevel% neq 0 goto end

:end
