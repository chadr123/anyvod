/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

// AnyVODAnalyserMKV.cpp : DLL 응용 프로그램을 위해 내보낸 함수를 정의합니다.
//

#include "stdafx.h"
#include "AnyVODAnalyserMKV.h"
#include "..\..\..\..\common\types.h"
#include "..\..\..\..\common\network.h"

const uint8_t AnyVODAnalyserMKV::EBML_ID[4] = {0x1A, 0x45, 0xDF, 0xA3};
const uint8_t AnyVODAnalyserMKV::SEGMENT_ID[4] = {0x18, 0x53, 0x80, 0x67};
const uint8_t AnyVODAnalyserMKV::SEEK_HEAD_ID[4] = {0x11, 0x4D, 0x9B, 0x74};
const uint8_t AnyVODAnalyserMKV::SEGMENT_INFO_ID[4] = {0x15, 0x49, 0xA9, 0x66};
const uint8_t AnyVODAnalyserMKV::TRACKS_ID[4] = {0x16, 0x54, 0xAE, 0x6B};
const uint8_t AnyVODAnalyserMKV::CUES_ID[4] = {0x1C, 0x53, 0xBB, 0x6B};
const uint8_t AnyVODAnalyserMKV::ATTACHMENTS_ID[4] = {0x19, 0x41, 0xA4, 0x69};
const uint8_t AnyVODAnalyserMKV::CHAPTERS_ID[4] = {0x10, 0x43, 0xA7, 0x70};
const uint8_t AnyVODAnalyserMKV::TAGS_ID[4] = {0x12, 0x54, 0xC3, 0x67};
const uint8_t AnyVODAnalyserMKV::VOID_ID[1] = {0xEC};
const uint8_t AnyVODAnalyserMKV::CRC32_ID[1] = {0xBF};

AnyVODAnalyserMKV::AnyVODAnalyserMKV(HMODULE handle)
:Analyser(handle)
{

}

AnyVODAnalyserMKV::~AnyVODAnalyserMKV()
{

}

void
AnyVODAnalyserMKV::GetName(wchar_t ret[MAX_ANALYSER_NAME_SIZE])
{
  wcsncpy_s(ret, MAX_ANALYSER_NAME_SIZE, L"matroska", MAX_ANALYSER_NAME_CHAR_SIZE);

}

void
AnyVODAnalyserMKV::GetDescription(wchar_t ret[MAX_ANALYSER_DESC_SIZE])
{
  wcsncpy_s(ret, MAX_ANALYSER_DESC_SIZE, L"MKV Analyser", MAX_ANALYSER_DESC_CHAR_SIZE);

}

bool
AnyVODAnalyserMKV::ConvertInteger(const uint8_t integer[8], int len, uint64_t *ret)
{
  switch (len)
  {
  case 8:
  case 7:
  case 6:
  case 5:
    {
      *ret = ntohll(*((uint64_t*)integer));

      break;

    }
  case 4:
  case 3:
    {
      *ret = ntohl(*((uint32_t*)integer));

      break;

    }
  case 2:
    {
      *ret = ntohs(*((uint16_t*)integer));

      break;

    }
  case 1:
    {
      *ret = integer[0];

      break;

    }
  default:
    {
      return false;

    }

  }

  if (len == 3)
    *ret >>= 8;

  if (len >= 5 && len < 8)
    *ret >>= ((8 - len) * 8);

  return true;

}

int
AnyVODAnalyserMKV::GetIntegerLength(uint8_t header)
{
  uint8_t masks[8] = {0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01};

  for (int i = 0; i < 8; i++)
  {
    if (header & masks[i])
      return i + 1;

  }

  return -1;

}

bool
AnyVODAnalyserMKV::GetInteger(HANDLE file, bool id, uint64_t *ret)
{
  DWORD read;
  uint8_t buf[8] = {0, };

  if (!ReadFile(file, &buf[0], 1, &read, nullptr))
    return false;

  if (read != 1)
    return false;

  int len = this->GetIntegerLength(buf[0]) - 1;

  if (len > 0)
  {
    if (!ReadFile(file, &buf[1], len, &read, nullptr))
      return false;

    if (read != len)
      return false;

  }

  if (!id)
  {
    uint8_t masks[8] = {0x7f, 0x3f, 0x1f, 0x0f, 0x07, 0x03, 0x01, 0x00};

    buf[0] &= masks[len];

  }

  return this->ConvertInteger(buf, len + 1, ret);

}

bool
AnyVODAnalyserMKV::ReadElement(HANDLE file, ELEMENT *ret)
{
  if (!this->GetInteger(file, true, &ret->id))
    return false;

  if (!this->GetInteger(file, false, &ret->size))
    return false;

  return true;

}

bool
AnyVODAnalyserMKV::FindSegment(HANDLE file, VECTOR_ANYVOD_ANALYSED_DATA_BLOCK *ret)
{
  ELEMENT element;
  LARGE_INTEGER offset;

  offset.QuadPart = 0;

  if (!SetFilePointerEx(file, offset, nullptr, FILE_BEGIN))
    return false;

  if (!this->ReadElement(file, &element))
    return false;

  offset.QuadPart = element.size;

  if (!SetFilePointerEx(file, offset, nullptr, FILE_CURRENT))
    return false;

  if (!this->ReadElement(file, &element))
    return false;

  uint64_t id;

  this->ConvertInteger(SEGMENT_ID, sizeof(SEGMENT_ID), &id);

  if (id != element.id)
    return false;

  this->AddElements(file, ret);

  return true;

}

bool
AnyVODAnalyserMKV::IsValidElement(uint64_t id)
{
  uint64_t tmp;

  this->ConvertInteger(SEEK_HEAD_ID, sizeof(SEEK_HEAD_ID), &tmp);

  if (id == tmp)
    return true;

  this->ConvertInteger(SEGMENT_INFO_ID, sizeof(SEGMENT_INFO_ID), &tmp);

  if (id == tmp)
    return true;

  this->ConvertInteger(TRACKS_ID, sizeof(TRACKS_ID), &tmp);

  if (id == tmp)
    return true;

  this->ConvertInteger(CUES_ID, sizeof(CUES_ID), &tmp);

  if (id == tmp)
    return true;

  this->ConvertInteger(ATTACHMENTS_ID, sizeof(ATTACHMENTS_ID), &tmp);

  if (id == tmp)
    return true;

  this->ConvertInteger(CHAPTERS_ID, sizeof(CHAPTERS_ID), &tmp);

  if (id == tmp)
    return true;

  this->ConvertInteger(TAGS_ID, sizeof(TAGS_ID), &tmp);

  if (id == tmp)
    return true;

  this->ConvertInteger(VOID_ID, sizeof(VOID_ID), &tmp);

  if (id == tmp)
    return true;

  this->ConvertInteger(CRC32_ID, sizeof(CRC32_ID), &tmp);

  if (id == tmp)
    return true;

  return false;

}

void
AnyVODAnalyserMKV::AddElements(HANDLE file, VECTOR_ANYVOD_ANALYSED_DATA_BLOCK *ret)
{
  ELEMENT element;
  ANYVOD_ANALYSED_DATA_BLOCK block;

  while (true)
  {
    LARGE_INTEGER pos;
    LARGE_INTEGER current;

    current.QuadPart = 0;

    if (!SetFilePointerEx(file, current, &pos, FILE_CURRENT))
      return;

    if (!this->ReadElement(file, &element))
      return;

    if (this->IsValidElement(element.id))
    {
      block.blockOffset = pos.QuadPart;

      LARGE_INTEGER dataStart;

      if (!SetFilePointerEx(file, current, &dataStart, FILE_CURRENT))
        return;

      block.size = element.size + (dataStart.QuadPart - pos.QuadPart);

      ret->push_back(block);

    }

    current.QuadPart = element.size;

    if (!SetFilePointerEx(file, current, nullptr, FILE_CURRENT))
      return;

  }

}

bool
AnyVODAnalyserMKV::Analyse(const wstring &path, VECTOR_ANYVOD_ANALYSED_DATA_BLOCK *ret)
{
  HANDLE file = CreateFileW(path.c_str(), GENERIC_READ,
    FILE_SHARE_READ, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
  FILE_CLOSER closer(file);

  if (file != INVALID_HANDLE_VALUE)
  {
    DWORD read;
    uint8_t buf[sizeof(EBML_ID)];

    if (!ReadFile(file, buf, sizeof(buf), &read, nullptr))
      return false;

    if (read != sizeof(buf))
      return false;

    if (memcmp(buf, EBML_ID, sizeof(buf)) != 0)
      return false;

    return this->FindSegment(file, ret);

  }

  return false;

}


