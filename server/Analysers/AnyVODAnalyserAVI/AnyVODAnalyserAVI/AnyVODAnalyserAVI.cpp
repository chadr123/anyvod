/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

// AnyVODAnalyserAVI.cpp : DLL 응용 프로그램을 위해 내보낸 함수를 정의합니다.
//

#include "stdafx.h"
#include "AnyVODAnalyserAVI.h"
#include "..\..\..\..\common\types.h"

AnyVODAnalyserAVI::AnyVODAnalyserAVI(HMODULE handle)
:Analyser(handle)
{

}

AnyVODAnalyserAVI::~AnyVODAnalyserAVI()
{

}

void
AnyVODAnalyserAVI::GetName(wchar_t ret[MAX_ANALYSER_NAME_SIZE])
{
  wcsncpy_s(ret, MAX_ANALYSER_NAME_SIZE, L"avi", MAX_ANALYSER_NAME_CHAR_SIZE);

}

void
AnyVODAnalyserAVI::GetDescription(wchar_t ret[MAX_ANALYSER_DESC_SIZE])
{
  wcsncpy_s(ret, MAX_ANALYSER_DESC_SIZE, L"AVI Analyser", MAX_ANALYSER_DESC_CHAR_SIZE);

}

bool
AnyVODAnalyserAVI::Analyse(const wstring &path, VECTOR_ANYVOD_ANALYSED_DATA_BLOCK *ret)
{
  HANDLE file = CreateFileW(path.c_str(), GENERIC_READ,
    FILE_SHARE_READ, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
  FILE_CLOSER closer(file);
  ANYVOD_ANALYSED_DATA_BLOCK block;

  if (file != INVALID_HANDLE_VALUE)
  {
    char byte4[5] = {0, };
    DWORD read = 0;
    LARGE_INTEGER totalReaded;

    totalReaded.QuadPart = 0ll;

    //RIFF 읽기
    if (!ReadFile(file, byte4, 4, &read, nullptr))
      return false;

    totalReaded.QuadPart += read;

    if (strcmp(byte4, "RIFF") != 0)
      return false;

    //헤더를 제외한 파일 크기 읽기(그냥 버린다.)
    if (!ReadFile(file, byte4, 4, &read, nullptr))
      return false;

    totalReaded.QuadPart += read;

    //AVI 읽기
    if (!ReadFile(file, byte4, 4, &read, nullptr))
      return false;

    totalReaded.QuadPart += read;

    if (strcmp(byte4, "AVI ") != 0)
      return false;

    //헤더
    block.blockOffset = 0ll;
    block.size = totalReaded.QuadPart;
    ret->push_back(block);

    //first
    block.blockOffset = totalReaded.QuadPart;
    block.size = 0ll;

    int size = 0;

    while (true)
    {
      //LIST 읽기
      if (!ReadFile(file, byte4, 4, &read, nullptr))
        return false;

      totalReaded.QuadPart += read;

      if (strcmp(byte4, "LIST") == 0)
        break;

      if (!ReadFile(file, &size, 4, &read, nullptr))
        return false;

      totalReaded.QuadPart += read;
      totalReaded.QuadPart += size;

      if (!SetFilePointerEx(file, totalReaded, nullptr, FILE_BEGIN))
        return false;

    }

    //처음 크기 읽기
    if (!ReadFile(file, &size, 4, &read, nullptr))
      return false;

    totalReaded.QuadPart += size;
    block.size += read;

    //hdrl 읽기
    if (!ReadFile(file, byte4, 4, &read, nullptr))
      return false;

    totalReaded.QuadPart += read;

    if (strcmp(byte4, "hdrl") != 0)
      return false;

    block.size += size + read;
    ret->push_back(block);

    if (!SetFilePointerEx(file, totalReaded, nullptr, FILE_BEGIN))
      return false;

    //middle
    block.blockOffset = totalReaded.QuadPart;
    block.size = 0ll;

    while (true)
    {
      //JUNK 읽기
      if (!ReadFile(file, byte4, 4, &read, nullptr))
        return false;

      totalReaded.QuadPart += read;

      if (strcmp(byte4, "JUNK") == 0 ||
        strcmp(byte4, "JUNQ") == 0 ||
        strcmp(byte4, "LIST") == 0)
      {
        //JUNK or LIST 크기 읽기
        if (!ReadFile(file, &size, 4, &read, nullptr))
          return false;

        totalReaded.QuadPart += size;
        totalReaded.QuadPart += read;

        if (strcmp(byte4, "LIST") == 0)
        {
          //movi 읽기
          if (!ReadFile(file, byte4, 4, &read, nullptr))
            return false;

          totalReaded.QuadPart += read;

          if (strcmp(byte4, "movi") == 0)
          {
            totalReaded.QuadPart -= size;
            totalReaded.QuadPart -= read;
            totalReaded.QuadPart -= read;
            totalReaded.QuadPart -= read;

            if (!SetFilePointerEx(file, totalReaded, nullptr, FILE_BEGIN))
              return false;

            break;

          }
          else
          {
            totalReaded.QuadPart -= read;

            if (!SetFilePointerEx(file, totalReaded, nullptr, FILE_BEGIN))
              return false;

          }

        }

        if (!SetFilePointerEx(file, totalReaded, nullptr, FILE_BEGIN))
          return false;

      }
      else
      {
        totalReaded.QuadPart -= read;

        if (!SetFilePointerEx(file, totalReaded, nullptr, FILE_BEGIN))
          return false;

        break;

      }

    }

    block.size = totalReaded.QuadPart - block.blockOffset;
    ret->push_back(block);

    //LIST 읽기
    if (!ReadFile(file, byte4, 4, &read, nullptr))
      return false;

    totalReaded.QuadPart += read;

    if (strcmp(byte4, "LIST") != 0)
      return false;

    //처음 크기 읽기
    if (!ReadFile(file, &size, 4, &read, nullptr))
      return false;

    totalReaded.QuadPart += size;

    //movi 읽기
    if (!ReadFile(file, byte4, 4, &read, nullptr))
      return false;

    totalReaded.QuadPart += read;

    if (strcmp(byte4, "movi") != 0)
      return false;

    if (!SetFilePointerEx(file, totalReaded, nullptr, FILE_BEGIN))
      return false;

    //last
    block.blockOffset = totalReaded.QuadPart;
    block.size = 0ll;

    //idx1 읽기
    if (!ReadFile(file, byte4, 4, &read, nullptr))
      return false;

    if (read == 0)
    {
      //인덱스가 없음. 없는 경우도 있나...
      //재생에는 문제가 없는 듯 하니 인덱스가 없어도
      //정상으로 처리
      return true;

    }
    else
    {
      totalReaded.QuadPart += read;
      block.size += read;

      if (strcmp(byte4, "idx1") == 0)
      {
        //idx1 크기 읽기
        if (!ReadFile(file, &size, 4, &read, nullptr))
          return false;

        block.size += read;
        block.size += size;
        ret->push_back(block);

      }
      else
      {
        return false;

      }

    }

    return true;

  }

  return false;

}


