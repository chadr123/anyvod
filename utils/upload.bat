@echo off
setlocal enabledelayedexpansion 

rem %1 : user name
rem %2 : version

set upload_files[0]=AnyVOD-%2_linux_amd64.deb
set upload_files[1]=AnyVOD-%2_mac.pkg
set upload_files[2]=AnyVOD-%2_win.exe
set upload_files[3]=AnyVODMobile-%2_raspi_armhf.deb
set upload_files[4]=AnyVODMobile-%2_ios.ipa
set upload_files[5]=AnyVODMobile-%2_android.apk

set options =

for /l %%n in (0,1,5) do (
  call set "options=%%options%% -F files=@!upload_files[%%n]!"
)

curl --progress-bar -s -u%1 -X POST https://api.bitbucket.org/2.0/repositories/chadr123/anyvod/downloads %options%

echo uploading completed