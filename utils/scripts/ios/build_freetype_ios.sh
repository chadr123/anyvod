#!/bin/sh

#  Automatic build script for freetype
#  for iPhoneOS and iPhoneSimulator
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#
###########################################################################
#  Change values here                                                     #
#                                                                         #
MIN_VERSION="8.0"                                                         #
#                                                                         #
###########################################################################
#                                                                         #
# Don't change anything under this line!                                  #
#								          #
###########################################################################


SDKVERSION=`xcrun -sdk iphoneos --show-sdk-version`
CURRENTPATH=`pwd`
ARCHS="arm64 x86_64"
DEVELOPER=`xcode-select -print-path`

set -e
mkdir -p "${CURRENTPATH}/bin"
mkdir -p "${CURRENTPATH}/lib_tmp"

for ARCH in ${ARCHS}
do
    if [ "${ARCH}" == "x86_64" ];
    then
        PLATFORM="iPhoneSimulator"
    else
        PLATFORM="iPhoneOS"
    fi
	
    echo "Building freetype for ${PLATFORM} ${SDKVERSION} ${ARCH}"
    echo "Please stand by..."
	
    DEVROOT="${DEVELOPER}/Platforms/${PLATFORM}.platform/Developer"
    SDKROOT="${DEVROOT}/SDKs/${PLATFORM}${SDKVERSION}.sdk"

    CC=${DEVELOPER}/usr/bin/gcc

    LDFLAGS="-arch ${ARCH} -pipe -no-cpp-precomp -isysroot ${SDKROOT} -L${CURRENTPATH}/lib_tmp -miphoneos-version-min=${MIN_VERSION} -fheinous-gnu-extensions"
    CFLAGS="-arch ${ARCH} -pipe -no-cpp-precomp -isysroot ${SDKROOT} -I${CURRENTPATH}/include -miphoneos-version-min=${MIN_VERSION} -fheinous-gnu-extensions"

    HOST="${ARCH}"
    if [ "${ARCH}" == "arm64" ];
    then
        HOST="aarch64"
    fi

    mkdir -p "${CURRENTPATH}/bin/${PLATFORM}${SDKVERSION}-${ARCH}.sdk"
    LOG="${CURRENTPATH}/bin/${PLATFORM}${SDKVERSION}-${ARCH}.sdk/build-freetype.log"

    echo "Configure..."
    ./configure --with-png=no --with-harfbuzz=no --with-brotli=no --host="${HOST}-apple-darwin" --prefix="${CURRENTPATH}/bin/${PLATFORM}${SDKVERSION}-${ARCH}.sdk" --disable-shared --enable-static CC=$CC CFLAGS="$CFLAGS" LDFLAGS="$LDFLAGS" > "${LOG}" 2>&1
    echo "Make..."
    make -j4 >> "${LOG}" 2>&1
    echo "Make install..."
    make install >> "${LOG}" 2>&1
    make clean >> "${LOG}" 2>&1
    cd "${CURRENTPATH}"
done

echo "Build library..."
lipo -create ${CURRENTPATH}/bin/iPhoneSimulator${SDKVERSION}-x86_64.sdk/lib/libfreetype.a \
             ${CURRENTPATH}/bin/iPhoneOS${SDKVERSION}-arm64.sdk/lib/libfreetype.a \
             -output ${CURRENTPATH}/lib_tmp/libfreetype.a

lipo -info ${CURRENTPATH}/lib_tmp/libfreetype.a
mkdir -p ${CURRENTPATH}/../lib
cp ${CURRENTPATH}/lib_tmp/libfreetype.a ${CURRENTPATH}/../lib
mkdir -p ${CURRENTPATH}/../include/freetype2
cp -a ${CURRENTPATH}/bin/iPhoneSimulator${SDKVERSION}-x86_64.sdk/include/freetype2/* ${CURRENTPATH}/../include/freetype2
echo "Building done."
echo "Done."
