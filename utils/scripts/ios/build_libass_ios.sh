#!/bin/sh

#  Automatic build script for ass 
#  for iPhoneOS and iPhoneSimulator
#
#  Created by Felix Schulze on 19.02.12.
#  Copyright 2012 Felix Schulze. All rights reserved.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#
###########################################################################
#  Change values here                                                     #
#                                                                         #
MIN_VERSION="8.0"                                                         #
#                                                                         #
###########################################################################
#                                                                         #
# Don't change anything under this line!                                  #
#								          #
###########################################################################


SDKVERSION=`xcrun -sdk iphoneos --show-sdk-version`
CURRENTPATH=`pwd`
ARCHS="x86_64 arm64"
DEVELOPER=`xcode-select -print-path`

set -e
mkdir -p "${CURRENTPATH}/bin"
mkdir -p "${CURRENTPATH}/lib_tmp"

for ARCH in ${ARCHS}
do
    if [ "${ARCH}" == "x86_64" ];
    then
        PLATFORM="iPhoneSimulator"
    else
        PLATFORM="iPhoneOS"
    fi
	
    echo "Building ass for ${PLATFORM} ${SDKVERSION} ${ARCH}"
    echo "Please stand by..."
	
    export DEVROOT="${DEVELOPER}/Platforms/${PLATFORM}.platform/Developer"
    export SDKROOT="${DEVROOT}/SDKs/${PLATFORM}${SDKVERSION}.sdk"

    export LD=${DEVELOPER}/Toolchains/XcodeDefault.xctoolchain/usr/bin/ld
    export CC=${DEVELOPER}/usr/bin/gcc
    export CXX=${DEVELOPER}/usr/bin/g++

    export AR=${DEVELOPER}/Toolchains/XcodeDefault.xctoolchain/usr/bin/ar
    export AS=${DEVELOPER}/Toolchains/XcodeDefault.xctoolchain/usr/bin/as
    export NM=${DEVELOPER}/Toolchains/XcodeDefault.xctoolchain/usr/bin/nm
    export RANLIB=${DEVELOPER}/Toolchains/XcodeDefault.xctoolchain/usr/bin/ranlib

    export LDFLAGS="-arch ${ARCH} -pipe -no-cpp-precomp -isysroot ${SDKROOT} -L/Users/chadr/Downloads/libass/complete/ios/lib -L${CURRENTPATH}/lib_tmp -miphoneos-version-min=${MIN_VERSION} -fheinous-gnu-extensions"
    export CFLAGS="-arch ${ARCH} -pipe -no-cpp-precomp -isysroot ${SDKROOT} -DLIBICONV_PLUG -I/Users/chadr/Downloads/libass/complete/ios/include -I${CURRENTPATH}/include -miphoneos-version-min=${MIN_VERSION} -fheinous-gnu-extensions"
    export CPPFLAGS="-arch ${ARCH} -pipe -no-cpp-precomp -isysroot ${SDKROOT} -I${CURRENTPATH}/include -miphoneos-version-min=${MIN_VERSION} -fheinous-gnu-extensions"
    export CXXFLAGS="-arch ${ARCH} -pipe -no-cpp-precomp -isysroot ${SDKROOT} -I${CURRENTPATH}/include -miphoneos-version-min=${MIN_VERSION} -fheinous-gnu-extensions"

    HOST="${ARCH}"
    if [ "${ARCH}" == "arm64" ];
    then
        HOST="aarch64"
    fi

    mkdir -p "${CURRENTPATH}/bin/${PLATFORM}${SDKVERSION}-${ARCH}.sdk"
    LOG="${CURRENTPATH}/bin/${PLATFORM}${SDKVERSION}-${ARCH}.sdk/build-ass.log"

    echo "Configure..."
    ./configure --disable-fontconfig --host="${HOST}-apple-darwin" --prefix="${CURRENTPATH}/bin/${PLATFORM}${SDKVERSION}-${ARCH}.sdk" --disable-shared --enable-static > "${LOG}" 2>&1
    echo "Make..."
    make -j4 >> "${LOG}" 2>&1
    echo "Make install..."
    make install >> "${LOG}" 2>&1
    make clean >> "${LOG}" 2>&1
    cd "${CURRENTPATH}"
done

echo "Build library..."
lipo -create ${CURRENTPATH}/bin/iPhoneSimulator${SDKVERSION}-x86_64.sdk/lib/libass.a \
             ${CURRENTPATH}/bin/iPhoneOS${SDKVERSION}-arm64.sdk/lib/libass.a \
             -output ${CURRENTPATH}/lib_tmp/libass.a

lipo -info ${CURRENTPATH}/lib_tmp/libass.a
mkdir -p ${CURRENTPATH}/../lib
cp ${CURRENTPATH}/lib_tmp/libass.a ${CURRENTPATH}/../lib
mkdir -p ${CURRENTPATH}/../include/ass
cp  ${CURRENTPATH}/bin/iPhoneSimulator${SDKVERSION}-x86_64.sdk/include/ass/* ${CURRENTPATH}/../include/ass
echo "Building done."
echo "Done."
