#!/bin/bash
TOOLCHAIN=/Users/chadr/Downloads/toolchain/x86
NDK=/Users/chadr/Downloads/android-ndk-r18b
USERROOT=$NDK/platforms/android-16/arch-x86/
PKG_CONFIG=/Users/chadr/Downloads/tools/bin/pkg-config

export PKG_CONFIG_PATH=$USERROOT/usr/lib/pkgconfig

function build_one
{
CFLAGS="-Os" \
LDFLAGS="-L$USERROOT/usr/lib -lm" \
 ./configure \
    --prefix=$PREFIX \
    --enable-shared \
    --disable-static \
    --disable-doc \
    --disable-ffmpeg \
    --disable-ffplay \
    --disable-ffprobe \
    --disable-avdevice \
    --disable-postproc \
    --disable-encoders \
    --enable-encoder="ac3,dca" \
    --disable-muxers \
    --enable-muxer="spdif" \
    --enable-gpl \
    --enable-runtime-cpudetect \
    --enable-bzlib \
    --enable-zlib \
    --enable-openssl \
    --enable-libspeex \
    --enable-libilbc \
    --enable-libdav1d \
    --enable-jni \
    --enable-mediacodec \
    --disable-asm \
    --disable-doc \
    --disable-symver \
    --cross-prefix=$TOOLCHAIN/bin/i686-linux-android- \
    --target-os=android \
    --arch=x86 \
    --cpu=i686 \
    --enable-cross-compile \
    --enable-pic \
    --pkg-config=$PKG_CONFIG \
    --extra-cflags="$ADDI_CFLAGS" \
    --extra-ldflags="$ADDI_LDFLAGS" \
    $ADDITIONAL_CONFIGURE_FLAG
make clean
make -j4
make install
}
CPU=x86
PREFIX=$(pwd)/android/$CPU 
ADDI_CFLAGS="-Os"
ADDI_LDFLAGS=""
build_one
