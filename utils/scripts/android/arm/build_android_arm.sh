#!/bin/bash
TOOLCHAIN=/Users/chadr/Downloads/toolchain/arm
NDK=/Users/chadr/Downloads/android-ndk-r18b
USERROOT=$NDK/platforms/android-16/arch-arm/
PKG_CONFIG=/Users/chadr/Downloads/tools/bin/pkg-config

export PKG_CONFIG_PATH=$USERROOT/usr/lib/pkgconfig

function build_one
{
CFLAGS="-Os -mfloat-abi=softfp -mfpu=neon -march=armv7-a -mtune=cortex-a8 -I$USERROOT/usr/include" \
LDFLAGS="-mfloat-abi=softfp -mfpu=neon -march=armv7-a -mtune=cortex-a8 -L$USERROOT/usr/lib -lm" \
 ./configure \
    --prefix=$PREFIX \
    --enable-shared \
    --disable-static \
    --disable-doc \
    --disable-ffmpeg \
    --disable-ffplay \
    --disable-ffprobe \
    --disable-avdevice \
    --disable-postproc \
    --disable-encoders \
    --enable-encoder="ac3,dca" \
    --disable-muxers \
    --enable-muxer="spdif" \
    --enable-gpl \
    --enable-runtime-cpudetect \
    --enable-bzlib \
    --enable-zlib \
    --enable-openssl \
    --enable-libspeex \
    --enable-libilbc \
    --enable-libdav1d \
    --enable-jni \
    --enable-mediacodec \
    --disable-doc \
    --disable-symver \
    --cross-prefix=$TOOLCHAIN/bin/arm-linux-androideabi- \
    --target-os=android \
    --arch=arm \
    --cpu=cortex-a8 \
    --enable-cross-compile \
    --enable-pic \
    --pkg-config=$PKG_CONFIG \
    --extra-cflags="$ADDI_CFLAGS" \
    --extra-ldflags="$ADDI_LDFLAGS" \
    $ADDITIONAL_CONFIGURE_FLAG
make clean
make -j4
make install
}
CPU=arm
PREFIX=$(pwd)/android/$CPU 
ADDI_CFLAGS="-Os -mfloat-abi=softfp -mfpu=neon -march=armv7-a -mtune=cortex-a8"
ADDI_LDFLAGS="-mfloat-abi=softfp -mfpu=neon -march=armv7-a -mtune=cortex-a8"
build_one
