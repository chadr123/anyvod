#!/bin/bash

cd build/lib
cp libavcodec.so.58 libavformat.so.58 libavutil.so.56 libavfilter.so.7 libswresample.so.3 libswscale.so.5 ~/project/anyvod/client/AnyVODMobileClient/libs/raspi
cd -

cd ~/project/anyvod/client/AnyVODMobileClient/libs/raspi
rm -f libavcodec.so libavformat.so libavutil.so libavfilter.so libswresample.so libswscale.so
ln -s libavcodec.so.58 libavcodec.so 
ln -s libavformat.so.58 libavformat.so
ln -s libavutil.so.56 libavutil.so
ln -s libavfilter.so.7 libavfilter.so
ln -s libswresample.so.3 libswresample.so
ln -s libswscale.so.5 libswscale.so
cd -
