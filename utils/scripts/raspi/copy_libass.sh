#!/bin/bash

cp build/lib/libass.so.9 ~/project/anyvod/client/AnyVODMobileClient/libs/raspi
cd ~/project/anyvod/client/AnyVODMobileClient/libs/raspi
rm -f libass.so
ln -s libass.so.9 libass.so
cd -
