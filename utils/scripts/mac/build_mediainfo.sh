#! /bin/sh

cd MediaInfoLib/Project/GNU/Library
cp AddThisToRoot_DLL_compile.sh ../../../../
chmod 775 autogen.sh
./autogen.sh
cd ../../../../
cd ZenLib/Project/GNU/Library
chmod 775 autogen.sh
./autogen.sh
cd ../../../../
export CFLAGS="-stdlib=libc++ -mmacosx-version-min=10.13"
export CPPFLAGS="-stdlib=libc++ -mmacosx-version-min=10.13"
export LDFLAGS="-stdlib=libc++ -mmacosx-version-min=10.13"
chmod 775 AddThisToRoot_DLL_compile.sh
./AddThisToRoot_DLL_compile.sh
rm -rf ../../build
mkdir ../../build
cp -a MediaInfoLib/Project/GNU/Library/.libs/libmediainfo*.dylib ../../build
cd ../../build
install_name_tool -id @executable_path/libmediainfo.0.dylib libmediainfo.0.dylib
