all
export MACOSX_DEPLOYMENT_TARGET=10.13

common
./configure --prefix=/Users/chadr/Downloads/tools --disable-shared --enable-static

freetype
./configure --prefix=/Users/chadr/Downloads/tools --disable-shared --enable-static --with-png=no --with-harfbuzz=no --with-brotli=no

harfbuzz
./configure --prefix=/Users/chadr/Downloads/tools --disable-shared --enable-static --with-glib=no --with-icu=no

pkg-config
./configure --prefix=/Users/chadr/Downloads/tools --with-internal-glib

libass
./configure --prefix=/Users/chadr/Downloads/libass/build --disable-static --enable-shared --disable-fontconfig PKG_CONFIG="/Users/chadr/Downloads/tools/bin/pkg-config" LIBS="-lbz2 -lz"

make install-strip
