<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>ContextMenuTextInput</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/ContextMenuTextInput.qml" line="102"/>
        <source>잘라내기</source>
        <translation>Cut</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ContextMenuTextInput.qml" line="117"/>
        <source>복사하기</source>
        <translation>Copy</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ContextMenuTextInput.qml" line="131"/>
        <source>붙여넣기</source>
        <translation>Paste</translation>
    </message>
</context>
<context>
    <name>DTVChannelList</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/DTVChannelList.qml" line="112"/>
        <source>채널 </source>
        <translation>Channel </translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/DTVChannelList.qml" line="112"/>
        <source>, 신호 감도 </source>
        <translation>, Signal Strength </translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/DTVChannelList.qml" line="116"/>
        <source>이름 없음</source>
        <translation>No Name</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/DTVChannelList.qml" line="156"/>
        <source>채널 목록이 없습니다.</source>
        <translation>There are no channels.</translation>
    </message>
</context>
<context>
    <name>DTVSettingItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/DTVSettingItems.qml" line="21"/>
        <source>채널 목록</source>
        <translation>Channel List</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/DTVSettingItems.qml" line="23"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/DTVSettingItems.qml" line="22"/>
        <source>채널 검색</source>
        <translation>Scan</translation>
    </message>
</context>
<context>
    <name>DirItemMenuItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/DirItemMenuItems.qml" line="22"/>
        <source>전체 재생</source>
        <translation>Play all</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/DirItemMenuItems.qml" line="27"/>
        <source>삭제</source>
        <translation>Delete</translation>
    </message>
</context>
<context>
    <name>EnumsTranslator</name>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="56"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="150"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="193"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="200"/>
        <source>사용 안 함</source>
        <translation>Don&apos;t Use</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="57"/>
        <source>왼쪽 영상 사용</source>
        <translation>Use Left Image</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="58"/>
        <source>오른쪽 영상 사용</source>
        <translation>Use Right Image</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="59"/>
        <source>상단 영상 사용</source>
        <translation>Use Top Image</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="60"/>
        <source>하단 영상 사용</source>
        <translation>Use Bottom Image</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="61"/>
        <source>좌우 영상 사용</source>
        <translation>Use Side by Side Image</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="62"/>
        <source>상하 영상 사용</source>
        <translation>Use Top and Bottom Image</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="63"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="67"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="71"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="75"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="79"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="83"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="87"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="91"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="95"/>
        <source>왼쪽 영상 우선 사용</source>
        <translation>Left Image Priority</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="64"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="68"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="72"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="76"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="80"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="84"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="88"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="92"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="96"/>
        <source>오른쪽 영상 우선 사용</source>
        <translation>Right Image Priority</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="65"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="69"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="73"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="77"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="81"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="85"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="89"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="93"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="97"/>
        <source>상단 영상 우선 사용</source>
        <translation>Top Image Priority</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="66"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="70"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="74"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="78"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="82"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="86"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="90"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="94"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="98"/>
        <source>하단 영상 우선 사용</source>
        <translation>Bottom Image Priority</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="103"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="104"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="105"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="106"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="107"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="108"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="109"/>
        <source>일반</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="110"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="111"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="112"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="113"/>
        <source>Page Flipping</source>
        <translation>Page Flipping</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="114"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="115"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="116"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="117"/>
        <source>Row Interlaced</source>
        <translation>Row Interlaced</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="118"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="119"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="120"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="121"/>
        <source>Column Interlaced</source>
        <translation>Column Interlaced</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="122"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="123"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="124"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="125"/>
        <source>Red-Cyan Anaglyph</source>
        <translation>Red-Cyan Anaglyph</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="126"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="127"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="128"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="129"/>
        <source>Green-Magenta Anaglyph</source>
        <translation>Green-Magenta Anaglyph</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="130"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="131"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="132"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="133"/>
        <source>Yellow-Blue Anaglyph</source>
        <translation>Yellow-Blue Anaglyph</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="134"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="135"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="136"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="137"/>
        <source>Red-Blue Anaglyph</source>
        <translation>Red-Blue Anaglyph</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="138"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="139"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="140"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="141"/>
        <source>Red-Green Anaglyph</source>
        <translation>Red-Green Anaglyph</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="142"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="143"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="144"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="145"/>
        <source>Checker Board</source>
        <translation>Checker Board</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="151"/>
        <source>상/하</source>
        <translation>Vert</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="152"/>
        <source>좌/우</source>
        <translation>Horiz</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="153"/>
        <source>페이지 플리핑</source>
        <translation>Page Flipping</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="154"/>
        <source>인터레이스</source>
        <translation>Interlaced</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="155"/>
        <source>체커 보드</source>
        <translation>Checker Board</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="156"/>
        <source>애너글리프</source>
        <translation>Anaglyph</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="182"/>
        <source>확인하지 않음</source>
        <translation>Don&apos;t Check</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="183"/>
        <source>매 실행 시</source>
        <translation>Every Launch</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="184"/>
        <source>1일</source>
        <translation>Every Day</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="185"/>
        <source>1주일</source>
        <translation>Every Week</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="186"/>
        <source>한 달</source>
        <translation>Every Month</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="187"/>
        <source>반 년</source>
        <translation>Every Half of Year</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="188"/>
        <source>1년</source>
        <translation>Every Year</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="194"/>
        <source>VR 평면 영상</source>
        <translation>Normal VR Mode</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="195"/>
        <source>360도 영상</source>
        <translation>360° Video</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="201"/>
        <source>좌우 영상(좌측 영상 우선)</source>
        <translation>Side by Side(Left Image Priority)</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="202"/>
        <source>좌우 영상(우측 영상 우선)</source>
        <translation>Side by Side(Right Image Priority)</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="203"/>
        <source>상하 영상(상단 영상 우선)</source>
        <translation>Top and Bottom(Top Image Priority)</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="204"/>
        <source>상하 영상(하단 영상 우선)</source>
        <translation>Top and Bottom(Bottom Image Priority)</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="205"/>
        <source>복제</source>
        <translation>Copy</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="206"/>
        <source>가상 3D</source>
        <translation>Virtual 3D</translation>
    </message>
</context>
<context>
    <name>Equalizer</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/Equalizer.qml" line="61"/>
        <location filename="../qml/AnyVODMobileClient/Equalizer.qml" line="135"/>
        <source>프리셋</source>
        <translation>Presets</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Equalizer.qml" line="167"/>
        <source>초기화</source>
        <translation>Reset</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Equalizer.qml" line="191"/>
        <source>이퀄라이저 사용</source>
        <translation>Use Equalizer</translation>
    </message>
</context>
<context>
    <name>EqualizerDelegate</name>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="173"/>
        <source>기본 값</source>
        <translation>Default</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="174"/>
        <source>클래식</source>
        <translation>Classical</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="175"/>
        <source>베이스</source>
        <translation>Bass</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="176"/>
        <source>베이스 &amp; 트레블</source>
        <translation>Bass &amp; Treble</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="177"/>
        <source>트레블</source>
        <translation>Treble</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="178"/>
        <source>헤드폰</source>
        <translation>Headset</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="179"/>
        <source>홀</source>
        <translation>Hall</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="180"/>
        <source>소프트 락</source>
        <translation>Soft Rock</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="181"/>
        <source>클럽</source>
        <translation>Club</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="182"/>
        <source>댄스</source>
        <translation>Dance</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="183"/>
        <source>라이브</source>
        <translation>Live</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="184"/>
        <source>파티</source>
        <translation>Party</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="185"/>
        <source>팝</source>
        <translation>Pop</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="186"/>
        <source>레게</source>
        <translation>Reggae</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="187"/>
        <source>락</source>
        <translation>Rock</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="188"/>
        <source>스카</source>
        <translation>Ska</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="189"/>
        <source>소프트</source>
        <translation>Soft</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="190"/>
        <source>테크노</source>
        <translation>Techno</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="191"/>
        <source>보컬</source>
        <translation>Vocal</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="192"/>
        <source>재즈</source>
        <translation>Jazz</translation>
    </message>
</context>
<context>
    <name>FileItemMenuItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/FileItemMenuItems.qml" line="22"/>
        <source>장면 탐색</source>
        <translation>Explore Screen</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/FileItemMenuItems.qml" line="27"/>
        <source>삭제</source>
        <translation>Delete</translation>
    </message>
</context>
<context>
    <name>FileSystemDialog</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/FileSystemDialog.qml" line="294"/>
        <source>내장 메모리</source>
        <translation>Internal Storage</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/FileSystemDialog.qml" line="331"/>
        <source>외장 메모리</source>
        <translation>External Storage</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/FileSystemDialog.qml" line="371"/>
        <source>파일 이름 :</source>
        <translation>File Name :</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/FileSystemDialog.qml" line="472"/>
        <source>%1 항목</source>
        <translation>%1 units</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/FileSystemDialog.qml" line="472"/>
        <source>갱신 중...</source>
        <translation>Updating...</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/FileSystemDialog.qml" line="541"/>
        <source>파일 또는 디렉토리가 없습니다</source>
        <translation>There are no files or directories</translation>
    </message>
</context>
<context>
    <name>FooterWithButtons</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/FooterWithButtons.qml" line="55"/>
        <source>열기</source>
        <translation>Open</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/FooterWithButtons.qml" line="69"/>
        <source>확인</source>
        <translation>Confirm</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/FooterWithButtons.qml" line="84"/>
        <source>닫기</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/FooterWithButtons.qml" line="98"/>
        <source>저장</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/FooterWithButtons.qml" line="112"/>
        <source>신규 저장</source>
        <translation>New Playlist</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/FooterWithButtons.qml" line="126"/>
        <source>취소</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/FooterWithButtons.qml" line="140"/>
        <source>갱신</source>
        <translation>Update</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/FooterWithButtons.qml" line="154"/>
        <source>전원 끄기</source>
        <translation>Power Off</translation>
    </message>
</context>
<context>
    <name>GLRenderer</name>
    <message>
        <location filename="../src/video/GLRenderer.cpp" line="352"/>
        <source>캡쳐 성공</source>
        <translation>Capturing Succeed</translation>
    </message>
    <message>
        <location filename="../src/video/GLRenderer.cpp" line="354"/>
        <source>캡쳐 실패</source>
        <translation>Capturing Failed</translation>
    </message>
    <message>
        <location filename="../src/video/GLRenderer.cpp" line="479"/>
        <source>캡쳐를 시작하지 못했습니다</source>
        <translation>Can&apos;t start capture</translation>
    </message>
</context>
<context>
    <name>InfoSettingItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/InfoSettingItems.qml" line="21"/>
        <source>재생 정보 보기</source>
        <translation>Show Playback Information</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/InfoSettingItems.qml" line="23"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/InfoSettingItems.qml" line="22"/>
        <source>AnyVOD 정보</source>
        <translation>About AnyVOD</translation>
    </message>
</context>
<context>
    <name>Initial</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="39"/>
        <source>캡처 확장자</source>
        <translation>Extension of Capturing</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="58"/>
        <source>글꼴</source>
        <translation>Fonts</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="85"/>
        <source>디인터레이스 활성화 기준</source>
        <translation>Deinterlacing Methods</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="89"/>
        <source>자동</source>
        <translation>Auto</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="90"/>
        <source>사용</source>
        <translation>Use</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="91"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="254"/>
        <source>사용 안 함</source>
        <translation>Don&apos;t Use</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="102"/>
        <source>디인터레이스 알고리즘</source>
        <translation>Deinterlacing Algorithm</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="106"/>
        <source>Blend</source>
        <translation>Blend</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="107"/>
        <source>BOB</source>
        <translation>BOB</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="108"/>
        <source>YADIF</source>
        <translation>YADIF</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="109"/>
        <source>YADIF(BOB)</source>
        <translation>YADIF(BOB)</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="110"/>
        <source>Weston 3 Field</source>
        <translation>Weston 3 Field</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="111"/>
        <source>Adaptive Kernel</source>
        <translation>Adaptive Kernel</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="112"/>
        <source>Motion Compensation</source>
        <translation>Motion Compensation</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="113"/>
        <source>Bob Weaver</source>
        <translation>Bob Weaver</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="114"/>
        <source>Bob Weaver(BOB)</source>
        <translation>Bob Weaver(BOB)</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="125"/>
        <source>자막 / 가사 가로 정렬</source>
        <translation>Horizontal Alignment</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="129"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="148"/>
        <source>기본 정렬</source>
        <translation>Default Align</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="130"/>
        <source>자동 정렬</source>
        <translation>Auto Align</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="131"/>
        <source>왼쪽 정렬</source>
        <translation>Left Align</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="132"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="150"/>
        <source>가운데 정렬</source>
        <translation>Middle Align</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="133"/>
        <source>오른쪽 정렬</source>
        <translation>Right Align</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="144"/>
        <source>자막 / 가사 세로 정렬</source>
        <translation>Vertical Alignment</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="149"/>
        <source>상단 정렬</source>
        <translation>Top Align</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="151"/>
        <source>하단 정렬</source>
        <translation>Bottom Align</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="160"/>
        <source>자막 / 가사 텍스트 인코딩</source>
        <translation>Text Encoding</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="179"/>
        <source>Languages</source>
        <translation>Languages</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="204"/>
        <source>오디오 장치</source>
        <translation>Audio Devices</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="211"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="233"/>
        <source>기본 장치</source>
        <translation>Default Device</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="226"/>
        <source>출력 장치</source>
        <translation>Output Device</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="250"/>
        <source>인코딩</source>
        <translation>Encoding</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="255"/>
        <source>AC3</source>
        <translation>AC3</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="256"/>
        <source>DTS</source>
        <translation>DTS</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="267"/>
        <source>샘플링 속도</source>
        <translation>Sampling Rate</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="290"/>
        <source>언어</source>
        <translation>Language</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="362"/>
        <source>앱 자동 업데이트 확인 주기</source>
        <translation>Auto-update Check Cycle</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="366"/>
        <source>확인하지 않음</source>
        <translation>Don&apos;t Check</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="367"/>
        <source>매 실행 시</source>
        <translation>Every Launch</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="368"/>
        <source>1일</source>
        <translation>Every Day</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="369"/>
        <source>1주일</source>
        <translation>Every Week</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="370"/>
        <source>한 달</source>
        <translation>Every Month</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="371"/>
        <source>반 년</source>
        <translation>Every Half of Year</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="372"/>
        <source>1년</source>
        <translation>Every Year</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="385"/>
        <source>이퀄라이저</source>
        <translation>Equalizer</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="394"/>
        <source>외부 열기</source>
        <translation>Open External</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="408"/>
        <source>프록시 설정</source>
        <translation>Proxy Settings</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="423"/>
        <source>서버 설정</source>
        <translation>Server Settings</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="441"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="503"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="563"/>
        <source>정보</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="442"/>
        <source>지원하지 않는 기능입니다.</source>
        <translation>Not supported feature.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="452"/>
        <source>파일을 열 수 없습니다.</source>
        <translation>Can&apos;t open the file.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="523"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="573"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="597"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="621"/>
        <source>질문</source>
        <translation>Question</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="451"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="461"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="471"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="482"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="493"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="543"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="553"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="636"/>
        <source>오류</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="462"/>
        <source>파일을 삭제 할 수 없습니다.</source>
        <translation>Can&apos;t delete the file.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="472"/>
        <source>디렉토리를 삭제 할 수 없습니다.</source>
        <translation>Can&apos;t delete the directory.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="483"/>
        <source>외장 메모리의 파일을 삭제 할 수 없습니다.
이는 안드로이드 정책에 의한 것일 수 있습니다.</source>
        <translation>Can not delete files in external memory.
This may be due to Android policy.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="494"/>
        <source>외장 메모리의 디렉토리를 삭제 할 수 없습니다.
이는 안드로이드 정책에 의한 것일 수 있습니다.</source>
        <translation>Can not delete directory of external memory.
This may be due to Android policy.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="504"/>
        <source>변경된 글꼴로 적용하기 위해서는 앱을 재 시작 해주세요.</source>
        <translation>Restart the app to apply the font.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="513"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="514"/>
        <source>Restart the app to apply changes.</source>
        <translation>Restart the app to apply changes.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="524"/>
        <source>종료 하시겠습니까?</source>
        <translation>Do you want to exit?</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="544"/>
        <source>설정을 불러 오지 못했습니다.</source>
        <translation>Can not load settings.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="554"/>
        <source>설정을 저장하지 못했습니다.</source>
        <translation>Can not save settings.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="564"/>
        <source>설정을 불러왔습니다. 적용하려면 앱를 재 시작 해주세요.</source>
        <translation>Your settings have been loaded. Restart app to take effect.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="574"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="598"/>
        <source>삭제 하시겠습니까?</source>
        <translation>Do you want to delete it?</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="622"/>
        <source>로그아웃 하시겠습니까?</source>
        <translation>Are you sure you want to sign out?</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="637"/>
        <source>로그인을 해야 합니다.</source>
        <translation>You need to sign in.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="645"/>
        <source>AnyVOD 정보</source>
        <translation>About AnyVOD</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="656"/>
        <source>라이센스</source>
        <translation>Licenses</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="667"/>
        <source>저장 디렉토리 설정</source>
        <translation>Select Save Directory</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="682"/>
        <source>설정 불러오기</source>
        <translation>Load Settings</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="704"/>
        <source>설정 저장하기</source>
        <translation>Save Settings</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="726"/>
        <source>블루투스 싱크</source>
        <translation>Bluetooth Audio Sync</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="727"/>
        <source>기기에 따라 값이 적용 되지 않을 수 있습니다.</source>
        <translation>Depending on the device, the value may not be applied.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="732"/>
        <source>초</source>
        <translation> Seconds</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="745"/>
        <source>재생 순서 설정</source>
        <translation>Playback Order</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="749"/>
        <source>전체 순차 재생</source>
        <translation>Full Sequential Playback</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="750"/>
        <source>전체 반복 재생</source>
        <translation>Full Repeat Playback</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="751"/>
        <source>한 개 재생</source>
        <translation>Play Single</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="752"/>
        <source>한 개 반복 재생</source>
        <translation>Repeat Single</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="753"/>
        <source>무작위 재생</source>
        <translation>Shuffle</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="762"/>
        <source>설정</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="858"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="909"/>
        <source>채널 검색</source>
        <translation>Scan</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="866"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="917"/>
        <source>채널 목록</source>
        <translation>Channel List</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="882"/>
        <source>DTV 열기</source>
        <translation>Open DTV</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="933"/>
        <source>라디오 열기</source>
        <translation>Open Radio</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="960"/>
        <source>원격 서버</source>
        <translation>Remote Server</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="1008"/>
        <source>재생 목록</source>
        <translation>Play List</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="1020"/>
        <source>검색</source>
        <translation>Search</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="1033"/>
        <source>원격 파일목록</source>
        <translation>Remote File List</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="1048"/>
        <source>원격 서버 로그인</source>
        <translation>Remote Server Sign in</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="1061"/>
        <source>장면 탐색</source>
        <translation>Explore Screen</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="1080"/>
        <source>업데이트 다운로드</source>
        <translation>Download Update</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="1082"/>
        <source>AnyVOD 업데이트가 존재합니다.
업데이트를 다운로드 하려면 여기를 클릭하세요.</source>
        <translation>There is an AnyVOD update.
If you want to download the update, click this.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="1545"/>
        <source>내장메모리</source>
        <translation>Internal</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="1581"/>
        <source>외장메모리</source>
        <translation>External</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="1611"/>
        <source>동영상</source>
        <translation>Movie</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="1647"/>
        <source>음악</source>
        <translation>Audio</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="1769"/>
        <source>%1 항목</source>
        <translation>%1 units</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="1769"/>
        <source>갱신 중...</source>
        <translation>Updating...</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="1891"/>
        <source>파일이 없습니다</source>
        <translation>There are no files</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="2650"/>
        <source>뒤로 버튼을 한 번 더 누르시면 종료합니다.</source>
        <translation>Press once more back button to exit.</translation>
    </message>
</context>
<context>
    <name>Login</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/Login.qml" line="84"/>
        <location filename="../qml/AnyVODMobileClient/Login.qml" line="94"/>
        <location filename="../qml/AnyVODMobileClient/Login.qml" line="104"/>
        <source>오류</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Login.qml" line="85"/>
        <source>아이디를 입력해 주세요.</source>
        <translation>Please enter your ID.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Login.qml" line="95"/>
        <source>비밀번호를 입력해 주세요.</source>
        <translation>Please enter a password.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Login.qml" line="105"/>
        <source>서버에 로그인 할 수 없습니다.
서버 주소, 아이디, 비밀번호가 올바른지 확인해 주세요.</source>
        <translatorcomment>서버에 로그인 할 수 없습니다.
서버 주소, 아이디, 비밀번호가 올바른지 확인해 주세요.</translatorcomment>
        <translation>Unable to sign in to the server.
Please verify that the server address, ID, and password are correct.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Login.qml" line="177"/>
        <source>아이디와 비밀번호를 입력해주세요</source>
        <translation>Please enter your ID and password</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Login.qml" line="192"/>
        <source>아이디</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Login.qml" line="214"/>
        <source>비밀번호</source>
        <translation>Password</translation>
    </message>
</context>
<context>
    <name>MainMenuItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainMenuItems.qml" line="23"/>
        <source>검색</source>
        <translation>Search</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainMenuItems.qml" line="29"/>
        <source>재생 목록</source>
        <translation>Play List</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainMenuItems.qml" line="41"/>
        <source>원격 서버</source>
        <translation>Remote Server</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainMenuItems.qml" line="53"/>
        <source>라디오 열기</source>
        <translation>Open Radio</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainMenuItems.qml" line="59"/>
        <source>이퀄라이저</source>
        <translation>Equalizer</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainMenuItems.qml" line="35"/>
        <source>외부 열기</source>
        <translation>Open External</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainMenuItems.qml" line="47"/>
        <source>DTV 열기</source>
        <translation>Open DTV</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainMenuItems.qml" line="65"/>
        <source>설정</source>
        <translation>Settings</translation>
    </message>
</context>
<context>
    <name>MainSettingItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="21"/>
        <source>AnyVOD 정보</source>
        <translation>About AnyVOD</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="21"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="22"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="23"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="24"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="25"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="26"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="27"/>
        <source>일반</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="22"/>
        <source>라이센스</source>
        <translation>Licenses</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="23"/>
        <source>Languages</source>
        <translation>Languages</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="24"/>
        <source>글꼴</source>
        <translation>Fonts</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="25"/>
        <source>앱 자동 업데이트 확인 주기</source>
        <translation>Auto-update Check Cycle</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="26"/>
        <source>프록시 설정</source>
        <translation>Proxy Settings</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="28"/>
        <source>설정 불러오기</source>
        <translation>Load Settings</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="28"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="29"/>
        <source>설정 관리</source>
        <translation>Manage Settings</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="29"/>
        <source>설정 저장하기</source>
        <translation>Save Settings</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="30"/>
        <source>하드웨어 디코더 사용</source>
        <translation>Use H/W Decoder</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="30"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="31"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="32"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="33"/>
        <source>화면</source>
        <translation>Screen</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="31"/>
        <source>저화질 모드 사용</source>
        <translation>Use Low Quality Mode</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="32"/>
        <source>프레임 드랍 사용</source>
        <translation>Use Frame Drops</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="33"/>
        <source>앨범 자켓 보기</source>
        <translation>View Album Jacket</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="34"/>
        <source>확장자</source>
        <translation>Extensions</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="34"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="35"/>
        <source>캡처</source>
        <translation>Capture</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="35"/>
        <source>저장 디렉토리 설정</source>
        <translation>Select Save Directory</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="36"/>
        <source>활성화 기준</source>
        <translation>Methods</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="36"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="37"/>
        <source>디인터레이스</source>
        <translation>Deinterlacing</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="37"/>
        <source>알고리즘</source>
        <translation>Algorithms</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="38"/>
        <source>재생 위치 기억</source>
        <translation>Remember Last Position</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="38"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="39"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="40"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="41"/>
        <source>재생</source>
        <translation>Playback</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="39"/>
        <source>키프레임 단위 이동</source>
        <translation>Seek to Keyframe</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="40"/>
        <source>버퍼링 모드 사용</source>
        <translation>Use Buffering Mode</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="41"/>
        <source>재생 순서 설정</source>
        <translation>Playback Order</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="42"/>
        <source>보이기</source>
        <translation>Show</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="42"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="43"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="44"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="45"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="46"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="47"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="48"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="49"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="50"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="51"/>
        <source>자막 / 가사</source>
        <translation>Subtitle / Lyrics</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="43"/>
        <source>고급 검색</source>
        <translation>Advanced Search</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="44"/>
        <source>자막 캐시 모드 사용</source>
        <translation>Use Subtitle Caching Mode</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="45"/>
        <source>가로 정렬</source>
        <translation>Horizontal Alignment</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="46"/>
        <source>세로 정렬</source>
        <translation>Vertical Alignment</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="47"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="57"/>
        <source>인코딩</source>
        <translation>Encoding</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="48"/>
        <source>자막 찾기</source>
        <translation>Enable Subtitles Searching</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="49"/>
        <source>가사 찾기</source>
        <translation>Enable Lyrics Searching</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="50"/>
        <source>찾은 가사 자동 저장</source>
        <translation>Autosave For Found Lyrics</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="51"/>
        <source>미디어 삭제 시 자막/가사 삭제</source>
        <translation>Delete Media With Subtitle/Lyrics</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="52"/>
        <source>오디오 장치</source>
        <translation>Audio Devices</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="53"/>
        <source>노멀라이저 사용</source>
        <translation>Use Normalizer</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="55"/>
        <source>블루투스 싱크</source>
        <translation>Bluetooth Audio Sync</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="56"/>
        <source>출력 장치</source>
        <translation>Output Device</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="56"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="57"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="58"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="59"/>
        <source>S/PDIF</source>
        <translation>S/PDIF</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="58"/>
        <source>샘플링 속도</source>
        <translation>Sampling Rate</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="59"/>
        <source>사용 하기</source>
        <translation>Use</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="60"/>
        <source>언어</source>
        <translation>Language</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="60"/>
        <source>키보드</source>
        <translation>Keyboard</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="52"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="53"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="54"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="55"/>
        <source>소리</source>
        <translation>Sound</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="27"/>
        <source>원격 서버 설정</source>
        <translation>Remote Server Settings</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="54"/>
        <source>이퀄라이저 사용</source>
        <translation>Use Equalizer</translation>
    </message>
</context>
<context>
    <name>ManagePlayList</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/ManagePlayList.qml" line="75"/>
        <source>질문</source>
        <translation>Question</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ManagePlayList.qml" line="76"/>
        <source>삭제 하시겠습니까?</source>
        <translation>Do you want to delete it?</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ManagePlayList.qml" line="90"/>
        <source>삭제</source>
        <translation>Delete</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ManagePlayList.qml" line="251"/>
        <source>재생 목록이 없습니다.</source>
        <translation>There are no playlists.</translation>
    </message>
</context>
<context>
    <name>MediaPresenter</name>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="2294"/>
        <source>프레임 저하가 일어나고 있습니다. 성능에 영향을 미치는 옵션 또는 수직 동기화를 꺼주세요.</source>
        <translation>Frame dropping is happening. Please disable options that affect performance, or vertical sync.</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="3002"/>
        <source>S/PDIF 출력 초기화를 실패 하였습니다. PCM 출력으로 전환합니다.</source>
        <translation>S/PDIF output initialization failed. Switch to the PCM output.</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="4192"/>
        <source>오프닝 스킵 : %1</source>
        <translation>Intro was skipped : %1</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="4213"/>
        <source>재생 스킵 : %1 ~ %2</source>
        <translation>Playback was skipped : %1 ~ %2</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="4331"/>
        <source>구간 반복 : %1 ~ %2</source>
        <translation>A-B was repeating : %1 ~ %2</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="4339"/>
        <source>S/PDIF 출력을 지원하지 않은 포맷이므로 PCM 출력으로 전환합니다.</source>
        <translation>Due to output does not support S/PDIF output format, it will be changed to PCM output.</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="4345"/>
        <source>하드웨어 디코딩을 지원하지 않는 코덱이므로 하드웨어 디코딩을 끕니다.</source>
        <translation>Turn off hardware decoding because it is a codec that does not support hardware decoding.</translation>
    </message>
</context>
<context>
    <name>OpenExternal</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/OpenExternal.qml" line="110"/>
        <source>외부 주소를 입력 하세요.
예) http://somehost.com/movie.mp4</source>
        <translation>Input external address.
Ex) http://somehost.com/movie.mp4</translation>
    </message>
</context>
<context>
    <name>PlayList</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="86"/>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="96"/>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="153"/>
        <source>정보</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="87"/>
        <source>지원하지 않는 기능입니다.</source>
        <translation>Not supported feature.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="97"/>
        <source>클립보드로 경로가 복사 되었습니다.</source>
        <translation>The URL is copied to clipboard.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="116"/>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="137"/>
        <source>질문</source>
        <translation>Question</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="106"/>
        <source>오류</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="107"/>
        <source>이름을 입력 해 주세요.</source>
        <translation>Input the name.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="117"/>
        <source>갱신 하시겠습니까?</source>
        <translation>Do you want to update?</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="138"/>
        <source>이미 동일한 재생 목록이 존재합니다.
갱신 하시겠습니까?</source>
        <translation>Playlist exists.
Do you want to update?</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="165"/>
        <source>화질 목록</source>
        <translation>Quality List</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="192"/>
        <source>재생 목록 신규 저장</source>
        <translation>New Playlist</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="225"/>
        <source>장면 탐색</source>
        <translation>Explore Screen</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="314"/>
        <source>재생 목록</source>
        <translation>Play List</translation>
    </message>
</context>
<context>
    <name>PlayListMenuItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayListMenuItems.qml" line="22"/>
        <source>경로 보기</source>
        <translation>View URL</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayListMenuItems.qml" line="27"/>
        <source>경로 복사</source>
        <translation>Copy URL</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayListMenuItems.qml" line="32"/>
        <source>장면 탐색</source>
        <translation>Explore Screen</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayListMenuItems.qml" line="37"/>
        <source>다른 화질</source>
        <translation>Another Quality</translation>
    </message>
</context>
<context>
    <name>PlaySettingItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlaySettingItems.qml" line="21"/>
        <source>재생 위치 기억</source>
        <translation>Remember Last Position</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlaySettingItems.qml" line="21"/>
        <location filename="../qml/AnyVODMobileClient/PlaySettingItems.qml" line="22"/>
        <location filename="../qml/AnyVODMobileClient/PlaySettingItems.qml" line="23"/>
        <location filename="../qml/AnyVODMobileClient/PlaySettingItems.qml" line="24"/>
        <location filename="../qml/AnyVODMobileClient/PlaySettingItems.qml" line="25"/>
        <location filename="../qml/AnyVODMobileClient/PlaySettingItems.qml" line="26"/>
        <source>재생</source>
        <translation>Playback</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlaySettingItems.qml" line="22"/>
        <source>키프레임 단위 이동</source>
        <translation>Seek to Keyframe</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlaySettingItems.qml" line="23"/>
        <source>버퍼링 모드 사용</source>
        <translation>Use Buffering Mode</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlaySettingItems.qml" line="24"/>
        <source>재생 속도 설정</source>
        <translation>Playback Speed</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlaySettingItems.qml" line="25"/>
        <source>재생 순서 설정</source>
        <translation>Playback Order</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlaySettingItems.qml" line="26"/>
        <source>구간 반복 설정</source>
        <translation>A-B Repeat Range</translation>
    </message>
</context>
<context>
    <name>ProxyInfo</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/ProxyInfo.qml" line="110"/>
        <source>id:password@ip:port 형태로 입력하세요.
예시)
    guest:mypass@192.168.0.10:8080
    192.168.0.10:8080</source>
        <translation>Please type in form as &quot;id:password@ip:port&quot;.
Ex)
    guest:mypass@192.168.0.10:8080
    192.168.0.10:8080</translation>
    </message>
</context>
<context>
    <name>RadioChannelList</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/RadioChannelList.qml" line="112"/>
        <source>채널 </source>
        <translation>Channel </translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/RadioChannelList.qml" line="112"/>
        <source>, 신호 감도 </source>
        <translation>, Signal Strength </translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/RadioChannelList.qml" line="156"/>
        <source>채널 목록이 없습니다.</source>
        <translation>There are no channels.</translation>
    </message>
</context>
<context>
    <name>RadioSettingItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/RadioSettingItems.qml" line="21"/>
        <source>채널 목록</source>
        <translation>Channel List</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/RadioSettingItems.qml" line="21"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/RadioSettingItems.qml" line="22"/>
        <source>채널 검색</source>
        <translation>Scan</translation>
    </message>
</context>
<context>
    <name>RemoteDirItemMenuItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/RemoteDirItemMenuItems.qml" line="22"/>
        <source>전체 재생</source>
        <translation>Play all</translation>
    </message>
</context>
<context>
    <name>RemoteFileListDialog</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/RemoteFileListDialog.qml" line="262"/>
        <source>갱신 중...</source>
        <translation>Updating...</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/RemoteFileListDialog.qml" line="354"/>
        <source>파일 또는 디렉토리가 없습니다</source>
        <translation>There are no files or directories</translation>
    </message>
</context>
<context>
    <name>RemoteServerSettingItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/RemoteServerSettingItems.qml" line="21"/>
        <source>로그인 / 로그아웃</source>
        <translation>Sign in / Sign out</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/RemoteServerSettingItems.qml" line="21"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/RemoteServerSettingItems.qml" line="22"/>
        <source>원격 파일 목록 열기</source>
        <translation>Open Remote File List</translation>
    </message>
</context>
<context>
    <name>RenderScreen</name>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="339"/>
        <source>자막 있음</source>
        <translation>Subtitle exists</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="341"/>
        <source>자막 없음</source>
        <translation>Subtitle does not exist</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="346"/>
        <source>가사 있음</source>
        <translation>Lyrics exists</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="348"/>
        <source>가사 없음</source>
        <translation>Lyrics does not exist</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="379"/>
        <source>일시정지</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="396"/>
        <source>재생</source>
        <translation>Play</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="464"/>
        <source>앞으로 %1초 키프레임 이동</source>
        <translation>Seek %1 Seconds Forward by Keyframe</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="466"/>
        <source>뒤로 %1초 키프레임 이동</source>
        <translation>Seek %1 Seconds Backward by Keyframe</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="471"/>
        <source>앞으로 %1초 이동</source>
        <translation>Seek %1 Seconds Forward</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="473"/>
        <source>뒤로 %1초 이동</source>
        <translation>Seek %1 Seconds Backward</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="939"/>
        <source>자막 열기 : %1</source>
        <translation>Subtitle was loaded : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="941"/>
        <source>가사 열기 : %1</source>
        <translation>Lyrics was loaded : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="959"/>
        <source>자막이 저장 되었습니다 : %1</source>
        <translation>Subtitle was saved : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="961"/>
        <source>가사가 저장 되었습니다 : %1</source>
        <translation>Lyrics was saved : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="966"/>
        <source>자막이 저장 되지 않았습니다 : %1</source>
        <translation>Subtitle was not saved : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="968"/>
        <source>가사가 저장 되지 않았습니다 : %1</source>
        <translation>Lyrics was not saved : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="986"/>
        <source>외부 자막 닫기</source>
        <translation>External subtitle was closed</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="988"/>
        <source>외부 가사 닫기</source>
        <translation>Externel lyrics was closed</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1246"/>
        <source>자막 보이기</source>
        <translation>Show Subtitle</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1248"/>
        <source>자막 숨기기</source>
        <translation>Hide Subtitle</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1253"/>
        <source>가사 보이기</source>
        <translation>Show Lyrics</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1255"/>
        <source>가사 숨기기</source>
        <translation>Hide Lyrics</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1275"/>
        <source>고급 자막 검색 사용</source>
        <translation>Advanced subtitle searching was activated</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1277"/>
        <source>고급 자막 검색 사용 안 함</source>
        <translation>Advanced subtitle searching was deactivated</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1282"/>
        <source>고급 가사 검색 사용</source>
        <translation>Advanced lyrics searching was activated</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1284"/>
        <source>고급 가사 검색 사용 안 함</source>
        <translation>Advanced lyrics searching was deactivated</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1341"/>
        <source>자막 언어 변경</source>
        <translation>Subtitle language was changed</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1343"/>
        <source>가사 언어 변경</source>
        <translation>Lyrics language was changed</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1360"/>
        <source>세로 자막 위치 : %1</source>
        <translation>Subtitle Vertical Position : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1371"/>
        <source>가로 자막 위치 : %1</source>
        <translation>Subtitle Horizontal Position : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1080"/>
        <source>블루투스 싱크 %1초 빠르게</source>
        <translation>Resync Bluetooth Audio (%1 Seconds faster)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1082"/>
        <source>블루투스 싱크 %1초 느리게</source>
        <translation>Resync Bluetooth Audio (%1 Seconds slower)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1392"/>
        <source>세로 3D 자막 위치 : %1</source>
        <translation>3D Subtitle Vertical Position : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1403"/>
        <source>가로 3D 자막 위치 : %1</source>
        <translation>3D Subtitle Horizontal Position : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1424"/>
        <source>가상 3D 입체감 : %1</source>
        <translation>Virtual 3D Depth : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1446"/>
        <source>구간 반복 시작 : %1</source>
        <translation>Beginning of A-B Repeat : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1459"/>
        <source>구간 반복 끝 : %1</source>
        <translation>Ending of A-B Repeat : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1475"/>
        <source>구간 반복 활성화</source>
        <translation>A-B Repeat On</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1477"/>
        <source>구간 반복 비활성화</source>
        <translation>A-B Repeat Off</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1483"/>
        <source>시작과 끝 시각이 같으므로 활성화 되지 않습니다</source>
        <translation>Because of A-B repeat start and end time is same, A-B repeat could not enable</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1511"/>
        <source>키프레임 단위로 이동 함</source>
        <translation>Seek to Keyframe</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1513"/>
        <source>키프레임 단위로 이동 안 함</source>
        <translation>Do not Seek to Keyframe</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1548"/>
        <source>3D 영상 (%1)</source>
        <translation>3D Video (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1550"/>
        <source>3D 영상 (%1 (%2))</source>
        <translation>3D Video (%1 (%2))</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1565"/>
        <source>3D 자막 (%1)</source>
        <translation>3D Subtitle (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1578"/>
        <source>VR 입력 영상 (%1)</source>
        <translation>VR Input Source (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1595"/>
        <source>헤드 트래킹 사용 함</source>
        <translation>Head Tracking On</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1597"/>
        <source>헤드 트래킹 사용 안 함</source>
        <translation>Head Tracking Off</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1616"/>
        <source>VR 왜곡 보정 사용 함</source>
        <translation>VR Correcting Distortion On</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1618"/>
        <source>VR 왜곡 보정 사용 안 함</source>
        <translation>VR Correcting Distortion Off</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1630"/>
        <source>평면 VR 왜곡 보정 계수 설정 (k1 : %1, k2 : %2)</source>
        <translation>Normal VR Distortion Coefficients (k1 : %1, k2 : %2)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1646"/>
        <source>360도 VR 왜곡 보정 계수 설정 (k1 : %1, k2 : %2)</source>
        <translation>VR Distortion Coefficients of 360° (k1 : %1, k2 : %2)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1662"/>
        <source>VR 렌즈 센터 설정 (X : %1, Y : %2)</source>
        <translation>VR Lens Center (X : %1, Y : %2)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1705"/>
        <source>노멀라이저 켜짐</source>
        <translation>Normalizer On</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1707"/>
        <source>노멀라이저 꺼짐</source>
        <translation>Normalizer Off</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1724"/>
        <source>이퀄라이저 켜짐</source>
        <translation>Equalizer On</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1726"/>
        <source>이퀄라이저 꺼짐</source>
        <translation>Equalizer Off</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1743"/>
        <source>음악 줄임 켜짐</source>
        <translation>Music Reducing On</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1745"/>
        <source>음악 줄임 꺼짐</source>
        <translation>Music Reducing Off</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1760"/>
        <source>자막 투명도</source>
        <translation>Subtitle Transparency</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1762"/>
        <source>가사 투명도</source>
        <translation>Lyrics Transparency</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1791"/>
        <source>자막 크기</source>
        <translation>Subtitle Font Size</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1832"/>
        <source>하드웨어 디코더 사용 함</source>
        <translation>Hardware video decoder was activated</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1835"/>
        <source> (그래픽 카드 또는 코덱이 지원하지 않을 경우 활성화가 안 될 수 있습니다)</source>
        <translation> (If your video card or codec does not support hardware video decoder, it may be not activated)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1839"/>
        <source>하드웨어 디코더 사용 안 함</source>
        <translation>Hardware video decoder was deactivated</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1857"/>
        <source>저화질 모드 사용 함</source>
        <translation>Low quality mode was activated</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1859"/>
        <source>저화질 모드 사용 안 함</source>
        <translation>Low quality mode was deactivated</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1876"/>
        <source>프레임 드랍 사용 함</source>
        <translation>Frame drop was activated</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1878"/>
        <source>프레임 드랍 사용 안 함</source>
        <translation>Frame drop was deactivated</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1895"/>
        <source>버퍼링 모드 사용 함</source>
        <translation>Buffering mode was activated</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1897"/>
        <source>버퍼링 모드 사용 안 함</source>
        <translation>Buffering mode was deactivated</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1915"/>
        <source>3D 전체 해상도 사용</source>
        <translation>3D full size was activated</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1917"/>
        <source>3D 전체 해상도 사용 안 함</source>
        <translation>3D full size was deactivated</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1934"/>
        <source>음성 줄임 켜짐</source>
        <translation>Voice Reducing On</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1936"/>
        <source>음성 줄임 꺼짐</source>
        <translation>Voice Reducing Off</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1953"/>
        <source>음성 강조 켜짐</source>
        <translation>Voice Emphasizing On</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1955"/>
        <source>음성 강조 꺼짐</source>
        <translation>Voice Emphasizing Off</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2256"/>
        <source>재생 속도 초기화</source>
        <translation>Reset Playback Speed</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2260"/>
        <source>재생 속도 : %1배</source>
        <translation>Playback Speed : %1 times</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2273"/>
        <source>화면 비율 사용 함</source>
        <translation>Screen aspect ratio was activated</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2276"/>
        <source> (화면 채우기)</source>
        <translation> (Stretch)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2282"/>
        <source>화면 비율 사용 안 함</source>
        <translation>Screen aspect ratio was deactivated</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2345"/>
        <source>자동 판단</source>
        <translation>Auto Detection</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2348"/>
        <source>항상 사용</source>
        <translation>Always Use</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2351"/>
        <location filename="../src/ui/RenderScreen.cpp" line="3022"/>
        <location filename="../src/ui/RenderScreen.cpp" line="3879"/>
        <source>사용 안 함</source>
        <translation>Don&apos;t Use</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2357"/>
        <source>디인터레이스 (%1)</source>
        <translation>Deinterlacing (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2365"/>
        <source>디인터레이스 알고리즘 (%1)</source>
        <translation>Deinterlacing Algorithm (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2384"/>
        <source>자막 가로 정렬 변경</source>
        <translation>Subtitle horizontal alignment was changed</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2386"/>
        <source>가사 가로 정렬 변경</source>
        <translation>Lyrics horizontal alignment was changed</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2391"/>
        <source>자동 정렬</source>
        <translation>Auto Align</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2394"/>
        <source>왼쪽 정렬</source>
        <translation>Left Align</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2397"/>
        <source>오른쪽 정렬</source>
        <translation>Right Align</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2400"/>
        <location filename="../src/ui/RenderScreen.cpp" line="2436"/>
        <source>가운데 정렬</source>
        <translation>Middle Align</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2403"/>
        <location filename="../src/ui/RenderScreen.cpp" line="2442"/>
        <source>기본 정렬</source>
        <translation>Default Align</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2426"/>
        <source>자막 세로 정렬 변경</source>
        <translation>Subtitle vertical alignment was changed</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2428"/>
        <source>가사 세로 정렬 변경</source>
        <translation>Lyrics vertical alignment was changed</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2433"/>
        <source>상단 정렬</source>
        <translation>Top Align</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2439"/>
        <source>하단 정렬</source>
        <translation>Bottom Align</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2471"/>
        <location filename="../src/ui/RenderScreen.cpp" line="3531"/>
        <source>자막 싱크 %1초 빠르게</source>
        <translation>Resync Subtitle (%1 Seconds faster)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2473"/>
        <location filename="../src/ui/RenderScreen.cpp" line="3533"/>
        <source>자막 싱크 %1초 느리게</source>
        <translation>Resync Subtitle (%1 Seconds slower)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2478"/>
        <location filename="../src/ui/RenderScreen.cpp" line="3538"/>
        <source>가사 싱크 %1초 빠르게</source>
        <translation>Resync Lyrics (%1 Seconds faster)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2480"/>
        <location filename="../src/ui/RenderScreen.cpp" line="3540"/>
        <source>가사 싱크 %1초 느리게</source>
        <translation>Resync Lyrics (%1 Seconds slower)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2486"/>
        <location filename="../src/ui/RenderScreen.cpp" line="2562"/>
        <source> (%1초)</source>
        <translation> (%1 Seconds)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2506"/>
        <source>자막 찾기 켜짐</source>
        <translation>Subtitle searching was activated</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2508"/>
        <source>자막 찾기 꺼짐</source>
        <translation>Subtitle searching was deactivated</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2519"/>
        <source>가사 찾기 켜짐</source>
        <translation>Lyrics searching was activated</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2521"/>
        <source>가사 찾기 꺼짐</source>
        <translation>Lyrics searching was deactivated</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2532"/>
        <source>찾은 가사 자동 저장 켜짐</source>
        <translation>Autosave for found lyrics activated</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2534"/>
        <source>찾은 가사 자동 저장 꺼짐</source>
        <translation>Autosave for found lyrics deactivated</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2555"/>
        <location filename="../src/ui/RenderScreen.cpp" line="3560"/>
        <source>소리 싱크 %1초 빠르게</source>
        <translation>Resync Sound (%1 Seconds faster)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2557"/>
        <location filename="../src/ui/RenderScreen.cpp" line="3562"/>
        <source>소리 싱크 %1초 느리게</source>
        <translation>Resync Sound (%1 Seconds slower)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2587"/>
        <source>소리 (%1%)</source>
        <translation>Volume (%1%)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2599"/>
        <source>소리 꺼짐</source>
        <translation>Mute On</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2601"/>
        <source>소리 켜짐</source>
        <translation>Mute Off</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2669"/>
        <source>앨범 자켓 보이기</source>
        <translation>Show Album Jacket</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2671"/>
        <source>앨범 자켓 숨기기</source>
        <translation>Hide Album Jacket</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2685"/>
        <source>이동 : %1 [%2]</source>
        <translation>Move To : %1 [%2]</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2709"/>
        <source>재생 위치 기억 켜짐</source>
        <translation>Remember Last Position On</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2711"/>
        <source>재생 위치 기억 꺼짐</source>
        <translation>Remember Last Position Off</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2853"/>
        <source>영상을 변경 할 수 없습니다</source>
        <translation>Can not change video</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2862"/>
        <source>음성을 변경 할 수 없습니다</source>
        <translation>Can not change audio</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3024"/>
        <source>화면 채우기</source>
        <translation>Stretch</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3026"/>
        <source>사용자 지정 (%1:%2)</source>
        <translation>Custom (%1:%2)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3581"/>
        <source>자막 캐시 모드 사용</source>
        <translation>Subtitle caching mode was activated</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3583"/>
        <source>자막 캐시 모드 사용 안 함</source>
        <translation>Subtitle caching mode was deactivated</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3658"/>
        <location filename="../src/ui/RenderScreen.cpp" line="3846"/>
        <source>기본 장치</source>
        <translation>Default Device</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3677"/>
        <source>S/PDIF 소리 출력 장치 변경 (%1)</source>
        <translation>S/PDIF sound output device was changed (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3679"/>
        <location filename="../src/ui/RenderScreen.cpp" line="3708"/>
        <location filename="../src/ui/RenderScreen.cpp" line="3746"/>
        <location filename="../src/ui/RenderScreen.cpp" line="3778"/>
        <source>S/PDIF 출력 초기화를 실패 하였습니다. PCM 출력으로 전환합니다.</source>
        <translation>S/PDIF output initialization failed. Switch to the PCM output.</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3702"/>
        <source>S/PDIF 출력 사용</source>
        <translation>Use S/PDIF Output</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3704"/>
        <source>S/PDIF 출력 사용 안 함</source>
        <translation>S/PDIF output was deactivated</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3732"/>
        <source>S/PDIF 출력 시 인코딩 사용 안 함</source>
        <translation>Encoding was deactivated</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3735"/>
        <source>S/PDIF 출력 시 AC3 인코딩 사용</source>
        <translation>AC3 encoding was activated</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3738"/>
        <source>S/PDIF 출력 시 DTS 인코딩 사용</source>
        <translation>DTS encoding was activated</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3769"/>
        <source>S/PDIF 샘플 속도 (%1)</source>
        <translation>S/PDIF Sampling Rate (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3772"/>
        <source>기본 속도</source>
        <translation>Default</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3801"/>
        <source>왜곡 보정 모드 사용 안 함</source>
        <translation>Distortion adjustment mode was deactivated</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3805"/>
        <source>왜곡 보정 모드 사용 : %1</source>
        <translation>Distortion adjustment mode was activated : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3862"/>
        <source>소리 출력 장치 변경 (%1)</source>
        <translation>Sound output device was changed (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3882"/>
        <source>90도</source>
        <translation>90°</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3885"/>
        <source>180도</source>
        <translation>180°</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3888"/>
        <source>270도</source>
        <translation>270°</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3897"/>
        <source>화면 회전 각도 (%1)</source>
        <translation>Degree of screen rotation (%1)</translation>
    </message>
</context>
<context>
    <name>RepeatRange</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/RepeatRange.qml" line="117"/>
        <source>구간 반복 설정</source>
        <translation>A-B Repeat Range</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/RepeatRange.qml" line="148"/>
        <location filename="../qml/AnyVODMobileClient/RepeatRange.qml" line="165"/>
        <source>시작</source>
        <translation>Begin</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/RepeatRange.qml" line="192"/>
        <location filename="../qml/AnyVODMobileClient/RepeatRange.qml" line="209"/>
        <source>종료</source>
        <translation>End</translation>
    </message>
</context>
<context>
    <name>SPDIFDelegate</name>
    <message>
        <location filename="../src/ui/delegates/SPDIFDelegate.cpp" line="46"/>
        <source>기본 속도</source>
        <translation>Default</translation>
    </message>
</context>
<context>
    <name>SPDIFInterface</name>
    <message>
        <location filename="../../AnyVODClient/src/audio/SPDIFInterface.cpp" line="104"/>
        <source>기본 장치</source>
        <translation>Default Device</translation>
    </message>
</context>
<context>
    <name>SavePlayListAs</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/SavePlayListAs.qml" line="108"/>
        <source>재생 목록 이름을 입력하세요.</source>
        <translation>Input the playlist name.</translation>
    </message>
</context>
<context>
    <name>ScanDTVChannel</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="171"/>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="299"/>
        <source>어댑터</source>
        <translation>Adapter</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="197"/>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="331"/>
        <source>형식</source>
        <translation>System Type</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="224"/>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="363"/>
        <source>국가</source>
        <translation>Country</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="400"/>
        <source>채널 범위</source>
        <translation>Channel Range</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="484"/>
        <source>검색</source>
        <translation>Scan</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="530"/>
        <source>중지</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="563"/>
        <source>저장</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="600"/>
        <source>현재 채널 : </source>
        <translation>Channel : </translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="609"/>
        <source>, 찾은 채널 개수 : </source>
        <translation>, Channel Count : </translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="625"/>
        <source>신호 감도</source>
        <translation>Signal Strength</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="724"/>
        <source>채널 </source>
        <translation>Channel </translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="724"/>
        <source>, 신호 강도 </source>
        <translation>, Signal Strength </translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="728"/>
        <source>이름 없음</source>
        <translation>No Name</translation>
    </message>
</context>
<context>
    <name>ScanRadioChannel</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanRadioChannel.qml" line="171"/>
        <location filename="../qml/AnyVODMobileClient/ScanRadioChannel.qml" line="299"/>
        <source>어댑터</source>
        <translation>Adapter</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanRadioChannel.qml" line="197"/>
        <location filename="../qml/AnyVODMobileClient/ScanRadioChannel.qml" line="331"/>
        <source>형식</source>
        <translation>System Type</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanRadioChannel.qml" line="224"/>
        <location filename="../qml/AnyVODMobileClient/ScanRadioChannel.qml" line="363"/>
        <source>국가</source>
        <translation>Country</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanRadioChannel.qml" line="400"/>
        <source>채널 범위</source>
        <translation>Channel Range</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanRadioChannel.qml" line="484"/>
        <source>검색</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanRadioChannel.qml" line="529"/>
        <source>중지</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanRadioChannel.qml" line="562"/>
        <source>저장</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanRadioChannel.qml" line="599"/>
        <source>현재 채널 : </source>
        <translation>Channel : </translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanRadioChannel.qml" line="608"/>
        <source>, 찾은 채널 개수 : </source>
        <translation>, Channel Count : </translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanRadioChannel.qml" line="624"/>
        <source>신호 감도</source>
        <translation>Signal Strength</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanRadioChannel.qml" line="723"/>
        <source>채널 </source>
        <translation>Channel </translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanRadioChannel.qml" line="723"/>
        <source>, 신호 감도 </source>
        <translation>, Signal Strength </translation>
    </message>
</context>
<context>
    <name>ScreenExplorer</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenExplorer.qml" line="132"/>
        <source>시</source>
        <translation>Hour</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenExplorer.qml" line="141"/>
        <source>분</source>
        <translation>Minute</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenExplorer.qml" line="150"/>
        <source>초</source>
        <translation>Second</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenExplorer.qml" line="194"/>
        <source>갱신</source>
        <translation>Update</translation>
    </message>
</context>
<context>
    <name>ScreenSettingItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="21"/>
        <source>비율</source>
        <translation>Aspect Ratio</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="21"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="22"/>
        <source>화면 비율</source>
        <translation>Screen Aspect Ratio</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="22"/>
        <source>사용자 지정</source>
        <translation>Custom</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="24"/>
        <source>회전 각도</source>
        <translation>Rotation Degree</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="23"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="24"/>
        <source>영상</source>
        <translation>Screen</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="25"/>
        <source>밝기</source>
        <translation>Brightness</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="25"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="26"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="27"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="28"/>
        <source>영상 속성</source>
        <translation>Video Attributes</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="26"/>
        <source>채도</source>
        <translation>Saturation</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="27"/>
        <source>색상</source>
        <translation>Hue</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="28"/>
        <source>대비</source>
        <translation>Contrast</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="29"/>
        <source>날카롭게</source>
        <translation>Sharply</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="29"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="30"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="31"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="32"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="33"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="34"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="35"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="36"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="37"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="38"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="39"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="40"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="41"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="42"/>
        <source>영상 효과</source>
        <translation>Video Effects</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="30"/>
        <source>선명하게</source>
        <translation>Sharpen</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="31"/>
        <source>부드럽게</source>
        <translation>Soften</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="32"/>
        <source>히스토그램 이퀄라이저</source>
        <translation>Histogram Equalization</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="33"/>
        <source>디밴드</source>
        <translation>Deband</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="34"/>
        <source>디블록</source>
        <translation>Deblock</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="35"/>
        <source>노멀라이즈</source>
        <translation>Normalize</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="36"/>
        <source>3D 노이즈 제거</source>
        <translation>3D denoise</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="37"/>
        <source>적응 시간 평균 노이즈 제거</source>
        <translation>Adaptive temporal averaging denoise</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="38"/>
        <source>Overcomplete Wavelet 노이즈 제거</source>
        <translation>Overcomplete wavelet denoise</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="39"/>
        <source>Non-local Means 노이즈 제거</source>
        <translation>Non-local means denoise</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="40"/>
        <source>Vague 노이즈 제거</source>
        <translation>Vague denoise</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="41"/>
        <source>좌우 반전</source>
        <translation>Mirroring</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="42"/>
        <source>상하 반전</source>
        <translation>Flip Vertically</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="43"/>
        <source>캡처 확장자</source>
        <translation>Extension of Capturing</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="43"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="44"/>
        <source>캡처</source>
        <translation>Capture</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="44"/>
        <source>캡처 저장 디렉토리 설정</source>
        <translation>Set the Save Directory of Capturing</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="45"/>
        <source>활성화 기준</source>
        <translation>Methods</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="45"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="46"/>
        <source>디인터레이스</source>
        <translation>Deinterlacing</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="46"/>
        <source>알고리즘</source>
        <translation>Algorithms</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="47"/>
        <source>하드웨어 디코더 사용</source>
        <translation>Use H/W Decoder</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="47"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="48"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="49"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="50"/>
        <source>화면</source>
        <translation>Screen</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="48"/>
        <source>저화질 모드 사용</source>
        <translation>Use Low Quality Mode</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="49"/>
        <source>프레임 드랍 사용</source>
        <translation>Use Frame Drops</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="50"/>
        <source>앨범 자켓 보기</source>
        <translation>View Album Jacket</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="54"/>
        <source>입력 영상 출력 방법</source>
        <translation>Rendering Method of Input Screen</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="54"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="55"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="56"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="57"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="58"/>
        <source>VR 영상 - 공통</source>
        <translation>VR Screen - Common</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="57"/>
        <source>왜곡 보정 모드</source>
        <translation>Distortion Adjustment Mode</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="58"/>
        <source>가상 3D 입체감</source>
        <translation>Virtual 3D depth</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="60"/>
        <source>360도 영상 모드 사용</source>
        <translation>Use 360° Video Mode</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="60"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="61"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="62"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="63"/>
        <source>VR 영상 - 360도</source>
        <translation>VR Screen - 360°</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="51"/>
        <source>전체 해상도 사용</source>
        <translation>Full Size</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="51"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="52"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="53"/>
        <source>3D 영상</source>
        <translation>3D Screen</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="52"/>
        <source>출력 방법</source>
        <translation>Rendering Method</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="53"/>
        <source>애너글리프 알고리즘</source>
        <translation>Anaglyph Algorithm</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="59"/>
        <source>VR 영상 - 평면</source>
        <translation>VR Screen - Normal</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="61"/>
        <source>프로젝션 종류</source>
        <translation>Projection Type</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="62"/>
        <source>헤드 트래킹 사용</source>
        <translation>Use Head Tracking</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="55"/>
        <source>왜곡 보정 사용</source>
        <translation>Use Correcting Distortion</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="59"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="63"/>
        <source>왜곡 보정 계수</source>
        <translation>Distortion Coefficients</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="56"/>
        <source>렌즈 센터 설정</source>
        <translation>Lens Center</translation>
    </message>
</context>
<context>
    <name>SearchMedia</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/SearchMedia.qml" line="71"/>
        <source>정보</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SearchMedia.qml" line="72"/>
        <source>검색 결과가 없습니다.</source>
        <translation>There is no media.</translation>
    </message>
</context>
<context>
    <name>SelectVector2D</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/SelectVector2D.qml" line="131"/>
        <source>첫째</source>
        <translation>First</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SelectVector2D.qml" line="164"/>
        <source>둘째</source>
        <translation>Second</translation>
    </message>
</context>
<context>
    <name>ServerSetting</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/ServerSetting.qml" line="116"/>
        <source>주소</source>
        <translation>Address</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ServerSetting.qml" line="137"/>
        <source>커맨드 포트</source>
        <translation>Command Port</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ServerSetting.qml" line="155"/>
        <source>스트림 포트</source>
        <translation>Stream Port</translation>
    </message>
</context>
<context>
    <name>SettingSlider</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/SettingSlider.qml" line="158"/>
        <source>초기화</source>
        <translation>Reset</translation>
    </message>
</context>
<context>
    <name>Socket</name>
    <message>
        <location filename="../../AnyVODClient/src/net/Socket.cpp" line="294"/>
        <source>접속이 끊겼습니다</source>
        <translation>Connection lost</translation>
    </message>
</context>
<context>
    <name>SortMenuItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/SortMenuItems.qml" line="22"/>
        <source>이름(오름차순)</source>
        <translation>Name(Ascending)</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SortMenuItems.qml" line="27"/>
        <source>변경 시각(오름차순)</source>
        <translation>Time(Ascending)</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SortMenuItems.qml" line="32"/>
        <source>크기(오름차순)</source>
        <translation>Size(Ascending)</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SortMenuItems.qml" line="37"/>
        <source>종류(오름차순)</source>
        <translation>Type(Ascending)</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SortMenuItems.qml" line="46"/>
        <source>이름(내림차순)</source>
        <translation>Name(Descending)</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SortMenuItems.qml" line="51"/>
        <source>변경 시각(내림차순)</source>
        <translation>Time(Descending)</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SortMenuItems.qml" line="56"/>
        <source>크기(내림차순)</source>
        <translation>Size(Descending)</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SortMenuItems.qml" line="61"/>
        <source>종류(내림차순)</source>
        <translation>Type(Descending)</translation>
    </message>
</context>
<context>
    <name>SoundSettingItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="21"/>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="22"/>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="23"/>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="24"/>
        <source>일반</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="21"/>
        <source>음성</source>
        <translation>Audio</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="22"/>
        <source>싱크</source>
        <translation>Synchronization</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="23"/>
        <source>블루투스 싱크</source>
        <translation>Bluetooth Audio Sync</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="24"/>
        <source>오디오 장치</source>
        <translation>Audio Devices</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="25"/>
        <source>노멀라이저 사용</source>
        <translation>Use Normalizer</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="25"/>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="26"/>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="27"/>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="28"/>
        <source>효과</source>
        <translation>Effect</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="26"/>
        <source>음성 줄임</source>
        <translation>Reduce Voice</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="27"/>
        <source>음성 강조</source>
        <translation>Emphasis Voice</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="28"/>
        <source>음악 줄임</source>
        <translation>Reduce Music</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="29"/>
        <source>출력 장치</source>
        <translation>Output Device</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="29"/>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="30"/>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="31"/>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="32"/>
        <source>S/PDIF</source>
        <translation>S/PDIF</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="30"/>
        <source>인코딩</source>
        <translation>Encoding</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="31"/>
        <source>샘플링 속도</source>
        <translation>Sampling Rate</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="32"/>
        <source>사용 하기</source>
        <translation>Use</translation>
    </message>
</context>
<context>
    <name>SubtitleLyricsSettingItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="21"/>
        <source>열기</source>
        <translation>Open</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="21"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="22"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="23"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="24"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="25"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="26"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="27"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="28"/>
        <source>일반</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="22"/>
        <source>외부 닫기</source>
        <translation>Close External Subtitle / Lyrics</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="23"/>
        <source>보이기</source>
        <translation>Show</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="24"/>
        <source>고급 검색</source>
        <translation>Advanced Search</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="25"/>
        <source>투명도</source>
        <translation>Transparency</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="26"/>
        <source>크기</source>
        <translation>Font Size</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="27"/>
        <source>싱크</source>
        <translation>Synchronization</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="28"/>
        <source>자막 캐시 모드 사용</source>
        <translation>Use Subtitle Caching Mode</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="29"/>
        <source>가로 위치</source>
        <translation>Horizontal Position</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="29"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="30"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="31"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="32"/>
        <source>정렬</source>
        <translation>Alignment</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="30"/>
        <source>세로 위치</source>
        <translation>Vertical Position</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="31"/>
        <source>가로 정렬</source>
        <translation>Horizontal Alignment</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="32"/>
        <source>세로 정렬</source>
        <translation>Vertical Alignment</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="33"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="34"/>
        <source>언어</source>
        <translation>Language</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="34"/>
        <source>인코딩</source>
        <translation>Encoding</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="35"/>
        <source>자막 찾기</source>
        <translation>Enable Subtitles Searching</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="35"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="36"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="37"/>
        <source>찾기</source>
        <translation>Search</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="36"/>
        <source>가사 찾기</source>
        <translation>Enable Lyrics Searching</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="37"/>
        <source>찾은 가사 자동 저장</source>
        <translation>Autosave For Found Lyrics</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="38"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="39"/>
        <source>저장</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="39"/>
        <source>다른 이름으로 저장</source>
        <translation>Save as</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="40"/>
        <source>가로 거리</source>
        <translation>Horizontal Distance</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="40"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="41"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="42"/>
        <source>3D 자막</source>
        <translation>3D Subtitle</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="41"/>
        <source>세로 거리</source>
        <translation>Vertical Distance</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="42"/>
        <source>출력 방법</source>
        <translation>Rendering Method</translation>
    </message>
</context>
<context>
    <name>UserAspectRatio</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/UserAspectRatio.qml" line="106"/>
        <source>넓이</source>
        <translation>Width</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/UserAspectRatio.qml" line="138"/>
        <source>높이</source>
        <translation>Height</translation>
    </message>
</context>
<context>
    <name>VideoRenderer</name>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1060"/>
        <source>가사 있음</source>
        <translation>Lyrics exists</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1062"/>
        <source>자막 있음</source>
        <translation>Subtitle exists</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1067"/>
        <source>가사 없음</source>
        <translation>Lyrics does not exist</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1069"/>
        <source>자막 없음</source>
        <translation>Subtitle does not exist</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1075"/>
        <source>파일 이름 : </source>
        <translation>File Name : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1092"/>
        <source>재생 위치 : </source>
        <translation>Position : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1108"/>
        <source>파일 포맷 : </source>
        <translation>File Format : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1115"/>
        <source>CPU 사용률 : </source>
        <translation>CPU Usage : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1127"/>
        <source>메모리 사용량 : </source>
        <translation>Using Memory Size : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1142"/>
        <source>DTV 신호 감도 : </source>
        <translation>DTV Signal Strength : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1156"/>
        <source>비디오 코덱 : </source>
        <translation>Video Codec : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1167"/>
        <source>하드웨어 디코더 : </source>
        <translation>Hardware Video Decoder : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1175"/>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1299"/>
        <source>입력 : </source>
        <translation>Input : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1190"/>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1313"/>
        <source>출력 : </source>
        <translation>Output : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1209"/>
        <source>색상 변환 : </source>
        <translation>Color Conversion : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1222"/>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1233"/>
        <source>프레임 : </source>
        <translation>Frames : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1255"/>
        <source>오디오 코덱 : </source>
        <translation>Audio Codec : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1266"/>
        <source>S/PDIF 오디오 장치 : </source>
        <translation>S/PDIF Audio Device : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1273"/>
        <source>인코딩 사용</source>
        <translation>Use Encoding</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1331"/>
        <source>가사 코덱 : </source>
        <translation>Lyrics Codec : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1333"/>
        <source>자막 코덱 : </source>
        <translation>Subtitle Codec : </translation>
    </message>
</context>
<context>
    <name>VideoScreen</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="191"/>
        <source>자막 찾음</source>
        <translation>Subtitle Found</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="201"/>
        <source>이퀄라이저</source>
        <translation>Equalizer</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="213"/>
        <source>채널 편성표</source>
        <translation>Program Guide</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="224"/>
        <source>원격 서버 로그인</source>
        <translation>Remote Server Sign in</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="546"/>
        <source>지원하지 않는 기능입니다.</source>
        <translation>Not supported feature.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="555"/>
        <source>영상</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="578"/>
        <source>디인터레이스 활성화 기준</source>
        <translation>Deinterlacing Methods</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="582"/>
        <source>자동</source>
        <translation>Auto</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="583"/>
        <source>사용</source>
        <translation>Use</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="584"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="730"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1373"/>
        <source>사용 안 함</source>
        <translation>Don&apos;t Use</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="595"/>
        <source>디인터레이스 알고리즘</source>
        <translation>Deinterlacing Algorithm</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="617"/>
        <source>화면 비율</source>
        <translation>Screen Aspect Ratio</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="639"/>
        <source>화면 비율 사용자 지정</source>
        <translation>Custom Screen Aspect Ratio</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="651"/>
        <source>평면 VR 왜곡 보정 계수 설정</source>
        <translation>VR Distortion Coefficients of Normal</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="652"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="671"/>
        <source>k1</source>
        <translation>k1</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="653"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="672"/>
        <source>k2</source>
        <translation>k2</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="670"/>
        <source>360도 VR 왜곡 보정 계수 설정</source>
        <translation>VR Distortion Coefficients of 360°</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="689"/>
        <source>VR 렌즈 센터 설정</source>
        <translation>VR Lens Center</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="690"/>
        <source>좌/우</source>
        <translation>Horiz</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="691"/>
        <source>상/하</source>
        <translation>Vert</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="708"/>
        <source>프로젝션 종류</source>
        <translation>Projection Type</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="726"/>
        <source>영상 회전 각도</source>
        <translation>Screen Rotation Degree</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="731"/>
        <source>90도</source>
        <translation>90°</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="732"/>
        <source>180도</source>
        <translation>180°</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="733"/>
        <source>270도</source>
        <translation>270°</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="744"/>
        <source>캡처 확장자</source>
        <translation>Extension of Capturing</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="763"/>
        <source>3D 영상 출력 방법</source>
        <translation>Rendering Method of 3D Video</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="786"/>
        <source>3D 영상 애너글리프 알고리즘</source>
        <translation>Anaglyph Algorithm of 3D Video</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="830"/>
        <source>왜곡 보정 모드</source>
        <translation>Distortion adjustment mode</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="852"/>
        <source>재생 순서 설정</source>
        <translation>Playback Order</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="856"/>
        <source>전체 순차 재생</source>
        <translation>Full Sequential Playback</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="857"/>
        <source>전체 반복 재생</source>
        <translation>Full Repeat Playback</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="858"/>
        <source>한 개 재생</source>
        <translation>Play Single</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="859"/>
        <source>한 개 반복 재생</source>
        <translation>Repeat Single</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="860"/>
        <source>무작위 재생</source>
        <translation>Shuffle</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="870"/>
        <source>밝기</source>
        <translation>Brightness</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="876"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="894"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="930"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="951"/>
        <source>배</source>
        <translation> Times</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="888"/>
        <source>채도</source>
        <translation>Saturation</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="906"/>
        <source>색상</source>
        <translation>Hue</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="912"/>
        <source>°</source>
        <translation>°</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="924"/>
        <source>대비</source>
        <translation>Contrast</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="942"/>
        <source>재생 속도 설정</source>
        <translation>Playback Speed</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="963"/>
        <source>투명도</source>
        <translation>Transparency</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="969"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="987"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="981"/>
        <source>크기</source>
        <translation>Font Size</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="999"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1205"/>
        <source>싱크</source>
        <translation>Synchronization</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1006"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1212"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1232"/>
        <source>초</source>
        <translation> Seconds</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1018"/>
        <source>가로 위치</source>
        <translation>Horizontal Position</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1019"/>
        <source>값이 클 수록 오른쪽으로 이동합니다.</source>
        <translation>The larger the value, move to the right.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1036"/>
        <source>세로 위치</source>
        <translation>Vertical Position</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1037"/>
        <source>값이 클 수록 윗쪽으로 이동합니다.</source>
        <translation>The value is greater to go to the top.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1055"/>
        <source>가로 정렬</source>
        <translation>Horizontal Alignment</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1059"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1078"/>
        <source>기본 정렬</source>
        <translation>Default Align</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1060"/>
        <source>자동 정렬</source>
        <translation>Auto Align</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1061"/>
        <source>왼쪽 정렬</source>
        <translation>Left Align</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1062"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1080"/>
        <source>가운데 정렬</source>
        <translation>Middle Align</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1063"/>
        <source>오른쪽 정렬</source>
        <translation>Right Align</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1074"/>
        <source>세로 정렬</source>
        <translation>Vertical Alignment</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1079"/>
        <source>상단 정렬</source>
        <translation>Top Align</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1081"/>
        <source>하단 정렬</source>
        <translation>Bottom Align</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1090"/>
        <source>텍스트 인코딩</source>
        <translation>Text Encoding</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1109"/>
        <source>언어</source>
        <translation>Language</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1129"/>
        <source>세로 거리</source>
        <translation>Vertical Distance</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1147"/>
        <source>가로 거리</source>
        <translation>Horizontal Distance</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1165"/>
        <source>가상 3D 입체감</source>
        <translation>Virtual 3D Depth</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1166"/>
        <source>값이 클 수록 입체감이 강해집니다.</source>
        <translation>The larger the value, stronger the stereoscopic effect.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1184"/>
        <source>3D 자막 출력 방법</source>
        <translation>Rendering Method of 3D Subtitle</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1224"/>
        <source>블루투스 싱크</source>
        <translation>Bluetooth Audio Sync</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1225"/>
        <source>기기에 따라 값이 적용 되지 않을 수 있습니다.</source>
        <translation>Depending on the device, the value may not be applied.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1244"/>
        <source>열기</source>
        <translation>Open</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1263"/>
        <source>다른 이름으로 저장</source>
        <translation>Save as</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1287"/>
        <source>저장 디렉토리 설정</source>
        <translation>Select Save Directory</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1302"/>
        <source>음성</source>
        <translation>Audio</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1323"/>
        <source>오디오 장치</source>
        <translation>Audio Devices</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1330"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1352"/>
        <source>기본 장치</source>
        <translation>Default Device</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1345"/>
        <source>출력 장치</source>
        <translation>Output Device</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1369"/>
        <source>인코딩</source>
        <translation>Encoding</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1374"/>
        <source>AC3</source>
        <translation>AC3</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1375"/>
        <source>DTS</source>
        <translation>DTS</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1386"/>
        <source>샘플링 속도</source>
        <translation>Sampling Rate</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1407"/>
        <source>화면</source>
        <translation>Screen</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1496"/>
        <source>재생</source>
        <translation>Playback</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1527"/>
        <source>자막 / 가사</source>
        <translation>Subtitle / Lyrics</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1607"/>
        <source>소리</source>
        <translation>Sound</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1655"/>
        <source>AnyVOD 정보</source>
        <translation>About AnyVOD</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="2266"/>
        <source>화면 잠김 해제</source>
        <translation>Screen is unlocked</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="545"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1666"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1747"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1758"/>
        <source>정보</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="808"/>
        <source>VR 입력 영상 출력 방법</source>
        <translation>Rendering Method of VR Input Screen</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1130"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1148"/>
        <source>값이 클 수록 가까워집니다. VR 모드일 경우 반대 입니다.</source>
        <translation>Closer to the greater value. If the VR mode, the opposite.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1748"/>
        <source>외부 자막을 닫았습니다.</source>
        <translation>External subtitle was closed.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1759"/>
        <source>자막 / 가사가 저장 되었습니다.</source>
        <translation>Subtitle / Lyrics was saved.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1769"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1780"/>
        <source>오류</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1770"/>
        <source>자막 / 가사가 저장 되지 않았습니다.</source>
        <translation>Subtitle / Lyrics was not saved.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1781"/>
        <source>자막 / 가사를 열지 못했습니다.</source>
        <translation>Can not open subtitle / lyrics.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="2261"/>
        <source>화면 잠김</source>
        <translation>Screen is locked</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="2748"/>
        <source>외부 서버에 자막이 존재합니다.
이동하시려면 여기를 클릭하세요.</source>
        <translation>Subtitle exists on external server.
If you want to download the subtitle,
click this.</translation>
    </message>
</context>
<context>
    <name>VideoScreenMenuItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreenMenuItems.qml" line="23"/>
        <source>이퀄라이저</source>
        <translation>Equalizer</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreenMenuItems.qml" line="29"/>
        <source>화면</source>
        <translation>Screen</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreenMenuItems.qml" line="35"/>
        <source>재생</source>
        <translation>Playback</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreenMenuItems.qml" line="41"/>
        <source>자막 / 가사</source>
        <translation>Subtitle / Lyrics</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreenMenuItems.qml" line="47"/>
        <source>소리</source>
        <translation>Sound</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreenMenuItems.qml" line="53"/>
        <source>정보</source>
        <translation>Info</translation>
    </message>
</context>
<context>
    <name>ViewEPG</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/ViewEPG.qml" line="154"/>
        <source>채널</source>
        <translation>Channel</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ViewEPG.qml" line="162"/>
        <source>제목</source>
        <translation>Title</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ViewEPG.qml" line="167"/>
        <location filename="../qml/AnyVODMobileClient/ViewEPG.qml" line="176"/>
        <location filename="../qml/AnyVODMobileClient/ViewEPG.qml" line="185"/>
        <source>업데이트 중...</source>
        <translation>Now updating...</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ViewEPG.qml" line="171"/>
        <source>방송 시간</source>
        <translation>Period</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ViewEPG.qml" line="180"/>
        <source>현재 시각</source>
        <translation>Current Date Time</translation>
    </message>
</context>
</TS>
