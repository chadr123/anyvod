<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ko_KR">
<context>
    <name>ContextMenuTextInput</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/ContextMenuTextInput.qml" line="102"/>
        <source>잘라내기</source>
        <translation>잘라내기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ContextMenuTextInput.qml" line="117"/>
        <source>복사하기</source>
        <translation>복사하기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ContextMenuTextInput.qml" line="131"/>
        <source>붙여넣기</source>
        <translation>붙여넣기</translation>
    </message>
</context>
<context>
    <name>DTVChannelList</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/DTVChannelList.qml" line="112"/>
        <source>채널 </source>
        <translation>채널 </translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/DTVChannelList.qml" line="112"/>
        <source>, 신호 감도 </source>
        <translation>, 신호 감도 </translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/DTVChannelList.qml" line="116"/>
        <source>이름 없음</source>
        <translation>이름 없음</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/DTVChannelList.qml" line="156"/>
        <source>채널 목록이 없습니다.</source>
        <translation>채널 목록이 없습니다.</translation>
    </message>
</context>
<context>
    <name>DTVSettingItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/DTVSettingItems.qml" line="21"/>
        <source>채널 목록</source>
        <translation>채널 목록</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/DTVSettingItems.qml" line="23"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/DTVSettingItems.qml" line="22"/>
        <source>채널 검색</source>
        <translation>채널 검색</translation>
    </message>
</context>
<context>
    <name>DirItemMenuItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/DirItemMenuItems.qml" line="22"/>
        <source>전체 재생</source>
        <translation>전체 재생</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/DirItemMenuItems.qml" line="27"/>
        <source>삭제</source>
        <translation>삭제</translation>
    </message>
</context>
<context>
    <name>EnumsTranslator</name>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="56"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="150"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="193"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="200"/>
        <source>사용 안 함</source>
        <translation>사용 안 함</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="57"/>
        <source>왼쪽 영상 사용</source>
        <translation>왼쪽 영상 사용</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="58"/>
        <source>오른쪽 영상 사용</source>
        <translation>오른쪽 영상 사용</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="59"/>
        <source>상단 영상 사용</source>
        <translation>상단 영상 사용</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="60"/>
        <source>하단 영상 사용</source>
        <translation>하단 영상 사용</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="61"/>
        <source>좌우 영상 사용</source>
        <translation>좌우 영상 사용</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="62"/>
        <source>상하 영상 사용</source>
        <translation>상하 영상 사용</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="63"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="67"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="71"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="75"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="79"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="83"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="87"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="91"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="95"/>
        <source>왼쪽 영상 우선 사용</source>
        <translation>왼쪽 영상 우선 사용</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="64"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="68"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="72"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="76"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="80"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="84"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="88"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="92"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="96"/>
        <source>오른쪽 영상 우선 사용</source>
        <translation>오른쪽 영상 우선 사용</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="65"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="69"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="73"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="77"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="81"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="85"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="89"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="93"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="97"/>
        <source>상단 영상 우선 사용</source>
        <translation>상단 영상 우선 사용</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="66"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="70"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="74"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="78"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="82"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="86"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="90"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="94"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="98"/>
        <source>하단 영상 우선 사용</source>
        <translation>하단 영상 우선 사용</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="103"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="104"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="105"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="106"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="107"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="108"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="109"/>
        <source>일반</source>
        <translation>일반</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="110"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="111"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="112"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="113"/>
        <source>Page Flipping</source>
        <translation>Page Flipping</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="114"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="115"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="116"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="117"/>
        <source>Row Interlaced</source>
        <translation>Row Interlaced</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="118"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="119"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="120"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="121"/>
        <source>Column Interlaced</source>
        <translation>Column Interlaced</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="122"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="123"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="124"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="125"/>
        <source>Red-Cyan Anaglyph</source>
        <translation>Red-Cyan Anaglyph</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="126"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="127"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="128"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="129"/>
        <source>Green-Magenta Anaglyph</source>
        <translation>Green-Magenta Anaglyph</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="130"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="131"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="132"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="133"/>
        <source>Yellow-Blue Anaglyph</source>
        <translation>Yellow-Blue Anaglyph</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="134"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="135"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="136"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="137"/>
        <source>Red-Blue Anaglyph</source>
        <translation>Red-Blue Anaglyph</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="138"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="139"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="140"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="141"/>
        <source>Red-Green Anaglyph</source>
        <translation>Red-Green Anaglyph</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="142"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="143"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="144"/>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="145"/>
        <source>Checker Board</source>
        <translation>Checker Board</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="151"/>
        <source>상/하</source>
        <translation>상/하</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="152"/>
        <source>좌/우</source>
        <translation>좌/우</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="153"/>
        <source>페이지 플리핑</source>
        <translation>페이지 플리핑</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="154"/>
        <source>인터레이스</source>
        <translation>인터레이스</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="155"/>
        <source>체커 보드</source>
        <translation>체커 보드</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="156"/>
        <source>애너글리프</source>
        <translation>애너글리프</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="182"/>
        <source>확인하지 않음</source>
        <translation>확인하지 않음</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="183"/>
        <source>매 실행 시</source>
        <translation>매 실행 시</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="184"/>
        <source>1일</source>
        <translation>1일</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="185"/>
        <source>1주일</source>
        <translation>1주일</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="186"/>
        <source>한 달</source>
        <translation>한 달</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="187"/>
        <source>반 년</source>
        <translation>반 년</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="188"/>
        <source>1년</source>
        <translation>1년</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="194"/>
        <source>VR 평면 영상</source>
        <translation>VR 평면 영상</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="195"/>
        <source>360도 영상</source>
        <translation>360도 영상</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="201"/>
        <source>좌우 영상(좌측 영상 우선)</source>
        <translation>좌우 영상(좌측 영상 우선)</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="202"/>
        <source>좌우 영상(우측 영상 우선)</source>
        <translation>좌우 영상(우측 영상 우선)</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="203"/>
        <source>상하 영상(상단 영상 우선)</source>
        <translation>상하 영상(상단 영상 우선)</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="204"/>
        <source>상하 영상(하단 영상 우선)</source>
        <translation>상하 영상(하단 영상 우선)</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="205"/>
        <source>복제</source>
        <translation>복제</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/EnumsTranslator.cpp" line="206"/>
        <source>가상 3D</source>
        <translation>가상 3D</translation>
    </message>
</context>
<context>
    <name>Equalizer</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/Equalizer.qml" line="61"/>
        <location filename="../qml/AnyVODMobileClient/Equalizer.qml" line="135"/>
        <source>프리셋</source>
        <translation>프리셋</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Equalizer.qml" line="167"/>
        <source>초기화</source>
        <translation>초기화</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Equalizer.qml" line="191"/>
        <source>이퀄라이저 사용</source>
        <translation>이퀄라이저 사용</translation>
    </message>
</context>
<context>
    <name>EqualizerDelegate</name>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="173"/>
        <source>기본 값</source>
        <translation>기본 값</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="174"/>
        <source>클래식</source>
        <translation>클래식</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="175"/>
        <source>베이스</source>
        <translation>베이스</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="176"/>
        <source>베이스 &amp; 트레블</source>
        <translation>베이스 &amp; 트레블</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="177"/>
        <source>트레블</source>
        <translation>트레블</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="178"/>
        <source>헤드폰</source>
        <translation>헤드폰</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="179"/>
        <source>홀</source>
        <translation>홀</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="180"/>
        <source>소프트 락</source>
        <translation>소프트 락</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="181"/>
        <source>클럽</source>
        <translation>클럽</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="182"/>
        <source>댄스</source>
        <translation>댄스</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="183"/>
        <source>라이브</source>
        <translation>라이브</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="184"/>
        <source>파티</source>
        <translation>파티</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="185"/>
        <source>팝</source>
        <translation>팝</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="186"/>
        <source>레게</source>
        <translation>레게</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="187"/>
        <source>락</source>
        <translation>락</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="188"/>
        <source>스카</source>
        <translation>스카</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="189"/>
        <source>소프트</source>
        <translation>소프트</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="190"/>
        <source>테크노</source>
        <translation>테크노</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="191"/>
        <source>보컬</source>
        <translation>보컬</translation>
    </message>
    <message>
        <location filename="../src/ui/delegates/EqualizerDelegate.cpp" line="192"/>
        <source>재즈</source>
        <translation>재즈</translation>
    </message>
</context>
<context>
    <name>FileItemMenuItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/FileItemMenuItems.qml" line="22"/>
        <source>장면 탐색</source>
        <translation>장면 탐색</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/FileItemMenuItems.qml" line="27"/>
        <source>삭제</source>
        <translation>삭제</translation>
    </message>
</context>
<context>
    <name>FileSystemDialog</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/FileSystemDialog.qml" line="294"/>
        <source>내장 메모리</source>
        <translation>내장 메모리</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/FileSystemDialog.qml" line="331"/>
        <source>외장 메모리</source>
        <translation>외장 메모리</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/FileSystemDialog.qml" line="371"/>
        <source>파일 이름 :</source>
        <translation>파일 이름 :</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/FileSystemDialog.qml" line="472"/>
        <source>%1 항목</source>
        <translation>%1 항목</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/FileSystemDialog.qml" line="472"/>
        <source>갱신 중...</source>
        <translation>갱신 중...</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/FileSystemDialog.qml" line="541"/>
        <source>파일 또는 디렉토리가 없습니다</source>
        <translation>파일 또는 디렉토리가 없습니다</translation>
    </message>
</context>
<context>
    <name>FooterWithButtons</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/FooterWithButtons.qml" line="55"/>
        <source>열기</source>
        <translation>열기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/FooterWithButtons.qml" line="69"/>
        <source>확인</source>
        <translation>확인</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/FooterWithButtons.qml" line="84"/>
        <source>닫기</source>
        <translation>닫기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/FooterWithButtons.qml" line="98"/>
        <source>저장</source>
        <translation>저장</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/FooterWithButtons.qml" line="112"/>
        <source>신규 저장</source>
        <translation>신규 저장</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/FooterWithButtons.qml" line="126"/>
        <source>취소</source>
        <translation>취소</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/FooterWithButtons.qml" line="140"/>
        <source>갱신</source>
        <translation>갱신</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/FooterWithButtons.qml" line="154"/>
        <source>전원 끄기</source>
        <translation>전원 끄기</translation>
    </message>
</context>
<context>
    <name>GLRenderer</name>
    <message>
        <location filename="../src/video/GLRenderer.cpp" line="352"/>
        <source>캡쳐 성공</source>
        <translation>캡쳐 성공</translation>
    </message>
    <message>
        <location filename="../src/video/GLRenderer.cpp" line="354"/>
        <source>캡쳐 실패</source>
        <translation>캡쳐 실패</translation>
    </message>
    <message>
        <location filename="../src/video/GLRenderer.cpp" line="479"/>
        <source>캡쳐를 시작하지 못했습니다</source>
        <translation>캡쳐를 시작하지 못했습니다</translation>
    </message>
</context>
<context>
    <name>InfoSettingItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/InfoSettingItems.qml" line="21"/>
        <source>재생 정보 보기</source>
        <translation>재생 정보 보기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/InfoSettingItems.qml" line="23"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/InfoSettingItems.qml" line="22"/>
        <source>AnyVOD 정보</source>
        <translation>AnyVOD 정보</translation>
    </message>
</context>
<context>
    <name>Initial</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="39"/>
        <source>캡처 확장자</source>
        <translation>캡처 확장자</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="58"/>
        <source>글꼴</source>
        <translation>글꼴</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="85"/>
        <source>디인터레이스 활성화 기준</source>
        <translation>디인터레이스 활성화 기준</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="89"/>
        <source>자동</source>
        <translation>자동</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="90"/>
        <source>사용</source>
        <translation>사용</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="91"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="254"/>
        <source>사용 안 함</source>
        <translation>사용 안 함</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="102"/>
        <source>디인터레이스 알고리즘</source>
        <translation>디인터레이스 알고리즘</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="106"/>
        <source>Blend</source>
        <translation>Blend</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="107"/>
        <source>BOB</source>
        <translation>BOB</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="108"/>
        <source>YADIF</source>
        <translation>YADIF</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="109"/>
        <source>YADIF(BOB)</source>
        <translation>YADIF(BOB)</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="110"/>
        <source>Weston 3 Field</source>
        <translation>Weston 3 Field</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="111"/>
        <source>Adaptive Kernel</source>
        <translation>Adaptive Kernel</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="112"/>
        <source>Motion Compensation</source>
        <translation>Motion Compensation</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="113"/>
        <source>Bob Weaver</source>
        <translation>Bob Weaver</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="114"/>
        <source>Bob Weaver(BOB)</source>
        <translation>Bob Weaver(BOB)</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="125"/>
        <source>자막 / 가사 가로 정렬</source>
        <translation>자막 / 가사 가로 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="129"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="148"/>
        <source>기본 정렬</source>
        <translation>기본 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="130"/>
        <source>자동 정렬</source>
        <translation>자동 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="131"/>
        <source>왼쪽 정렬</source>
        <translation>왼쪽 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="132"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="150"/>
        <source>가운데 정렬</source>
        <translation>가운데 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="133"/>
        <source>오른쪽 정렬</source>
        <translation>오른쪽 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="144"/>
        <source>자막 / 가사 세로 정렬</source>
        <translation>자막 / 가사 세로 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="149"/>
        <source>상단 정렬</source>
        <translation>상단 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="151"/>
        <source>하단 정렬</source>
        <translation>하단 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="160"/>
        <source>자막 / 가사 텍스트 인코딩</source>
        <translation>자막 / 가사 텍스트 인코딩</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="179"/>
        <source>Languages</source>
        <translation>언어</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="204"/>
        <source>오디오 장치</source>
        <translation>오디오 장치</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="211"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="233"/>
        <source>기본 장치</source>
        <translation>기본 장치</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="226"/>
        <source>출력 장치</source>
        <translation>출력 장치</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="250"/>
        <source>인코딩</source>
        <translation>인코딩</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="255"/>
        <source>AC3</source>
        <translation>AC3</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="256"/>
        <source>DTS</source>
        <translation>DTS</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="267"/>
        <source>샘플링 속도</source>
        <translation>샘플링 속도</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="290"/>
        <source>언어</source>
        <translation>언어</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="362"/>
        <source>앱 자동 업데이트 확인 주기</source>
        <translation>앱 자동 업데이트 확인 주기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="366"/>
        <source>확인하지 않음</source>
        <translation>확인하지 않음</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="367"/>
        <source>매 실행 시</source>
        <translation>매 실행 시</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="368"/>
        <source>1일</source>
        <translation>1일</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="369"/>
        <source>1주일</source>
        <translation>1주일</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="370"/>
        <source>한 달</source>
        <translation>한 달</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="371"/>
        <source>반 년</source>
        <translation>반 년</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="372"/>
        <source>1년</source>
        <translation>1년</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="385"/>
        <source>이퀄라이저</source>
        <translation>이퀄라이저</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="394"/>
        <source>외부 열기</source>
        <translation>외부 열기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="408"/>
        <source>프록시 설정</source>
        <translation>프록시 설정</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="423"/>
        <source>서버 설정</source>
        <translation>서버 세팅</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="441"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="503"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="563"/>
        <source>정보</source>
        <translation>정보</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="442"/>
        <source>지원하지 않는 기능입니다.</source>
        <translation>지원하지 않는 기능입니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="452"/>
        <source>파일을 열 수 없습니다.</source>
        <translation>파일을 열 수 없습니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="523"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="573"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="597"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="621"/>
        <source>질문</source>
        <translation>질문</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="451"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="461"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="471"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="482"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="493"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="543"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="553"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="636"/>
        <source>오류</source>
        <translation>오류</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="462"/>
        <source>파일을 삭제 할 수 없습니다.</source>
        <translation>파일을 삭제 할 수 없습니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="472"/>
        <source>디렉토리를 삭제 할 수 없습니다.</source>
        <translation>디렉토리를 삭제 할 수 없습니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="483"/>
        <source>외장 메모리의 파일을 삭제 할 수 없습니다.
이는 안드로이드 정책에 의한 것일 수 있습니다.</source>
        <translation>외장 메모리의 파일을 삭제 할 수 없습니다.
이는 안드로이드 정책에 의한 것일 수 있습니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="494"/>
        <source>외장 메모리의 디렉토리를 삭제 할 수 없습니다.
이는 안드로이드 정책에 의한 것일 수 있습니다.</source>
        <translation>외장 메모리의 디렉토리를 삭제 할 수 없습니다.
이는 안드로이드 정책에 의한 것일 수 있습니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="504"/>
        <source>변경된 글꼴로 적용하기 위해서는 앱을 재 시작 해주세요.</source>
        <translation>변경된 글꼴로 적용하기 위해서는 앱을 재 시작 해주세요.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="513"/>
        <source>Info</source>
        <translation>정보</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="514"/>
        <source>Restart the app to apply changes.</source>
        <translation>변경 사항을 적용하기 위해서 앱을 재 시작 해주세요.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="524"/>
        <source>종료 하시겠습니까?</source>
        <translation>종료 하시겠습니까?</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="544"/>
        <source>설정을 불러 오지 못했습니다.</source>
        <translation>설정을 불러 오지 못했습니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="554"/>
        <source>설정을 저장하지 못했습니다.</source>
        <translation>설정을 저장하지 못했습니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="564"/>
        <source>설정을 불러왔습니다. 적용하려면 앱를 재 시작 해주세요.</source>
        <translation>설정을 불러왔습니다. 적용하려면 앱를 재 시작 해주세요.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="574"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="598"/>
        <source>삭제 하시겠습니까?</source>
        <translation>삭제 하시겠습니까?</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="622"/>
        <source>로그아웃 하시겠습니까?</source>
        <translation>로그아웃 하시겠습니까?</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="637"/>
        <source>로그인을 해야 합니다.</source>
        <translation>로그인을 해야 합니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="645"/>
        <source>AnyVOD 정보</source>
        <translation>AnyVOD 정보</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="656"/>
        <source>라이센스</source>
        <translation>라이센스</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="667"/>
        <source>저장 디렉토리 설정</source>
        <translation>저장 디렉토리 설정</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="682"/>
        <source>설정 불러오기</source>
        <translation>설정 불러오기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="704"/>
        <source>설정 저장하기</source>
        <translation>설정 저장하기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="726"/>
        <source>블루투스 싱크</source>
        <translation>블루투스 싱크</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="727"/>
        <source>기기에 따라 값이 적용 되지 않을 수 있습니다.</source>
        <translation>기기에 따라 값이 적용 되지 않을 수 있습니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="732"/>
        <source>초</source>
        <translation>초</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="745"/>
        <source>재생 순서 설정</source>
        <translation>재생 순서 설정</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="749"/>
        <source>전체 순차 재생</source>
        <translation>전체 순차 재생</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="750"/>
        <source>전체 반복 재생</source>
        <translation>전체 반복 재생</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="751"/>
        <source>한 개 재생</source>
        <translation>한 개 재생</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="752"/>
        <source>한 개 반복 재생</source>
        <translation>한 개 반복 재생</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="753"/>
        <source>무작위 재생</source>
        <translation>무작위 재생</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="762"/>
        <source>설정</source>
        <translation>설정</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="858"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="909"/>
        <source>채널 검색</source>
        <translation>채널 검색</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="866"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="917"/>
        <source>채널 목록</source>
        <translation>채널 목록</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="882"/>
        <source>DTV 열기</source>
        <translation>DTV 열기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="933"/>
        <source>라디오 열기</source>
        <translation>라디오 열기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="960"/>
        <source>원격 서버</source>
        <translation>원격 서버</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="1008"/>
        <source>재생 목록</source>
        <translation>재생 목록</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="1020"/>
        <source>검색</source>
        <translation>검색</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="1033"/>
        <source>원격 파일목록</source>
        <translation>원격 파일목록</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="1048"/>
        <source>원격 서버 로그인</source>
        <translation>원격 서버 로그인</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="1061"/>
        <source>장면 탐색</source>
        <translation>장면 탐색</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="1080"/>
        <source>업데이트 다운로드</source>
        <translation>업데이트 다운로드</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="1082"/>
        <source>AnyVOD 업데이트가 존재합니다.
업데이트를 다운로드 하려면 여기를 클릭하세요.</source>
        <translation>AnyVOD 업데이트가 존재합니다.
업데이트를 다운로드 하려면 여기를 클릭하세요.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="1545"/>
        <source>내장메모리</source>
        <translation>내장메모리</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="1581"/>
        <source>외장메모리</source>
        <translation>외장메모리</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="1611"/>
        <source>동영상</source>
        <translation>동영상</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="1647"/>
        <source>음악</source>
        <translation>음악</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="1769"/>
        <source>%1 항목</source>
        <translation>%1 항목</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="1769"/>
        <source>갱신 중...</source>
        <translation>갱신 중...</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="1891"/>
        <source>파일이 없습니다</source>
        <translation>파일이 없습니다</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="2650"/>
        <source>뒤로 버튼을 한 번 더 누르시면 종료합니다.</source>
        <translation>뒤로 버튼을 한 번 더 누르시면 종료합니다.</translation>
    </message>
</context>
<context>
    <name>Login</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/Login.qml" line="84"/>
        <location filename="../qml/AnyVODMobileClient/Login.qml" line="94"/>
        <location filename="../qml/AnyVODMobileClient/Login.qml" line="104"/>
        <source>오류</source>
        <translation>오류</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Login.qml" line="85"/>
        <source>아이디를 입력해 주세요.</source>
        <translation>아이디를 입력해 주세요.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Login.qml" line="95"/>
        <source>비밀번호를 입력해 주세요.</source>
        <translation>비밀번호를 입력해 주세요.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Login.qml" line="105"/>
        <source>서버에 로그인 할 수 없습니다.
서버 주소, 아이디, 비밀번호가 올바른지 확인해 주세요.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Login.qml" line="177"/>
        <source>아이디와 비밀번호를 입력해주세요</source>
        <translation>아이디와 비밀번호를 입력해주세요</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Login.qml" line="192"/>
        <source>아이디</source>
        <translation>아이디</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Login.qml" line="214"/>
        <source>비밀번호</source>
        <translation>비밀번호</translation>
    </message>
</context>
<context>
    <name>MainMenuItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainMenuItems.qml" line="23"/>
        <source>검색</source>
        <translation>검색</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainMenuItems.qml" line="29"/>
        <source>재생 목록</source>
        <translation>재생 목록</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainMenuItems.qml" line="41"/>
        <source>원격 서버</source>
        <translation>원격 서버</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainMenuItems.qml" line="53"/>
        <source>라디오 열기</source>
        <translation>라디오 열기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainMenuItems.qml" line="59"/>
        <source>이퀄라이저</source>
        <translation>이퀄라이저</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainMenuItems.qml" line="35"/>
        <source>외부 열기</source>
        <translation>외부 열기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainMenuItems.qml" line="47"/>
        <source>DTV 열기</source>
        <translation>DTV 열기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainMenuItems.qml" line="65"/>
        <source>설정</source>
        <translation>설정</translation>
    </message>
</context>
<context>
    <name>MainSettingItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="21"/>
        <source>AnyVOD 정보</source>
        <translation>AnyVOD 정보</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="21"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="22"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="23"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="24"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="25"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="26"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="27"/>
        <source>일반</source>
        <translation>일반</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="22"/>
        <source>라이센스</source>
        <translation>라이센스</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="23"/>
        <source>Languages</source>
        <translation>언어</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="24"/>
        <source>글꼴</source>
        <translation>글꼴</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="25"/>
        <source>앱 자동 업데이트 확인 주기</source>
        <translation>앱 자동 업데이트 확인 주기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="26"/>
        <source>프록시 설정</source>
        <translation>프록시 설정</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="28"/>
        <source>설정 불러오기</source>
        <translation>설정 불러오기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="28"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="29"/>
        <source>설정 관리</source>
        <translation>설정 관리</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="29"/>
        <source>설정 저장하기</source>
        <translation>설정 저장하기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="30"/>
        <source>하드웨어 디코더 사용</source>
        <translation>하드웨어 디코더 사용</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="30"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="31"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="32"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="33"/>
        <source>화면</source>
        <translation>화면</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="31"/>
        <source>저화질 모드 사용</source>
        <translation>저화질 모드 사용</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="32"/>
        <source>프레임 드랍 사용</source>
        <translation>프레임 드랍 사용</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="33"/>
        <source>앨범 자켓 보기</source>
        <translation>앨범 자켓 보기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="34"/>
        <source>확장자</source>
        <translation>확장자</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="34"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="35"/>
        <source>캡처</source>
        <translation>캡처</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="35"/>
        <source>저장 디렉토리 설정</source>
        <translation>저장 디렉토리 설정</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="36"/>
        <source>활성화 기준</source>
        <translation>활성화 기준</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="36"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="37"/>
        <source>디인터레이스</source>
        <translation>디인터레이스</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="37"/>
        <source>알고리즘</source>
        <translation>알고리즘</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="38"/>
        <source>재생 위치 기억</source>
        <translation>재생 위치 기억</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="38"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="39"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="40"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="41"/>
        <source>재생</source>
        <translation>재생</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="39"/>
        <source>키프레임 단위 이동</source>
        <translation>키프레임 단위 이동</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="40"/>
        <source>버퍼링 모드 사용</source>
        <translation>버퍼링 모드 사용</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="41"/>
        <source>재생 순서 설정</source>
        <translation>재생 순서 설정</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="42"/>
        <source>보이기</source>
        <translation>보이기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="42"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="43"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="44"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="45"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="46"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="47"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="48"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="49"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="50"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="51"/>
        <source>자막 / 가사</source>
        <translation>자막 / 가사</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="43"/>
        <source>고급 검색</source>
        <translation>고급 검색</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="44"/>
        <source>자막 캐시 모드 사용</source>
        <translation>자막 캐시 모드 사용</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="45"/>
        <source>가로 정렬</source>
        <translation>가로 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="46"/>
        <source>세로 정렬</source>
        <translation>세로 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="47"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="57"/>
        <source>인코딩</source>
        <translation>인코딩</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="48"/>
        <source>자막 찾기</source>
        <translation>자막 찾기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="49"/>
        <source>가사 찾기</source>
        <translation>가사 찾기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="50"/>
        <source>찾은 가사 자동 저장</source>
        <translation>찾은 가사 자동 저장</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="51"/>
        <source>미디어 삭제 시 자막/가사 삭제</source>
        <translation>미디어 삭제 시 자막/가사 삭제</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="52"/>
        <source>오디오 장치</source>
        <translation>오디오 장치</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="53"/>
        <source>노멀라이저 사용</source>
        <translation>노멀라이저 사용</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="55"/>
        <source>블루투스 싱크</source>
        <translation>블루투스 싱크</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="56"/>
        <source>출력 장치</source>
        <translation>출력 장치</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="56"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="57"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="58"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="59"/>
        <source>S/PDIF</source>
        <translation>S/PDIF</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="58"/>
        <source>샘플링 속도</source>
        <translation>샘플링 속도</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="59"/>
        <source>사용 하기</source>
        <translation>사용 하기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="60"/>
        <source>언어</source>
        <translation>언어</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="60"/>
        <source>키보드</source>
        <translation>키보드</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="52"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="53"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="54"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="55"/>
        <source>소리</source>
        <translation>소리</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="27"/>
        <source>원격 서버 설정</source>
        <translation>원격 서버 설정</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="54"/>
        <source>이퀄라이저 사용</source>
        <translation>이퀄라이저 사용</translation>
    </message>
</context>
<context>
    <name>ManagePlayList</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/ManagePlayList.qml" line="75"/>
        <source>질문</source>
        <translation>질문</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ManagePlayList.qml" line="76"/>
        <source>삭제 하시겠습니까?</source>
        <translation>삭제 하시겠습니까?</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ManagePlayList.qml" line="90"/>
        <source>삭제</source>
        <translation>삭제</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ManagePlayList.qml" line="251"/>
        <source>재생 목록이 없습니다.</source>
        <translation>재생 목록이 없습니다.</translation>
    </message>
</context>
<context>
    <name>MediaPresenter</name>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="2294"/>
        <source>프레임 저하가 일어나고 있습니다. 성능에 영향을 미치는 옵션 또는 수직 동기화를 꺼주세요.</source>
        <translation>프레임 저하가 일어나고 있습니다. 성능에 영향을 미치는 옵션 또는 수직 동기화를 꺼주세요.</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="3002"/>
        <source>S/PDIF 출력 초기화를 실패 하였습니다. PCM 출력으로 전환합니다.</source>
        <translation>S/PDIF 출력 초기화를 실패 하였습니다. PCM 출력으로 전환합니다.</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="4192"/>
        <source>오프닝 스킵 : %1</source>
        <translation>오프닝 스킵 : %1</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="4213"/>
        <source>재생 스킵 : %1 ~ %2</source>
        <translation>재생 스킵 : %1 ~ %2</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="4331"/>
        <source>구간 반복 : %1 ~ %2</source>
        <translation>구간 반복 : %1 ~ %2</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="4339"/>
        <source>S/PDIF 출력을 지원하지 않은 포맷이므로 PCM 출력으로 전환합니다.</source>
        <translation>S/PDIF 출력을 지원하지 않은 포맷이므로 PCM 출력으로 전환합니다.</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="4345"/>
        <source>하드웨어 디코딩을 지원하지 않는 코덱이므로 하드웨어 디코딩을 끕니다.</source>
        <translation>하드웨어 디코딩을 지원하지 않는 코덱이므로 하드웨어 디코딩을 끕니다.</translation>
    </message>
</context>
<context>
    <name>OpenExternal</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/OpenExternal.qml" line="110"/>
        <source>외부 주소를 입력 하세요.
예) http://somehost.com/movie.mp4</source>
        <translation>외부 주소를 입력 하세요.
예) http://somehost.com/movie.mp4</translation>
    </message>
</context>
<context>
    <name>PlayList</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="86"/>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="96"/>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="153"/>
        <source>정보</source>
        <translation>정보</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="87"/>
        <source>지원하지 않는 기능입니다.</source>
        <translation>지원하지 않는 기능입니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="97"/>
        <source>클립보드로 경로가 복사 되었습니다.</source>
        <translation>클립보드로 경로가 복사 되었습니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="116"/>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="137"/>
        <source>질문</source>
        <translation>질문</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="106"/>
        <source>오류</source>
        <translation>오류</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="107"/>
        <source>이름을 입력 해 주세요.</source>
        <translation>이름을 입력 해 주세요.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="117"/>
        <source>갱신 하시겠습니까?</source>
        <translation>갱신 하시겠습니까?</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="138"/>
        <source>이미 동일한 재생 목록이 존재합니다.
갱신 하시겠습니까?</source>
        <translation>이미 동일한 재생 목록이 존재합니다.
갱신 하시겠습니까?</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="165"/>
        <source>화질 목록</source>
        <translation>화질 목록</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="192"/>
        <source>재생 목록 신규 저장</source>
        <translation>재생 목록 신규 저장</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="225"/>
        <source>장면 탐색</source>
        <translation>장면 탐색</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="314"/>
        <source>재생 목록</source>
        <translation>재생 목록</translation>
    </message>
</context>
<context>
    <name>PlayListMenuItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayListMenuItems.qml" line="22"/>
        <source>경로 보기</source>
        <translation>경로 보기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayListMenuItems.qml" line="27"/>
        <source>경로 복사</source>
        <translation>경로 복사</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayListMenuItems.qml" line="32"/>
        <source>장면 탐색</source>
        <translation>장면 탐색</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayListMenuItems.qml" line="37"/>
        <source>다른 화질</source>
        <translation>다른 화질</translation>
    </message>
</context>
<context>
    <name>PlaySettingItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlaySettingItems.qml" line="21"/>
        <source>재생 위치 기억</source>
        <translation>재생 위치 기억</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlaySettingItems.qml" line="21"/>
        <location filename="../qml/AnyVODMobileClient/PlaySettingItems.qml" line="22"/>
        <location filename="../qml/AnyVODMobileClient/PlaySettingItems.qml" line="23"/>
        <location filename="../qml/AnyVODMobileClient/PlaySettingItems.qml" line="24"/>
        <location filename="../qml/AnyVODMobileClient/PlaySettingItems.qml" line="25"/>
        <location filename="../qml/AnyVODMobileClient/PlaySettingItems.qml" line="26"/>
        <source>재생</source>
        <translation>재생</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlaySettingItems.qml" line="22"/>
        <source>키프레임 단위 이동</source>
        <translation>키프레임 단위 이동</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlaySettingItems.qml" line="23"/>
        <source>버퍼링 모드 사용</source>
        <translation>버퍼링 모드 사용</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlaySettingItems.qml" line="24"/>
        <source>재생 속도 설정</source>
        <translation>재생 속도 설정</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlaySettingItems.qml" line="25"/>
        <source>재생 순서 설정</source>
        <translation>재생 순서 설정</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlaySettingItems.qml" line="26"/>
        <source>구간 반복 설정</source>
        <translation>구간 반복 설정</translation>
    </message>
</context>
<context>
    <name>ProxyInfo</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/ProxyInfo.qml" line="110"/>
        <source>id:password@ip:port 형태로 입력하세요.
예시)
    guest:mypass@192.168.0.10:8080
    192.168.0.10:8080</source>
        <translation>id:password@ip:port 형태로 입력하세요.
예시)
    guest:mypass@192.168.0.10:8080
    192.168.0.10:8080</translation>
    </message>
</context>
<context>
    <name>RadioChannelList</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/RadioChannelList.qml" line="112"/>
        <source>채널 </source>
        <translation>채널 </translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/RadioChannelList.qml" line="112"/>
        <source>, 신호 감도 </source>
        <translation>, 신호 감도 </translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/RadioChannelList.qml" line="156"/>
        <source>채널 목록이 없습니다.</source>
        <translation>채널 목록이 없습니다.</translation>
    </message>
</context>
<context>
    <name>RadioSettingItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/RadioSettingItems.qml" line="21"/>
        <source>채널 목록</source>
        <translation>채널 목록</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/RadioSettingItems.qml" line="21"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/RadioSettingItems.qml" line="22"/>
        <source>채널 검색</source>
        <translation>채널 검색</translation>
    </message>
</context>
<context>
    <name>RemoteDirItemMenuItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/RemoteDirItemMenuItems.qml" line="22"/>
        <source>전체 재생</source>
        <translation>전체 재생</translation>
    </message>
</context>
<context>
    <name>RemoteFileListDialog</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/RemoteFileListDialog.qml" line="262"/>
        <source>갱신 중...</source>
        <translation>갱신 중...</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/RemoteFileListDialog.qml" line="354"/>
        <source>파일 또는 디렉토리가 없습니다</source>
        <translation>파일 또는 디렉토리가 없습니다</translation>
    </message>
</context>
<context>
    <name>RemoteServerSettingItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/RemoteServerSettingItems.qml" line="21"/>
        <source>로그인 / 로그아웃</source>
        <translation>로그인 / 로그아웃</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/RemoteServerSettingItems.qml" line="21"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/RemoteServerSettingItems.qml" line="22"/>
        <source>원격 파일 목록 열기</source>
        <translation>원격 파일 목록 열기</translation>
    </message>
</context>
<context>
    <name>RenderScreen</name>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="339"/>
        <source>자막 있음</source>
        <translation>자막 있음</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="341"/>
        <source>자막 없음</source>
        <translation>자막 없음</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="346"/>
        <source>가사 있음</source>
        <translation>가사 있음</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="348"/>
        <source>가사 없음</source>
        <translation>가사 없음</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="379"/>
        <source>일시정지</source>
        <translation>일시정지</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="396"/>
        <source>재생</source>
        <translation>재생</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="464"/>
        <source>앞으로 %1초 키프레임 이동</source>
        <translation>앞으로 %1초 키프레임 이동</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="466"/>
        <source>뒤로 %1초 키프레임 이동</source>
        <translation>뒤로 %1초 키프레임 이동</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="471"/>
        <source>앞으로 %1초 이동</source>
        <translation>앞으로 %1초 이동</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="473"/>
        <source>뒤로 %1초 이동</source>
        <translation>뒤로 %1초 이동</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="939"/>
        <source>자막 열기 : %1</source>
        <translation>자막 열기 : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="941"/>
        <source>가사 열기 : %1</source>
        <translation>가사 열기 : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="959"/>
        <source>자막이 저장 되었습니다 : %1</source>
        <translation>자막이 저장 되었습니다 : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="961"/>
        <source>가사가 저장 되었습니다 : %1</source>
        <translation>가사가 저장 되었습니다 : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="966"/>
        <source>자막이 저장 되지 않았습니다 : %1</source>
        <translation>자막이 저장 되지 않았습니다 : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="968"/>
        <source>가사가 저장 되지 않았습니다 : %1</source>
        <translation>가사가 저장 되지 않았습니다 : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="986"/>
        <source>외부 자막 닫기</source>
        <translation>외부 자막 닫기</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="988"/>
        <source>외부 가사 닫기</source>
        <translation>외부 가사 닫기</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1246"/>
        <source>자막 보이기</source>
        <translation>자막 보이기</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1248"/>
        <source>자막 숨기기</source>
        <translation>자막 숨기기</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1253"/>
        <source>가사 보이기</source>
        <translation>가사 보이기</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1255"/>
        <source>가사 숨기기</source>
        <translation>가사 숨기기</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1275"/>
        <source>고급 자막 검색 사용</source>
        <translation>고급 자막 검색 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1277"/>
        <source>고급 자막 검색 사용 안 함</source>
        <translation>고급 자막 검색 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1282"/>
        <source>고급 가사 검색 사용</source>
        <translation>고급 가사 검색 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1284"/>
        <source>고급 가사 검색 사용 안 함</source>
        <translation>고급 가사 검색 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1341"/>
        <source>자막 언어 변경</source>
        <translation>자막 언어 변경</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1343"/>
        <source>가사 언어 변경</source>
        <translation>가사 언어 변경</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1360"/>
        <source>세로 자막 위치 : %1</source>
        <translation>세로 자막 위치 : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1371"/>
        <source>가로 자막 위치 : %1</source>
        <translation>가로 자막 위치 : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1080"/>
        <source>블루투스 싱크 %1초 빠르게</source>
        <translation>블루투스 싱크 %1초 빠르게</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1082"/>
        <source>블루투스 싱크 %1초 느리게</source>
        <translation>블루투스 싱크 %1초 느리게</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1392"/>
        <source>세로 3D 자막 위치 : %1</source>
        <translation>세로 3D 자막 위치 : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1403"/>
        <source>가로 3D 자막 위치 : %1</source>
        <translation>가로 3D 자막 위치 : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1424"/>
        <source>가상 3D 입체감 : %1</source>
        <translation>가상 3D 입체감 : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1446"/>
        <source>구간 반복 시작 : %1</source>
        <translation>구간 반복 시작 : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1459"/>
        <source>구간 반복 끝 : %1</source>
        <translation>구간 반복 끝 : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1475"/>
        <source>구간 반복 활성화</source>
        <translation>구간 반복 활성화</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1477"/>
        <source>구간 반복 비활성화</source>
        <translation>구간 반복 비활성화</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1483"/>
        <source>시작과 끝 시각이 같으므로 활성화 되지 않습니다</source>
        <translation>시작과 끝 시각이 같으므로 활성화 되지 않습니다</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1511"/>
        <source>키프레임 단위로 이동 함</source>
        <translation>키프레임 단위로 이동 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1513"/>
        <source>키프레임 단위로 이동 안 함</source>
        <translation>키프레임 단위로 이동 안 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1548"/>
        <source>3D 영상 (%1)</source>
        <translation>3D 영상 (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1550"/>
        <source>3D 영상 (%1 (%2))</source>
        <translation>3D 영상 (%1 (%2))</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1565"/>
        <source>3D 자막 (%1)</source>
        <translation>3D 자막 (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1578"/>
        <source>VR 입력 영상 (%1)</source>
        <translation>VR 입력 영상 (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1595"/>
        <source>헤드 트래킹 사용 함</source>
        <translation>헤드 트래킹 사용 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1597"/>
        <source>헤드 트래킹 사용 안 함</source>
        <translation>헤드 트래킹 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1616"/>
        <source>VR 왜곡 보정 사용 함</source>
        <translation>VR 왜곡 보정 사용 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1618"/>
        <source>VR 왜곡 보정 사용 안 함</source>
        <translation>VR 왜곡 보정 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1630"/>
        <source>평면 VR 왜곡 보정 계수 설정 (k1 : %1, k2 : %2)</source>
        <translation>평면 VR 왜곡 보정 계수 설정 (k1 : %1, k2 : %2)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1646"/>
        <source>360도 VR 왜곡 보정 계수 설정 (k1 : %1, k2 : %2)</source>
        <translation>360도 VR 왜곡 보정 계수 설정 (k1 : %1, k2 : %2)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1662"/>
        <source>VR 렌즈 센터 설정 (X : %1, Y : %2)</source>
        <translation>VR 렌즈 센터 설정 (X : %1, Y : %2)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1705"/>
        <source>노멀라이저 켜짐</source>
        <translation>노멀라이저 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1707"/>
        <source>노멀라이저 꺼짐</source>
        <translation>노멀라이저 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1724"/>
        <source>이퀄라이저 켜짐</source>
        <translation>이퀄라이저 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1726"/>
        <source>이퀄라이저 꺼짐</source>
        <translation>이퀄라이저 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1743"/>
        <source>음악 줄임 켜짐</source>
        <translation>음악 줄임 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1745"/>
        <source>음악 줄임 꺼짐</source>
        <translation>음악 줄임 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1760"/>
        <source>자막 투명도</source>
        <translation>자막 투명도</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1762"/>
        <source>가사 투명도</source>
        <translation>가사 투명도</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1791"/>
        <source>자막 크기</source>
        <translation>자막 크기</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1832"/>
        <source>하드웨어 디코더 사용 함</source>
        <translation>하드웨어 디코더 사용 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1835"/>
        <source> (그래픽 카드 또는 코덱이 지원하지 않을 경우 활성화가 안 될 수 있습니다)</source>
        <translation> (그래픽 카드 또는 코덱이 지원하지 않을 경우 활성화가 안 될 수 있습니다)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1839"/>
        <source>하드웨어 디코더 사용 안 함</source>
        <translation>하드웨어 디코더 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1857"/>
        <source>저화질 모드 사용 함</source>
        <translation>저화질 모드 사용 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1859"/>
        <source>저화질 모드 사용 안 함</source>
        <translation>저화질 모드 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1876"/>
        <source>프레임 드랍 사용 함</source>
        <translation>프레임 드랍 사용 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1878"/>
        <source>프레임 드랍 사용 안 함</source>
        <translation>프레임 드랍 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1895"/>
        <source>버퍼링 모드 사용 함</source>
        <translation>버퍼링 모드 사용 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1897"/>
        <source>버퍼링 모드 사용 안 함</source>
        <translation>버퍼링 모드 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1915"/>
        <source>3D 전체 해상도 사용</source>
        <translation>3D 전체 해상도 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1917"/>
        <source>3D 전체 해상도 사용 안 함</source>
        <translation>3D 전체 해상도 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1934"/>
        <source>음성 줄임 켜짐</source>
        <translation>음성 줄임 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1936"/>
        <source>음성 줄임 꺼짐</source>
        <translation>음성 줄임 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1953"/>
        <source>음성 강조 켜짐</source>
        <translation>음성 강조 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1955"/>
        <source>음성 강조 꺼짐</source>
        <translation>음성 강조 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2256"/>
        <source>재생 속도 초기화</source>
        <translation>재생 속도 초기화</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2260"/>
        <source>재생 속도 : %1배</source>
        <translation>재생 속도 : %1배</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2273"/>
        <source>화면 비율 사용 함</source>
        <translation>화면 비율 사용 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2276"/>
        <source> (화면 채우기)</source>
        <translation> (화면 채우기)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2282"/>
        <source>화면 비율 사용 안 함</source>
        <translation>화면 비율 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2345"/>
        <source>자동 판단</source>
        <translation>자동 판단</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2348"/>
        <source>항상 사용</source>
        <translation>항상 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2351"/>
        <location filename="../src/ui/RenderScreen.cpp" line="3022"/>
        <location filename="../src/ui/RenderScreen.cpp" line="3879"/>
        <source>사용 안 함</source>
        <translation>사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2357"/>
        <source>디인터레이스 (%1)</source>
        <translation>디인터레이스 (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2365"/>
        <source>디인터레이스 알고리즘 (%1)</source>
        <translation>디인터레이스 알고리즘 (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2384"/>
        <source>자막 가로 정렬 변경</source>
        <translation>자막 가로 정렬 변경</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2386"/>
        <source>가사 가로 정렬 변경</source>
        <translation>가사 가로 정렬 변경</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2391"/>
        <source>자동 정렬</source>
        <translation>자동 정렬</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2394"/>
        <source>왼쪽 정렬</source>
        <translation>왼쪽 정렬</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2397"/>
        <source>오른쪽 정렬</source>
        <translation>오른쪽 정렬</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2400"/>
        <location filename="../src/ui/RenderScreen.cpp" line="2436"/>
        <source>가운데 정렬</source>
        <translation>가운데 정렬</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2403"/>
        <location filename="../src/ui/RenderScreen.cpp" line="2442"/>
        <source>기본 정렬</source>
        <translation>기본 정렬</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2426"/>
        <source>자막 세로 정렬 변경</source>
        <translation>자막 세로 정렬 변경</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2428"/>
        <source>가사 세로 정렬 변경</source>
        <translation>가사 세로 정렬 변경</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2433"/>
        <source>상단 정렬</source>
        <translation>상단 정렬</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2439"/>
        <source>하단 정렬</source>
        <translation>하단 정렬</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2471"/>
        <location filename="../src/ui/RenderScreen.cpp" line="3531"/>
        <source>자막 싱크 %1초 빠르게</source>
        <translation>자막 싱크 %1초 빠르게</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2473"/>
        <location filename="../src/ui/RenderScreen.cpp" line="3533"/>
        <source>자막 싱크 %1초 느리게</source>
        <translation>자막 싱크 %1초 느리게</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2478"/>
        <location filename="../src/ui/RenderScreen.cpp" line="3538"/>
        <source>가사 싱크 %1초 빠르게</source>
        <translation>가사 싱크 %1초 빠르게</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2480"/>
        <location filename="../src/ui/RenderScreen.cpp" line="3540"/>
        <source>가사 싱크 %1초 느리게</source>
        <translation>가사 싱크 %1초 느리게</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2486"/>
        <location filename="../src/ui/RenderScreen.cpp" line="2562"/>
        <source> (%1초)</source>
        <translation> (%1초)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2506"/>
        <source>자막 찾기 켜짐</source>
        <translation>자막 찾기 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2508"/>
        <source>자막 찾기 꺼짐</source>
        <translation>자막 찾기 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2519"/>
        <source>가사 찾기 켜짐</source>
        <translation>가사 찾기 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2521"/>
        <source>가사 찾기 꺼짐</source>
        <translation>가사 찾기 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2532"/>
        <source>찾은 가사 자동 저장 켜짐</source>
        <translation>찾은 가사 자동 저장 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2534"/>
        <source>찾은 가사 자동 저장 꺼짐</source>
        <translation>찾은 가사 자동 저장 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2555"/>
        <location filename="../src/ui/RenderScreen.cpp" line="3560"/>
        <source>소리 싱크 %1초 빠르게</source>
        <translation>소리 싱크 %1초 빠르게</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2557"/>
        <location filename="../src/ui/RenderScreen.cpp" line="3562"/>
        <source>소리 싱크 %1초 느리게</source>
        <translation>소리 싱크 %1초 느리게</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2587"/>
        <source>소리 (%1%)</source>
        <translation>소리 (%1%)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2599"/>
        <source>소리 꺼짐</source>
        <translation>소리 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2601"/>
        <source>소리 켜짐</source>
        <translation>소리 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2669"/>
        <source>앨범 자켓 보이기</source>
        <translation>앨범 자켓 보이기</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2671"/>
        <source>앨범 자켓 숨기기</source>
        <translation>앨범 자켓 숨기기</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2685"/>
        <source>이동 : %1 [%2]</source>
        <translation>이동 : %1 [%2]</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2709"/>
        <source>재생 위치 기억 켜짐</source>
        <translation>재생 위치 기억 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2711"/>
        <source>재생 위치 기억 꺼짐</source>
        <translation>재생 위치 기억 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2853"/>
        <source>영상을 변경 할 수 없습니다</source>
        <translation>영상을 변경 할 수 없습니다</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2862"/>
        <source>음성을 변경 할 수 없습니다</source>
        <translation>음성을 변경 할 수 없습니다</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3024"/>
        <source>화면 채우기</source>
        <translation>화면 채우기</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3026"/>
        <source>사용자 지정 (%1:%2)</source>
        <translation>사용자 지정 (%1:%2)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3581"/>
        <source>자막 캐시 모드 사용</source>
        <translation>자막 캐시 모드 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3583"/>
        <source>자막 캐시 모드 사용 안 함</source>
        <translation>자막 캐시 모드 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3658"/>
        <location filename="../src/ui/RenderScreen.cpp" line="3846"/>
        <source>기본 장치</source>
        <translation>기본 장치</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3677"/>
        <source>S/PDIF 소리 출력 장치 변경 (%1)</source>
        <translation>S/PDIF 소리 출력 장치 변경 (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3679"/>
        <location filename="../src/ui/RenderScreen.cpp" line="3708"/>
        <location filename="../src/ui/RenderScreen.cpp" line="3746"/>
        <location filename="../src/ui/RenderScreen.cpp" line="3778"/>
        <source>S/PDIF 출력 초기화를 실패 하였습니다. PCM 출력으로 전환합니다.</source>
        <translation>S/PDIF 출력 초기화를 실패 하였습니다. PCM 출력으로 전환합니다.</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3702"/>
        <source>S/PDIF 출력 사용</source>
        <translation>S/PDIF 출력 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3704"/>
        <source>S/PDIF 출력 사용 안 함</source>
        <translation>S/PDIF 출력 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3732"/>
        <source>S/PDIF 출력 시 인코딩 사용 안 함</source>
        <translation>S/PDIF 출력 시 인코딩 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3735"/>
        <source>S/PDIF 출력 시 AC3 인코딩 사용</source>
        <translation>S/PDIF 출력 시 AC3 인코딩 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3738"/>
        <source>S/PDIF 출력 시 DTS 인코딩 사용</source>
        <translation>S/PDIF 출력 시 DTS 인코딩 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3769"/>
        <source>S/PDIF 샘플 속도 (%1)</source>
        <translation>S/PDIF 샘플 속도 (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3772"/>
        <source>기본 속도</source>
        <translation>기본 속도</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3801"/>
        <source>왜곡 보정 모드 사용 안 함</source>
        <translation>왜곡 보정 모드 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3805"/>
        <source>왜곡 보정 모드 사용 : %1</source>
        <translation>왜곡 보정 모드 사용 : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3862"/>
        <source>소리 출력 장치 변경 (%1)</source>
        <translation>소리 출력 장치 변경 (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3882"/>
        <source>90도</source>
        <translation>90도</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3885"/>
        <source>180도</source>
        <translation>180도</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3888"/>
        <source>270도</source>
        <translation>270도</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3897"/>
        <source>화면 회전 각도 (%1)</source>
        <translation>화면 회전 각도 (%1)</translation>
    </message>
</context>
<context>
    <name>RepeatRange</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/RepeatRange.qml" line="117"/>
        <source>구간 반복 설정</source>
        <translation>구간 반복 설정</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/RepeatRange.qml" line="148"/>
        <location filename="../qml/AnyVODMobileClient/RepeatRange.qml" line="165"/>
        <source>시작</source>
        <translation>시작</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/RepeatRange.qml" line="192"/>
        <location filename="../qml/AnyVODMobileClient/RepeatRange.qml" line="209"/>
        <source>종료</source>
        <translation>종료</translation>
    </message>
</context>
<context>
    <name>SPDIFDelegate</name>
    <message>
        <location filename="../src/ui/delegates/SPDIFDelegate.cpp" line="46"/>
        <source>기본 속도</source>
        <translation>기본 속도</translation>
    </message>
</context>
<context>
    <name>SPDIFInterface</name>
    <message>
        <location filename="../../AnyVODClient/src/audio/SPDIFInterface.cpp" line="104"/>
        <source>기본 장치</source>
        <translation>기본 장치</translation>
    </message>
</context>
<context>
    <name>SavePlayListAs</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/SavePlayListAs.qml" line="108"/>
        <source>재생 목록 이름을 입력하세요.</source>
        <translation>재생 목록 이름을 입력하세요.</translation>
    </message>
</context>
<context>
    <name>ScanDTVChannel</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="171"/>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="299"/>
        <source>어댑터</source>
        <translation>어댑터</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="197"/>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="331"/>
        <source>형식</source>
        <translation>형식</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="224"/>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="363"/>
        <source>국가</source>
        <translation>국가</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="400"/>
        <source>채널 범위</source>
        <translation>채널 범위</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="484"/>
        <source>검색</source>
        <translation>검색</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="530"/>
        <source>중지</source>
        <translation>중지</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="563"/>
        <source>저장</source>
        <translation>저장</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="600"/>
        <source>현재 채널 : </source>
        <translation>현재 채널 : </translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="609"/>
        <source>, 찾은 채널 개수 : </source>
        <translation>, 찾은 채널 개수 : </translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="625"/>
        <source>신호 감도</source>
        <translation>신호 감도</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="724"/>
        <source>채널 </source>
        <translation>채널 </translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="724"/>
        <source>, 신호 강도 </source>
        <translation>, 신호 강도 </translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="728"/>
        <source>이름 없음</source>
        <translation>이름 없음</translation>
    </message>
</context>
<context>
    <name>ScanRadioChannel</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanRadioChannel.qml" line="171"/>
        <location filename="../qml/AnyVODMobileClient/ScanRadioChannel.qml" line="299"/>
        <source>어댑터</source>
        <translation>어댑터</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanRadioChannel.qml" line="197"/>
        <location filename="../qml/AnyVODMobileClient/ScanRadioChannel.qml" line="331"/>
        <source>형식</source>
        <translation>형식</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanRadioChannel.qml" line="224"/>
        <location filename="../qml/AnyVODMobileClient/ScanRadioChannel.qml" line="363"/>
        <source>국가</source>
        <translation>국가</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanRadioChannel.qml" line="400"/>
        <source>채널 범위</source>
        <translation>채널 범위</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanRadioChannel.qml" line="484"/>
        <source>검색</source>
        <translation>검색</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanRadioChannel.qml" line="529"/>
        <source>중지</source>
        <translation>중지</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanRadioChannel.qml" line="562"/>
        <source>저장</source>
        <translation>저장</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanRadioChannel.qml" line="599"/>
        <source>현재 채널 : </source>
        <translation>현재 채널 : </translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanRadioChannel.qml" line="608"/>
        <source>, 찾은 채널 개수 : </source>
        <translation>, 찾은 채널 개수 : </translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanRadioChannel.qml" line="624"/>
        <source>신호 감도</source>
        <translation>신호 감도</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanRadioChannel.qml" line="723"/>
        <source>채널 </source>
        <translation>채널 </translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanRadioChannel.qml" line="723"/>
        <source>, 신호 감도 </source>
        <translation>, 신호 감도 </translation>
    </message>
</context>
<context>
    <name>ScreenExplorer</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenExplorer.qml" line="132"/>
        <source>시</source>
        <translation>시</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenExplorer.qml" line="141"/>
        <source>분</source>
        <translation>분</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenExplorer.qml" line="150"/>
        <source>초</source>
        <translation>초</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenExplorer.qml" line="194"/>
        <source>갱신</source>
        <translation>갱신</translation>
    </message>
</context>
<context>
    <name>ScreenSettingItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="21"/>
        <source>비율</source>
        <translation>비율</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="21"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="22"/>
        <source>화면 비율</source>
        <translation>화면 비율</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="22"/>
        <source>사용자 지정</source>
        <translation>사용자 지정</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="24"/>
        <source>회전 각도</source>
        <translation>회전 각도</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="23"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="24"/>
        <source>영상</source>
        <translation>영상</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="25"/>
        <source>밝기</source>
        <translation>밝기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="25"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="26"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="27"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="28"/>
        <source>영상 속성</source>
        <translation>영상 속성</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="26"/>
        <source>채도</source>
        <translation>채도</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="27"/>
        <source>색상</source>
        <translation>색상</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="28"/>
        <source>대비</source>
        <translation>대비</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="29"/>
        <source>날카롭게</source>
        <translation>날카롭게</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="29"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="30"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="31"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="32"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="33"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="34"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="35"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="36"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="37"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="38"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="39"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="40"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="41"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="42"/>
        <source>영상 효과</source>
        <translation>영상 효과</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="30"/>
        <source>선명하게</source>
        <translation>선명하게</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="31"/>
        <source>부드럽게</source>
        <translation>부드럽게</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="32"/>
        <source>히스토그램 이퀄라이저</source>
        <translation>히스토그램 이퀄라이저</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="33"/>
        <source>디밴드</source>
        <translation>디밴드</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="34"/>
        <source>디블록</source>
        <translation>디블록</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="35"/>
        <source>노멀라이즈</source>
        <translation>노멀라이즈</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="36"/>
        <source>3D 노이즈 제거</source>
        <translation>3D 노이즈 제거</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="37"/>
        <source>적응 시간 평균 노이즈 제거</source>
        <translation>적응 시간 평균 노이즈 제거</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="38"/>
        <source>Overcomplete Wavelet 노이즈 제거</source>
        <translation>Overcomplete Wavelet 노이즈 제거</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="39"/>
        <source>Non-local Means 노이즈 제거</source>
        <translation>Non-local Means 노이즈 제거</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="40"/>
        <source>Vague 노이즈 제거</source>
        <translation>Vague 노이즈 제거</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="41"/>
        <source>좌우 반전</source>
        <translation>좌우 반전</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="42"/>
        <source>상하 반전</source>
        <translation>상하 반전</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="43"/>
        <source>캡처 확장자</source>
        <translation>캡처 확장자</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="43"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="44"/>
        <source>캡처</source>
        <translation>캡처</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="44"/>
        <source>캡처 저장 디렉토리 설정</source>
        <translation>캡처 저장 디렉토리 설정</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="45"/>
        <source>활성화 기준</source>
        <translation>활성화 기준</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="45"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="46"/>
        <source>디인터레이스</source>
        <translation>디인터레이스</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="46"/>
        <source>알고리즘</source>
        <translation>알고리즘</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="47"/>
        <source>하드웨어 디코더 사용</source>
        <translation>하드웨어 디코더 사용</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="47"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="48"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="49"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="50"/>
        <source>화면</source>
        <translation>화면</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="48"/>
        <source>저화질 모드 사용</source>
        <translation>저화질 모드 사용</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="49"/>
        <source>프레임 드랍 사용</source>
        <translation>프레임 드랍 사용</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="50"/>
        <source>앨범 자켓 보기</source>
        <translation>앨범 자켓 보기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="54"/>
        <source>입력 영상 출력 방법</source>
        <translation>입력 영상 출력 방법</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="54"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="55"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="56"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="57"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="58"/>
        <source>VR 영상 - 공통</source>
        <translation>VR 영상 - 공통</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="57"/>
        <source>왜곡 보정 모드</source>
        <translation>왜곡 보정 모드</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="58"/>
        <source>가상 3D 입체감</source>
        <translation>가상 3D 입체감</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="60"/>
        <source>360도 영상 모드 사용</source>
        <translation>360도 영상 모드 사용</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="60"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="61"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="62"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="63"/>
        <source>VR 영상 - 360도</source>
        <translation>VR 영상 - 360도</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="51"/>
        <source>전체 해상도 사용</source>
        <translation>전체 해상도 사용</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="51"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="52"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="53"/>
        <source>3D 영상</source>
        <translation>3D 영상</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="52"/>
        <source>출력 방법</source>
        <translation>출력 방법</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="53"/>
        <source>애너글리프 알고리즘</source>
        <translation>애너글리프 알고리즘</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="59"/>
        <source>VR 영상 - 평면</source>
        <translation>VR 영상 - 평면</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="61"/>
        <source>프로젝션 종류</source>
        <translation>프로젝션 종류</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="62"/>
        <source>헤드 트래킹 사용</source>
        <translation>헤드 트래킹 사용</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="55"/>
        <source>왜곡 보정 사용</source>
        <translation>왜곡 보정 사용</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="59"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="63"/>
        <source>왜곡 보정 계수</source>
        <translation>왜곡 보정 계수</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="56"/>
        <source>렌즈 센터 설정</source>
        <translation>렌즈 센터 설정</translation>
    </message>
</context>
<context>
    <name>SearchMedia</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/SearchMedia.qml" line="71"/>
        <source>정보</source>
        <translation>정보</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SearchMedia.qml" line="72"/>
        <source>검색 결과가 없습니다.</source>
        <translation>검색 결과가 없습니다.</translation>
    </message>
</context>
<context>
    <name>SelectVector2D</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/SelectVector2D.qml" line="131"/>
        <source>첫째</source>
        <translation>첫째</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SelectVector2D.qml" line="164"/>
        <source>둘째</source>
        <translation>둘째</translation>
    </message>
</context>
<context>
    <name>ServerSetting</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/ServerSetting.qml" line="116"/>
        <source>주소</source>
        <translation>주소</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ServerSetting.qml" line="137"/>
        <source>커맨드 포트</source>
        <translation>커맨드 포트</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ServerSetting.qml" line="155"/>
        <source>스트림 포트</source>
        <translation>스트림 포트</translation>
    </message>
</context>
<context>
    <name>SettingSlider</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/SettingSlider.qml" line="158"/>
        <source>초기화</source>
        <translation>초기화</translation>
    </message>
</context>
<context>
    <name>Socket</name>
    <message>
        <location filename="../../AnyVODClient/src/net/Socket.cpp" line="294"/>
        <source>접속이 끊겼습니다</source>
        <translation>접속이 끊겼습니다</translation>
    </message>
</context>
<context>
    <name>SortMenuItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/SortMenuItems.qml" line="22"/>
        <source>이름(오름차순)</source>
        <translation>이름(오름차순)</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SortMenuItems.qml" line="27"/>
        <source>변경 시각(오름차순)</source>
        <translation>변경 시각(오름차순)</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SortMenuItems.qml" line="32"/>
        <source>크기(오름차순)</source>
        <translation>크기(오름차순)</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SortMenuItems.qml" line="37"/>
        <source>종류(오름차순)</source>
        <translation>종류(오름차순)</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SortMenuItems.qml" line="46"/>
        <source>이름(내림차순)</source>
        <translation>이름(내림차순)</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SortMenuItems.qml" line="51"/>
        <source>변경 시각(내림차순)</source>
        <translation>변경 시각(내림차순)</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SortMenuItems.qml" line="56"/>
        <source>크기(내림차순)</source>
        <translation>크기(내림차순)</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SortMenuItems.qml" line="61"/>
        <source>종류(내림차순)</source>
        <translation>종류(내림차순)</translation>
    </message>
</context>
<context>
    <name>SoundSettingItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="21"/>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="22"/>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="23"/>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="24"/>
        <source>일반</source>
        <translation>일반</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="21"/>
        <source>음성</source>
        <translation>음성</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="22"/>
        <source>싱크</source>
        <translation>싱크</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="23"/>
        <source>블루투스 싱크</source>
        <translation>블루투스 싱크</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="24"/>
        <source>오디오 장치</source>
        <translation>오디오 장치</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="25"/>
        <source>노멀라이저 사용</source>
        <translation>노멀라이저 사용</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="25"/>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="26"/>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="27"/>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="28"/>
        <source>효과</source>
        <translation>효과</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="26"/>
        <source>음성 줄임</source>
        <translation>음성 줄임</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="27"/>
        <source>음성 강조</source>
        <translation>음성 강조</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="28"/>
        <source>음악 줄임</source>
        <translation>음악 줄임</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="29"/>
        <source>출력 장치</source>
        <translation>출력 장치</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="29"/>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="30"/>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="31"/>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="32"/>
        <source>S/PDIF</source>
        <translation>S/PDIF</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="30"/>
        <source>인코딩</source>
        <translation>인코딩</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="31"/>
        <source>샘플링 속도</source>
        <translation>샘플링 속도</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="32"/>
        <source>사용 하기</source>
        <translation>사용 하기</translation>
    </message>
</context>
<context>
    <name>SubtitleLyricsSettingItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="21"/>
        <source>열기</source>
        <translation>열기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="21"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="22"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="23"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="24"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="25"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="26"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="27"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="28"/>
        <source>일반</source>
        <translation>일반</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="22"/>
        <source>외부 닫기</source>
        <translation>외부 닫기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="23"/>
        <source>보이기</source>
        <translation>보이기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="24"/>
        <source>고급 검색</source>
        <translation>고급 검색</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="25"/>
        <source>투명도</source>
        <translation>투명도</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="26"/>
        <source>크기</source>
        <translation>크기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="27"/>
        <source>싱크</source>
        <translation>싱크</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="28"/>
        <source>자막 캐시 모드 사용</source>
        <translation>자막 캐시 모드 사용</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="29"/>
        <source>가로 위치</source>
        <translation>가로 위치</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="29"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="30"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="31"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="32"/>
        <source>정렬</source>
        <translation>정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="30"/>
        <source>세로 위치</source>
        <translation>세로 위치</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="31"/>
        <source>가로 정렬</source>
        <translation>가로 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="32"/>
        <source>세로 정렬</source>
        <translation>세로 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="33"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="34"/>
        <source>언어</source>
        <translation>언어</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="34"/>
        <source>인코딩</source>
        <translation>인코딩</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="35"/>
        <source>자막 찾기</source>
        <translation>자막 찾기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="35"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="36"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="37"/>
        <source>찾기</source>
        <translation>찾기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="36"/>
        <source>가사 찾기</source>
        <translation>가사 찾기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="37"/>
        <source>찾은 가사 자동 저장</source>
        <translation>찾은 가사 자동 저장</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="38"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="39"/>
        <source>저장</source>
        <translation>저장</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="39"/>
        <source>다른 이름으로 저장</source>
        <translation>다른 이름으로 저장</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="40"/>
        <source>가로 거리</source>
        <translation>가로 거리</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="40"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="41"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="42"/>
        <source>3D 자막</source>
        <translation>3D 자막</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="41"/>
        <source>세로 거리</source>
        <translation>세로 거리</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="42"/>
        <source>출력 방법</source>
        <translation>출력 방법</translation>
    </message>
</context>
<context>
    <name>UserAspectRatio</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/UserAspectRatio.qml" line="106"/>
        <source>넓이</source>
        <translation>넓이</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/UserAspectRatio.qml" line="138"/>
        <source>높이</source>
        <translation>높이</translation>
    </message>
</context>
<context>
    <name>VideoRenderer</name>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1060"/>
        <source>가사 있음</source>
        <translation>가사 있음</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1062"/>
        <source>자막 있음</source>
        <translation>자막 있음</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1067"/>
        <source>가사 없음</source>
        <translation>가사 없음</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1069"/>
        <source>자막 없음</source>
        <translation>자막 없음</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1075"/>
        <source>파일 이름 : </source>
        <translation>파일 이름 : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1092"/>
        <source>재생 위치 : </source>
        <translation>재생 위치 : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1108"/>
        <source>파일 포맷 : </source>
        <translation>파일 포맷 : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1115"/>
        <source>CPU 사용률 : </source>
        <translation>CPU 사용률 : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1127"/>
        <source>메모리 사용량 : </source>
        <translation>메모리 사용량 : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1142"/>
        <source>DTV 신호 감도 : </source>
        <translation>DTV 신호 감도 : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1156"/>
        <source>비디오 코덱 : </source>
        <translation>비디오 코덱 : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1167"/>
        <source>하드웨어 디코더 : </source>
        <translation>하드웨어 디코더 : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1175"/>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1299"/>
        <source>입력 : </source>
        <translation>입력 : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1190"/>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1313"/>
        <source>출력 : </source>
        <translation>출력 : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1209"/>
        <source>색상 변환 : </source>
        <translation>색상 변환 : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1222"/>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1233"/>
        <source>프레임 : </source>
        <translation>프레임 : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1255"/>
        <source>오디오 코덱 : </source>
        <translation>오디오 코덱 : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1266"/>
        <source>S/PDIF 오디오 장치 : </source>
        <translation>S/PDIF 오디오 장치 : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1273"/>
        <source>인코딩 사용</source>
        <translation>인코딩 사용</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1331"/>
        <source>가사 코덱 : </source>
        <translation>가사 코덱 : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/VideoRenderer.cpp" line="1333"/>
        <source>자막 코덱 : </source>
        <translation>자막 코덱 : </translation>
    </message>
</context>
<context>
    <name>VideoScreen</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="191"/>
        <source>자막 찾음</source>
        <translation>자막 찾음</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="201"/>
        <source>이퀄라이저</source>
        <translation>이퀄라이저</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="213"/>
        <source>채널 편성표</source>
        <translation>채널 편성표</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="224"/>
        <source>원격 서버 로그인</source>
        <translation>원격 서버 로그인</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="546"/>
        <source>지원하지 않는 기능입니다.</source>
        <translation>지원하지 않는 기능입니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="555"/>
        <source>영상</source>
        <translation>영상</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="578"/>
        <source>디인터레이스 활성화 기준</source>
        <translation>디인터레이스 활성화 기준</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="582"/>
        <source>자동</source>
        <translation>자동</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="583"/>
        <source>사용</source>
        <translation>사용</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="584"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="730"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1373"/>
        <source>사용 안 함</source>
        <translation>사용 안 함</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="595"/>
        <source>디인터레이스 알고리즘</source>
        <translation>디인터레이스 알고리즘</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="617"/>
        <source>화면 비율</source>
        <translation>화면 비율</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="639"/>
        <source>화면 비율 사용자 지정</source>
        <translation>화면 비율 사용자 지정</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="651"/>
        <source>평면 VR 왜곡 보정 계수 설정</source>
        <translation>평면 VR 왜곡 보정 계수 설정</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="652"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="671"/>
        <source>k1</source>
        <translation>k1</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="653"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="672"/>
        <source>k2</source>
        <translation>k2</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="670"/>
        <source>360도 VR 왜곡 보정 계수 설정</source>
        <translation>360도 VR 왜곡 보정 계수 설정</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="689"/>
        <source>VR 렌즈 센터 설정</source>
        <translation>VR 렌즈 센터 설정</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="690"/>
        <source>좌/우</source>
        <translation>좌/우</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="691"/>
        <source>상/하</source>
        <translation>상/하</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="708"/>
        <source>프로젝션 종류</source>
        <translation>프로젝션 종류</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="726"/>
        <source>영상 회전 각도</source>
        <translation>영상 회전 각도</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="731"/>
        <source>90도</source>
        <translation>90도</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="732"/>
        <source>180도</source>
        <translation>180도</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="733"/>
        <source>270도</source>
        <translation>270도</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="744"/>
        <source>캡처 확장자</source>
        <translation>캡처 확장자</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="763"/>
        <source>3D 영상 출력 방법</source>
        <translation>3D 영상 출력 방법</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="786"/>
        <source>3D 영상 애너글리프 알고리즘</source>
        <translation>3D 영상 애너글리프 알고리즘</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="830"/>
        <source>왜곡 보정 모드</source>
        <translation>왜곡 보정 모드</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="852"/>
        <source>재생 순서 설정</source>
        <translation>재생 순서 설정</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="856"/>
        <source>전체 순차 재생</source>
        <translation>전체 순차 재생</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="857"/>
        <source>전체 반복 재생</source>
        <translation>전체 반복 재생</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="858"/>
        <source>한 개 재생</source>
        <translation>한 개 재생</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="859"/>
        <source>한 개 반복 재생</source>
        <translation>한 개 반복 재생</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="860"/>
        <source>무작위 재생</source>
        <translation>무작위 재생</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="870"/>
        <source>밝기</source>
        <translation>밝기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="876"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="894"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="930"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="951"/>
        <source>배</source>
        <translation>배</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="888"/>
        <source>채도</source>
        <translation>채도</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="906"/>
        <source>색상</source>
        <translation>색상</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="912"/>
        <source>°</source>
        <translation>°</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="924"/>
        <source>대비</source>
        <translation>대비</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="942"/>
        <source>재생 속도 설정</source>
        <translation>재생 속도 설정</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="963"/>
        <source>투명도</source>
        <translation>투명도</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="969"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="987"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="981"/>
        <source>크기</source>
        <translation>크기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="999"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1205"/>
        <source>싱크</source>
        <translation>싱크</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1006"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1212"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1232"/>
        <source>초</source>
        <translation>초</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1018"/>
        <source>가로 위치</source>
        <translation>가로 위치</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1019"/>
        <source>값이 클 수록 오른쪽으로 이동합니다.</source>
        <translation>값이 클 수록 오른쪽으로 이동합니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1036"/>
        <source>세로 위치</source>
        <translation>세로 위치</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1037"/>
        <source>값이 클 수록 윗쪽으로 이동합니다.</source>
        <translation>값이 클 수록 윗쪽으로 이동합니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1055"/>
        <source>가로 정렬</source>
        <translation>가로 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1059"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1078"/>
        <source>기본 정렬</source>
        <translation>기본 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1060"/>
        <source>자동 정렬</source>
        <translation>자동 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1061"/>
        <source>왼쪽 정렬</source>
        <translation>왼쪽 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1062"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1080"/>
        <source>가운데 정렬</source>
        <translation>가운데 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1063"/>
        <source>오른쪽 정렬</source>
        <translation>오른쪽 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1074"/>
        <source>세로 정렬</source>
        <translation>세로 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1079"/>
        <source>상단 정렬</source>
        <translation>상단 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1081"/>
        <source>하단 정렬</source>
        <translation>하단 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1090"/>
        <source>텍스트 인코딩</source>
        <translation>텍스트 인코딩</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1109"/>
        <source>언어</source>
        <translation>언어</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1129"/>
        <source>세로 거리</source>
        <translation>세로 거리</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1147"/>
        <source>가로 거리</source>
        <translation>가로 거리</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1165"/>
        <source>가상 3D 입체감</source>
        <translation>가상 3D 입체감</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1166"/>
        <source>값이 클 수록 입체감이 강해집니다.</source>
        <translation>값이 클 수록 입체감이 강해집니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1184"/>
        <source>3D 자막 출력 방법</source>
        <translation>3D 자막 출력 방법</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1224"/>
        <source>블루투스 싱크</source>
        <translation>블루투스 싱크</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1225"/>
        <source>기기에 따라 값이 적용 되지 않을 수 있습니다.</source>
        <translation>기기에 따라 값이 적용 되지 않을 수 있습니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1244"/>
        <source>열기</source>
        <translation>열기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1263"/>
        <source>다른 이름으로 저장</source>
        <translation>다른 이름으로 저장</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1287"/>
        <source>저장 디렉토리 설정</source>
        <translation>저장 디렉토리 설정</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1302"/>
        <source>음성</source>
        <translation>음성</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1323"/>
        <source>오디오 장치</source>
        <translation>오디오 장치</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1330"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1352"/>
        <source>기본 장치</source>
        <translation>기본 장치</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1345"/>
        <source>출력 장치</source>
        <translation>출력 장치</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1369"/>
        <source>인코딩</source>
        <translation>인코딩</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1374"/>
        <source>AC3</source>
        <translation>AC3</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1375"/>
        <source>DTS</source>
        <translation>DTS</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1386"/>
        <source>샘플링 속도</source>
        <translation>샘플링 속도</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1407"/>
        <source>화면</source>
        <translation>화면</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1496"/>
        <source>재생</source>
        <translation>재생</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1527"/>
        <source>자막 / 가사</source>
        <translation>자막 / 가사</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1607"/>
        <source>소리</source>
        <translation>소리</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1655"/>
        <source>AnyVOD 정보</source>
        <translation>AnyVOD 정보</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="2266"/>
        <source>화면 잠김 해제</source>
        <translation>화면 잠김 해제</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="545"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1666"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1747"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1758"/>
        <source>정보</source>
        <translation>정보</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="808"/>
        <source>VR 입력 영상 출력 방법</source>
        <translation>VR 입력 영상 출력 방법</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1130"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1148"/>
        <source>값이 클 수록 가까워집니다. VR 모드일 경우 반대 입니다.</source>
        <translation>값이 클 수록 가까워집니다. VR 모드일 경우 반대 입니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1748"/>
        <source>외부 자막을 닫았습니다.</source>
        <translation>외부 자막을 닫았습니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1759"/>
        <source>자막 / 가사가 저장 되었습니다.</source>
        <translation>자막 / 가사가 저장 되었습니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1769"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1780"/>
        <source>오류</source>
        <translation>오류</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1770"/>
        <source>자막 / 가사가 저장 되지 않았습니다.</source>
        <translation>자막 / 가사가 저장 되지 않았습니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1781"/>
        <source>자막 / 가사를 열지 못했습니다.</source>
        <translation>자막 / 가사를 열지 못했습니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="2261"/>
        <source>화면 잠김</source>
        <translation>화면 잠김</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="2748"/>
        <source>외부 서버에 자막이 존재합니다.
이동하시려면 여기를 클릭하세요.</source>
        <translation>외부 서버에 자막이 존재합니다.
이동하시려면 여기를 클릭하세요.</translation>
    </message>
</context>
<context>
    <name>VideoScreenMenuItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreenMenuItems.qml" line="23"/>
        <source>이퀄라이저</source>
        <translation>이퀄라이저</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreenMenuItems.qml" line="29"/>
        <source>화면</source>
        <translation>화면</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreenMenuItems.qml" line="35"/>
        <source>재생</source>
        <translation>재생</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreenMenuItems.qml" line="41"/>
        <source>자막 / 가사</source>
        <translation>자막 / 가사</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreenMenuItems.qml" line="47"/>
        <source>소리</source>
        <translation>소리</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreenMenuItems.qml" line="53"/>
        <source>정보</source>
        <translation>정보</translation>
    </message>
</context>
<context>
    <name>ViewEPG</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/ViewEPG.qml" line="154"/>
        <source>채널</source>
        <translation>채널</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ViewEPG.qml" line="162"/>
        <source>제목</source>
        <translation>제목</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ViewEPG.qml" line="167"/>
        <location filename="../qml/AnyVODMobileClient/ViewEPG.qml" line="176"/>
        <location filename="../qml/AnyVODMobileClient/ViewEPG.qml" line="185"/>
        <source>업데이트 중...</source>
        <translation>업데이트 중...</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ViewEPG.qml" line="171"/>
        <source>방송 시간</source>
        <translation>방송 시간</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ViewEPG.qml" line="180"/>
        <source>현재 시각</source>
        <translation>현재 시각</translation>
    </message>
</context>
</TS>
