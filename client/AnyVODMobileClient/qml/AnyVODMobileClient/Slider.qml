﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12
import QtQuick.Controls 2.12

Slider {
    property int backgroundLeftMargin: 0
    property real backgroundHeightRatio: 1.0
    property int handleBorderWidth: 0
    property real handleWidthRatio: 1.0

    signal handleXChanged

    background: Rectangle {
        x: isVertical() ? 0 : parent.leftPadding + radius
        y: isVertical() ? parent.topPadding + radius : 0
        width: isVertical() ? parent.width : parent.availableWidth - radius * 2
        height: isVertical() ? parent.availableHeight - radius * 2 : parent.height * 0.7
        anchors.horizontalCenter: isVertical() ? parent.horizontalCenter : undefined
        anchors.verticalCenter: isVertical() ? undefined : parent.verticalCenter
        color: "lightgray"
        radius: 8

        Rectangle{
            anchors.left: parent.left
            anchors.leftMargin: backgroundLeftMargin
            anchors.verticalCenter: isVertical() ? undefined : parent.verticalCenter
            y: isVertical() ? parent.parent.handle.y : 0
            width: isVertical() ? parent.width : parent.parent.visualPosition * parent.width - anchors.leftMargin
            height: isVertical() ? (parent.height - y) * backgroundHeightRatio : parent.height * backgroundHeightRatio
            color: "#1f88f9"
            radius: parent.radius
        }
    }

    handle: Rectangle {
        x: isVertical() ? 0 : parent.leftPadding + parent.visualPosition * (parent.availableWidth - width)
        y: isVertical() ? parent.topPadding + parent.visualPosition * (parent.availableHeight - height) : 0
        anchors.horizontalCenter: isVertical() ? parent.horizontalCenter : undefined
        color: parent.pressed ? "white" : "lightgray"
        border.color: "gray"
        border.width: handleBorderWidth
        width: isVertical() ? parent.width * handleWidthRatio : parent.height * handleWidthRatio
        height: width
        radius: width

        onXChanged: {
            handleXChanged()
        }
    }

    function isVertical() {
        return orientation == Qt.Vertical
    }
}
