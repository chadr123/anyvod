﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.0
import com.dcple.anyvod 1.0

Rectangle {
    id: topRoot
    focus: true
    color: "#88000000"

    property string desc
    property string link

    property alias windowWidth: root.width
    property alias windowHeight: root.height
    property alias title: header.title

    signal goBack

    function getDesc()
    {
        return desc.replace(/\n/gi, "<br>")
    }

    DropShadow {
        anchors.fill: root
        radius: 16.0
        samples: 16
        color: "#80000000"
        source: root
    }

    MouseArea {
        anchors.fill: parent

        onWheel: wheel.accepted = true
        onClicked: exit()
        onCanceled: exit()
    }

    Rectangle {
        id: root
        anchors.centerIn: parent
        width: parent.width * 0.8
        height: parent.height * 0.8
        radius: 3
        border.color: "gray"

        MouseArea {
            anchors.fill: parent

            ColumnLayout {
                anchors.fill: parent
                spacing: 0

                Header {
                    id: header
                    Layout.fillWidth: true
                    Layout.minimumHeight: 50
                    Layout.maximumHeight: Layout.minimumHeight
                }

                Rectangle {
                    id: itemRoot
                    color: "white"
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    Text {
                        id: link
                        focus: true
                        text: "<a style='color: black;text-decoration: none;' href='" + topRoot.link + "'>" + getDesc() + "</a>"
                        textFormat: Text.RichText
                        anchors.fill: parent
                        anchors.topMargin: 5
                        anchors.bottomMargin: 5
                        anchors.leftMargin: 20
                        anchors.rightMargin: 10
                        font.pixelSize: 15
                        wrapMode: Text.Wrap
                        verticalAlignment: Text.AlignVCenter

                        onLinkActivated: {
                            Qt.openUrlExternally(topRoot.link)
                            goBack()
                        }
                    }
                }

                FooterWithButtons {
                    types: AnyVODEnums.BT_CANCEL
                    Layout.fillWidth: true
                    Layout.minimumHeight: 50
                    Layout.maximumHeight: Layout.minimumHeight

                    onCancelClicked: {
                        goBack()
                    }
                }
            }
        }
    }

    Keys.onReleased: {
        if (event.key === Qt.Key_Back)
        {
            event.accepted = true

            goBack()
        }
    }
}
