﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.0
import com.dcple.anyvod 1.0
import "." as AnyVOD

Rectangle {
    id: topRoot
    focus: true
    color: "#88000000"

    property MediaPlayerSettingDelegate setting: null
    property alias windowWidth: root.width
    property alias windowHeight: root.height
    property alias title: header.title
    property size aspectSize: Qt.size(0.0, 0.0)

    signal goBack
    signal accepted
    signal ignore

    function exit()
    {
        goBack()
        ignore()
    }

    VirtualKeyboardPanel {
        id: keyboard
    }

    DropShadow {
        anchors.fill: root
        radius: 16.0
        samples: 16
        color: "#80000000"
        source: root
    }

    MouseArea {
        anchors.fill: parent

        onWheel: wheel.accepted = true
        onClicked: exit()
        onCanceled: exit()
    }

    Rectangle {
        id: root
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 10
        width: parent.width * 0.9
        height: parent.height * 0.8
        radius: 3
        border.color: "gray"

        MouseArea {
            anchors.fill: parent

            ColumnLayout {
                anchors.fill: parent
                spacing: 0

                Header {
                    id: header
                    Layout.fillWidth: true
                    Layout.minimumHeight: 50
                    Layout.maximumHeight: Layout.minimumHeight
                }

                Rectangle {
                    id: itemRoot
                    color: "white"
                    clip: true
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    RowLayout {
                        anchors.fill: parent

                        Rectangle {
                            color: "transparent"
                            Layout.minimumWidth: 10
                            Layout.maximumWidth: Layout.minimumWidth
                            Layout.fillHeight: true
                        }

                        Text {
                            text: qsTr("넓이")
                        }

                        Rectangle {
                            color: "transparent"
                            Layout.minimumWidth: 5
                            Layout.maximumWidth: Layout.minimumWidth
                            Layout.fillHeight: true
                        }

                        AnyVOD.SpinBoxFloat {
                            id: widthValue
                            valueF: aspectSize.width
                            fromF: 1
                            toF: 100
                            decimals: 2
                            fontSize: 15
                            indicatorWidth: 30
                            Layout.minimumWidth: 110
                            Layout.maximumWidth: Layout.minimumWidth
                            Layout.minimumHeight: 40
                            Layout.maximumHeight: Layout.minimumHeight
                            Layout.alignment: Qt.AlignHCenter
                        }

                        Rectangle {
                            color: "transparent"
                            Layout.fillWidth: true
                            Layout.fillHeight: true
                        }

                        Text {
                            text: qsTr("높이")
                        }

                        Rectangle {
                            color: "transparent"
                            Layout.minimumWidth: 5
                            Layout.maximumWidth: Layout.minimumWidth
                            Layout.fillHeight: true
                        }

                        AnyVOD.SpinBoxFloat {
                            id: heightValue
                            valueF: aspectSize.height
                            fromF: 1
                            toF: 100
                            decimals: 2
                            fontSize: 15
                            indicatorWidth: 30
                            Layout.minimumWidth: 110
                            Layout.maximumWidth: Layout.minimumWidth
                            Layout.minimumHeight: 40
                            Layout.maximumHeight: Layout.minimumHeight
                            Layout.alignment: Qt.AlignHCenter
                        }

                        Rectangle {
                            color: "transparent"
                            Layout.minimumWidth: 10
                            Layout.maximumWidth: Layout.minimumWidth
                            Layout.fillHeight: true
                        }
                    }
                }

                FooterWithButtons {
                    types: AnyVODEnums.BT_OK | AnyVODEnums.BT_CANCEL
                    Layout.fillWidth: true
                    Layout.minimumHeight: 50
                    Layout.maximumHeight: Layout.minimumHeight

                    onOkClicked: {
                        setting.setSetting(AnyVODEnums.SK_USER_ASPECT_RATIO, Qt.size(widthValue.realValue, heightValue.realValue))

                        goBack()
                        accepted()
                    }

                    onCancelClicked: {
                        exit()
                    }
                }
            }
        }
    }

    Keys.onReleased: {
        if (event.key === Qt.Key_Back)
        {
            event.accepted = true

            exit()
        }
    }
}
