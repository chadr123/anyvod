﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.0

Rectangle {
    id: root

    property bool inputFocus: true
    property alias text: input.text
    property alias inputMethodHints: input.inputMethodHints
    property alias echoMode: input.echoMode
    property alias maximumLength: input.maximumLength
    signal finished

    function finish()
    {
        if (input.activeFocus)
        {
            input.selectAll()
            input.deselect()
        }
    }

    DropShadow {
        anchors.fill: source
        visible: source.visible
        opacity: source.opacity
        radius: 16.0
        samples: 16
        color: "#80000000"
        source: menu
    }

    Rectangle {
        id: menu
        anchors.top: parent.top
        anchors.topMargin: -(height + 5)
        width: menuContent.width
        height: 40
        visible: false
        opacity: 0.0
        z: 10

        function show()
        {
            if (Qt.platform.os === "ios" || Qt.platform.os === "android")
                return

            visible = true
            opacity = 1.0
        }

        function hide()
        {
            if (Qt.platform.os === "ios" || Qt.platform.os === "android")
                return

            visible = false
            opacity = 0.0
        }

        Behavior on opacity {
            NumberAnimation {
                id: menuOpacityAni
                duration: 200
                easing.type: Easing.OutQuad
            }
        }

        RowLayout {
            id: menuContent
            height: parent.height
            spacing: 17

            Rectangle {
                color: "transparent"
                Layout.minimumWidth: 0
                Layout.maximumWidth: Layout.minimumWidth
                Layout.fillHeight: true
            }

            ImageButton {
                id: cut
                paddingValue: 15
                visible: input.isSelected()
                text.text: qsTr("잘라내기")
                text.verticalAlignment: Text.AlignVCenter
                text.horizontalAlignment: Text.AlignHCenter
                Layout.fillHeight: true

                onClicked: {
                    input.cut()
                    menu.hide()
                }
            }

            ImageButton {
                id: copy
                paddingValue: 15
                visible: input.isSelected()
                text.text: qsTr("복사하기")
                text.verticalAlignment: Text.AlignVCenter
                text.horizontalAlignment: Text.AlignHCenter
                Layout.fillHeight: true

                onClicked: {
                    input.copy()
                    menu.hide()
                }
            }

            ImageButton {
                id: paste
                paddingValue: 15
                text.text: qsTr("붙여넣기")
                text.verticalAlignment: Text.AlignVCenter
                text.horizontalAlignment: Text.AlignHCenter
                Layout.fillHeight: true

                onClicked: {
                    input.paste()
                    menu.hide()
                }
            }
        }
    }

    Rectangle {
        anchors.fill: parent
        clip: true

        TextInput {
            id: input
            focus: true
            width: parent.width - cursorWidth
            height: parent.height - bottomLine.height * 2
            selectionColor: "lightblue"
            selectedTextColor: color
            cursorDelegate: Rectangle {
                width: parent.cursorWidth
                visible: parent.cursorVisible
                color: "#1f88f9"

                SequentialAnimation on opacity {
                    running: true
                    loops: Animation.Infinite

                    NumberAnimation {
                        to: 0
                        duration: 600
                    }

                    NumberAnimation {
                        to: 1
                        duration: 600
                    }
                }
            }

            property int cursorWidth: 2

            function isSelected()
            {
                return input.selectionStart != input.selectionEnd
            }

            onAccepted: {
                finished()
            }

            Component.onCompleted: {
                if (inputFocus)
                    forceActiveFocus()
            }
        }

        Rectangle {
            id: bottomLine
            anchors.bottom: parent.bottom
            width: parent.width
            height: 2
            color: "#1f88f9"
        }
    }

    MouseArea {
        anchors.fill: parent

        property real threshold: 10.0
        property real startX: 0
        property bool dragging: false
        property bool selectedByDragging: false
        property int lastStart: -1

        onPressAndHold: {
            if (!dragging)
            {
                input.selectAll()
                menu.show()
            }
        }

        onDoubleClicked: {
            menu.show()
        }

        onPositionChanged: {
            var p = mouse

            if (pressed)
            {
                var xDelta = p.x - startX

                if (!dragging && Math.abs(xDelta) >= threshold)
                    dragging = true

                if (dragging)
                {
                    var start = lastStart
                    var end =  input.positionAt(p.x, p.y, TextInput.CursorOnCharacter)

                    input.select(start, end)
                    selectedByDragging = true
                }
            }
        }

        onPressed: {
            startX = mouse.x
            lastStart = input.positionAt(startX, mouse.y, TextInput.CursorOnCharacter)
            selectedByDragging = false
        }

        onReleased: {
            startX = 0
            lastStart = -1
            dragging = false

            if (selectedByDragging)
                menu.show()
        }

        onClicked: {
            if (!dragging)
            {
                input.forceActiveFocus()

                if (!selectedByDragging)
                {
                    input.cursorPosition = input.positionAt(mouse.x, mouse.y, TextInput.CursorOnCharacter)

                    if (input.isSelected())
                        input.deselect()

                    menu.hide()
                }

                if (!Qt.inputMethod.visible)
                    Qt.inputMethod.show()

                selectedByDragging = false
            }
        }
    }
}
