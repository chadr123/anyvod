﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12
import com.dcple.anyvod 1.0

ListModel {
    ListElement {
        text: qsTr("이름(오름차순)")
        menuID: FileListModel.NameAscending
    }

    ListElement {
        text: qsTr("변경 시각(오름차순)")
        menuID: FileListModel.TimeAscending
    }

    ListElement {
        text: qsTr("크기(오름차순)")
        menuID: FileListModel.SizeAscending
    }

    ListElement {
        text: qsTr("종류(오름차순)")
        menuID: FileListModel.TypeAscending
    }

    ListElement {
        separator: true
    }

    ListElement {
        text: qsTr("이름(내림차순)")
        menuID: FileListModel.NameDescending
    }

    ListElement {
        text: qsTr("변경 시각(내림차순)")
        menuID: FileListModel.TimeDescending
    }

    ListElement {
        text: qsTr("크기(내림차순)")
        menuID: FileListModel.SizeDescending
    }

    ListElement {
        text: qsTr("종류(내림차순)")
        menuID: FileListModel.TypeDescending
    }
}
