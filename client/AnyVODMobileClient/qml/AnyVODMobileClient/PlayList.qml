﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.0
import com.dcple.anyvod 1.0

Rectangle {
    id: topRoot
    focus: true
    color: "#88000000"

    property alias windowWidth: root.width
    property alias windowHeight: root.height
    property PlayListModel playListModel: null
    property MediaPlayerSettingDelegate settingDelegate: null
    property RenderScreen screen: null
    property bool useReload: false

    signal goBack
    signal itemSelected(int index)
    signal itemSelectedAndGoto(int index, double time)
    signal newQualitySelected(int index, int quality)

    function exit()
    {
        hideMenus()
        goBack()
    }

    function showItemMenu(menu, parent, index)
    {
        var localCoord = parent.mapToItem(root, parent.x, parent.y)
        var showX = parent.x - menu.menuWidth + parent.width
        var showY = localCoord.y - parent.height
        var offset = 10
        var maxHeight = showY + menu.menuHeight + offset

        if (maxHeight >= root.height)
            showY = root.height - (menu.menuHeight + offset)

        menu.itemIndex = index
        menu.show(showX, showY)
    }

    function hideMenus()
    {
        itemMenu.hide()
    }

    DropShadow {
        anchors.fill: root
        radius: 16.0
        samples: 16
        color: "#80000000"
        source: root
    }

    MouseArea {
        anchors.fill: parent

        onWheel: wheel.accepted = true
        onClicked: exit()
        onCanceled: exit()
    }

    Component {
        id: notSupported

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("정보")
            message: qsTr("지원하지 않는 기능입니다.")
        }
    }

    Component {
        id: copyClipboard

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("정보")
            message: qsTr("클립보드로 경로가 복사 되었습니다.")
        }
    }

    Component {
        id: confirmName

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("오류")
            message: qsTr("이름을 입력 해 주세요.")
        }
    }

    Component {
        id: reloadPlayList

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("질문")
            message: qsTr("갱신 하시겠습니까?")
            useCancel: true

            onOk: {
                screen.wantToPlay = true

                playListModel.clearPlayList()
                screen.open(playListModel.getBaseURL())
                playListModel.updateCurrentPlayList()

                settingDlgLoader.exit()
            }
        }
    }

    Component {
        id: alreadyExist

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 170)
            title: qsTr("질문")
            message: qsTr("이미 동일한 재생 목록이 존재합니다.\n갱신 하시겠습니까?")
            useCancel: true

            onOk: {
                playListModel.savePlayListAs(messageBoxLoader.text)
                settingDlgLoader.exit()
            }
        }
    }

    Component {
        id: viewPath

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 250)
            title: qsTr("정보")
            message: messageBoxLoader.text
            messageVAlign: Text.AlignTop
        }
    }

    Component {
        id: qualityList

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 470)
            listHeight: Math.min(settingListDefaultHeight, 423)
            title: qsTr("화질 목록")
            setting: settingDelegate
            type: AnyVODEnums.SK_OTHER_QUALITY
            model: ListModel {
                Component.onCompleted: {
                    var descriptions = playListModel.getQualityDescriptions(itemMenu.itemIndex)
                    var i = 0

                    descriptions.forEach(function(desc) {
                        append({ "desc": desc, "value": i++ })
                    })
                }
            }

            onItemSelected: {
                topRoot.exit()
                newQualitySelected(itemMenu.itemIndex, index)
            }
        }
    }

    Component {
        id: savePlayListAs

        SavePlayListAs {
            id: savePlayListAsObject
            windowHeight: Math.min(settingListDefaultHeight, 190)
            title: qsTr("재생 목록 신규 저장")
            name: playListModel.getName()

            onAccepted: {
                if (name.length <= 0)
                {
                    messageBoxLoader.focusParent = savePlayListAsObject
                    messageBoxLoader.sourceComponent = confirmName
                }
                else
                {
                    if (playListModel.existPlayList(name))
                    {
                        messageBoxLoader.text = name
                        messageBoxLoader.sourceComponent = alreadyExist
                    }
                    else
                    {
                        playListModel.savePlayListAs(name)
                        settingDlgLoader.exit()
                    }
                }

                if (messageBoxLoader.sourceComponent != undefined)
                    messageBoxLoader.forceActiveFocus()
            }
        }
    }

    Component {
        id: screenExplorer

        ScreenExplorer {
            title: qsTr("장면 탐색")
            listModel: screenExplorerListModel

            onSelected: {
                topRoot.exit()
                itemSelectedAndGoto(messageBoxLoader.index, time)
            }

            Component.onCompleted: {
                filePath = messageBoxLoader.text
                init()
            }
        }
    }

    ScreenExplorerListModel {
        id: screenExplorerListModel
    }

    Rectangle {
        id: root
        anchors.centerIn: parent
        width: parent.width * 0.9
        height: parent.height * 0.8
        radius: 3
        border.color: "gray"

        Menus {
            id: itemMenu
            menuHeight: 160
            model: PlayListMenuItems {}

            property int itemIndex

            onMenuItemSelected: {
                var item = model.get(index)
                var path = playListModel.getPath(itemIndex, false)

                switch (item.menuID)
                {
                    case AnyVODEnums.MID_VIEW_PATH:
                        messageBoxLoader.text = path
                        messageBoxLoader.sourceComponent = viewPath

                        break
                    case AnyVODEnums.MID_COPY_PATH:
                        playListModel.copyPath(path)
                        messageBoxLoader.sourceComponent = copyClipboard

                        break
                    case AnyVODEnums.MID_SCREEN_EXPLORER:
                        if (screenExplorerListModel.isMovie(path))
                        {
                            messageBoxLoader.index = itemIndex
                            messageBoxLoader.text = playListModel.getPath(itemIndex, true)
                            messageBoxLoader.sourceComponent = screenExplorer
                        }
                        else
                        {
                            messageBoxLoader.sourceComponent = notSupported
                        }

                        break
                    case AnyVODEnums.MID_OTHER_QUALITY:
                        if (playListModel.hasQualities(itemIndex))
                            settingDlgLoader.sourceComponent = qualityList
                        else
                            messageBoxLoader.sourceComponent = notSupported

                        break
                }

                if (settingDlgLoader.sourceComponent != undefined)
                    settingDlgLoader.forceActiveFocus()

                if (messageBoxLoader.sourceComponent != undefined)
                    messageBoxLoader.forceActiveFocus()
            }
        }

        MouseArea {
            anchors.fill: parent

            ColumnLayout {
                anchors.fill: parent
                spacing: 0

                Header {
                    id: header
                    title: qsTr("재생 목록")
                    Layout.fillWidth: true
                    Layout.minimumHeight: 50
                    Layout.maximumHeight: Layout.minimumHeight
                }

                Rectangle {
                    id: listRoot
                    color: "white"
                    clip: true
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    property int itemHeight: 40

                    ListView {
                        id: list
                        anchors.fill: parent
                        model: playListModel

                        function initScroll()
                        {
                            var curIndex = playListModel.getCurrentIndex()
                            var totalCount = playListModel.getCount()

                            if (curIndex >= 0 && totalCount > 0)
                            {
                                if (totalCount * listRoot.itemHeight > list.height)
                                {
                                    var remainHeight = (totalCount - curIndex) * listRoot.itemHeight

                                    if (remainHeight <= list.height)
                                    {
                                        var screenItemCount = list.height / listRoot.itemHeight

                                        list.contentY = (totalCount - screenItemCount) * listRoot.itemHeight
                                    }
                                    else
                                    {
                                        list.contentY = curIndex * listRoot.itemHeight
                                    }
                                }
                            }
                        }

                        Component.onCompleted: {
                            initScroll()
                        }

                        onHeightChanged: {
                            initScroll()
                        }

                        delegate: Rectangle {
                            id: itemRoot
                            width: list.width
                            height: listRoot.itemHeight
                            color: listRoot.color

                            Rectangle {
                                x: 10
                                width: parent.width - x
                                height: parent.height
                                color: "transparent"

                                MouseArea {
                                    id: mouseArea
                                    anchors.verticalCenter: parent.verticalCenter
                                    anchors.fill: parent

                                    RowLayout {
                                        anchors.fill: parent
                                        spacing: 10

                                        Rectangle {
                                            id: thumbnailRoot
                                            color: "transparent"
                                            border.width: 1
                                            border.color: "#d0d0d0"
                                            Layout.minimumWidth: 30
                                            Layout.maximumWidth: Layout.minimumWidth
                                            Layout.minimumHeight: 30
                                            Layout.maximumHeight: Layout.minimumHeight
                                            Layout.alignment: Qt.AlignHCenter

                                            Image {
                                                id: thumbnail
                                                width: parent.width - thumbnailRoot.border.width * 2
                                                height: parent.height - thumbnailRoot.border.width * 2
                                                anchors.verticalCenter: parent.verticalCenter
                                                anchors.horizontalCenter: parent.horizontalCenter
                                                source: model.thumbnail
                                                fillMode: Image.PreserveAspectCrop
                                            }
                                        }

                                        Text {
                                            verticalAlignment: Text.AlignVCenter
                                            color: model.color
                                            font.pixelSize: parent.height * 0.38
                                            elide: Text.ElideMiddle
                                            text: model.title.length > 0 ? model.title : model.name
                                            Layout.fillWidth: true
                                            Layout.fillHeight: true
                                        }

                                        ImageButton {
                                            id: openItemMenu
                                            image.width: 30
                                            image.height: 30
                                            source: "assets/itemMenu.png"
                                            Layout.minimumWidth: 40
                                            Layout.maximumWidth: Layout.minimumWidth
                                            Layout.minimumHeight: image.height
                                            Layout.maximumHeight: Layout.minimumHeight
                                            Layout.alignment: Qt.AlignHCenter

                                            onClicked: {
                                                showItemMenu(itemMenu, openItemMenu, model.index)
                                            }
                                        }
                                    }

                                    onClicked: {
                                        exit()
                                        itemSelected(model.index)
                                    }

                                    onPressed: {
                                        itemRoot.color = "lightgray"
                                        itemRoot.opacity = 0.5
                                    }

                                    onCanceled: {
                                        itemRoot.color = listRoot.color
                                        itemRoot.opacity = 1.0
                                    }

                                    onReleased: {
                                        onCanceled()
                                    }
                                }
                            }
                        }
                    }

                    Scrollbar {
                        id: scrollbar
                        flickable: list
                    }
                }

                FooterWithButtons {
                    types: (useReload ? AnyVODEnums.BT_RELOAD : 0) | AnyVODEnums.BT_CLOSE | AnyVODEnums.BT_NEW_SAVE
                    Layout.fillWidth: true
                    Layout.minimumHeight: 50
                    Layout.maximumHeight: Layout.minimumHeight

                    onCloseClicked: {
                        exit()
                    }

                    onNewSaveClicked: {
                        settingDlgLoader.sourceComponent = savePlayListAs
                        settingDlgLoader.forceActiveFocus()
                    }

                    onReloadClicked: {
                        messageBoxLoader.sourceComponent = reloadPlayList
                        messageBoxLoader.forceActiveFocus()
                    }
                }
            }
        }
    }

    Loader {
        id: settingDlgLoader
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true
        layer.enabled: true

        function exit()
        {
            root.forceActiveFocus()

            opacity = 0.0
            settingDlgHideTimer.restart()
        }

        Timer {
            id: settingDlgHideTimer
            interval: settingDlgOpacityAni.duration

            onTriggered: {
                parent.visible = false
                parent.sourceComponent = undefined
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: settingDlgOpacityAni
                duration: 200
                easing.type: Easing.OutQuad
            }
        }

        onLoaded: {
            visible = true
            opacity = 1.0
        }

        Connections {
            ignoreUnknownSignals: true
            target: settingDlgLoader.item

            function onGoBack() {
                settingDlgLoader.exit()
            }
        }
    }

    Loader {
        id: messageBoxLoader
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true
        layer.enabled: true

        property Item focusParent: null
        property int index: -1
        property string text

        function exit()
        {
            if (focusParent != null)
            {
                focusParent.forceActiveFocus()
                focusParent = null
            }

            opacity = 0.0
            messageBoxHideTimer.restart()
        }

        Timer {
            id: messageBoxHideTimer
            interval: messageBoxOpacityAni.duration

            onTriggered: {
                parent.visible = false
                parent.sourceComponent = undefined
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: messageBoxOpacityAni
                duration: 200
                easing.type: Easing.OutQuad
            }
        }

        onLoaded: {
            visible = true
            opacity = 1.0
        }

        Connections {
            ignoreUnknownSignals: true
            target: messageBoxLoader.item

            function onGoBack() {
                messageBoxLoader.exit()
            }
        }
    }

    Keys.onReleased: {
        if (event.key === Qt.Key_Back)
        {
            event.accepted = true

            exit()
        }
    }
}
