﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import com.dcple.anyvod 1.0

Item {
    anchors.fill: parent
    focus: true

    property MainSettingDelegate query: null
    property MediaPlayerSettingDelegate setting: null
    property alias title: header.title
    property alias model: list.model

    signal goBack
    signal checkItemSelected(int index, bool checked)
    signal itemSelected(int index)
    signal notSupportedFunction

    ColumnLayout {
        id: root
        anchors.fill: parent
        spacing: 0

        HeaderWithBack {
            id: header
            Layout.fillWidth: true
            Layout.minimumHeight: 50
            Layout.maximumHeight: Layout.minimumHeight

            onBackClicked: {
                goBack()
            }
        }

        Rectangle {
            id: listRoot
            color: "white"
            clip: true
            Layout.fillWidth: true
            Layout.fillHeight: true

            Component {
                id: listHeader

                Rectangle {
                    width: list.width
                    height: 40
                    color: "#f0f0f0"

                    Text {
                        x:10
                        width: parent.width
                        height: parent.height
                        text: section
                        verticalAlignment: Text.AlignVCenter
                        color: "gray"
                        font.pixelSize: parent.height * 0.35
                        font.bold: true
                    }
                }
            }

            Rectangle {
                anchors.fill: parent
                color: "#f8f8f8"

                ListView {
                    id: list
                    width: parent.width
                    height: parent.height

                    section.property: "category"
                    section.criteria: ViewSection.FullString
                    section.delegate: listHeader

                    delegate: Rectangle {
                        width: list.width
                        height: 50

                        Rectangle {
                            id: settingItemRoot
                            anchors.fill: parent
                            color: "white"

                            MouseArea {
                                id: mouseArea
                                anchors.fill: parent

                                Text {
                                    x: 10
                                    width: parent.width - checkButton.width - checkButton.anchors.rightMargin - x * 2
                                    height: parent.height
                                    verticalAlignment: Text.AlignVCenter
                                    font.pixelSize: parent.height * 0.36
                                    elide: Text.ElideMiddle
                                    text: model.desc
                                }

                                ImageCheckButton {
                                    id: checkButton
                                    anchors.verticalCenter: parent.verticalCenter
                                    anchors.right: parent.right
                                    anchors.rightMargin: scrollbar.width + 20
                                    visible: model.check
                                    width: 20
                                    height: width
                                    checked: model.check ? (query == null ? setting.getSetting(model.type) : query.getSetting(model.type)) : false
                                    checkedSource: "assets/checked.png"
                                    unCheckedSource: "assets/unchecked.png"

                                    onToggled: {
                                        if (query)
                                            query.setSetting(model.type, checked)
                                        else
                                            setting.setSetting(model.type, checked)

                                        checkItemSelected(index, checked)
                                    }
                                }

                                onClicked: {
                                    var isSupported = (query == null ? setting.isSupported(model.type) : query.isSupported(model.type))

                                    if (isSupported)
                                    {
                                        if (model.check)
                                        {
                                            checkButton.checked = !checkButton.checked
                                            checkButton.toggled()
                                        }
                                        else
                                        {
                                            itemSelected(index)
                                        }
                                    }
                                    else
                                    {
                                        notSupportedFunction()
                                    }
                                }

                                onPressed: {
                                    settingItemRoot.color = "lightgray"
                                    settingItemRoot.opacity = 0.5
                                }

                                onCanceled: {
                                    settingItemRoot.color = "white"
                                    settingItemRoot.opacity = 1.0
                                }

                                onReleased: {
                                    onCanceled()
                                }
                            }
                        }

                        Rectangle {
                            anchors.bottom: parent.bottom
                            width: parent.width
                            height: 1
                            color: model.index === list.count - 1 ? "white" : "lightgray"
                        }
                    }
                }

                Scrollbar {
                    id: scrollbar
                    flickable: list
                }
            }
        }
    }

    Keys.onReleased: {
        if (event.key === Qt.Key_Back)
        {
            event.accepted = true

            goBack()
        }
    }
}
