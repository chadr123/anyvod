﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12
import com.dcple.anyvod 1.0

ListModel {
    ListElement { desc: qsTr("음성"); type: AnyVODEnums.SK_AUDIO_ORDER; category: qsTr("일반"); check: false }
    ListElement { desc: qsTr("싱크"); type: AnyVODEnums.SK_AUDIO_SYNC; category: qsTr("일반"); check: false }
    ListElement { desc: qsTr("블루투스 싱크"); type: AnyVODEnums.SK_BLUETOOTH_AUDIO_SYNC; category: qsTr("일반"); check: false }
    ListElement { desc: qsTr("오디오 장치"); type: AnyVODEnums.SK_AUDIO_DEVICE_ORDER; category: qsTr("일반"); check: false }
    ListElement { desc: qsTr("노멀라이저 사용"); type: AnyVODEnums.SK_AUDIO_NORMALIZE; category: qsTr("효과"); check: true }
    ListElement { desc: qsTr("음성 줄임"); type: AnyVODEnums.SK_LOWER_VOICE; category: qsTr("효과"); check: true }
    ListElement { desc: qsTr("음성 강조"); type: AnyVODEnums.SK_HIGHER_VOICE; category: qsTr("효과"); check: true }
    ListElement { desc: qsTr("음악 줄임"); type: AnyVODEnums.SK_LOWER_MUSIC; category: qsTr("효과"); check: true }
    ListElement { desc: qsTr("출력 장치"); type: AnyVODEnums.SK_SPDIF_AUDIO_DEVICE_ORDER; category: qsTr("S/PDIF"); check: false }
    ListElement { desc: qsTr("인코딩"); type: AnyVODEnums.SK_USE_SPDIF_ENCODING_ORDER; category: qsTr("S/PDIF"); check: false }
    ListElement { desc: qsTr("샘플링 속도"); type: AnyVODEnums.SK_SPDIF_SAMPLE_RATE_ORDER; category: qsTr("S/PDIF"); check: false }
    ListElement { desc: qsTr("사용 하기"); type: AnyVODEnums.SK_USE_SPDIF; category: qsTr("S/PDIF"); check: true }
}
