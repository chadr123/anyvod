﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12

Rectangle {
    property Flickable flickable: null
    readonly property real targetHeight: (flickable.height - anchors.topMargin - anchors.bottomMargin)
    readonly property real targetWidth: (flickable.width - anchors.leftMargin - anchors.rightMargin)

    property bool vertical: true
    property bool hideScrollBarsWhenStopped: true

    property int scrollbarWidth: 6
    property int scrollbarMargin: 3

    property real maximumOpacity: 0.75

    id: scrollbar
    color: "gray"
    radius: vertical ? width / 2 : height / 2
    visible: vertical ? flickable.height < flickable.contentHeight : flickable.width < flickable.contentWidth
    opacity: hideScrollBarsWhenStopped ? 0.0 : maximumOpacity
    width: vertical ? scrollbarWidth : Math.max(flickable.visibleArea.widthRatio * targetWidth - scrollbarMargin * 2, scrollbarWidth)
    height: vertical ? Math.max(flickable.visibleArea.heightRatio * targetHeight - scrollbarMargin * 2, scrollbarWidth) : scrollbarWidth
    x: vertical ? flickable.width - width - scrollbarMargin : flickable.visibleArea.xPosition * targetWidth + scrollbarMargin
    y: vertical ? flickable.visibleArea.yPosition * targetHeight + scrollbarMargin : flickable.height - height - scrollbarMargin

    Behavior on opacity {
        NumberAnimation { duration: 200 }
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        preventStealing: true
        drag.axis: vertical ? Drag.YAxis :Drag.XAxis
        drag.target: scrollbar
        drag.minimumX: vertical ? 0 : scrollbarMargin
        drag.maximumX: vertical ? 0 : flickable.width - scrollbar.width - scrollbarMargin
        drag.minimumY: vertical ? scrollbarMargin : 0
        drag.maximumY: vertical ? flickable.height - scrollbar.height - scrollbarMargin : 0

        onPositionChanged: {
            showScrollbar()

            if (vertical)
                flickable.contentY = (scrollbar.y - scrollbarMargin) * flickable.contentHeight / targetHeight
            else
                flickable.contentX = (scrollbar.x - scrollbarMargin) * flickable.contentWidth / targetWidth
        }
    }

    Timer {
        id: hideTimer
        interval: 1000

        onTriggered: {
            scrollbar.opacity = 0.0
        }
    }

    Connections {
        ignoreUnknownSignals: true
        target: flickable

        function onMovementStarted() {
            showScrollbar()
        }

        function onMovementEnded() {
            if (hideScrollBarsWhenStopped)
                hideScrollbar()
        }
    }

    function showScrollbar()
    {
        hideTimer.stop()
        scrollbar.opacity = maximumOpacity
    }

    function hideScrollbar()
    {
        hideTimer.restart()
    }
}
