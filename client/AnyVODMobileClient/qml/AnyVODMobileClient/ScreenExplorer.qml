﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.0
import com.dcple.anyvod 1.0

Rectangle {
    id: topRoot
    anchors.fill: parent
    focus: true
    color: "#88000000"

    property string filePath
    property ScreenExplorerListModel listModel: null

    property alias title: header.title

    signal goBack
    signal selected(double time)

    function exit()
    {
        listModel.clear()
        goBack()
    }

    function init()
    {
        var duration = listModel.getDuration(filePath)
        var fiveMinutes = 60.0 * 5.0
        var thirtySeconds = 30.0

        if (duration <= 0.0 || duration > fiveMinutes)
            minute.currentIndex = 5
        else if (duration <= fiveMinutes && duration > thirtySeconds)
            second.currentIndex = 30
        else
            second.currentIndex = 1

        updateList()
    }

    function updateList()
    {
        listModel.updateScreen(filePath, getTime())
    }

    function getTime()
    {
        return (hour.currentIndex * 3600) + (minute.currentIndex * 60) + second.currentIndex
    }

    DropShadow {
        anchors.fill: root
        radius: 16.0
        samples: 16
        color: "#80000000"
        source: root
    }

    MouseArea {
        anchors.fill: parent

        onWheel: wheel.accepted = true
        onClicked: exit()
        onCanceled: exit()
    }

    Rectangle {
        id: root
        anchors.centerIn: parent
        width: Math.min(parent.width * 0.95, 700)
        height: Math.min(parent.height * 0.95, 375)
        radius: 3
        border.color: "gray"

        MouseArea {
            anchors.fill: parent

            ColumnLayout {
                anchors.fill: parent
                spacing: 0

                Header {
                    id: header
                    Layout.fillWidth: true
                    Layout.minimumHeight: 50
                    Layout.maximumHeight: Layout.minimumHeight
                }

                Rectangle {
                    Layout.fillWidth: true
                    Layout.minimumHeight: 5
                    Layout.maximumHeight: Layout.minimumHeight
                }

                RowLayout {
                    spacing: 0
                    Layout.fillWidth: true
                    Layout.minimumHeight: 80
                    Layout.maximumHeight: Layout.minimumHeight

                    Rectangle {
                        Layout.fillHeight: true
                        Layout.minimumWidth: 10
                        Layout.maximumWidth: Layout.minimumWidth
                    }

                    GridLayout {
                        columns: 3
                        columnSpacing: 0

                        Text {
                            id: hourText
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            text: qsTr("시")
                            color: "green"
                            Layout.fillWidth: true
                        }

                        Text {
                            id: minuteText
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            text: qsTr("분")
                            color: "green"
                            Layout.fillWidth: true
                        }

                        Text {
                            id: secondText
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            text: qsTr("초")
                            color: "green"
                            Layout.fillWidth: true
                        }

                        WheelPicker {
                            id: hour
                            model: 11
                            Layout.fillHeight: true
                            Layout.alignment: Qt.AlignHCenter
                        }

                        WheelPicker {
                            id: minute
                            model: 60
                            Layout.fillHeight: true
                            Layout.alignment: Qt.AlignHCenter
                        }

                        WheelPicker {
                            id: second
                            model: 60
                            Layout.fillHeight: true
                            Layout.alignment: Qt.AlignHCenter
                        }
                    }

                    Rectangle {
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                    }

                    Rectangle {
                        color: "gray"
                        Layout.minimumWidth: update.width + 20
                        Layout.maximumWidth: Layout.minimumWidth
                        Layout.minimumHeight: parent.height * 0.7 - 20
                        Layout.maximumHeight: Layout.minimumHeight
                        Layout.alignment: Qt.AlignHCenter

                        ImageButton {
                            id: update
                            anchors.horizontalCenter: parent.horizontalCenter
                            height: parent.height
                            text.text: qsTr("갱신")
                            text.color: "white"
                            text.verticalAlignment: Text.AlignVCenter
                            text.horizontalAlignment: Text.AlignHCenter

                            onClicked: {
                                topRoot.updateList()
                            }
                        }
                    }

                    Rectangle {
                        Layout.fillHeight: true
                        Layout.minimumWidth: 10
                        Layout.maximumWidth: Layout.minimumWidth
                    }
                }

                Rectangle {
                    Layout.fillWidth: true
                    Layout.minimumHeight: 10
                    Layout.maximumHeight: Layout.minimumHeight
                }

                Rectangle {
                    id: listRoot
                    clip: true
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    ListView {
                        id: list
                        anchors.fill: parent
                        orientation: ListView.Horizontal
                        model: listModel
                        spacing: 10

                        readonly property int itemSize: 220

                        delegate: Rectangle {
                            width: list.itemSize
                            height: list.height

                            Rectangle {
                                id: itemRoot
                                anchors.fill: parent
                                color: "transparent"

                                MouseArea {
                                    anchors.fill: parent

                                    ColumnLayout {
                                        anchors.fill: parent
                                        spacing: 0

                                        Image {
                                            fillMode: Image.PreserveAspectFit
                                            source: model.screen
                                            cache: false
                                            Layout.fillHeight: true
                                            Layout.minimumWidth: list.itemSize
                                            Layout.maximumWidth: Layout.minimumWidth
                                        }

                                        Rectangle {
                                            color: "transparent"
                                            Layout.fillWidth: true
                                            Layout.minimumHeight: 5
                                            Layout.maximumHeight: Layout.minimumHeight
                                        }

                                        Text {
                                            horizontalAlignment: Text.AlignHCenter
                                            text: model.timeString
                                            Layout.fillWidth: true
                                        }

                                        Rectangle {
                                            color: "transparent"
                                            Layout.fillWidth: true
                                            Layout.minimumHeight: 10
                                            Layout.maximumHeight: Layout.minimumHeight
                                        }
                                    }

                                    onClicked: {
                                        goBack()
                                        selected(model.time)
                                    }

                                    onPressed: {
                                        itemRoot.color = "lightgray"
                                        itemRoot.opacity = 0.5
                                    }

                                    onCanceled: {
                                        itemRoot.color = "transparent"
                                        itemRoot.opacity = 1.0
                                    }

                                    onReleased: {
                                        onCanceled()
                                    }
                                }
                            }
                        }
                    }

                    Scrollbar {
                        id: scrollbar
                        vertical: false
                        flickable: list
                    }
                }

                FooterWithButtons {
                    types: AnyVODEnums.BT_CLOSE
                    Layout.fillWidth: true
                    Layout.minimumHeight: 50
                    Layout.maximumHeight: Layout.minimumHeight

                    onCloseClicked: {
                        exit()
                    }
                }
            }
        }
    }

    Keys.onReleased: {
        if (event.key === Qt.Key_Back)
        {
            event.accepted = true

            topRoot.exit()
        }
    }
}
