﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.0
import com.dcple.anyvod 1.0

Rectangle {
    id: topRoot
    focus: true
    color: "#88000000"

    property alias windowWidth: root.width
    property alias windowHeight: root.height
    property alias title: header.title
    property alias url: url.text

    signal goBack
    signal accepted
    signal ignore

    function exit()
    {
        goBack()
        ignore()
    }

    function save()
    {
        url.finish()

        goBack()
        accepted()
    }

    VirtualKeyboardPanel {
        id: keyboard
    }

    DropShadow {
        anchors.fill: root
        radius: 16.0
        samples: 16
        color: "#80000000"
        source: root
    }

    MouseArea {
        anchors.fill: parent

        onWheel: wheel.accepted = true
        onClicked: exit()
        onCanceled: exit()
    }

    Rectangle {
        id: root
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 10
        width: parent.width * 0.95
        height: parent.height * 0.8
        radius: 3
        border.color: "gray"

        MouseArea {
            anchors.fill: parent

            ColumnLayout {
                anchors.fill: parent
                spacing: 0

                Header {
                    id: header
                    Layout.fillWidth: true
                    Layout.minimumHeight: 50
                    Layout.maximumHeight: Layout.minimumHeight
                }

                Rectangle {
                    id: itemRoot
                    color: "white"
                    clip: true
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    ColumnLayout {
                        anchors.fill: parent
                        anchors.topMargin: 5
                        anchors.bottomMargin: 5
                        anchors.leftMargin: 10
                        anchors.rightMargin: 10
                        spacing: 15

                        Text {
                            text: qsTr("id:password@ip:port 형태로 입력하세요.\n예시)\n    guest:mypass@192.168.0.10:8080\n    192.168.0.10:8080")
                            font.pixelSize: 15
                        }

                        Rectangle {
                            Layout.fillWidth: true
                            Layout.minimumHeight: 25
                            Layout.maximumHeight: Layout.minimumHeight

                            ContextMenuTextInput {
                                id: url
                                anchors.fill: parent
                                inputMethodHints: Qt.ImhUrlCharactersOnly

                                onFinished: {
                                    save()
                                }
                            }
                        }
                    }
                }

                FooterWithButtons {
                    types: AnyVODEnums.BT_SAVE | AnyVODEnums.BT_CANCEL
                    Layout.fillWidth: true
                    Layout.minimumHeight: 50
                    Layout.maximumHeight: Layout.minimumHeight

                    onSaveClicked: {
                        save()
                    }

                    onCancelClicked: {
                        exit()
                    }
                }
            }
        }
    }

    Keys.onReleased: {
        if (event.key === Qt.Key_Back)
        {
            event.accepted = true

            exit()
        }
    }
}
