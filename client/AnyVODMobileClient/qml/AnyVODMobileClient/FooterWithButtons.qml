﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import com.dcple.anyvod 1.0

ColumnLayout {
    spacing: 0

    property int types: AnyVODEnums.BT_NONE

    signal closeClicked
    signal openClicked
    signal cancelClicked
    signal okClicked
    signal newSaveClicked
    signal saveClicked
    signal reloadClicked
    signal powerOffClicked

    Rectangle {
        color: "gray"
        Layout.fillWidth: true
        Layout.minimumHeight: 1
        Layout.maximumHeight: Layout.minimumHeight
    }

    Rectangle {
        Layout.fillWidth: true
        Layout.minimumHeight: 50
        Layout.maximumHeight: Layout.minimumHeight

        RowLayout {
            anchors.fill: parent
            spacing: 10
            layoutDirection: Qt.RightToLeft

            ImageButton {
                id: openButton
                paddingValue: 20
                text.text: qsTr("열기")
                text.color: "green"
                text.verticalAlignment: Text.AlignVCenter
                text.horizontalAlignment: Text.AlignHCenter
                Layout.fillHeight: true

                onClicked: {
                    openClicked()
                }
            }

            ImageButton {
                id: okButton
                paddingValue: 20
                text.text: qsTr("확인")
                text.color: "green"
                text.verticalAlignment: Text.AlignVCenter
                text.horizontalAlignment: Text.AlignHCenter
                Layout.fillHeight: true

                onClicked: {
                    okClicked()
                }
            }


            ImageButton {
                id: closeButton
                paddingValue: 20
                text.text: qsTr("닫기")
                text.color: "green"
                text.verticalAlignment: Text.AlignVCenter
                text.horizontalAlignment: Text.AlignHCenter
                Layout.fillHeight: true

                onClicked: {
                    closeClicked()
                }
            }

            ImageButton {
                id: saveButton
                paddingValue: 20
                text.text: qsTr("저장")
                text.color: "green"
                text.verticalAlignment: Text.AlignVCenter
                text.horizontalAlignment: Text.AlignHCenter
                Layout.fillHeight: true

                onClicked: {
                    saveClicked()
                }
            }

            ImageButton {
                id: newSaveButton
                paddingValue: 20
                text.text: qsTr("신규 저장")
                text.color: "green"
                text.verticalAlignment: Text.AlignVCenter
                text.horizontalAlignment: Text.AlignHCenter
                Layout.fillHeight: true

                onClicked: {
                    newSaveClicked()
                }
            }

            ImageButton {
                id: cancelButton
                paddingValue: 20
                text.text: qsTr("취소")
                text.color: "green"
                text.verticalAlignment: Text.AlignVCenter
                text.horizontalAlignment: Text.AlignHCenter
                Layout.fillHeight: true

                onClicked: {
                    cancelClicked()
                }
            }

            ImageButton {
                id: reloadButton
                paddingValue: 20
                text.text: qsTr("갱신")
                text.color: "green"
                text.verticalAlignment: Text.AlignVCenter
                text.horizontalAlignment: Text.AlignHCenter
                Layout.fillHeight: true

                onClicked: {
                    reloadClicked()
                }
            }

            ImageButton {
                id: powerOffButton
                paddingValue: 20
                text.text: qsTr("전원 끄기")
                text.color: "green"
                text.verticalAlignment: Text.AlignVCenter
                text.horizontalAlignment: Text.AlignHCenter
                Layout.fillHeight: true

                onClicked: {
                    powerOffClicked()
                }
            }

            Rectangle {
                color: "transparent"
                Layout.fillWidth: true
                Layout.fillHeight: true
            }
        }
    }

    onTypesChanged: {
        closeButton.visible = (types & AnyVODEnums.BT_CLOSE) === AnyVODEnums.BT_CLOSE
        openButton.visible = (types & AnyVODEnums.BT_OPEN) === AnyVODEnums.BT_OPEN
        cancelButton.visible = (types & AnyVODEnums.BT_CANCEL) === AnyVODEnums.BT_CANCEL
        okButton.visible = (types & AnyVODEnums.BT_OK) === AnyVODEnums.BT_OK
        newSaveButton.visible = (types & AnyVODEnums.BT_NEW_SAVE) === AnyVODEnums.BT_NEW_SAVE
        saveButton.visible = (types & AnyVODEnums.BT_SAVE) === AnyVODEnums.BT_SAVE
        reloadButton.visible = (types & AnyVODEnums.BT_RELOAD) === AnyVODEnums.BT_RELOAD
        powerOffButton.visible = (types & AnyVODEnums.BT_POWER_OFF) === AnyVODEnums.BT_POWER_OFF
    }
}
