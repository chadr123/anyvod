﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import com.dcple.anyvod 1.0
import "." as AnyVOD

Item {
    id: topRoot
    anchors.fill: parent
    focus: true

    property alias title: header.title

    readonly property variant bands: ["31Hz", "62Hz", "125Hz", "250Hz", "500Hz", "1kHz", "2kHz", "4kHz", "8kHz", "16kHz"]
    readonly property int settingListDefaultWidth: width * 0.8

    signal goBack

    function exit()
    {
        equalizerDelegate.savePreamp(preamp.value)
        equalizerDelegate.saveBand()

        goBack()
    }

    function reloadList()
    {
        listModel.clear()
        equalizerDelegate.initValue()

        bands.forEach(function(band) {
            listModel.append({ "band": band })
        })
    }

    EqualizerDelegate {
        id: equalizerDelegate
    }

    Component {
        id: presets

        ComboItem {
            listWidth: Math.min(settingListDefaultWidth, 300)
            title: qsTr("프리셋")
            model: ListModel {
                Component.onCompleted: {
                    var names = equalizerDelegate.getPresetNames()
                    var i = 0;

                    names.forEach(function(name) {
                        append({ "desc": name, "value": i++ })
                    })

                    defaultValue = equalizerDelegate.getCurrentIndex()
                }
            }

            onItemSelected: {
                equalizerDelegate.setPreset(index)
                reloadList()
            }
        }
    }

    ColumnLayout {
        id: root
        anchors.fill: parent
        spacing: 0

        function valueTodB(value)
        {
            var v = equalizerDelegate.valueTodB(value)

            return v.toFixed(1)
        }

        HeaderWithBack {
            id: header
            Layout.fillWidth: true
            Layout.minimumHeight: 50
            Layout.maximumHeight: Layout.minimumHeight

            onBackClicked: {
                topRoot.exit()
            }
        }

        Rectangle {
            Layout.fillWidth: true
            Layout.minimumHeight: 50
            Layout.maximumHeight: Layout.minimumHeight

            RowLayout {
                id: menu
                anchors.fill: parent
                spacing: 0

                Rectangle {
                    color: "transparent"
                    Layout.minimumWidth: 15
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.fillHeight: true
                }

                Rectangle {
                    id: presetBox
                    color: "gray"
                    Layout.minimumWidth: preset.width + 20
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.minimumHeight: parent.height - 20
                    Layout.maximumHeight: Layout.minimumHeight
                    Layout.alignment: Qt.AlignHCenter

                    ImageButton {
                        id: preset
                        anchors.horizontalCenter: parent.horizontalCenter
                        height: parent.height
                        text.text: qsTr("프리셋")
                        text.color: "white"
                        text.verticalAlignment: Text.AlignVCenter
                        text.horizontalAlignment: Text.AlignHCenter

                        onClicked: {
                            comboLoader.sourceComponent = presets
                            comboLoader.forceActiveFocus()
                        }
                    }
                }

                Rectangle {
                    color: "transparent"
                    Layout.minimumWidth: 15
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.fillHeight: true
                }

                Rectangle {
                    id: resetBox
                    color: "gray"
                    Layout.minimumWidth: reset.width + 20
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.minimumHeight: parent.height - 20
                    Layout.maximumHeight: Layout.minimumHeight
                    Layout.alignment: Qt.AlignHCenter

                    ImageButton {
                        id: reset
                        anchors.horizontalCenter: parent.horizontalCenter
                        height: parent.height
                        text.text: qsTr("초기화")
                        text.color: "white"
                        text.verticalAlignment: Text.AlignVCenter
                        text.horizontalAlignment: Text.AlignHCenter

                        onClicked: {
                            preamp.value = 0
                            equalizerDelegate.setPreset(0)
                            reloadList()
                        }
                    }
                }

                Rectangle {
                    color: "transparent"
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                }

                RowLayout {
                    id: useEqualizer
                    Layout.fillHeight: true

                    Text {
                        text: qsTr("이퀄라이저 사용")
                        verticalAlignment: Text.AlignVCenter
                        Layout.fillHeight: true
                    }

                    Rectangle {
                        color: "transparent"
                        Layout.minimumWidth: 0
                        Layout.maximumWidth: Layout.minimumWidth
                        Layout.fillHeight: true
                    }

                    ImageCheckButton {
                        checked: equalizerDelegate.isUse()
                        checkedSource: "assets/checked.png"
                        unCheckedSource: "assets/unchecked.png"
                        Layout.minimumWidth: 20
                        Layout.maximumWidth: Layout.minimumWidth
                        Layout.minimumHeight: 20
                        Layout.maximumHeight: Layout.minimumHeight
                        Layout.alignment: Qt.AlignHCenter

                        onToggled: {
                            equalizerDelegate.use(checked)
                        }
                    }
                }

                Rectangle {
                    color: "transparent"
                    Layout.minimumWidth: 10
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.fillHeight: true
                }
            }
        }

        Rectangle {
            Layout.fillWidth: true
            Layout.minimumHeight: 10
            Layout.maximumHeight: Layout.minimumHeight
        }

        Rectangle {
            Layout.fillWidth: true
            Layout.minimumHeight: 20
            Layout.maximumHeight: Layout.minimumHeight

            RowLayout {
                id: preampRoot
                anchors.fill: parent

                Rectangle {
                    color: "transparent"
                    Layout.minimumWidth: 10
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.fillHeight: true
                }

                Text {
                    verticalAlignment: Text.AlignVCenter
                    text: "PreAmp"
                    color: "gray"
                    Layout.fillHeight: true
                }

                Rectangle {
                    color: "transparent"
                    Layout.minimumWidth: 0
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.fillHeight: true
                }

                AnyVOD.Slider {
                    id: preamp
                    from: -150
                    to: 150
                    stepSize: 1
                    handleBorderWidth: 2
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    Component.onCompleted: {
                        var preamp = equalizerDelegate.getPreamp()

                        value = preamp
                        preampValue.text = root.valueTodB(value) + "dB"
                    }

                    onValueChanged: {
                        preampValue.text = root.valueTodB(value) + "dB"
                    }
                }

                Rectangle {
                    color: "transparent"
                    Layout.minimumWidth: 0
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.fillHeight: true
                }

                Text {
                    id: preampValue
                    verticalAlignment: Text.AlignVCenter
                    color: "gray"
                    Layout.fillHeight: true
                }

                Rectangle {
                    color: "transparent"
                    Layout.minimumWidth: 10
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.fillHeight: true
                }
            }
        }

        Rectangle {
            id: listRoot
            color: "white"
            Layout.fillWidth: true
            Layout.fillHeight: true

            ListView {
                id: list
                anchors.fill: parent
                orientation: ListView.Horizontal

                function itemWidth()
                {
                    var w
                    var count = listModel.rowCount()

                    if (count)
                        w = width / count
                    else
                        w = 0

                    return Math.max(60, w)
                }

                model: ListModel {
                    id: listModel

                    Component.onCompleted: {
                        reloadList()
                    }
                }

                delegate: Rectangle {
                    width: list.itemWidth()
                    height: list.height

                    Rectangle {
                        id: itemRoot
                        anchors.fill: parent
                        anchors.leftMargin: 20
                        anchors.rightMargin: 20
                        color: "white"

                        ColumnLayout {
                            anchors.fill: parent
                            spacing: 0

                            Rectangle {
                                color: "transparent"
                                Layout.fillWidth: true
                                Layout.minimumHeight: 20
                                Layout.maximumHeight: Layout.minimumHeight
                            }

                            Text {
                                id: equValue
                                horizontalAlignment: Text.AlignHCenter
                                color: "gray"
                                Layout.fillWidth: true
                                Layout.minimumHeight: 20
                                Layout.maximumHeight: Layout.minimumHeight
                            }

                            Rectangle {
                                color: "transparent"
                                Layout.fillWidth: true
                                Layout.minimumHeight: 0
                                Layout.maximumHeight: Layout.minimumHeight
                            }

                            AnyVOD.Slider {
                                id: equlItem
                                from: -150
                                to: 150
                                stepSize: 1
                                handleBorderWidth: 3
                                handleWidthRatio: 1.4
                                orientation: Qt.Vertical
                                Layout.minimumWidth: 17
                                Layout.maximumWidth: Layout.minimumWidth
                                Layout.fillHeight: true
                                Layout.alignment: Qt.AlignHCenter

                                Component.onCompleted: {
                                    var equl = equalizerDelegate.getBandGain(index)

                                    value = equl
                                    equValue.text = root.valueTodB(value) + "dB"
                                }

                                onValueChanged: {
                                    equalizerDelegate.setBandForSave(index, value)
                                    equValue.text = root.valueTodB(value) + "dB"
                                }
                            }

                            Rectangle {
                                color: "transparent"
                                Layout.fillWidth: true
                                Layout.minimumHeight: 0
                                Layout.maximumHeight: Layout.minimumHeight
                            }

                            Text {
                                id: equLabel
                                horizontalAlignment: Text.AlignHCenter
                                text: model.band
                                color: "gray"
                                Layout.fillWidth: true
                                Layout.minimumHeight: 20
                                Layout.maximumHeight: Layout.minimumHeight
                            }

                            Rectangle {
                                color: "transparent"
                                Layout.fillWidth: true
                                Layout.minimumHeight: 10
                                Layout.maximumHeight: Layout.minimumHeight
                            }
                        }

                    }
                }
            }

            Scrollbar {
                id: scrollbar
                vertical: false
                flickable: list
            }
        }
    }

    Loader {
        id: comboLoader
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true
        z: 10
        layer.enabled: true

        function exit()
        {
            root.forceActiveFocus()

            opacity = 0.0
            comboHideTimer.restart()
        }

        Timer {
            id: comboHideTimer
            interval: comboOpacityAni.duration

            onTriggered: {
                parent.visible = false
                parent.sourceComponent = undefined
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: comboOpacityAni
                duration: 200
                easing.type: Easing.OutQuad
            }
        }

        onLoaded: {
            visible = true
            opacity = 1.0
        }

        Connections {
            ignoreUnknownSignals: true
            target: comboLoader.item

            function onGoBack() {
                comboLoader.exit()
            }
        }
    }

    Keys.onReleased: {
        if (event.key === Qt.Key_Back)
        {
            event.accepted = true

            topRoot.exit()
        }
    }
}
