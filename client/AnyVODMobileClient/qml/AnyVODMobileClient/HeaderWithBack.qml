﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12
import QtQuick.Layouts 1.12

Rectangle {
    property alias title: title.textValue
    property alias back: back

    signal backClicked

    gradient: HeaderGradient {}

    RowLayout {
        anchors.fill: parent

        Rectangle {
            color: "transparent"
            Layout.minimumWidth: 10
            Layout.maximumWidth: Layout.minimumWidth
            Layout.fillHeight: true
        }

        ImageButton {
            id: back
            image.width: 20
            image.height: image.width
            source: "assets/back.png"
            Layout.minimumWidth: 30
            Layout.maximumWidth: Layout.minimumWidth
            Layout.fillHeight: true
            Layout.alignment: Qt.AlignHCenter

            onClicked: {
                backClicked()
            }
        }

        MarqueeText {
            id: title
            text.color: "white"
            text.verticalAlignment: Text.AlignVCenter
            text.elide: Text.ElideMiddle
            text.font.pixelSize: parent.height * 0.3
            Layout.fillWidth: true
            Layout.fillHeight: true
        }

        Rectangle {
            color: "transparent"
            Layout.minimumWidth: 10
            Layout.maximumWidth: Layout.minimumWidth
            Layout.fillHeight: true
        }
    }
}
