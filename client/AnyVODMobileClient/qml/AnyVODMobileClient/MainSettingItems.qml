﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12
import com.dcple.anyvod 1.0

ListModel {
    ListElement { desc: qsTr("AnyVOD 정보"); type: AnyVODEnums.SK_INFO; category: qsTr("일반"); check: false }
    ListElement { desc: qsTr("라이센스"); type: AnyVODEnums.SK_LICENSE; category: qsTr("일반"); check: false }
    ListElement { desc: qsTr("Languages"); type: AnyVODEnums.SK_LANG; category: qsTr("일반"); check: false }
    ListElement { desc: qsTr("글꼴"); type: AnyVODEnums.SK_FONT; category: qsTr("일반"); check: false }
    ListElement { desc: qsTr("앱 자동 업데이트 확인 주기"); type: AnyVODEnums.SK_CHECK_UPDATE_GAP; category: qsTr("일반"); check: false }
    ListElement { desc: qsTr("프록시 설정"); type: AnyVODEnums.SK_PROXY_INFO; category: qsTr("일반"); check: false }
    ListElement { desc: qsTr("원격 서버 설정"); type: AnyVODEnums.SK_OPEN_SERVER_SETTING; category: qsTr("일반"); check: false }
    ListElement { desc: qsTr("설정 불러오기"); type: AnyVODEnums.SK_LOAD_SETTINGS; category: qsTr("설정 관리"); check: false }
    ListElement { desc: qsTr("설정 저장하기"); type: AnyVODEnums.SK_SAVE_SETTINGS; category: qsTr("설정 관리"); check: false }
    ListElement { desc: qsTr("하드웨어 디코더 사용"); type: AnyVODEnums.SK_USE_HW_DECODER; category: qsTr("화면"); check: true }
    ListElement { desc: qsTr("저화질 모드 사용"); type: AnyVODEnums.SK_USE_LOW_QUALITY_MODE; category: qsTr("화면"); check: true }
    ListElement { desc: qsTr("프레임 드랍 사용"); type: AnyVODEnums.SK_USE_FRAME_DROP; category: qsTr("화면"); check: true }
    ListElement { desc: qsTr("앨범 자켓 보기"); type: AnyVODEnums.SK_SHOW_ALBUM_JACKET; category: qsTr("화면"); check: true }
    ListElement { desc: qsTr("확장자"); type: AnyVODEnums.SK_SELECT_CAPTURE_EXT_ORDER; category: qsTr("캡처"); check: false }
    ListElement { desc: qsTr("저장 디렉토리 설정"); type: AnyVODEnums.SK_SELECT_CAPTURE_DIRECTORY; category: qsTr("캡처"); check: false }
    ListElement { desc: qsTr("활성화 기준"); type: AnyVODEnums.SK_DEINTERLACE_METHOD_ORDER; category: qsTr("디인터레이스"); check: false }
    ListElement { desc: qsTr("알고리즘"); type: AnyVODEnums.SK_DEINTERLACE_ALGORITHM_ORDER; category: qsTr("디인터레이스"); check: false }
    ListElement { desc: qsTr("재생 위치 기억"); type: AnyVODEnums.SK_LAST_PLAY; category: qsTr("재생"); check: true }
    ListElement { desc: qsTr("키프레임 단위 이동"); type: AnyVODEnums.SK_SEEK_KEYFRAME; category: qsTr("재생"); check: true }
    ListElement { desc: qsTr("버퍼링 모드 사용"); type: AnyVODEnums.SK_USE_BUFFERING_MODE; category: qsTr("재생"); check: true }
    ListElement { desc: qsTr("재생 순서 설정"); type: AnyVODEnums.SK_PLAY_ORDER; category: qsTr("재생"); check: false }
    ListElement { desc: qsTr("보이기"); type: AnyVODEnums.SK_SUBTITLE_TOGGLE; category: qsTr("자막 / 가사"); check: true }
    ListElement { desc: qsTr("고급 검색"); type: AnyVODEnums.SK_SEARCH_SUBTITLE_COMPLEX; category: qsTr("자막 / 가사"); check: true }
    ListElement { desc: qsTr("자막 캐시 모드 사용"); type: AnyVODEnums.SK_SUBTITLE_CACHE_MODE; category: qsTr("자막 / 가사"); check: true }
    ListElement { desc: qsTr("가로 정렬"); type: AnyVODEnums.SK_SUBTITLE_HALIGN_ORDER; category: qsTr("자막 / 가사"); check: false }
    ListElement { desc: qsTr("세로 정렬"); type: AnyVODEnums.SK_SUBTITLE_VALIGN_ORDER; category: qsTr("자막 / 가사"); check: false }
    ListElement { desc: qsTr("인코딩"); type: AnyVODEnums.SK_OPEN_TEXT_ENCODING; category: qsTr("자막 / 가사"); check: false }
    ListElement { desc: qsTr("자막 찾기"); type: AnyVODEnums.SK_ENABLE_SEARCH_SUBTITLE; category: qsTr("자막 / 가사"); check: true }
    ListElement { desc: qsTr("가사 찾기"); type: AnyVODEnums.SK_ENABLE_SEARCH_LYRICS; category: qsTr("자막 / 가사"); check: true }
    ListElement { desc: qsTr("찾은 가사 자동 저장"); type: AnyVODEnums.SK_AUTO_SAVE_SEARCH_LYRICS; category: qsTr("자막 / 가사"); check: true }
    ListElement { desc: qsTr("미디어 삭제 시 자막/가사 삭제"); type: AnyVODEnums.SK_DELETE_MEDIA_WITH_SUBTITLE; category: qsTr("자막 / 가사"); check: true }
    ListElement { desc: qsTr("오디오 장치"); type: AnyVODEnums.SK_AUDIO_DEVICE_ORDER; category: qsTr("소리"); check: false }
    ListElement { desc: qsTr("노멀라이저 사용"); type: AnyVODEnums.SK_AUDIO_NORMALIZE; category: qsTr("소리"); check: true }
    ListElement { desc: qsTr("이퀄라이저 사용"); type: AnyVODEnums.SK_AUDIO_EQUALIZER; category: qsTr("소리"); check: true }
    ListElement { desc: qsTr("블루투스 싱크"); type: AnyVODEnums.SK_BLUETOOTH_AUDIO_SYNC; category: qsTr("소리"); check: false }
    ListElement { desc: qsTr("출력 장치"); type: AnyVODEnums.SK_SPDIF_AUDIO_DEVICE_ORDER; category: qsTr("S/PDIF"); check: false }
    ListElement { desc: qsTr("인코딩"); type: AnyVODEnums.SK_USE_SPDIF_ENCODING_ORDER; category: qsTr("S/PDIF"); check: false }
    ListElement { desc: qsTr("샘플링 속도"); type: AnyVODEnums.SK_SPDIF_SAMPLE_RATE_ORDER; category: qsTr("S/PDIF"); check: false }
    ListElement { desc: qsTr("사용 하기"); type: AnyVODEnums.SK_USE_SPDIF; category: qsTr("S/PDIF"); check: true }
    ListElement { desc: qsTr("언어"); type: AnyVODEnums.SK_KEYBOARD_LANGUAGE; category: qsTr("키보드"); check: false }
}
