﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12
import com.dcple.anyvod 1.0

ListModel {
    ListElement { desc: qsTr("재생 위치 기억"); type: AnyVODEnums.SK_LAST_PLAY; category: qsTr("재생"); check: true }
    ListElement { desc: qsTr("키프레임 단위 이동"); type: AnyVODEnums.SK_SEEK_KEYFRAME; category: qsTr("재생"); check: true }
    ListElement { desc: qsTr("버퍼링 모드 사용"); type: AnyVODEnums.SK_USE_BUFFERING_MODE; category: qsTr("재생"); check: true }
    ListElement { desc: qsTr("재생 속도 설정"); type: AnyVODEnums.SK_PLAYBACK_SPEED; category: qsTr("재생"); check: false }
    ListElement { desc: qsTr("재생 순서 설정"); type: AnyVODEnums.SK_PLAY_ORDER; category: qsTr("재생"); check: false }
    ListElement { desc: qsTr("구간 반복 설정"); type: AnyVODEnums.SK_REPEAT_RANGE; category: qsTr("재생"); check: false }
}
