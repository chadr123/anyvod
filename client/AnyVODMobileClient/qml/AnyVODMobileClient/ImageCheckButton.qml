﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12

Rectangle {
    id: button
    color: "transparent"

    signal clicked
    signal pressed
    signal released
    signal toggled
    signal canceled

    property alias image: img
    property Timer resetTimer: null
    property bool checked: false
    property bool transparentWhenDisabled: true
    property alias enabled: button.enabled
    property string checkedSource: ""
    property string unCheckedSource: ""

    Image {
        id: img
        anchors.fill: parent
        fillMode: Image.Stretch
        source: checked ? button.checkedSource : button.unCheckedSource
    }

    MouseArea {
        id: mouseArea
        enabled: button.enabled
        anchors.fill: button
        hoverEnabled: true

        onClicked: {
            if (resetTimer != null)
                resetTimer.restart()

            button.clicked()

            button.checked = !button.checked
            button.toggled()
        }

        onCanceled: {
            button.canceled()
            button.released()
        }

        onPressed: button.pressed()
        onReleased: button.released()
    }

    onPressed: {
        opacity = 0.5
    }

    onReleased: {
        opacity = 1.0
    }

    onEnabledChanged: {
        if (enabled)
        {
            opacity = 1.0
        }
        else
        {
            if (transparentWhenDisabled)
                opacity = 0.3
            else
                opacity = 1.0
        }
    }
}
