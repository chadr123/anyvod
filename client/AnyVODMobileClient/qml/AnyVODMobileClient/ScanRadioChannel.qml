﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.0
import com.dcple.anyvod 1.0
import "." as AnyVOD

Rectangle {
    id: topRoot
    anchors.fill: parent
    focus: true
    color: "#88000000"

    property int currentAdapter: delegate.getCurrentAdapterValue()
    property int currentDemodulatorType: delegate.getCurrentDemodulatorTypeValue()
    property int currentCountry: delegate.getCurrentCountryValue()

    property alias title: header.title

    readonly property int settingListDefaultWidth: width * 0.8

    signal goBack

    function exit()
    {
        goBack()
    }

    function updateChannelRange()
    {
        var exists = false

        exists = delegate.setChannelCountry(currentCountry, currentDemodulatorType) && delegate.getChannelCount() > 0

        if (exists)
        {
            var first = delegate.getChannelFirst()
            var last = delegate.getChannelLast()

            start.from = first
            start.to = last

            end.from = first
            end.to = last

            start.value = first
            end.value = last

            scan.enabled = true
            stop.enabled = false
        }
        else
        {
            start.from = 0
            start.to = 0

            end.from = 0
            end.to = 0

            start.value = 0
            end.value = 0

            scan.enabled = false
            stop.enabled = false
        }
    }

    function adjustButtons()
    {
        if (start.value <= end.value)
        {
            scan.enabled = true
            stop.enabled = false
        }
        else
        {
            scan.enabled = false
            stop.enabled = false
        }
    }

    function restoreControls()
    {
        stop.enabled = false
        scan.enabled = true
        adapter.enabled = true
        demodulatorType.enabled = true
        start.enabled = true
        end.enabled = true
        country.enabled = true
        save.enabled = true
        signalStrength.value = 0
    }

    VirtualKeyboardPanel {
        id: keyboard
    }

    DropShadow {
        anchors.fill: root
        radius: 16.0
        samples: 16
        color: "#80000000"
        source: root
    }

    MouseArea {
        anchors.fill: parent

        onWheel: wheel.accepted = true
        onClicked: exit()
        onCanceled: exit()
    }

    RadioDelegate {
        id: delegate

        RadioListModel {
            id: radioListModel
            objectName: "radioListModel"

            onScanFinished: {
                restoreControls()
            }

            onSignalStrength: {
                signalStrength.value = value
            }

            onScannedChannel: {
                curChannel.text = current.toString() + "(" + freq + "Hz)"
                foundChannelCount.text = total.toString()
            }

            onInitProgress: {
                progress.from = start
                progress.to = end
                progress.value = start
            }

            onSetProgress: {
                progress.value = value
            }
        }

        Component.onCompleted: {
            init()
        }
    }

    Component {
        id: adapters

        ComboItem {
            listWidth: Math.min(settingListDefaultWidth, 500)
            title: qsTr("어댑터")
            model: ListModel {
                Component.onCompleted: {
                    var infos = delegate.getAdapterInfos()
                    var values = delegate.getAdapterInfoValues()
                    var i = 0

                    infos.forEach(function(info) {
                        append({ "desc": info, "value": values[i++] })
                    })

                    defaultValue = currentAdapter
                }
            }

            onItemSelected: {
                currentAdapter = delegate.getAdapterInfoValue(index)
            }
        }
    }

    Component {
        id: demodulatorTypes

        ComboItem {
            listWidth: Math.min(settingListDefaultWidth, 300)
            title: qsTr("형식")
            model: ListModel {
                Component.onCompleted: {
                    var descs = delegate.getDemodulatorTypeInfoDescs()
                    var values = delegate.getDemodulatorTypeInfoValues()
                    var i = 0

                    descs.forEach(function(desc) {
                        append({ "desc": desc, "value": values[i++] })
                    })

                    defaultValue = currentDemodulatorType
                }
            }

            onItemSelected: {
                currentDemodulatorType = delegate.getDemodulatorTypeInfoValue(index)
                updateChannelRange()
            }
        }
    }

    Component {
        id: countries

        ComboItem {
            listWidth: Math.min(settingListDefaultWidth, 300)
            title: qsTr("국가")
            model: ListModel {
                Component.onCompleted: {
                    var descs = delegate.getCountryInfoDescs()
                    var values = delegate.getCountryInfoValues()
                    var i = 0

                    descs.forEach(function(desc) {
                        append({ "desc": desc, "value": values[i++] })
                    })

                    defaultValue = currentCountry
                }
            }

            onItemSelected: {
                currentCountry = delegate.getCountryInfoValue(index)
                updateChannelRange()
            }
        }
    }

    Rectangle {
        id: root
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 10
        width: Math.min(parent.width * 0.8, 700)
        height: Math.min(parent.height * 0.9, 400)
        radius: 3
        border.color: "gray"

        MouseArea {
            anchors.fill: parent

            ColumnLayout {
                anchors.fill: parent
                spacing: 0

                Header {
                    id: header
                    Layout.fillWidth: true
                    Layout.minimumHeight: 50
                    Layout.maximumHeight: Layout.minimumHeight
                }

                Rectangle {
                    Layout.fillWidth: true
                    Layout.minimumHeight: 50
                    Layout.maximumHeight: Layout.minimumHeight

                    RowLayout {
                        id: menu
                        anchors.fill: parent

                        Rectangle {
                            color: "transparent"
                            Layout.minimumWidth: 15
                            Layout.maximumWidth: Layout.minimumWidth
                            Layout.fillHeight: true
                        }

                        Rectangle {
                            id: adaptersBox
                            color: "gray"
                            Layout.minimumWidth: adapter.width + 20
                            Layout.maximumWidth: Layout.minimumWidth
                            Layout.minimumHeight: parent.height - 20
                            Layout.maximumHeight: Layout.minimumHeight
                            Layout.alignment: Qt.AlignHCenter

                            ImageButton {
                                id: adapter
                                anchors.horizontalCenter: parent.horizontalCenter
                                height: parent.height
                                text.text: qsTr("어댑터")
                                text.color: "white"
                                text.verticalAlignment: Text.AlignVCenter
                                text.horizontalAlignment: Text.AlignHCenter

                                onClicked: {
                                    comboLoader.sourceComponent = adapters
                                    comboLoader.forceActiveFocus()
                                }
                            }
                        }

                        Rectangle {
                            color: "transparent"
                            Layout.minimumWidth: 15
                            Layout.maximumWidth: Layout.minimumWidth
                            Layout.fillHeight: true
                        }

                        Rectangle {
                            id: demodulatorTypesBox
                            color: "gray"
                            Layout.minimumWidth: demodulatorType.width + 20
                            Layout.maximumWidth: Layout.minimumWidth
                            Layout.minimumHeight: parent.height - 20
                            Layout.maximumHeight: Layout.minimumHeight
                            Layout.alignment: Qt.AlignHCenter

                            ImageButton {
                                id: demodulatorType
                                anchors.horizontalCenter: parent.horizontalCenter
                                height: parent.height
                                text.text: qsTr("형식")
                                text.color: "white"
                                text.verticalAlignment: Text.AlignVCenter
                                text.horizontalAlignment: Text.AlignHCenter

                                onClicked: {
                                    comboLoader.sourceComponent = demodulatorTypes
                                    comboLoader.forceActiveFocus()
                                }
                            }
                        }

                        Rectangle {
                            color: "transparent"
                            Layout.minimumWidth: 15
                            Layout.maximumWidth: Layout.minimumWidth
                            Layout.fillHeight: true
                        }

                        Rectangle {
                            id: countriesBox
                            color: "gray"
                            Layout.minimumWidth: country.width + 20
                            Layout.maximumWidth: Layout.minimumWidth
                            Layout.minimumHeight: parent.height - 20
                            Layout.maximumHeight: Layout.minimumHeight
                            Layout.alignment: Qt.AlignHCenter

                            ImageButton {
                                id: country
                                anchors.horizontalCenter: parent.horizontalCenter
                                height: parent.height
                                text.text: qsTr("국가")
                                text.color: "white"
                                text.verticalAlignment: Text.AlignVCenter
                                text.horizontalAlignment: Text.AlignHCenter

                                onClicked: {
                                    comboLoader.sourceComponent = countries
                                    comboLoader.forceActiveFocus()
                                }
                            }
                        }

                        Rectangle {
                            color: "transparent"
                            Layout.fillWidth: true
                            Layout.fillHeight: true
                        }
                    }
                }

                Rectangle {
                    Layout.fillWidth: true
                    Layout.minimumHeight: 50
                    Layout.maximumHeight: Layout.minimumHeight

                    RowLayout {
                        id: scanChannel
                        anchors.fill: parent

                        Rectangle {
                            color: "transparent"
                            Layout.minimumWidth: 10
                            Layout.maximumWidth: Layout.minimumWidth
                            Layout.fillHeight: true
                        }

                        Text {
                            text: qsTr("채널 범위")
                        }

                        Rectangle {
                            color: "transparent"
                            Layout.minimumWidth: 5
                            Layout.maximumWidth: Layout.minimumWidth
                            Layout.fillHeight: true
                        }

                        AnyVOD.SpinBox {
                            id: start
                            value: 0
                            from: 0
                            to: 0
                            fontSize: 12
                            indicatorWidth: 30
                            Layout.minimumWidth: 100
                            Layout.maximumWidth: Layout.minimumWidth
                            Layout.minimumHeight: 30
                            Layout.maximumHeight: Layout.minimumHeight
                            Layout.alignment: Qt.AlignHCenter

                            onValueChanged: {
                                adjustButtons()
                            }
                        }

                        Rectangle {
                            color: "transparent"
                            Layout.minimumWidth: 5
                            Layout.maximumWidth: Layout.minimumWidth
                            Layout.fillHeight: true
                        }

                        Text {
                            text: "~"
                        }

                        Rectangle {
                            color: "transparent"
                            Layout.minimumWidth: 5
                            Layout.maximumWidth: Layout.minimumWidth
                            Layout.fillHeight: true
                        }

                        AnyVOD.SpinBox {
                            id: end
                            value: 0
                            from: 0
                            to: 0
                            fontSize: 12
                            indicatorWidth: 30
                            Layout.minimumWidth: 100
                            Layout.maximumWidth: Layout.minimumWidth
                            Layout.minimumHeight: 30
                            Layout.maximumHeight: Layout.minimumHeight
                            Layout.alignment: Qt.AlignHCenter

                            onValueChanged: {
                                adjustButtons()
                            }
                        }

                        Rectangle {
                            color: "transparent"
                            Layout.minimumWidth: 20
                            Layout.maximumWidth: Layout.minimumWidth
                            Layout.fillHeight: true
                        }

                        Rectangle {
                            id: scanBox
                            color: "gray"
                            Layout.minimumWidth: scan.width + 20
                            Layout.maximumWidth: Layout.minimumWidth
                            Layout.minimumHeight: parent.height - 20
                            Layout.maximumHeight: Layout.minimumHeight
                            Layout.alignment: Qt.AlignHCenter

                            ImageButton {
                                id: scan
                                anchors.horizontalCenter: parent.horizontalCenter
                                height: parent.height
                                text.text: qsTr("검색")
                                text.color: "white"
                                text.verticalAlignment: Text.AlignVCenter
                                text.horizontalAlignment: Text.AlignHCenter

                                onClicked: {
                                    enabled = false
                                    stop.enabled = true

                                    adapter.enabled = false
                                    demodulatorType.enabled = false
                                    start.enabled = false
                                    end.enabled = false
                                    country.enabled = false
                                    save.enabled = false

                                    signalStrength.value = 0
                                    curChannel.text = "0"
                                    foundChannelCount.text = "0"

                                    delegate.scan(currentAdapter, currentDemodulatorType, currentCountry, start.value, end.value)
                                }
                            }
                        }

                        Rectangle {
                            color: "transparent"
                            Layout.minimumWidth: 10
                            Layout.maximumWidth: Layout.minimumWidth
                            Layout.fillHeight: true
                        }

                        Rectangle {
                            id: stopBox
                            color: "gray"
                            Layout.minimumWidth: scan.width + 20
                            Layout.maximumWidth: Layout.minimumWidth
                            Layout.minimumHeight: parent.height -20
                            Layout.maximumHeight: Layout.minimumHeight
                            Layout.alignment: Qt.AlignHCenter

                            ImageButton {
                                id: stop
                                anchors.horizontalCenter: parent.horizontalCenter
                                height: parent.height
                                text.text: qsTr("중지")
                                text.color: "white"
                                text.verticalAlignment: Text.AlignVCenter
                                text.horizontalAlignment: Text.AlignHCenter
                                enabled: false

                                onClicked: {
                                    delegate.stop()
                                    restoreControls()
                                }
                            }
                        }

                        Rectangle {
                            color: "transparent"
                            Layout.minimumWidth: 10
                            Layout.maximumWidth: Layout.minimumWidth
                            Layout.fillHeight: true
                        }

                        Rectangle {
                            id: saveBox
                            color: "gray"
                            Layout.minimumWidth: save.width + 20
                            Layout.maximumWidth: Layout.minimumWidth
                            Layout.minimumHeight: parent.height - 20
                            Layout.maximumHeight: Layout.minimumHeight
                            Layout.alignment: Qt.AlignHCenter

                            ImageButton {
                                id: save
                                anchors.horizontalCenter: parent.horizontalCenter
                                height: parent.height
                                text.text: qsTr("저장")
                                text.color: "white"
                                text.verticalAlignment: Text.AlignVCenter
                                text.horizontalAlignment: Text.AlignHCenter

                                onClicked: {
                                    delegate.save()
                                    topRoot.exit()
                                }
                            }
                        }

                        Rectangle {
                            color: "transparent"
                            Layout.fillWidth: true
                            Layout.fillHeight: true
                        }
                    }
                }

                Rectangle {
                    Layout.fillWidth: true
                    Layout.minimumHeight: 50
                    Layout.maximumHeight: Layout.minimumHeight

                    RowLayout {
                        id: scanningChannel
                        anchors.fill: parent

                        Rectangle {
                            color: "transparent"
                            Layout.minimumWidth: 10
                            Layout.maximumWidth: Layout.minimumWidth
                            Layout.fillHeight: true
                        }

                        Text {
                            text: qsTr("현재 채널 : ")
                        }

                        Text {
                            id: curChannel
                            text: "0"
                        }

                        Text {
                            text: qsTr(", 찾은 채널 개수 : ")
                        }

                        Text {
                            id: foundChannelCount
                            text: "0"
                        }

                        Rectangle {
                            color: "transparent"
                            Layout.minimumWidth: 40
                            Layout.maximumWidth: Layout.minimumWidth
                            Layout.fillHeight: true
                        }

                        Text {
                            text: qsTr("신호 감도")
                        }

                        Rectangle {
                            color: "transparent"
                            Layout.minimumWidth: 5
                            Layout.maximumWidth: Layout.minimumWidth
                            Layout.fillHeight: true
                        }

                        AnyVOD.ProgressBar {
                            id: signalStrength
                            from: 0
                            to: 100
                            border: 1
                            Layout.fillWidth: true
                            Layout.minimumHeight: 20
                            Layout.maximumHeight: Layout.minimumHeight
                            Layout.alignment: Qt.AlignHCenter
                        }

                        Rectangle {
                            color: "transparent"
                            Layout.minimumWidth: 10
                            Layout.maximumWidth: Layout.minimumWidth
                            Layout.fillHeight: true
                        }
                    }
                }

                Rectangle {
                    Layout.fillWidth: true
                    Layout.minimumHeight: 20
                    Layout.maximumHeight: Layout.minimumHeight

                    RowLayout {
                        id: progressPanel
                        anchors.fill: parent

                        Rectangle {
                            color: "transparent"
                            Layout.minimumWidth: 10
                            Layout.maximumWidth: Layout.minimumWidth
                            Layout.fillHeight: true
                        }

                        AnyVOD.ProgressBar {
                            id: progress
                            from: 0
                            to: 100
                            border: 1
                            Layout.fillWidth: true
                            Layout.fillHeight: true
                            Layout.alignment: Qt.AlignHCenter
                        }

                        Rectangle {
                            color: "transparent"
                            Layout.minimumWidth: 10
                            Layout.maximumWidth: Layout.minimumWidth
                            Layout.fillHeight: true
                        }
                    }
                }

                Rectangle {
                    Layout.fillWidth: true
                    Layout.minimumHeight: 10
                    Layout.maximumHeight: Layout.minimumHeight
                }

                Rectangle {
                    id: listRoot
                    color: "white"
                    clip: true
                    Layout.fillHeight: true
                    Layout.fillWidth: true

                    ListView {
                        id: list
                        anchors.fill: parent
                        model: radioListModel

                        delegate: Rectangle {
                            width: list.width
                            height: 60

                            Rectangle {
                                id: itemRoot
                                anchors.fill: parent
                                anchors.leftMargin: 20
                                anchors.rightMargin: 20
                                color: "white"

                                ColumnLayout {
                                    anchors.fill: parent
                                    spacing: 0

                                    Text {
                                        text: qsTr("채널 ") + model.channel + qsTr(", 신호 감도 ") + model.signalStrength + "%"
                                    }

                                    Text {
                                        text: model.frequency + "Hz"
                                        color: "red"
                                    }

                                    Rectangle {
                                        color: "transparent"
                                        Layout.minimumWidth: 10
                                        Layout.maximumWidth: Layout.minimumWidth
                                        Layout.fillHeight: true
                                    }
                                }
                            }
                        }
                    }

                    Scrollbar {
                        id: scrollbar
                        flickable: list
                    }
                }

                FooterWithButtons {
                    types: AnyVODEnums.BT_CLOSE
                    Layout.fillWidth: true
                    Layout.minimumHeight: 50
                    Layout.maximumHeight: Layout.minimumHeight

                    onCloseClicked: {
                        exit()
                    }
                }
            }
        }
    }

    Loader {
        id: comboLoader
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true
        z: 10
        layer.enabled: true

        function exit()
        {
            root.forceActiveFocus()

            opacity = 0.0
            comboHideTimer.restart()
        }

        Timer {
            id: comboHideTimer
            interval: comboOpacityAni.duration

            onTriggered: {
                parent.visible = false
                parent.sourceComponent = undefined
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: comboOpacityAni
                duration: 200
                easing.type: Easing.OutQuad
            }
        }

        onLoaded: {
            visible = true
            opacity = 1.0
        }

        Connections {
            ignoreUnknownSignals: true
            target: comboLoader.item

            function onGoBack() {
                comboLoader.exit()
            }
        }
    }

    Component.onCompleted: {
        updateChannelRange()
    }

    Keys.onReleased: {
        if (event.key === Qt.Key_Back)
        {
            event.accepted = true

            topRoot.exit()
        }
    }
}
