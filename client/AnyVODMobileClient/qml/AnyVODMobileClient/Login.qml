﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.0
import com.dcple.anyvod 1.0

Rectangle {
    id: topRoot
    focus: true
    color: "#88000000"

    readonly property int settingListDefaultHeight: height * 0.8
    readonly property int settingListDefaultWidth: width * 0.8

    property alias windowWidth: root.width
    property alias windowHeight: root.height
    property alias title: header.title
    property alias id: id.text
    property alias password: password.text

    signal goBack
    signal accepted
    signal ignore

    function exit()
    {
        goBack()
        ignore()
    }

    function ok()
    {
        id.finish()
        password.finish()

        var inputID = id.text.trim()
        var inputPass = password.text.trim()

        if (inputID.length === 0)
        {
            messageBoxLoader.sourceComponent = inputIDError
            messageBoxLoader.forceActiveFocus()
        }
        else if (inputPass.length === 0)
        {
            messageBoxLoader.sourceComponent = inputPasswordError
            messageBoxLoader.forceActiveFocus()
        }
        else
        {
            if (socketDelegate.login(inputID, inputPass))
            {
                goBack()
                accepted()
            }
            else
            {
                messageBoxLoader.sourceComponent = loginError
                messageBoxLoader.forceActiveFocus()
            }
        }
    }

    Component {
        id: inputIDError

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("오류")
            message: qsTr("아이디를 입력해 주세요.")
        }
    }

    Component {
        id: inputPasswordError

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("오류")
            message: qsTr("비밀번호를 입력해 주세요.")
        }
    }

    Component {
        id: loginError

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 180)
            title: qsTr("오류")
            message: qsTr("서버에 로그인 할 수 없습니다.\n서버 주소, 아이디, 비밀번호가 올바른지 확인해 주세요.")
        }
    }

    SocketDelegate {
        id: socketDelegate
    }

    VirtualKeyboardPanel {
        id: keyboard
    }

    DropShadow {
        anchors.fill: root
        radius: 16.0
        samples: 16
        color: "#80000000"
        source: root
    }

    MouseArea {
        anchors.fill: parent

        onWheel: wheel.accepted = true
        onClicked: exit()
        onCanceled: exit()
    }

    Rectangle {
        id: root
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 10
        width: parent.width * 0.95
        height: parent.height * 0.8
        radius: 3
        border.color: "gray"

        MouseArea {
            anchors.fill: parent

            ColumnLayout {
                anchors.fill: parent
                spacing: 0

                Header {
                    id: header
                    Layout.fillWidth: true
                    Layout.minimumHeight: 50
                    Layout.maximumHeight: Layout.minimumHeight
                }

                Rectangle {
                    id: itemRoot
                    color: "white"
                    clip: true
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    GridLayout {
                        anchors.fill: parent
                        anchors.topMargin: 5
                        anchors.bottomMargin: 10
                        anchors.leftMargin: 10
                        anchors.rightMargin: 10
                        columns: 2
                        rowSpacing: 5
                        columnSpacing: 7

                        Text {
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignHCenter
                            text: qsTr("아이디와 비밀번호를 입력해주세요")
                            Layout.fillWidth: true
                            Layout.columnSpan: 2
                        }

                        Rectangle {
                            color: "transparent"
                            Layout.minimumHeight: 2
                            Layout.maximumHeight: Layout.minimumHeight
                            Layout.fillWidth: true
                            Layout.columnSpan: 2
                        }

                        Text {
                            verticalAlignment: Text.AlignVCenter
                            text: qsTr("아이디")
                        }

                        Rectangle {
                            Layout.fillWidth: true
                            Layout.minimumHeight: 25
                            Layout.maximumHeight: Layout.minimumHeight

                            ContextMenuTextInput {
                                id: id
                                anchors.fill: parent
                                inputMethodHints: Qt.ImhNone
                                maximumLength: socketDelegate.maxIDLength

                                onFinished: {
                                    ok()
                                }
                            }
                        }

                        Text {
                            verticalAlignment: Text.AlignVCenter
                            text: qsTr("비밀번호")
                        }

                        Rectangle {
                            Layout.fillWidth: true
                            Layout.minimumHeight: 25
                            Layout.maximumHeight: Layout.minimumHeight

                            ContextMenuTextInput {
                                id: password
                                anchors.fill: parent
                                echoMode: TextInput.Password
                                maximumLength: socketDelegate.maxPasswordLength

                                onFinished: {
                                    ok()
                                }
                            }
                        }
                    }
                }

                FooterWithButtons {
                    types: AnyVODEnums.BT_OK | AnyVODEnums.BT_CANCEL
                    Layout.fillWidth: true
                    Layout.minimumHeight: 50
                    Layout.maximumHeight: Layout.minimumHeight

                    onOkClicked: {
                        ok()
                    }

                    onCancelClicked: {
                        exit()
                    }
                }
            }
        }
    }

    Loader {
        id: messageBoxLoader
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true
        layer.enabled: true

        function exit()
        {
            topRoot.forceActiveFocus()

            opacity = 0.0
            messageBoxHideTimer.restart()
        }

        Timer {
            id: messageBoxHideTimer
            interval: messageBoxOpacityAni.duration

            onTriggered: {
                parent.visible = false
                parent.sourceComponent = undefined
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: messageBoxOpacityAni
                duration: 200
                easing.type: Easing.OutQuad
            }
        }

        onLoaded: {
            visible = true
            opacity = 1.0
        }

        Connections {
            ignoreUnknownSignals: true
            target: messageBoxLoader.item

            function onGoBack() {
                messageBoxLoader.exit()
            }
        }
    }

    Keys.onReleased: {
        if (event.key === Qt.Key_Back)
        {
            event.accepted = true

            exit()
        }
    }
}
