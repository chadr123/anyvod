﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.0
import com.dcple.anyvod 1.0
import "." as AnyVOD

Rectangle {
    id: topRoot
    anchors.fill: parent
    focus: true
    color: "#88000000"

    property alias title: header.title
    property alias channel: channel.text

    signal goBack

    function exit()
    {
        goBack()
    }

    DropShadow {
        anchors.fill: root
        radius: 16.0
        samples: 16
        color: "#80000000"
        source: root
    }

    MouseArea {
        anchors.fill: parent

        onWheel: wheel.accepted = true
        onClicked: exit()
        onCanceled: exit()
    }

    Timer {
        id: initTimer
        interval: 500

        onTriggered: {
            epgListModel.init()
        }
    }

    Timer {
        id: curUpdateTimer
        interval: 1000
        repeat: true

        onTriggered: {
            var date =  epgListModel.updateCurrentTime()

            curTime.text = date.toLocaleString(Qt.locale(), "yyyy-MM-dd hh:mm:ss")
        }
    }

    EPGListModel {
        id: epgListModel

        onItemSelected: {
            if (index !== -1)
            {
                updateToCurrentItem(index)
                list.contentX = list.itemSize * index
            }
            else
            {
                programTitle.text = " "
                period.text = " "
            }
        }

        onInitUpdateTimer: {
            curUpdateTimer.restart()
        }

        function updateToCurrentItem(i)
        {
            var mIndex = index(i, 0)

            programTitle.text = data(mIndex, EPGListModel.TitleRole)

            var start = data(mIndex, EPGListModel.StartTimeRole).toLocaleString(Qt.locale(), "yyyy-MM-dd hh:mm:ss")
            var end = data(mIndex, EPGListModel.EndTimeRole).toLocaleString(Qt.locale(), "hh:mm:ss")

            period.text = start + " ~ " + end
        }

        Component.onCompleted: {
            initTimer.restart()
        }
    }

    Rectangle {
        id: root
        anchors.centerIn: parent
        width: Math.min(parent.width * 0.8, 700)
        height: 375
        radius: 3
        border.color: "gray"

        MouseArea {
            anchors.fill: parent

            ColumnLayout {
                anchors.fill: parent
                spacing: 0

                Header {
                    id: header
                    Layout.fillWidth: true
                    Layout.minimumHeight: 50
                    Layout.maximumHeight: Layout.minimumHeight
                }

                Rectangle {
                    Layout.fillWidth: true
                    Layout.minimumHeight: 10
                    Layout.maximumHeight: Layout.minimumHeight
                }

                Rectangle {
                    Layout.fillWidth: true
                    Layout.minimumHeight: 120
                    Layout.maximumHeight: Layout.minimumHeight

                    Grid {
                        columns: 2
                        columnSpacing: 10
                        rowSpacing: 5
                        x: 10
                        width: parent.width - x * 2

                        Text {
                            text: qsTr("채널")
                        }

                        Text {
                            id: channel
                        }

                        Text {
                            text: qsTr("제목")
                        }

                        Text {
                            id: programTitle
                            text: qsTr("업데이트 중...")
                        }

                        Text {
                            text: qsTr("방송 시간")
                        }

                        Text {
                            id: period
                            text: qsTr("업데이트 중...")
                        }

                        Text {
                            text: qsTr("현재 시각")
                        }

                        Text {
                            id: curTime
                            text: qsTr("업데이트 중...")
                        }
                    }
                }

                Rectangle {
                    Layout.fillWidth: true
                    Layout.minimumHeight: 20
                    Layout.maximumHeight: Layout.minimumHeight
                }

                Rectangle {
                    id: listRoot
                    clip: true
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    ListView {
                        id: list
                        anchors.fill: parent
                        orientation: ListView.Horizontal
                        model: epgListModel

                        readonly property int itemSize: 220

                        delegate: Rectangle {
                            width: list.itemSize
                            height: list.height

                            Rectangle {
                                id: itemRoot
                                anchors.fill: parent
                                color: "transparent"

                                MouseArea {
                                    anchors.fill: parent

                                    ColumnLayout {
                                        anchors.fill: parent
                                        spacing: 10

                                        Text {
                                            text: model.startTime.toLocaleString(Qt.locale(), "[yyyy-MM-dd]")
                                            font.bold: true
                                            Layout.alignment: Qt.AlignHCenter
                                        }

                                        Text {
                                            text: model.title
                                            horizontalAlignment: Text.AlignHCenter
                                            elide: Text.ElideMiddle
                                            Layout.minimumWidth: parent.width - 20
                                            Layout.maximumWidth: Layout.minimumWidth
                                            Layout.alignment: Qt.AlignHCenter
                                        }

                                        RowLayout {
                                            spacing: 10
                                            Layout.alignment: Qt.AlignHCenter

                                            Text {
                                                text: model.startTime.toLocaleString(Qt.locale(), "hh:mm:ss")
                                            }

                                            AnyVOD.ProgressBar {
                                                id: progress
                                                border: 1
                                                Layout.minimumWidth: 50
                                                Layout.maximumWidth: Layout.minimumWidth
                                                Layout.minimumHeight: 10
                                                Layout.maximumHeight: Layout.minimumHeight
                                            }

                                            Text {
                                                text: model.endTime.toLocaleString(Qt.locale(), "hh:mm:ss")
                                            }

                                            Component.onCompleted: {
                                                var curDate = new Date()

                                                if (model.startTime <= curDate && model.endTime >= curDate)
                                                {
                                                    progress.from = 0
                                                    progress.to = model.duration
                                                    progress.value = (curDate.getTime() - model.startTime.getTime()) / 1000
                                                }
                                                else if (curDate < model.startTime)
                                                {
                                                    progress.from = 0
                                                    progress.to = 1
                                                    progress.value = 0
                                                }
                                                else
                                                {
                                                    progress.from = 0
                                                    progress.to = 1
                                                    progress.value = 1
                                                }
                                            }
                                        }

                                        Rectangle {
                                            color: "transparent"
                                            Layout.fillWidth: true
                                            Layout.fillHeight: true
                                        }
                                    }

                                    onClicked: {
                                        epgListModel.updateToCurrentItem(model.index)
                                    }

                                    onPressed: {
                                        itemRoot.color = "lightgray"
                                        itemRoot.opacity = 0.5
                                    }

                                    onCanceled: {
                                        itemRoot.color = "transparent"
                                        itemRoot.opacity = 1.0
                                    }

                                    onReleased: {
                                        onCanceled()
                                    }
                                }
                            }
                        }
                    }

                    Scrollbar {
                        id: scrollbar
                        vertical: false
                        flickable: list
                    }
                }

                FooterWithButtons {
                    types: AnyVODEnums.BT_CLOSE
                    Layout.fillWidth: true
                    Layout.minimumHeight: 50
                    Layout.maximumHeight: Layout.minimumHeight

                    onCloseClicked: {
                        exit()
                    }
                }
            }
        }
    }

    Keys.onReleased: {
        if (event.key === Qt.Key_Back)
        {
            event.accepted = true

            topRoot.exit()
        }
    }
}
