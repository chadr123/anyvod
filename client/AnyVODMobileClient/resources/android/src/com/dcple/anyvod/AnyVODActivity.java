/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

package com.dcple.anyvod;

import android.view.WindowManager;
import android.view.Window;
import android.view.KeyEvent;

import android.graphics.Color;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.provider.Settings;
import android.provider.MediaStore;

import android.telephony.TelephonyManager;
import android.telephony.PhoneStateListener;

import android.widget.Toast;
import android.net.Uri;
import android.database.Cursor;
import android.util.Log;
import android.Manifest;

import android.os.IBinder;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.Bundle;
import android.os.Build;
import android.os.Environment;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.ComponentName;
import android.content.res.Configuration;
import android.content.ActivityNotFoundException;

import android.app.PendingIntent;
import android.app.UiModeManager;
import android.app.Notification;
import android.app.ActivityManager.TaskDescription;

import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;

import androidx.core.content.ContextCompat;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.app.NotificationCompat;
import androidx.media.app.NotificationCompat.MediaStyle;

import androidx.mediarouter.media.MediaRouter;
import androidx.mediarouter.media.MediaRouter.RouteInfo;
import androidx.mediarouter.media.MediaRouteSelector;
import androidx.mediarouter.media.MediaControlIntent;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.qtproject.qt5.android.bindings.QtActivity;
import org.qtproject.qt5.android.bindings.QtApplication;

public class AnyVODActivity extends QtActivity
{
    public static final String ACTION_REMOTE_GENERIC = BuildConfig.APPLICATION_ID + ".remote.";
    public static final String ACTION_REMOTE_PLAY = ACTION_REMOTE_GENERIC + "Play";
    public static final String ACTION_REMOTE_TOGGLE = ACTION_REMOTE_GENERIC + "Toggle";
    public static final String ACTION_REMOTE_PAUSE = ACTION_REMOTE_GENERIC + "Pause";
    public static final String ACTION_REMOTE_STOP = ACTION_REMOTE_GENERIC + "Stop";
    public static final String ACTION_REMOTE_PREVIOUS = ACTION_REMOTE_GENERIC + "Previous";
    public static final String ACTION_REMOTE_NEXT = ACTION_REMOTE_GENERIC + "Next";
    public static final String ACTION_REMOTE_REWIND = ACTION_REMOTE_GENERIC + "Rewind";
    public static final String ACTION_REMOTE_FORWARD = ACTION_REMOTE_GENERIC + "Forward";

    private static final int REQUEST_PERMS = 1;
    private static final int NOTIFICATION_ID = 1;
    private static final String CHANNEL_ID = BuildConfig.APPLICATION_ID + "playback_channel" + NOTIFICATION_ID;
    private static final String[] NO_MEDIA_STYLE_MANUFACTURERS = {"huawei", "symphony teleca"};

    private WakeLock m_wakeLock = null;
    private MediaSessionCompat m_mediaSession = null;
    private PhoneStateListener m_phoneListener = null;
    private HeadsetPlugReceiver m_headPhonePlugReceiver = null;
    private BroadcastReceiver m_remoteReceiver = null;
    private RemoteControlReceiver m_remoteControlReceiver = null;
    private Intent m_preventKillServiceIntent = null;
    private PreventKillService m_preventKillService = null;
    private MediaRouter m_mediaRouter = null;
    private MediaRouter.Callback m_mediaRouterCallback = null;
    private ServiceConnection m_preventKillServiceConnection = null;
    private String m_startingURL = "";

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        this.handleIntent(this.getIntent(), false);

        final TelephonyManager tm = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);

        if (tm != null)
        {
            this.m_phoneListener = new PhoneStateListener()
            {
                private int m_currentState = tm.getCallState();

                @Override
                public void onCallStateChanged(int state, String incomingNumber)
                {
                    if (this.m_currentState == state)
                        return;

                    this.m_currentState = state;

                    switch (state)
                    {
                        case TelephonyManager.CALL_STATE_IDLE:
                            NativeFunctions.onResumeMediaOwnState();
                            break;
                        case TelephonyManager.CALL_STATE_OFFHOOK:
                        case TelephonyManager.CALL_STATE_RINGING:
                            NativeFunctions.onPauseMediaOwnState();
                            break;
                        default:
                            break;
                    }
                }
            };

            tm.listen(this.m_phoneListener, PhoneStateListener.LISTEN_CALL_STATE);
        }

        PowerManager pm = (PowerManager)this.getSystemService(Context.POWER_SERVICE);

        this.m_wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "AnyVOD_AWake_CPU");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            ArrayList<String> requiredPermissions = new ArrayList<String>();

            requiredPermissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            requiredPermissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            requiredPermissions.add(Manifest.permission.READ_PHONE_STATE);

            String[] perms = this.getPerms(requiredPermissions);

            if (perms != null && perms.length > 0)
                ActivityCompat.requestPermissions(this, perms, REQUEST_PERMS);

            String pkgName = this.getPackageName();

            if (!pm.isIgnoringBatteryOptimizations(pkgName))
            {
                Intent intent = new Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);

                intent.setData(Uri.parse("package:" + pkgName));

                try
                {
                    this.startActivity(intent);
                }
                catch (ActivityNotFoundException e) {}
            }
        }

        Utils.createChannel(this, CHANNEL_ID, Notification.VISIBILITY_PUBLIC);

        this.m_headPhonePlugReceiver = new HeadsetPlugReceiver(this);

        IntentFilter filter = new IntentFilter();

        filter.setPriority(Integer.MAX_VALUE);

        filter.addAction(ACTION_REMOTE_PLAY);
        filter.addAction(ACTION_REMOTE_TOGGLE);
        filter.addAction(ACTION_REMOTE_PAUSE);
        filter.addAction(ACTION_REMOTE_STOP);
        filter.addAction(ACTION_REMOTE_PREVIOUS);
        filter.addAction(ACTION_REMOTE_NEXT);
        filter.addAction(ACTION_REMOTE_REWIND);
        filter.addAction(ACTION_REMOTE_FORWARD);

        this.m_remoteReceiver = new BroadcastReceiver()
        {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                String action = intent.getAction();
                AnyVODActivity activity = AnyVODActivity.this;
                TelephonyManager tm = (TelephonyManager)activity.getSystemService(Context.TELEPHONY_SERVICE);

                if (tm != null && tm.getCallState() != TelephonyManager.CALL_STATE_IDLE)
                    return;

                if (action.equalsIgnoreCase(ACTION_REMOTE_TOGGLE))
                    NativeFunctions.onToggleMedia();
                else if (action.equalsIgnoreCase(ACTION_REMOTE_PLAY))
                    NativeFunctions.onResumeMedia();
                else if (action.equalsIgnoreCase(ACTION_REMOTE_PAUSE))
                    NativeFunctions.onPauseMedia();
                else if (action.equalsIgnoreCase(ACTION_REMOTE_STOP))
                    NativeFunctions.onStopMedia();
                else if (action.equalsIgnoreCase(ACTION_REMOTE_REWIND))
                    NativeFunctions.onRewindMedia();
                else if (action.equalsIgnoreCase(ACTION_REMOTE_FORWARD))
                    NativeFunctions.onForwardMedia();
                else if (action.equalsIgnoreCase(ACTION_REMOTE_PREVIOUS))
                    NativeFunctions.onPreviousMedia();
                else if (action.equalsIgnoreCase(ACTION_REMOTE_NEXT))
                    NativeFunctions.onNextMedia();
            }
        };
        this.registerReceiver(this.m_remoteReceiver, filter);

        filter = new IntentFilter();

        filter.setPriority(Integer.MAX_VALUE);
        filter.addAction(Intent.ACTION_MEDIA_BUTTON);

        this.m_remoteControlReceiver = new RemoteControlReceiver();
        this.registerReceiver(this.m_remoteControlReceiver, filter);

        this.setRemoteControlReceiverEnabled(false);
        this.hideNotification();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            int color = 0xff1f88f9;
            Bitmap bm = BitmapFactory.decodeResource(this.getResources(), R.drawable.icon);
            TaskDescription desc = new TaskDescription(null, bm, color);

            this.setTaskDescription(desc);
            bm.recycle();
        }

        this.m_preventKillServiceConnection = new ServiceConnection()
        {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service)
            {
                PreventKillService.LocalBinder binder = (PreventKillService.LocalBinder)service;
                AnyVODActivity activity = AnyVODActivity.this;

                activity.m_preventKillService = binder.getService();
                activity.updateServiceNotification(true);
            }

            @Override
            public void onServiceDisconnected(ComponentName name)
            {
                AnyVODActivity activity = AnyVODActivity.this;

                activity.m_preventKillService = null;
            }
        };
    }

    @Override
    public void onNewIntent(Intent intent)
    {
        super.onNewIntent(intent);

        this.setIntent(intent);
        this.handleIntent(intent, true);
    }

    @Override
    public void onPause()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
        {
            if (this.isInMultiWindowMode() && NativeFunctions.isVideo())
            {
                Object delegate = QtApplication.m_delegateObject;

                QtApplication.m_delegateObject = null;
                super.onPause();
                QtApplication.m_delegateObject = delegate;

                return;
            }
        }

        super.onPause();
    }

    @Override
    public void onDestroy()
    {
        this.hideNotification();
        this.stopPreventKillService();

        TelephonyManager tm = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);

        if (tm != null && this.m_phoneListener != null)
            tm.listen(this.m_phoneListener, PhoneStateListener.LISTEN_NONE);

        try
        {
            if (this.m_remoteReceiver != null)
                this.unregisterReceiver(this.m_remoteReceiver);

            if (this.m_remoteControlReceiver != null)
                this.unregisterReceiver(this.m_remoteControlReceiver);
        }
        catch (IllegalArgumentException e) {}

        this.unInitMediaSession();

        super.onDestroy();
    }

    private void handleIntent(Intent intent, boolean report)
    {
        String action = intent.getAction();
        String type = intent.getType();
        int flags = intent.getFlags();

        if ((flags & Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY) != 0)
            return;

        if (Intent.ACTION_SEND.equals(action) && type != null)
        {
            if (type.equals("text/plain"))
            {
                String url = intent.getStringExtra(Intent.EXTRA_TEXT);

                if (report)
                    NativeFunctions.setStartingURL(url);
                else
                    this.m_startingURL = url;
            }
        }
    }

    private void startPreventKillService()
    {
        if (this.m_preventKillServiceIntent != null)
            return;

        this.m_preventKillServiceIntent = new Intent(this, PreventKillService.class);

        ContextCompat.startForegroundService(this, this.m_preventKillServiceIntent);
        this.bindService(this.m_preventKillServiceIntent, this.m_preventKillServiceConnection, 0);
    }

    private void stopPreventKillService()
    {
        if (this.m_preventKillService != null)
            this.m_preventKillService.stopUpdate();

        if (this.m_preventKillServiceIntent != null)
        {
            this.unbindService(this.m_preventKillServiceConnection);
            this.stopService(this.m_preventKillServiceIntent);

            this.m_preventKillServiceIntent = null;
        }
    }

    private void showNotification(boolean isPlaying)
    {
        if (this.m_mediaSession == null)
            return;

        MediaMetadataCompat metaData = this.m_mediaSession.getController().getMetadata();
        String title = metaData.getString(MediaMetadataCompat.METADATA_KEY_TITLE);
        long trackIndex = metaData.getLong(MediaMetadataCompat.METADATA_KEY_TRACK_NUMBER) - 1;
        long trackCount = metaData.getLong(MediaMetadataCompat.METADATA_KEY_NUM_TRACKS);
        long duration = metaData.getLong(MediaMetadataCompat.METADATA_KEY_DURATION);
        Bitmap cover = metaData.getBitmap(MediaMetadataCompat.METADATA_KEY_ART);
        PendingIntent toggle = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_REMOTE_TOGGLE), PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent stop = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_REMOTE_STOP), PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent prev = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_REMOTE_PREVIOUS), PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent next = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_REMOTE_NEXT), PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent content = PendingIntent.getActivity(this, 0, new Intent(this, AnyVODActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);
        String timeFormat;

        if (duration >= 360000)
            timeFormat = "HH:mm:ss";
        else
            timeFormat = "mm:ss";

        DateFormat formatter = new SimpleDateFormat(timeFormat);
        String durString = formatter.format(new Date(duration));

        if (cover == null)
            cover = BitmapFactory.decodeResource(this.getResources(), R.drawable.icon_large);

        builder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
        builder.setContentTitle("[" + (trackIndex + 1) + " / " + trackCount + "] - " + durString);
        builder.setContentText(title);
        builder.setAutoCancel(false);
        builder.setOngoing(true);
        builder.setDeleteIntent(stop);
        builder.setContentIntent(content);
        builder.setLargeIcon(cover);

        boolean prevEnabled = false;
        boolean nextEnabled = false;

        if (trackCount <= 1)
        {
            prevEnabled = false;
            nextEnabled = false;
        }
        else if (trackIndex - 1 < 0)
        {
            prevEnabled = false;
            nextEnabled = true;
        }
        else if (trackIndex + 1 >= trackCount)
        {
            prevEnabled = true;
            nextEnabled = false;
        }
        else
        {
            prevEnabled = true;
            nextEnabled = true;
        }

        int[] buttonIndex = new int[prevEnabled && nextEnabled ? 3 : prevEnabled || nextEnabled ? 2 : 1];
        int prevIcon;
        int nextIcon;
        int resumeIcon;
        int pauseIcon;
        int smallIcon;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            prevIcon = R.drawable.prev;
            nextIcon = R.drawable.next;
            resumeIcon = R.drawable.resume;
            pauseIcon = R.drawable.pause;
            smallIcon = R.drawable.icon_white;
        }
        else
        {
            prevIcon = R.drawable.prev_white;
            nextIcon = R.drawable.next_white;
            resumeIcon = R.drawable.resume_white;
            pauseIcon = R.drawable.pause_white;
            smallIcon = R.drawable.icon;
        }

        builder.setSmallIcon(smallIcon);

        if (prevEnabled)
        {
            builder.addAction(new NotificationCompat.Action.Builder(prevIcon, "Previous", prev).build());
            buttonIndex[0] = 0;
        }

        if (isPlaying)
            builder.addAction(new NotificationCompat.Action.Builder(pauseIcon, "Pause", toggle).build());
        else
            builder.addAction(new NotificationCompat.Action.Builder(resumeIcon, "Resume", toggle).build());

        if (prevEnabled)
            buttonIndex[1] = 1;
        else
            buttonIndex[0] = 0;

        if (nextEnabled)
        {
            builder.addAction(new NotificationCompat.Action.Builder(nextIcon, "Next", next).build());

            if (prevEnabled)
                buttonIndex[2] = 2;
            else
                buttonIndex[1] = 1;
        }

        if (!this.isManufacturerBannedForMediaStyleNotifications())
        {
            builder.setStyle(new MediaStyle()
                            .setMediaSession(this.m_mediaSession.getSessionToken())
                            .setShowActionsInCompactView(buttonIndex)
                            .setShowCancelButton(true)
                            .setCancelButtonIntent(stop));
        }

        NotificationManagerCompat.from(this).notify(NOTIFICATION_ID, builder.build());
    }

    private void hideNotification()
    {
        NotificationManagerCompat.from(this).cancel(NOTIFICATION_ID);
    }

    private boolean isManufacturerBannedForMediaStyleNotifications()
    {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
        {
            for (String manufacturer : NO_MEDIA_STYLE_MANUFACTURERS)
            {
                if (Build.MANUFACTURER.toLowerCase(Locale.getDefault()).contains(manufacturer))
                    return true;
            }
        }

        return false;
    }

    private String[] getPerms(ArrayList<String> permList)
    {
        ArrayList<String> requiredPermissions = new ArrayList<String>();

        for (String perm : permList)
        {
            if (ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED)
                requiredPermissions.add(perm);
        }

        return requiredPermissions.toArray(new String[requiredPermissions.size()]);
    }

    private void setRemoteControlReceiverEnabled(boolean enabled)
    {
        this.getPackageManager().setComponentEnabledSetting(new ComponentName(this, RemoteControlReceiver.class),
            enabled ? PackageManager.COMPONENT_ENABLED_STATE_ENABLED : PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
            PackageManager.DONT_KILL_APP);
    }

    private void updateServiceNotification(boolean playOrPause)
    {
        if (this.m_preventKillService != null)
            this.m_preventKillService.updateNotification(playOrPause);
    }

    private void publishState(String state)
    {
        if (this.m_mediaSession == null)
            return;

        PlaybackStateCompat.Builder bob = new PlaybackStateCompat.Builder();
        long actions = PlaybackStateCompat.ACTION_PAUSE |
            PlaybackStateCompat.ACTION_PLAY |
            PlaybackStateCompat.ACTION_STOP |
            PlaybackStateCompat.ACTION_SKIP_TO_NEXT |
            PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS |
            PlaybackStateCompat.ACTION_FAST_FORWARD |
            PlaybackStateCompat.ACTION_REWIND;

        bob.setActions(actions);

        if (ACTION_REMOTE_PLAY.equals(state))
            bob.setState(PlaybackStateCompat.STATE_PLAYING, -1, 1);
        else if (ACTION_REMOTE_PAUSE.equals(state))
            bob.setState(PlaybackStateCompat.STATE_PAUSED, -1, 0);
        else if (ACTION_REMOTE_STOP.equals(state))
            bob.setState(PlaybackStateCompat.STATE_STOPPED, -1, 0);

        this.m_mediaSession.setPlaybackState(bob.build());
    }

    private void updateMetaData()
    {
        if (this.m_mediaSession == null)
            return;

        Bitmap cover = BitmapFactory.decodeFile(NativeFunctions.getCoverFilePath());
        MediaMetadataCompat.Builder bob = new MediaMetadataCompat.Builder();

        bob.putString(MediaMetadataCompat.METADATA_KEY_TITLE, NativeFunctions.getTitle());
        bob.putString(MediaMetadataCompat.METADATA_KEY_ARTIST, NativeFunctions.getArtist());
        bob.putLong(MediaMetadataCompat.METADATA_KEY_TRACK_NUMBER, NativeFunctions.getCurrentPlayIndex() + 1);
        bob.putLong(MediaMetadataCompat.METADATA_KEY_NUM_TRACKS, NativeFunctions.getTotalPlayListCount());
        bob.putLong(MediaMetadataCompat.METADATA_KEY_DURATION, NativeFunctions.getDuration());

        if (cover != null && cover.getConfig() != null)
            bob.putBitmap(MediaMetadataCompat.METADATA_KEY_ART, cover.copy(cover.getConfig(), false));

        this.m_mediaSession.setMetadata(bob.build());
    }

    private void initMediaSession()
    {
        this.unInitMediaSession();

        ComponentName mediaButtonEventReceiver = new ComponentName(this, RemoteControlReceiver.class);

        this.m_mediaSession = new MediaSessionCompat(this, "anyvod", mediaButtonEventReceiver, null);

        this.m_mediaSession.setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS | MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
        this.m_mediaSession.setCallback(new MediaSessionCompat.Callback()
        {
            private HeadsetHookHandler m_hookHandler = new HeadsetHookHandler();

            @Override
            public boolean onMediaButtonEvent(Intent mediaButtonIntent)
            {
                KeyEvent event = (KeyEvent)mediaButtonIntent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);

                if (event == null)
                    return false;

                int action = event.getAction();
                int keyCode = event.getKeyCode();
                AnyVODActivity activity = AnyVODActivity.this;

                switch (keyCode)
                {
                    case KeyEvent.KEYCODE_HEADSETHOOK:
                    case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
                        this.m_hookHandler.processKey(activity, action);
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onPlay()
            {
                NativeFunctions.onResumeMedia();
            }

            @Override
            public void onPause()
            {
                NativeFunctions.onPauseMedia();
            }

            @Override
            public void onStop()
            {
                NativeFunctions.onStopMedia();
            }

            @Override
            public void onSkipToNext()
            {
                NativeFunctions.onNextMedia();
            }

            @Override
            public void onSkipToPrevious()
            {
                NativeFunctions.onPreviousMedia();
            }

            @Override
            public void onFastForward()
            {
                NativeFunctions.onForwardMedia();
            }

            @Override
            public void onRewind()
            {
                NativeFunctions.onRewindMedia();
            }
        });

        try
        {
            this.m_mediaSession.setActive(true);
        }
        catch (NullPointerException e)
        {
            this.m_mediaSession.setActive(false);
            this.m_mediaSession.setFlags(MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
            this.m_mediaSession.setActive(true);
        }
    }

    private void unInitMediaSession()
    {
        if (this.m_mediaSession != null)
        {
            this.m_mediaSession.setActive(false);
            this.m_mediaSession.release();
            this.m_mediaSession = null;
        }
    }

    private String[] getMediaURLs(Uri uri, String field, String desc, String nameFilter)
    {
        ArrayList<String> list = new ArrayList<String>();
        String[] proj = { MediaStore.MediaColumns.DATA, MediaStore.MediaColumns.DATE_MODIFIED,
                          MediaStore.MediaColumns.MIME_TYPE, MediaStore.MediaColumns.SIZE,
                          MediaStore.MediaColumns.DISPLAY_NAME };
        String[] filterArg;
        String sortColumn;
        String filterColumn;

        if (field.equals("name"))
            sortColumn = MediaStore.MediaColumns.DISPLAY_NAME;
        else if (field.equals("time"))
            sortColumn = MediaStore.MediaColumns.DATE_MODIFIED;
        else if (field.equals("type"))
            sortColumn = MediaStore.MediaColumns.MIME_TYPE;
        else if (field.equals("size"))
            sortColumn = MediaStore.MediaColumns.SIZE;
        else
            sortColumn = MediaStore.MediaColumns.DISPLAY_NAME;

        if (nameFilter == null)
        {
            filterColumn = null;
            filterArg = null;
        }
        else
        {
            filterColumn = MediaStore.MediaColumns.DISPLAY_NAME + " like ?";
            filterArg = new String[] { "%" + nameFilter + "%" };
        }

        Cursor cursor = this.getApplicationContext().getContentResolver().query(uri, proj, filterColumn, filterArg, sortColumn + " " + desc);

        if (cursor != null)
        {
            if (cursor.moveToFirst())
            {
                int colIndex = cursor.getColumnIndex(MediaStore.MediaColumns.DATA);

                do
                {
                    String path = cursor.getString(colIndex);

                    list.add(path);
                }
                while (cursor.moveToNext());
            }

            cursor.close();
        }

        return list.toArray(new String[list.size()]);
    }

    public void prepair()
    {
        this.runOnUiThread(new Runnable()
        {
            public void run()
            {
                AnyVODActivity activity = AnyVODActivity.this;

                activity.m_mediaRouter = MediaRouter.getInstance(activity);

                if (activity.m_mediaRouter != null)
                {
                    MediaRouteSelector selector = new MediaRouteSelector.Builder()
                                                    .addControlCategory(MediaControlIntent.CATEGORY_LIVE_AUDIO)
                                                    .build();

                    activity.m_mediaRouterCallback = new MediaRouter.Callback()
                    {
                        @Override
                        public void onRouteSelected(MediaRouter router, RouteInfo route)
                        {
                            boolean isBluetoothConnected = route.isBluetooth() && !route.isDeviceSpeaker();

                            NativeFunctions.onAudioRouteSelected(isBluetoothConnected);
                        }
                    };
                    activity.m_mediaRouter.addCallback(selector, activity.m_mediaRouterCallback);
                }

                if (activity.m_headPhonePlugReceiver != null)
                {
                    activity.m_headPhonePlugReceiver.resetState();
                    activity.registerReceiver(activity.m_headPhonePlugReceiver, new IntentFilter(Intent.ACTION_HEADSET_PLUG));
                }
            }
        });
    }

    public void started()
    {
        this.runOnUiThread(new Runnable()
        {
            public void run()
            {
                if (!NativeFunctions.isValid())
                    return;

                boolean isVideo = NativeFunctions.isVideo();
                AnyVODActivity activity = AnyVODActivity.this;

                activity.setRemoteControlReceiverEnabled(!isVideo);

                if (isVideo)
                {
                    activity.hideNotification();
                    activity.stopPreventKillService();
                }
                else
                {
                    activity.initMediaSession();
                    activity.updateMetaData();
                    activity.startPreventKillService();
                }
            }
        });
    }

    public void playing()
    {
        this.runOnUiThread(new Runnable()
        {
            public void run()
            {
                AnyVODActivity activity = AnyVODActivity.this;

                activity.publishState(ACTION_REMOTE_PLAY);
                activity.showNotification(true);
                activity.updateServiceNotification(true);
            }
        });
    }

    public void paused()
    {
        this.runOnUiThread(new Runnable()
        {
            public void run()
            {
                AnyVODActivity activity = AnyVODActivity.this;

                activity.publishState(ACTION_REMOTE_PAUSE);
                activity.showNotification(false);
                activity.updateServiceNotification(false);
            }
        });
    }

    public void stopped()
    {
        this.runOnUiThread(new Runnable()
        {
            public void run()
            {
                AnyVODActivity activity = AnyVODActivity.this;

                activity.publishState(ACTION_REMOTE_STOP);
                activity.unInitMediaSession();
                activity.setRemoteControlReceiverEnabled(false);
            }
        });
    }

    public void exit()
    {
        this.runOnUiThread(new Runnable()
        {
            public void run()
            {
                AnyVODActivity activity = AnyVODActivity.this;

                activity.hideNotification();
                activity.stopPreventKillService();

                if (activity.m_mediaRouter != null)
                {
                    activity.m_mediaRouter.removeCallback(activity.m_mediaRouterCallback);
                    activity.m_mediaRouter = null;
                }

                try
                {
                    if (activity.m_headPhonePlugReceiver != null)
                        activity.unregisterReceiver(activity.m_headPhonePlugReceiver);
                }
                catch (IllegalArgumentException e) {}
            }
        });
    }

    public void setKeepActive()
    {
        this.runOnUiThread(new Runnable()
        {
            public void run()
            {
                AnyVODActivity activity = AnyVODActivity.this;

                activity.setKeepInActive();
                activity.m_wakeLock.acquire();
            }
        });
    }

    public void setKeepInActive()
    {
        this.runOnUiThread(new Runnable()
        {
            public void run()
            {
                AnyVODActivity activity = AnyVODActivity.this;

                if (activity.m_wakeLock.isHeld())
                    activity.m_wakeLock.release();
            }
        });
    }

    public void disableIdleTimer()
    {
        this.runOnUiThread(new Runnable()
        {
            public void run()
            {
                AnyVODActivity.this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }
        });
    }

    public void enableIdleTimer()
    {
        this.runOnUiThread(new Runnable()
        {
            public void run()
            {
                AnyVODActivity.this.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }
        });
    }

    public void showToast(final String msg)
    {
        this.runOnUiThread(new Runnable()
        {
            public void run()
            {
                Toast.makeText(AnyVODActivity.this.getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public int getToastLength()
    {
        return 2000;
    }

    public boolean setOrientationLandscape()
    {
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);

        return this.getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE;
    }

    public boolean setOrientationPortrait()
    {
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        return this.getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
    }

    public boolean setOrientationRestore()
    {
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

        return this.getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED;
    }

    public boolean isTablet()
    {
        UiModeManager uiModeManager = (UiModeManager)this.getSystemService(Context.UI_MODE_SERVICE);
        boolean isTV = uiModeManager.getCurrentModeType() == Configuration.UI_MODE_TYPE_TELEVISION;

        return this.getResources().getBoolean(R.bool.isTablet) || isTV;
    }

    public String[] getVideoURLs(String field, String desc, String nameFilter)
    {
        return this.getMediaURLs(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, field, desc, nameFilter);
    }

    public String[] getAudioURLs(String field, String desc, String nameFilter)
    {
        return this.getMediaURLs(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, field, desc, nameFilter);
    }

    public StorageVolume[] getRemovableVolumes()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
        {
            ArrayList<StorageVolume> retList = new ArrayList<StorageVolume>();
            StorageManager man = (StorageManager)this.getSystemService(Context.STORAGE_SERVICE);
            List<StorageVolume> list = man.getStorageVolumes();

            for (StorageVolume vol : list)
            {
                if (!vol.isRemovable())
                    continue;

                if (!(vol.getState().equals(Environment.MEDIA_MOUNTED) || vol.getState().equals(Environment.MEDIA_MOUNTED_READ_ONLY)))
                    continue;

                retList.add(vol);
            }

            return retList.toArray(new StorageVolume[retList.size()]);
        }
        else
        {
            return null;
        }
    }

    public void setStatusBarStyle(final boolean enable, final int color)
    {
        this.runOnUiThread(new Runnable()
        {
            public void run()
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                {
                    Window window = AnyVODActivity.this.getWindow();

                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

                    if (enable)
                        window.setStatusBarColor(Color.rgb((color & 0xff0000) >> 16, (color & 0x00ff00) >> 8, (color & 0x0000ff) >> 0));
                    else
                        window.setStatusBarColor(Color.BLACK);
                }
            }
        });
    }

    public void requestUpdateBluetoothHeadsetIsConnected()
    {
        this.runOnUiThread(new Runnable()
        {
            public void run()
            {
                boolean connected = AnyVODActivity.this.isBluetoothHeadsetConnected();

                NativeFunctions.onAudioRouteSelected(connected);
            }
        });
    }

    public boolean isBluetoothHeadsetConnected()
    {
        if (this.m_mediaRouter != null)
        {
            RouteInfo routeInfo = this.m_mediaRouter.getSelectedRoute();

            return routeInfo.isBluetooth() && !routeInfo.isDeviceSpeaker();
        }

        return false;
    }

    public String getStartingURL()
    {
        return this.m_startingURL;
    }

    public void setStartingURL(String url)
    {
        this.m_startingURL = url;
    }
}
