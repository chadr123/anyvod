/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

package com.dcple.anyvod;

public class NativeFunctions
{
    public static native void onResumeMediaOwnState();
    public static native void onPauseMediaOwnState();

    public static native void onResumeMedia();
    public static native void onPauseMedia();
    public static native void onToggleMedia();
    public static native void onStopMedia();
    public static native void onRewindMedia();
    public static native void onForwardMedia();
    public static native void onPreviousMedia();
    public static native void onNextMedia();

    public static native void onAudioRouteSelected(boolean isBluetooth);

    public static native boolean isVideo();
    public static native boolean isValid();

    public static native String getTitle();
    public static native String getArtist();
    public static native long getCurrentPosition();
    public static native long getDuration();
    public static native long getTotalPlayListCount();
    public static native long getCurrentPlayIndex();
    public static native String getCoverFilePath();

    public static native void setStartingURL(String url);
}
