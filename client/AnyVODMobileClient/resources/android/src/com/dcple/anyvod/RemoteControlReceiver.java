/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

package com.dcple.anyvod;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import android.view.KeyEvent;
import android.util.Log;

public class RemoteControlReceiver extends BroadcastReceiver
{
    private static HeadsetHookHandler m_hookHandler = new HeadsetHookHandler();

    @Override
    public void onReceive(Context context, Intent intent)
    {
        if (!intent.getAction().equals(Intent.ACTION_MEDIA_BUTTON))
            return;

        KeyEvent event = (KeyEvent)intent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);

        if (event == null)
            return;

        int action = event.getAction();
        int keyCode = event.getKeyCode();

        if (keyCode != KeyEvent.KEYCODE_HEADSETHOOK &&
            keyCode != KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE &&
            action != KeyEvent.ACTION_DOWN)
            return;

        Intent i = null;

        switch (keyCode)
        {
            case KeyEvent.KEYCODE_HEADSETHOOK:
            case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
                this.m_hookHandler.processKey(context, action);
                break;
            case KeyEvent.KEYCODE_MEDIA_PLAY:
                i = new Intent(AnyVODActivity.ACTION_REMOTE_PLAY);
                break;
            case KeyEvent.KEYCODE_MEDIA_PAUSE:
                i = new Intent(AnyVODActivity.ACTION_REMOTE_PAUSE);
                break;
            case KeyEvent.KEYCODE_MEDIA_STOP:
                i = new Intent(AnyVODActivity.ACTION_REMOTE_STOP);
                break;
            case KeyEvent.KEYCODE_MEDIA_NEXT:
                i = new Intent(AnyVODActivity.ACTION_REMOTE_NEXT);
                break;
            case KeyEvent.KEYCODE_MEDIA_PREVIOUS:
                i = new Intent(AnyVODActivity.ACTION_REMOTE_PREVIOUS);
                break;
            case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:
                i = new Intent(AnyVODActivity.ACTION_REMOTE_FORWARD);
                break;
            case KeyEvent.KEYCODE_MEDIA_REWIND:
                i = new Intent(AnyVODActivity.ACTION_REMOTE_REWIND);
                break;
            default:
                return;
        }

        if (this.isOrderedBroadcast())
            this.abortBroadcast();

        if (i != null)
            context.sendBroadcast(i);
    }
}
