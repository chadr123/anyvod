﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "RadioListItemUpdater.h"
#include "models/RadioListModel.h"

#include <QGuiApplication>

RadioListItemUpdater::RadioListItemUpdater(RadioListModel *parent) :
    m_parent(parent),
    m_reader(nullptr),
    m_adapter(-1),
    m_type(RadioReaderInterface::DT_NONE),
    m_country(QLocale::AnyCountry),
    m_start(-1),
    m_end(-1),
    m_stop(false)
{

}

void RadioListItemUpdater::setData(long adapter, RadioReaderInterface::DemodulatorType demodulatorType, QLocale::Country country,
                                 int start, int end)
{
    this->m_adapter = adapter;
    this->m_type = demodulatorType;
    this->m_country = country;
    this->m_start = start;
    this->m_end = end;
}

void RadioListItemUpdater::setReader(RadioReader *reader)
{
    this->m_reader = reader;
}

void RadioListItemUpdater::setStop(bool stop)
{
    this->m_stop = stop;
}

void RadioListItemUpdater::run()
{
    QVector<RadioChannelMap::Channel> channels;
    int startChannelIndex = 0;
    int endChannelIndex = 0;

    if (this->m_reader->setChannelCountry(this->m_country, this->m_type))
        this->m_reader->getChannels(&channels);

    for (int i = 0; i < channels.count(); i++)
    {
        if (channels[i].channel == this->m_start)
        {
            startChannelIndex = i;
            break;
        }
    }

    for (int i = channels.count() - 1; i >= 0; i--)
    {
        if (channels[i].channel == this->m_end)
        {
            endChannelIndex = i;
            break;
        }
    }

    QGuiApplication::postEvent(this->m_parent, new RadioListModel::InitProgressEvent(startChannelIndex, endChannelIndex));

    if (this->m_reader->prepareScanning(this->m_adapter, this->m_type))
    {
        for (int i = startChannelIndex; i <= endChannelIndex && i < channels.count(); i++)
        {
            const RadioChannelMap::Channel &channel = channels[i];
            RadioReader::ChannelInfo info;

            if (!this->m_parent->existScannedChannel(channel.channel))
            {
                bool ok = this->m_reader->scan(this->m_country, channel, i, &info);

                if (ok)
                {
                    info.index = i;
                    info.country = this->m_country;
                    info.freq = channel.freq;

                    QGuiApplication::postEvent(this->m_parent, new RadioListModel::AddItemEvent(info));
                }

                QGuiApplication::postEvent(this->m_parent, new RadioListModel::SignalStrengthEvent(this->m_reader->getSignalStrengthInPercentage(info.signal)));
                QGuiApplication::postEvent(this->m_parent, new RadioListModel::CurrentChannelEvent(info.channel, channel.freq));
            }

            QGuiApplication::postEvent(this->m_parent, new RadioListModel::SetProgressEvent(i));

            if (this->m_stop)
                break;
        }

        this->m_reader->finishScanning();
    }

    this->m_stop = false;

    QGuiApplication::postEvent(this->m_parent, new RadioListModel::ScanFinishedEvent());
}
