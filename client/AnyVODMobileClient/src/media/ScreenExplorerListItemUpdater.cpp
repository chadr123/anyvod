﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "ScreenExplorerListItemUpdater.h"
#include "models/ScreenExplorerListModel.h"
#include "media/FrameExtractor.h"

#include <QGuiApplication>

ScreenExplorerListItemUpdater::ScreenExplorerListItemUpdater(ScreenExplorerListModel *parent) :
    m_quit(true),
    m_parent(parent),
    m_second(5 * 60)
{

}

void ScreenExplorerListItemUpdater::startUpdate(const QString &filePath, int second)
{
    if (second <= 0)
        return;

    this->m_filePath = filePath;
    this->m_second = second;
    this->m_quit = false;

    this->start();
}

void ScreenExplorerListItemUpdater::stopUpdate()
{
    this->m_quit = true;
    this->wait();
}

void ScreenExplorerListItemUpdater::run()
{
    FrameExtractor ex;

    if (!ex.open(this->m_filePath, true))
    {
        QGuiApplication::postEvent(this->m_parent, new ScreenExplorerListModel::UpdateParentEvent());
        return;
    }

    for (double time = 0.0; time <= ex.getDuration() && !this->m_quit; time += this->m_second)
    {
        FrameExtractor::FrameItem item;

        if (ex.getFrame(time, false, &item))
            QGuiApplication::postEvent(this->m_parent, new ScreenExplorerListModel::AddItemEvent(item.time, item.rotation, item.frame));
    }

    ex.close();

    QGuiApplication::postEvent(this->m_parent, new ScreenExplorerListModel::UpdateParentEvent());
}
