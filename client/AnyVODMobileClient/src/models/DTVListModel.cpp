﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "DTVListModel.h"

const QEvent::Type DTVListModel::SCAN_FINISHED_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type DTVListModel::ADD_ITEM_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type DTVListModel::SIGNAL_STRENGTH_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type DTVListModel::CURRENT_CHANNEL_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type DTVListModel::INIT_PROGRESS_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type DTVListModel::SET_PROGRESS_EVENT = (QEvent::Type)QEvent::registerEventType();

DTVListModel::DTVListModel(QObject *parent) :
    QAbstractListModel(parent),
    m_updater(this)
{
    this->m_roleNames[ChannelRole] = "channel";
    this->m_roleNames[SignalStrengthRole] = "signalStrength";
    this->m_roleNames[NameRole] = "name";
}

void DTVListModel::setReader(DTVReader *reader)
{
    this->m_reader = reader;
    this->m_updater.setReader(reader);
}

void DTVListModel::update(long adapter, DTVReaderInterface::SystemType systemType, QLocale::Country country,
                          int start, int end)
{
    if (!this->m_updater.isRunning())
    {
        this->m_updater.setData(adapter, systemType, country, start, end);
        this->m_updater.setStop(false);

        this->m_updater.start();
    }
}

void DTVListModel::stop()
{
    this->m_updater.setStop(true);
    this->m_updater.wait();
}

void DTVListModel::clear()
{
    this->beginResetModel();
    this->m_channels.clear();
    this->endResetModel();
}

void DTVListModel::setChannelInfo(const QVector<DTVReader::ChannelInfo> &list)
{
    this->beginResetModel();
    this->m_channels = list;
    this->endResetModel();
}

QVector<DTVReader::ChannelInfo> DTVListModel::getChannelInfo() const
{
    return this->m_channels;
}

bool DTVListModel::existScannedChannel(int channel)
{
    for (const DTVReader::ChannelInfo &info : qAsConst(this->m_channels))
    {
        if (info.channel == channel)
            return true;
    }

    return false;
}

int DTVListModel::rowCount(const QModelIndex &parent) const
{
    (void)parent;

    return this->m_channels.count();
}

QVariant DTVListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    int row = index.row();

    if (row < 0 || row >= this->m_channels.count())
        return QVariant();

    QVariant ret;
    const DTVReader::ChannelInfo &item = this->m_channels[row];

    switch (role)
    {
        case ChannelRole:
        {
            ret = item.channel;
            break;
        }
        case SignalStrengthRole:
        {
            ret = (int)item.signal;
            break;
        }
        case NameRole:
        {
            ret = item.name;
            break;
        }
        default:
        {
            ret = QVariant();
            break;
        }
    }

    return ret;
}

QHash<int, QByteArray> DTVListModel::roleNames() const
{
    return this->m_roleNames;
}

void DTVListModel::customEvent(QEvent *event)
{
    if (event->type() == SCAN_FINISHED_EVENT)
    {
        emit this->scanFinished();
    }
    else if (event->type() == ADD_ITEM_EVENT)
    {
        AddItemEvent *e = (AddItemEvent*)event;
        int pos = this->m_channels.count();

        this->beginInsertRows(this->index(pos), pos, pos);
        this->m_channels.append(e->getInfo());
        this->endInsertRows();
    }
    else if (event->type() == SIGNAL_STRENGTH_EVENT)
    {
        SignalStrengthEvent *e = (SignalStrengthEvent*)event;

        emit this->signalStrength(e->getValue());
    }
    else if (event->type() == CURRENT_CHANNEL_EVENT)
    {
        CurrentChannelEvent *e = (CurrentChannelEvent*)event;

        emit this->scannedChannel(e->getChannel(), this->m_channels.count());
    }
    else if (event->type() == INIT_PROGRESS_EVENT)
    {
        InitProgressEvent *e = (InitProgressEvent*)event;

        emit this->initProgress(e->getStart(), e->getEnd());
    }
    else if (event->type() == SET_PROGRESS_EVENT)
    {
        SetProgressEvent *e = (SetProgressEvent*)event;

        emit this->setProgress(e->getValue());
    }
    else
    {
        QAbstractListModel::customEvent(event);
    }
}
