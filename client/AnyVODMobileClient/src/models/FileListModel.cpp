﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "FileListModel.h"
#include "setting/Settings.h"
#include "setting/SettingsFactory.h"
#include "ui/RenderScreen.h"
#include "ui/delegates/MainSettingDelegate.h"
#include "utils/ConvertingUtils.h"
#include "utils/MediaTypeUtils.h"

#ifdef Q_OS_ANDROID
# include <QAndroidJniEnvironment>
# include <QAndroidJniObject>
# include <QtAndroid>
#endif

#include <QDir>
#include <QSettings>
#include <QStandardPaths>
#include <QDebug>

const QEvent::Type FileListModel::UPDATE_ITEM_EVENT = (QEvent::Type)QEvent::registerEventType();
const QString FileListModel::DEFAULT_MOVIE_ICON = "assets/defaultMovie.png";
const QString FileListModel::DEFAULT_AUDIO_ICON = "assets/defaultAudio.png";
const QString FileListModel::DEFAULT_SUBTITLE_ICON = "assets/defaultSubtitle.png";
const QString FileListModel::DEFAULT_ETC_ICON = "assets/defaultETC.png";
const QString FileListModel::DEFAULT_DIRECTORY_ICON = "assets/dirClosed.png";
const QString FileListModel::OPEN_DIRECTORY_ICON = "assets/dir.png";

FileListModel::FileListModel(QObject *parent) :
    QAbstractListModel(parent),
    m_onlyDirectory(false),
    m_onlySubtitle(false),
    m_onlyETC(false),
    m_quickBarType(InternalType)
{
    this->m_roleNames[ThumbnailRole] = "thumbnail";
    this->m_roleNames[NameRole] = "name";
    this->m_roleNames[TypeRole] = "type";
    this->m_roleNames[PathRole] = "path";
    this->m_roleNames[FileInfoRole] = "fileInfo";
    this->m_roleNames[FileNameRole] = "fileName";
    this->m_roleNames[DirEntryCountRole] = "dirEntryCount";
    this->m_roleNames[DurationRole] = "duration";
    this->m_roleNames[CurPositionRole] = "curPosition";

    this->m_sortType[InternalType] = NameAscending;
    this->m_sortType[ExternalType] = NameAscending;
    this->m_sortType[VideoType] = TimeDescending;
    this->m_sortType[AudioType] = TimeDescending;
}

FileListModel::~FileListModel()
{

}

int FileListModel::rowCount(const QModelIndex &parent) const
{
    (void)parent;

    return this->m_curList.count();
}

QVariant FileListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    int row = index.row();

    if (row < 0 || row >= this->m_curList.count())
        return QVariant();

    FileListModel *me = (FileListModel*)this;
    QVariant ret;
    const Item &item = this->m_curList[row];

    switch (role)
    {
        case ThumbnailRole:
        {
            if (item.imgPath.isEmpty())
            {
                QFileInfo info(item.path);
                const QString defaultImg = DEFAULT_ETC_ICON;
                const QString dirImg = DEFAULT_DIRECTORY_ICON;

                if (info.isFile() && (MediaTypeUtils::isExtension(info.suffix(), MT_VIDEO) ||
                                      MediaTypeUtils::isExtension(info.suffix(), MT_AUDIO)))
                {
                    me->m_cache.requestToUpdate(item.path, index, role, this->m_signature);
                    ret = defaultImg;
                }
                else if (info.isFile() && MediaTypeUtils::isExtension(info.suffix(), MT_SUBTITLE))
                {
                    ret = DEFAULT_SUBTITLE_ICON;
                }
                else if (info.isDir())
                {
                    ret = dirImg;
                }
                else
                {
                    ret = defaultImg;
                }
            }
            else
            {
                if (item.imgPath.startsWith("assets"))
                    ret = item.imgPath;
                else
                    ret = "file://" + item.imgPath;
            }

            break;
        }
        case DirEntryCountRole:
        {
            ret = 0;

            if (item.dirEntryCount == -1)
            {
                QFileInfo info(item.path);

                if (info.isDir())
                    me->m_cache.requestToUpdate(item.path, index, role, this->m_signature);
            }
            else
            {
                ret = item.dirEntryCount;
            }

            break;
        }
        case FileInfoRole:
        {
            if (item.fileInfo.isEmpty())
            {
                QFileInfo info(item.path);

                if (info.isDir() || (info.isFile() && (MediaTypeUtils::isExtension(info.suffix(), MT_VIDEO) ||
                                                       MediaTypeUtils::isExtension(info.suffix(), MT_AUDIO))))
                {
                    me->m_cache.requestToUpdate(item.path, index, role, this->m_signature);
                    ret = QString();
                }
                else if (info.isFile())
                {
                    ret = ConvertingUtils::sizeToString(info.size()) + "B";
                }
            }
            else
            {
                ret = item.fileInfo;
            }

            break;
        }
        case FileNameRole:
        {
            ret = QFileInfo(item.path).fileName();
            break;
        }
        case NameRole:
        {
            QFileInfo info(item.path);

            if (info.isDir())
                ret = info.fileName();
            else
                ret = info.completeBaseName();

            break;
        }
        case TypeRole:
        {
            QFileInfo info(item.path);

            if (info.isDir())
                ret = Directory;
            else if (info.isFile())
                ret = File;

            break;
        }
        case PathRole:
        {
            ret = item.path;
            break;
        }
        case DurationRole:
        {
            if (item.duration < 0.0)
            {
                QFileInfo info(item.path);

                if (info.isFile() && (MediaTypeUtils::isExtension(info.suffix(), MT_VIDEO) ||
                                      MediaTypeUtils::isExtension(info.suffix(), MT_AUDIO)))
                {
                    me->m_cache.requestToUpdate(item.path, index, role, this->m_signature);
                    ret = -1.0;
                }
                else
                {
                    ret = 0.0;
                }
            }
            else
            {
                ret = item.duration;
            }

            break;
        }
        case CurPositionRole:
        {
            if (item.curPosition < 0.0)
            {
                QFileInfo info(item.path);

                if (info.isFile() && (MediaTypeUtils::isExtension(info.suffix(), MT_VIDEO) ||
                                      MediaTypeUtils::isExtension(info.suffix(), MT_AUDIO)))
                {
                    me->m_cache.requestToUpdate(item.path, index, role, this->m_signature);
                    ret = -1.0;
                }
                else
                {
                    ret = 0.0;
                }
            }
            else
            {
                ret = item.curPosition;
            }

            break;
        }
        default:
        {
            ret = QVariant();
            break;
        }
    }

    return ret;
}

bool FileListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid())
        return false;

    int row = index.row();

    if (row < 0 || row >= this->m_curList.count())
        return false;

    Item &item = this->m_curList[row];
    QVector<int> roles;

    roles.append(role);

    switch (role)
    {
        case ThumbnailRole:
        {
            item.imgPath = value.toString();
            break;
        }
        case DirEntryCountRole:
        {
            item.dirEntryCount = value.toInt();
            break;
        }
        case FileInfoRole:
        {
            item.fileInfo = value.toString();
            break;
        }
        case DurationRole:
        {
            item.duration = value.toDouble();

            if (item.duration < 0.0)
                item.duration = 0.0;

            break;
        }
        case CurPositionRole:
        {
            item.curPosition = value.toDouble();

            if (item.curPosition < 0.0)
                item.curPosition = 0.0;

            break;
        }
        default:
        {
            return false;
        }
    }

    item.isLoaded = true;

    emit dataChanged(index, index, roles);

    return true;
}

bool FileListModel::removeRows(int row, int count, const QModelIndex &parent)
{
    int last = row + count - 1;

    if (row >= this->m_curList.count() || last >= this->m_curList.count())
        return false;

    this->beginRemoveRows(parent, row, last);

    while (count --> 0)
        this->m_curList.removeAt(row);

    this->endRemoveRows();

    return true;
}

void FileListModel::init()
{
    QString primaryPath;
    const QSettings &settings = SettingsFactory::getInstance(SettingsFactory::ST_GLOBAL);

#if defined Q_OS_ANDROID
    QAndroidJniObject extDir = QAndroidJniObject::callStaticObjectMethod("android/os/Environment", "getExternalStorageDirectory", "()Ljava/io/File;");
    QAndroidJniObject extPath = extDir.callObjectMethod("getAbsolutePath", "()Ljava/lang/String;");

    primaryPath = extPath.toString();
#elif defined Q_OS_IOS
    const auto locations = QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation);

    primaryPath = locations.first();
#elif defined Q_OS_RASPBERRY_PI
    const auto locations = QStandardPaths::standardLocations(QStandardPaths::HomeLocation);

    primaryPath = locations.first();
#endif

    this->m_rootDirSecondaries = this->findSecondaryPaths() + this->findRemovablePaths();
    this->m_rootDirSecondaries.removeDuplicates();

    this->m_rootDirPrimary = primaryPath;

    if (!this->m_rootDirSecondaries.isEmpty())
        this->m_rootDirSecondary = this->m_rootDirSecondaries.first();

    if (this->m_startDirectory.isEmpty())
    {
        QuickBarType type = (QuickBarType)settings.value(SETTING_CURRENT_QUICKBAR_ITEM + this->objectName(), InternalType).toInt();

        switch (type)
        {
            case InternalType:
            {
                this->update(this->m_rootDirPrimary);
                break;
            }
            case ExternalType:
            {
                this->update(this->m_rootDirSecondary);
                break;
            }
            case VideoType:
            {
                if (this->isSubFilelistMode())
                {
                    type = InternalType;
                    this->update(this->m_rootDirPrimary);
                }
                else
                {
                    this->updateFromDatabase(Video);
                }

                break;
            }
            case AudioType:
            {
                if (this->isSubFilelistMode())
                {
                    type = InternalType;
                    this->update(this->m_rootDirPrimary);
                }
                else
                {
                    this->updateFromDatabase(Audio);
                }

                break;
            }
            default:
            {
                break;
            }
        }

        this->m_quickBarType = type;
    }
    else
    {
        this->update(this->m_startDirectory);

        if (this->m_startDirectory.startsWith(this->m_rootDirPrimary))
            this->m_quickBarType = InternalType;
        else if (this->m_startDirectory.startsWith(this->m_rootDirSecondary))
            this->m_quickBarType = ExternalType;
    }

    if (!this->isSubFilelistMode())
    {
        RenderScreen *tmp = new RenderScreen;

        if (settings.value(SETTING_FIRST_LOADING, true).toBool())
            tmp->saveSettings();

        delete tmp;
    }
}

double FileListModel::cdup()
{
    if (this->m_rootDirPrimary == this->m_curDir ||
        this->m_rootDirSecondary == this->m_curDir ||
        this->m_curDir.isEmpty())
        return 0.0;

    QDir dir(this->m_curDir);

    dir.cdUp();
    this->m_curDir = dir.absolutePath();

    if (this->m_scrollPos.isEmpty())
        return 0.0;
    else
        return this->m_scrollPos.pop();
}

void FileListModel::update()
{
    this->update(this->m_curDir);
}

void FileListModel::cd(const QString &path, double scrollPos)
{
    this->update(path);
    this->m_scrollPos.push(scrollPos);
}

QString FileListModel::getFirstFilePath(const QString &path) const
{
    QDir dir(path);
    const QFileInfoList files = dir.entryInfoList(QDir::Files, QDir::Name);

    for (const QFileInfo &file : files)
    {
        QString suffix = file.suffix();

        if (MediaTypeUtils::isMediaExtension(suffix))
            return file.absoluteFilePath();
    }

    return QString();
}

bool FileListModel::deletePath(const QString &path)
{
    QFileInfo info(path);
    bool success = false;

    if (info.isDir())
    {
        QDir d(path);

        success = d.exists() ? d.removeRecursively() : true;
    }
    else
    {
        MainSettingDelegate delegate;
        QFile f(path);
        bool removed = f.exists() ? f.remove() : true;
        bool deleteMediaWithSubtitle = delegate.getSetting(AnyVODEnums::SK_DELETE_MEDIA_WITH_SUBTITLE).toBool();

        if (deleteMediaWithSubtitle)
        {
            RenderScreen *tmp = new RenderScreen;

            tmp->tryOpenSubtitle(path);

            if (tmp->existExternalSubtitle())
            {
                QFile subtitle(tmp->getSubtitlePath());

                subtitle.remove();
            }
        }

        success = removed;
    }

    if (!success)
        return false;

    int i;

    for (i = 0; i < this->m_curList.count(); i++)
    {
        if (this->m_curList[i].path == path)
            break;
    }

    if (i >= this->m_curList.count())
        return false;

    return this->removeRow(i);
}

bool FileListModel::isSecondaryPath(const QString &path) const
{
    return !this->m_rootDirSecondary.isEmpty() && path.startsWith(this->m_rootDirSecondary);
}

QString FileListModel::appendEntry(const QString &base, const QString &entry) const
{
    QFileInfo info(base);

    if (info.isDir())
    {
        return base + QDir::separator() + entry;
    }
    else
    {
        QDir path(base);

        path.cdUp();

        return path.absolutePath() + QDir::separator() + entry;
    }
}

void FileListModel::update(const QString &path)
{
    this->m_cache.setReciever(this);
    this->m_cache.setOnlyDirectory(this->m_onlyDirectory);
    this->m_cache.setOnlySubtitle(this->m_onlySubtitle);
    this->m_cache.setOnlyETC(this->m_onlyETC);
    this->m_cache.clear();

    if (path == this->m_rootDirPrimary)
        this->saveQuickBarType(InternalType);
    else if (path == this->m_rootDirSecondary)
        this->saveQuickBarType(ExternalType);

    this->beginResetModel();
    this->m_curList.clear();

    if (!path.isEmpty())
    {
        QDir sdcard(path);
        QVector<Item> dirList;
        QVector<Item> fileList;
        int filter = QDir::NoDotAndDotDot;
        int sort;

        if (this->m_onlyDirectory)
            filter |= QDir::Dirs | QDir::Drives;
        else
            filter |= QDir::AllEntries;

        switch (this->m_sortType[this->m_quickBarType])
        {
            case NameAscending:
                sort = QDir::Name;
                break;
            case TimeAscending:
                sort = QDir::Time | QDir::Reversed;
                break;
            case SizeAscending:
                sort = QDir::Size | QDir::Reversed;
                break;
            case TypeAscending:
                sort = QDir::Type;
                break;
            case NameDescending:
                sort = QDir::Name | QDir::Reversed;
                break;
            case TimeDescending:
                sort = QDir::Time;
                break;
            case SizeDescending:
                sort = QDir::Size;
                break;
            case TypeDescending:
                sort = QDir::Type | QDir::Reversed;
                break;
            default:
                sort = QDir::Name;
                break;
        }

        const QFileInfoList dirs = sdcard.entryInfoList((QDir::Filter)filter, (QDir::SortFlag)sort);

        for (const QFileInfo &dir : dirs)
        {
            Item item;
            QString dirPath = dir.absoluteFilePath();

            if (dir.isDir())
            {
                QDir d(dirPath);
                QFileInfoList list = d.entryInfoList((QDir::Filter)filter);
                bool exists = false;

                for (const QFileInfo &subPath : qAsConst(list))
                {
                    if (subPath.isFile())
                    {
                        QString suffix = subPath.suffix();

                        if (this->m_onlySubtitle)
                        {
                            exists = MediaTypeUtils::isExtension(suffix, MT_SUBTITLE);
                        }
                        else
                        {
                            if (this->m_onlyETC)
                                exists = MediaTypeUtils::isETCExtension(suffix);
                            else
                                exists = MediaTypeUtils::isMediaExtension(suffix);
                        }

                        if (exists)
                            break;
                    }
                    else if (subPath.isDir())
                    {
                        exists = true;
                        break;
                    }
                }

                if (exists || this->m_onlyDirectory || this->m_onlyETC)
                {
                    item.path = dirPath;
                    dirList.append(item);
                }
            }
            else
            {
                QString suffix = dir.suffix();
                bool matched = false;

                if (this->m_onlySubtitle)
                {
                    matched = MediaTypeUtils::isExtension(suffix, MT_SUBTITLE);
                }
                else
                {
                    if (this->m_onlyETC)
                        matched = MediaTypeUtils::isETCExtension(suffix);
                    else
                        matched = MediaTypeUtils::isMediaExtension(suffix);
                }

                if (matched)
                {
                    item.path = dirPath;
                    fileList.append(item);
                }
            }
        }

        this->m_curList.append(dirList);
        this->m_curList.append(fileList);
    }

    this->m_curDir = path;
    this->m_signature = QUuid::createUuid();

    this->endResetModel();
}

void FileListModel::updateFromDatabase(int type)
{
    this->m_cache.setReciever(this);
    this->m_cache.setOnlyDirectory(false);
    this->m_cache.setOnlySubtitle(false);
    this->m_cache.setOnlyETC(false);
    this->m_cache.clear();

    switch (type)
    {
        case Video:
            this->saveQuickBarType(VideoType);
            break;
        case Audio:
            this->saveQuickBarType(AudioType);
            break;
        default:
            break;
    }

    this->beginResetModel();
    this->m_curList.clear();

#ifdef Q_OS_ANDROID
    const char *func = nullptr;
    QString field;
    QString descending;

    switch (type)
    {
        case Video:
            func = "getVideoURLs";
            break;
        case Audio:
            func = "getAudioURLs";
            break;
        default:
            break;
    }

    switch (this->m_sortType[this->m_quickBarType])
    {
        case NameAscending:
            field = "name";
            break;
        case TimeAscending:
            field = "time";
            break;
        case SizeAscending:
            field = "size";
            break;
        case TypeAscending:
            field = "type";
            break;
        case NameDescending:
            field = "name";
            descending = "desc";
            break;
        case TimeDescending:
            field = "time";
            descending = "desc";
            break;
        case SizeDescending:
            field = "size";
            descending = "desc";
            break;
        case TypeDescending:
            field = "type";
            descending = "desc";
            break;
        default:
            field = "name";
            break;
    }

    QAndroidJniEnvironment env;
    QAndroidJniObject activity = QtAndroid::androidActivity();
    QAndroidJniObject paths = activity.callObjectMethod(func, "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;",
                                                        QAndroidJniObject::fromString(field).object<jstring>(),
                                                        QAndroidJniObject::fromString(descending).object<jstring>(),
                                                        nullptr);

    if (paths.isValid())
    {
        int pathCount = env->GetArrayLength(paths.object<jarray>());
        jobjectArray pathsJava = paths.object<jobjectArray>();

        for (int i = 0; i < pathCount; i++)
        {
            jobject pathJava = env->GetObjectArrayElement(pathsJava, i);
            QAndroidJniObject path = pathJava;
            Item item;

            item.path = path.toString();

            if (QFile(item.path).exists())
                this->m_curList.append(item);

            env->DeleteLocalRef(pathJava);
        }
    }
#else
    (void)type;
#endif

    this->m_curDir.clear();
    this->m_signature = QUuid::createUuid();

    this->endResetModel();
}

void FileListModel::updateCurrentPos(const QString &filePath)
{
    for (int i = 0; i < this->m_curList.count(); i++)
    {
        const Item &item = this->m_curList[i];

        if (item.path == filePath)
        {
            if (item.isLoaded)
                this->m_cache.requestToUpdate(filePath, this->index(i), CurPositionRole, this->m_signature);

            break;
        }
    }
}

void FileListModel::clearScrollPos()
{
    this->m_scrollPos.clear();
}

QString FileListModel::curDirName() const
{
    QDir d(this->m_curDir);
    QString name = d.dirName();

    if (name == QDir(this->m_rootDirPrimary).dirName() ||
        name == QDir(this->m_rootDirSecondary).dirName() ||
        this->m_curDir.isEmpty())
        return this->topSignature();
    else
        return name;
}

QString FileListModel::curDirPath() const
{
    return this->m_curDir;
}

QString FileListModel::topSignature() const
{
    return "AnyVOD Mobile";
}

QString FileListModel::primaryPath() const
{
    return this->m_rootDirPrimary;
}

QString FileListModel::secondaryPath() const
{
    return this->m_rootDirSecondary;
}

bool FileListModel::existsMediaFiles(const QString &path) const
{
    QDir d(path);
    QFileInfoList list = d.entryInfoList(QDir::AllEntries | QDir::NoDotAndDotDot | QDir::NoSymLinks);

    if (list.isEmpty())
    {
        return false;
    }
    else
    {
        bool exist = false;

        for (const QFileInfo &subPath : qAsConst(list))
        {
            QString suffix = subPath.suffix();

            if (subPath.isFile() && MediaTypeUtils::isMediaExtension(suffix))
                exist |= true;
            else
                exist |= this->existsMediaFiles(subPath.absoluteFilePath());
        }

        return exist;
    }
}

QStringList FileListModel::findSecondaryPaths() const
{
    QStringList retList;

#ifdef Q_OS_ANDROID
    QString path = getenv("SECONDARY_STORAGE");

    if (path.isEmpty())
    {
        QStringList list;
        QAndroidJniEnvironment env;
        QAndroidJniObject activity = QtAndroid::androidActivity();
        QAndroidJniObject extDirs = activity.callObjectMethod("getExternalFilesDirs",
                                                              "(Ljava/lang/String;)[Ljava/io/File;",
                                                              nullptr);

        if (extDirs.isValid())
        {
            int dirCount = env->GetArrayLength(extDirs.object<jarray>());
            jobjectArray dirsJava = extDirs.object<jobjectArray>();

            for (int i = 1; i < dirCount; i++)
            {
                jobject dirJava = env->GetObjectArrayElement(dirsJava, i);
                QAndroidJniObject dir = dirJava;

                if (dir.isValid())
                {
                    QAndroidJniObject extPath = dir.callObjectMethod("getAbsolutePath", "()Ljava/lang/String;");
                    QDir path(extPath.toString());

                    while (path.dirName() != "Android")
                    {
                        if (!path.cdUp())
                            break;
                    }

                    path.cdUp();

                    list.append(path.absolutePath());
                }

                env->DeleteLocalRef(dirJava);
            }
        }

        list.append("/storage/sdcard0/ext_sd");
        list.append("/sdcard2");
        list.append("/mnt/sdcard/ext_sd");
        list.append("/storage/sdcard1");
        list.append("/storage/extsdcard");
        list.append("/storage/sdcard0/external_sdcard");
        list.append("/mnt/extsdcard");
        list.append("/mnt/sdcard/external_sd");
        list.append("/mnt/external_sd");
        list.append("/mnt/media_rw/sdcard1");
        list.append("/removable/microsd");
        list.append("/mnt/emmc");
        list.append("/storage/external_SD");
        list.append("/storage/ext_sd");
        list.append("/storage/removable/sdcard1");
        list.append("/data/sdext");
        list.append("/data/sdext2");
        list.append("/data/sdext3");
        list.append("/data/sdext4");

        for (const QString &item : list)
        {
            QDir d(item);

            if (!d.entryList(QDir::AllEntries | QDir::NoDotAndDotDot).isEmpty())
                retList.append(item);
        }
    }
    else
    {
        QStringList pathList = path.split(":", Qt::SkipEmptyParts);

        if (!pathList.isEmpty())
        {
            for (const QString &item : qAsConst(pathList))
            {
                QDir d(item);

                if (!d.entryList(QDir::AllEntries | QDir::NoDotAndDotDot).isEmpty())
                    retList.append(item);
            }
        }
    }
#endif

    return retList;
}

QStringList FileListModel::findRemovablePaths() const
{
    QStringList retList;

#ifdef Q_OS_ANDROID
    QAndroidJniEnvironment env;
    QAndroidJniObject activity = QtAndroid::androidActivity();
    QAndroidJniObject vols = activity.callObjectMethod("getRemovableVolumes", "()[Landroid/os/storage/StorageVolume;");

    if (vols.isValid())
    {
        int volCount = env->GetArrayLength(vols.object<jarray>());
        jobjectArray volsJava = vols.object<jobjectArray>();

        for (int i = 0; i < volCount; i++)
        {
            jobject volJava = env->GetObjectArrayElement(volsJava, i);
            QAndroidJniObject vol = volJava;
            QAndroidJniObject item = vol.callObjectMethod("getPath", "()Ljava/lang/String;");

            if (env->ExceptionCheck())
            {
                env->ExceptionClear();
            }
            else
            {
                if (item.isValid())
                    retList.append(item.toString());
            }

            env->DeleteLocalRef(volJava);
        }
    }
#endif

    return retList;
}

void FileListModel::saveQuickBarType(FileListModel::QuickBarType type)
{
    QSettings &settings = SettingsFactory::getInstance(SettingsFactory::ST_GLOBAL);

    settings.setValue(SETTING_CURRENT_QUICKBAR_ITEM + this->objectName(), type);
    this->m_quickBarType = type;
}

bool FileListModel::isSubFilelistMode() const
{
    return this->m_onlyDirectory || this->m_onlySubtitle || this->m_onlyETC;
}

void FileListModel::setSortType(int type)
{
    this->m_sortType[this->m_quickBarType] = (SortType)type;
}

int FileListModel::getSortType() const
{
    return this->m_sortType[this->m_quickBarType];
}

void FileListModel::selectSecondaryPath(int index)
{
    if (index < 0 || index >= this->m_rootDirSecondaries.count())
        return;

    this->m_rootDirSecondary = this->m_rootDirSecondaries[index];
}

int FileListModel::getSecondaryPathCount() const
{
    return this->m_rootDirSecondaries.count();
}

bool FileListModel::existSecondaryPath() const
{
    return this->getSecondaryPathCount() > 1;
}

int FileListModel::getSelectedSecondaryPath() const
{
    for (int i = 0; i < this->m_rootDirSecondaries.count(); i++)
    {
        if (this->m_rootDirSecondary.startsWith(this->m_rootDirSecondaries[i]))
            return i;
    }

    return -1;
}

QStringList FileListModel::getSecondaryPathNames() const
{
    QStringList list;

    for (const QString &dir : this->m_rootDirSecondaries)
        list.append(QDir(dir).dirName());

    return list;
}

QStringList FileListModel::getItems() const
{
    QStringList list;

    for (const Item &item : this->m_curList)
        list.append(item.path);

    return list;
}

void FileListModel::customEvent(QEvent *event)
{
    if (event->type() == UPDATE_ITEM_EVENT)
    {
        UpdateItemEvent *e = (UpdateItemEvent*)event;

        if (this->m_signature == e->getSignature())
            this->setData(e->getIndex(), e->getVar(), e->getRole());
    }
    else
    {
        QAbstractListModel::customEvent(event);
    }
}

QHash<int, QByteArray> FileListModel::roleNames() const
{
    return this->m_roleNames;
}
