﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "device/RadioReader.h"
#include "media/RadioListItemUpdater.h"

#include <QAbstractListModel>
#include <QEvent>

class RadioListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    static const QEvent::Type SCAN_FINISHED_EVENT;
    static const QEvent::Type ADD_ITEM_EVENT;
    static const QEvent::Type SIGNAL_STRENGTH_EVENT;
    static const QEvent::Type CURRENT_CHANNEL_EVENT;
    static const QEvent::Type INIT_PROGRESS_EVENT;
    static const QEvent::Type SET_PROGRESS_EVENT;

public:
    enum RoleName
    {
        ChannelRole = Qt::UserRole,
        SignalStrengthRole,
        FrequencyRole,
    };

    class ScanFinishedEvent : public QEvent
    {
    public:
        ScanFinishedEvent() :
            QEvent(SCAN_FINISHED_EVENT)
        {

        }
    };

    class AddItemEvent : public QEvent
    {
    public:
        AddItemEvent(const RadioReader::ChannelInfo &info) :
            QEvent(ADD_ITEM_EVENT),
            m_info(info)
        {

        }

        RadioReader::ChannelInfo getInfo() const { return this->m_info; }

    private:
        RadioReader::ChannelInfo m_info;
    };

    class SignalStrengthEvent : public QEvent
    {
    public:
        SignalStrengthEvent(int value) :
            QEvent(SIGNAL_STRENGTH_EVENT),
            m_value(value)
        {

        }

        int getValue() const { return this->m_value; }

    private:
        int m_value;
    };

    class CurrentChannelEvent : public QEvent
    {
    public:
        CurrentChannelEvent(int channel, uint64_t freq) :
            QEvent(CURRENT_CHANNEL_EVENT),
            m_channel(channel),
            m_freq(freq)
        {

        }

        int getChannel() const { return this->m_channel; }
        uint64_t getFrequency() const { return this->m_freq; }

    private:
        int m_channel;
        uint64_t m_freq;
    };

    class InitProgressEvent : public QEvent
    {
    public:
        InitProgressEvent(int start, int end) :
            QEvent(INIT_PROGRESS_EVENT),
            m_start(start),
            m_end(end)
        {

        }

        int getStart() const { return this->m_start; }
        int getEnd() const { return this->m_end; }

    private:
        int m_start;
        int m_end;
    };

    class SetProgressEvent : public QEvent
    {
    public:
        SetProgressEvent(int value) :
            QEvent(SET_PROGRESS_EVENT),
            m_value(value)
        {

        }

        int getValue() const { return this->m_value; }

    private:
        int m_value;
    };

public:
    explicit RadioListModel(QObject *parent = nullptr);

    void setReader(RadioReader *reader);
    void update(long adapter, RadioReaderInterface::DemodulatorType demodulatorType, QLocale::Country country,
                int start, int end);
    void stop();
    void clear();

    void setChannelInfo(const QVector<RadioReader::ChannelInfo> &list);
    QVector<RadioReader::ChannelInfo> getChannelInfo() const;

    bool existScannedChannel(int channel);

public:
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

protected:
    virtual QHash<int, QByteArray> roleNames() const;

private:
    virtual void customEvent(QEvent *event);

signals:
    void scanFinished();
    void signalStrength(int value);
    void scannedChannel(int current, QString freq, int total);
    void initProgress(int start, int end);
    void setProgress(int value);

private:
    QHash<int, QByteArray> m_roleNames;
    QVector<RadioReader::ChannelInfo> m_channels;
    RadioListItemUpdater m_updater;
    RadioReader *m_reader;
};
