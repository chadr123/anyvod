﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "RemoteFileListModel.h"
#include "FileListModel.h"
#include "PlayListModel.h"
#include "utils/RemoteFileUtils.h"
#include "utils/SeparatingUtils.h"
#include "utils/ConvertingUtils.h"
#include "utils/MediaTypeUtils.h"
#include "ui/delegates/SocketDelegate.h"

#include <QDir>
#include <QDebug>

const QString RemoteFileListModel::ROOT_DIR = "/";

RemoteFileListModel::RemoteFileListModel(QObject *parent) :
    QAbstractListModel(parent)
{
    this->m_roleNames[FileListModel::ThumbnailRole] = "thumbnail";
    this->m_roleNames[FileListModel::NameRole] = "name";
    this->m_roleNames[FileListModel::TypeRole] = "type";
    this->m_roleNames[FileListModel::PathRole] = "path";
    this->m_roleNames[FileListModel::FileInfoRole] = "fileInfo";
    this->m_roleNames[FileListModel::FileNameRole] = "fileName";
}

RemoteFileListModel::~RemoteFileListModel()
{

}

int RemoteFileListModel::rowCount(const QModelIndex &parent) const
{
    (void)parent;

    return this->m_curList.count();
}

QVariant RemoteFileListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    int row = index.row();

    if (row < 0 || row >= this->m_curList.count())
        return QVariant();

    QVariant ret;
    const Item &item = this->m_curList[row];

    switch (role)
    {
        case FileListModel::ThumbnailRole:
        {
            QFileInfo info(item.path);

            if (item.type == FT_FILE)
            {
                if (MediaTypeUtils::isExtension(info.suffix(), MT_VIDEO))
                    ret = FileListModel::DEFAULT_MOVIE_ICON;
                else if (MediaTypeUtils::isExtension(info.suffix(), MT_AUDIO))
                    ret = FileListModel::DEFAULT_AUDIO_ICON;
                else
                    ret = FileListModel::DEFAULT_ETC_ICON;
            }
            else if (item.type == FT_DIR)
            {
                ret = FileListModel::DEFAULT_DIRECTORY_ICON;
            }
            else
            {
                ret = FileListModel::DEFAULT_ETC_ICON;
            }

            break;
        }
        case FileListModel::FileInfoRole:
        {
            ret = item.fileInfo;
            break;
        }
        case FileListModel::FileNameRole:
        {
            ret = QFileInfo(item.path).fileName();
            break;
        }
        case FileListModel::NameRole:
        {
            QFileInfo info(item.path);

            if (item.type == FT_DIR)
                ret = info.fileName();
            else
                ret = info.completeBaseName();

            break;
        }
        case FileListModel::TypeRole:
        {
            if (item.type == FT_DIR)
                ret = FileListModel::Directory;
            else if (item.type == FT_FILE)
                ret = FileListModel::File;

            break;
        }
        case FileListModel::PathRole:
        {
            ret = item.path;
            break;
        }
        default:
        {
            ret = QVariant();
            break;
        }
    }

    return ret;
}

void RemoteFileListModel::init()
{
    this->update(ROOT_DIR);
}

double RemoteFileListModel::cdup()
{
    if (ROOT_DIR == this->m_curDir || this->m_curDir.isEmpty())
        return 0.0;

    int index = this->m_curDir.lastIndexOf(ROOT_DIR);

    if (index < 0)
        return 0.0;

    if (index == 0)
        index++;

    this->m_curDir = this->m_curDir.mid(0, index);

    if (this->m_scrollPos.isEmpty())
        return 0.0;
    else
        return this->m_scrollPos.pop();
}

void RemoteFileListModel::update()
{
    this->update(this->m_curDir);
}

void RemoteFileListModel::cd(const QString &path, double scrollPos)
{
    this->update(path);
    this->m_scrollPos.push(scrollPos);
}

void RemoteFileListModel::update(const QString &path)
{
    this->beginResetModel();
    this->m_curList.clear();

    if (!path.isEmpty())
    {
        QVector<Item> dirList;
        QVector<Item> fileList;
        QList<ANYVOD_FILE_ITEM> list;
        SocketDelegate socket;

        list = socket.requestFilelist(path);

        for (const ANYVOD_FILE_ITEM &orgItem : qAsConst(list))
        {
            if (!orgItem.permission)
                continue;

            Item item = this->convertAnyVODItemToItem(path, orgItem);

            if (orgItem.type == FT_DIR)
                dirList.append(item);
            else if (orgItem.type == FT_FILE)
                fileList.append(item);
        }

        this->m_curList.append(dirList);
        this->m_curList.append(fileList);
    }

    this->m_curDir = path;
    this->m_signature = QUuid::createUuid();

    this->endResetModel();
}

void RemoteFileListModel::clearScrollPos()
{
    this->m_scrollPos.clear();
}

QString RemoteFileListModel::curDirName() const
{
    QDir d(this->m_curDir);
    QString name = d.dirName();

    if (name == ROOT_DIR || name.isEmpty() || this->m_curDir.isEmpty())
        return this->topSignature();
    else
        return name;
}

QString RemoteFileListModel::curDirPath() const
{
    return this->m_curDir;
}

QString RemoteFileListModel::topSignature() const
{
    return "AnyVOD Mobile";
}

QVariantList RemoteFileListModel::getFileItems() const
{
    QVariantList list;

    for (const Item &item : this->m_curList)
    {
        if (item.type == FT_FILE)
            list.append(QVariant::fromValue(item.playItem));
    }

    return list;
}

QVariantList RemoteFileListModel::getFilePaths(const QString &parent) const
{
    QList<ANYVOD_FILE_ITEM> list;
    QVariantList ret;
    SocketDelegate socket;

    list = socket.requestFilelist(parent);

    for (const ANYVOD_FILE_ITEM &orgItem : qAsConst(list))
    {
        if (!orgItem.permission)
            continue;

        if (orgItem.type == FT_DIR)
        {
            QString dir = parent;

            SeparatingUtils::appendDirSeparator(&dir);
            dir += QString::fromStdWString(orgItem.fileName);

            ret += this->getFilePaths(dir);
        }
        else
        {
            Item item = this->convertAnyVODItemToItem(parent, orgItem);

            ret.append(QVariant::fromValue(item.playItem));
        }
    }

    return ret;
}

int RemoteFileListModel::getIndexByPath(const QVariantList &list, const QString &path) const
{
    const QString anyvodPath = RemoteFileUtils::ANYVOD_PROTOCOL + path;

    for (int i = 0; i < list.size(); i++)
    {
        const QVariant &var = list[i];
        const PlayItem item = var.value<PlayItem>();

        if (item.path == anyvodPath)
            return i;
    }

    return -1;
}

RemoteFileListModel::Item RemoteFileListModel::convertAnyVODItemToItem(const QString &parentPath, const ANYVOD_FILE_ITEM &item) const
{
    Item ret;
    QString withSepPath = parentPath;

    SeparatingUtils::appendDirSeparator(&withSepPath);

    ret.type = item.type;
    ret.path = withSepPath + QString::fromStdWString(item.fileName);
    ret.duration = (double)item.totalTime;

    ConvertingUtils::getTimeString(ret.duration, ConvertingUtils::TIME_HH_MM_SS, &ret.fileInfo);
    ret.fileInfo += " - ";
    ret.fileInfo += QString().setNum(item.bitRate / 1024 / 8) + " KB";

    ret.playItem.path = RemoteFileUtils::ANYVOD_PROTOCOL + ret.path;
    ret.playItem.extraData.duration = (double)item.totalTime;
    ret.playItem.extraData.totalFrame = item.totalFrame;
    ret.playItem.extraData.valid = true;
    ret.playItem.title = QString::fromStdWString(item.fileName);
    ret.playItem.totalTime = (double)item.totalTime;
    ret.playItem.itemUpdated = true;

    return ret;
}

QHash<int, QByteArray> RemoteFileListModel::roleNames() const
{
    return this->m_roleNames;
}
