﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "device/DTVReader.h"
#include "media/DTVListItemUpdater.h"

#include <QAbstractListModel>
#include <QEvent>

class DTVListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    static const QEvent::Type SCAN_FINISHED_EVENT;
    static const QEvent::Type ADD_ITEM_EVENT;
    static const QEvent::Type SIGNAL_STRENGTH_EVENT;
    static const QEvent::Type CURRENT_CHANNEL_EVENT;
    static const QEvent::Type INIT_PROGRESS_EVENT;
    static const QEvent::Type SET_PROGRESS_EVENT;

public:
    enum RoleName
    {
        ChannelRole = Qt::UserRole,
        SignalStrengthRole,
        NameRole,
    };

    class ScanFinishedEvent : public QEvent
    {
    public:
        ScanFinishedEvent() :
            QEvent(SCAN_FINISHED_EVENT)
        {

        }
    };

    class AddItemEvent : public QEvent
    {
    public:
        AddItemEvent(const DTVReader::ChannelInfo &info) :
            QEvent(ADD_ITEM_EVENT),
            m_info(info)
        {

        }

        DTVReader::ChannelInfo getInfo() const { return this->m_info; }

    private:
        DTVReader::ChannelInfo m_info;
    };

    class SignalStrengthEvent : public QEvent
    {
    public:
        SignalStrengthEvent(int value) :
            QEvent(SIGNAL_STRENGTH_EVENT),
            m_value(value)
        {

        }

        int getValue() const { return this->m_value; }

    private:
        int m_value;
    };

    class CurrentChannelEvent : public QEvent
    {
    public:
        CurrentChannelEvent(int channel) :
            QEvent(CURRENT_CHANNEL_EVENT),
            m_channel(channel)
        {

        }

        int getChannel() const { return this->m_channel; }

    private:
        int m_channel;
    };

    class InitProgressEvent : public QEvent
    {
    public:
        InitProgressEvent(int start, int end) :
            QEvent(INIT_PROGRESS_EVENT),
            m_start(start),
            m_end(end)
        {

        }

        int getStart() const { return this->m_start; }
        int getEnd() const { return this->m_end; }

    private:
        int m_start;
        int m_end;
    };

    class SetProgressEvent : public QEvent
    {
    public:
        SetProgressEvent(int value) :
            QEvent(SET_PROGRESS_EVENT),
            m_value(value)
        {

        }

        int getValue() const { return this->m_value; }

    private:
        int m_value;
    };

public:
    explicit DTVListModel(QObject *parent = nullptr);

    void setReader(DTVReader *reader);
    void update(long adapter, DTVReaderInterface::SystemType systemType, QLocale::Country country,
                int start, int end);
    void stop();
    void clear();

    void setChannelInfo(const QVector<DTVReader::ChannelInfo> &list);
    QVector<DTVReader::ChannelInfo> getChannelInfo() const;

    bool existScannedChannel(int channel);

public:
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

protected:
    virtual QHash<int, QByteArray> roleNames() const;

private:
    virtual void customEvent(QEvent *event);

signals:
    void scanFinished();
    void signalStrength(int value);
    void scannedChannel(int current, int total);
    void initProgress(int start, int end);
    void setProgress(int value);

private:
    QHash<int, QByteArray> m_roleNames;
    QVector<DTVReader::ChannelInfo> m_channels;
    DTVListItemUpdater m_updater;
    DTVReader *m_reader;
};
