﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "core/PlayItem.h"

#include "../../../../common/types.h"

#include <QAbstractListModel>
#include <QEvent>
#include <QUuid>
#include <QStack>

class RemoteFileListModel : public QAbstractListModel
{
    Q_OBJECT
private:
    struct Item
    {
        Item()
        {
            dirEntryCount = -1;
            duration = -1.0;
            curPosition = -1.0;
            type = FT_DIR;
        }

        PlayItem playItem;
        FILE_ITEM_TYPE type;
        QString path;
        QString imgPath;
        QString fileInfo;
        int dirEntryCount;
        double duration;
        double curPosition;
    };

public:
    explicit RemoteFileListModel(QObject *parent = nullptr);
    ~RemoteFileListModel();

public:
    virtual int rowCount(const QModelIndex &parent) const;
    virtual QVariant data(const QModelIndex &index, int role) const;

public:
    Q_INVOKABLE void init();
    Q_INVOKABLE double cdup();
    Q_INVOKABLE void update();
    Q_INVOKABLE void update(const QString &path);
    Q_INVOKABLE void clearScrollPos();
    Q_INVOKABLE void cd(const QString &path, double scrollPos);

    Q_INVOKABLE QVariantList getFileItems() const;
    Q_INVOKABLE QVariantList getFilePaths(const QString &parent) const;
    Q_INVOKABLE int getIndexByPath(const QVariantList &list, const QString &path) const;

public:
    Q_PROPERTY(QString curDirName READ curDirName CONSTANT)
    Q_PROPERTY(QString curDirPath READ curDirPath CONSTANT)
    Q_PROPERTY(QString topSignature READ topSignature CONSTANT)

private:
    Item convertAnyVODItemToItem(const QString &parentPath, const ANYVOD_FILE_ITEM &item) const;

protected:
    QString curDirName() const;
    QString curDirPath() const;
    QString topSignature() const;

protected:
    virtual QHash<int, QByteArray> roleNames() const;

protected:
    QVector<Item> m_curList;

private:
    static const QString ROOT_DIR;

private:
    QHash<int, QByteArray> m_roleNames;
    QString m_curDir;
    QUuid m_signature;
    QStack<double> m_scrollPos;
};
