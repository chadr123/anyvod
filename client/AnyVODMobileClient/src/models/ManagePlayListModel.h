﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <QAbstractListModel>

class QSettings;

class ManagePlayListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum RoleName
    {
        NameRole = Qt::UserRole,
    };

public:
    explicit ManagePlayListModel(QObject *parent = nullptr);
    ~ManagePlayListModel();

    Q_INVOKABLE void deleteItem(const QString &name);

    void reload(const QVariantHash &hash);

public:
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    virtual bool removeRows(int row, int count, const QModelIndex &parent);

protected:
    virtual QHash<int, QByteArray> roleNames() const;

private:
    QHash<int, QByteArray> m_roleNames;
    QStringList m_names;
    QSettings &m_settings;
};
