﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "RadioListModel.h"
#include "utils/ConvertingUtils.h"

const QEvent::Type RadioListModel::SCAN_FINISHED_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type RadioListModel::ADD_ITEM_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type RadioListModel::SIGNAL_STRENGTH_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type RadioListModel::CURRENT_CHANNEL_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type RadioListModel::INIT_PROGRESS_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type RadioListModel::SET_PROGRESS_EVENT = (QEvent::Type)QEvent::registerEventType();

RadioListModel::RadioListModel(QObject *parent) :
    QAbstractListModel(parent),
    m_updater(this)
{
    this->m_roleNames[ChannelRole] = "channel";
    this->m_roleNames[SignalStrengthRole] = "signalStrength";
    this->m_roleNames[FrequencyRole] = "frequency";
}

void RadioListModel::setReader(RadioReader *reader)
{
    this->m_reader = reader;
    this->m_updater.setReader(reader);
}

void RadioListModel::update(long adapter, RadioReaderInterface::DemodulatorType demodulatorType, QLocale::Country country,
                          int start, int end)
{
    if (!this->m_updater.isRunning())
    {
        this->m_updater.setData(adapter, demodulatorType, country, start, end);
        this->m_updater.setStop(false);

        this->m_updater.start();
    }
}

void RadioListModel::stop()
{
    this->m_updater.setStop(true);
    this->m_updater.wait();
}

void RadioListModel::clear()
{
    this->beginResetModel();
    this->m_channels.clear();
    this->endResetModel();
}

void RadioListModel::setChannelInfo(const QVector<RadioReader::ChannelInfo> &list)
{
    this->beginResetModel();
    this->m_channels = list;
    this->endResetModel();
}

QVector<RadioReader::ChannelInfo> RadioListModel::getChannelInfo() const
{
    return this->m_channels;
}

bool RadioListModel::existScannedChannel(int channel)
{
    for (const RadioReader::ChannelInfo &info : qAsConst(this->m_channels))
    {
        if (info.channel == channel)
            return true;
    }

    return false;
}

int RadioListModel::rowCount(const QModelIndex &parent) const
{
    (void)parent;

    return this->m_channels.count();
}

QVariant RadioListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    int row = index.row();

    if (row < 0 || row >= this->m_channels.count())
        return QVariant();

    QVariant ret;
    const RadioReader::ChannelInfo &item = this->m_channels[row];

    switch (role)
    {
        case ChannelRole:
        {
            ret = item.channel;
            break;
        }
        case SignalStrengthRole:
        {
            ret = (int)this->m_reader->getSignalStrengthInPercentage(item.signal);
            break;
        }
        case FrequencyRole:
        {
            ret = ConvertingUtils::valueToString(item.freq);
            break;
        }
        default:
        {
            ret = QVariant();
            break;
        }
    }

    return ret;
}

QHash<int, QByteArray> RadioListModel::roleNames() const
{
    return this->m_roleNames;
}

void RadioListModel::customEvent(QEvent *event)
{
    if (event->type() == SCAN_FINISHED_EVENT)
    {
        emit this->scanFinished();
    }
    else if (event->type() == ADD_ITEM_EVENT)
    {
        AddItemEvent *e = (AddItemEvent*)event;
        int pos = this->m_channels.count();

        this->beginInsertRows(this->index(pos), pos, pos);
        this->m_channels.append(e->getInfo());
        this->endInsertRows();
    }
    else if (event->type() == SIGNAL_STRENGTH_EVENT)
    {
        SignalStrengthEvent *e = (SignalStrengthEvent*)event;

        emit this->signalStrength(e->getValue());
    }
    else if (event->type() == CURRENT_CHANNEL_EVENT)
    {
        CurrentChannelEvent *e = (CurrentChannelEvent*)event;

        emit this->scannedChannel(e->getChannel(), ConvertingUtils::valueToString(e->getFrequency()), this->m_channels.count());
    }
    else if (event->type() == INIT_PROGRESS_EVENT)
    {
        InitProgressEvent *e = (InitProgressEvent*)event;

        emit this->initProgress(e->getStart(), e->getEnd());
    }
    else if (event->type() == SET_PROGRESS_EVENT)
    {
        SetProgressEvent *e = (SetProgressEvent*)event;

        emit this->setProgress(e->getValue());
    }
    else
    {
        QAbstractListModel::customEvent(event);
    }
}
