﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include <QGuiApplication>

#ifdef Q_OS_IOS
# include "IOSDelegate.h"
#endif

extern "C"
{
#ifdef Q_OS_ANDROID
# include <libavcodec/jni.h>
#endif
}

#include "AnyVODWindow.h"
#include "QmlRegistrar.h"
#include "ui/FontInfo.h"
#include "ui/RenderScreen.h"
#include "ui/delegates/LanguageDelegate.h"
#include "models/PlayListModel.h"
#include "models/ScreenExplorerListModel.h"
#include "models/ScreenExplorerProvider.h"
#include "media/MediaPresenter.h"
#include "media/LastPlay.h"
#include "device/DTVReader.h"
#include "device/RadioReader.h"
#include "net/Updater.h"
#include "net/NetworkProxy.h"
#include "setting/Settings.h"
#include "setting/SettingsFactory.h"
#include "setting/SettingBackupAndRestore.h"
#include "core/Common.h"
#include "language/GlobalLanguage.h"
#include "language/KeyboardLanguage.h"

#include "../../../../common/version.h"
#include "../../../../common/network.h"

#ifdef Q_OS_ANDROID
# include <QAndroidJniEnvironment>
#endif

#include <QString>
#include <QSettings>
#include <QDir>
#include <QFontDatabase>
#include <QTranslator>
#include <QQmlEngine>
#include <QDebug>

int main(int argc, char *argv[])
{
#ifdef Q_OS_RASPBERRY_PI
    qputenv("QT_IM_MODULE", QByteArray("qtvirtualkeyboard"));
    qputenv("QT_QPA_EGLFS_FORCE888", QByteArray("1"));
# if defined Q_PROCESSOR_X86 || defined QT_DEBUG
    qputenv("QT_QPA_PLATFORM", QByteArray("xcb"));
# else
    qputenv("QT_QPA_PLATFORM", QByteArray("eglfs"));
# endif
#endif

#ifdef Q_OS_ANDROID
    av_jni_set_java_vm(QAndroidJniEnvironment::javaVM(), nullptr);
#endif

    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication::setAttribute(Qt::AA_DisableShaderDiskCache);

    qRegisterMetaTypeStreamOperators<PlayItem>("PlayItem");
    qRegisterMetaTypeStreamOperators<LastPlay::Item>("LastPlay::Item");
    qRegisterMetaTypeStreamOperators<RenderScreen::EqualizerItem>("RenderScreen::EqualizerItem");
    qRegisterMetaTypeStreamOperators<MediaPresenter::Range>("MediaPresenter::Range");
    qRegisterMetaTypeStreamOperators<DTVReader::ChannelInfo>("DTVReader::ChannelInfo");
    qRegisterMetaTypeStreamOperators<RadioReader::ChannelInfo>("RadioReader::ChannelInfo");

    QmlRegistrar::registerQml();
    SettingBackupAndRestore::restoreSettings();

    QSurfaceFormat format = QSurfaceFormat::defaultFormat();

    format.setRenderableType(QSurfaceFormat::OpenGLES);
    QSurfaceFormat::setDefaultFormat(format);

    QGuiApplication app(argc, argv);
    AnyVODWindow viewer;
    int ret;

    QObject::connect(viewer.engine(), &QQmlEngine::exit, &app, &QGuiApplication::exit);

#if defined Q_OS_RASPBERRY_PI && defined QT_NO_DEBUG
    QDir::setCurrent(app.applicationDirPath());
#endif

#ifdef Q_OS_IOS
    IOSDelegate::initHeadsetConnectivity();
#endif

    QDir fontsDir(ASSETS_PREFIX"/fonts");
    QString defaultFamily;
    QString fontPath;
    const QStringList fonts = fontsDir.entryList(QDir::NoDotAndDotDot | QDir::Files);

    for (const QString &font : fonts)
    {
        int id;

        fontPath = fontsDir.absoluteFilePath(font);
        id = QFontDatabase::addApplicationFont(fontPath);

        if (id < 0)
            continue;

        QStringList families = QFontDatabase::applicationFontFamilies(id);

        if (families.isEmpty())
            continue;

        defaultFamily = families.first();
    }

    FontInfo::getInstance().setDefaultInfo(defaultFamily, fontPath);

    QSettings &settings = SettingsFactory::getInstance(SettingsFactory::ST_GLOBAL);
    QFont font;
    QString family = settings.value(SETTING_FONT, defaultFamily).toString();

    if (!family.isEmpty())
    {
        font.setFamily(family);
        settings.setValue(SETTING_FONT, family);
    }

    QString localName = QLocale::system().name();
    QString localLang = localName.split('_').first();
    QString templeate = "%1_%2";
    QString lang = settings.value(SETTING_LANGUAGE, localLang).toString();
    QTranslator trans;

    if (trans.load(templeate.arg(LanguageDelegate::LANG_PREFIX, lang), LanguageDelegate::LANG_DIR))
    {
        app.installTranslator(&trans);
        settings.setValue(SETTING_LANGUAGE, lang);
        GlobalLanguage::getInstance().setLanguage(lang);
    }

    KeyboardLanguage::getInstance().setLanguage(settings.value(SETTING_KEYBOARD_LANGUAGE, "").toString());
    Updater::getInstance().fixUpdateGap();

    QString serverAddress = settings.value(SETTING_ADDRESS, "").toString();
    int serverCommandPort = settings.value(SETTING_COMMAND_PORT, 0).toInt();
    int serverStreamPort = settings.value(SETTING_STREAM_PORT, 0).toInt();

    if (serverAddress.isEmpty())
        settings.setValue(SETTING_ADDRESS, QString::fromWCharArray(DEFAULT_SERVER_ADDRESS));

    if (serverCommandPort == 0)
        settings.setValue(SETTING_COMMAND_PORT, DEFAULT_NETWORK_PORT);

    if (serverStreamPort == 0)
        settings.setValue(SETTING_STREAM_PORT, DEFAULT_NETWORK_STREAM_PORT);

    NetworkProxy::setupProxyManual(settings.value(SETTING_PROXY_INFO, "").toString());

    MediaPresenter::init();
    srand((unsigned int)time(nullptr));

    app.setApplicationName("anyvod");
    app.setOrganizationDomain(COMPANY);
    app.setOrganizationName(COMPANY);
    app.setFont(font);

    viewer.engine()->addImageProvider(ScreenExplorerListModel::SCREEN_ID, new ScreenExplorerProvider);
    viewer.setMainQmlFile(QStringLiteral("qml/AnyVODMobileClient/main.qml"));
    viewer.resize(320, 480);
    viewer.showExpanded();

    ret = app.exec();

    MediaPresenter::deInit();

#ifdef Q_OS_IOS
    IOSDelegate::unInitHeadsetConnectivity();
#endif

    return ret;
}
