﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "AutoStart.h"
#include "ui/delegates/AutoStartDelegate.h"

#ifdef Q_OS_ANDROID
# include <QAndroidJniObject>
# include <QtAndroid>
#endif

AutoStart::AutoStart()
{

}

AutoStart& AutoStart::getInstance()
{
    static AutoStart instance;

    return instance;
}

void AutoStart::setStartingURL(const QString &url)
{
    this->m_startingURLLock.lock();
    this->m_startingURL = url;
    this->m_startingURLLock.unlock();

    AutoStartDelegate *autoStartDelegate = AutoStartDelegate::getSelf();

    if (autoStartDelegate)
        autoStartDelegate->newStartingURL();
}

QString AutoStart::getStartingURL()
{
    QMutexLocker locker(&this->m_startingURLLock);

#ifdef Q_OS_ANDROID
    QAndroidJniObject activity = QtAndroid::androidActivity();
    QAndroidJniObject urlObj = activity.callObjectMethod("getStartingURL", "()Ljava/lang/String;");
    QString url = urlObj.toString();

    if (!url.isEmpty())
    {
        this->m_startingURL = url;
        activity.callMethod<void>("setStartingURL", "(Ljava/lang/String;)V",
                                  QAndroidJniObject::fromString("").object<jstring>());
    }
#endif

    return this->m_startingURL;
}
