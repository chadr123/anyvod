﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

class IOSDelegate
{
private:
    IOSDelegate() {}
    ~IOSDelegate() {}

public:
    static void disableIdleTimer();
    static void enableIdleTimer();

    static void setStatusBarStyle(bool enable, int color);
    static void initHeadsetConnectivity();
    static void unInitHeadsetConnectivity();

    static void registerMediaCenterCommand();
    static void unRegisterMediaCenterCommand();
    static void updateMediaCenterCommand();
    static bool isRegisteredMediaCenterCommand();

    static void updateNowPlayingInfo();

    static bool isBluetoothHeadsetConnected();

private:
    static bool m_isPauseByNative;
    static bool m_prevBluetoothConnected;
    static bool m_isRegisteredMediaCenterCommand;
    static void *m_routeChangeObserver;
    static void *m_playTarget;
    static void *m_pauseTarget;
    static void *m_toggleTarget;
    static void *m_stopTarget;
    static void *m_prevTarget;
    static void *m_nextTarget;
    static void *m_backwardTarget;
    static void *m_forwardTarget;
};
