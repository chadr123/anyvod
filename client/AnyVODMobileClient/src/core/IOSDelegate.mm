﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include <objc/objc.h>

#include "IOSDelegate.h"

#define AVMediaType __AVMediaType__
# include "ui/RenderScreen.h"
#undef AVMediaType

#include "models/PlayListModel.h"

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <Foundation/Foundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import <dispatch/dispatch.h>

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

bool IOSDelegate::m_isPauseByNative = false;
bool IOSDelegate::m_prevBluetoothConnected = false;
bool IOSDelegate::m_isRegisteredMediaCenterCommand = false;

void *IOSDelegate::m_routeChangeObserver = nullptr;
void *IOSDelegate::m_playTarget = nullptr;
void *IOSDelegate::m_pauseTarget = nullptr;
void *IOSDelegate::m_toggleTarget = nullptr;
void *IOSDelegate::m_stopTarget = nullptr;
void *IOSDelegate::m_prevTarget = nullptr;
void *IOSDelegate::m_nextTarget = nullptr;
void *IOSDelegate::m_backwardTarget = nullptr;
void *IOSDelegate::m_forwardTarget = nullptr;

static NSMutableDictionary *s_mediaMeta = nil;

void IOSDelegate::disableIdleTimer()
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIApplication sharedApplication].idleTimerDisabled = YES;
    });
}

void IOSDelegate::enableIdleTimer()
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIApplication sharedApplication].idleTimerDisabled = NO;
    });
}

void IOSDelegate::setStatusBarStyle(bool enable, int color)
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIView *statusBar = nil;
        UIApplication *app = [UIApplication sharedApplication];

        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"13.0"))
        {
            const NSInteger tag = 38482458385l;
            UIWindow *window = nil;

            for (NSUInteger i = 0; i < app.windows.count; i++)
            {
                window = app.windows[i];

                if (window.isKeyWindow)
                    break;
            }

            if (!window)
                return;

            statusBar = [window viewWithTag:tag];

            if (!statusBar)
            {
                statusBar = [[UIView alloc] initWithFrame:app.statusBarFrame];

                statusBar.tag = tag;
                statusBar.autoresizingMask = UIViewAutoresizingFlexibleWidth;

                [window addSubview:statusBar];
            }

            statusBar.hidden = enable ? NO : YES;
        }
        else
        {
            statusBar = [[app valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
        }

        if ([statusBar respondsToSelector:@selector(setBackgroundColor:)])
        {
            if (enable)
            {
                statusBar.backgroundColor = [UIColor colorWithRed:((float)((color & 0xff0000) >> 16)) / 255.0
                        green:((float)((color & 0x00ff00) >> 8)) / 255.0
                        blue:((float)((color & 0x0000ff) >> 0)) / 255.0
                        alpha:1.0];
            }
            else
            {
                statusBar.backgroundColor = [UIColor blackColor];
            }
        }
    });
}

void IOSDelegate::initHeadsetConnectivity()
{
    void (^routeChangeBlock)(NSNotification*) =
          ^(NSNotification *notification)
        {
            RenderScreen *screen = RenderScreen::getSelf();

            if (!screen)
                return;

            NSNumber *reasonNumber = notification.userInfo[AVAudioSessionRouteChangeReasonKey];
            AVAudioSessionRouteChangeReason reason = (AVAudioSessionRouteChangeReason)reasonNumber.unsignedIntegerValue;

            switch (reason)
            {
                case AVAudioSessionRouteChangeReasonNewDeviceAvailable:
                {
                    if (IOSDelegate::isBluetoothHeadsetConnected())
                    {
                        IOSDelegate::m_prevBluetoothConnected = true;
                        screen->setBluetoothHeadsetConnected(true);

                        return;
                    }
                    else
                    {
                        IOSDelegate::m_prevBluetoothConnected = false;
                        screen->setBluetoothHeadsetConnected(false);
                    }

                    if (!IOSDelegate::m_isPauseByNative)
                        return;

                    if (screen->getStatus() == RenderScreen::Paused)
                    {
                        IOSDelegate::m_isPauseByNative = false;
                        screen->resume();
                    }

                    break;
                }
                case AVAudioSessionRouteChangeReasonOldDeviceUnavailable:
                {
                    if (IOSDelegate::m_prevBluetoothConnected)
                    {
                        IOSDelegate::m_prevBluetoothConnected = IOSDelegate::isBluetoothHeadsetConnected();
                        screen->setBluetoothHeadsetConnected(IOSDelegate::m_prevBluetoothConnected);

                        return;
                    }
                    else
                    {
                        IOSDelegate::m_prevBluetoothConnected = IOSDelegate::isBluetoothHeadsetConnected();
                        screen->setBluetoothHeadsetConnected(IOSDelegate::m_prevBluetoothConnected);
                    }

                    if (screen->getStatus() == RenderScreen::Playing || screen->getStatus() == RenderScreen::Started)
                    {
                        IOSDelegate::m_isPauseByNative = true;
                        screen->pause();
                    }

                    break;
                }
                default:
                {
                    IOSDelegate::m_prevBluetoothConnected = IOSDelegate::isBluetoothHeadsetConnected();
                    screen->setBluetoothHeadsetConnected(IOSDelegate::m_prevBluetoothConnected);

                    break;
                }
            }
        };

    AVAudioSession *audioSession = [AVAudioSession sharedInstance];

    [audioSession setCategory:AVAudioSessionCategoryPlayback error:nil];
    [audioSession setActive:YES error:nil];

    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    id routeChangeObserver = [center addObserverForName:AVAudioSessionRouteChangeNotification
                              object:nil
                              queue:[NSOperationQueue mainQueue]
                              usingBlock:routeChangeBlock];

    IOSDelegate::m_routeChangeObserver = (__bridge_retained void*)routeChangeObserver;
    IOSDelegate::m_prevBluetoothConnected = IOSDelegate::isBluetoothHeadsetConnected();
}

void IOSDelegate::unInitHeadsetConnectivity()
{
    if (IOSDelegate::m_routeChangeObserver != nullptr)
    {
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        id observer = (__bridge_transfer id)IOSDelegate::m_routeChangeObserver;

        [center removeObserver:observer];
        IOSDelegate::m_routeChangeObserver = nullptr;

        AVAudioSession *audioSession = [AVAudioSession sharedInstance];

        [audioSession setActive:NO error:nil];
    }
}

void IOSDelegate::registerMediaCenterCommand()
{
    if (![MPRemoteCommandCenter class])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
        });
        return;
    }

    MPRemoteCommandCenter *commandCenter = [MPRemoteCommandCenter sharedCommandCenter];
    RenderScreen *screen = RenderScreen::getSelf();

    if (!screen)
        return;

    commandCenter.playCommand.enabled = YES;
    id playTarget = [commandCenter.playCommand addTargetWithHandler:^(MPRemoteCommandEvent *)
    {
        screen->resume();
        return MPRemoteCommandHandlerStatusSuccess;
    }];
    IOSDelegate::m_playTarget = (__bridge_retained void*)playTarget;

    commandCenter.pauseCommand.enabled = YES;
    id pauseTarget = [commandCenter.pauseCommand addTargetWithHandler:^(MPRemoteCommandEvent *)
    {
        screen->pause();
        return MPRemoteCommandHandlerStatusSuccess;
    }];
    IOSDelegate::m_pauseTarget = (__bridge_retained void*)pauseTarget;

    commandCenter.togglePlayPauseCommand.enabled = YES;
    id toggleTarget = [commandCenter.togglePlayPauseCommand addTargetWithHandler:^(MPRemoteCommandEvent *)
    {
        screen->toggle();
        return MPRemoteCommandHandlerStatusSuccess;
    }];
    IOSDelegate::m_toggleTarget = (__bridge_retained void*)toggleTarget;

    commandCenter.stopCommand.enabled = YES;
    id stopTarget = [commandCenter.stopCommand addTargetWithHandler:^(MPRemoteCommandEvent *)
    {
        screen->closeAndGoBack();
        return MPRemoteCommandHandlerStatusSuccess;
    }];
    IOSDelegate::m_stopTarget = (__bridge_retained void*)stopTarget;

    id prevTarget = [commandCenter.previousTrackCommand addTargetWithHandler:^(MPRemoteCommandEvent *)
    {
        return screen->prev(false) ? MPRemoteCommandHandlerStatusSuccess : MPRemoteCommandHandlerStatusNoSuchContent;
    }];
    IOSDelegate::m_prevTarget = (__bridge_retained void*)prevTarget;

    id nextTarget = [commandCenter.nextTrackCommand addTargetWithHandler:^(MPRemoteCommandEvent *)
    {
        return screen->next(false) ? MPRemoteCommandHandlerStatusSuccess : MPRemoteCommandHandlerStatusNoSuchContent;
    }];
    IOSDelegate::m_nextTarget = (__bridge_retained void*)nextTarget;

    commandCenter.skipBackwardCommand.preferredIntervals = @[@(5.0)];
    id backwardTarget = [commandCenter.skipBackwardCommand addTargetWithHandler:^(MPRemoteCommandEvent *event)
    {
        if ([event isKindOfClass:[MPSkipIntervalCommandEvent class]])
        {
            MPSkipIntervalCommandEvent *skipEvent = (MPSkipIntervalCommandEvent*)event;

            screen->rewind(skipEvent.interval);

            return MPRemoteCommandHandlerStatusSuccess;
        }
        else
        {
            return MPRemoteCommandHandlerStatusCommandFailed;
        }
    }];
    IOSDelegate::m_backwardTarget = (__bridge_retained void*)backwardTarget;

    commandCenter.skipForwardCommand.preferredIntervals = @[@(5.0)];
    id forwardTarget = [commandCenter.skipForwardCommand addTargetWithHandler:^(MPRemoteCommandEvent *event)
    {
        if ([event isKindOfClass:[MPSkipIntervalCommandEvent class]])
        {
            MPSkipIntervalCommandEvent *skipEvent = (MPSkipIntervalCommandEvent*)event;

            screen->forward(skipEvent.interval);

            return MPRemoteCommandHandlerStatusSuccess;
        }
        else
        {
            return MPRemoteCommandHandlerStatusCommandFailed;
        }
    }];
    IOSDelegate::m_forwardTarget = (__bridge_retained void*)forwardTarget;

    IOSDelegate::m_isRegisteredMediaCenterCommand = true;
}

void IOSDelegate::unRegisterMediaCenterCommand()
{
    IOSDelegate::m_isRegisteredMediaCenterCommand = false;

    if (![MPRemoteCommandCenter class])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
        });
        return;
    }

    MPRemoteCommandCenter *commandCenter = [MPRemoteCommandCenter sharedCommandCenter];

    if (IOSDelegate::m_playTarget != nullptr)
    {
        id playTarget = (__bridge_transfer id)IOSDelegate::m_playTarget;

        [commandCenter.playCommand removeTarget:playTarget];
        IOSDelegate::m_playTarget = nullptr;
    }

    if (IOSDelegate::m_pauseTarget != nullptr)
    {
        id pauseTarget = (__bridge_transfer id)IOSDelegate::m_pauseTarget;

        [commandCenter.pauseCommand removeTarget:pauseTarget];
        IOSDelegate::m_pauseTarget = nullptr;
    }

    if (IOSDelegate::m_toggleTarget != nullptr)
    {
        id toggleTarget = (__bridge_transfer id)IOSDelegate::m_toggleTarget;

        [commandCenter.togglePlayPauseCommand removeTarget:toggleTarget];
        IOSDelegate::m_toggleTarget = nullptr;
    }

    if (IOSDelegate::m_stopTarget != nullptr)
    {
        id stopTarget = (__bridge_transfer id)IOSDelegate::m_stopTarget;

        [commandCenter.stopCommand removeTarget:stopTarget];
        IOSDelegate::m_stopTarget = nullptr;
    }

    if (IOSDelegate::m_prevTarget != nullptr)
    {
        id prevTarget = (__bridge_transfer id)IOSDelegate::m_prevTarget;

        [commandCenter.previousTrackCommand removeTarget:prevTarget];
        IOSDelegate::m_prevTarget = nullptr;
    }

    if (IOSDelegate::m_nextTarget != nullptr)
    {
        id nextTarget = (__bridge_transfer id)IOSDelegate::m_nextTarget;

        [commandCenter.nextTrackCommand removeTarget:nextTarget];
        IOSDelegate::m_nextTarget = nullptr;
    }

    if (IOSDelegate::m_backwardTarget != nullptr)
    {
        id backwardTarget = (__bridge_transfer id)IOSDelegate::m_backwardTarget;

        [commandCenter.skipBackwardCommand removeTarget:backwardTarget];
        IOSDelegate::m_backwardTarget = nullptr;
    }

    if (IOSDelegate::m_forwardTarget != nullptr)
    {
        id forwardTarget = (__bridge_transfer id)IOSDelegate::m_forwardTarget;

        [commandCenter.skipForwardCommand removeTarget:forwardTarget];
        IOSDelegate::m_forwardTarget = nullptr;
    }

    [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo = nil;
    s_mediaMeta = nil;
}

void IOSDelegate::updateMediaCenterCommand()
{
    MPRemoteCommandCenter *commandCenter = [MPRemoteCommandCenter sharedCommandCenter];
    RenderScreen *screen = RenderScreen::getSelf();

    if (!screen)
        return;

    BOOL enableSkip = screen->getPlayList()->getCount() <= 1;
    int totalCount = screen->getPlayList()->getCount();
    int curIndex = screen->getPlayList()->getCurrentIndex();

    if (totalCount <= 1)
    {
        commandCenter.previousTrackCommand.enabled = NO;
        commandCenter.nextTrackCommand.enabled = NO;
    }
    else if (curIndex - 1 < 0)
    {
        commandCenter.previousTrackCommand.enabled = NO;
        commandCenter.nextTrackCommand.enabled = YES;
    }
    else if (curIndex + 1 >= totalCount)
    {
        commandCenter.previousTrackCommand.enabled = YES;
        commandCenter.nextTrackCommand.enabled = NO;
    }
    else
    {
        commandCenter.previousTrackCommand.enabled = YES;
        commandCenter.nextTrackCommand.enabled = YES;
    }

    commandCenter.skipBackwardCommand.enabled = enableSkip;
    commandCenter.skipForwardCommand.enabled = enableSkip;

    UIImage *coverImage = [UIImage imageWithContentsOfFile:screen->getLargeCoverFilePath().toNSString()];
    MPMediaItemArtwork *cover = (coverImage == nil ? nil : [[MPMediaItemArtwork alloc] initWithBoundsSize:coverImage.size requestHandler:^UIImage* _Nonnull(CGSize) { return coverImage; }]);

    s_mediaMeta = [NSMutableDictionary dictionary];

    s_mediaMeta[MPMediaItemPropertyTitle] = screen->getTitle().toNSString();
    s_mediaMeta[MPMediaItemPropertyArtist] = screen->getArtist().toNSString();
    s_mediaMeta[MPMediaItemPropertyPlaybackDuration] = @(screen->getDuration());

    s_mediaMeta[MPNowPlayingInfoPropertyPlaybackQueueIndex] = @((NSUInteger)screen->getPlayList()->getCurrentIndex());
    s_mediaMeta[MPNowPlayingInfoPropertyPlaybackQueueCount] = @((NSUInteger)screen->getPlayList()->getCount());

    if (cover != nil)
        s_mediaMeta[MPMediaItemPropertyArtwork] = cover;
}

void IOSDelegate::updateNowPlayingInfo()
{
    RenderScreen *screen = RenderScreen::getSelf();

    if (!screen)
        return;

    double tempo = 1.0 + screen->getTempo() / 100.0;

    s_mediaMeta[MPNowPlayingInfoPropertyElapsedPlaybackTime] = @(screen->getCurrentPosition());
    s_mediaMeta[MPNowPlayingInfoPropertyPlaybackRate] = @(screen->getStatus() == RenderScreen::Playing ? tempo : 0.0);

    [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo = s_mediaMeta;
}

bool IOSDelegate::isRegisteredMediaCenterCommand()
{
    return IOSDelegate::m_isRegisteredMediaCenterCommand;
}

bool IOSDelegate::isBluetoothHeadsetConnected()
{
    AVAudioSession *session = [AVAudioSession sharedInstance];
    AVAudioSessionRouteDescription *routeDescription = [session currentRoute];

    if (routeDescription)
    {
        NSArray *outputs = [routeDescription outputs];

        if (outputs && [outputs count] > 0)
        {
            AVAudioSessionPortDescription *portDescription = [outputs objectAtIndex:0];
            NSString *portType = [portDescription portType];

            if (portType)
            {
                if ([portType isEqualToString:AVAudioSessionPortBluetoothA2DP] ||
                    [portType isEqualToString:AVAudioSessionPortBluetoothHFP] ||
                    [portType isEqualToString:AVAudioSessionPortBluetoothLE])
                    return true;
            }
        }
    }

    return false;
}
