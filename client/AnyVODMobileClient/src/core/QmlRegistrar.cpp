﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "QmlRegistrar.h"
#include "core/AnyVODGlobal.h"
#include "models/FileListModel.h"
#include "models/PlayListModel.h"
#include "models/DTVListModel.h"
#include "models/RadioListModel.h"
#include "models/EPGListModel.h"
#include "models/SearchMediaModel.h"
#include "models/ManagePlayListModel.h"
#include "models/RemoteFileListModel.h"
#include "models/ScreenExplorerListModel.h"
#include "ui/delegates/MediaPlayerSettingDelegate.h"
#include "ui/delegates/DTVDelegate.h"
#include "ui/delegates/RadioDelegate.h"
#include "ui/delegates/MainSettingDelegate.h"
#include "ui/delegates/UpdaterDelegate.h"
#include "ui/delegates/SocketDelegate.h"
#include "ui/delegates/EqualizerDelegate.h"
#include "ui/delegates/TextCodecDelegate.h"
#include "ui/delegates/AutoStartDelegate.h"
#include "ui/delegates/FontInfoDelegate.h"
#include "ui/delegates/TimeUtilDelegate.h"
#include "ui/delegates/LanguageDelegate.h"
#include "ui/delegates/VersionDelegate.h"
#include "ui/delegates/LicenseDelegate.h"
#include "ui/delegates/ToastDelegate.h"
#include "ui/delegates/StatusBarDelegate.h"
#include "ui/delegates/AudioDeviceDelegate.h"
#include "ui/delegates/SPDIFDelegate.h"
#include "ui/delegates/FileTypeDelegate.h"
#include "ui/delegates/CaptureExtDelegate.h"
#include "ui/delegates/RemoteFileUtilDelegate.h"
#include "ui/delegates/SplashScreenDelegate.h"
#include "ui/delegates/FloatPointUtilDelegate.h"

QmlRegistrar::QmlRegistrar()
{

}

void QmlRegistrar::registerQml()
{
    qmlRegisterType<FileListModel>("com.dcple.anyvod", 1, 0, "FileListModel");
    qmlRegisterType<PlayListModel>("com.dcple.anyvod", 1, 0, "PlayListModel");
    qmlRegisterType<DTVListModel>("com.dcple.anyvod", 1, 0, "DTVListModel");
    qmlRegisterType<RadioListModel>("com.dcple.anyvod", 1, 0, "RadioListModel");
    qmlRegisterType<EPGListModel>("com.dcple.anyvod", 1, 0, "EPGListModel");
    qmlRegisterType<SearchMediaModel>("com.dcple.anyvod", 1, 0, "SearchMediaModel");
    qmlRegisterType<ManagePlayListModel>("com.dcple.anyvod", 1, 0, "ManagePlayListModel");
    qmlRegisterType<RemoteFileListModel>("com.dcple.anyvod", 1, 0, "RemoteFileListModel");
    qmlRegisterType<ScreenExplorerListModel>("com.dcple.anyvod", 1, 0, "ScreenExplorerListModel");

    qmlRegisterType<MediaPlayerSettingDelegate>("com.dcple.anyvod", 1, 0, "MediaPlayerSettingDelegate");
    qmlRegisterType<DTVDelegate>("com.dcple.anyvod", 1, 0, "DTVDelegate");
    qmlRegisterType<RadioDelegate>("com.dcple.anyvod", 1, 0, "RadioDelegate");
    qmlRegisterType<MainSettingDelegate>("com.dcple.anyvod", 1, 0, "MainSettingDelegate");
    qmlRegisterType<UpdaterDelegate>("com.dcple.anyvod", 1, 0, "UpdaterDelegate");
    qmlRegisterType<SocketDelegate>("com.dcple.anyvod", 1, 0, "SocketDelegate");
    qmlRegisterType<EqualizerDelegate>("com.dcple.anyvod", 1, 0, "EqualizerDelegate");
    qmlRegisterType<TextCodecDelegate>("com.dcple.anyvod", 1, 0, "TextCodecDelegate");
    qmlRegisterType<AutoStartDelegate>("com.dcple.anyvod", 1, 0, "AutoStartDelegate");
    qmlRegisterType<FontInfoDelegate>("com.dcple.anyvod", 1, 0, "FontInfoDelegate");
    qmlRegisterType<TimeUtilDelegate>("com.dcple.anyvod", 1, 0, "TimeUtilDelegate");
    qmlRegisterType<LanguageDelegate>("com.dcple.anyvod", 1, 0, "LanguageDelegate");
    qmlRegisterType<VersionDelegate>("com.dcple.anyvod", 1, 0, "VersionDelegate");
    qmlRegisterType<LicenseDelegate>("com.dcple.anyvod", 1, 0, "LicenseDelegate");
    qmlRegisterType<ToastDelegate>("com.dcple.anyvod", 1, 0, "ToastDelegate");
    qmlRegisterType<StatusBarDelegate>("com.dcple.anyvod", 1, 0, "StatusBarDelegate");
    qmlRegisterType<AudioDeviceDelegate>("com.dcple.anyvod", 1, 0, "AudioDeviceDelegate");
    qmlRegisterType<SPDIFDelegate>("com.dcple.anyvod", 1, 0, "SPDIFDelegate");
    qmlRegisterType<FileTypeDelegate>("com.dcple.anyvod", 1, 0, "FileTypeDelegate");
    qmlRegisterType<CaptureExtDelegate>("com.dcple.anyvod", 1, 0, "CaptureExtDelegate");
    qmlRegisterType<RemoteFileUtilDelegate>("com.dcple.anyvod", 1, 0, "RemoteFileUtilDelegate");
    qmlRegisterType<SplashScreenDelegate>("com.dcple.anyvod", 1, 0, "SplashScreenDelegate");
    qmlRegisterType<FloatPointUtilDelegate>("com.dcple.anyvod", 1, 0, "FloatPointUtilDelegate");

    qmlRegisterType<RenderScreen>("com.dcple.anyvod", 1, 0, "RenderScreen");
    qmlRegisterType<AnyVODEnums>("com.dcple.anyvod", 1, 0, "AnyVODEnums");
    qmlRegisterType<AnyVODGlobal>("com.dcple.anyvod", 1, 0, "AnyVODGlobal");
}
