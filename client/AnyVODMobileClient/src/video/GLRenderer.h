﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "core/TextureInfo.h"
#include "video/ScreenCaptureHelper.h"

#include <QOpenGLFunctions>
#include <QSize>

class MediaPresenter;
class AnyVODWindow;
class RenderScreen;
class QOpenGLFramebufferObject;

class GLRenderer : public QObject, protected QOpenGLFunctions
{
    Q_OBJECT
public:
    enum ColorConversion
    {
        CC_NONE,
        CC_ON,
        CC_OFF,
    };

public:
    GLRenderer(MediaPresenter *presenter, RenderScreen *parent);
    ~GLRenderer();

    void scheduleRebuildShader();
    void scheduleCleanup();
    void scheduleUseColorConversion(ColorConversion use);

    void setViewPortSize(const QSize &size, AnyVODWindow *window);

    ScreenCaptureHelper& getCaptureHelper();
    const ScreenCaptureHelper& getCaptureHelper() const;

public slots:
    void paint();
    void cleanup();
    void setup();

private:
    void deleteTexture(TextureID type);
    void genTexture(TextureID type);
    void genTextureLuma(TextureID type);
    void initTextures();
    void genOffScreen(int size);
    bool isForceRGBA();

    void capturePrologue(const QSize &orgSize);
    void captureEpilogue(bool captureBound, const QSize &orgSize);
    void renderCapturePicture(const QRect &picArea, const QSizeF &texSize);

private:
    static const QStringList BGRA_BLACK_LIST;

private:
    QSize m_viewPortSize;
    MediaPresenter *m_presenter;
    TextureInfo m_texInfo[TEX_COUNT];
    AnyVODWindow *m_window;
    ScreenCaptureHelper m_captureHelper;
    QOpenGLFramebufferObject *m_offScreen;
    RenderScreen *m_parent;
    bool m_wantToCleanup;
    bool m_rebuildShader;
    bool m_isReady;
    bool m_isReadyToRender;
    ColorConversion m_setupColorConversion;
};
