﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "core/Lyrics.h"
#include "ui/UserAspectRatios.h"
#include "video/GLRenderer.h"
#include "sensor/RotationSensor.h"
#include "media/MediaPresenter.h"
#include "media/LastPlay.h"
#include "screensaver/ScreenSaverPreventer.h"
#include "StatusUpdater.h"

#include <QQuickItem>
#include <QMutex>
#include <QUuid>

struct PlayItem;

class PlayListModel;
class SPDIFSampleRates;

class RenderScreen : public QQuickItem
{
    Q_OBJECT
public:
    enum Status
    {
        Started = 0,
        Playing,
        Paused,
        Stopped,
        Ended,
        Exit
    };
    Q_ENUM(Status)

public:
    struct EqualizerItem
    {
        EqualizerItem()
        {
            gain = 0.0f;
        }

        float gain;
    };

public:
    RenderScreen();
    ~RenderScreen();

    Q_INVOKABLE bool open(const QString &path);
    Q_INVOKABLE bool playCurrent();
    Q_INVOKABLE bool playAt(int index);
    Q_INVOKABLE void pause();
    Q_INVOKABLE void resume();
    Q_INVOKABLE void stop();
    Q_INVOKABLE void seek(double time, bool any);
    Q_INVOKABLE void closeAndGoBack();

    Q_INVOKABLE void toggle();

    Q_INVOKABLE QString getSubtitleFileName() const;
    Q_INVOKABLE QString getDirectoryOfSubtitle() const;

    Q_INVOKABLE bool openSubtitle(const QString &filePath);
    Q_INVOKABLE bool saveSubtitleAs(const QString &filePath);
    Q_INVOKABLE bool saveSubtitle();
    Q_INVOKABLE void closeExternalSubtitle();
    Q_INVOKABLE bool existAudioSubtitle();

    Q_INVOKABLE QString getTitle() const;
    Q_INVOKABLE QString getRetainedFilePath() const;
    Q_INVOKABLE QString getGOMSubtitleURL() const;

    Q_INVOKABLE QStringList getVideoStreamDescs() const;
    Q_INVOKABLE QVector<int> getVideoStreamIndice() const;

    Q_INVOKABLE QStringList getAudioStreamDescs() const;
    Q_INVOKABLE QVector<int> getAudioStreamIndice() const;

    Q_INVOKABLE bool isShowDetail() const;
    Q_INVOKABLE void showOptionDesc(const QString &desc);

    Q_INVOKABLE qreal getDefaultVirtual3DDepth() const;
    Q_INVOKABLE int getVRInputSource() const;

    Q_INVOKABLE void setBarrelDistortionCoefficients(const QVector2D &coefficients);
    Q_INVOKABLE QVector2D getBarrelDistortionCoefficients() const;

    Q_INVOKABLE void setPincushionDistortionCoefficients(const QVector2D &coefficients);
    Q_INVOKABLE QVector2D getPincushionDistortionCoefficients() const;

    Q_INVOKABLE void setCameraAngles(const QPointF &angles);
    Q_INVOKABLE QPointF getCameraAngles();

    Q_INVOKABLE void useEqualizer(bool use);

    Q_INVOKABLE void volume(int volume);
    Q_INVOKABLE void mute(bool mute);

    Q_INVOKABLE int getMaxVolume() const;
    Q_INVOKABLE int getVolume() const;

    Q_INVOKABLE double getDuration() const;
    Q_INVOKABLE double getCurrentPosition();
    Q_INVOKABLE bool hasDuration() const;

    Q_INVOKABLE RenderScreen::Status getStatus();

    Q_INVOKABLE bool canCapture() const;

    Q_INVOKABLE bool isEnabledVideo() const;
    Q_INVOKABLE bool isAudio() const;

    Q_INVOKABLE void enableGotoLastPos(bool enable, bool raw);
    Q_INVOKABLE bool isGotoLastPos() const;

    Q_INVOKABLE QString get3DMethodDesc(int method) const;
    Q_INVOKABLE QString get3DMethodCategory(int method) const;
    Q_INVOKABLE QString getSubtitle3DMethodDesc(int method) const;
    Q_INVOKABLE QString getAnaglyphAlgoritmDesc(int algorithm) const;
    Q_INVOKABLE QString getDeinterlaceDesc(int algorithm) const;
    Q_INVOKABLE QString getVRInputSourceDesc(int source) const;
    Q_INVOKABLE QString getDistortionAdjustModeDesc(int mode) const;

    Q_INVOKABLE bool addFOV(float value);
    Q_INVOKABLE void resetFOV();

    Q_INVOKABLE void rewindProper();
    Q_INVOKABLE void forwardProper();

    Q_INVOKABLE bool prev(bool check);
    Q_INVOKABLE bool next(bool check);

    Q_INVOKABLE void captureSingle();

    Q_INVOKABLE bool setScreenOrientation(bool landscape);
    Q_INVOKABLE bool getScreenOrientation() const;

    Q_INVOKABLE void loadSettings();
    Q_INVOKABLE void saveSettings();

    Q_INVOKABLE void loadEqualizer();
    Q_INVOKABLE void saveEqualizer();

    Q_INVOKABLE bool isDTVOpened() const;

    Q_INVOKABLE void setOptionDescY(int y);

    Q_INVOKABLE void setUseCustomControl(bool use);
    Q_INVOKABLE bool getUseCustomControl() const;

    Q_INVOKABLE QString getTimeString(double value) const;

    Q_INVOKABLE void resetLyrics();
    Q_INVOKABLE void exitAtCppSide();
    Q_INVOKABLE void setFullScreen();

    Q_INVOKABLE bool isUseSPDIF() const;

    Q_INVOKABLE QStringList getSubtitleClasses();
    Q_INVOKABLE QStringList getUserAspectRatioDescs() const;

    Q_INVOKABLE int getDistortionAdjustMode() const;

    Q_INVOKABLE void determineToStartRotation();
    Q_INVOKABLE void calibrateRotation();

    Q_INVOKABLE bool isLogined() const;
    Q_INVOKABLE void logout();

    Q_INVOKABLE void showCurrentPos(double pos, double initPos);

    bool openExternal(const QString &path);
    bool play();
    void close();
    void closeAllExternalSubtitles();

    void rewind(double distance);
    void forward(double distance);

    bool isOpened() const;
    bool isPlayOrPause() const;

    QString getSubtitlePath() const;
    QString getArtist() const;
    QString getFilePath() const;
    QString getCaptureExt() const;

    float getTempo() const;
    void setTempo(float percent);

    void setUserAspectRatio(UserAspectRatioInfo &ratio);

    void retreiveExternalSubtitle(const QString &filePath);

    int getCurrentVideoStreamIndex() const;
    QVector<VideoStreamInfo> getVideoStreamInfo() const;

    int getCurrentAudioStreamIndex() const;
    QVector<AudioStreamInfo> getAudioStreamInfo() const;

    void setDeinterlaceMethod(AnyVODEnums::DeinterlaceMethod method);
    void setDeinterlaceAlgorithm(AnyVODEnums::DeinterlaceAlgorithm algorithm);
    AnyVODEnums::DeinterlaceMethod getDeinterlaceMethod();
    AnyVODEnums::DeinterlaceAlgorithm getDeinterlaceAlgorithm();

    void setHAlign(AnyVODEnums::HAlignMethod align);
    AnyVODEnums::HAlignMethod getHAlign() const;
    void setVAlign(AnyVODEnums::VAlignMethod align);
    AnyVODEnums::VAlignMethod getVAlign() const;
    bool isAlignable();

    void showDetail(bool show);

    bool setAudioDevice(int device);
    int getCurrentAudioDevice() const;

    void setScreenRotationDegree(AnyVODEnums::ScreenRotationDegree degree);
    AnyVODEnums::ScreenRotationDegree getScreenRotationDegree() const;

    void showSubtitle(bool show);
    bool isShowSubtitle() const;

    void setSearchSubtitleComplex(bool use);
    bool getSearchSubtitleComplex() const;

    bool existSubtitle();
    bool existFileSubtitle();
    bool existExternalSubtitle();
    bool existAudioSubtitleGender() const;

    QString getCurrentSubtitleClass();
    bool setCurrentSubtitleClass(const QString &className);

    void setVerticalSubtitlePosition(int pos);
    int getVerticalSubtitlePosition();

    void setHorizontalSubtitlePosition(int pos);
    int getHorizontalSubtitlePosition();

    void setVertical3DSubtitleOffset(int offset);
    void setHorizontal3DSubtitleOffset(int offset);

    int getVertical3DSubtitleOffset();
    int getHorizontal3DSubtitleOffset();

    void setVirtual3DDepth(qreal depth);
    qreal getVirtual3DDepth() const;

    void setRepeatStart(double start);
    void setRepeatEnd(double end);
    void setRepeatEnable(bool enable);
    bool getRepeatEnable() const;
    double getRepeatStart() const;
    double getRepeatEnd() const;

    void setSeekKeyFrame(bool keyFrame);
    bool isSeekKeyFrame() const;

    void setSubtitleDirectory(const QStringList &paths, bool prior);

    void set3DMethod(AnyVODEnums::Video3DMethod method);
    AnyVODEnums::Video3DMethod get3DMethod() const;

    void setSubtitle3DMethod(AnyVODEnums::Subtitle3DMethod method);
    AnyVODEnums::Subtitle3DMethod getSubtitle3DMethod() const;

    void setVRInputSource(AnyVODEnums::VRInputSource source);

    void useHeadTracking(bool use);
    bool isUseHeadTracking() const;

    void useDistortion(bool use);
    bool isUseDistortion() const;

    void setDistortionLensCenter(const QVector2D &lensCenter);
    QVector2D getDistortionLensCenter() const;

    void set360DegreeAngles(const QVector3D &angles);
    void reset360DegreeAngles();

    void useNormalizer(bool use);
    bool isUsingNormalizer() const;

    bool isUsingEqualizer() const;

    void useLowerVoice(bool use);
    bool isUsingLowerVoice() const;

    void useHigherVoice(bool use);
    bool isUsingHigherVoice() const;

    void useLowerMusic(bool use);
    bool isUsingLowerMusic() const;

    void setSubtitleOpaque(float opaque);
    float getSubtitleOpaque() const;

    float getSubtitleSize() const;
    void setSubtitleSize(float size);
    void setScheduleRecomputeSubtitleSize();
    void saveSubtitleSize(float size);
    void applySubtitleSize();

    void useHWDecoder(bool enable);
    bool isUseHWDecoder() const;

    void useLowQualityMode(bool enable);
    bool isUseLowQualityMode() const;

    void useFrameDrop(bool enable);
    bool isUseFrameDrop() const;

    void useBufferingMode(bool enable);
    bool isUseBufferingMode() const;

    void use3DFull(bool enable);
    bool isUse3DFull() const;

    bool setPreAmp(float dB);
    float getPreAmp() const;

    bool setEqualizerGain(int band, float gain);
    float getEqualizerGain(int band) const;
    int getBandCount() const;

    bool isEnableSearchSubtitle() const;
    bool isEnableSearchLyrics() const;

    void enableSearchSubtitle(bool enable);
    void enableSearchLyrics(bool enable);

    void enableAutoSaveSearchLyrics(bool enable);
    bool isAutoSaveSearchLyrics() const;

    bool isVideo() const;
    bool isMovieSubtitleVisiable() const;

    void showAlbumJacket(bool show);
    bool isShowAlbumJacket() const;

    void setPlayingMethod(AnyVODEnums::PlayingMethod method);
    void selectPlayingMethod(int method);
    AnyVODEnums::PlayingMethod getPlayingMethod() const;

    void rewind5();
    void forward5();
    void rewind10();
    void forward10();

    void selectVideoStream(int index);
    void selectAudioStream(int index);

    void selectDeinterlacerMethod(int method);
    void selectDeinterlacerAlgorithem(int algorithm);

    void selectCaptureExt(const QString &ext);
    void setCaptureDirectory(const QString &path);
    QString getCaptureDirectory() const;

    void enableSearchSubtitle();
    void enableSearchLyrics();

    void selectAudioDevice(int device);

    void selectSubtitleHAlignMethod(int method);
    void selectSubtitleVAlignMethod(int method);

    void selectUserAspectRatio(int index);
    int getCurrentUserAspectRatioIndex() const;
    int getUserAspectRatioIndex() const;
    void setCustomUserAspectRatio(const QSizeF &size);
    QSizeF getCustomUserAspectRatio() const;

    void useHWDecoder();
    void useFrameDrop();
    void use3DFull();
    void useBufferingMode();

    void showAlbumJacket();

    void select3DMethod(int method);
    void selectVRInputSource(int source);
    void selectAnaglyphAlgorithm(int algorithm);
    void select3DSubtitleMethod(int method);
    void selectScreenRotationDegree(int degree);

    bool setScreenOrientationRestore();
    bool isTablet() const;

    void setSubtitleSync(double sync);
    double getSubtitleSync() const;

    void setAudioSync(double sync);
    double getAudioSync() const;

    void useSubtitleCacheMode(bool use);
    bool isUseSubtitleCacheMode() const;

    bool setSPDIFAudioDevice(int device);
    int getCurrentSPDIFAudioDevice() const;
    QStringList getSPDIFAudioDevices();

    void useSPDIF(bool enable);

    void setSPDIFEncodingMethod(int method);
    int getSPDIFEncodingMethod() const;

    void selectSPDIFSampleRate(int index);
    void setDistortionAdjustMode(int mode);

    void setVerticalScreenOffset(int offset);
    void setHorizontalScreenOffset(int offset);

    bool playNext();
    bool playPrev();

    bool recover();

    bool isPrevEnabled() const;
    bool isNextEnabled() const;
    bool isValid() const;

    bool open(const QString &path, ExtraPlayData &data, const QUuid &unique);

    FilterGraph& getFilterGraph();
    bool configFilterGraph();

    int getCurrentURLPickerIndex() const;
    QString getLargeCoverFilePath() const;
    PlayListModel* getPlayList() const;

    void scheduleRebuildShader();
    void checkGOMSubtitle();
    void tryOpenSubtitle(const QString &filePath);

    void setBluetoothHeadsetConnected(bool connected);
    void resetSPDIF();

    void setBluetoothHeadsetSync(double sync);
    double getBluetoothHeadsetSync() const;

    void getSubtitleDirectory(QStringList *path, bool *prior) const;

    void callStartedEvent();
    void callPlayingEvent();
    void callPausedEvent();
    void callStoppedEvent();
    void callEndEvent();
    void callExitEvent();

public:
    static RenderScreen* getSelf();

public:
    Q_PROPERTY(bool wantToPlay READ wantToPlay WRITE setWantToPlay)
    Q_PROPERTY(bool disableLastPosTmp READ disableLastPosTmp WRITE setDisableLastPosTmp)
    Q_PROPERTY(double gotoTimeTmp READ gotoTimeTmp WRITE setGotoTimeTmp)

private:
    void setScreenKeepOn(bool on);
    bool setKeepActive(bool on);

    bool wantToPlay() const;
    void setWantToPlay(bool want);

    bool disableLastPosTmp() const;
    void setDisableLastPosTmp(bool disable);

    double gotoTimeTmp() const;
    void setGotoTimeTmp(double time);

    void callChanged();

    void navigate(double distance);
    void subtitleSync(double value);
    void audioSync(double value);

    void retreiveLyrics(const QString &filePath);
    void retreiveSubtitleURL(const QString &filePath);

    void updateLastPlay();

    bool updatePlayList(const QString &path);
    QString getURLPickerPlayItems(const QString &address, QVector<PlayItem> *itemVector, QString *playListURL);
    void setPlayList(const QVector<PlayItem> &list);
    void addToPlayList(const QVector<PlayItem> &list);

    bool isRotatable() const;

    void handlePlayingEvent();
    void handleEmptyBufferEvent(QEvent *event);
    void handleAudioSubtitleEvent(QEvent *event);
    void handleAbortEvent(QEvent *event);

#ifdef Q_OS_ANDROID
    void callActivityEvent(const QString &funcName) const;
#endif

private:
    virtual void customEvent(QEvent *event);

signals:
    void viewLyrics();
    void showSubtitlePopup();
    void failedPlay();
    void goBack(const QString &filePath);
    void screenUpdated();
    void playing();
    void resumed();
    void started();
    void paused();
    void stopped();
    void ended();
    void exit();
    void recovered();
    void exitFromRenderer();
    void spdifEnabled();
    void vrInputSourceChanged();
    void needLogin();

private slots:
    void sync();
    void handleWindowChanged(QQuickWindow *window);
    void handleApplicationStateChanged(Qt::ApplicationState state);
    void handleExitFromRenderer();

private:
#ifdef Q_OS_IOS
    static void CALLBACK iosNotifyCallback(DWORD status);
#endif
    static void playingCallback(void *userData);
    static void endedCallback(void *userData);
    static void emptyBufferCallback(void *userData, bool empty);
    static void audioSubtitleCallback(void *userData, const QVector<Lyrics> &lines);
    static void paintCallback(void *userData);
    static void abortCallback(void *userData, int reason);
    static void recoverCallback(void *userData);

private:
    static RenderScreen *m_self;
    static QMutex m_selfLock;

private:
    static const QEvent::Type PLAYING_EVENT;
    static const QEvent::Type EMPTY_BUFFER_EVENT;
    static const QEvent::Type AUDIO_SUBTITLE_EVENT;
    static const QEvent::Type ABORT_EVENT;

private:
    class PlayingEvent : public QEvent
    {
    public:
        PlayingEvent() :
            QEvent(PLAYING_EVENT)
        {

        }
    };

    class EmptyBufferEvent : public QEvent
    {
    public:
        EmptyBufferEvent(bool empty) :
            QEvent(EMPTY_BUFFER_EVENT),
            m_empty(empty)
        {

        }

        bool isEmpty() const { return this->m_empty; }

    private:
        bool m_empty;
    };

    class AudioSubtitleEvent : public QEvent
    {
    public:
        AudioSubtitleEvent(const QVector<Lyrics> &lines) :
            QEvent(AUDIO_SUBTITLE_EVENT),
            m_lines(lines)
        {

        }

        QVector<Lyrics> getLines() const { return this->m_lines; }

    private:
        QVector<Lyrics> m_lines;
    };

    class AbortEvent : public QEvent
    {
    public:
        AbortEvent(int reason) :
            QEvent(ABORT_EVENT),
            m_reason(reason)
        {

        }

        int getReason() const { return this->m_reason; }

    private:
        int m_reason;
    };

private:
    GLRenderer m_renderer;
    MediaPresenter m_presenter;
    QString m_filePath;
    QString m_retainedFilePath;
    bool m_wantToPlay;
    bool m_disableLastPosTmp;
    double m_gotoTimeTmp;
    Status m_status;
    LastPlay m_lastPlay;
    bool m_gotoLastPlay;
    bool m_priorSubtitleDirectory;
    bool m_useSearchSubtitleComplex;
    QStringList m_subtitleDirectory;
    QString m_lastPlayPath;
    AnyVODEnums::PlayingMethod m_playingMethod;
    UserAspectRatios m_userAspectRatios;
    SPDIFSampleRates &m_spdifSampleRates;
    bool m_pausedByInactive;
    bool m_isLandscape;
    bool m_useHeadTracking;
    bool m_appSuspended;
    float m_subtitleSize;
    PlayListModel *m_playList;
    QUuid m_unique;
    QString m_title;
    ExtraPlayData m_playData;
    StatusUpdater m_statusUpdater;
    RotationSensor m_rotationSensor;
    ScreenSaverPreventer m_screenSaverPreventer;
#ifdef Q_OS_IOS
    bool m_isPausedByIOSNotify;
#endif
    QQuickItem *m_lyrics1;
    QQuickItem *m_lyrics2;
    QQuickItem *m_lyrics3;
    QQuickItem *m_empty;
};

Q_DECLARE_METATYPE(RenderScreen::EqualizerItem)
QDataStream& operator << (QDataStream &out, const RenderScreen::EqualizerItem &item);
QDataStream& operator >> (QDataStream &in, RenderScreen::EqualizerItem &item);
