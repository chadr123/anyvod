﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "DTVDelegate.h"
#include "models/DTVListModel.h"
#include "setting/Settings.h"
#include "setting/SettingsFactory.h"

#include <QSettings>

DTVDelegate::DTVDelegate() :
    m_reader(DTVReader::getInstance()),
    m_model(nullptr)

{

}

DTVDelegate::~DTVDelegate()
{

}

void DTVDelegate::init()
{
    this->findModel();
}

void DTVDelegate::scan(int adapter, int systemType, int country, int start, int end)
{
    this->m_model->clear();
    this->m_model->update(adapter, (DTVReaderInterface::SystemType)systemType, (QLocale::Country)country, start, end);
}

void DTVDelegate::stop()
{
    this->m_model->stop();
}

void DTVDelegate::save()
{
    this->m_reader.setScannedChannels(this->m_model->getChannelInfo());
}

QString DTVDelegate::getPath(int index)
{
#if defined Q_OS_RASPBERRY_PI
    QVector<DTVReader::ChannelInfo> infos = this->m_model->getChannelInfo();

    return DTVReader::makeDTVPath(infos[index]);
#else
    (void)index;

    return QString();
#endif
}

QVector<int> DTVDelegate::getAdapterInfoValues() const
{
    QVector<int> ret;
    QVector<DTVReaderInterface::AdapterInfo> infos;

    if (this->m_reader.getAdapterList(&infos))
    {
        for (const DTVReaderInterface::AdapterInfo &info : qAsConst(infos))
            ret.append(info.index);

        return ret;
    }
    else
    {
        return QVector<int>();
    }
}

QStringList DTVDelegate::getAdapterInfos() const
{
    QStringList ret;
    QVector<DTVReaderInterface::AdapterInfo> infos;

    if (this->m_reader.getAdapterList(&infos))
    {
        for (const DTVReaderInterface::AdapterInfo &info : qAsConst(infos))
            ret.append(info.name);

        return ret;
    }
    else
    {
        return QStringList();
    }
}

int DTVDelegate::getAdapterInfoValue(int index) const
{
    DTVReaderInterface::AdapterInfo info;

    if (this->m_reader.getAdapterInfo(index, &info))
        return info.index;
    else
        return -1;
}

QStringList DTVDelegate::getSystemTypeInfoDescs() const
{
    QStringList descs;
    const auto infos = this->getSystemTypeList();

    for (const DTVReaderInterface::SystemTypeInfo &info : infos)
        descs.append(info.name);

    return descs;
}

QVector<int> DTVDelegate::getSystemTypeInfoValues() const
{
    QVector<int> values;
    const auto infos = this->getSystemTypeList();

    for (const DTVReaderInterface::SystemTypeInfo &info : infos)
        values.append(info.type);

    return values;
}

QStringList DTVDelegate::getCountryInfoDescs() const
{
    QStringList descs;
    const auto infos = this->getCountryList();

    for (const DTVReaderInterface::CountryInfo &info : infos)
        descs.append(info.name);

    return descs;
}

QVector<int> DTVDelegate::getCountryInfoValues() const
{
    QVector<int> values;
    const auto infos = this->getCountryList();

    for (const DTVReaderInterface::CountryInfo &info : infos)
        values.append(info.country);

    return values;
}

int DTVDelegate::getCurrentAdapterValue()
{
    return this->getAdapterInfoValue(0);
}

int DTVDelegate::getCurrentSystemTypeValue() const
{
    QLocale::Country country;
    DTVReaderInterface::SystemType type;

    this->m_reader.getChannelCountry(&country, &type);

    return type;
}

int DTVDelegate::getCurrentCountryValue() const
{
    QLocale::Country country;
    DTVReaderInterface::SystemType type;

    this->m_reader.getChannelCountry(&country, &type);

    return country;
}

bool DTVDelegate::setChannelCountry(int country, int type)
{
    return this->m_reader.setChannelCountry((QLocale::Country)country, (DTVReaderInterface::SystemType)type);
}

int DTVDelegate::getChannelCount() const
{
    QVector<DTVChannelMap::Channel> channels;

    this->m_reader.getChannels(&channels);

    return channels.count();
}

int DTVDelegate::getChannelFirst() const
{
    QVector<DTVChannelMap::Channel> channels;

    this->m_reader.getChannels(&channels);

    return channels.first().channel;
}

int DTVDelegate::getChannelLast() const
{
    QVector<DTVChannelMap::Channel> channels;

    this->m_reader.getChannels(&channels);

    return channels.last().channel;
}

QVector<DTVReaderInterface::SystemTypeInfo> DTVDelegate::getSystemTypeList() const
{
    QVector<DTVReaderInterface::SystemTypeInfo> list;

    for (int i = DTVReaderInterface::ST_NONE + 1; i < DTVReaderInterface::ST_COUNT; i++)
    {
        QString name = this->m_reader.systemTypeToString((DTVReaderInterface::SystemType)i);

        if (!name.isEmpty())
        {
            DTVReaderInterface::SystemTypeInfo item;

            item.name = name;
            item.type = (DTVReaderInterface::SystemType)i;

            list.append(item);
        }
    }

    return list;
}

QVector<DTVReaderInterface::CountryInfo> DTVDelegate::getCountryList() const
{
    QVector<DTVReaderInterface::CountryInfo> list;

    for (int i = QLocale::AnyCountry + 1; i < QLocale::LastCountry; i++)
    {
        QString name = QLocale::countryToString((QLocale::Country)i);

        if (!name.isEmpty())
        {
            DTVReaderInterface::CountryInfo item;

            item.name = name;
            item.country = (QLocale::Country)i;

            list.append(item);
        }
    }

    return list;
}

void DTVDelegate::findModel()
{
    if (!this->m_model)
    {
        QVector<DTVReader::ChannelInfo> list;

        this->m_reader.getScannedChannels(&list);

        this->m_model = this->findChild<DTVListModel*>("dtvListModel");

        this->m_model->setReader(&this->m_reader);
        this->m_model->setChannelInfo(list);
    }
}
