﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "MediaPlayerSettingDelegate.h"
#include "audio/SPDIFSampleRates.h"
#include "ui/RenderScreen.h"
#include "parsers/subtitle/GlobalSubtitleCodecName.h"
#include "utils/FloatPointUtils.h"
#include "utils/AnyVODEnumsUtils.h"

MediaPlayerSettingDelegate::MediaPlayerSettingDelegate() :
    m_screen(nullptr)
{

}

void MediaPlayerSettingDelegate::setSetting(int key, QVariant value)
{
    this->initParent();

    if (!this->m_screen)
        return;

    ShaderCompositer &shader = ShaderCompositer::getInstance();

    switch (key)
    {
        case AnyVODEnums::SK_SHARPLY:
            shader.useSharply(value.toBool());
            this->m_screen->scheduleRebuildShader();
            break;
        case AnyVODEnums::SK_SHARPEN:
            shader.useSharpen(value.toBool());
            this->m_screen->scheduleRebuildShader();
            break;
        case AnyVODEnums::SK_SOFTEN:
            shader.useSoften(value.toBool());
            this->m_screen->scheduleRebuildShader();
            break;
        case AnyVODEnums::SK_HISTOGRAM_EQ:
            this->m_screen->getFilterGraph().setEnableHistEQ(value.toBool());
            this->m_screen->configFilterGraph();
            break;
        case AnyVODEnums::SK_DEBAND:
            this->m_screen->getFilterGraph().setEnableDeBand(value.toBool());
            this->m_screen->configFilterGraph();
            break;
        case AnyVODEnums::SK_DEBLOCK:
            this->m_screen->getFilterGraph().setEnableDeBlock(value.toBool());
            this->m_screen->configFilterGraph();
            break;
        case AnyVODEnums::SK_VIDEO_NORMALIZE:
            this->m_screen->getFilterGraph().setEnableNormalize(value.toBool());
            this->m_screen->configFilterGraph();
            break;
        case AnyVODEnums::SK_HIGH_QUALITY_3D_DENOISE:
            this->m_screen->getFilterGraph().setEnableHighQuality3DDenoise(value.toBool());
            this->m_screen->configFilterGraph();
            break;
        case AnyVODEnums::SK_ATA_DENOISE:
            this->m_screen->getFilterGraph().setEnableATADenoise(value.toBool());
            this->m_screen->configFilterGraph();
            break;
        case AnyVODEnums::SK_OW_DENOISE:
            this->m_screen->getFilterGraph().setEnableOWDenoise(value.toBool());
            this->m_screen->configFilterGraph();
            break;
        case AnyVODEnums::SK_NLMEANS_DENOISE:
            this->m_screen->getFilterGraph().setEnableNLMeansDenoise(value.toBool());
            this->m_screen->configFilterGraph();
            break;
        case AnyVODEnums::SK_VAGUE_DENOISE:
            this->m_screen->getFilterGraph().setEnableVagueDenoise(value.toBool());
            this->m_screen->configFilterGraph();
            break;
        case AnyVODEnums::SK_LEFT_RIGHT_INVERT:
            shader.useLeftRightInvert(value.toBool());
            this->m_screen->scheduleRebuildShader();
            break;
        case AnyVODEnums::SK_TOP_BOTTOM_INVERT:
            shader.useTopBottomInvert(value.toBool());
            this->m_screen->scheduleRebuildShader();
            break;
        case AnyVODEnums::SK_USE_HW_DECODER:
            this->m_screen->useHWDecoder(value.toBool());
            break;
        case AnyVODEnums::SK_USE_FRAME_DROP:
            this->m_screen->useFrameDrop(value.toBool());
            break;
        case AnyVODEnums::SK_SHOW_ALBUM_JACKET:
            this->m_screen->showAlbumJacket(value.toBool());
            break;
        case AnyVODEnums::SK_DEINTERLACE_METHOD_ORDER:
            this->m_screen->selectDeinterlacerMethod(value.toInt());
            break;
        case AnyVODEnums::SK_DEINTERLACE_ALGORITHM_ORDER:
            this->m_screen->selectDeinterlacerAlgorithem(value.toInt());
            break;
        case AnyVODEnums::SK_USER_ASPECT_RATIO_ORDER:
            this->m_screen->selectUserAspectRatio(value.toInt());
            break;
        case AnyVODEnums::SK_USER_ASPECT_RATIO:
            this->m_screen->setCustomUserAspectRatio(value.toSizeF());
            this->m_screen->selectUserAspectRatio(this->m_screen->getUserAspectRatioIndex());
            break;
        case AnyVODEnums::SK_SCREEN_ROTATION_DEGREE_ORDER:
            this->m_screen->selectScreenRotationDegree(value.toInt());
            break;
        case AnyVODEnums::SK_BRIGHTNESS:
            shader.setBrightness(value.toDouble());
            break;
        case AnyVODEnums::SK_SATURATION:
            shader.setSaturation(value.toDouble());
            break;
        case AnyVODEnums::SK_HUE:
            shader.setHue(value.toDouble() / 360.0);
            break;
        case AnyVODEnums::SK_CONTRAST:
            shader.setContrast(value.toDouble());
            break;
        case AnyVODEnums::SK_SELECT_CAPTURE_EXT_ORDER:
            this->m_screen->selectCaptureExt(value.toString());
            break;
        case AnyVODEnums::SK_USE_3D_FULL:
            this->m_screen->use3DFull(value.toBool());
            break;
        case AnyVODEnums::SK_3D_VIDEO_METHOD_ORDER:
            this->m_screen->select3DMethod(value.toInt());
            this->m_screen->scheduleRebuildShader();
            break;
        case AnyVODEnums::SK_ANAGLYPH_ALGORITHM_ORDER:
            this->m_screen->selectAnaglyphAlgorithm(value.toInt());
            break;
        case AnyVODEnums::SK_LAST_PLAY:
            this->m_screen->enableGotoLastPos(value.toBool(), false);
            break;
        case AnyVODEnums::SK_SEEK_KEYFRAME:
            this->m_screen->setSeekKeyFrame(value.toBool());
            break;
        case AnyVODEnums::SK_USE_BUFFERING_MODE:
            this->m_screen->useBufferingMode(value.toBool());
            break;
        case AnyVODEnums::SK_PLAY_ORDER:
            this->m_screen->selectPlayingMethod(value.toInt());
            break;
        case AnyVODEnums::SK_PLAYBACK_SPEED:
            this->m_screen->setTempo(value.toFloat());
            break;
        case AnyVODEnums::SK_SUBTITLE_TOGGLE:
            this->m_screen->showSubtitle(value.toBool());
            break;
        case AnyVODEnums::SK_SEARCH_SUBTITLE_COMPLEX:
            this->m_screen->setSearchSubtitleComplex(value.toBool());
            break;
        case AnyVODEnums::SK_ENABLE_SEARCH_SUBTITLE:
            this->m_screen->enableSearchSubtitle();
            break;
        case AnyVODEnums::SK_ENABLE_SEARCH_LYRICS:
            this->m_screen->enableSearchLyrics();
            break;
        case AnyVODEnums::SK_SUBTITLE_OPAQUE:
            this->m_screen->setSubtitleOpaque(value.toFloat() / 100.0f);
            break;
        case AnyVODEnums::SK_SUBTITLE_SIZE:
            this->m_screen->saveSubtitleSize(value.toFloat() / 100.0f);
            break;
        case AnyVODEnums::SK_SUBTITLE_SYNC:
            this->m_screen->setSubtitleSync(value.toDouble());
            break;
        case AnyVODEnums::SK_LEFTRIGHT_SUBTITLE_POSITION:
            this->m_screen->setHorizontalSubtitlePosition(-value.toInt());
            break;
        case AnyVODEnums::SK_UPDOWN_SUBTITLE_POSITION:
            this->m_screen->setVerticalSubtitlePosition(value.toInt());
            break;
        case AnyVODEnums::SK_SUBTITLE_HALIGN_ORDER:
            this->m_screen->selectSubtitleHAlignMethod(value.toInt());
            break;
        case AnyVODEnums::SK_SUBTITLE_VALIGN_ORDER:
            this->m_screen->selectSubtitleVAlignMethod(value.toInt());
            break;
        case AnyVODEnums::SK_OPEN_TEXT_ENCODING:
            GlobalSubtitleCodecName::getInstance().setCodecName(value.toString());
            break;
        case AnyVODEnums::SK_SUBTITLE_LANGUAGE_ORDER:
            this->m_screen->setCurrentSubtitleClass(value.toString());
            break;
        case AnyVODEnums::SK_SUBTITLE_CACHE_MODE:
            this->m_screen->useSubtitleCacheMode(value.toBool());
            break;
        case AnyVODEnums::SK_UPDOWN_3D_SUBTITLE_OFFSET:
            this->m_screen->setVertical3DSubtitleOffset(value.toInt());
            break;
        case AnyVODEnums::SK_LEFTRIGHT_3D_SUBTITLE_OFFSET:
            this->m_screen->setHorizontal3DSubtitleOffset(value.toInt());
            break;
        case AnyVODEnums::SK_3D_SUBTITLE_METHOD_ORDER:
            this->m_screen->select3DSubtitleMethod(value.toInt());
            break;
        case AnyVODEnums::SK_AUDIO_NORMALIZE:
            this->m_screen->useNormalizer(value.toBool());
            break;
        case AnyVODEnums::SK_LOWER_VOICE:
            this->m_screen->useLowerVoice(value.toBool());
            break;
        case AnyVODEnums::SK_HIGHER_VOICE:
            this->m_screen->useHigherVoice(value.toBool());
            break;
        case AnyVODEnums::SK_LOWER_MUSIC:
            this->m_screen->useLowerMusic(value.toBool());
            break;
        case AnyVODEnums::SK_AUDIO_SYNC:
            this->m_screen->setAudioSync(value.toDouble());
            break;
        case AnyVODEnums::SK_AUDIO_ORDER:
            this->m_screen->selectAudioStream(value.toInt());
            break;
        case AnyVODEnums::SK_VIDEO_ORDER:
            this->m_screen->selectVideoStream(value.toInt());
            break;
        case AnyVODEnums::SK_DETAIL:
            this->m_screen->showDetail(value.toBool());
            break;
        case AnyVODEnums::SK_REPEAT_RANGE_START:
            if (value.toBool())
                this->m_screen->setRepeatStart(this->m_screen->getCurrentPosition());
            else
                this->m_screen->setRepeatStart(0.0);
            break;
        case AnyVODEnums::SK_REPEAT_RANGE_END:
            if (value.toBool())
                this->m_screen->setRepeatEnd(this->m_screen->getCurrentPosition());
            else
                this->m_screen->setRepeatEnd(0.0);
            break;
        case AnyVODEnums::SK_REPEAT_RANGE_ENABLE:
            this->m_screen->setRepeatEnable(value.toBool());
            break;
        case AnyVODEnums::SK_SELECT_CAPTURE_DIRECTORY:
            this->m_screen->setCaptureDirectory(value.toString());
            break;
        case AnyVODEnums::SK_USE_LOW_QUALITY_MODE:
            this->m_screen->useLowQualityMode(value.toBool());
            break;
        case AnyVODEnums::SK_AUDIO_DEVICE_ORDER:
            this->m_screen->selectAudioDevice(value.toInt());
            break;
        case AnyVODEnums::SK_SPDIF_AUDIO_DEVICE_ORDER:
            this->m_screen->setSPDIFAudioDevice(value.toInt());
            break;
        case AnyVODEnums::SK_USE_SPDIF_ENCODING_ORDER:
            this->m_screen->setSPDIFEncodingMethod(value.toInt());
            break;
        case AnyVODEnums::SK_SPDIF_SAMPLE_RATE_ORDER:
            this->m_screen->selectSPDIFSampleRate(value.toInt());
            break;
        case AnyVODEnums::SK_USE_SPDIF:
            this->m_screen->useSPDIF(value.toBool());
            break;
        case AnyVODEnums::SK_VR_INPUT_SOURCE_ORDER:
            this->m_screen->selectVRInputSource(value.toInt());
            break;
        case AnyVODEnums::SK_HEAD_TRACKING:
            this->m_screen->useHeadTracking(value.toBool());
            break;
        case AnyVODEnums::SK_VR_USE_DISTORTION:
            this->m_screen->useDistortion(value.toBool());
            break;
        case AnyVODEnums::SK_VR_BARREL_COEFFICIENTS:
            this->m_screen->setBarrelDistortionCoefficients(QVector2D(value.toPointF()));
            break;
        case AnyVODEnums::SK_VR_PINCUSHION_COEFFICIENTS:
            this->m_screen->setPincushionDistortionCoefficients(QVector2D(value.toPointF()));
            break;
        case AnyVODEnums::SK_VR_LENS_CENTER:
            this->m_screen->setDistortionLensCenter(QVector2D(value.toPointF()));
            break;
        case AnyVODEnums::SK_USE_360_DEGREE:
            shader.use360Degree(value.toBool());
            this->m_screen->scheduleRebuildShader();
            break;
        case AnyVODEnums::SK_DISTORION_ADJUST_MODE:
            this->m_screen->setDistortionAdjustMode(value.toInt());
            break;
        case AnyVODEnums::SK_VIRTUAL_3D_DEPTH:
            this->m_screen->setVirtual3DDepth(value.toReal());
            break;
        case AnyVODEnums::SK_AUTO_SAVE_SEARCH_LYRICS:
            this->m_screen->enableAutoSaveSearchLyrics(value.toBool());
            break;
        case AnyVODEnums::SK_BLUETOOTH_AUDIO_SYNC:
            this->m_screen->setBluetoothHeadsetSync(value.toDouble());
            break;
        case AnyVODEnums::SK_PROJECTION_TYPE_ORDER:
            shader.setVideo360Type((AnyVODEnums::Video360Type)value.toInt());
            this->m_screen->scheduleRebuildShader();
            break;
        default:
            break;
    }
}

QVariant MediaPlayerSettingDelegate::getSetting(int key)
{
    QVariant val = false;

    this->initParent();

    if (!this->m_screen)
        return val;

    ShaderCompositer &shader = ShaderCompositer::getInstance();

    switch (key)
    {
        case AnyVODEnums::SK_SHARPLY:
            val = shader.isUsingSharply();
            break;
        case AnyVODEnums::SK_SHARPEN:
            val = shader.isUsingSharpen();
            break;
        case AnyVODEnums::SK_SOFTEN:
            val = shader.isUsingSoften();
            break;
        case AnyVODEnums::SK_HISTOGRAM_EQ:
            val = this->m_screen->getFilterGraph().getEnableHistEQ();
            break;
        case AnyVODEnums::SK_DEBAND:
            val = this->m_screen->getFilterGraph().getEnableDeBand();
            break;
        case AnyVODEnums::SK_DEBLOCK:
            val = this->m_screen->getFilterGraph().getEnableDeBlock();
            break;
        case AnyVODEnums::SK_VIDEO_NORMALIZE:
            val = this->m_screen->getFilterGraph().getEnableNormalize();
            break;
        case AnyVODEnums::SK_HIGH_QUALITY_3D_DENOISE:
            val = this->m_screen->getFilterGraph().getEnableHighQuality3DDenoise();
            break;
        case AnyVODEnums::SK_ATA_DENOISE:
            val = this->m_screen->getFilterGraph().getEnableATADenoise();
            break;
        case AnyVODEnums::SK_OW_DENOISE:
            val = this->m_screen->getFilterGraph().getEnableOWDenoise();
            break;
        case AnyVODEnums::SK_NLMEANS_DENOISE:
            val = this->m_screen->getFilterGraph().getEnableNLMeansDenoise();
            break;
        case AnyVODEnums::SK_VAGUE_DENOISE:
            val = this->m_screen->getFilterGraph().getEnableVagueDenoise();
            break;
        case AnyVODEnums::SK_LEFT_RIGHT_INVERT:
            val = shader.isUsingLeftRightInvert();
            break;
        case AnyVODEnums::SK_TOP_BOTTOM_INVERT:
            val = shader.isUsingTopBottomInvert();
            break;
        case AnyVODEnums::SK_USE_HW_DECODER:
            val = this->m_screen->isUseHWDecoder();
            break;
        case AnyVODEnums::SK_USE_FRAME_DROP:
            val = this->m_screen->isUseFrameDrop();
            break;
        case AnyVODEnums::SK_SHOW_ALBUM_JACKET:
            val = this->m_screen->isShowAlbumJacket();
            break;
        case AnyVODEnums::SK_DEINTERLACE_METHOD_ORDER:
            val = (int)this->m_screen->getDeinterlaceMethod();
            break;
        case AnyVODEnums::SK_DEINTERLACE_ALGORITHM_ORDER:
            val = (int)this->m_screen->getDeinterlaceAlgorithm();
            break;
        case AnyVODEnums::SK_USER_ASPECT_RATIO_ORDER:
            val = this->m_screen->getCurrentUserAspectRatioIndex();
            break;
        case AnyVODEnums::SK_USER_ASPECT_RATIO:
            val = this->m_screen->getCustomUserAspectRatio();
            break;
        case AnyVODEnums::SK_SCREEN_ROTATION_DEGREE_ORDER:
            val = (int)this->m_screen->getScreenRotationDegree();
            break;
        case AnyVODEnums::SK_BRIGHTNESS:
            val = shader.getBrightness();
            break;
        case AnyVODEnums::SK_SATURATION:
            val = shader.getSaturation();
            break;
        case AnyVODEnums::SK_HUE:
            val = shader.getHue() * 360.0;
            break;
        case AnyVODEnums::SK_CONTRAST:
            val = shader.getContrast();
            break;
        case AnyVODEnums::SK_SELECT_CAPTURE_EXT_ORDER:
            val = this->m_screen->getCaptureExt();
            break;
        case AnyVODEnums::SK_USE_3D_FULL:
            val = this->m_screen->isUse3DFull();
            break;
        case AnyVODEnums::SK_3D_VIDEO_METHOD_ORDER:
            val = (int)this->m_screen->get3DMethod();
            break;
        case AnyVODEnums::SK_ANAGLYPH_ALGORITHM_ORDER:
            val = (int)shader.getAnaglyphAlgorithm();
            break;
        case AnyVODEnums::SK_LAST_PLAY:
            val = this->m_screen->isGotoLastPos();
            break;
        case AnyVODEnums::SK_SEEK_KEYFRAME:
            val = this->m_screen->isSeekKeyFrame();
            break;
        case AnyVODEnums::SK_USE_BUFFERING_MODE:
            val = this->m_screen->isUseBufferingMode();
            break;
        case AnyVODEnums::SK_PLAY_ORDER:
            val = (int)this->m_screen->getPlayingMethod();
            break;
        case AnyVODEnums::SK_PLAYBACK_SPEED:
            val = this->m_screen->getTempo();
            break;
        case AnyVODEnums::SK_SUBTITLE_TOGGLE:
            val = this->m_screen->isShowSubtitle();
            break;
        case AnyVODEnums::SK_SEARCH_SUBTITLE_COMPLEX:
            val = this->m_screen->getSearchSubtitleComplex();
            break;
        case AnyVODEnums::SK_ENABLE_SEARCH_SUBTITLE:
            val = this->m_screen->isEnableSearchSubtitle();
            break;
        case AnyVODEnums::SK_ENABLE_SEARCH_LYRICS:
            val = this->m_screen->isEnableSearchLyrics();
            break;
        case AnyVODEnums::SK_SUBTITLE_OPAQUE:
            val = this->m_screen->getSubtitleOpaque() * 100.0f;
            break;
        case AnyVODEnums::SK_SUBTITLE_SIZE:
            val = this->m_screen->getSubtitleSize() * 100.0f;
            break;
        case AnyVODEnums::SK_SUBTITLE_SYNC:
            val = this->m_screen->getSubtitleSync();
            break;
        case AnyVODEnums::SK_LEFTRIGHT_SUBTITLE_POSITION:
            val = -this->m_screen->getHorizontalSubtitlePosition();
            break;
        case AnyVODEnums::SK_UPDOWN_SUBTITLE_POSITION:
            val = this->m_screen->getVerticalSubtitlePosition();
            break;
        case AnyVODEnums::SK_SUBTITLE_HALIGN_ORDER:
            val = (int)this->m_screen->getHAlign();
            break;
        case AnyVODEnums::SK_SUBTITLE_VALIGN_ORDER:
            val = (int)this->m_screen->getVAlign();
            break;
        case AnyVODEnums::SK_OPEN_TEXT_ENCODING:
            val = GlobalSubtitleCodecName::getInstance().getCodecName();
            break;
        case AnyVODEnums::SK_SUBTITLE_LANGUAGE_ORDER:
            val = this->m_screen->getCurrentSubtitleClass();
            break;
        case AnyVODEnums::SK_SUBTITLE_CACHE_MODE:
            val = this->m_screen->isUseSubtitleCacheMode();
            break;
        case AnyVODEnums::SK_UPDOWN_3D_SUBTITLE_OFFSET:
            val = this->m_screen->getVertical3DSubtitleOffset();
            break;
        case AnyVODEnums::SK_LEFTRIGHT_3D_SUBTITLE_OFFSET:
            val = this->m_screen->getHorizontal3DSubtitleOffset();
            break;
        case AnyVODEnums::SK_3D_SUBTITLE_METHOD_ORDER:
            val = (int)this->m_screen->getSubtitle3DMethod();
            break;
        case AnyVODEnums::SK_AUDIO_NORMALIZE:
            val = this->m_screen->isUsingNormalizer();
            break;
        case AnyVODEnums::SK_LOWER_VOICE:
            val = this->m_screen->isUsingLowerVoice();
            break;
        case AnyVODEnums::SK_HIGHER_VOICE:
            val = this->m_screen->isUsingHigherVoice();
            break;
        case AnyVODEnums::SK_LOWER_MUSIC:
            val = this->m_screen->isUsingLowerMusic();
            break;
        case AnyVODEnums::SK_AUDIO_SYNC:
            val = this->m_screen->getAudioSync();
            break;
        case AnyVODEnums::SK_AUDIO_ORDER:
            val = this->m_screen->getCurrentAudioStreamIndex();
            break;
        case AnyVODEnums::SK_VIDEO_ORDER:
            val = this->m_screen->getCurrentVideoStreamIndex();
            break;
        case AnyVODEnums::SK_DETAIL:
            val = this->m_screen->isShowDetail();
            break;
        case AnyVODEnums::SK_REPEAT_RANGE_START:
            val = FloatPointUtils::zeroDouble(this->m_screen->getRepeatStart());
            break;
        case AnyVODEnums::SK_REPEAT_RANGE_END:
            val = FloatPointUtils::zeroDouble(this->m_screen->getRepeatEnd());
            break;
        case AnyVODEnums::SK_REPEAT_RANGE_ENABLE:
            val = this->m_screen->getRepeatEnable();
            break;
        case AnyVODEnums::SK_SELECT_CAPTURE_DIRECTORY:
            val = this->m_screen->getCaptureDirectory();
            break;
        case AnyVODEnums::SK_OTHER_QUALITY:
            val = this->m_screen->getCurrentURLPickerIndex();
            break;
        case AnyVODEnums::SK_USE_LOW_QUALITY_MODE:
            val = this->m_screen->isUseLowQualityMode();
            break;
        case AnyVODEnums::SK_AUDIO_DEVICE_ORDER:
            val = this->m_screen->getCurrentAudioDevice();
            break;
        case AnyVODEnums::SK_SPDIF_AUDIO_DEVICE_ORDER:
            val = this->m_screen->getCurrentSPDIFAudioDevice();
            break;
        case AnyVODEnums::SK_USE_SPDIF_ENCODING_ORDER:
            val = this->m_screen->getSPDIFEncodingMethod();
            break;
        case AnyVODEnums::SK_SPDIF_SAMPLE_RATE_ORDER:
            val = SPDIFSampleRates::getInstance().getSelectedSampleRate();
            break;
        case AnyVODEnums::SK_USE_SPDIF:
            val = this->m_screen->isUseSPDIF();
            break;
        case AnyVODEnums::SK_VR_INPUT_SOURCE_ORDER:
            val = this->m_screen->getVRInputSource();
            break;
        case AnyVODEnums::SK_HEAD_TRACKING:
            val = this->m_screen->isUseHeadTracking();
            break;
        case AnyVODEnums::SK_VR_USE_DISTORTION:
            val = this->m_screen->isUseDistortion();
            break;
        case AnyVODEnums::SK_VR_BARREL_COEFFICIENTS:
            val = this->m_screen->getBarrelDistortionCoefficients().toPointF();
            break;
        case AnyVODEnums::SK_VR_PINCUSHION_COEFFICIENTS:
            val = this->m_screen->getPincushionDistortionCoefficients().toPointF();
            break;
        case AnyVODEnums::SK_VR_LENS_CENTER:
            val = this->m_screen->getDistortionLensCenter().toPointF();
            break;
        case AnyVODEnums::SK_USE_360_DEGREE:
            val = shader.is360Degree();
            break;
        case AnyVODEnums::SK_DISTORION_ADJUST_MODE:
            val = this->m_screen->getDistortionAdjustMode();
            break;
        case AnyVODEnums::SK_VIRTUAL_3D_DEPTH:
            val = this->m_screen->getVirtual3DDepth();
            break;
        case AnyVODEnums::SK_AUTO_SAVE_SEARCH_LYRICS:
            val = this->m_screen->isAutoSaveSearchLyrics();
            break;
        case AnyVODEnums::SK_BLUETOOTH_AUDIO_SYNC:
            val = this->m_screen->getBluetoothHeadsetSync();
            break;
        case AnyVODEnums::SK_PROJECTION_TYPE_ORDER:
            val = shader.getVideo360Type();
            break;
        default:
            break;
    }

    return val;
}

bool MediaPlayerSettingDelegate::isSupported(int key) const
{
    return AnyVODEnumsUtils::isSupported((AnyVODEnums::ShortcutKey)key);
}

void MediaPlayerSettingDelegate::initParent()
{
    if (this->m_screen)
        return;

    QQuickItem *parent = this->parentItem();

    if (!parent || parent->objectName() != "screen")
        return;

    this->m_screen = (RenderScreen*)parent;
}
