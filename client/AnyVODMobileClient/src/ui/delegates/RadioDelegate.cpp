﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "RadioDelegate.h"
#include "models/RadioListModel.h"
#include "setting/Settings.h"
#include "setting/SettingsFactory.h"

#include <QSettings>

RadioDelegate::RadioDelegate() :
    m_reader(RadioReader::getInstance()),
    m_model(nullptr)

{

}

RadioDelegate::~RadioDelegate()
{

}

void RadioDelegate::init()
{
    this->findModel();
}

void RadioDelegate::scan(int adapter, int demodulatorType, int country, int start, int end)
{
    this->m_model->clear();
    this->m_model->update(adapter, (RadioReaderInterface::DemodulatorType)demodulatorType, (QLocale::Country)country, start, end);
}

void RadioDelegate::stop()
{
    this->m_model->stop();
}

void RadioDelegate::save()
{
    this->m_reader.setScannedChannels(this->m_model->getChannelInfo());
}

QString RadioDelegate::getPath(int index)
{
#if defined Q_OS_RASPBERRY_PI
    QVector<RadioReader::ChannelInfo> infos = this->m_model->getChannelInfo();

    return RadioReader::makeRadioPath(infos[index]);
#else
    (void)index;

    return QString();
#endif
}

QVector<int> RadioDelegate::getAdapterInfoValues() const
{
    QVector<int> ret;
    QVector<RadioReaderInterface::AdapterInfo> infos;

    if (this->m_reader.getAdapterList(&infos))
    {
        for (const RadioReaderInterface::AdapterInfo &info : qAsConst(infos))
            ret.append(info.data.index);

        return ret;
    }
    else
    {
        return QVector<int>();
    }
}

QStringList RadioDelegate::getAdapterInfos() const
{
    QStringList ret;
    QVector<RadioReaderInterface::AdapterInfo> infos;

    if (this->m_reader.getAdapterList(&infos))
    {
        for (const RadioReaderInterface::AdapterInfo &info : qAsConst(infos))
            ret.append(info.name);

        return ret;
    }
    else
    {
        return QStringList();
    }
}

int RadioDelegate::getAdapterInfoValue(int index) const
{
    RadioReaderInterface::AdapterInfo info;

    if (this->m_reader.getAdapterInfo(index, &info))
        return info.data.index;
    else
        return -1;
}

QStringList RadioDelegate::getDemodulatorTypeInfoDescs() const
{
    QStringList descs;
    const auto infos = this->getDemodulatorTypeList();

    for (const RadioReaderInterface::DemodulatorTypeInfo &info : infos)
        descs.append(info.name);

    return descs;
}

QVector<int> RadioDelegate::getDemodulatorTypeInfoValues() const
{
    QVector<int> values;
    const auto infos = this->getDemodulatorTypeList();

    for (const RadioReaderInterface::DemodulatorTypeInfo &info : infos)
        values.append(info.type);

    return values;
}

QStringList RadioDelegate::getCountryInfoDescs() const
{
    QStringList descs;
    const auto infos = this->getCountryList();

    for (const RadioReaderInterface::CountryInfo &info : infos)
        descs.append(info.name);

    return descs;
}

QVector<int> RadioDelegate::getCountryInfoValues() const
{
    QVector<int> values;
    const auto infos = this->getCountryList();

    for (const RadioReaderInterface::CountryInfo &info : infos)
        values.append(info.country);

    return values;
}

int RadioDelegate::getCurrentAdapterValue()
{
    return this->getAdapterInfoValue(0);
}

int RadioDelegate::getCurrentDemodulatorTypeValue() const
{
    QLocale::Country country;
    RadioReaderInterface::DemodulatorType type;

    this->m_reader.getChannelCountry(&country, &type);

    return type;
}

int RadioDelegate::getCurrentCountryValue() const
{
    QLocale::Country country;
    RadioReaderInterface::DemodulatorType type;

    this->m_reader.getChannelCountry(&country, &type);

    return country;
}

bool RadioDelegate::setChannelCountry(int country, int type)
{
    return this->m_reader.setChannelCountry((QLocale::Country)country, (RadioReaderInterface::DemodulatorType)type);
}

int RadioDelegate::getChannelCount() const
{
    QVector<RadioChannelMap::Channel> channels;

    this->m_reader.getChannels(&channels);

    return channels.count();
}

int RadioDelegate::getChannelFirst() const
{
    QVector<RadioChannelMap::Channel> channels;

    this->m_reader.getChannels(&channels);

    return channels.first().channel;
}

int RadioDelegate::getChannelLast() const
{
    QVector<RadioChannelMap::Channel> channels;

    this->m_reader.getChannels(&channels);

    return channels.last().channel;
}

QVector<RadioReaderInterface::DemodulatorTypeInfo> RadioDelegate::getDemodulatorTypeList() const
{
    QVector<RadioReaderInterface::DemodulatorTypeInfo> list;

    for (int i = RadioReaderInterface::DT_NONE + 1; i < RadioReaderInterface::DT_COUNT; i++)
    {
        QString name = this->m_reader.demodulatorTypeToString((RadioReaderInterface::DemodulatorType)i);

        if (!name.isEmpty())
        {
            RadioReaderInterface::DemodulatorTypeInfo item;

            item.name = name;
            item.type = (RadioReaderInterface::DemodulatorType)i;

            list.append(item);
        }
    }

    return list;
}

QVector<RadioReaderInterface::CountryInfo> RadioDelegate::getCountryList() const
{
    QVector<RadioReaderInterface::CountryInfo> list;

    for (int i = QLocale::AnyCountry + 1; i < QLocale::LastCountry; i++)
    {
        QString name = QLocale::countryToString((QLocale::Country)i);

        if (!name.isEmpty())
        {
            RadioReaderInterface::CountryInfo item;

            item.name = name;
            item.country = (QLocale::Country)i;

            list.append(item);
        }
    }

    return list;
}

void RadioDelegate::findModel()
{
    if (!this->m_model)
    {
        QVector<RadioReader::ChannelInfo> list;

        this->m_reader.getScannedChannels(&list);

        this->m_model = this->findChild<RadioListModel*>("radioListModel");

        this->m_model->setReader(&this->m_reader);
        this->m_model->setChannelInfo(list);
    }
}
