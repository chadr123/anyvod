﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "LanguageDelegate.h"
#include "core/Common.h"

#include <QDir>

const QString LanguageDelegate::LANG_PREFIX = "anyvod";
const QString LanguageDelegate::LANG_DIR = ASSETS_PREFIX"/languages";

LanguageDelegate::LanguageDelegate()
{

}

QString LanguageDelegate::getDesc(const QString &lang) const
{
    QLocale locale(lang);

    return QString("%1 (%2)").arg(locale.nativeLanguageName(), locale.nativeCountryName());
}

QStringList LanguageDelegate::getAllList() const
{
    QStringList langs = this->getList();
    QStringList all;

    for (const QString &lang : qAsConst(langs))
    {
        QString adjusted = lang;

        adjusted.truncate(adjusted.lastIndexOf('.'));
        adjusted.remove(0, adjusted.indexOf('_') + 1);

        all.append(adjusted);
    }

    return all;
}

QStringList LanguageDelegate::getList() const
{
    QDir dir(LANG_DIR);
    QStringList fileNames = dir.entryList(QStringList(QString("%1_*.qm").arg(LANG_PREFIX)));

    return fileNames;
}
