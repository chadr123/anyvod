﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "TimeUtilDelegate.h"
#include "utils/ConvertingUtils.h"

TimeUtilDelegate::TimeUtilDelegate()
{

}

QString TimeUtilDelegate::getTimeStringWithMSec(double value) const
{
    QString time;

    ConvertingUtils::getTimeString(value, ConvertingUtils::TIME_HH_MM_SS_ZZZ, &time);
    return time;
}
