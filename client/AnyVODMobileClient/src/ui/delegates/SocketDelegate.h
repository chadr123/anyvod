﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "../../../../common/packets.h"

#include <QQuickItem>

class SocketDelegate : public QQuickItem
{
    Q_OBJECT
public:
    SocketDelegate();

    Q_INVOKABLE bool login(const QString &id, const QString &password) const;
    Q_INVOKABLE void logout() const;
    Q_INVOKABLE bool isLogined() const;

    QList<ANYVOD_FILE_ITEM> requestFilelist(const QString &path) const;

public:
    Q_PROPERTY(int maxIDLength READ maxIDLength CONSTANT)
    Q_PROPERTY(int maxPasswordLength READ maxPasswordLength CONSTANT)

private:
    int maxIDLength() const;
    int maxPasswordLength() const;
};
