﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "AutoStartDelegate.h"
#include "core/AutoStart.h"

#include "../RenderScreen.h"

AutoStartDelegate *AutoStartDelegate::m_self = nullptr;
QMutex AutoStartDelegate::m_selfLock;

AutoStartDelegate::AutoStartDelegate()
{

}

AutoStartDelegate* AutoStartDelegate::getSelf()
{
    AutoStartDelegate *self;

    AutoStartDelegate::m_selfLock.lock();
    self = AutoStartDelegate::m_self;
    AutoStartDelegate::m_selfLock.unlock();

    return self;
}

void AutoStartDelegate::newStartingURL()
{
    emit this->newStarted();
}

void AutoStartDelegate::init()
{
    AutoStartDelegate::m_selfLock.lock();
    AutoStartDelegate::m_self = this;
    AutoStartDelegate::m_selfLock.unlock();
}

QString AutoStartDelegate::getStartingURL()
{
    return AutoStart::getInstance().getStartingURL();
}

void AutoStartDelegate::forceStopMedia()
{
    RenderScreen *self = RenderScreen::getSelf();

    if (self)
        self->closeAndGoBack();
}

void AutoStartDelegate::setStartingURL(const QString &url)
{
    AutoStart::getInstance().setStartingURL(url);
}

