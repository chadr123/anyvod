﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "SPDIFDelegate.h"
#include "audio/SPDIF.h"
#include "audio/SPDIFSampleRates.h"

SPDIFDelegate::SPDIFDelegate()
{

}

QStringList SPDIFDelegate::getDevices() const
{
    QStringList list;

    SPDIF::getInstance().getDeviceList(&list);

    return list;
}

QStringList SPDIFDelegate::getSampleRateDescs() const
{
    SPDIFSampleRates &samples = SPDIFSampleRates::getInstance();
    QStringList descs;

    for (int i = 0; i < samples.getCount(); i++)
    {
        int item = samples.getSampleRate(i);
        QString desc;

        if (item == 0)
            desc = tr("기본 속도");
        else
            desc = QString("%1kHz").arg(item / 1000.0f, 0, 'f', 1);

        descs.append(desc);
    }

    return descs;
}

QVector<int> SPDIFDelegate::getSampleRates() const
{
    SPDIFSampleRates &samples = SPDIFSampleRates::getInstance();
    QVector<int> list;

    for (int i = 0; i < samples.getCount(); i++)
        list.append(samples.getSampleRate(i));

    return list;
}
