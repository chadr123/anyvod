﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "EqualizerDelegate.h"
#include "core/Common.h"
#include "media/AudioRenderer.h"
#include "setting/Settings.h"
#include "setting/SettingsFactory.h"

#include <QSettings>

EqualizerDelegate::EqualizerDelegate() :
    m_settings(SettingsFactory::getInstance(SettingsFactory::ST_GLOBAL))
{
    AudioRenderer *tmp = new AudioRenderer(nullptr);
    int maxBand = tmp->getBandCount();

    this->initPreset(maxBand);
    this->initValue();

    delete tmp;
}

void EqualizerDelegate::use(bool use) const
{
    this->m_settings.setValue(SETTING_USE_EQUALIZER, use);
}

bool EqualizerDelegate::isUse() const
{
    return this->m_settings.value(SETTING_USE_EQUALIZER).toBool();
}

QStringList EqualizerDelegate::getPresetNames() const
{
    QStringList names;

    for (const Preset &preset : this->m_presets)
        names.append(preset.desc);

    return names;
}

int EqualizerDelegate::getPresetBandGain(int preset, int band) const
{
    return this->dBToValue(this->m_presets[preset].gains[band].gain);
}

int EqualizerDelegate::getBandGain(int band) const
{
    return this->dBToValue(this->m_saveGains[band].gain);
}

int EqualizerDelegate::getCurrentIndex() const
{
    for (int i = 0; i < this->m_presets.count(); i++)
    {
        const Preset &preset = this->m_presets[i];
        bool same = true;

        for (int j = 0; j < preset.gains.count() && j < this->m_saveGains.count(); j++)
            same &= preset.gains[j].gain == this->m_saveGains[j].gain;

        if (same)
            return i;
    }

    return -1;
}

int EqualizerDelegate::getPreamp() const
{
    return this->dBToValue(this->m_settings.value(SETTING_EQUALIZER_PREAMP).toFloat());
}

void EqualizerDelegate::initValue()
{
    QVariantList list = this->m_settings.value(SETTING_EQUALIZER_GAINS, QVariantList()).toList();

    this->m_saveGains.clear();

    for (int i = 0; i < list.count(); i++)
    {
        RenderScreen::EqualizerItem item;

        item.gain = list[i].value<RenderScreen::EqualizerItem>().gain;
        this->m_saveGains.append(item);
    }
}

void EqualizerDelegate::setPreset(int preset) const
{
    QVariantList list;
    QVector<RenderScreen::EqualizerItem> eqGains;

    for (int i = 0; i < this->getPresetBandCount(preset); i++)
    {
        RenderScreen::EqualizerItem item;

        item.gain = this->valueTodB(this->getPresetBandGain(preset, i));

        eqGains.append(item);
    }

    for (int i = 0; i < eqGains.count(); i++)
        list.append(QVariant::fromValue(eqGains[i]));

    this->m_settings.setValue(SETTING_EQUALIZER_GAINS, list);
}

void EqualizerDelegate::setBandForSave(int band, int value)
{
    this->m_saveGains[band].gain = this->valueTodB(value);
}

void EqualizerDelegate::savePreamp(int value) const
{
    this->m_settings.setValue(SETTING_EQUALIZER_PREAMP, this->valueTodB(value));
}

void EqualizerDelegate::saveBand() const
{
    QVariantList list;

    for (int i = 0; i < this->m_saveGains.count(); i++)
        list.append(QVariant::fromValue(this->m_saveGains[i]));

    this->m_settings.setValue(SETTING_EQUALIZER_GAINS, list);
}

int EqualizerDelegate::getPresetBandCount(int preset) const
{
    return this->m_presets[preset].gains.count();
}

void EqualizerDelegate::initPreset(int maxBand)
{
    QString presets =
            "기본 값/0/0/0/0/0/0/0/0/0/0\n"
            "클래식/0/0/0/0/0/0/-4.8/-4.8/-4.8/-6.3\n"
            "베이스/5.9/5.9/5.9/3.6/1/-2.9/-5.5/-6.7/-7/-7\n"
            "베이스 & 트레블/4.4/3.6/0/-4.8/-3.2/1/5.1/6.7/7.4/7.4\n"
            "트레블/-6.3/-6.3/-6.3/-2.9/1.7/6.7/9.7/9.7/9.7/10.5\n"
            "헤드폰/2.9/6.7/3.2/-2.5/-1.7/1/2.9/5.9/7.8/9\n"
            "홀/6.7/6.7/3.6/3.6/0/-3.2/-3.2/-3.2/0/0\n"
            "소프트 락/2.5/2.5/1.3/-0.6/-2.9/-3.6/-2.5/-0.6/1.7/5.5\n"
            "클럽/0/0/2.5/3.6/3.6/3.6/2.5/0/0/0\n"
            "댄스/5.9/4.4/1.3/0/0/-3.6/-4.8/-4.8/0/0\n"
            "라이브/-3.2/0/2.5/3.2/3.6/3.6/2.5/1.7/1.7/1.3\n"
            "파티/4.4/4.4/0/0/0/0/0/0/4.4/4.4\n"
            "팝/-1.3/2.9/4.4/4.8/3.2/-1/-1.7/-1.7/-1.3/-1.3\n"
            "레게/0/0/-0.6/-3.6/0/4.4/4.4/0/0/0\n"
            "락/4.8/2.9/-3.6/-5.1/-2.5/2.5/5.5/6.7/6.7/6.7\n"
            "스카/-1.7/-3.2/-2.9/-0.6/2.5/3.6/5.5/5.9/6.7/5.9\n"
            "소프트/2.9/1/-1/-1.7/-1/2.5/5.1/5.9/6.7/7.4\n"
            "테크노/4.8/3.6/0/3.6/3.2/0/4.8/5.9/5.9/5.5\n"
            "보컬/-4.2/-2.7/-1.2/3.6/5.4/5.4/3.2/2.1/1.4/0.6\n"
            "재즈/2.1/3.9/3.2/-4.5/-0.9/2.9/5.4/3.9/-0.9/2.9\n";

    tr("기본 값");
    tr("클래식");
    tr("베이스");
    tr("베이스 & 트레블");
    tr("트레블");
    tr("헤드폰");
    tr("홀");
    tr("소프트 락");
    tr("클럽");
    tr("댄스");
    tr("라이브");
    tr("파티");
    tr("팝");
    tr("레게");
    tr("락");
    tr("스카");
    tr("소프트");
    tr("테크노");
    tr("보컬");
    tr("재즈");

    QTextStream stream(&presets);

    this->m_presets.clear();

    while (!stream.atEnd())
    {
        QString line = stream.readLine();
        Preset preset;
        RenderScreen::EqualizerItem gain;

        line = line.trimmed();

        if (line.length() == 0)
            continue;

        if (line.startsWith(COMMENT_PREFIX))
            continue;

        QStringList pair = line.split('/');

        if (pair.length() < maxBand + 1)
            continue;

        preset.desc = tr(pair[0].toUtf8());

        for (int i = 0; i < maxBand; i++)
        {
            gain.gain = pair[i + 1].toFloat();
            preset.gains.append(gain);
        }

        this->m_presets.append(preset);
    }
}

float EqualizerDelegate::valueTodB(int value) const
{
    return (float)value / 10.0f;
}

int EqualizerDelegate::dBToValue(float dB) const
{
    return (int)(dB * 10.0f);
}
