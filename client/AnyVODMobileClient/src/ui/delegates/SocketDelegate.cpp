﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "SocketDelegate.h"
#include "net/Socket.h"
#include "setting/Settings.h"
#include "setting/SettingsFactory.h"

#include <QSettings>

SocketDelegate::SocketDelegate()
{

}

bool SocketDelegate::login(const QString &id, const QString &password) const
{
    const QSettings &settings(SettingsFactory::getInstance(SettingsFactory::ST_GLOBAL));
    Socket &socket = Socket::getInstance();
    QString error;
    QString address = settings.value(SETTING_ADDRESS).toString();
    int commandPort = settings.value(SETTING_COMMAND_PORT).toInt();
    int streamPort = settings.value(SETTING_STREAM_PORT).toInt();

    if (!socket.connect(address, commandPort))
        return false;

    if (!socket.login(id, password, &error))
    {
        socket.disconnect();
        return false;
    }

    Socket &streamSocket = Socket::getStreamInstance();

    if (!streamSocket.connect(address, streamPort))
    {
        socket.disconnect();
        return false;
    }

    QString ticket;

    socket.getTicket(&ticket);

    if (!streamSocket.join(ticket, &error))
    {
        socket.disconnect();
        streamSocket.disconnect();

        return false;
    }

    return true;
}

void SocketDelegate::logout() const
{
    QString error;

    Socket::getInstance().logout(&error);
    Socket::getStreamInstance().logout(&error);
}

bool SocketDelegate::isLogined() const
{
    return Socket::getInstance().isLogined() && Socket::getStreamInstance().isLogined();
}

QList<ANYVOD_FILE_ITEM> SocketDelegate::requestFilelist(const QString &path) const
{
    QString error;
    QList<ANYVOD_FILE_ITEM> list;

    Socket::getInstance().requestFilelist(path, &error, &list);

    return list;
}

int SocketDelegate::maxIDLength() const
{
    return MAX_ID_CHAR_SIZE;
}

int SocketDelegate::maxPasswordLength() const
{
    return MAX_PASS_CHAR_SIZE;
}
