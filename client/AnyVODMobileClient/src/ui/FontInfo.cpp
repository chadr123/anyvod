﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "FontInfo.h"

#include <QFontDatabase>

FontInfo::FontInfo()
{
    QFontDatabase fontDatabase;
    QStringList fontBlacklist;

#if defined Q_OS_IOS
    fontBlacklist << "Bangla Sangam MN";
    fontBlacklist << "Heiti SC";
    fontBlacklist << "Heiti TC";
    fontBlacklist << "Telugu Sangam MN";
#endif

    const QStringList families = fontDatabase.families();

    for (const QString &family : families)
    {
        if (fontDatabase.isPrivateFamily(family))
            continue;

        if (fontBlacklist.contains(family))
            continue;

        this->m_families.append(family);
    }
}

FontInfo& FontInfo::getInstance()
{
    static FontInfo instance;

    return instance;
}

void FontInfo::setDefaultInfo(const QString &family, const QString &path)
{
    this->m_family = family;
    this->m_path = path;
}

QString FontInfo::getFamily() const
{
    return this->m_family;
}

QString FontInfo::getPath() const
{
    return this->m_path;
}

QStringList FontInfo::getFamilies() const
{
    return this->m_families;
}
