﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "MediaCodec.h"
#include "core/Common.h"

#include <QAndroidJniEnvironment>
#include <QAndroidJniObject>
#include <QDebug>

extern "C"
{
# include <libavutil/frame.h>
# include <libavutil/pixdesc.h>
# include <libavutil/imgutils.h>
}

#define INFO_OUTPUT_BUFFERS_CHANGED -3
#define INFO_OUTPUT_FORMAT_CHANGED  -2

#define BS_WL32(p, d) { \
  ((uint8_t*)(p))[0] = (d); \
  ((uint8_t*)(p))[1] = (d) >> 8; \
  ((uint8_t*)(p))[2] = (d) >> 16; \
  ((uint8_t*)(p))[3] = (d) >> 24; }

const MediaCodec::CodecDesc MediaCodec::CODEC_DESCS[] =
{
    { AV_CODEC_ID_VP8,        "video/x-vnd.on2.vp8" },
    { AV_CODEC_ID_VP9,        "video/x-vnd.on2.vp9" },
    { AV_CODEC_ID_H264,       "video/avc" },
    { AV_CODEC_ID_H263,       "video/3gpp" },
    { AV_CODEC_ID_HEVC,       "video/hevc" },
    { AV_CODEC_ID_MPEG4,      "video/mp4v-es" },
    { AV_CODEC_ID_MPEG2VIDEO, "video/mpeg2" },
    { AV_CODEC_ID_WMV3,       "video/x-ms-wmv" },
    { AV_CODEC_ID_VC1,        "video/wvc1" },
    { AV_CODEC_ID_NONE,       nullptr }
};

const MediaCodec::OMXToH264ProfileIDC MediaCodec::OMX_H264_PROFILE_IDC[] =
{
    { OMX_VIDEO_AVCProfileBaseline,  FF_PROFILE_H264_BASELINE },
    { OMX_VIDEO_AVCProfileMain,      FF_PROFILE_H264_MAIN },
    { OMX_VIDEO_AVCProfileExtended,  FF_PROFILE_H264_EXTENDED },
    { OMX_VIDEO_AVCProfileHigh,      FF_PROFILE_H264_HIGH },
    { OMX_VIDEO_AVCProfileHigh10,    FF_PROFILE_H264_HIGH_10 },
    { OMX_VIDEO_AVCProfileHigh422,   FF_PROFILE_H264_HIGH_422 },
    { OMX_VIDEO_AVCProfileHigh444,   FF_PROFILE_H264_HIGH_444_PREDICTIVE },
};

const MediaCodec::OMXToHEVCProfileIDC MediaCodec::OMX_HEVC_PROFILE_IDC[] =
{
    { OMX_VIDEO_HEVCProfileMain,       FF_PROFILE_HEVC_MAIN },
    { OMX_VIDEO_HEVCProfileMain,       FF_PROFILE_HEVC_MAIN_STILL_PICTURE },
    { OMX_VIDEO_HEVCProfileMain10,     FF_PROFILE_HEVC_MAIN_10 },
};

const MediaCodec::OMXToVP9ProfileIDC MediaCodec::OMX_VP9_PROFILE_IDC[] =
{
    { OMX_VIDEO_VP9Profile0,     FF_PROFILE_VP9_0 },
    { OMX_VIDEO_VP9Profile1,     FF_PROFILE_VP9_1 },
    { OMX_VIDEO_VP9Profile2,     FF_PROFILE_VP9_2 },
    { OMX_VIDEO_VP9Profile3,     FF_PROFILE_VP9_3 },
};

const MediaCodec::OMXToMPEG4ProfileIDC MediaCodec::OMX_MPEG4_PROFILE_IDC[] =
{
    { OMX_VIDEO_MPEG4ProfileSimple,             FF_PROFILE_MPEG4_SIMPLE },
    { OMX_VIDEO_MPEG4ProfileSimpleScalable,     FF_PROFILE_MPEG4_SIMPLE_SCALABLE },
    { OMX_VIDEO_MPEG4ProfileCore,               FF_PROFILE_MPEG4_CORE },
    { OMX_VIDEO_MPEG4ProfileMain,               FF_PROFILE_MPEG4_MAIN },
    { OMX_VIDEO_MPEG4ProfileNbit,               FF_PROFILE_MPEG4_N_BIT },
    { OMX_VIDEO_MPEG4ProfileScalableTexture,    FF_PROFILE_MPEG4_SCALABLE_TEXTURE },
    { OMX_VIDEO_MPEG4ProfileSimpleFace,         FF_PROFILE_MPEG4_SIMPLE_FACE_ANIMATION },
    { OMX_VIDEO_MPEG4ProfileSimpleFBA,          FF_PROFILE_MPEG4_SIMPLE_FACE_ANIMATION },
    { OMX_VIDEO_MPEG4ProfileBasicAnimated,      FF_PROFILE_MPEG4_BASIC_ANIMATED_TEXTURE },
    { OMX_VIDEO_MPEG4ProfileHybrid,             FF_PROFILE_MPEG4_HYBRID },
    { OMX_VIDEO_MPEG4ProfileAdvancedRealTime,   FF_PROFILE_MPEG4_ADVANCED_REAL_TIME },
    { OMX_VIDEO_MPEG4ProfileCoreScalable,       FF_PROFILE_MPEG4_CORE_SCALABLE },
    { OMX_VIDEO_MPEG4ProfileAdvancedCoding,     FF_PROFILE_MPEG4_ADVANCED_CODING },
    { OMX_VIDEO_MPEG4ProfileAdvancedCore,       FF_PROFILE_MPEG4_ADVANCED_CORE },
    { OMX_VIDEO_MPEG4ProfileAdvancedScalable,   FF_PROFILE_MPEG4_ADVANCED_SCALABLE_TEXTURE },
    { OMX_VIDEO_MPEG4ProfileAdvancedSimple,     FF_PROFILE_MPEG4_ADVANCED_SIMPLE },
};

const char *MediaCodec::CLASS_NAMES[MediaCodec::CN_COUNT] =
{
    "android/media/MediaCodecList",
    "android/media/MediaCodec",
    "android/media/MediaFormat",
    "android/media/MediaCodec$BufferInfo",
    "java/nio/ByteBuffer"
};

const MediaCodec::MethodDesc MediaCodec::METHOD_DESCS[MediaCodec::MN_COUNT] =
{
    { "toString", "()Ljava/lang/String;", "java/lang/Object" },

    { "getCodecCount", "()I", "android/media/MediaCodecList" },
    { "getCodecInfoAt", "(I)Landroid/media/MediaCodecInfo;", "android/media/MediaCodecList" },

    { "isEncoder", "()Z", "android/media/MediaCodecInfo" },
    { "getSupportedTypes", "()[Ljava/lang/String;", "android/media/MediaCodecInfo" },
    { "getName", "()Ljava/lang/String;", "android/media/MediaCodecInfo" },
    { "getCapabilitiesForType", "(Ljava/lang/String;)Landroid/media/MediaCodecInfo$CodecCapabilities;", "android/media/MediaCodecInfo" },

    { "profileLevels", "[Landroid/media/MediaCodecInfo$CodecProfileLevel;", "android/media/MediaCodecInfo$CodecCapabilities" },
    { "profile", "I", "android/media/MediaCodecInfo$CodecProfileLevel" },
    { "level", "I", "android/media/MediaCodecInfo$CodecProfileLevel" },

    { "createByCodecName", "(Ljava/lang/String;)Landroid/media/MediaCodec;", "android/media/MediaCodec" },
    { "configure", "(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V", "android/media/MediaCodec" },
    { "start", "()V", "android/media/MediaCodec" },
    { "stop", "()V", "android/media/MediaCodec" },
    { "flush", "()V", "android/media/MediaCodec" },
    { "release", "()V", "android/media/MediaCodec" },
    { "getOutputFormat", "()Landroid/media/MediaFormat;", "android/media/MediaCodec" },
    { "getInputBuffers", "()[Ljava/nio/ByteBuffer;", "android/media/MediaCodec" },
    { "getOutputBuffers", "()[Ljava/nio/ByteBuffer;", "android/media/MediaCodec" },
    { "dequeueInputBuffer", "(J)I", "android/media/MediaCodec" },
    { "dequeueOutputBuffer", "(Landroid/media/MediaCodec$BufferInfo;J)I", "android/media/MediaCodec" },
    { "queueInputBuffer", "(IIIJI)V", "android/media/MediaCodec" },
    { "releaseOutputBuffer", "(IZ)V", "android/media/MediaCodec" },

    { "createVideoFormat", "(Ljava/lang/String;II)Landroid/media/MediaFormat;", "android/media/MediaFormat" },
    { "setInteger", "(Ljava/lang/String;I)V", "android/media/MediaFormat" },
    { "getInteger", "(Ljava/lang/String;)I", "android/media/MediaFormat" },
    { "setByteBuffer", "(Ljava/lang/String;Ljava/nio/ByteBuffer;)V", "android/media/MediaFormat" },

    { "<init>", "()V", "android/media/MediaCodec$BufferInfo" },
    { "size", "I", "android/media/MediaCodec$BufferInfo" },
    { "offset", "I", "android/media/MediaCodec$BufferInfo" },
    { "presentationTimeUs", "J", "android/media/MediaCodec$BufferInfo" },

    { "allocateDirect", "(I)Ljava/nio/ByteBuffer;", "java/nio/ByteBuffer" },
    { "limit", "(I)Ljava/nio/Buffer;", "java/nio/ByteBuffer" }
};

MediaCodec::MediaCodec() :
    m_codec(nullptr),
    m_inputBuffers(nullptr),
    m_outputBuffers(nullptr),
    m_bufferInfo(nullptr),
    m_codecContext(nullptr),
    m_started(false),
    m_pixFormat(-1),
    m_nalSize(0),
    m_sliceHeight(0),
    m_stride(0),
    m_cropLeft(0),
    m_cropRight(0),
    m_cropTop(0),
    m_cropBottom(0)
{

}

MediaCodec::~MediaCodec()
{
    this->closeInternal();
}

bool MediaCodec::open(AVCodecContext *codec)
{
    QString mime = this->getMime(codec->codec_id);

    if (mime.isEmpty())
        return false;

    for (int i = 0; i < CN_COUNT; i++)
    {
        if (!QAndroidJniObject::isClassAvailable(CLASS_NAMES[i]))
            return false;
    }

    QAndroidJniEnvironment env;
    QAndroidJniObject mimeJava = QAndroidJniObject::fromString(mime);
    int codecCount = QAndroidJniObject::callStaticMethod<int>(METHOD_DESCS[MN_GET_CODEC_COUNT].className,
                                                              METHOD_DESCS[MN_GET_CODEC_COUNT].name,
                                                              METHOD_DESCS[MN_GET_CODEC_COUNT].sig);

    for (int i = 0; i < codecCount; i++)
    {
        QAndroidJniObject info = QAndroidJniObject::callStaticObjectMethod(METHOD_DESCS[MN_GET_CODEC_INFO_AT].className,
                                                                           METHOD_DESCS[MN_GET_CODEC_INFO_AT].name,
                                                                           METHOD_DESCS[MN_GET_CODEC_INFO_AT].sig,
                                                                           i);

        if (info.callMethod<jboolean>(METHOD_DESCS[MN_IS_ENCODER].name, METHOD_DESCS[MN_IS_ENCODER].sig))
            continue;

        QAndroidJniObject nameJava = info.callObjectMethod(METHOD_DESCS[MN_GET_NAME].name,
                                                       METHOD_DESCS[MN_GET_NAME].sig);
        QString name = nameJava.toString();

        if (name.startsWith("OMX.google.") || name.contains(".sw."))
            continue;

        QAndroidJniObject profileLevels;
        int profileLevelsCount = 0;
        QAndroidJniObject capabilities = info.callObjectMethod(METHOD_DESCS[MN_GET_CAPABILITIES_FOR_TYPE].name,
                                                               METHOD_DESCS[MN_GET_CAPABILITIES_FOR_TYPE].sig,
                                                               mimeJava.object<jstring>());

        if (env->ExceptionCheck())
        {
            env->ExceptionClear();
            continue;
        }
        else if (capabilities.isValid())
        {
            profileLevels = capabilities.getObjectField(METHOD_DESCS[MN_PROFILE_LEVELS].name,
                                                        METHOD_DESCS[MN_PROFILE_LEVELS].sig);

            if (profileLevels.isValid())
                profileLevelsCount = env->GetArrayLength(profileLevels.object<jarray>());
        }


        bool found = false;
        QAndroidJniObject types = info.callObjectMethod(METHOD_DESCS[MN_GET_SUPPORTED_TYPES].name,
                                                        METHOD_DESCS[MN_GET_SUPPORTED_TYPES].sig);
        int typeCount = env->GetArrayLength(types.object<jarray>());
        jobjectArray typesJava = types.object<jobjectArray>();

        for (int j = 0; j < typeCount && !found; j++)
        {
            jobject typeJava = env->GetObjectArrayElement(typesJava, j);
            QAndroidJniObject type = typeJava;

            if (type.toString() == mime)
            {
                if (codec->profile >= 0)
                {
                    jobjectArray profileLevelsJava = profileLevels.object<jobjectArray>();

                    if (profileLevelsCount == 0)
                        found = true;

                    for (int k = 0; k < profileLevelsCount && !found; k++)
                    {
                        jobject profileLevelJava = env->GetObjectArrayElement(profileLevelsJava, k);
                        QAndroidJniObject profileLevel = profileLevelJava;
                        int profile = profileLevel.getField<int>(METHOD_DESCS[MN_PROFILE].name);

                        env->DeleteLocalRef(profileLevelJava);

                        if (codec->codec_id == AV_CODEC_ID_H264)
                        {
                            OMX_VIDEO_AVCPROFILETYPE omxProfile = (OMX_VIDEO_AVCPROFILETYPE)profile;
                            int codecProfile = this->convertOMXToProfileIDC(omxProfile, OMX_H264_PROFILE_IDC);

                            if (codecProfile != codec->profile &&
                                    (codecProfile | FF_PROFILE_H264_CONSTRAINED) != codec->profile &&
                                    (codecProfile | FF_PROFILE_H264_INTRA) != codec->profile)
                                continue;

                            found = true;
                        }
                        else if (codec->codec_id == AV_CODEC_ID_HEVC)
                        {
                            OMX_VIDEO_HEVCPROFILETYPE omxProfile = (OMX_VIDEO_HEVCPROFILETYPE)profile;
                            int codecProfile = this->convertOMXToProfileIDC(omxProfile, OMX_HEVC_PROFILE_IDC);

                            if (codecProfile != codec->profile)
                                continue;

                            found = true;
                        }
                        else if (codec->codec_id == AV_CODEC_ID_VP9)
                        {
                            OMX_VIDEO_VP9PROFILETYPE omxProfile = (OMX_VIDEO_VP9PROFILETYPE)profile;
                            int codecProfile = this->convertOMXToProfileIDC(omxProfile, OMX_VP9_PROFILE_IDC);

                            if (codecProfile != codec->profile)
                                continue;

                            found = true;
                        }
                        else if (codec->codec_id == AV_CODEC_ID_MPEG4)
                        {
                            OMX_VIDEO_MPEG4PROFILETYPE omxProfile = (OMX_VIDEO_MPEG4PROFILETYPE)profile;
                            int codecProfile = this->convertOMXToProfileIDC(omxProfile, OMX_MPEG4_PROFILE_IDC);

                            if (codecProfile != codec->profile)
                                continue;

                            found = true;
                        }
                    }
                }
                else
                {
                    found = true;
                }
            }

            env->DeleteLocalRef(typeJava);
        }

        if (found)
        {
            this->m_codecName = name;
            break;
        }
    }

    if (this->m_codecName.isEmpty())
    {
        this->close();
        return false;
    }

    QAndroidJniObject codecTmp = QAndroidJniObject::callStaticObjectMethod(METHOD_DESCS[MN_CREATE_BY_CODEC_NAME].className,
                                                                           METHOD_DESCS[MN_CREATE_BY_CODEC_NAME].name,
                                                                           METHOD_DESCS[MN_CREATE_BY_CODEC_NAME].sig,
                                                                           QAndroidJniObject::fromString(this->m_codecName).object<jstring>());

    if (env->ExceptionCheck())
    {
        env->ExceptionClear();
        this->close();

        return false;
    }

    this->m_codec = new QAndroidJniObject(codecTmp.object());

    int extraSize = codec->extradata_size;
    QAndroidJniObject format = QAndroidJniObject::callStaticObjectMethod(METHOD_DESCS[MN_CREATE_VIDEO_FORMAT].className,
                                                                         METHOD_DESCS[MN_CREATE_VIDEO_FORMAT].name,
                                                                         METHOD_DESCS[MN_CREATE_VIDEO_FORMAT].sig,
                                                                         mimeJava.object<jstring>(),
                                                                         codec->width, codec->height);

    if (extraSize > 0)
    {
        int bufSize = extraSize + 100;
        QAndroidJniObject byteBuf = QAndroidJniObject::callStaticObjectMethod(METHOD_DESCS[MN_ALLOCATE_DIRECT].className,
                                                                              METHOD_DESCS[MN_ALLOCATE_DIRECT].name,
                                                                              METHOD_DESCS[MN_ALLOCATE_DIRECT].sig,
                                                                              bufSize);
        uint8_t *ptr = (uint8_t*)env->GetDirectBufferAddress(byteBuf.object());

        if (codec->codec_id == AV_CODEC_ID_H264 && codec->extradata[0] == 1)
        {
            this->convertH264SPSAndPPS(codec->extradata, extraSize, ptr, bufSize, &extraSize, &this->m_nalSize);
        }
        else if (codec->codec_id == AV_CODEC_ID_HEVC)
        {
            this->convertHEVCVPSAndSPSAndPPS(codec->extradata, extraSize, ptr, bufSize, &extraSize, &this->m_nalSize);
        }
        else if (codec->codec_id == AV_CODEC_ID_WMV3 && (extraSize == 4 || extraSize == 5))
        {
            this->convertSMTPE(codec->extradata, ptr, codec->width, codec->height, &extraSize);
        }
        else if (codec->codec_id == AV_CODEC_ID_VC1)
        {
            if (!this->convertVC1Data(codec->extradata, extraSize, ptr, bufSize, &extraSize))
            {
                this->close();
                return false;
            }
        }
        else
        {
            memcpy(ptr, codec->extradata, extraSize);
        }

        byteBuf.callObjectMethod(METHOD_DESCS[MN_LIMIT].name,
                                 METHOD_DESCS[MN_LIMIT].sig,
                                 extraSize);
        format.callMethod<void>(METHOD_DESCS[MN_SET_BYTE_BUFFER].name,
                                METHOD_DESCS[MN_SET_BYTE_BUFFER].sig,
                                QAndroidJniObject::fromString("csd-0").object<jstring>(),
                                byteBuf.object());
    }

    this->m_codec->callMethod<void>(METHOD_DESCS[MN_CONFIGURE].name,
                                    METHOD_DESCS[MN_CONFIGURE].sig,
                                    format.object(), nullptr, nullptr, 0);

    if (env->ExceptionCheck())
    {
        env->ExceptionClear();
        this->close();

        return false;
    }

    this->m_codec->callMethod<void>(METHOD_DESCS[MN_START].name, METHOD_DESCS[MN_START].sig);

    if (env->ExceptionCheck())
    {
        env->ExceptionClear();
        this->close();

        return false;
    }

    QAndroidJniObject inputBuffersTmp = this->m_codec->callObjectMethod(METHOD_DESCS[MN_GET_INPUT_BUFFERS].name, METHOD_DESCS[MN_GET_INPUT_BUFFERS].sig);
    QAndroidJniObject outputBuffersTmp = this->m_codec->callObjectMethod(METHOD_DESCS[MN_GET_OUTPUT_BUFFERS].name, METHOD_DESCS[MN_GET_OUTPUT_BUFFERS].sig);
    QAndroidJniObject bufferInfoTmp = QAndroidJniObject(CLASS_NAMES[CN_MEDIA_CODEC_$_BUFFERINFO]);

    this->m_inputBuffers = new QAndroidJniObject(inputBuffersTmp.object());
    this->m_outputBuffers = new QAndroidJniObject(outputBuffersTmp.object());
    this->m_bufferInfo = new QAndroidJniObject(bufferInfoTmp.object());

    this->m_started = true;
    this->m_codecContext = codec;

    return this->m_started;
}

void MediaCodec::close()
{
    this->closeInternal();
}

bool MediaCodec::prepare(AVCodecContext *codec)
{
    (void)codec;

    return true;
}

bool MediaCodec::getBuffer(AVFrame *ret)
{
    (void)ret;

    return false;
}

void MediaCodec::releaseBuffer(uint8_t *data[])
{
    (void)data;
}

AVPixelFormat MediaCodec::getFormat() const
{
    return this->convertToAVPixFormat(this->m_pixFormat);
}

QString MediaCodec::getName() const
{
    return "MediaCodec";
}

bool MediaCodec::decodePicture(const AVPacket &packet, AVFrame *ret)
{
    bool extracted = false;
    int timeout = 0;
    QAndroidJniEnvironment env;
    H264ConvertState state;

    while (true)
    {
        int index = this->m_codec->callMethod<int>(METHOD_DESCS[MN_DEQUEUE_INPUT_BUFFER].name,
                                                   METHOD_DESCS[MN_DEQUEUE_INPUT_BUFFER].sig,
                                                   (jlong)timeout);

        if (env->ExceptionCheck())
        {
            env->ExceptionClear();
            this->reOpen();

            return false;
        }

        if (index < 0)
        {
            timeout = 30 * 1000;

            if (extracted)
                continue;

            extracted = this->extractDecodedFrame(env, timeout, ret);

            continue;
        }

        jobject buf = env->GetObjectArrayElement(this->m_inputBuffers->object<jobjectArray>(), index);
        int size = env->GetDirectBufferCapacity(buf);
        uint8_t *ptr = (uint8_t*)env->GetDirectBufferAddress(buf);

        if (size > packet.size)
            size = packet.size;

        memcpy(ptr, packet.data, size);

        this->convertH264ToAnnexB(ptr, size, this->m_nalSize, &state);

        int64_t ts = packet.pts;

        if (ts == AV_NOPTS_VALUE && packet.dts != AV_NOPTS_VALUE)
            ts = packet.dts;

        this->m_ptsQueue.enqueue(packet.pts ? AV_NOPTS_VALUE : packet.dts);

        this->m_codec->callMethod<void>(METHOD_DESCS[MN_QUEUE_INPUT_BUFFER].name,
                                        METHOD_DESCS[MN_QUEUE_INPUT_BUFFER].sig,
                                        index, 0, size, ts, 0);

        if (env->ExceptionCheck())
        {
            env->ExceptionClear();
            env->DeleteLocalRef(buf);
            this->reOpen();

            return false;
        }

        env->DeleteLocalRef(buf);

        break;
    }

    if (extracted)
        return true;
    else
        return this->extractDecodedFrame(env, 0, ret);
}

bool MediaCodec::copyPicture(const AVFrame &src, AVFrame *ret)
{
    (void)src;
    (void)ret;

    return false;
}

bool MediaCodec::isDecodable(AVPixelFormat format) const
{
    (void)format;

    return true;
}

void MediaCodec::getDecoderDesc(QString *ret) const
{
    *ret = QString("Android %1").arg(this->m_codecName);
}

void MediaCodec::flushSurfaceQueue()
{
    QAndroidJniEnvironment env;

    this->m_codec->callMethod<void>(METHOD_DESCS[MN_FLUSH].name,
                                    METHOD_DESCS[MN_FLUSH].sig);

    if (env->ExceptionCheck())
        env->ExceptionClear();

    this->m_ptsQueue.clear();
}

int MediaCodec::getSurfaceQueueCount() const
{
    return 0;
}

bool MediaCodec::isUseDefaultGetBuffer() const
{
    return false;
}

QString MediaCodec::getMime(AVCodecID id) const
{
    for (int i = 0; CODEC_DESCS[i].mime; i++)
    {
        if (CODEC_DESCS[i].id == id)
            return CODEC_DESCS[i].mime;
    }

    return QString();
}

void MediaCodec::closeInternal()
{
    QAndroidJniEnvironment env;

    if (this->m_inputBuffers)
    {
        delete this->m_inputBuffers;
        this->m_inputBuffers = nullptr;
    }

    if (this->m_outputBuffers)
    {
        delete this->m_outputBuffers;
        this->m_outputBuffers = nullptr;
    }

    if (this->m_codec)
    {
        if (this->m_started)
        {
            this->m_codec->callMethod<void>(METHOD_DESCS[MN_STOP].name, METHOD_DESCS[MN_STOP].sig);

            if (env->ExceptionCheck())
                env->ExceptionClear();

            this->m_started = false;
        }

        this->m_codec->callMethod<void>(METHOD_DESCS[MN_RELEASE].name, METHOD_DESCS[MN_RELEASE].sig);

        if (env->ExceptionCheck())
            env->ExceptionClear();

        delete this->m_codec;
        this->m_codec = nullptr;
    }

    if (this->m_bufferInfo)
    {
        delete this->m_bufferInfo;
        this->m_bufferInfo = nullptr;
    }

    this->m_ptsQueue.clear();
    this->m_codecName.clear();
    this->m_pixFormat = -1;
    this->m_nalSize = 0;
    this->m_sliceHeight = 0;
    this->m_stride = 0;
    this->m_cropLeft = 0;
    this->m_cropRight = 0;
    this->m_cropTop = 0;
    this->m_cropBottom = 0;
}

template<typename T1, typename T2>
int MediaCodec::convertOMXToProfileIDC(T1 profile, const T2 &array) const
{
    int length = sizeof(array) / sizeof(array[0]);

    for (int i = 0; i < length; ++i)
    {
        if (array[i].omxProfile == profile)
            return array[i].profileIDC;
    }

    return 0;
}

bool MediaCodec::extractDecodedFrame(QAndroidJniEnvironment &env, int timeout, AVFrame *frame)
{
    while (true)
    {
        int index = this->m_codec->callMethod<int>(METHOD_DESCS[MN_DEQUEUE_OUTPUT_BUFFER].name,
                                                   METHOD_DESCS[MN_DEQUEUE_OUTPUT_BUFFER].sig,
                                                   this->m_bufferInfo->object(),
                                                   (jlong)timeout);

        if (env->ExceptionCheck())
        {
            env->ExceptionClear();
            this->reOpen();

            return false;
        }

        if (index >= 0)
        {
            if (this->m_pixFormat == -1)
            {
                this->m_codec->callMethod<void>(METHOD_DESCS[MN_RELEASE_OUTPUT_BUFFER].name,
                                                METHOD_DESCS[MN_RELEASE_OUTPUT_BUFFER].sig,
                                                index, false);

                if (env->ExceptionCheck())
                {
                    env->ExceptionClear();
                    this->reOpen();

                    return false;
                }

                continue;
            }

            if (this->m_codecContext->width != frame->width || this->m_codecContext->height != frame->height)
            {
                if (frame->data[0])
                    av_freep(&frame->data[0]);

                av_image_alloc(frame->data, frame->linesize, this->m_codecContext->width, this->m_codecContext->height, this->convertToAVPixFormat(this->m_pixFormat), 1);

                frame->width = this->m_codecContext->width;
                frame->height = this->m_codecContext->height;
            }

            int64_t ts = this->m_ptsQueue.dequeue();

            if (ts == AV_NOPTS_VALUE)
                ts = this->m_bufferInfo->getField<int64_t>(METHOD_DESCS[MN_PRESENTATION_TIME_US].name);

            frame->best_effort_timestamp = ts;

            jobject buf = env->GetObjectArrayElement(this->m_outputBuffers->object<jobjectArray>(), index);
            uint8_t *ptr = (uint8_t*)env->GetDirectBufferAddress(buf);
            int offset = this->m_bufferInfo->getField<int>(METHOD_DESCS[MN_OFFSET].name);

            ptr += offset;

            this->copyHWPicture(frame, ptr);

            this->m_codec->callMethod<void>(METHOD_DESCS[MN_RELEASE_OUTPUT_BUFFER].name,
                                            METHOD_DESCS[MN_RELEASE_OUTPUT_BUFFER].sig,
                                            index, false);

            if (env->ExceptionCheck())
            {
                env->ExceptionClear();
                env->DeleteLocalRef(buf);
                this->reOpen();

                return false;
            }

            env->DeleteLocalRef(buf);

            return true;
        }
        else if (index == INFO_OUTPUT_BUFFERS_CHANGED)
        {
            delete this->m_outputBuffers;

            QAndroidJniObject outputBuffersTmp = this->m_codec->callObjectMethod(METHOD_DESCS[MN_GET_OUTPUT_BUFFERS].name, METHOD_DESCS[MN_GET_OUTPUT_BUFFERS].sig);

            this->m_outputBuffers = new QAndroidJniObject(outputBuffersTmp.object());

            if (env->ExceptionCheck())
                env->ExceptionClear();

            return false;
        }
        else if (index == INFO_OUTPUT_FORMAT_CHANGED)
        {
            QAndroidJniObject format = this->m_codec->callObjectMethod(METHOD_DESCS[MN_GET_OUTPUT_FORMAT].name,
                                                                       METHOD_DESCS[MN_GET_OUTPUT_FORMAT].sig);

            if (env->ExceptionCheck())
            {
                env->ExceptionClear();

                return false;
            }

            int width = format.callMethod<int>(METHOD_DESCS[MN_GET_INTEGER].name,
                                               METHOD_DESCS[MN_GET_INTEGER].sig,
                                               QAndroidJniObject::fromString("width").object<jstring>());
            int height = format.callMethod<int>(METHOD_DESCS[MN_GET_INTEGER].name,
                                                METHOD_DESCS[MN_GET_INTEGER].sig,
                                                QAndroidJniObject::fromString("height").object<jstring>());
            this->m_cropLeft = format.callMethod<int>(METHOD_DESCS[MN_GET_INTEGER].name,
                                                      METHOD_DESCS[MN_GET_INTEGER].sig,
                                                      QAndroidJniObject::fromString("crop-left").object<jstring>());
            this->m_cropTop = format.callMethod<int>(METHOD_DESCS[MN_GET_INTEGER].name,
                                                     METHOD_DESCS[MN_GET_INTEGER].sig,
                                                     QAndroidJniObject::fromString("crop-top").object<jstring>());
            this->m_cropRight = format.callMethod<int>(METHOD_DESCS[MN_GET_INTEGER].name,
                                                       METHOD_DESCS[MN_GET_INTEGER].sig,
                                                       QAndroidJniObject::fromString("crop-right").object<jstring>());
            this->m_cropBottom = format.callMethod<int>(METHOD_DESCS[MN_GET_INTEGER].name,
                                                        METHOD_DESCS[MN_GET_INTEGER].sig,
                                                        QAndroidJniObject::fromString("crop-bottom").object<jstring>());
            this->m_sliceHeight = format.callMethod<int>(METHOD_DESCS[MN_GET_INTEGER].name,
                                                         METHOD_DESCS[MN_GET_INTEGER].sig,
                                                         QAndroidJniObject::fromString("slice-height").object<jstring>());
            this->m_stride = format.callMethod<int>(METHOD_DESCS[MN_GET_INTEGER].name,
                                                    METHOD_DESCS[MN_GET_INTEGER].sig,
                                                    QAndroidJniObject::fromString("stride").object<jstring>());
            this->m_pixFormat = format.callMethod<int>(METHOD_DESCS[MN_GET_INTEGER].name,
                                                       METHOD_DESCS[MN_GET_INTEGER].sig,
                                                       QAndroidJniObject::fromString("color-format").object<jstring>());

            if (this->m_codecName == "OMX.k3.video.decoder.avc" && this->m_pixFormat == OMX_COLOR_FormatYCbYCr)
                this->m_pixFormat = OMX_TI_COLOR_FormatYUV420PackedSemiPlanar;

            AVPixelFormat avFormat = this->convertToAVPixFormat(this->m_pixFormat);

            if (avFormat == AV_PIX_FMT_NONE)
                return false;

            if (this->m_stride <= 0)
                this->m_stride = width;

            if (this->m_codecName.startsWith("OMX.Nvidia.") && this->m_sliceHeight <= 0)
            {
                this->m_sliceHeight = FFALIGN(height, 16);
            }
            else if (this->m_codecName == "OMX.SEC.avc.dec")
            {
                this->m_sliceHeight = this->m_codecContext->height;
                this->m_stride = this->m_codecContext->width;
            }
            else if (this->m_sliceHeight <= 0)
            {
                this->m_sliceHeight = height;
            }

            if (env->ExceptionCheck())
                env->ExceptionClear();

            if (this->m_pixFormat == OMX_TI_COLOR_FormatYUV420PackedSemiPlanar)
                this->m_sliceHeight -= this->m_cropTop / 2;

            if (this->isNoPaddingDecoder(this->m_codecName))
            {
                this->m_sliceHeight = 0;
                this->m_stride = 0;
            }

            return false;
        }
        else
        {
            return false;
        }
    }

    return false;
}

AVPixelFormat MediaCodec::convertToAVPixFormat(int pixFormat) const
{
    AVPixelFormat format;

    switch (pixFormat)
    {
        case OMX_COLOR_FormatYUV420Planar:
            format = AV_PIX_FMT_YUV420P;
            break;
        case OMX_QCOM_COLOR_FormatYVU420SemiPlanar:
        case OMX_QCOM_COLOR_FormatYUV420SemiPlanar32m:
        case OMX_COLOR_FormatYUV420SemiPlanar:
        case OMX_COLOR_FormatYUV420PackedPlanar:
        case OMX_TI_COLOR_FormatYUV420PackedSemiPlanar:
        case OMA_TI_COLOR_FormatYUV420PackedSemiPlanarInterlaced:
        case QOMX_COLOR_FormatYUV420PackedSemiPlanar64x32Tile2m8ka:
            format = AV_PIX_FMT_NV12;
            break;
        default:
            format = AV_PIX_FMT_NONE;
            break;
    }

    return format;
}

void MediaCodec::convertH264SPSAndPPS(uint8_t *srcData, int srcSize, uint8_t *dstData, int dstSize, int *realSize, unsigned int *retNalSize) const
{
    int dataSize = srcSize;
    int nalSize;
    int spsppsSize = 0;
    int loopEnd;

    if (dataSize < 7)
        return;

    if (retNalSize)
        *retNalSize  = (srcData[4] & 0x03) + 1;

    srcData += 5;
    dataSize -= 5;

    for (int j = 0; j < 2; j++)
    {
        if (dataSize < 1)
            return;

        loopEnd = srcData[0] & (j == 0 ? 0x1f : 0xff);

        srcData++;
        dataSize--;

        for (int i = 0; i < loopEnd; i++)
        {
            if (dataSize < 2)
                return;

            nalSize = (srcData[0] << 8) | srcData[1];
            srcData += 2;
            dataSize -= 2;

            if (dataSize < nalSize)
                return;

            if (spsppsSize + 4 + nalSize > dstSize)
                return;

            dstData[spsppsSize++] = 0;
            dstData[spsppsSize++] = 0;
            dstData[spsppsSize++] = 0;
            dstData[spsppsSize++] = 1;

            memcpy(dstData + spsppsSize, srcData, nalSize);
            spsppsSize += nalSize;

            srcData += nalSize;
            dataSize -= nalSize;
        }
    }

    *realSize = spsppsSize;
}

void MediaCodec::convertHEVCVPSAndSPSAndPPS(uint8_t *srcData, int srcSize, uint8_t *dstData, int dstSize, int *realSize, unsigned int *retNalSize) const
{
    int numArrays;
    const uint8_t *end = srcData + srcSize;
    int spsppsSize = 0;

    if (srcSize <= 3 || (!srcData[0] && !srcData[1] && srcData[2] <= 1))
        return;

    if (end - srcData < 23)
        return;

    srcData += 21;

    if (retNalSize)
        *retNalSize = (*srcData & 0x03) + 1;

    srcData++;
    numArrays = *srcData++;

    for (int i = 0; i < numArrays; i++ )
    {
        int type, count;

        if (end - srcData < 3)
            return;

        type = *(srcData++) & 0x3f;
        (void)type;

        count = srcData[0] << 8 | srcData[1];

        srcData += 2;

        for (int j = 0; j < count; j++)
        {
            int nalSize;

            if (end - srcData < 2)
                return;

            nalSize = srcData[0] << 8 | srcData[1];
            srcData += 2;

            if (nalSize < 0 || end - srcData < nalSize)
                return;

            if (spsppsSize + 4 + nalSize > dstSize)
                return;

            dstData[spsppsSize++] = 0;
            dstData[spsppsSize++] = 0;
            dstData[spsppsSize++] = 0;
            dstData[spsppsSize++] = 1;

            memcpy(dstData + spsppsSize, srcData, nalSize);
            srcData += nalSize;

            spsppsSize += nalSize;
        }
    }

    *realSize = spsppsSize;
}

void MediaCodec::convertSMTPE(uint8_t *srcData, uint8_t *dstData, int width, int height, int *realSize) const
{
    static uint8_t annexLHdr1[] = {0x8e, 0x01, 0x00, 0xc5, 0x04, 0x00, 0x00, 0x00};
    static uint8_t annexLHdr2[] = {0x0c, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    int offset = 0;
    char buf[4];

    memcpy(dstData, annexLHdr1, sizeof(annexLHdr1));
    offset += sizeof(annexLHdr1);

    memcpy(&dstData[offset], srcData, 4);
    offset += 4;
    BS_WL32(buf, height);

    memcpy(&dstData[offset], buf, 4);
    offset += 4;
    BS_WL32(buf, width);

    memcpy(&dstData[offset], buf, 4);
    offset += 4;

    memcpy(&dstData[offset], annexLHdr2, sizeof(annexLHdr2));
    offset += sizeof(annexLHdr2);

    *realSize = offset;
}

bool MediaCodec::convertVC1Data(uint8_t *srcData, int srcSize, uint8_t *dstData, int dstSize, int *realSize) const
{
    int offset;

    if (srcSize < 16)
        return false;

    for (offset = 0; offset <= srcSize - 4; offset++)
    {
        uint8_t *ptr = &srcData[offset];

        if (ptr[0] == 0x00 && ptr[1] == 0x00 && ptr[2] == 0x01 && ptr[3] == 0x0f)
            break;
    }

    if (offset > srcSize - 4)
        return false;

    if (offset)
    {
        *realSize = srcSize - offset;

        if (*realSize > dstSize)
            return false;

        memcpy(dstData, &srcData[offset], *realSize);
    }

    return true;
}

void MediaCodec::convertH264ToAnnexB(uint8_t *data, unsigned int size, unsigned int nalSize, H264ConvertState *state) const
{
    if (nalSize < 3 || nalSize > 4)
        return;

    while (size > 0)
    {
        if (state->nalPos < nalSize)
        {
            unsigned int i;

            for (i = 0; state->nalPos < nalSize && i < size; i++, state->nalPos++)
            {
                state->nalLen = (state->nalLen << 8) | data[i];
                data[i] = 0;
            }

            if (state->nalPos < nalSize)
                return;

            data[i - 1] = 1;
            data += i;
            size -= i;
        }

        if (state->nalLen > INT_MAX)
            return;

        if (state->nalLen > size)
        {
            state->nalLen -= size;
            return;
        }
        else
        {
            data += state->nalLen;
            size -= state->nalLen;
            state->nalLen = 0;
            state->nalPos = 0;
        }
    }
}

bool MediaCodec::isNoPaddingDecoder(const QString &name) const
{
    static const char *decoders[] =
    {
        "OMX.SEC.avc.dec",
        "OMX.SEC.avcdec",
        "OMX.SEC.MPEG4.Decoder",
        "OMX.SEC.mpeg4.dec",
        "OMX.SEC.vc1.dec",
        nullptr
    };

    for (int i = 0; decoders[i]; i++)
    {
        if (name == decoders[i])
            return true;
    }

    return false;
}

void MediaCodec::yuv420PlanarCopy(uint8_t *ptr, AVFrame *frame) const
{
    for (int i = 0; i < 3; i++)
    {
        uint8_t *src = ptr;
        int stride = this->m_stride;
        int height;

        if (i == 0)
        {
            height = this->m_codecContext->height;

            src += this->m_cropTop * this->m_stride;
            src += this->m_cropLeft;
        }
        else
        {
            height = this->m_codecContext->height / 2;
            stride = (this->m_stride + 1) / 2;

            src += this->m_sliceHeight * this->m_stride;

            if (i == 2)
                src += ((this->m_sliceHeight + 1) / 2) * stride;

            src += this->m_cropTop * stride;
            src += this->m_cropLeft / 2;
        }

        if (frame->linesize[i] == stride)
        {
            memcpy(frame->data[i], src, height * stride);
        }
        else
        {
            int width;
            uint8_t *dst = frame->data[i];

            if (i == 0)
                width = this->m_codecContext->width;
            else
                width = qMin(frame->linesize[i], FFALIGN(this->m_codecContext->width, 2) / 2);

            for (int j = 0; j < height; j++)
            {
                memcpy(dst, src, width);

                src += stride;
                dst += frame->linesize[i];
            }
        }
    }
}

void MediaCodec::yuv420SemiPlanarCopy(uint8_t *ptr, AVFrame *frame) const
{
    for (int i = 0; i < 2; i++)
    {
        uint8_t *src = ptr;
        int height;

        if (i == 0)
        {
            height = this->m_codecContext->height;

            src += this->m_cropTop * this->m_stride;
            src += this->m_cropLeft;
        }
        else
        {
            height = this->m_codecContext->height / 2;

            src += this->m_sliceHeight * this->m_stride;
            src += this->m_cropTop * this->m_stride;
            src += this->m_cropLeft;
        }

        if (frame->linesize[i] == this->m_stride)
        {
            memcpy(frame->data[i], src, height * this->m_stride);
        }
        else
        {
            int width;
            uint8_t *dst = frame->data[i];

            if (i == 0)
                width = this->m_codecContext->width;
            else
                width = qMin(frame->linesize[i], FFALIGN(this->m_codecContext->width, 2));

            for (int j = 0; j < height; j++)
            {
                memcpy(dst, src, width);

                src += this->m_stride;
                dst += frame->linesize[i];
            }
        }
    }
}

void MediaCodec::yuv420PackedSemiPlanarCopy(uint8_t *ptr, AVFrame *frame) const
{
    for (int i = 0; i < 2; i++)
    {
        uint8_t *src = ptr;
        int height;

        if (i == 0)
        {
            height = this->m_codecContext->height;
        }
        else
        {
            height = this->m_codecContext->height / 2;

            src += (this->m_sliceHeight - this->m_cropTop / 2) * this->m_stride;
            src += this->m_cropTop * this->m_stride;
            src += this->m_cropLeft;
        }

        if (frame->linesize[i] == this->m_stride)
        {
            memcpy(frame->data[i], src, height * this->m_stride);
        }
        else
        {
            int width;
            uint8_t *dst = frame->data[i];

            if (i == 0)
                width = this->m_codecContext->width;
            else
                width = qMin(frame->linesize[i], FFALIGN(this->m_codecContext->width, 2));

            for (int j = 0; j < height; j++)
            {
                memcpy(dst, src, width);

                src += this->m_stride;
                dst += frame->linesize[i];
            }
        }
    }
}

void MediaCodec::qcomCopy(uint8_t *ptr, AVFrame *frame) const
{
    const int TILE_WIDTH = 64;
    const int TILE_HEIGHT = 32;
    const int TILE_SIZE = TILE_WIDTH * TILE_HEIGHT;
    const int TILE_GROUP_SIZE = 4 * TILE_SIZE;

    int width = frame->width;
    int pitch = frame->linesize[0];
    int height = frame->height;

    const int tileWidth = (width - 1) / TILE_WIDTH + 1;
    const int tileWidthAlign = (tileWidth + 1) & ~1;

    const int tileHeightLuma = (height - 1) / TILE_HEIGHT + 1;
    const int tileHeightChroma = (height / 2 - 1) / TILE_HEIGHT + 1;

    int lumaSize = tileWidthAlign * tileHeightLuma * TILE_SIZE;

    if ((lumaSize % TILE_GROUP_SIZE) != 0)
        lumaSize = (((lumaSize - 1) / TILE_GROUP_SIZE) + 1) * TILE_GROUP_SIZE;

    for (int y = 0; y < tileHeightLuma; y++)
    {
        int rowWidth = width;

        for (int x = 0; x < tileWidth; x++)
        {
            const uint8_t *srcLuma  = ptr + this->qcomTilePos(x, y, tileWidthAlign, tileHeightLuma) * TILE_SIZE;
            const uint8_t *srcChroma = ptr + lumaSize +
                    this->qcomTilePos(x, y / 2, tileWidthAlign, tileHeightChroma) * TILE_SIZE;

            if (y & 1)
                srcChroma += TILE_SIZE / 2;

            int tileWidth = rowWidth;

            if (tileWidth > TILE_WIDTH)
                tileWidth = TILE_WIDTH;

            size_t tileHeight = height;

            if (tileHeight > TILE_HEIGHT)
                tileHeight = TILE_HEIGHT;

            size_t lumaIdx = y * TILE_HEIGHT * pitch + x * TILE_WIDTH;
            size_t chromaIdx = (lumaIdx / pitch) * pitch / 2 + (lumaIdx % pitch);

            tileHeight /= 2;

            while (tileHeight--)
            {
                memcpy(&frame->data[0][lumaIdx], srcLuma, tileWidth);
                srcLuma += TILE_WIDTH;
                lumaIdx += pitch;

                memcpy(&frame->data[0][lumaIdx], srcLuma, tileWidth);
                srcLuma += TILE_WIDTH;
                lumaIdx += pitch;

                memcpy(&frame->data[1][chromaIdx], srcChroma, tileWidth);
                srcChroma += TILE_WIDTH;
                chromaIdx += pitch;
            }

            rowWidth -= TILE_WIDTH;
        }

        height -= TILE_HEIGHT;
    }
}

int MediaCodec::qcomTilePos(int x, int y, int width, int height) const
{
    int flim = x + (y & ~1) * width;

    if (y & 1)
        flim += (x & ~3) + 2;
    else if ((height & 1) == 0 || y != (height - 1))
        flim += (x + 2) & ~3;

    return flim;
}

bool MediaCodec::reOpen()
{
    this->close();
    return this->open(this->m_codecContext);
}

void MediaCodec::copyHWPicture(AVFrame *frame, uint8_t *ptr) const
{
    switch (this->m_pixFormat)
    {
        case OMX_COLOR_FormatYUV420Planar:
            this->yuv420PlanarCopy(ptr, frame);
            break;
        case OMX_QCOM_COLOR_FormatYVU420SemiPlanar:
        case OMX_QCOM_COLOR_FormatYUV420SemiPlanar32m:
        case OMX_COLOR_FormatYUV420SemiPlanar:
            this->yuv420SemiPlanarCopy(ptr, frame);
            break;
        case OMX_COLOR_FormatYUV420PackedPlanar:
        case OMX_TI_COLOR_FormatYUV420PackedSemiPlanar:
        case OMA_TI_COLOR_FormatYUV420PackedSemiPlanarInterlaced:
            this->yuv420PackedSemiPlanarCopy(ptr, frame);
            break;
        case QOMX_COLOR_FormatYUV420PackedSemiPlanar64x32Tile2m8ka:
            this->qcomCopy(ptr, frame);
            break;
        default:
            break;
    }
}
