﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "audio/SPDIFInterface.h"

class QAndroidJniObject;

class SPDIFAudioTrack : public SPDIFInterface
{
public:
    SPDIFAudioTrack();
    virtual ~SPDIFAudioTrack();

    virtual bool open();
    virtual void close();
    virtual bool checkSupport();

    virtual bool play();
    virtual bool pause();
    virtual bool resume();
    virtual bool stop();

    virtual bool fillBuffer(void *buffer, int size);
    virtual unsigned int getNeedBufferSize() const;

    virtual void getDeviceList(QStringList *ret);
    virtual int getDeviceCount() const;

    virtual double getLatency();
    virtual bool isSupportPull() const;
    virtual int getAlignSize() const;

    virtual bool canFillBufferBeforeStart() const;

private:
    bool isAML() const;

    void closeInternal();

    bool createAudioTrack(int sampleRate, int channelMask, int audioFormat, int bufferSize);
    int getMinBufferSize(int sampleRate, int channelMask, int encoding) const;

    void audioPlay();
    void audioPause();
    void audioStop();
    void audioFlush();
    void audioRelease();

    int audioWrite(void *buffer, int size);
    int audioGetState();

private:
    static int STREAM_MUSIC;
    static int MODE_STREAM;

    static int ENCODING_PCM_16BIT;
    static int ENCODING_IEC61937;

    static int CHANNEL_OUT_STEREO;
    static int CHANNEL_OUT_5POINT1;

    static int CHANNEL_OUT_SIDE_LEFT;
    static int CHANNEL_OUT_SIDE_RIGHT;

    static int STATE_INITIALIZED;

private:
    static const char *AUDIO_MANAGER;
    static const char *AUDIO_TRACK;
    static const char *AUDIO_FORMAT;

private:
    QAndroidJniObject *m_audioTrack;
    QAndroidJniObject *m_buffer;
    int m_audioFormat;
};
