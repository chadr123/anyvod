#!/bin/bash

. ../../../../installer/common/functions.sh

qt_path=/usr/local/qt5

if [ ! -d ../../../../package ]; then
  mkdir ../../../../package
  check
fi

if [ -d ../../../../package/client ]; then
  rm -rf ../../../../package/client
  check
fi

if [ -d ../../../../package/client/licenses ]; then
  rm -rf ../../../../package/client/licenses
  check
fi

mkdir ../../../../package/client
check

mkdir ../../../../package/client/imageformats
check

mkdir ../../../../package/client/platforms
check

mkdir ../../../../package/client/licenses
check

mkdir ../../../../package/client/qt_qml
check

cp ../../../../licenses/noto.txt ../../../../licenses/bass.txt ../../../../licenses/ffmpeg.txt \
   ../../../../licenses/libass.txt ../../../../licenses/qt.txt ../../../../licenses/anyvod.txt \
   ../../../../package/client/licenses
check

cp -a ../../libs/raspi/* ../../../../package/client
check

mkdir ../../../../package/client/fonts
check

cp ../../fonts/* ../../../../package/client/fonts
check

cp -a AnyVODMobileClient.sh ../../../../package/client
check

cp -a qt.conf ../../../../package/client
check

cd ../../../../package/client
check

cd -
check

cp -a ../../../AnyVODMobileClient-build-raspi/AnyVODMobileClient ../../../../package/client/AnyVODMobileClient_bin
check

mkdir ../../../../package/client/languages
check

cp ../../languages/*.qm ../../../../package/client/languages
check

cp -a $qt_path/lib/libQt?Widgets.so* ../../../../package/client
check

cp -a $qt_path/lib/libQt?Xml.so* ../../../../package/client
check

cp -a $qt_path/lib/libQt?Network.so* ../../../../package/client
check

cp -a $qt_path/lib/libQt?Gui.so* ../../../../package/client
check

cp -a $qt_path/lib/libQt?Core.so* ../../../../package/client
check

cp -a $qt_path/lib/libQt?DBus.so* ../../../../package/client
check

cp -a $qt_path/lib/libQt?EglFSDeviceIntegration.so* ../../../../package/client
check

cp -a $qt_path/lib/libQt?EglFsKmsSupport.so* ../../../../package/client
check

cp -a $qt_path/lib/libQt?Qml*.so* ../../../../package/client
check

cp -a $qt_path/lib/libQt?Quick.so* ../../../../package/client
check

cp -a $qt_path/lib/libQt?QuickControls2.so* ../../../../package/client
check

cp -a $qt_path/lib/libQt?QuickTemplates2.so* ../../../../package/client
check

cp -a $qt_path/lib/libQt?Svg.so* ../../../../package/client
check

cp -a $qt_path/lib/libQt?Sensors.so* ../../../../package/client
check

cp -a $qt_path/lib/libQt?VirtualKeyboard.so** ../../../../package/client
check

cp -a $qt_path/qml/* ../../../../package/client/qt_qml
check

cp -a $qt_path/plugins/imageformats/*.so ../../../../package/client/imageformats
check

cp -a $qt_path/plugins/platforms/libqeglfs.so ../../../../package/client/platforms
check

cp -a $qt_path/plugins/platforminputcontexts ../../../../package/client
check

cp -a $qt_path/plugins/virtualkeyboard ../../../../package/client
check

cp -a $qt_path/plugins/egldeviceintegrations ../../../../package/client
check

exit 0
