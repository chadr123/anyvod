#!/bin/bash

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:%BIN_PATH%
%BIN_PATH%/AnyVODMobileClient_bin "$@"

ret=$?

[ $ret -eq 3 ] && sudo poweroff && exit 0

exit $ret
