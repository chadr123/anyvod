#!/bin/bash

. ../../../../installer/common/functions.sh

version=`cat ../../../AnyVODClient/scripts/linux/version`
qt_path=`../../../AnyVODClient/scripts/linux/qt_path.sh`

export PATH=$HOME/$qt_path/$version/ios/bin:$PATH

if [ ! -d ../../../../package ]; then
  mkdir ../../../../package
  check
fi

cd ../../../AnyVODMobileClient-build-ios
check

cp ../AnyVODMobileClient/scripts/ios/exportPlist.plist ./
check

xcodebuild -exportArchive -archivePath AnyVODMobileClient.xcarchive -exportPath ./ -exportOptionsPlist exportPlist.plist
check

exit 0
