#!/bin/bash

. ../../../../installer/common/functions.sh

version=`cat ../../../AnyVODClient/scripts/linux/version`
qt_path=`../../../AnyVODClient/scripts/linux/qt_path.sh`
developer=$1

export PATH=$HOME/$qt_path/$version/ios/bin:$PATH

./premake.sh
check

cd ../../../
check

if [ -e AnyVODMobileClient-build-ios ]; then
  rm -rf AnyVODMobileClient-build-ios
  check
fi

mkdir AnyVODMobileClient-build-ios
check

cd AnyVODMobileClient-build-ios
check

make distclean -w

if [ -e ../AnyVODMobileClient/ios.pri ]; then
  mv ../AnyVODMobileClient/ios.pri ../AnyVODMobileClient/ios.pri_org
  check
fi

sed "s/%TEAMID%/${developer}/g" ../AnyVODMobileClient/scripts/ios/ios.pri > ../AnyVODMobileClient/ios.pri
ret=$?
if [ $ret -ne 0 ]; then
  if [ -e ../AnyVODMobileClient/ios.pri_org ]; then
    mv ../AnyVODMobileClient/ios.pri_org ../AnyVODMobileClient/ios.pri
  fi

  exit $ret
fi

qmake ../AnyVODMobileClient/AnyVODMobileClient.pro -r -spec macx-ios-clang CONFIG+=release CONFIG+=iphoneos CONFIG+=device CONFIG+=qtquickcompiler
ret=$?
if [ $ret -ne 0 ]; then
  if [ -e ../AnyVODMobileClient/ios.pri_org ]; then
    mv ../AnyVODMobileClient/ios.pri_org ../AnyVODMobileClient/ios.pri
  fi

  exit $ret
fi

if [ -e ../AnyVODMobileClient/ios.pri_org ]; then
  mv ../AnyVODMobileClient/ios.pri_org ../AnyVODMobileClient/ios.pri
else
  rm ../AnyVODMobileClient/ios.pri
fi
check

xcodebuild -allowProvisioningUpdates -scheme AnyVODMobileClient -archivePath AnyVODMobileClient.xcarchive OTHER_CODE_SIGN_FLAGS='--generate-entitlement-der' archive
check

cd ../AnyVODMobileClient/scripts/ios
check

exit 0
