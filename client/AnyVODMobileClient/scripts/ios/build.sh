#!/bin/bash

. ../../../../installer/common/functions.sh

./make.sh $1
check

./deploy.sh
check

exit 0
