#!/bin/bash

. ../../../../installer/common/functions.sh

version=`cat ../../../AnyVODClient/scripts/linux/version`
qt_path=`../../../AnyVODClient/scripts/linux/qt_path.sh`
export PATH=$HOME/$qt_path/$version/ios/bin:$PATH

lrelease ../../AnyVODMobileClient.pro
check

exit 0
