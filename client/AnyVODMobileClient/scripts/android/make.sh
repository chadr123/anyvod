#!/bin/bash

. ../../../../installer/common/functions.sh

version=`cat ../../../AnyVODClient/scripts/linux/version`
qt_path=`../../../AnyVODClient/scripts/linux/qt_path.sh`
export PATH=$HOME/$qt_path/$version/android/bin:$PATH

./premake.sh
check

. ./toolchain.sh

cd ../../../
check

if [ -e AnyVODMobileClient-build-android ]; then
  rm -rf AnyVODMobileClient-build-android
  check
fi

mkdir AnyVODMobileClient-build-android
check

cd AnyVODMobileClient-build-android
check

make distclean -w

qmake ../AnyVODMobileClient/AnyVODMobileClient.pro -r -spec android-clang CONFIG+=qtquickcompiler ANDROID_ABIS="armeabi-v7a arm64-v8a x86"
check

make -w -j8
check

make install INSTALL_ROOT=./android-build
check

cd ../AnyVODMobileClient/scripts/android
check

exit 0
