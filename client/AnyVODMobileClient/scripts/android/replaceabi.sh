#!/bin/bash

. ../../../../installer/common/functions.sh

cmd=$1
abi=$2

function replace()
{
  cp -af ../../resources/android/AndroidManifest.xml ./AndroidManifest.xml_org
  check

  version=`perl -ne 'print $1 if /android:versionName=\"([^\"]*)/' ../../resources/android/AndroidManifest.xml`
  ret=$?
  if [ $ret -ne 0 ]; then
    unreplace
    exit $ret
  fi

  code=`echo $version | awk -F . '{ if ($1 == "" || $2 == "" || $3 == "" || "'${abi}'" == "") printf ""; else printf "%d%02d%02d%02d", $1, $2, $3, "'${abi}'"; }'`
  if [ "$code" == "" ]; then
    echo "Version code is empty -> version : $version, abi : $abi"
    unreplace
    exit 1
  fi

  sed -e 's/android:versionCode=\"[^\"]*/android:versionCode=\"'${code}'/g' ../../resources/android/AndroidManifest.xml > ../../resources/android/AndroidManifest.xml_replaced
  ret=$?
  if [ $ret -ne 0 ]; then
    unreplace
    exit $ret
  fi

  mv -f ../../resources/android/AndroidManifest.xml_replaced ../../resources/android/AndroidManifest.xml
  ret=$?
  if [ $ret -ne 0 ]; then
    unreplace
    exit $ret
  fi
}

function unreplace()
{
  mv -f ./AndroidManifest.xml_org ../../resources/android/AndroidManifest.xml
  check
}

case "$cmd" in
  replace)
    replace
  ;;
  unreplace)
    unreplace
  ;;
  *)
    echo "Unsupported command : $cmd"
    exit 1
  ;;
esac

exit 0
