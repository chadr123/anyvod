# Add more folders to ship with the application, here
android-* {
    languages.source = languages/*.qm
    languages.target = languages

    fonts.source = fonts/*.*
    fonts.target = fonts

    licenses.target = licenses
    licenses.source = ../../licenses/noto.txt ../../licenses/bass.txt ../../licenses/ffmpeg.txt \
                      ../../licenses/libass.txt ../../licenses/qt.txt ../../licenses/anyvod.txt

    DEPLOYMENTFOLDERS += languages licenses
}

ios {
    icon.source = ../../images/icons/app.icns
    icon.target = ./

    fonts.source = fonts
    fonts.target = ./

    DEPLOYMENTFOLDERS += icon

    languages.files = $$files($$PWD/languages/*.qm)
    languages.path = languages

    licenses.files = ../../licenses/noto.txt ../../licenses/bass.txt ../../licenses/ffmpeg.txt \
                     ../../licenses/libass.txt ../../licenses/qt.txt ../../licenses/anyvod.txt
    licenses.path = licenses

    icons.files = $$files($$PWD/resources/ios/icons/*.png)
    launches.files = resources/ios/splash/AnyVODLaunchScreen.xib $$files($$PWD/resources/ios/splash/*.png)

    QMAKE_BUNDLE_DATA += languages licenses icons launches
}

!linux-rasp-pi3-* : !linux-g++ : DEPLOYMENTFOLDERS += fonts

# Additional import path used to resolve QML modules in Creator's code model
QML_IMPORT_PATH =

QT += core gui network qml quick xml sensors

android-*: QT += androidextras

CONFIG += c++11

ios: CONFIG -= bitcode

HEADERS += \
    ../../common/MemBuffer.h \
    ../../common/RetreiveSubtitle.h \
    ../../common/SubtitleFileNameGenerator.h \
    ../AnyVODClient/src/audio/SPDIFSampleRates.h \
    ../AnyVODClient/src/core/AnyVODEnums.h \
    ../AnyVODClient/src/core/ChapterInfo.h \
    ../AnyVODClient/src/core/EnumsTranslator.h \
    ../AnyVODClient/src/core/ExtraPlayData.h \
    ../AnyVODClient/src/core/FileExtensions.h \
    ../AnyVODClient/src/core/FrameMetaData.h \
    ../AnyVODClient/src/core/Lyrics.h \
    ../AnyVODClient/src/core/MediaType.h \
    ../AnyVODClient/src/core/PlayItem.h \
    ../AnyVODClient/src/core/StreamInfo.h \
    ../AnyVODClient/src/core/SystemResourceUsage.h \
    ../AnyVODClient/src/core/TextureInfo.h \
    ../AnyVODClient/src/core/Common.h \
    ../AnyVODClient/src/core/TextEncodingDetector.h \
    ../AnyVODClient/src/core/UserAspectRatioInfo.h \
    ../AnyVODClient/src/core/Version.h \
    ../AnyVODClient/src/core/VersionDescription.h \
    ../AnyVODClient/src/device/RadioChannelMap.h \
    ../AnyVODClient/src/device/RadioReader.h \
    ../AnyVODClient/src/device/RadioReaderInterface.h \
    ../AnyVODClient/src/language/GlobalLanguage.h \
    ../AnyVODClient/src/media/AudioRenderer.h \
    ../AnyVODClient/src/media/Callbacks.h \
    ../AnyVODClient/src/media/Concurrent.h \
    ../AnyVODClient/src/media/Detail.h \
    ../AnyVODClient/src/media/MediaPresenterInterface.h \
    ../AnyVODClient/src/media/MediaThreadBase.h \
    ../AnyVODClient/src/media/Surface.h \
    ../AnyVODClient/src/media/VideoFrameQueue.h \
    ../AnyVODClient/src/media/VideoPicture.h \
    ../AnyVODClient/src/media/VideoRenderer.h \
    ../AnyVODClient/src/net/NetworkProxy.h \
    ../AnyVODClient/src/parsers/playlist/ASXParser.h \
    ../AnyVODClient/src/parsers/playlist/B4SParser.h \
    ../AnyVODClient/src/parsers/playlist/CueParser.h \
    ../AnyVODClient/src/parsers/playlist/M3UParser.h \
    ../AnyVODClient/src/parsers/playlist/PlayListParserGenerator.h \
    ../AnyVODClient/src/parsers/playlist/PlayListParserInterface.h \
    ../AnyVODClient/src/parsers/playlist/PLSParser.h \
    ../AnyVODClient/src/parsers/playlist/WPLParser.h \
    ../AnyVODClient/src/parsers/subtitle/ASSParser.h \
    ../AnyVODClient/src/parsers/subtitle/AVParser.h \
    ../AnyVODClient/src/parsers/subtitle/GlobalSubtitleCodecName.h \
    ../AnyVODClient/src/parsers/subtitle/LRCParser.h \
    ../AnyVODClient/src/parsers/subtitle/SAMIParser.h \
    ../AnyVODClient/src/parsers/subtitle/SRTParser.h \
    ../AnyVODClient/src/parsers/subtitle/YouTubeParser.h \
    ../AnyVODClient/src/parsers/subtitle/NaverTVParser.h \
    ../AnyVODClient/src/audio/SPDIF.h \
    ../AnyVODClient/src/audio/SPDIFEncoder.h \
    ../AnyVODClient/src/audio/SPDIFInterface.h \
    ../AnyVODClient/src/decoders/HWDecoder.h \
    ../AnyVODClient/src/decoders/HWDecoderInterface.h \
    ../AnyVODClient/src/media/PacketQueue.h \
    ../AnyVODClient/src/media/SyncType.h \
    ../AnyVODClient/src/media/MediaPresenter.h \
    ../AnyVODClient/src/media/MediaState.h \
    ../AnyVODClient/src/media/ReadThread.h \
    ../AnyVODClient/src/media/SubtitleThread.h \
    ../AnyVODClient/src/media/VideoThread.h \
    ../AnyVODClient/src/media/LastPlay.h \
    ../AnyVODClient/src/media/RefreshThread.h \
    ../AnyVODClient/src/media/FrameExtractor.h \
    ../AnyVODClient/src/net/HttpDownloader.h \
    ../AnyVODClient/src/net/SubtitleImpl.h \
    ../AnyVODClient/src/net/SyncHttp.h \
    ../AnyVODClient/src/net/Updater.h \
    ../AnyVODClient/src/net/VirtualFile.h \
    ../AnyVODClient/src/net/NetFile.h \
    ../AnyVODClient/src/net/Socket.h \
    ../AnyVODClient/src/screensaver/ScreenSaverPreventer.h \
    ../AnyVODClient/src/screensaver/ScreenSaverPreventerInterface.h \
    ../AnyVODClient/src/setting/DTVReaderSettingBase.h \
    ../AnyVODClient/src/setting/DTVReaderSettingLoader.h \
    ../AnyVODClient/src/setting/DTVReaderSettingSaver.h \
    ../AnyVODClient/src/setting/GlobalSettingBase.h \
    ../AnyVODClient/src/setting/RadioReaderSettingBase.h \
    ../AnyVODClient/src/setting/RadioReaderSettingLoader.h \
    ../AnyVODClient/src/setting/RadioReaderSettingSaver.h \
    ../AnyVODClient/src/setting/SettingBackupAndRestore.h \
    ../AnyVODClient/src/setting/Settings.h \
    ../AnyVODClient/src/setting/SettingsFactory.h \
    ../AnyVODClient/src/setting/SettingsSynchronizer.h \
    ../AnyVODClient/src/ui/UserAspectRatios.h \
    ../AnyVODClient/src/utils/ConvertingUtils.h \
    ../AnyVODClient/src/utils/FileListUtils.h \
    ../AnyVODClient/src/utils/FloatPointUtils.h \
    ../AnyVODClient/src/utils/FrameUtils.h \
    ../AnyVODClient/src/utils/MathUtils.h \
    ../AnyVODClient/src/utils/MediaTypeUtils.h \
    ../AnyVODClient/src/utils/PathUtils.h \
    ../AnyVODClient/src/utils/PixelUtils.h \
    ../AnyVODClient/src/utils/PlayItemUtils.h \
    ../AnyVODClient/src/utils/RemoteFileUtils.h \
    ../AnyVODClient/src/utils/RotationUtils.h \
    ../AnyVODClient/src/utils/SeparatingUtils.h \
    ../AnyVODClient/src/utils/StringUtils.h \
    ../AnyVODClient/src/utils/TimeUtils.h \
    ../AnyVODClient/src/video/AnaglyphMatrix.h \
    ../AnyVODClient/src/video/BaseShaderPrograms.h \
    ../AnyVODClient/src/video/ColorConversion.h \
    ../AnyVODClient/src/video/Deinterlacer.h \
    ../AnyVODClient/src/video/Font.h \
    ../AnyVODClient/src/video/ScreenCaptureHelper.h \
    ../AnyVODClient/src/video/ShaderCompositer.h \
    ../AnyVODClient/src/video/FilterGraph.h \
    ../AnyVODClient/src/video/Camera.h \
    ../AnyVODClient/src/video/Sphere.h \
    ../AnyVODClient/src/video/Cube.h \
    ../AnyVODClient/src/video/PrimaryMatrix.h \
    ../AnyVODClient/src/pickers/URLPicker.h \
    ../AnyVODClient/src/pickers/URLPickerInterface.h \
    ../AnyVODClient/src/pickers/YouTubeURLPicker.h \
    ../AnyVODClient/src/pickers/KakaoTVURLPicker.h \
    ../AnyVODClient/src/pickers/NaverTVURLPicker.h \
    ../AnyVODClient/src/pickers/TwitchURLPicker.h \
    src/core/AutoStart.h \
    src/core/QmlRegistrar.h \
    src/language/KeyboardLanguage.h \
    src/media/RadioListItemUpdater.h \
    src/models/FileListModel.h \
    src/models/PlayListModel.h \
    src/models/DTVListModel.h \
    src/models/EPGListModel.h \
    src/models/RadioListModel.h \
    src/models/SearchMediaModel.h \
    src/models/ManagePlayListModel.h \
    src/models/RemoteFileListModel.h \
    src/models/ScreenExplorerListModel.h \
    src/models/ScreenExplorerProvider.h \
    src/media/ThumbnailCache.h \
    src/media/PlayListItemUpdater.h \
    src/media/DTVListItemUpdater.h \
    src/media/ScreenExplorerListItemUpdater.h \
    src/setting/RenderScreenSettingBase.h \
    src/setting/RenderScreenSettingLoader.h \
    src/setting/RenderScreenSettingSaver.h \
    src/ui/FontInfo.h \
    src/ui/delegates/AudioDeviceDelegate.h \
    src/ui/delegates/AutoStartDelegate.h \
    src/ui/delegates/CaptureExtDelegate.h \
    src/ui/delegates/DTVDelegate.h \
    src/ui/delegates/EqualizerDelegate.h \
    src/ui/delegates/FileTypeDelegate.h \
    src/ui/delegates/FloatPointUtilDelegate.h \
    src/ui/delegates/FontInfoDelegate.h \
    src/ui/delegates/LanguageDelegate.h \
    src/ui/delegates/LicenseDelegate.h \
    src/ui/delegates/MainSettingDelegate.h \
    src/ui/delegates/MediaPlayerSettingDelegate.h \
    src/ui/delegates/RadioDelegate.h \
    src/ui/delegates/RemoteFileUtilDelegate.h \
    src/ui/delegates/SPDIFDelegate.h \
    src/ui/delegates/SocketDelegate.h \
    src/ui/delegates/SplashScreenDelegate.h \
    src/ui/delegates/StatusBarDelegate.h \
    src/ui/delegates/TextCodecDelegate.h \
    src/ui/delegates/TimeUtilDelegate.h \
    src/ui/delegates/ToastDelegate.h \
    src/ui/delegates/UpdaterDelegate.h \
    src/ui/delegates/VersionDelegate.h \
    src/utils/AnyVODEnumsUtils.h \
    src/video/GLRenderer.h \
    src/ui/RenderScreen.h \
    src/ui/StatusUpdater.h \
    src/core/AnyVODWindow.h \
    src/core/BuildNumber.h \
    src/core/AnyVODGlobal.h \
    src/sensor/RotationSensor.h

android-*: HEADERS += \
    src/decoders/MediaCodec.h \
    src/screensaver/AndroidPreventer.h \
    src/audio/SPDIFAudioTrack.h

ios: HEADERS += \
    ../AnyVODClient/src/utils/MacUtils.h \
    ../AnyVODClient/src/decoders/VideoToolBoxDecoder.h \
    src/core/IOSDelegate.h \
    src/core/IOSViewController.h \
    src/audio/SPDIFAudioUnit.h \
    src/screensaver/IOSPreventer.h \

linux-rasp-pi3-* | linux-g++: HEADERS += \
    ../AnyVODClient/src/audio/SPDIFAlsa.h \
    ../AnyVODClient/src/device/DTVLinuxDVB.h \
    ../AnyVODClient/src/device/DTVReader.h \
    ../AnyVODClient/src/device/DTVReaderInterface.h \
    ../AnyVODClient/src/device/DTVChannelMap.h \
    ../AnyVODClient/src/device/radio/RadioGNURadio.h \
    ../AnyVODClient/src/device/radio/gnuradio/RadioReceiverInterface.h \
    ../AnyVODClient/src/device/radio/gnuradio/RadioReceiverWideBandFM.h \
    ../AnyVODClient/src/device/radio/gnuradio/blocks/RadioAudioSink.h \
    ../AnyVODClient/src/device/radio/gnuradio/blocks/RadioBandPassFilter.h \
    ../AnyVODClient/src/device/radio/gnuradio/blocks/RadioComplexResampler.h \
    ../AnyVODClient/src/device/radio/gnuradio/blocks/RadioDownConverter.h \
    ../AnyVODClient/src/device/radio/gnuradio/blocks/RadioFIRDecimator.h \
    ../AnyVODClient/src/device/radio/gnuradio/blocks/RadioFMDeemphasis.h \
    ../AnyVODClient/src/device/radio/gnuradio/blocks/RadioFMDemodulator.h \
    ../AnyVODClient/src/device/radio/gnuradio/blocks/RadioFloatPointResampler.h \
    ../AnyVODClient/src/device/radio/gnuradio/blocks/RadioLowPassFilter.h \
    ../AnyVODClient/src/device/radio/gnuradio/blocks/RadioResamplerBase.h \
    ../AnyVODClient/src/device/radio/gnuradio/blocks/RadioStereoDemodulator.h \
    ../AnyVODClient/src/screensaver/LinuxPreventer.h \
    ../AnyVODClient/src/utils/DeviceUtils.h \
    src/screensaver/RaspberryPiPreventer.h \
    src/screensaver/XDGScreenSaverDisabler.h

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += \
    ../../common/MemBuffer.cpp \
    ../../common/RetreiveSubtitle.cpp \
    ../../common/SubtitleFileNameGenerator.cpp \
    ../AnyVODClient/src/audio/SPDIFSampleRates.cpp \
    ../AnyVODClient/src/core/Common.cpp \
    ../AnyVODClient/src/core/EnumsTranslator.cpp \
    ../AnyVODClient/src/core/FileExtensions.cpp \
    ../AnyVODClient/src/core/SystemResourceUsage.cpp \
    ../AnyVODClient/src/core/TextEncodingDetector.cpp \
    ../AnyVODClient/src/core/Version.cpp \
    ../AnyVODClient/src/core/VersionDescription.cpp \
    ../AnyVODClient/src/device/RadioChannelMap.cpp \
    ../AnyVODClient/src/device/RadioReader.cpp \
    ../AnyVODClient/src/device/RadioReaderInterface.cpp \
    ../AnyVODClient/src/language/GlobalLanguage.cpp \
    ../AnyVODClient/src/media/AudioRenderer.cpp \
    ../AnyVODClient/src/media/MediaPresenterInterface.cpp \
    ../AnyVODClient/src/media/MediaThreadBase.cpp \
    ../AnyVODClient/src/media/Surface.cpp \
    ../AnyVODClient/src/media/VideoFrameQueue.cpp \
    ../AnyVODClient/src/media/VideoPicture.cpp \
    ../AnyVODClient/src/media/VideoRenderer.cpp \
    ../AnyVODClient/src/net/NetworkProxy.cpp \
    ../AnyVODClient/src/parsers/playlist/ASXParser.cpp \
    ../AnyVODClient/src/parsers/playlist/B4SParser.cpp \
    ../AnyVODClient/src/parsers/playlist/CueParser.cpp \
    ../AnyVODClient/src/parsers/playlist/M3UParser.cpp \
    ../AnyVODClient/src/parsers/playlist/PlayListParserGenerator.cpp \
    ../AnyVODClient/src/parsers/playlist/PlayListParserInterface.cpp \
    ../AnyVODClient/src/parsers/playlist/PLSParser.cpp \
    ../AnyVODClient/src/parsers/playlist/WPLParser.cpp \
    ../AnyVODClient/src/parsers/subtitle/ASSParser.cpp \
    ../AnyVODClient/src/parsers/subtitle/AVParser.cpp \
    ../AnyVODClient/src/parsers/subtitle/GlobalSubtitleCodecName.cpp \
    ../AnyVODClient/src/parsers/subtitle/LRCParser.cpp \
    ../AnyVODClient/src/parsers/subtitle/SAMIParser.cpp \
    ../AnyVODClient/src/parsers/subtitle/SRTParser.cpp \
    ../AnyVODClient/src/parsers/subtitle/YouTubeParser.cpp \
    ../AnyVODClient/src/parsers/subtitle/NaverTVParser.cpp \
    ../AnyVODClient/src/audio/SPDIF.cpp \
    ../AnyVODClient/src/audio/SPDIFEncoder.cpp \
    ../AnyVODClient/src/audio/SPDIFInterface.cpp \
    ../AnyVODClient/src/decoders/HWDecoder.cpp \
    ../AnyVODClient/src/decoders/HWDecoderInterface.cpp \
    ../AnyVODClient/src/media/MediaPresenter.cpp \
    ../AnyVODClient/src/media/ReadThread.cpp \
    ../AnyVODClient/src/media/SubtitleThread.cpp \
    ../AnyVODClient/src/media/VideoThread.cpp \
    ../AnyVODClient/src/media/LastPlay.cpp \
    ../AnyVODClient/src/media/RefreshThread.cpp \
    ../AnyVODClient/src/media/PacketQueue.cpp \
    ../AnyVODClient/src/media/FrameExtractor.cpp \
    ../AnyVODClient/src/net/SubtitleImpl.cpp \
    ../AnyVODClient/src/net/SyncHttp.cpp \
    ../AnyVODClient/src/net/HttpDownloader.cpp \
    ../AnyVODClient/src/net/Updater.cpp \
    ../AnyVODClient/src/net/VirtualFile.cpp \
    ../AnyVODClient/src/net/NetFile.cpp \
    ../AnyVODClient/src/net/Socket.cpp \
    ../AnyVODClient/src/screensaver/ScreenSaverPreventer.cpp \
    ../AnyVODClient/src/screensaver/ScreenSaverPreventerInterface.cpp \
    ../AnyVODClient/src/setting/DTVReaderSettingBase.cpp \
    ../AnyVODClient/src/setting/DTVReaderSettingLoader.cpp \
    ../AnyVODClient/src/setting/DTVReaderSettingSaver.cpp \
    ../AnyVODClient/src/setting/GlobalSettingBase.cpp \
    ../AnyVODClient/src/setting/RadioReaderSettingBase.cpp \
    ../AnyVODClient/src/setting/RadioReaderSettingLoader.cpp \
    ../AnyVODClient/src/setting/RadioReaderSettingSaver.cpp \
    ../AnyVODClient/src/setting/SettingBackupAndRestore.cpp \
    ../AnyVODClient/src/setting/Settings.cpp \
    ../AnyVODClient/src/setting/SettingsFactory.cpp \
    ../AnyVODClient/src/setting/SettingsSynchronizer.cpp \
    ../AnyVODClient/src/ui/UserAspectRatios.cpp \
    ../AnyVODClient/src/utils/ConvertingUtils.cpp \
    ../AnyVODClient/src/utils/FileListUtils.cpp \
    ../AnyVODClient/src/utils/FloatPointUtils.cpp \
    ../AnyVODClient/src/utils/FrameUtils.cpp \
    ../AnyVODClient/src/utils/MathUtils.cpp \
    ../AnyVODClient/src/utils/MediaTypeUtils.cpp \
    ../AnyVODClient/src/utils/PathUtils.cpp \
    ../AnyVODClient/src/utils/PixelUtils.cpp \
    ../AnyVODClient/src/utils/PlayItemUtils.cpp \
    ../AnyVODClient/src/utils/RemoteFileUtils.cpp \
    ../AnyVODClient/src/utils/RotationUtils.cpp \
    ../AnyVODClient/src/utils/SeparatingUtils.cpp \
    ../AnyVODClient/src/utils/StringUtils.cpp \
    ../AnyVODClient/src/utils/TimeUtils.cpp \
    ../AnyVODClient/src/video/AnaglyphMatrix.cpp \
    ../AnyVODClient/src/video/BaseShaderPrograms.cpp \
    ../AnyVODClient/src/video/ColorConversion.cpp \
    ../AnyVODClient/src/video/Deinterlacer.cpp \
    ../AnyVODClient/src/video/Font.cpp \
    ../AnyVODClient/src/video/ScreenCaptureHelper.cpp \
    ../AnyVODClient/src/video/ShaderCompositer.cpp \
    ../AnyVODClient/src/video/FilterGraph.cpp \
    ../AnyVODClient/src/video/Camera.cpp \
    ../AnyVODClient/src/video/Sphere.cpp \
    ../AnyVODClient/src/video/Cube.cpp \
    ../AnyVODClient/src/video/PrimaryMatrix.cpp \
    ../AnyVODClient/src/device/DTVReader.cpp \
    ../AnyVODClient/src/device/DTVReaderInterface.cpp \
    ../AnyVODClient/src/device/DTVChannelMap.cpp \
    ../AnyVODClient/src/pickers/URLPicker.cpp \
    ../AnyVODClient/src/pickers/URLPickerInterface.cpp \
    ../AnyVODClient/src/pickers/YouTubeURLPicker.cpp \
    ../AnyVODClient/src/pickers/KakaoTVURLPicker.cpp \
    ../AnyVODClient/src/pickers/NaverTVURLPicker.cpp \
    ../AnyVODClient/src/pickers/TwitchURLPicker.cpp \
    src/core/AutoStart.cpp \
    src/core/QmlRegistrar.cpp \
    src/core/main.cpp \
    src/core/AnyVODWindow.cpp \
    src/core/AnyVODGlobal.cpp \
    src/language/KeyboardLanguage.cpp \
    src/media/RadioListItemUpdater.cpp \
    src/models/FileListModel.cpp \
    src/models/PlayListModel.cpp \
    src/models/DTVListModel.cpp \
    src/models/EPGListModel.cpp \
    src/models/RadioListModel.cpp \
    src/models/SearchMediaModel.cpp \
    src/models/ManagePlayListModel.cpp \
    src/models/RemoteFileListModel.cpp \
    src/models/ScreenExplorerListModel.cpp \
    src/models/ScreenExplorerProvider.cpp \
    src/media/ThumbnailCache.cpp \
    src/media/PlayListItemUpdater.cpp \
    src/media/DTVListItemUpdater.cpp \
    src/media/ScreenExplorerListItemUpdater.cpp \
    src/setting/RenderScreenSettingBase.cpp \
    src/setting/RenderScreenSettingLoader.cpp \
    src/setting/RenderScreenSettingSaver.cpp \
    src/ui/FontInfo.cpp \
    src/ui/delegates/AudioDeviceDelegate.cpp \
    src/ui/delegates/AutoStartDelegate.cpp \
    src/ui/delegates/CaptureExtDelegate.cpp \
    src/ui/delegates/DTVDelegate.cpp \
    src/ui/delegates/EqualizerDelegate.cpp \
    src/ui/delegates/FileTypeDelegate.cpp \
    src/ui/delegates/FloatPointUtilDelegate.cpp \
    src/ui/delegates/FontInfoDelegate.cpp \
    src/ui/delegates/LanguageDelegate.cpp \
    src/ui/delegates/LicenseDelegate.cpp \
    src/ui/delegates/MainSettingDelegate.cpp \
    src/ui/delegates/MediaPlayerSettingDelegate.cpp \
    src/ui/delegates/RadioDelegate.cpp \
    src/ui/delegates/RemoteFileUtilDelegate.cpp \
    src/ui/delegates/SPDIFDelegate.cpp \
    src/ui/delegates/SocketDelegate.cpp \
    src/ui/delegates/SplashScreenDelegate.cpp \
    src/ui/delegates/StatusBarDelegate.cpp \
    src/ui/delegates/TextCodecDelegate.cpp \
    src/ui/delegates/TimeUtilDelegate.cpp \
    src/ui/delegates/ToastDelegate.cpp \
    src/ui/delegates/UpdaterDelegate.cpp \
    src/ui/delegates/VersionDelegate.cpp \
    src/utils/AnyVODEnumsUtils.cpp \
    src/video/GLRenderer.cpp \
    src/ui/RenderScreen.cpp \
    src/ui/StatusUpdater.cpp \
    src/sensor/RotationSensor.cpp

android-*: SOURCES += \
    src/core/AndroidNative.cpp \
    src/decoders/MediaCodec.cpp \
    src/screensaver/AndroidPreventer.cpp \
    src/audio/SPDIFAudioTrack.cpp

ios: SOURCES += \
    ../AnyVODClient/src/decoders/VideoToolBoxDecoder.cpp \
    src/screensaver/IOSPreventer.cpp \

linux-rasp-pi3-* | linux-g++: SOURCES += \
    ../AnyVODClient/src/audio/SPDIFAlsa.cpp \
    ../AnyVODClient/src/device/DTVLinuxDVB.cpp \
    ../AnyVODClient/src/device/radio/RadioGNURadio.cpp \
    ../AnyVODClient/src/device/radio/gnuradio/RadioReceiverInterface.cpp \
    ../AnyVODClient/src/device/radio/gnuradio/RadioReceiverWideBandFM.cpp \
    ../AnyVODClient/src/device/radio/gnuradio/blocks/RadioAudioSink.cpp \
    ../AnyVODClient/src/device/radio/gnuradio/blocks/RadioBandPassFilter.cpp \
    ../AnyVODClient/src/device/radio/gnuradio/blocks/RadioComplexResampler.cpp \
    ../AnyVODClient/src/device/radio/gnuradio/blocks/RadioDownConverter.cpp \
    ../AnyVODClient/src/device/radio/gnuradio/blocks/RadioFIRDecimator.cpp \
    ../AnyVODClient/src/device/radio/gnuradio/blocks/RadioFMDeemphasis.cpp \
    ../AnyVODClient/src/device/radio/gnuradio/blocks/RadioFMDemodulator.cpp \
    ../AnyVODClient/src/device/radio/gnuradio/blocks/RadioFloatPointResampler.cpp \
    ../AnyVODClient/src/device/radio/gnuradio/blocks/RadioLowPassFilter.cpp \
    ../AnyVODClient/src/device/radio/gnuradio/blocks/RadioResamplerBase.cpp \
    ../AnyVODClient/src/device/radio/gnuradio/blocks/RadioStereoDemodulator.cpp \
    ../AnyVODClient/src/screensaver/LinuxPreventer.cpp \
    ../AnyVODClient/src/utils/DeviceUtils.cpp \
    src/screensaver/RaspberryPiPreventer.cpp \
    src/screensaver/XDGScreenSaverDisabler.cpp

OBJECTIVE_SOURCES += \
    ../AnyVODClient/src/utils/MacUtils.mm \
    src/core/IOSDelegate.mm \
    src/core/IOSViewController.mm \
    src/audio/SPDIFAudioUnit.mm

TRANSLATIONS += \
    languages/anyvod_en.ts \
    languages/anyvod_ko.ts

android-*: DISTFILES += \
    resources/android/gradle/wrapper/gradle-wrapper.jar \
    resources/android/gradlew \
    resources/android/gradle.properties \
    resources/android/res/values/libs.xml \
    resources/android/res/values/theme.xml \
    resources/android/res/values/bool.xml \
    resources/android/res/values-sw600dp/bool.xml \
    resources/android/res/drawable/splash.xml \
    resources/android/build.gradle \
    resources/android/gradle/wrapper/gradle-wrapper.properties \
    resources/android/gradlew.bat

ios: DISTFILES += \
    resources/ios/ios.plist \
    resources/ios/splash/AnyVODLaunchScreen.xib

DISTFILES += \
    languages/anyvod_en.ts \
    languages/anyvod_ko.ts

linux-rasp-pi3-*: INCLUDEPATH += /usr/include/dbus-1.0 /usr/lib/arm-linux-gnueabihf/dbus-1.0/include
linux-rasp-pi3-*: DEPENDPATH += /usr/include/dbus-1.0 /usr/lib/arm-linux-gnueabihf/dbus-1.0/include

linux-g++: INCLUDEPATH += /usr/include/dbus-1.0 /usr/lib/x86_64-linux-gnu/dbus-1.0/include
linux-g++: DEPENDPATH += /usr/include/dbus-1.0 /usr/lib/x86_64-linux-gnu/dbus-1.0/include

linux-rasp-pi3-* | linux-g++: DEFINES += Q_OS_RASPBERRY_PI

INCLUDEPATH += $$PWD/../AnyVODClient/src \
               $$PWD/src \
               $$PWD/include \
               $$PWD/include/ffmpeg

DEFINES += QT_DEPRECATED_WARNINGS __STDC_CONSTANT_MACROS Q_OS_MOBILE

LIBS += -lavcodec -lavformat -lswresample -lavutil -lavfilter -lswscale -lbass -lbassmix -lbass_fx -lass

android-*: equals(ANDROID_TARGET_ARCH, arm64-v8a): \
    LIBS += -L$$PWD/resources/android/libs/arm64-v8a
android-*: equals(ANDROID_TARGET_ARCH, armeabi-v7a): \
    LIBS += -L$$PWD/resources/android/libs/armeabi-v7a
android-*: equals(ANDROID_TARGET_ARCH, x86): \
    LIBS += -L$$PWD/resources/android/libs/x86

QMAKE_OBJECTIVE_CFLAGS += -fobjc-arc

ios: LIBS += -framework CoreVideo -framework CoreMedia -framework VideoToolBox -framework AudioToolbox \
             -framework MediaPlayer -framework AVFoundation -framework SystemConfiguration -framework CFNetwork \
             -framework Accelerate
ios: LIBS += -lbz2 -liconv -lfribidi -lharfbuzz -lfreetype -lspeex -lilbc -ldav1d -lz
ios: LIBS += -L$$PWD/libs/ios

linux-rasp-pi3-* | linux-g++: LIBS += -ldl -lX11 -lz -lgomp -lasound -ldvbpsi -ldbus-1 -llog4cpp \
                                      -lgnuradio-runtime -lgnuradio-pmt -lgnuradio-osmosdr -lgnuradio-filter -lgnuradio-blocks -lgnuradio-analog -lgnuradio-digital
linux-rasp-pi3-*: LIBS += -L$$PWD/libs/raspi
linux-g++: LIBS += -L$$PWD/../AnyVODClient/libs/linux

linux-g++: QMAKE_LFLAGS += -fuse-ld=gold

android-*: QMAKE_CXXFLAGS += -fopenmp
android-*: QMAKE_CFLAGS += -fopenmp
android-*: QMAKE_LFLAGS += -fopenmp -static-openmp
android-*: equals(ANDROID_TARGET_ARCH, armeabi-v7a): \
    QMAKE_CXXFLAGS += -mtune=cortex-a8
android-*: equals(ANDROID_TARGET_ARCH, armeabi-v7a): \
    QMAKE_CFLAGS += -mtune=cortex-a8
android-*: equals(ANDROID_TARGET_ARCH, armeabi-v7a): \
    QMAKE_LFLAGS += -mtune=cortex-a8

linux-rasp-pi3-* | linux-g++: QMAKE_CXXFLAGS += -fopenmp -Wno-psabi
linux-rasp-pi3-* | linux-g++: QMAKE_CFLAGS += -fopenmp -Wno-psabi

linux-rasp-pi3-*: QMAKE_CXXFLAGS += -faligned-new
linux-rasp-pi3-*: QMAKE_CFLAGS += -faligned-new

contains(QMAKE_HOST.os, Windows): {
    android-*: equals(ANDROID_TARGET_ARCH, arm64-v8a): \
        QMAKE_POST_LINK += ..\AnyVODMobileClient\scripts\android\postlink.bat
    android-*: equals(ANDROID_TARGET_ARCH, armeabi-v7a): \
        QMAKE_POST_LINK += ..\AnyVODMobileClient\scripts\android\postlink.bat
    android-*: equals(ANDROID_TARGET_ARCH, x86): \
        QMAKE_POST_LINK += ..\AnyVODMobileClient\scripts\android\postlink.bat
}

!contains(QMAKE_HOST.os, Windows): {
    android-*: equals(ANDROID_TARGET_ARCH, arm64-v8a): \
        QMAKE_POST_LINK += ../AnyVODMobileClient/scripts/android/postlink.sh
    android-*: equals(ANDROID_TARGET_ARCH, armeabi-v7a): \
        QMAKE_POST_LINK += ../AnyVODMobileClient/scripts/android/postlink.sh
    android-*: equals(ANDROID_TARGET_ARCH, x86): \
        QMAKE_POST_LINK += ../AnyVODMobileClient/scripts/android/postlink.sh
}

ios: QMAKE_POST_LINK += ../AnyVODMobileClient/scripts/ios/postlink.sh
ios: QMAKE_INFO_PLIST = resources/ios/ios.plist

linux-rasp-pi3-*: QMAKE_POST_LINK += ../AnyVODMobileClient/scripts/raspi/postlink.sh

# Installation path
# target.path =

# Please do not modify the following two lines. Required for deployment.
include(qtquick2applicationviewer/qtquick2applicationviewer.pri)
qtcAddDeployment()

ios {
    QMAKE_IOS_DEPLOYMENT_TARGET = 12.0

    !exists(ios.pri): {
        FIRST_IOS_TEMPLATE = "MY_DEVELOPMENT_TEAM.value = FILL_TEAMID"
        write_file(ios.pri, FIRST_IOS_TEMPLATE)
    }

    include(ios.pri)

    infile(ios.pri, MY_DEVELOPMENT_TEAM.value, FILL_TEAMID) {
        message(You can set MY_DEVELOPMENT_TEAM.value to your own value. Refers to the DevelopmentTeam in the project.pbxproj file.)
    }

    MY_DEVELOPMENT_TEAM.name = DEVELOPMENT_TEAM
    QMAKE_MAC_XCODE_SETTINGS += MY_DEVELOPMENT_TEAM
}

android-*: ANDROID_PACKAGE_SOURCE_DIR = $$PWD/resources/android

android-*: OTHER_FILES += \
    resources/android/AndroidManifest.xml \
    resources/android/src/com/dcple/anyvod/Utils.java \
    resources/android/src/com/dcple/anyvod/AnyVODActivity.java \
    resources/android/src/com/dcple/anyvod/NativeFunctions.java \
    resources/android/src/com/dcple/anyvod/RemoteControlReceiver.java \
    resources/android/src/com/dcple/anyvod/HeadsetHookHandler.java \
    resources/android/src/com/dcple/anyvod/HeadsetPlugReceiver.java \
    resources/android/src/com/dcple/anyvod/PreventKillService.java

RESOURCES += \
    qml/qml_resources.qrc
