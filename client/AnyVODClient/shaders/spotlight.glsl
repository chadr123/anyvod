﻿// entry spotlight

void spotlight(inout vec4 texel, inout vec2 coord)
{
  float PI = acos(-1.0);
  vec3 lightsrc = vec3(sin(AnyVOD_Clock * PI / 1.5) / 2.4 + 0.17, cos(AnyVOD_Clock * PI) / 4.5 + 0.14, 1.0);
  vec3 light = normalize(lightsrc - vec3(coord.s, coord.t, 0.0));

  texel *= pow(dot(light, vec3(0.0, 0.0 ,1.0)), 50.0);
  texel.a = 1.0;
}
