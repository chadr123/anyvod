﻿// entry nightvision

void nightvision(inout vec4 texel, inout vec2 coord)
{
  float c = dot(texel, vec4(0.2, 0.6, 0.1, 0.1));

  texel = vec4(0.0, c, 0.0, 1.0);
}
