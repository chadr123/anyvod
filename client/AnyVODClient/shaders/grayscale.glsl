﻿// entry grayscale

void grayscale(inout vec4 texel, inout vec2 coord)
{
  float c = dot(texel, vec4(0.299, 0.587, 0.114, 0.0));

  texel = vec4(c, c, c, 1.0);
}
