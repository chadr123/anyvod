#!/bin/bash

. ../../../../installer/common/functions.sh

version=`cat version`
qt_path=`./qt_path.sh`

if [ ! -d ../../../../package ]; then
  mkdir ../../../../package
  check
fi

if [ -d ../../../../package/client ]; then
  rm -rf ../../../../package/client
  check
fi

if [ -d ../../../../package/licenses ]; then
  rm -rf ../../../../package/licenses
  check
fi

mkdir ../../../../package/client
check

mkdir ../../../../package/client/imageformats
check

mkdir ../../../../package/client/platforms
check

mkdir ../../../../package/client/xcbglintegrations
check

mkdir ../../../../package/licenses
check

cp ../../../../licenses/* ../../../../package/licenses
check

cp -a ../../libs/linux/*.so ../../../../package/client
check

cp -a ../../libs/linux/*.so.* ../../../../package/client
check

cp ../../equalizer_template.ini ../../../../package/client/equalizer.ini
check

cp ../../settings_template.ini ../../../../package/client/settings.ini
check

mkdir ../../../../package/client/fonts
check

cp ../../fonts/* ../../../../package/client/fonts
check

mkdir ../../../../package/client/skins
check

cp ../../skins/* ../../../../package/client/skins
check

mkdir ../../../../package/client/shaders
check

cp ../../shaders/* ../../../../package/client/shaders
check

cp -a AnyVODClient.sh ../../../../package/client
check

cd ../../../../package/client
check

cd -
check

cp -a ../../../AnyVODClient-build-desktop/AnyVODClient ../../../../package/client/AnyVODClient_bin
check

mkdir ../../../../package/client/languages
check

cp $HOME/$qt_path/$version/gcc_64/translations/qt_zh_??.qm ../../../../package/client/languages
check

cp $HOME/$qt_path/$version/gcc_64/translations/qt_??.qm ../../../../package/client/languages
check

cp $HOME/$qt_path/$version/gcc_64/translations/qtbase_??.qm ../../../../package/client/languages
check

cp ../../languages/*.qm ../../../../package/client/languages
check

cp -a $HOME/$qt_path/$version/gcc_64/lib/libQt?OpenGL.so* ../../../../package/client
check

cp -a $HOME/$qt_path/$version/gcc_64/lib/libQt?Widgets.so* ../../../../package/client
check

cp -a $HOME/$qt_path/$version/gcc_64/lib/libQt?Xml.so* ../../../../package/client
check

cp -a $HOME/$qt_path/$version/gcc_64/lib/libQt?Network.so* ../../../../package/client
check

cp -a $HOME/$qt_path/$version/gcc_64/lib/libQt?Gui.so* ../../../../package/client
check

cp -a $HOME/$qt_path/$version/gcc_64/lib/libQt?Core.so* ../../../../package/client
check

cp -a $HOME/$qt_path/$version/gcc_64/lib/libQt?DBus.so* ../../../../package/client
check

cp -a $HOME/$qt_path/$version/gcc_64/lib/libQt?Qml.so* ../../../../package/client
check

cp -a $HOME/$qt_path/$version/gcc_64/lib/libQt?XcbQpa.so* ../../../../package/client
check

cp -a $HOME/$qt_path/$version/gcc_64/lib/libicui18n.so* ../../../../package/client
check

cp -a $HOME/$qt_path/$version/gcc_64/lib/libicuuc.so* ../../../../package/client
check

cp -a $HOME/$qt_path/$version/gcc_64/lib/libicudata.so* ../../../../package/client
check

cp -a $HOME/$qt_path/$version/gcc_64/plugins/imageformats/libqjpeg.so ../../../../package/client/imageformats
check

cp -a $HOME/$qt_path/$version/gcc_64/plugins/imageformats/libqtiff.so ../../../../package/client/imageformats
check

cp -a $HOME/$qt_path/$version/gcc_64/plugins/platforms/libqxcb.so ../../../../package/client/platforms
check

cp -a $HOME/$qt_path/$version/gcc_64/plugins/xcbglintegrations/libqxcb-glx-integration.so ../../../../package/client/xcbglintegrations
check

cp -a $HOME/$qt_path/$version/gcc_64/plugins/platforminputcontexts ../../../../package/client/
check

cp -a $HOME/$qt_path/$version/gcc_64/plugins/platformthemes ../../../../package/client/
check

cp -a $HOME/$qt_path/$version/gcc_64/lib/libicudata.so* ../../../../package/client
check

cp -a $HOME/$qt_path/$version/gcc_64/lib/libicudata.so* ../../../../package/client
check

exit 0
