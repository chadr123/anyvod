#!/bin/bash

. ../../../../installer/common/functions.sh

version=`cat ../linux/version`
qt_path=`../linux/qt_path.sh`
export PATH=$HOME/$qt_path/$version/clang_64/bin:$PATH

cd ../../../
check

if [ -e AnyVODClient-build-desktop ]; then
  rm -rf AnyVODClient-build-desktop
  check
fi

mkdir AnyVODClient-build-desktop
check

cd AnyVODClient-build-desktop
check

make distclean -w

qmake ../AnyVODClient/AnyVODClient.pro -r -spec macx-clang CONFIG+=release CONFIG+=x86_64
check

make -w -j8
check

cd ../AnyVODClient/scripts/mac
check

exit 0
