#!/bin/bash

. ../../installer/common/functions.sh

version=`cat ../AnyVODClient/scripts/linux/version`
qt_path=`../AnyVODClient/scripts/linux/qt_path.sh`
export PATH=$HOME/$qt_path/$version/clang_64/bin:$PATH

if [ ! -f ../AnyVODClient/equalizer.ini ]; then
  cp ../AnyVODClient/equalizer_template.ini ../AnyVODClient/equalizer.ini
  check
fi

if [ ! -f ../AnyVODClient/settings.ini ]; then
  cp ../AnyVODClient/settings_template.ini ../AnyVODClient/settings.ini
  check
fi

lrelease ../AnyVODClient/AnyVODClient.pro
check

exit 0
