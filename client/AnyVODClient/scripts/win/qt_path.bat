call version.bat %1

set qt_path_online_installer=C:\Qt
set qt_path=%qt_path_online_installer%

if "%1"=="x86" (
set mingw_path=mingw%mingw_version%_32
set mingw_tool_path=%qt_path%\Tools\mingw%mingw_tool_version%_32
set openssl_crypto_path=%qt_path%\Tools\OpenSSL\Win_x86\bin\libcrypto-1_1.dll
set openssl_ssl_path=%qt_path%\Tools\OpenSSL\Win_x86\bin\libssl-1_1.dll
) else (
set mingw_path=mingw%mingw_version%_64
set mingw_tool_path=%qt_path%\Tools\mingw%mingw_tool_version%_64
set openssl_crypto_path=%qt_path%\Tools\OpenSSL\Win_x64\bin\libcrypto-1_1-x64.dll
set openssl_ssl_path=%qt_path%\Tools\OpenSSL\Win_x64\bin\libssl-1_1-x64.dll
)
