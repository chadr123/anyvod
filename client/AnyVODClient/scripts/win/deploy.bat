set arch=%1

call qt_path.bat %arch%

if exist "..\..\..\..\package\client\%arch%" (
del ..\..\..\..\package\client\%arch%\*.* /f /q /a-h-s-r
if %errorlevel% neq 0 goto end
)

if exist "..\..\..\..\package\client\%arch%\fonts" (
del ..\..\..\..\package\client\%arch%\fonts\*.* /f /q /a-h-s-r
if %errorlevel% neq 0 goto end
)

if exist "..\..\..\..\package\client\%arch%\languages" (
del ..\..\..\..\package\client\%arch%\languages\*.* /f /q /a-h-s-r
if %errorlevel% neq 0 goto end
)

if exist "..\..\..\..\package\client\%arch%\skins" (
del ..\..\..\..\package\client\%arch%\skins\*.* /f /q /a-h-s-r
if %errorlevel% neq 0 goto end
)

if exist "..\..\..\..\package\client\%arch%\shaders" (
del ..\..\..\..\package\client\%arch%\shaders\*.* /f /q /a-h-s-r
if %errorlevel% neq 0 goto end
)

if exist "..\..\..\..\package\client\%arch%\imageformats" (
del ..\..\..\..\package\client\%arch%\imageformats\*.* /f /q /a-h-s-r
if %errorlevel% neq 0 goto end
)

if exist "..\..\..\..\package\client\%arch%\platforms" (
del ..\..\..\..\package\client\%arch%\platforms\*.* /f /q /a-h-s-r
if %errorlevel% neq 0 goto end
)

if exist "..\..\..\..\package\licenses" (
del ..\..\..\..\package\licenses\*.* /f /q /a-h-s-r
if %errorlevel% neq 0 goto end
)

if not exist "..\..\..\..\package\client\%arch%\imageformats" (
mkdir ..\..\..\..\package\client\%arch%\imageformats
if %errorlevel% neq 0 goto end
)

if not exist "..\..\..\..\package\client\%arch%\platforms" (
mkdir ..\..\..\..\package\client\%arch%\platforms
if %errorlevel% neq 0 goto end
)

xcopy "..\..\libs\win\%arch%\*.dll" "..\..\..\..\package\client\%arch%" /y /i
if %errorlevel% neq 0 goto end

copy "..\..\equalizer_template.ini" "..\..\..\..\package\client\%arch%\equalizer.ini"
if %errorlevel% neq 0 goto end

copy "..\..\settings_template.ini" "..\..\..\..\package\client\%arch%\settings.ini"
if %errorlevel% neq 0 goto end

xcopy "..\..\fonts" "..\..\..\..\package\client\%arch%\fonts" /y /e /i
if %errorlevel% neq 0 goto end

xcopy "..\..\skins" "..\..\..\..\package\client\%arch%\skins" /y /e /i
if %errorlevel% neq 0 goto end

xcopy "..\..\shaders" "..\..\..\..\package\client\%arch%\shaders" /y /e /i
if %errorlevel% neq 0 goto end

xcopy "..\..\..\..\licenses" "..\..\..\..\package\licenses" /y /e /i
if %errorlevel% neq 0 goto end

xcopy "..\..\..\AnyVODClient-build-desktop\release\AnyVODClient.exe" "..\..\..\..\package\client\%arch%" /y
if %errorlevel% neq 0 goto end

if "%1"=="x86" (
xcopy "%mingw_tool_path%\bin\libgcc_s_dw2-1.dll" "..\..\..\..\package\client\%arch%" /y
if %errorlevel% neq 0 goto end
) else (
xcopy "%mingw_tool_path%\bin\libgcc_s_seh-1.dll" "..\..\..\..\package\client\%arch%" /y
if %errorlevel% neq 0 goto end
)

xcopy "%mingw_tool_path%\bin\libwinpthread-1.dll" "..\..\..\..\package\client\%arch%" /y
if %errorlevel% neq 0 goto end

xcopy "%mingw_tool_path%\bin\libstdc++-6.dll" "..\..\..\..\package\client\%arch%" /y
if %errorlevel% neq 0 goto end

xcopy "%mingw_tool_path%\bin\libgomp-1.dll" "..\..\..\..\package\client\%arch%" /y
if %errorlevel% neq 0 goto end

xcopy "%openssl_crypto_path%" "..\..\..\..\package\client\%arch%" /y
if %errorlevel% neq 0 goto end

xcopy "%openssl_ssl_path%" "..\..\..\..\package\client\%arch%" /y
if %errorlevel% neq 0 goto end

xcopy "%qt_path%\%version%\%mingw_path%\bin\Qt?Core.dll" "..\..\..\..\package\client\%arch%" /y
if %errorlevel% neq 0 goto end

xcopy "%qt_path%\%version%\%mingw_path%\bin\Qt?Gui.dll" "..\..\..\..\package\client\%arch%" /y
if %errorlevel% neq 0 goto end

xcopy "%qt_path%\%version%\%mingw_path%\bin\Qt?Widgets.dll" "..\..\..\..\package\client\%arch%" /y
if %errorlevel% neq 0 goto end

xcopy "%qt_path%\%version%\%mingw_path%\bin\Qt?OpenGL.dll" "..\..\..\..\package\client\%arch%" /y
if %errorlevel% neq 0 goto end

xcopy "%qt_path%\%version%\%mingw_path%\bin\Qt?Network.dll" "..\..\..\..\package\client\%arch%" /y
if %errorlevel% neq 0 goto end

xcopy "%qt_path%\%version%\%mingw_path%\bin\Qt?Xml.dll" "..\..\..\..\package\client\%arch%" /y
if %errorlevel% neq 0 goto end

xcopy "%qt_path%\%version%\%mingw_path%\bin\Qt?Qml.dll" "..\..\..\..\package\client\%arch%" /y
if %errorlevel% neq 0 goto end

xcopy "%qt_path%\%version%\%mingw_path%\bin\Qt?WinExtras.dll" "..\..\..\..\package\client\%arch%" /y
if %errorlevel% neq 0 goto end

xcopy "%qt_path%\%version%\%mingw_path%\plugins\imageformats\qjpeg.dll" "..\..\..\..\package\client\%arch%\imageformats" /y
if %errorlevel% neq 0 goto end

xcopy "%qt_path%\%version%\%mingw_path%\plugins\imageformats\qtiff.dll" "..\..\..\..\package\client\%arch%\imageformats" /y
if %errorlevel% neq 0 goto end

xcopy "%qt_path%\%version%\%mingw_path%\plugins\platforms\qwindows.dll" "..\..\..\..\package\client\%arch%\platforms" /y
if %errorlevel% neq 0 goto end

xcopy "%qt_path%\%version%\%mingw_path%\plugins\styles" "..\..\..\..\package\client\%arch%\styles" /y /e /i
if %errorlevel% neq 0 goto end

xcopy "%qt_path%\%version%\%mingw_path%\translations\qt_zh_??.qm" "..\..\..\..\package\client\%arch%\languages" /y /e /i
xcopy "%qt_path%\%version%\%mingw_path%\translations\qt_??.qm" "..\..\..\..\package\client\%arch%\languages" /y /e /i
xcopy "%qt_path%\%version%\%mingw_path%\translations\qtbase_??.qm" "..\..\..\..\package\client\%arch%\languages" /y /e /i
if %errorlevel% neq 0 goto end

xcopy "..\..\languages\*.qm" "..\..\..\..\package\client\%arch%\languages" /y /e /i
if %errorlevel% neq 0 goto end

:end
