﻿<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>CustomShortcut</name>
    <message>
        <location filename="../forms/customshortcut.ui" line="14"/>
        <source>단축 키 설정</source>
        <translation>Shortcut Key Settings</translation>
    </message>
    <message>
        <location filename="../forms/customshortcut.ui" line="105"/>
        <source>메인</source>
        <translation>Main</translation>
    </message>
    <message>
        <location filename="../forms/customshortcut.ui" line="134"/>
        <source>DTV</source>
        <translation>DTV</translation>
    </message>
    <message>
        <location filename="../forms/customshortcut.ui" line="163"/>
        <source>라디오</source>
        <translation>Radio</translation>
    </message>
    <message>
        <location filename="../forms/customshortcut.ui" line="192"/>
        <source>화면</source>
        <translation>Screen</translation>
    </message>
    <message>
        <location filename="../forms/customshortcut.ui" line="221"/>
        <source>재생</source>
        <translation>Playback</translation>
    </message>
    <message>
        <location filename="../forms/customshortcut.ui" line="250"/>
        <source>자막 / 가사</source>
        <translation>Subtitle / Lyrics</translation>
    </message>
    <message>
        <location filename="../forms/customshortcut.ui" line="279"/>
        <source>소리</source>
        <translation>Sound</translation>
    </message>
    <message>
        <location filename="../forms/customshortcut.ui" line="31"/>
        <source>초기화</source>
        <translation>Reset</translation>
    </message>
    <message>
        <location filename="../forms/customshortcut.ui" line="57"/>
        <source>확인</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../forms/customshortcut.ui" line="73"/>
        <source>취소</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../src/ui/CustomShortcut.cpp" line="66"/>
        <source>&quot;%1&quot;와 중복 되는 단축키 입니다</source>
        <translation>Shortcut key was duplicated with &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../src/ui/CustomShortcut.cpp" line="423"/>
        <source>단축키를 초기화 하시겠습니까?</source>
        <translation>Do you want to reset shortcut keys?</translation>
    </message>
</context>
<context>
    <name>EnumsTranslator</name>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="56"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="150"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="193"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="200"/>
        <source>사용 안 함</source>
        <translation>Disable</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="57"/>
        <source>왼쪽 영상 사용</source>
        <translation>Use Left Image</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="58"/>
        <source>오른쪽 영상 사용</source>
        <translation>Use Right Image</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="59"/>
        <source>상단 영상 사용</source>
        <translation>Use Top Image</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="60"/>
        <source>하단 영상 사용</source>
        <translation>Use Bottom Image</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="61"/>
        <source>좌우 영상 사용</source>
        <translation>Use Side by Side Image</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="62"/>
        <source>상하 영상 사용</source>
        <translation>Use Top and Bottom Image</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="63"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="67"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="71"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="75"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="79"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="83"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="87"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="91"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="95"/>
        <source>왼쪽 영상 우선 사용</source>
        <translation>Left Image Priority</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="64"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="68"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="72"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="76"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="80"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="84"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="88"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="92"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="96"/>
        <source>오른쪽 영상 우선 사용</source>
        <translation>Right Image Priority</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="65"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="69"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="73"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="77"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="81"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="85"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="89"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="93"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="97"/>
        <source>상단 영상 우선 사용</source>
        <translation>Top Image Priority</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="66"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="70"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="74"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="78"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="82"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="86"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="90"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="94"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="98"/>
        <source>하단 영상 우선 사용</source>
        <translation>Bottom Image Priority</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="103"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="104"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="105"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="106"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="107"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="108"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="109"/>
        <source>일반</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="110"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="111"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="112"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="113"/>
        <source>Page Flipping</source>
        <translation>Page Flipping</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="114"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="115"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="116"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="117"/>
        <source>Row Interlaced</source>
        <translation>Row Interlaced</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="118"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="119"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="120"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="121"/>
        <source>Column Interlaced</source>
        <translation>Column Interlaced</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="122"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="123"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="124"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="125"/>
        <source>Red-Cyan Anaglyph</source>
        <translation>Red-Cyan Anaglyph</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="126"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="127"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="128"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="129"/>
        <source>Green-Magenta Anaglyph</source>
        <translation>Green-Magenta Anaglyph</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="130"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="131"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="132"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="133"/>
        <source>Yellow-Blue Anaglyph</source>
        <translation>Yellow-Blue Anaglyph</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="134"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="135"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="136"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="137"/>
        <source>Red-Blue Anaglyph</source>
        <translation>Red-Blue Anaglyph</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="138"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="139"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="140"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="141"/>
        <source>Red-Green Anaglyph</source>
        <translation>Red-Green Anaglyph</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="142"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="143"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="144"/>
        <location filename="../src/core/EnumsTranslator.cpp" line="145"/>
        <source>Checker Board</source>
        <translation>Checker Board</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="151"/>
        <source>상/하</source>
        <translation>Top and Bottom</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="152"/>
        <source>좌/우</source>
        <translation>Side By Side</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="153"/>
        <source>페이지 플리핑</source>
        <translation>Page Flipping</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="154"/>
        <source>인터레이스</source>
        <translation>Interlaced</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="155"/>
        <source>체커 보드</source>
        <translation>Checker Board</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="156"/>
        <source>애너글리프</source>
        <translation>Anaglyph</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="182"/>
        <source>확인하지 않음</source>
        <translation>Don&apos;t Check</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="183"/>
        <source>매 실행 시</source>
        <translation>Every Launch</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="184"/>
        <source>1일</source>
        <translation>Every Day</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="185"/>
        <source>1주일</source>
        <translation>Every Week</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="186"/>
        <source>한 달</source>
        <translation>Every Month</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="187"/>
        <source>반 년</source>
        <translation>Every Half of Year</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="188"/>
        <source>1년</source>
        <translation>Every Half of Year</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="194"/>
        <source>VR 평면 영상</source>
        <translation>Normal VR Mode</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="195"/>
        <source>360도 영상</source>
        <translation>360° Video Mode</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="201"/>
        <source>좌우 영상(좌측 영상 우선)</source>
        <translation>Side by Side(Left Image Priority)</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="202"/>
        <source>좌우 영상(우측 영상 우선)</source>
        <translation>Side by Side(Right Image Priority)</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="203"/>
        <source>상하 영상(상단 영상 우선)</source>
        <translation>Top and Bottom(Top Image Priority)</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="204"/>
        <source>상하 영상(하단 영상 우선)</source>
        <translation>Top and Bottom(Bottom Image Priority)</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="205"/>
        <source>복제</source>
        <translation>Copy</translation>
    </message>
    <message>
        <location filename="../src/core/EnumsTranslator.cpp" line="206"/>
        <source>가상 3D</source>
        <translation>Virtual 3D</translation>
    </message>
</context>
<context>
    <name>Equalizer</name>
    <message>
        <location filename="../forms/equalizer.ui" line="19"/>
        <source>이퀄라이저</source>
        <translation>Equalizer</translation>
    </message>
    <message>
        <location filename="../forms/equalizer.ui" line="33"/>
        <location filename="../src/ui/Equalizer.cpp" line="269"/>
        <source>프리셋 추가</source>
        <translation>Add Preset</translation>
    </message>
    <message>
        <location filename="../forms/equalizer.ui" line="49"/>
        <source>프리셋 삭제</source>
        <translation>Delete Preset</translation>
    </message>
    <message>
        <location filename="../forms/equalizer.ui" line="65"/>
        <source>프리셋 저장</source>
        <translation>Save Preset</translation>
    </message>
    <message>
        <location filename="../forms/equalizer.ui" line="97"/>
        <source>초기화</source>
        <translation>Reset Preset</translation>
    </message>
    <message>
        <location filename="../forms/equalizer.ui" line="113"/>
        <source>프리셋</source>
        <translation>Presets</translation>
    </message>
    <message>
        <location filename="../forms/equalizer.ui" line="2085"/>
        <source>이퀄라이저 사용</source>
        <translation>Use Equalizer</translation>
    </message>
    <message>
        <location filename="../forms/equalizer.ui" line="2111"/>
        <source>확인</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../forms/equalizer.ui" line="2127"/>
        <source>취소</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="99"/>
        <source>기본값</source>
        <translation>Default</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="100"/>
        <source>클래식</source>
        <translation>Classical</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="101"/>
        <source>베이스</source>
        <translation>Bass</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="102"/>
        <source>베이스 &amp; 트레블</source>
        <translation>Bass &amp; Treble</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="103"/>
        <source>트레블</source>
        <translation>Treble</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="104"/>
        <source>헤드폰</source>
        <translation>Headset</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="105"/>
        <source>홀</source>
        <translation>Hall</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="106"/>
        <source>소프트 락</source>
        <translation>Soft Rock</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="107"/>
        <source>클럽</source>
        <translation>Club</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="108"/>
        <source>댄스</source>
        <translation>Dance</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="109"/>
        <source>라이브</source>
        <translation>Live</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="110"/>
        <source>파티</source>
        <translation>Party</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="111"/>
        <source>팝</source>
        <translation>Pop</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="112"/>
        <source>레게</source>
        <translation>Reggae</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="113"/>
        <source>락</source>
        <translation>Rock</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="114"/>
        <source>스카</source>
        <translation>Ska</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="115"/>
        <source>소프트</source>
        <translation>Soft</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="116"/>
        <source>테크노</source>
        <translation>Techno</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="117"/>
        <source>보컬</source>
        <translation>Vocal</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="118"/>
        <source>재즈</source>
        <translation>Jazz</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="269"/>
        <source>프리셋 이름 :</source>
        <translation>Preset name :</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="369"/>
        <source>프리셋이 없습니다</source>
        <translation>There were no presets</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="452"/>
        <source>삭제 하시겠습니까?</source>
        <translation>Do you want to delete a preset?</translation>
    </message>
</context>
<context>
    <name>FileAssociation</name>
    <message>
        <location filename="../forms/fileassociation.ui" line="14"/>
        <source>확장자 연결</source>
        <translation>File Associations</translation>
    </message>
    <message>
        <location filename="../forms/fileassociation.ui" line="22"/>
        <source>비디오</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../forms/fileassociation.ui" line="29"/>
        <source>오디오</source>
        <translation>Audio</translation>
    </message>
    <message>
        <location filename="../forms/fileassociation.ui" line="42"/>
        <source>자막</source>
        <translation>Subtitle</translation>
    </message>
    <message>
        <location filename="../forms/fileassociation.ui" line="58"/>
        <location filename="../forms/fileassociation.ui" line="71"/>
        <location filename="../forms/fileassociation.ui" line="84"/>
        <location filename="../forms/fileassociation.ui" line="100"/>
        <source>기본 값</source>
        <translation>Set to Default</translation>
    </message>
    <message>
        <location filename="../forms/fileassociation.ui" line="107"/>
        <source>재생 목록</source>
        <translation>Playback List</translation>
    </message>
    <message>
        <location filename="../forms/fileassociation.ui" line="156"/>
        <source>적용 하기</source>
        <translation>Apply</translation>
    </message>
    <message>
        <location filename="../forms/fileassociation.ui" line="169"/>
        <source>닫기</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="../src/ui/FileAssociation.cpp" line="115"/>
        <source>관리자 권한으로 실행하지 않았을 경우 정상적으로 적용 되지 않을 수 있습니다</source>
        <translation>If AnyVOD was not excuted as administrator, file associations may be not applied completely</translation>
    </message>
</context>
<context>
    <name>FileAssociationInstaller</name>
    <message>
        <location filename="../src/core/FileAssociationInstaller.cpp" line="58"/>
        <source>%1로 열기</source>
        <translation>Open with %1</translation>
    </message>
</context>
<context>
    <name>Login</name>
    <message>
        <location filename="../forms/login.ui" line="31"/>
        <location filename="../forms/login.ui" line="136"/>
        <source>로그인</source>
        <translation>Login</translation>
    </message>
    <message>
        <location filename="../forms/login.ui" line="46"/>
        <source>아이디와 비밀번호를 입력해주세요</source>
        <translation>Input ID and Password</translation>
    </message>
    <message>
        <location filename="../forms/login.ui" line="70"/>
        <source>아이디</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../forms/login.ui" line="80"/>
        <source>비밀번호</source>
        <translation>Password</translation>
    </message>
    <message>
        <location filename="../forms/login.ui" line="165"/>
        <source>취소</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../src/ui/Login.cpp" line="70"/>
        <source>아이디를 입력해 주세요</source>
        <translation>Please input the ID</translation>
    </message>
    <message>
        <location filename="../src/ui/Login.cpp" line="74"/>
        <source>비밀번호를 입력해 주세요</source>
        <translation>Please input the password</translation>
    </message>
    <message>
        <location filename="../src/ui/Login.cpp" line="83"/>
        <source>서버에 접속 할 수 없습니다</source>
        <translation>Can not connect to the server</translation>
    </message>
    <message>
        <location filename="../src/ui/Login.cpp" line="99"/>
        <source>스트림에 접속 할 수 없습니다</source>
        <translation>Can not connect with stream</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../forms/mainwindow.ui" line="239"/>
        <source>재생 위치</source>
        <translation>Playback Position</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="353"/>
        <source>음소거 켜기 / 끄기</source>
        <translation>Mute On / Off</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="386"/>
        <source>소리</source>
        <translation>Sound</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="495"/>
        <source>가사</source>
        <translation>Lyrics</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="620"/>
        <location filename="../src/ui/MainWindow.cpp" line="209"/>
        <source>재생</source>
        <translation>Play</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="643"/>
        <location filename="../src/ui/MainWindow.cpp" line="216"/>
        <source>일시정지</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="672"/>
        <location filename="../src/ui/MainWindow.cpp" line="225"/>
        <source>정지</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="701"/>
        <source>5초 뒤로</source>
        <translation>5 Seconds Backward</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="730"/>
        <source>5초 앞으로</source>
        <translation>5 Seconds Forward</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="759"/>
        <source>열기</source>
        <translation>Open</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="807"/>
        <source>스펙트럼</source>
        <translation>Spectrum</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="963"/>
        <location filename="../src/ui/MainWindow.cpp" line="1575"/>
        <source>버퍼링 중입니다</source>
        <translation>Now Buffering</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="989"/>
        <location filename="../src/ui/MainWindow.cpp" line="226"/>
        <source>이전 파일</source>
        <translation>Previous File</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="1012"/>
        <location filename="../src/ui/MainWindow.cpp" line="227"/>
        <source>다음 파일</source>
        <translation>Next File</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="1043"/>
        <source>투명도</source>
        <translation>Transparency</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="1740"/>
        <source>스킨을 읽을 수 없습니다 (%1)</source>
        <translation>Can not load skin (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="2751"/>
        <source>전체 화면</source>
        <translation>Full Screen</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="2858"/>
        <source>재생 목록에 추가</source>
        <translation>Add to Playback List</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="553"/>
        <source>자막 폰트 가져오기</source>
        <translation>Import Subtitle Fonts</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="278"/>
        <source>탐색 중입니다</source>
        <translation>Seeking</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="546"/>
        <source>관리자 권한으로 실행되지 않을 경우 정상적으로 가져오기가 안될 수 있습니다.
만약에 관리자 권한이 아닐 경우 아래 경로에 폰트를 수동으로 복사 하세요.
계속 하시겠습니까?</source>
        <translation>If you did not execute as administrator, importing will be failed.
Then you should copy fonts to below path manually.
Do you want to go ahead?</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="570"/>
        <source>복사가 완료 되었습니다.</source>
        <translation>Completed.</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="572"/>
        <source>일부 파일이 복사되지 않았습니다.</source>
        <translation>Not completed.</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="739"/>
        <source>라디오 수신 장치가 없습니다.</source>
        <translation>Can&apos;t detect the radio device.</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="806"/>
        <source>설정을 저장하지 못했습니다.</source>
        <translation>Can not save settings.</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="819"/>
        <source>설정을 불러오지 못했습니다. 관리자 권한으로 실행 후 재 시도 해주세요.</source>
        <translation>Can not load settings. Please try again with administrator privileges.</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="843"/>
        <source>시디롬 (sr*);;모든 파일 (*.*)</source>
        <translation>CD-ROM (sr*);;All Files (*.*)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="848"/>
        <source>기본 장치</source>
        <translation>Default Device</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="863"/>
        <source>오디오 시디(%1)</source>
        <translation>Audio CD(%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="1119"/>
        <source>AnyVOD 업데이트가 존재합니다.
업데이트를 다운로드 하시겠습니까?</source>
        <translation>There is an AnyVOD update.
Do you want to download the update?</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="1364"/>
        <source>가사가 없습니다</source>
        <translation>There was no lyrics</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="1651"/>
        <source>다음 에러로 중지 되었습니다 : %1</source>
        <translation>Due to following error, aborted : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="1815"/>
        <source>모노</source>
        <translation>Mono</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="1817"/>
        <source>스테레오</source>
        <translation>Stereo</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="1819"/>
        <source>서라운드</source>
        <translation>Surround</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="1878"/>
        <source>외부 서버에 자막이 존재합니다.
이동하시려면 여기를 클릭하세요.</source>
        <translation>Subtitle exists on external server.
If you want to download the subtitle, click this.</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="1922"/>
        <location filename="../src/ui/MainWindow.cpp" line="1942"/>
        <source>파일을 열 수 없습니다</source>
        <translation>Can not open file</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="2098"/>
        <source>재생 목록에서 삭제 하시겠습니까?</source>
        <translation>Do you want to delete from playback list?</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="2312"/>
        <source>화면 크기 %1배</source>
        <translation>%1 Times Screen Size</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="2512"/>
        <source>투명도 (%1%)</source>
        <translation>Transparency (%1%)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="2598"/>
        <source>항상 위 켜짐</source>
        <translation>Always On Top On</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="2600"/>
        <source>항상 위 꺼짐</source>
        <translation>Always On Top Off</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="2607"/>
        <source>지원하는 모든 파일 (*.%1);;동영상 (*.%2);;음악 (*.%3);;자막 / 가사 (*.%4);;재생 목록 (*.%5);;모든 파일 (*.*)</source>
        <translation>All supported files (*.%1);;Movies (*.%2);;Musics (*.%3);;Subtitles / Lyrics (*.%4);;Playback List (*.%5);;All files (*.*)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="2853"/>
        <source>지원하는 모든 파일 (*.%1);;동영상 (*.%2);;음악 (*.%3);;재생 목록 (*.%4);;모든 파일 (*.*)</source>
        <translation>All supported files (*.%1);;Movies (*.%2);;Musics (*.%3);;Playback List (*.%4);;All files (*.*)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="552"/>
        <source>폰트 (*.%1);;모든 파일 (*.*)</source>
        <translation>Fonts (*.%1);;All Files (*.*)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="768"/>
        <source>DTV 수신 장치가 없습니다.</source>
        <translation>Can&apos;t detect the DTV device.</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="800"/>
        <location filename="../src/ui/MainWindow.cpp" line="811"/>
        <source>설정 (*.%1);;모든 파일 (*.*)</source>
        <translation>Settings (*.%1);;All Files (*.*)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="817"/>
        <source>설정을 불러왔습니다. 적용하려면 AnyVOD를 재 시작 해주세요.</source>
        <translation>Your settings have been loaded. Restart AnyVOD to take effect.</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="2737"/>
        <source>일반 화면</source>
        <translation>Normal Screen</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="2802"/>
        <source>컨트롤바 보임</source>
        <translation>Show ControlBar</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="2804"/>
        <source>컨트롤바 숨김</source>
        <translation>Hide ControlBar</translation>
    </message>
</context>
<context>
    <name>MediaPlayer</name>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="156"/>
        <source>자막 열기 : %1</source>
        <translation>Subtitle was loaded : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="158"/>
        <source>가사 열기 : %1</source>
        <translation>Lyrics was loaded : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="173"/>
        <source>자막이 저장 되었습니다 : %1</source>
        <translation>Subtitle was saved : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="175"/>
        <source>가사가 저장 되었습니다 : %1</source>
        <translation>Lyrics was saved : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="180"/>
        <source>자막이 저장 되지 않았습니다 : %1</source>
        <translation>Subtitle was not saved : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="182"/>
        <source>가사가 저장 되지 않았습니다 : %1</source>
        <translation>Lyrics was not saved : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="200"/>
        <source>외부 자막 닫기</source>
        <translation>External subtitle was closed</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="202"/>
        <source>외부 가사 닫기</source>
        <translation>Externel lyrics was closed</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="407"/>
        <source>영상 변경 (%1)</source>
        <translation>Video stream was changed (%1)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="477"/>
        <source>자막 보이기</source>
        <translation>Show Subtitle</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="479"/>
        <source>자막 숨기기</source>
        <translation>Hide Subtitle</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="484"/>
        <source>가사 보이기</source>
        <translation>Show Lyrics</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="486"/>
        <source>가사 숨기기</source>
        <translation>Hide Lyrics</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="506"/>
        <source>고급 자막 검색 사용</source>
        <translation>Advanced subtitle searching was activated</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="508"/>
        <source>고급 자막 검색 사용 안 함</source>
        <translation>Advanced subtitle searching was deactivated</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="513"/>
        <source>고급 가사 검색 사용</source>
        <translation>Advanced lyrics searching was activated</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="515"/>
        <source>고급 가사 검색 사용 안 함</source>
        <translation>Advanced lyrics searching was deactivated</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="566"/>
        <source>자막 언어 변경</source>
        <translation>Subtitle language was changed</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="568"/>
        <source>가사 언어 변경</source>
        <translation>Lyrics language was changed</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="582"/>
        <source>자막 위치 초기화</source>
        <translation>Subtitle position was reset</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="626"/>
        <source>3D 자막 위치 초기화</source>
        <translation>3D Subtitle position was reset</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="636"/>
        <source>3D 자막 위치 가깝게(세로) : %1</source>
        <translation>Near 3D Subtitle Position(Vertical) : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="638"/>
        <source>3D 자막 위치 멀게(세로) : %1</source>
        <translation>Far 3D Subtitle Position(Vertical) : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="650"/>
        <source>3D 자막 위치 가깝게(가로) : %1</source>
        <translation>Near 3D Subtitle Position(Horizontal) : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="652"/>
        <source>3D 자막 위치 멀게(가로) : %1</source>
        <translation>Far 3D Subtitle Position(Horizontal) : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="674"/>
        <source>구간 반복 시작 : %1</source>
        <translation>Beginning of A-B Repeat : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="687"/>
        <source>구간 반복 끝 : %1</source>
        <translation>Ending of A-B Repeat : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="703"/>
        <source>구간 반복 활성화</source>
        <translation>A-B Repeat On</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="705"/>
        <source>구간 반복 비활성화</source>
        <translation>A-B Repeat Off</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="711"/>
        <source>시작과 끝 시각이 같으므로 활성화 되지 않습니다</source>
        <translation>Because of A-B repeat start and end time is same, A-B repeat could not enable</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="757"/>
        <source>구간 반복 시작 위치가 범위를 벗어났습니다</source>
        <translation>Out of beginning position for A-B repeat</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="762"/>
        <location filename="../src/media/MediaPlayer.cpp" line="796"/>
        <location filename="../src/media/MediaPlayer.cpp" line="835"/>
        <source>구간 반복이 설정 되지 않았습니다</source>
        <translation>A-B repeat range is not set</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="791"/>
        <source>구간 반복 끝 위치가 범위를 벗어났습니다</source>
        <translation>Out of ending position for A-B repeat</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="830"/>
        <source>구간 반복 위치가 범위를 벗어났습니다</source>
        <translation>Out of A-B repeat range</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="846"/>
        <source>키프레임 단위로 이동 함</source>
        <translation>Seek to Keyframe</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="848"/>
        <source>키프레임 단위로 이동 안 함</source>
        <translation>Do not Seek to Keyframe</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="883"/>
        <source>3D 영상 (%1 (%2))</source>
        <translation>3D Video (%1 (%2))</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="898"/>
        <source>3D 자막 (%1)</source>
        <translation>3D Subtitle (%1)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="925"/>
        <source>오프닝 스킵 사용 함</source>
        <translation>Intro Skip On</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="927"/>
        <source>오프닝 스킵 사용 안 함</source>
        <translation>Intro Skip Off</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="949"/>
        <source>엔딩 스킵 사용 함</source>
        <translation>Ending Skip On</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="951"/>
        <source>엔딩 스킵 사용 안 함</source>
        <translation>Ending Skip Off</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="992"/>
        <source>노멀라이저 켜짐</source>
        <translation>Normalizer On</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="994"/>
        <source>노멀라이저 꺼짐</source>
        <translation>Normalizer Off</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1014"/>
        <source>이퀄라이저 켜짐</source>
        <translation>Equalizer On</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1016"/>
        <source>이퀄라이저 꺼짐</source>
        <translation>Equalizer Off</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1036"/>
        <source>음악 줄임 켜짐</source>
        <translation>Music Reducing On</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1038"/>
        <source>음악 줄임 꺼짐</source>
        <translation>Music Reducing Off</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1054"/>
        <source>자막 투명도</source>
        <translation>Subtitle Transparency</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1056"/>
        <source>가사 투명도</source>
        <translation>Lyrics Transparency</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1080"/>
        <source>자막 투명도 초기화</source>
        <translation>Subtitle transparency was reset</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1082"/>
        <source>가사 투명도 초기화</source>
        <translation>Lyrics transparency was reset</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1091"/>
        <source>자막 크기</source>
        <translation>Subtitle Font Size</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1114"/>
        <source>자막 크기 초기화</source>
        <translation>Subtitle font size was reset</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1131"/>
        <source>하드웨어 디코더 사용 함</source>
        <translation>Hardware video decoder was activated</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1138"/>
        <source>하드웨어 디코더 사용 안 함</source>
        <translation>Hardware video decoder was deactivated</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1164"/>
        <source>하드웨어 디코더 변경 (%1)</source>
        <translation>Hardware video decoder was changed (%1)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1191"/>
        <source>프레임 드랍 사용 함</source>
        <translation>Frame drop was activated</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1193"/>
        <source>프레임 드랍 사용 안 함</source>
        <translation>Frame drop was deactivated</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1210"/>
        <source>버퍼링 모드 사용 함</source>
        <translation>Buffering mode was activated</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1212"/>
        <source>버퍼링 모드 사용 안 함</source>
        <translation>Buffering mode was deactivated</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1229"/>
        <source>GPU 디코딩 사용 함</source>
        <translation>GPU decoding was activated</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1231"/>
        <source>GPU 디코딩 사용 안 함</source>
        <translation>GPU decoding was deactivated</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1248"/>
        <source>HDR 사용 함</source>
        <translation>HDR was activated</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1250"/>
        <source>HDR 사용 안 함</source>
        <translation>HDR was deactivated</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1279"/>
        <source>색상 자동 보정 사용 함</source>
        <translation>Auto color conversion was activated</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1281"/>
        <source>색상 자동 보정 사용 안 함</source>
        <translation>Auto color conversion was deactivated</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1312"/>
        <source>S/PDIF 출력 사용 안 함</source>
        <translation>S/PDIF output was deactivated</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1338"/>
        <source>S/PDIF 출력 시 인코딩 사용 안 함</source>
        <translation>Encoding was deactivated</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1341"/>
        <source>S/PDIF 출력 시 AC3 인코딩 사용</source>
        <translation>AC3 encoding was activated</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1370"/>
        <source>3D 전체 해상도 사용</source>
        <translation>3D full size was activated</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1372"/>
        <source>3D 전체 해상도 사용 안 함</source>
        <translation>3D full size was deactivated</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1389"/>
        <source>고속 렌더링 사용</source>
        <translation>Hardware accelerated rendering was activated</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1391"/>
        <source>고속 렌더링 사용 안 함</source>
        <translation>Hardware accelerated rendering was deactivated</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1446"/>
        <source>음성 줄임 켜짐</source>
        <translation>Voice Reducing On</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1448"/>
        <source>음성 줄임 꺼짐</source>
        <translation>Voice Reducing Off</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1468"/>
        <source>음성 강조 켜짐</source>
        <translation>Voice Emphasizing On</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1470"/>
        <source>음성 강조 꺼짐</source>
        <translation>Voice Emphasizing Off</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1572"/>
        <source>자막 있음</source>
        <translation>Subtitle exists</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1574"/>
        <source>자막 없음</source>
        <translation>Subtitle does not exist</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1579"/>
        <source>가사 있음</source>
        <translation>Lyrics exists</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1581"/>
        <source>가사 없음</source>
        <translation>Lyrics does not exist</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1780"/>
        <source>화면 비율 사용 함</source>
        <translation>Screen aspect ratio was activated</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1783"/>
        <source> (화면 채우기)</source>
        <translation> (Stretch)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1789"/>
        <source>화면 비율 사용 안 함</source>
        <translation>Screen aspect ratio was deactivated</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1810"/>
        <source>전체 순차 재생</source>
        <translation>Full Sequential Playback</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1813"/>
        <source>전체 반복 재생</source>
        <translation>Full Repeat Playback</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1816"/>
        <source>한 개 재생</source>
        <translation>Play Single</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1819"/>
        <source>한 개 반복 재생</source>
        <translation>Repeat Single</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1822"/>
        <source>무작위 재생</source>
        <translation>Shuffle</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1828"/>
        <source>재생 순서 (%1)</source>
        <translation>Playback Method (%1)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1933"/>
        <source>역텔레시네 사용 함</source>
        <translation>Inverse telecine was activated</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1935"/>
        <source>역텔레시네 사용 안 함</source>
        <translation>Inverse telecine was deactivated</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1956"/>
        <source>자막 가로 정렬 변경</source>
        <translation>Subtitle horizontal alignment was changed</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1958"/>
        <source>가사 가로 정렬 변경</source>
        <translation>Lyrics horizontal alignment was changed</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1963"/>
        <source>자동 정렬</source>
        <translation>Auto Align</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1966"/>
        <source>왼쪽 정렬</source>
        <translation>Left Align</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1969"/>
        <source>오른쪽 정렬</source>
        <translation>Right Align</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1972"/>
        <location filename="../src/media/MediaPlayer.cpp" line="2008"/>
        <source>가운데 정렬</source>
        <translation>Middle Align</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1975"/>
        <location filename="../src/media/MediaPlayer.cpp" line="2014"/>
        <source>기본 정렬</source>
        <translation>Default Align</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1998"/>
        <source>자막 세로 정렬 변경</source>
        <translation>Subtitle vertical alignment was changed</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2000"/>
        <source>가사 세로 정렬 변경</source>
        <translation>Lyrics vertical alignment was changed</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2005"/>
        <source>상단 정렬</source>
        <translation>Top Align</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2011"/>
        <source>하단 정렬</source>
        <translation>Bottom Align</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2046"/>
        <source>자막 싱크 초기화</source>
        <translation>Subtitle synchronization was reset</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2048"/>
        <source>가사 싱크 초기화</source>
        <translation>Lyrics synchronization was reset</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2066"/>
        <source>자막 싱크 %1초 빠르게</source>
        <translation>Resync Subtitle (%1 Seconds faster)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2068"/>
        <source>자막 싱크 %1초 느리게</source>
        <translation>Resync Subtitle (%1 Seconds slower)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2073"/>
        <source>가사 싱크 %1초 빠르게</source>
        <translation>Resync Lyrics (%1 Seconds faster)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2075"/>
        <source>가사 싱크 %1초 느리게</source>
        <translation>Resync Lyrics (%1 Seconds slower)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2081"/>
        <location filename="../src/media/MediaPlayer.cpp" line="2177"/>
        <source> (%1초)</source>
        <translation> (%1 Seconds)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2121"/>
        <source>자막 찾기 켜짐</source>
        <translation>Subtitle searching was activated</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2123"/>
        <source>자막 찾기 꺼짐</source>
        <translation>Subtitle searching was deactivated</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2134"/>
        <source>가사 찾기 켜짐</source>
        <translation>Lyrics searching was activated</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2136"/>
        <source>가사 찾기 꺼짐</source>
        <translation>Lyrics searching was deactivated</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2147"/>
        <source>찾은 가사 자동 저장 켜짐</source>
        <translation>Autosave for found lyrics activated</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2149"/>
        <source>찾은 가사 자동 저장 꺼짐</source>
        <translation>Autosave for found lyrics deactivated</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2170"/>
        <source>소리 싱크 %1초 빠르게</source>
        <translation>Resync Sound (%1 Seconds faster)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2172"/>
        <source>소리 싱크 %1초 느리게</source>
        <translation>Resync Sound (%1 Seconds slower)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2232"/>
        <source>앞으로 %1초 키프레임 이동</source>
        <translation>Seek %1 Seconds Forward by Keyframe</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2234"/>
        <source>뒤로 %1초 키프레임 이동</source>
        <translation>Seek %1 Seconds Backward by Keyframe</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2239"/>
        <source>앞으로 %1초 이동</source>
        <translation>Seek %1 Seconds Forward</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2241"/>
        <source>뒤로 %1초 이동</source>
        <translation>Seek %1 Seconds Backward</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2279"/>
        <source>소리 꺼짐</source>
        <translation>Mute On</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2281"/>
        <source>소리 켜짐</source>
        <translation>Mute Off</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2382"/>
        <source>앨범 자켓 숨기기</source>
        <translation>Hide Album Jacket</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2701"/>
        <source>S/PDIF 소리 출력 장치 변경 (%1)</source>
        <translation>S/PDIF sound output device was changed (%1)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2725"/>
        <source>90도</source>
        <translation>90°</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2728"/>
        <source>180도</source>
        <translation>180°</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2731"/>
        <source>270도</source>
        <translation>270°</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2737"/>
        <source>화면 회전 각도 (%1)</source>
        <translation>Degree of screen rotation (%1)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1899"/>
        <location filename="../src/media/MediaPlayer.cpp" line="2722"/>
        <source>사용 안 함</source>
        <translation>Disable</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="592"/>
        <source>자막 위치 위로 : %1</source>
        <translation>Move Up Subtitle Position : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="594"/>
        <source>자막 위치 아래로 : %1</source>
        <translation>Move Down Subtitle Position : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="606"/>
        <source>자막 위치 왼쪽으로 : %1</source>
        <translation>Move Left Subtitle Position : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="608"/>
        <source>자막 위치 오른쪽으로 : %1</source>
        <translation>Move Right Subtitle Position : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="749"/>
        <source>구간 반복 시작 위치 %1초 뒤로 이동 (%2)</source>
        <translation>Subtract %1 seconds from beginning position for A-B repeat (%2)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="751"/>
        <source>구간 반복 시작 위치 %1초 앞으로 이동 (%2)</source>
        <translation>Add %1 seconds to beginning position for A-B repeat (%2)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="783"/>
        <source>구간 반복 끝 위치 %1초 뒤로 이동 (%2)</source>
        <translation>Subtract %1 seconds from ending position for A-B repeat (%2)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="785"/>
        <source>구간 반복 끝 위치 %1초 앞으로 이동 (%2)</source>
        <translation>Add %1 seconds to ending position for A-B repeat (%2)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="821"/>
        <source>구간 반복 위치 %1초 뒤로 이동 (%2 ~ %3)</source>
        <translation>Subtract %1 seconds from A-B repeat range (%2 ~ %3)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="823"/>
        <source>구간 반복 위치 %1초 앞으로 이동 (%2 ~ %3)</source>
        <translation>Add %1 seconds to A-B repeat range (%2 ~ %3)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="881"/>
        <source>3D 영상 (%1)</source>
        <translation>3D Video (%1)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="969"/>
        <source>재생 스킵 사용 함</source>
        <translation>Playback skip was activated</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="971"/>
        <source>재생 스킵 사용 안 함</source>
        <translation>Playback skip was deactivated</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1134"/>
        <source> (그래픽 카드 또는 코덱이 지원하지 않을 경우 활성화가 안 될 수 있습니다)</source>
        <translation> (If your video card or codec does not support hardware video decoder, it may be not activated)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1310"/>
        <source>S/PDIF 출력 사용</source>
        <translation>Use S/PDIF Output</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1316"/>
        <location filename="../src/media/MediaPlayer.cpp" line="1352"/>
        <location filename="../src/media/MediaPlayer.cpp" line="1430"/>
        <location filename="../src/media/MediaPlayer.cpp" line="2703"/>
        <source>S/PDIF 출력 초기화를 실패 하였습니다. PCM 출력으로 전환합니다.</source>
        <translation>S/PDIF output initialization failed. Switch to the PCM output.</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1344"/>
        <source>S/PDIF 출력 시 DTS 인코딩 사용</source>
        <translation>DTS encoding was activated</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1421"/>
        <source>S/PDIF 샘플 속도 (%1)</source>
        <translation>S/PDIF Sampling Rate (%1)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1424"/>
        <source>기본 속도</source>
        <translation>Default</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1622"/>
        <source>일시정지</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1646"/>
        <source>재생</source>
        <translation>Play</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1716"/>
        <source>이전으로 %1 프레임 이동</source>
        <translation>Move to Previous %1 Frames</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1731"/>
        <source>다음으로 %1 프레임 이동</source>
        <translation>Move to Next %1 Frames</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1763"/>
        <source>재생 속도 초기화</source>
        <translation>Playback speed was reset</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1767"/>
        <source>재생 속도 : %1배</source>
        <translation>Playback Speed : %1 times</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1874"/>
        <source>음성 변경 (%1)</source>
        <translation>Audio stream was changed (%1)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1893"/>
        <source>자동 판단</source>
        <translation>Auto Detection</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1896"/>
        <source>항상 사용</source>
        <translation>Always Use</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1905"/>
        <source>디인터레이스 (%1)</source>
        <translation>Deinterlacing (%1)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1913"/>
        <source>디인터레이스 알고리즘 (%1)</source>
        <translation>Deinterlacing Algorithm (%1)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2100"/>
        <source>소리 싱크 초기화</source>
        <translation>Sound synchronization was reset</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2265"/>
        <source>소리 (%1%)</source>
        <translation>Volume (%1%)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2380"/>
        <source>앨범 자켓 보이기</source>
        <translation>Show Album Jacket</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2413"/>
        <source>재생 위치 기억 켜짐</source>
        <translation>Remember Last Position On</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2415"/>
        <source>재생 위치 기억 꺼짐</source>
        <translation>Remember Last Position Off</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2436"/>
        <source>360도 영상 왜곡 보정 값 증가(K1) : %1</source>
        <translation>Increase 360° Distortion Coefficient(K1) : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2438"/>
        <source>360도 영상 왜곡 보정 값 감소(K1) : %1</source>
        <translation>Decrease 360° Distortion Coefficient(K1) : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2455"/>
        <source>360도 영상 왜곡 보정 값 증가(K2) : %1</source>
        <translation>Increase 360° Distortion Coefficient(K2) : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2457"/>
        <source>360도 영상 왜곡 보정 값 감소(K2) : %1</source>
        <translation>Decrease 360° Distortion Coefficient(K2) : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2484"/>
        <source>VR 평면 영상 왜곡 보정 값 증가(K1) : %1</source>
        <translation>Increase Normal VR Distortion Coefficient(K1) : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2486"/>
        <source>VR 평면 영상 왜곡 보정 값 감소(K1) : %1</source>
        <translation>Decrease Normal VR Distortion Coefficient(K1) : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2503"/>
        <source>VR 평면 영상 왜곡 보정 값 증가(K2) : %1</source>
        <translation>Increase Normal VR Distortion Coefficient(K2) : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2505"/>
        <source>VR 평면 영상 왜곡 보정 값 감소(K2) : %1</source>
        <translation>Decrease Normal VR Distortion Coefficient(K2) : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2535"/>
        <source>가상 3D 입체감 : %1</source>
        <translation>Virtual 3D Depth : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2642"/>
        <location filename="../src/media/MediaPlayer.cpp" line="2682"/>
        <source>기본 장치</source>
        <translation>Default Device</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2658"/>
        <source>소리 출력 장치 변경 (%1)</source>
        <translation>Sound output device was changed (%1)</translation>
    </message>
</context>
<context>
    <name>MediaPlayerDelegate</name>
    <message>
        <location filename="../src/delegate/MediaPlayerDelegate.cpp" line="187"/>
        <source>히스토그램 이퀄라이저 켜짐</source>
        <translation>Histogram equalization was activated</translation>
    </message>
    <message>
        <location filename="../src/delegate/MediaPlayerDelegate.cpp" line="189"/>
        <source>히스토그램 이퀄라이저 꺼짐</source>
        <translation>Histogram equalization was deactivated</translation>
    </message>
    <message>
        <location filename="../src/delegate/MediaPlayerDelegate.cpp" line="208"/>
        <source>3D 노이즈 제거 켜짐</source>
        <translation>3D denoise was activated</translation>
    </message>
    <message>
        <location filename="../src/delegate/MediaPlayerDelegate.cpp" line="210"/>
        <source>3D 노이즈 제거 꺼짐</source>
        <translation>3D denoise was deactivated</translation>
    </message>
    <message>
        <location filename="../src/delegate/MediaPlayerDelegate.cpp" line="229"/>
        <source>적응 시간 평균 노이즈 제거 켜짐</source>
        <translation>Adaptive temporal averaging denoise was activated</translation>
    </message>
    <message>
        <location filename="../src/delegate/MediaPlayerDelegate.cpp" line="231"/>
        <source>적응 시간 평균 노이즈 제거 꺼짐</source>
        <translation>Adaptive temporal averaging denoise was deactivated</translation>
    </message>
    <message>
        <location filename="../src/delegate/MediaPlayerDelegate.cpp" line="250"/>
        <source>Overcomplete Wavelet 노이즈 제거 켜짐</source>
        <translation>Overcomplete wavelet denoise was activated</translation>
    </message>
    <message>
        <location filename="../src/delegate/MediaPlayerDelegate.cpp" line="252"/>
        <source>Overcomplete Wavelet 노이즈 제거 꺼짐</source>
        <translation>Overcomplete wavelet denoise was deactivated</translation>
    </message>
    <message>
        <location filename="../src/delegate/MediaPlayerDelegate.cpp" line="271"/>
        <source>디밴드 켜짐</source>
        <translation>Deband was activated</translation>
    </message>
    <message>
        <location filename="../src/delegate/MediaPlayerDelegate.cpp" line="273"/>
        <source>디밴드 꺼짐</source>
        <translation>Deband was deactivated</translation>
    </message>
    <message>
        <location filename="../src/delegate/MediaPlayerDelegate.cpp" line="292"/>
        <source>디블록 켜짐</source>
        <translation>Deblock was activated</translation>
    </message>
    <message>
        <location filename="../src/delegate/MediaPlayerDelegate.cpp" line="294"/>
        <source>디블록 꺼짐</source>
        <translation>Deblock was deactivated</translation>
    </message>
    <message>
        <location filename="../src/delegate/MediaPlayerDelegate.cpp" line="328"/>
        <source>VR 입력 영상 (%1)</source>
        <translation>VR Input Source (%1)</translation>
    </message>
    <message>
        <location filename="../src/delegate/MediaPlayerDelegate.cpp" line="365"/>
        <source>VR 렌즈 센터 설정 (X : %1, Y : %2)</source>
        <translation>VR Lens Center (X : %1, Y : %2)</translation>
    </message>
    <message>
        <location filename="../src/delegate/MediaPlayerDelegate.cpp" line="439"/>
        <source>왜곡 보정 모드 사용 안 함</source>
        <translation>Distortion adjustment mode was deactivated</translation>
    </message>
    <message>
        <location filename="../src/delegate/MediaPlayerDelegate.cpp" line="443"/>
        <source>왜곡 보정 모드 사용 : %1</source>
        <translation>Distortion adjustment mode was activated : %1</translation>
    </message>
    <message>
        <location filename="../src/delegate/MediaPlayerDelegate.cpp" line="459"/>
        <source>왜곡 보정 사용 함</source>
        <translation>Correcting Distortion On</translation>
    </message>
    <message>
        <location filename="../src/delegate/MediaPlayerDelegate.cpp" line="461"/>
        <source>왜곡 보정 사용 안 함</source>
        <translation>Correcting Distortion Off</translation>
    </message>
    <message>
        <location filename="../src/delegate/MediaPlayerDelegate.cpp" line="488"/>
        <source>노멀라이즈 켜짐</source>
        <translation>Normalize was activated</translation>
    </message>
    <message>
        <location filename="../src/delegate/MediaPlayerDelegate.cpp" line="490"/>
        <source>노멀라이즈 꺼짐</source>
        <translation>Normalize was deactivated</translation>
    </message>
    <message>
        <location filename="../src/delegate/MediaPlayerDelegate.cpp" line="509"/>
        <source>Non-local means 노이즈 제거 켜짐</source>
        <translation>Non-local means denoise was activated</translation>
    </message>
    <message>
        <location filename="../src/delegate/MediaPlayerDelegate.cpp" line="511"/>
        <source>Non-local means 노이즈 제거 꺼짐</source>
        <translation>Non-local means denoise was deactivated</translation>
    </message>
    <message>
        <location filename="../src/delegate/MediaPlayerDelegate.cpp" line="530"/>
        <source>Vague 노이즈 제거 켜짐</source>
        <translation>Vague denoise was activated</translation>
    </message>
    <message>
        <location filename="../src/delegate/MediaPlayerDelegate.cpp" line="532"/>
        <source>Vague 노이즈 제거 꺼짐</source>
        <translation>Vague denoise was deactivated</translation>
    </message>
    <message>
        <location filename="../src/delegate/MediaPlayerDelegate.cpp" line="681"/>
        <source>이전 챕터로 이동 (%1)</source>
        <translation>Move to Previous Chapter (%1)</translation>
    </message>
    <message>
        <location filename="../src/delegate/MediaPlayerDelegate.cpp" line="693"/>
        <source>다음 챕터로 이동 (%1)</source>
        <translation>Move to Next Chapter (%1)</translation>
    </message>
    <message>
        <location filename="../src/delegate/MediaPlayerDelegate.cpp" line="745"/>
        <source>자막</source>
        <translation>Subtitle</translation>
    </message>
    <message>
        <location filename="../src/delegate/MediaPlayerDelegate.cpp" line="745"/>
        <source>가사</source>
        <translation>Lyrics</translation>
    </message>
    <message>
        <location filename="../src/delegate/MediaPlayerDelegate.cpp" line="746"/>
        <source>%1 (*.%2);;모든 파일 (*.*)</source>
        <translation>%1 (*.%2);;All files (*.*)</translation>
    </message>
    <message>
        <location filename="../src/delegate/MediaPlayerDelegate.cpp" line="782"/>
        <source>자막을 변경 할 수 없습니다</source>
        <translation>Can not change subtitle</translation>
    </message>
    <message>
        <location filename="../src/delegate/MediaPlayerDelegate.cpp" line="784"/>
        <source>가사를 변경 할 수 없습니다</source>
        <translation>Can not change lyrics</translation>
    </message>
    <message>
        <location filename="../src/delegate/MediaPlayerDelegate.cpp" line="796"/>
        <source>영상을 변경 할 수 없습니다</source>
        <translation>Can not change video</translation>
    </message>
    <message>
        <location filename="../src/delegate/MediaPlayerDelegate.cpp" line="805"/>
        <source>음성을 변경 할 수 없습니다</source>
        <translation>Can not change audio</translation>
    </message>
    <message>
        <location filename="../src/delegate/MediaPlayerDelegate.cpp" line="896"/>
        <source>처음으로 이동</source>
        <translation>Move to Begin</translation>
    </message>
</context>
<context>
    <name>MediaPresenter</name>
    <message>
        <location filename="../src/media/MediaPresenter.cpp" line="2287"/>
        <source>프레임 저하가 일어나고 있습니다. 성능에 영향을 미치는 옵션 또는 수직 동기화를 꺼주세요.</source>
        <translation>Frame dropping is happening. Please disable options that affect performance, or vertical sync.</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPresenter.cpp" line="2995"/>
        <source>S/PDIF 출력 초기화를 실패 하였습니다. PCM 출력으로 전환합니다.</source>
        <translation>S/PDIF output initialization failed. Switch to the PCM output.</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPresenter.cpp" line="4185"/>
        <source>오프닝 스킵 : %1</source>
        <translation>Intro was skipped : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPresenter.cpp" line="4206"/>
        <source>재생 스킵 : %1 ~ %2</source>
        <translation>Playback was skipped : %1 ~ %2</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPresenter.cpp" line="4324"/>
        <source>구간 반복 : %1 ~ %2</source>
        <translation>A-B was repeating : %1 ~ %2</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPresenter.cpp" line="4332"/>
        <source>S/PDIF 출력을 지원하지 않은 포맷이므로 PCM 출력으로 전환합니다.</source>
        <translation>Due to output does not support S/PDIF output format, it will be changed to PCM output.</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPresenter.cpp" line="4338"/>
        <source>하드웨어 디코딩을 지원하지 않는 코덱이므로 하드웨어 디코딩을 끕니다.</source>
        <translation>Turn off hardware decoding because it is a codec that does not support hardware decoding.</translation>
    </message>
</context>
<context>
    <name>MessageBoxUtils</name>
    <message>
        <location filename="../src/utils/MessageBoxUtils.cpp" line="28"/>
        <source>오류</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../src/utils/MessageBoxUtils.cpp" line="33"/>
        <source>정보</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../src/utils/MessageBoxUtils.cpp" line="38"/>
        <source>질문</source>
        <translation>Question</translation>
    </message>
</context>
<context>
    <name>MultipleCapture</name>
    <message>
        <location filename="../forms/multiplecapture.ui" line="19"/>
        <source>여러 장 캡쳐</source>
        <translation>Capture Multiple Shots</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="33"/>
        <source>캡쳐 시작</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="52"/>
        <source>캡쳐 중지</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="81"/>
        <source>닫기</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="93"/>
        <source>프레임</source>
        <translation>Frames</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="162"/>
        <source>매</source>
        <translation> Sheets</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="177"/>
        <source>지정 시간까지 캡쳐</source>
        <translation>Captured up to the specified time</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="190"/>
        <source>지정 개수까지 캡쳐</source>
        <translation>Captured up to the specified count</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="219"/>
        <location filename="../forms/multiplecapture.ui" line="234"/>
        <source>확장자</source>
        <translation>Extension</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="257"/>
        <source>품질</source>
        <translation>Quality</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="292"/>
        <source>기본 품질</source>
        <translation>Default Quality</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="302"/>
        <source>캡쳐 현황</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="316"/>
        <source>현재 개수</source>
        <translation>Current Count</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="343"/>
        <source>총 개수</source>
        <translation>Total Count</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="365"/>
        <source>크기</source>
        <translation>Size</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="371"/>
        <source>원본 크기로 캡쳐</source>
        <translation>Original Size</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="384"/>
        <source>현재 크기로 캡쳐</source>
        <translation>Current Size</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="394"/>
        <source>사용자 정의 크기로 캡쳐</source>
        <translation>Custom Size</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="481"/>
        <source>비율</source>
        <translation>Aspect Ratio</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="496"/>
        <source>기타</source>
        <translation>ETC</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="502"/>
        <source>자막도 같이 캡쳐</source>
        <translation>Capture with subtitle</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="509"/>
        <source>세부 정보도 같이 캡쳐</source>
        <translation>Caputre with detail info</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="525"/>
        <source>저장 위치</source>
        <translation>Save Directory</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="546"/>
        <source>변경</source>
        <translation>Change</translation>
    </message>
    <message>
        <location filename="../src/ui/MultipleCapture.cpp" line="142"/>
        <source>캡쳐를 할 수 없습니다. 지정 시간 또는 지정 개수를 확인 해 주세요.</source>
        <translation>Can not capture. Please specify a time or a number of sheet.</translation>
    </message>
</context>
<context>
    <name>OpenDTVScanChannel</name>
    <message>
        <location filename="../forms/opendtvscanchannel.ui" line="14"/>
        <source>DTV 채널 검색</source>
        <translation>Scan DTV Channels</translation>
    </message>
    <message>
        <location filename="../forms/opendtvscanchannel.ui" line="20"/>
        <source>채널 검색</source>
        <translation>Scan Channels</translation>
    </message>
    <message>
        <location filename="../forms/opendtvscanchannel.ui" line="26"/>
        <source>장치</source>
        <translation>Device</translation>
    </message>
    <message>
        <location filename="../forms/opendtvscanchannel.ui" line="48"/>
        <source>어댑터</source>
        <translation>Adapter</translation>
    </message>
    <message>
        <location filename="../forms/opendtvscanchannel.ui" line="61"/>
        <source>형식</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../forms/opendtvscanchannel.ui" line="81"/>
        <source>상태</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../forms/opendtvscanchannel.ui" line="89"/>
        <source>현재 채널 :</source>
        <translation>Current Channel :</translation>
    </message>
    <message>
        <location filename="../forms/opendtvscanchannel.ui" line="103"/>
        <source>찾은 채널 개수 :</source>
        <translation>Scanned Channel Count :</translation>
    </message>
    <message>
        <location filename="../forms/opendtvscanchannel.ui" line="119"/>
        <location filename="../src/ui/OpenDTVScanChannel.cpp" line="98"/>
        <source>신호 감도</source>
        <translation>Signal Strength</translation>
    </message>
    <message>
        <location filename="../forms/opendtvscanchannel.ui" line="162"/>
        <source>채널 범위</source>
        <translation>Channel Range</translation>
    </message>
    <message>
        <location filename="../forms/opendtvscanchannel.ui" line="215"/>
        <source>검색</source>
        <translation>Scan</translation>
    </message>
    <message>
        <location filename="../forms/opendtvscanchannel.ui" line="231"/>
        <source>중지</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location filename="../forms/opendtvscanchannel.ui" line="248"/>
        <source>국가 선택</source>
        <translation>Country</translation>
    </message>
    <message>
        <location filename="../forms/opendtvscanchannel.ui" line="273"/>
        <source>채널 정보</source>
        <translation>Channel Infomation</translation>
    </message>
    <message>
        <location filename="../src/ui/OpenDTVScanChannel.cpp" line="97"/>
        <source>채널</source>
        <translation>Channel</translation>
    </message>
    <message>
        <location filename="../forms/opendtvscanchannel.ui" line="343"/>
        <source>저장</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="../forms/opendtvscanchannel.ui" line="372"/>
        <source>취소</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../src/ui/OpenDTVScanChannel.cpp" line="99"/>
        <source>이름</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../src/ui/OpenDTVScanChannel.cpp" line="210"/>
        <source>채널 정보가 없습니다.</source>
        <translation>No channel information.</translation>
    </message>
    <message>
        <location filename="../src/ui/OpenDTVScanChannel.cpp" line="219"/>
        <source>시작 채널이 종료 채널보다 큽니다.</source>
        <translation>Start channel is greater then end channel.</translation>
    </message>
    <message>
        <location filename="../src/ui/OpenDTVScanChannel.cpp" line="228"/>
        <source>채널 범위가 맞지 않습니다.</source>
        <translation>Check channel range.</translation>
    </message>
</context>
<context>
    <name>OpenDevice</name>
    <message>
        <location filename="../forms/opendevice.ui" line="20"/>
        <source>장치 열기</source>
        <translation>Open Device</translation>
    </message>
    <message>
        <location filename="../forms/opendevice.ui" line="34"/>
        <source>비디오 장치</source>
        <translation>Video Device</translation>
    </message>
    <message>
        <location filename="../forms/opendevice.ui" line="53"/>
        <source>오디오 장치</source>
        <translation>Audio Device</translation>
    </message>
    <message>
        <location filename="../forms/opendevice.ui" line="107"/>
        <source>열기</source>
        <translation>Open</translation>
    </message>
    <message>
        <location filename="../forms/opendevice.ui" line="136"/>
        <source>취소</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../src/ui/OpenDevice.cpp" line="42"/>
        <location filename="../src/ui/OpenDevice.cpp" line="45"/>
        <source>선택 안 함</source>
        <translation>Do not Select</translation>
    </message>
    <message>
        <location filename="../src/ui/OpenDevice.cpp" line="158"/>
        <source>비디오 장치를 선택 해 주세요.</source>
        <translation>Select the video device.</translation>
    </message>
    <message>
        <location filename="../src/ui/OpenDevice.cpp" line="162"/>
        <source>오디오 장치를 선택 해 주세요.</source>
        <translation>Select the audio device.</translation>
    </message>
    <message>
        <location filename="../src/ui/OpenDevice.cpp" line="172"/>
        <source>비디오 장치 또는 오디오 장치를 선택 해야 합니다.</source>
        <translation>You have to select a video device or a audio device.</translation>
    </message>
</context>
<context>
    <name>OpenExternal</name>
    <message>
        <location filename="../forms/openexternal.ui" line="19"/>
        <source>외부 열기</source>
        <translation>Open External</translation>
    </message>
    <message>
        <location filename="../forms/openexternal.ui" line="31"/>
        <source>주소를 입력 해 주세요</source>
        <translation>Please Input Address</translation>
    </message>
    <message>
        <location filename="../forms/openexternal.ui" line="55"/>
        <source>주소</source>
        <translation>Address</translation>
    </message>
    <message>
        <location filename="../forms/openexternal.ui" line="110"/>
        <source>예) http://somehost.com/movie.mp4</source>
        <translation>Ex) http://somehost.com/movie.mp4</translation>
    </message>
    <message>
        <location filename="../forms/openexternal.ui" line="160"/>
        <source>열기</source>
        <translation>Open</translation>
    </message>
    <message>
        <location filename="../forms/openexternal.ui" line="189"/>
        <source>취소</source>
        <translation>Cancel</translation>
    </message>
</context>
<context>
    <name>OpenRadioScanChannel</name>
    <message>
        <location filename="../forms/openradioscanchannel.ui" line="14"/>
        <source>라디오 채널 검색</source>
        <translation>Scan DTV Channels</translation>
    </message>
    <message>
        <location filename="../forms/openradioscanchannel.ui" line="20"/>
        <source>채널 검색</source>
        <translation>Scan Channels</translation>
    </message>
    <message>
        <location filename="../forms/openradioscanchannel.ui" line="26"/>
        <source>장치</source>
        <translation>Device</translation>
    </message>
    <message>
        <location filename="../forms/openradioscanchannel.ui" line="48"/>
        <source>어댑터</source>
        <translation>Adapter</translation>
    </message>
    <message>
        <location filename="../forms/openradioscanchannel.ui" line="61"/>
        <source>형식</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../forms/openradioscanchannel.ui" line="81"/>
        <source>상태</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../forms/openradioscanchannel.ui" line="89"/>
        <source>현재 채널 :</source>
        <translation>Current Channel :</translation>
    </message>
    <message>
        <location filename="../forms/openradioscanchannel.ui" line="103"/>
        <source>찾은 채널 개수 :</source>
        <translation>Scanned Channel Count :</translation>
    </message>
    <message>
        <location filename="../forms/openradioscanchannel.ui" line="119"/>
        <location filename="../src/ui/OpenRadioScanChannel.cpp" line="99"/>
        <source>신호 감도</source>
        <translation>Signal Strength</translation>
    </message>
    <message>
        <location filename="../forms/openradioscanchannel.ui" line="171"/>
        <source>채널 범위</source>
        <translation>Channel Range</translation>
    </message>
    <message>
        <location filename="../forms/openradioscanchannel.ui" line="224"/>
        <source>검색</source>
        <translation>Scan</translation>
    </message>
    <message>
        <location filename="../forms/openradioscanchannel.ui" line="240"/>
        <source>중지</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location filename="../forms/openradioscanchannel.ui" line="257"/>
        <source>국가 선택</source>
        <translation>Country</translation>
    </message>
    <message>
        <location filename="../forms/openradioscanchannel.ui" line="282"/>
        <source>채널 정보</source>
        <translation>Channel Infomation</translation>
    </message>
    <message>
        <location filename="../forms/openradioscanchannel.ui" line="352"/>
        <source>저장</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="../forms/openradioscanchannel.ui" line="381"/>
        <source>취소</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../src/ui/OpenRadioScanChannel.cpp" line="98"/>
        <source>순서</source>
        <translation>No.</translation>
    </message>
    <message>
        <location filename="../src/ui/OpenRadioScanChannel.cpp" line="100"/>
        <source>주파수</source>
        <translation>Frequency</translation>
    </message>
    <message>
        <location filename="../src/ui/OpenRadioScanChannel.cpp" line="207"/>
        <source>채널 정보가 없습니다.</source>
        <translation>No channel information.</translation>
    </message>
    <message>
        <location filename="../src/ui/OpenRadioScanChannel.cpp" line="216"/>
        <source>시작 채널이 종료 채널보다 큽니다.</source>
        <translation>Start channel is greater then end channel.</translation>
    </message>
    <message>
        <location filename="../src/ui/OpenRadioScanChannel.cpp" line="225"/>
        <source>채널 범위가 맞지 않습니다.</source>
        <translation>Check channel range.</translation>
    </message>
    <message>
        <location filename="../src/ui/OpenRadioScanChannel.cpp" line="287"/>
        <source>라디오 장치를 열 수 없습니다.</source>
        <translation>Can&apos;t open the radio device.</translation>
    </message>
</context>
<context>
    <name>PlayList</name>
    <message>
        <location filename="../forms/playlist.ui" line="19"/>
        <source>재생 목록</source>
        <translation>Playback List</translation>
    </message>
    <message>
        <location filename="../forms/playlist.ui" line="71"/>
        <source>닫기</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="../src/ui/PlayList.cpp" line="386"/>
        <source>삭제</source>
        <translation>Delete</translation>
    </message>
    <message>
        <location filename="../src/ui/PlayList.cpp" line="394"/>
        <source>맨 위로</source>
        <translation>Move to the Most Top</translation>
    </message>
    <message>
        <location filename="../src/ui/PlayList.cpp" line="400"/>
        <source>맨 아래로</source>
        <translation>Move to the Most Bottom</translation>
    </message>
    <message>
        <location filename="../src/ui/PlayList.cpp" line="408"/>
        <source>위로</source>
        <translation>Move Up</translation>
    </message>
    <message>
        <location filename="../src/ui/PlayList.cpp" line="414"/>
        <source>아래로</source>
        <translation>Move Down</translation>
    </message>
    <message>
        <location filename="../src/ui/PlayList.cpp" line="422"/>
        <source>파일 경로 보기</source>
        <translation>View File Path</translation>
    </message>
    <message>
        <location filename="../src/ui/PlayList.cpp" line="428"/>
        <source>파일 경로 복사</source>
        <translation>Copy File Path</translation>
    </message>
    <message>
        <location filename="../src/ui/PlayList.cpp" line="438"/>
        <source>다른 화질</source>
        <translation>Quality</translation>
    </message>
    <message>
        <location filename="../src/ui/PlayList.cpp" line="474"/>
        <source>장면 탐색</source>
        <translation>Screen Explorer</translation>
    </message>
</context>
<context>
    <name>Popup</name>
    <message>
        <location filename="../forms/popup.ui" line="19"/>
        <source>알림</source>
        <translation>Notify</translation>
    </message>
</context>
<context>
    <name>RemoteFileList</name>
    <message>
        <location filename="../forms/remotefilelist.ui" line="19"/>
        <source>원격 파일 목록</source>
        <translation>Remote Playback List</translation>
    </message>
    <message>
        <location filename="../forms/remotefilelist.ui" line="67"/>
        <source>폴더,</source>
        <translation>Folders,</translation>
    </message>
    <message>
        <location filename="../forms/remotefilelist.ui" line="87"/>
        <source>파일</source>
        <translation>Files</translation>
    </message>
    <message>
        <location filename="../forms/remotefilelist.ui" line="119"/>
        <source>새로 고침</source>
        <translation>Refresh</translation>
    </message>
    <message>
        <location filename="../forms/remotefilelist.ui" line="141"/>
        <source>닫기</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="../src/ui/RemoteFileList.cpp" line="66"/>
        <source>이름</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../src/ui/RemoteFileList.cpp" line="67"/>
        <source>확장자</source>
        <translation>Extension</translation>
    </message>
    <message>
        <location filename="../src/ui/RemoteFileList.cpp" line="68"/>
        <source>재생 시간</source>
        <translation>Duration</translation>
    </message>
    <message>
        <location filename="../src/ui/RemoteFileList.cpp" line="69"/>
        <source>전송 속도</source>
        <translation>Transfer Speed</translation>
    </message>
    <message>
        <location filename="../src/ui/RemoteFileList.cpp" line="144"/>
        <source>&lt;DIR&gt;</source>
        <translation>&lt;DIR&gt;</translation>
    </message>
    <message>
        <location filename="../src/ui/RemoteFileList.cpp" line="262"/>
        <source>재생 목록에 추가</source>
        <translation>Add to Playback List</translation>
    </message>
</context>
<context>
    <name>SPDIFInterface</name>
    <message>
        <location filename="../src/audio/SPDIFInterface.cpp" line="104"/>
        <source>기본 장치</source>
        <translation>Default Device</translation>
    </message>
</context>
<context>
    <name>Screen</name>
    <message>
        <location filename="../src/ui/Screen.cpp" line="359"/>
        <source>캡쳐 성공</source>
        <translation>Capturing Succeed</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="361"/>
        <source>캡쳐 실패</source>
        <translation>Capturing Failed</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="421"/>
        <source>수직 동기화 상태를 변경 후 적용 하려면 프로그램을 재 시작 해야 합니다.</source>
        <translation>You must restart the program for the vertical sync state to take effect after you change it.</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="218"/>
        <source>캡쳐를 시작하지 못했습니다</source>
        <translation>Can&apos;t start capture</translation>
    </message>
</context>
<context>
    <name>ScreenContextMenu</name>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="75"/>
        <source>화면 회전 각도</source>
        <translation>Screen Rotation Degree</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="82"/>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="338"/>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="373"/>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="1426"/>
        <source>사용 안 함</source>
        <translation>Disable</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="82"/>
        <source>90도</source>
        <translation>90°</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="82"/>
        <source>180도</source>
        <translation>180°</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="82"/>
        <source>270도</source>
        <translation>270°</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="101"/>
        <source>영상</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="137"/>
        <source>3D 영상 설정</source>
        <translation>3D Mode Settings</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="176"/>
        <source>체커 보드</source>
        <translation>Checker Board</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="189"/>
        <source>페이지 플리핑</source>
        <translation>Page Flipping</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="202"/>
        <source>인터레이스</source>
        <translation>Interlaced</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="222"/>
        <source>애너글리프</source>
        <translation>Anaglyph</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="269"/>
        <source>하드웨어 디코더</source>
        <translation>Hardware Video Decoder</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="298"/>
        <source>화면 크기</source>
        <translation>Screen Size</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="325"/>
        <source>화면 비율</source>
        <translation>Screen Aspect Ratio</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="340"/>
        <source>화면 채우기</source>
        <translation>Stretch</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="342"/>
        <source>사용자 지정 (%1:%2)</source>
        <translation>Custom (%1:%2)</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="366"/>
        <source>디인터레이스</source>
        <translation>Deinterlace</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="373"/>
        <source>자동 판단</source>
        <translation>Auto Detection</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="373"/>
        <source>항상 사용</source>
        <translation>Always Use</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="388"/>
        <source>알고리즘</source>
        <translation>Algorithm</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="422"/>
        <source>캡쳐</source>
        <translation>Capture</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="477"/>
        <source>영상 속성</source>
        <translation>Video Attributes</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="505"/>
        <source>영상 효과</source>
        <translation>Video Effects</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="561"/>
        <source>왜곡 보정 모드</source>
        <translation>Distortion Adjustment Mode</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="584"/>
        <source>360도 영상</source>
        <translation>360° Video Mode</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="618"/>
        <source>VR 평면 영상</source>
        <translation>Normal VR Mode</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="675"/>
        <source>프로젝션 종류</source>
        <translation>Projection Type</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="701"/>
        <source>Language</source>
        <translation>Language</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="726"/>
        <source>업데이트 확인 주기</source>
        <translation>Update Check Cycle</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="749"/>
        <source>화면</source>
        <translation>Screen</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="882"/>
        <source>재생 순서</source>
        <translation>Playback Order</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="889"/>
        <source>전체 순차 재생</source>
        <translation>Full Sequential Playback</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="889"/>
        <source>전체 반복 재생</source>
        <translation>Full Repeat Playback</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="889"/>
        <source>한 개 재생</source>
        <translation>Play One</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="889"/>
        <source>한 개 반복 재생</source>
        <translation>Repeat One</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="889"/>
        <source>무작위 재생</source>
        <translation>Shuffle</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="908"/>
        <source>재생 스킵</source>
        <translation>Playback Skip</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="940"/>
        <source>구간 반복</source>
        <translation>A-B Repeat</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="988"/>
        <source>재생</source>
        <translation>Playback</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="1143"/>
        <source>정렬</source>
        <translation>Alignment</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="1151"/>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="1173"/>
        <source>기본 정렬</source>
        <translation>Default Align</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="1151"/>
        <source>자동 정렬</source>
        <translation>Auto Align</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="1151"/>
        <source>왼쪽 정렬</source>
        <translation>Left Align</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="1151"/>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="1173"/>
        <source>가운데 정렬</source>
        <translation>Middle Align</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="1151"/>
        <source>오른쪽 정렬</source>
        <translation>Right Align</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="1173"/>
        <source>상단 정렬</source>
        <translation>Top Align</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="1173"/>
        <source>하단 정렬</source>
        <translation>Bottom Align</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="1198"/>
        <source>언어</source>
        <translation>Languages</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="1225"/>
        <source>위치</source>
        <translation>Position</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="1252"/>
        <source>자막 / 가사</source>
        <translation>Subtitle / Lyrics</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="1377"/>
        <source>3D 자막 설정</source>
        <translation>3D Subtitle Mode Settings</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="1419"/>
        <source>인코딩 사용</source>
        <translation>Use Encoding</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="1442"/>
        <source>DTV 열기</source>
        <translation>Open DTV</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="1473"/>
        <source>라디오 열기</source>
        <translation>Open Radio</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="1511"/>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="1641"/>
        <source>소리 출력 장치</source>
        <translation>Sound Output Devices</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="1518"/>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="1648"/>
        <source>기본 장치</source>
        <translation>Default Device</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="1538"/>
        <source>S/PDIF 설정</source>
        <translation>S/PDIF Settings</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="1564"/>
        <source>기본 속도</source>
        <translation>Default Sample Rate</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="1577"/>
        <source>소리 효과</source>
        <translation>Sound Effects</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="1604"/>
        <source>음성</source>
        <translation>Audio</translation>
    </message>
    <message>
        <location filename="../src/ui/ScreenContextMenu.cpp" line="1667"/>
        <source>소리</source>
        <translation>Sound</translation>
    </message>
</context>
<context>
    <name>ScreenDelegate</name>
    <message>
        <location filename="../src/delegate/ScreenDelegate.cpp" line="47"/>
        <source>HDR 사용 상태를 변경 후 적용 하려면 프로그램을 재 시작 해야 합니다.</source>
        <translation>You must restart the program to apply the HDR usage status after changing it.</translation>
    </message>
    <message>
        <location filename="../src/delegate/ScreenDelegate.cpp" line="62"/>
        <source>캡쳐 저장 경로 변경 : %1</source>
        <translation>Capturing save directory was changed : %1</translation>
    </message>
    <message>
        <location filename="../src/delegate/ScreenDelegate.cpp" line="137"/>
        <source>캡쳐 확장자 변경 : %1</source>
        <translation>Capturing extension was changed : %1</translation>
    </message>
</context>
<context>
    <name>ScreenExplorer</name>
    <message>
        <location filename="../forms/screenexplorer.ui" line="14"/>
        <source>장면 탐색</source>
        <translation>Screen Explorer</translation>
    </message>
    <message>
        <location filename="../forms/screenexplorer.ui" line="20"/>
        <source>설정</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="../forms/screenexplorer.ui" line="39"/>
        <source>간격 :</source>
        <translation>Step :</translation>
    </message>
    <message>
        <location filename="../forms/screenexplorer.ui" line="87"/>
        <source>닫기</source>
        <translation>Close</translation>
    </message>
</context>
<context>
    <name>ScreenExplorerItem</name>
    <message>
        <location filename="../forms/screenexploreritem.ui" line="17"/>
        <source>screen</source>
        <translation>screen</translation>
    </message>
    <message>
        <location filename="../forms/screenexploreritem.ui" line="27"/>
        <source>time</source>
        <translation>time</translation>
    </message>
</context>
<context>
    <name>ServerSetting</name>
    <message>
        <location filename="../forms/serversetting.ui" line="20"/>
        <source>서버 설정</source>
        <translation>Server Setting</translation>
    </message>
    <message>
        <location filename="../forms/serversetting.ui" line="32"/>
        <source>설정</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="../forms/serversetting.ui" line="38"/>
        <source>주소</source>
        <translation>Address</translation>
    </message>
    <message>
        <location filename="../forms/serversetting.ui" line="48"/>
        <source>커맨드 포트</source>
        <translation>Command Port</translation>
    </message>
    <message>
        <location filename="../forms/serversetting.ui" line="55"/>
        <source>스트림 포트</source>
        <translation>Stream Port</translation>
    </message>
    <message>
        <location filename="../forms/serversetting.ui" line="128"/>
        <source>확인</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../forms/serversetting.ui" line="144"/>
        <source>취소</source>
        <translation>Cancel</translation>
    </message>
</context>
<context>
    <name>Shader</name>
    <message>
        <location filename="../forms/shader.ui" line="25"/>
        <source>셰이더</source>
        <translation>Shader</translation>
    </message>
    <message>
        <location filename="../forms/shader.ui" line="31"/>
        <source>목록</source>
        <translation>List</translation>
    </message>
    <message>
        <location filename="../forms/shader.ui" line="80"/>
        <source>목록에 등록 된 순서대로 적용 됩니다</source>
        <translation>Will be applied in the order they are registered on the list</translation>
    </message>
    <message>
        <location filename="../forms/shader.ui" line="104"/>
        <source>추가</source>
        <translation>Add</translation>
    </message>
    <message>
        <location filename="../forms/shader.ui" line="120"/>
        <source>선택 삭제</source>
        <translation>Delete Selection</translation>
    </message>
    <message>
        <location filename="../forms/shader.ui" line="136"/>
        <source>모두 삭제</source>
        <translation>Delete All</translation>
    </message>
    <message>
        <location filename="../forms/shader.ui" line="165"/>
        <source>선택 위로</source>
        <translation>Move Up Selection</translation>
    </message>
    <message>
        <location filename="../forms/shader.ui" line="181"/>
        <source>선택 아래로</source>
        <translation>Move Down Selection</translation>
    </message>
    <message>
        <location filename="../forms/shader.ui" line="210"/>
        <source>적용</source>
        <translation>Apply</translation>
    </message>
    <message>
        <location filename="../forms/shader.ui" line="226"/>
        <source>닫기</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="../forms/shader.ui" line="243"/>
        <source>적용 결과</source>
        <translation>Result</translation>
    </message>
    <message>
        <location filename="../src/ui/Shader.cpp" line="110"/>
        <source>삭제</source>
        <translation>Delete</translation>
    </message>
    <message>
        <location filename="../src/ui/Shader.cpp" line="118"/>
        <source>맨 위로</source>
        <translation>Move to the Most Top</translation>
    </message>
    <message>
        <location filename="../src/ui/Shader.cpp" line="124"/>
        <source>맨 아래로</source>
        <translation>Move to the Most Bottom</translation>
    </message>
    <message>
        <location filename="../src/ui/Shader.cpp" line="132"/>
        <source>위로</source>
        <translation>Move Up</translation>
    </message>
    <message>
        <location filename="../src/ui/Shader.cpp" line="138"/>
        <source>아래로</source>
        <translation>Move Down</translation>
    </message>
    <message>
        <location filename="../src/ui/Shader.cpp" line="321"/>
        <source>GLSL (*.glsl);;텍스트 (*.txt);;모든 파일 (*.*)</source>
        <translation>GLSL (*.glsl);;Text (*.txt);;All files (*.*)</translation>
    </message>
    <message>
        <location filename="../src/ui/Shader.cpp" line="344"/>
        <source>성공</source>
        <translation>Succeed</translation>
    </message>
    <message>
        <location filename="../src/ui/Shader.cpp" line="352"/>
        <source>실패</source>
        <translation>Failed</translation>
    </message>
</context>
<context>
    <name>ShaderCompositerDelegate</name>
    <message>
        <location filename="../src/delegate/ShaderCompositerDelegate.cpp" line="55"/>
        <source>애너글리프 알고리즘 (%1)</source>
        <translation>Anaglyph Algorithm (%1)</translation>
    </message>
    <message>
        <location filename="../src/delegate/ShaderCompositerDelegate.cpp" line="75"/>
        <source>360도 영상 프로젝션 종류 : %1</source>
        <translation>Projection Type of 360° Video : %1</translation>
    </message>
    <message>
        <location filename="../src/delegate/ShaderCompositerDelegate.cpp" line="89"/>
        <source>영상 속성 초기화</source>
        <translation>Reset Video Attributes</translation>
    </message>
    <message>
        <location filename="../src/delegate/ShaderCompositerDelegate.cpp" line="116"/>
        <source>영상 밝기 %1배</source>
        <translation>%1 Times Screen Brightness</translation>
    </message>
    <message>
        <location filename="../src/delegate/ShaderCompositerDelegate.cpp" line="143"/>
        <source>영상 채도 %1배</source>
        <translation>%1 Times Screen Saturation</translation>
    </message>
    <message>
        <location filename="../src/delegate/ShaderCompositerDelegate.cpp" line="173"/>
        <source>영상 색상 각도 : %1°</source>
        <translation>Degree of Hue : %1°</translation>
    </message>
    <message>
        <location filename="../src/delegate/ShaderCompositerDelegate.cpp" line="200"/>
        <source>영상 대비 %1배</source>
        <translation>%1 Times Screen Contrast</translation>
    </message>
    <message>
        <location filename="../src/delegate/ShaderCompositerDelegate.cpp" line="215"/>
        <source>영상 날카롭게 켜짐</source>
        <translation>Sharply On</translation>
    </message>
    <message>
        <location filename="../src/delegate/ShaderCompositerDelegate.cpp" line="217"/>
        <source>영상 날카롭게 꺼짐</source>
        <translation>Sharply Off</translation>
    </message>
    <message>
        <location filename="../src/delegate/ShaderCompositerDelegate.cpp" line="234"/>
        <source>영상 선명하게 켜짐</source>
        <translation>Sharpen On</translation>
    </message>
    <message>
        <location filename="../src/delegate/ShaderCompositerDelegate.cpp" line="236"/>
        <source>영상 선명하게 꺼짐</source>
        <translation>Sharpen Off</translation>
    </message>
    <message>
        <location filename="../src/delegate/ShaderCompositerDelegate.cpp" line="253"/>
        <source>영상 부드럽게 켜짐</source>
        <translation>Soften On</translation>
    </message>
    <message>
        <location filename="../src/delegate/ShaderCompositerDelegate.cpp" line="255"/>
        <source>영상 부드럽게 꺼짐</source>
        <translation>Soften Off</translation>
    </message>
    <message>
        <location filename="../src/delegate/ShaderCompositerDelegate.cpp" line="272"/>
        <source>영상 좌우 반전 켜짐</source>
        <translation>Mirroring On</translation>
    </message>
    <message>
        <location filename="../src/delegate/ShaderCompositerDelegate.cpp" line="274"/>
        <source>영상 좌우 반전 꺼짐</source>
        <translation>Mirroring Off</translation>
    </message>
    <message>
        <location filename="../src/delegate/ShaderCompositerDelegate.cpp" line="291"/>
        <source>영상 상하 반전 켜짐</source>
        <translation>Flip Vertically On</translation>
    </message>
    <message>
        <location filename="../src/delegate/ShaderCompositerDelegate.cpp" line="293"/>
        <source>영상 상하 반전 꺼짐</source>
        <translation>Flip Vertically Off</translation>
    </message>
    <message>
        <location filename="../src/delegate/ShaderCompositerDelegate.cpp" line="308"/>
        <source>360도 영상 모드 사용 함</source>
        <translation>360° video mode was activated</translation>
    </message>
    <message>
        <location filename="../src/delegate/ShaderCompositerDelegate.cpp" line="310"/>
        <source>360도 영상 모드 사용 안 함</source>
        <translation>360° video mode was deactivated</translation>
    </message>
</context>
<context>
    <name>ShortcutKey</name>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="82"/>
        <source>로그인</source>
        <translation>Login</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="83"/>
        <source>로그아웃</source>
        <translation>Logout</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="84"/>
        <source>항상 위</source>
        <translation>Most Top</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="85"/>
        <source>원격 파일 목록 열기</source>
        <translation>Open RemoteFile List</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="86"/>
        <source>외부 열기</source>
        <translation>Open External</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="87"/>
        <source>열기</source>
        <translation>Open</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="88"/>
        <source>닫기</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="89"/>
        <source>종료</source>
        <translation>Exit</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="161"/>
        <source>노멀라이저 사용</source>
        <translation>Use Normalizer</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="162"/>
        <source>이퀄라이저 사용</source>
        <translation>Use Equalizer</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="90"/>
        <source>이퀄라이저 설정</source>
        <translation>Equalizer Settings</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="91"/>
        <source>재생 목록 열기</source>
        <translation>Open Playback List</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="163"/>
        <source>자막 기본 위치</source>
        <translation>Default Subtitle Position</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="164"/>
        <source>자막 위치 위로</source>
        <translation>Move Up Subtitle Position</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="165"/>
        <source>자막 위치 아래로</source>
        <translation>Move Down Subtitle Position</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="166"/>
        <source>자막 위치 왼쪽으로</source>
        <translation>Move Left Subtitle Position</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="167"/>
        <source>자막 위치 오른쪽으로</source>
        <translation>Move Right Subtitle Position</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="92"/>
        <source>재생 / 일시정지</source>
        <translation>Play / Pause</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="93"/>
        <source>재생 / 일시정지(재생)</source>
        <translation>Play / Pause(Play)</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="94"/>
        <source>재생 / 일시정지(일시정지)</source>
        <translation>Play / Pause(Pause)</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="95"/>
        <source>재생 / 일시정지(토글)</source>
        <translation>Play / Pause(Toggle)</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="96"/>
        <source>전체 화면</source>
        <translation>Full Screen</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="97"/>
        <source>전체 화면(추가)</source>
        <translation>Full Screen(Additional)</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="168"/>
        <source>5초 뒤로</source>
        <translation>5 Seconds Backward</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="169"/>
        <source>5초 앞으로</source>
        <translation>5 Seconds Forward</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="170"/>
        <source>30초 뒤로</source>
        <translation>30 Seconds Backward</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="171"/>
        <source>30초 앞으로</source>
        <translation>30 Seconds Forward</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="172"/>
        <source>1분 뒤로</source>
        <translation>1 Minute Backward</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="173"/>
        <source>1분 앞으로</source>
        <translation>1 Minute Forward</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="174"/>
        <source>처음으로 이동</source>
        <translation>Move to Begin</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="98"/>
        <source>이전 파일</source>
        <translation>Previous File</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="99"/>
        <source>이전 파일(추가)</source>
        <translation>Previous File(Additional)</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="100"/>
        <source>다음 파일</source>
        <translation>Next File</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="101"/>
        <source>다음 파일(추가)</source>
        <translation>Next File(Additional)</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="175"/>
        <source>재생 순서 순차 선택</source>
        <translation>Select Playback Order</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="176"/>
        <source>보이기</source>
        <translation>Visible</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="177"/>
        <location filename="../src/ui/ShortcutKey.cpp" line="185"/>
        <source>싱크 느리게</source>
        <translation>Resync (Slower)</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="178"/>
        <location filename="../src/ui/ShortcutKey.cpp" line="186"/>
        <source>싱크 빠르게</source>
        <translation>Resync (Faster)</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="179"/>
        <location filename="../src/ui/ShortcutKey.cpp" line="187"/>
        <source>싱크 초기화</source>
        <translation>Reset Sync</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="180"/>
        <source>자막 언어 순차 선택</source>
        <translation>Select Subtitle Language Order</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="102"/>
        <source>소리 크게</source>
        <translation>Volume Up</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="103"/>
        <source>소리 크게(추가)</source>
        <translation>Volume Up(Additional)</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="104"/>
        <source>소리 작게</source>
        <translation>Volume Down</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="105"/>
        <source>소리 작게(추가)</source>
        <translation>Volume Down(Additional)</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="106"/>
        <source>음소거</source>
        <translation>Mute</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="107"/>
        <source>음소거(추가)</source>
        <translation>Mute(Additional)</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="181"/>
        <source>음성 순차 선택</source>
        <translation>Select Audio Order</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="108"/>
        <source>정보</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="182"/>
        <source>재생 정보</source>
        <translation>Playback Information</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="109"/>
        <source>컨트롤바 보이기</source>
        <translation>Show ControlBar</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="110"/>
        <location filename="../src/ui/ShortcutKey.cpp" line="204"/>
        <source>투명도 증가</source>
        <translation>Increase Transparency</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="111"/>
        <location filename="../src/ui/ShortcutKey.cpp" line="205"/>
        <source>투명도 감소</source>
        <translation>Decrease Transparency</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="112"/>
        <source>최대 투명도</source>
        <translation>Maximum Transparency</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="113"/>
        <source>최소 투명도</source>
        <translation>Minimum Transparency</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="183"/>
        <source>디인터레이스 순차 선택</source>
        <translation>Select Deinterlacing Method Order</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="184"/>
        <source>디인터레이스 알고리즘 순차 선택</source>
        <translation>Select Deinterlacing Algorithm Order</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="188"/>
        <source>자막 가로 정렬 방법 순차 선택</source>
        <translation>Select Horizontal Alignment Method of Subtitle</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="189"/>
        <source>구간 반복 시작</source>
        <translation>Beginning of A-B Repeat</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="190"/>
        <source>구간 반복 끝</source>
        <translation>Ending of A-B Repeat</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="191"/>
        <source>구간 반복 활성화</source>
        <translation>Enable A-B Repeat</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="192"/>
        <source>키프레임 단위로 이동</source>
        <translation>Seek to Keyframe</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="193"/>
        <source>오프닝 스킵 사용</source>
        <translation>Intro Skip</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="194"/>
        <source>엔딩 스킵 사용</source>
        <translation>Ending Skip</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="195"/>
        <source>재생 스킵 사용</source>
        <translation>Use Playback Skipping</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="114"/>
        <source>재생 스킵 설정</source>
        <translation>Playback Skipping Settings</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="147"/>
        <source>캡쳐 확장자 순차 선택</source>
        <translation>Select Order of Extension of Capturing</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="145"/>
        <source>한 장 캡쳐</source>
        <translation>Captures One Shot</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="139"/>
        <source>재생 목록에 라디오 채널 추가</source>
        <translation>Add Radio Channels to Playlist</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="140"/>
        <source>라디오 채널 검색</source>
        <translation>Scan Radio Channels</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="146"/>
        <source>여러 장 캡쳐</source>
        <translation>Capture Multiple Shots</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="196"/>
        <source>이전 프레임으로 이동</source>
        <translation>Move to Previous Frame</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="197"/>
        <source>다음 프레임으로 이동</source>
        <translation>Move to Next Frame</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="198"/>
        <source>재생 속도 느리게</source>
        <translation>Slower Playback</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="199"/>
        <source>재생 속도 빠르게</source>
        <translation>Faster Playback</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="200"/>
        <source>재생 속도 초기화</source>
        <translation>Reset Playback Speed</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="281"/>
        <source>영상 속성 초기화</source>
        <translation>Reset Video Attributes</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="282"/>
        <source>영상 밝기 감소</source>
        <translation>Decrease Brightness</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="283"/>
        <source>영상 밝기 증가</source>
        <translation>Increase Brightness</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="284"/>
        <source>영상 채도 감소</source>
        <translation>Decrease Saturation</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="285"/>
        <source>영상 채도 증가</source>
        <translation>Increase Saturation</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="286"/>
        <source>영상 색상 감소</source>
        <translation>Decrease Hue</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="287"/>
        <source>영상 색상 증가</source>
        <translation>Increase Hue</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="288"/>
        <source>영상 대비 감소</source>
        <translation>Decrease Contrast</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="289"/>
        <source>영상 대비 증가</source>
        <translation>Increase Contrast</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="115"/>
        <source>셰이더 조합</source>
        <translation>Shader Composition</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="201"/>
        <source>음악 줄임</source>
        <translation>Reduce Music</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="202"/>
        <source>음성 줄임</source>
        <translation>Reduce Voice</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="203"/>
        <source>음성 강조</source>
        <translation>Emphasis Voice</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="290"/>
        <source>날카롭게</source>
        <translation>Sharply</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="291"/>
        <source>선명하게</source>
        <translation>Sharpen</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="292"/>
        <source>부드럽게</source>
        <translation>Soften</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="293"/>
        <source>좌우 반전</source>
        <translation>Mirror</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="294"/>
        <source>상하 반전</source>
        <translation>Flip Vertically</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="117"/>
        <source>재생 목록에 추가</source>
        <translation>Add to Playback List</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="148"/>
        <source>저장 디렉토리 열기</source>
        <translation>Open the Save Directory</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="149"/>
        <source>저장 디렉토리 설정</source>
        <translation>Set the Save Directory</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="116"/>
        <source>단축 키 설정</source>
        <translation>Shortcut Key Settings</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="206"/>
        <source>소리 출력 장치 순차 선택</source>
        <translation>Select Order of Output Device of Sound</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="136"/>
        <source>확장자 연결</source>
        <translation>File Associations</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="207"/>
        <source>자막 찾기 켜기</source>
        <translation>Enable Subtitles Searching</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="208"/>
        <source>가사 찾기 켜기</source>
        <translation>Enable Lyrics Searching</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="209"/>
        <source>자막 세로 정렬 방법 순차 선택</source>
        <translation>Select Vertical Alignment Method of Subtitle</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="210"/>
        <source>자막 크기 증가</source>
        <translation>Increase Subtitle Font Size</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="211"/>
        <source>자막 크기 감소</source>
        <translation>Decrease Subtitle Font Size</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="212"/>
        <source>자막 크기 초기화</source>
        <translation>Reset Subtitle Font Size</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="213"/>
        <source>투명도 초기화</source>
        <translation>Reset Transparency</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="118"/>
        <source>화면 비율 순차 선택</source>
        <translation>Select Order of Screen Aspect Ratio</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="119"/>
        <source>화면 비율 사용자 지정</source>
        <translation>Custom Screen Aspect Ratio</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="120"/>
        <source>화면 크기 0.5배</source>
        <translation>0.5 Times Screen Size</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="121"/>
        <source>화면 크기 1배</source>
        <translation>1 Times Screen Size</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="122"/>
        <source>화면 크기 1.5배</source>
        <translation>1.5 Times Screen Size</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="123"/>
        <source>화면 크기 2배</source>
        <translation>2 Times Screen Size</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="214"/>
        <source>하드웨어 디코더 사용</source>
        <translation>Use Hardware Video Decoder</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="215"/>
        <source>S/PDIF 출력 사용</source>
        <translation>Use S/PDIF Output</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="216"/>
        <source>S/PDIF 샘플 속도 순차 선택</source>
        <translation>Select Order of S/PDIF Sampling Rate</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="217"/>
        <source>고속 렌더링 사용</source>
        <translation>Use Hardware Accelerated Rendering</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="218"/>
        <source>3D 영상 출력 방법 순차 선택</source>
        <translation>Select Order of 3D Video Rendering Method</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="150"/>
        <source>수직 동기화 사용</source>
        <translation>Use VSync</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="219"/>
        <source>이전 챕터로 이동</source>
        <translation>Move to Previous Chapter</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="220"/>
        <source>다음 챕터로 이동</source>
        <translation>Move to Next Chapter</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="221"/>
        <source>외부 닫기</source>
        <translation>Close External</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="302"/>
        <source>종료 시 재생 목록 비움</source>
        <translation>Clear Playback List on Exit</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="222"/>
        <source>앨범 자켓 보기</source>
        <translation>Show Album Jacket</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="223"/>
        <source>재생 위치 기억</source>
        <translation>Remember Last Position</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="137"/>
        <source>인코딩 설정</source>
        <translation>Encoding Setting</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="138"/>
        <source>서버 설정</source>
        <translation>Server Setting</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="224"/>
        <source>저장</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="225"/>
        <source>다른 이름으로 저장</source>
        <translation>Save As</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="226"/>
        <source>S/PDIF 소리 출력 장치 순차 선택</source>
        <translation>Select Order of S/PDIF Output Device of Sound</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="124"/>
        <source>장면 탐색</source>
        <translation>Screen Explorer</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="227"/>
        <source>시작 위치 0.1초 뒤로 이동</source>
        <translation>Subtract 0.1 Seconds from Beginning Position</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="228"/>
        <source>시작 위치 0.1초 앞으로 이동</source>
        <translation>Add 0.1 Seconds to Beginning Position</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="229"/>
        <source>끝 위치 0.1초 뒤로 이동</source>
        <translation>Subtract 0.1 Seconds from Ending Position</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="230"/>
        <source>끝 위치 0.1초 앞으로 이동</source>
        <translation>Add 0.1 Seconds to Ending Position</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="231"/>
        <source>구간 반복 0.1초 뒤로 이동</source>
        <translation>Subtract 0.1 Seconds from A-B Repeat Range</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="232"/>
        <source>구간 반복 0.1초 앞으로 이동</source>
        <translation>Add 0.1 Seconds to A-B Repeat Range</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="233"/>
        <source>프레임 드랍 사용</source>
        <translation>Use Frame Drop</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="234"/>
        <source>인코딩 순차 선택</source>
        <translation>Select Encoding Order</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="295"/>
        <source>애너글리프 알고리즘 순차 선택</source>
        <translation>Anaglyph Algorithm Order</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="235"/>
        <source>3D 자막 출력 방법 순차 선택</source>
        <translation>Select Order of 3D Subtitle Rendering Method</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="236"/>
        <source>3D 전체 해상도 사용</source>
        <translation>Use 3D Full Size</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="237"/>
        <source>3D 자막 기본 위치</source>
        <translation>Default 3D Subtitle Position</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="238"/>
        <source>3D 자막 위치 가깝게(세로)</source>
        <translation>Near 3D Subtitle Position(Vertical)</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="239"/>
        <source>3D 자막 위치 멀게(세로)</source>
        <translation>Far 3D Subtitle Position(Vertical)</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="240"/>
        <source>3D 자막 위치 가깝게(가로)</source>
        <translation>Near 3D Subtitle Position(Horizontal)</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="241"/>
        <source>3D 자막 위치 멀게(가로)</source>
        <translation>Far 3D Subtitle Position(Horizontal)</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="125"/>
        <source>검색 디렉토리</source>
        <translation>Search Directory</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="126"/>
        <source>자막 폰트 가져오기</source>
        <translation>Import Subtitle Fonts</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="242"/>
        <source>화면 회전 각도 순차 선택</source>
        <translation>Select Order of Screen Rotation Degree</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="127"/>
        <source>장치 열기</source>
        <translation>Open Device</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="128"/>
        <source>재생 목록에 DTV 채널 추가</source>
        <translation>Add DTV Channels to Playlist</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="129"/>
        <source>DTV 채널 검색</source>
        <translation>Scan DTV Channels</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="243"/>
        <source>히스토그램 이퀄라이저</source>
        <translation>Histogram Equalization</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="244"/>
        <source>3D 노이즈 제거</source>
        <translation>3D Denoise</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="130"/>
        <source>채널 편성표</source>
        <translation>Program Guide</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="245"/>
        <source>고급 검색</source>
        <translation>Advanced Search</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="246"/>
        <source>디밴드</source>
        <translation>Deband</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="247"/>
        <source>적응 시간 평균 노이즈 제거</source>
        <translation>Adaptive Temporal Averaging Denoise</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="248"/>
        <source>Overcomplete Wavelet 노이즈 제거</source>
        <translation>Overcomplete Wavelet Denoise</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="249"/>
        <source>버퍼링 모드 사용</source>
        <translation>Use Buffering Mode</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="131"/>
        <source>정지</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="132"/>
        <source>정지(추가)</source>
        <translation>Stop(Additional)</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="296"/>
        <source>360도 영상 사용</source>
        <translation>Use 360° Video Mode</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="250"/>
        <source>360도 영상 왜곡 보정 값 증가(K1)</source>
        <translation>360° Distortion Coefficients Value Increase(K1)</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="251"/>
        <source>360도 영상 왜곡 보정 값 감소(K1)</source>
        <translation>360° Distortion Coefficients Value Decrease(K1)</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="252"/>
        <source>360도 영상 왜곡 보정 값 증가(K2)</source>
        <translation>360° Distortion Coefficients Value Increase(K2)</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="253"/>
        <source>360도 영상 왜곡 보정 값 감소(K2)</source>
        <translation>360° Distortion Coefficients Value Decrease(K2)</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="254"/>
        <source>GPU 디코딩 사용</source>
        <translation>Use GPU Decoding</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="255"/>
        <source>찾은 가사 자동 저장</source>
        <translation>Autosave For Found Lyrics</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="156"/>
        <source>업데이트 확인 주기 순차 선택</source>
        <translation>Update Check Cycle Order</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="256"/>
        <source>입력 영상 출력 방법 순차 선택</source>
        <translation>Order of Rendering Method of Input Screen</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="257"/>
        <source>렌즈 센터 값 증가(좌/우)</source>
        <translation>Lens Center Value Increase (Side by Side)</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="258"/>
        <source>렌즈 센터 값 감소(좌/우)</source>
        <translation>Lens Center Value Decrease (Side by Side)</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="259"/>
        <source>렌즈 센터 값 증가(상/하)</source>
        <translation>Lens Center Value Increase (Top and Bottom)</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="260"/>
        <source>렌즈 센터 감소(상/하)</source>
        <translation>Lens Center Value Decrease (Top and Bottom)</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="261"/>
        <source>가상 3D 입체감 증가</source>
        <translation>Increase Virtual 3D Depth</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="262"/>
        <source>가상 3D 입체감 감소</source>
        <translation>Decrease Virtual 3D Depth</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="263"/>
        <source>VR 평면 영상 왜곡 보정 값 증가(K1)</source>
        <translation>Normal VR Distortion Coefficients Value Increase(K1)</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="264"/>
        <source>VR 평면 영상 왜곡 보정 값 감소(K1)</source>
        <translation>Normal VR Distortion Coefficients Value Decrease(K1)</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="265"/>
        <source>VR 평면 영상 왜곡 보정 값 증가(K2)</source>
        <translation>Normal VR Distortion Coefficients Value Increase(K2)</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="266"/>
        <source>VR 평면 영상 왜곡 보정 값 감소(K2)</source>
        <translation>Normal VR Distortion Coefficients Value Decrease(K2)</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="267"/>
        <source>왜곡 보정 모드 순차 선택</source>
        <translation>Order of Distortion Correction Mode</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="268"/>
        <source>왜곡 보정 사용</source>
        <translation>Use Distortion Correction</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="133"/>
        <source>설정 저장하기</source>
        <translation>Save Settings</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="134"/>
        <source>설정 불러오기</source>
        <translation>Load Settings</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="135"/>
        <source>오디오 시디 열기</source>
        <translation>Open Audio CD</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="269"/>
        <source>노멀라이즈</source>
        <translation>Normalize</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="270"/>
        <source>Non-local Means 노이즈 제거</source>
        <translation>Non-local Means Denoise</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="271"/>
        <source>Vague 노이즈 제거</source>
        <translation>Vague Denoise</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="151"/>
        <source>HDR 사용</source>
        <translation>Use HDR</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="272"/>
        <source>하드웨어 디코더 순차 선택</source>
        <translation>Hardware Video Decoder Order</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="297"/>
        <source>프로젝션 순차 선택</source>
        <translation>Select Order of Projection Type</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="273"/>
        <source>색상 자동 보정 사용</source>
        <translation>Use Auto Color Conversion</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="274"/>
        <source>디블록</source>
        <translation>Deblock</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="275"/>
        <source>역텔레시네 사용</source>
        <translation>Use Inverse Telecine</translation>
    </message>
    <message>
        <location filename="../src/ui/ShortcutKey.cpp" line="276"/>
        <source>영상 순차 선택</source>
        <translation>Select Video Order</translation>
    </message>
</context>
<context>
    <name>SkipRange</name>
    <message>
        <location filename="../forms/skiprange.ui" line="19"/>
        <source>재생 스킵 설정</source>
        <translation>Playback Skipping Settings</translation>
    </message>
    <message>
        <location filename="../forms/skiprange.ui" line="25"/>
        <source>설정</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="../forms/skiprange.ui" line="33"/>
        <location filename="../forms/skiprange.ui" line="47"/>
        <source>건너 뛰기</source>
        <translation>Skips</translation>
    </message>
    <message>
        <location filename="../forms/skiprange.ui" line="40"/>
        <source>오프닝 (앞에서)</source>
        <translation>Intro</translation>
    </message>
    <message>
        <location filename="../forms/skiprange.ui" line="67"/>
        <source>엔딩 (뒤에서)</source>
        <translation>Ending</translation>
    </message>
    <message>
        <location filename="../forms/skiprange.ui" line="93"/>
        <source>삭제</source>
        <translation>Delete</translation>
    </message>
    <message>
        <location filename="../forms/skiprange.ui" line="123"/>
        <source>구간</source>
        <translation>Range</translation>
    </message>
    <message>
        <location filename="../forms/skiprange.ui" line="139"/>
        <location filename="../src/ui/SkipRange.cpp" line="85"/>
        <source>시작</source>
        <translation>Beginning</translation>
    </message>
    <message>
        <location filename="../forms/skiprange.ui" line="169"/>
        <location filename="../src/ui/SkipRange.cpp" line="87"/>
        <source>범위</source>
        <translation>Range</translation>
    </message>
    <message>
        <location filename="../forms/skiprange.ui" line="197"/>
        <source>추가</source>
        <translation>Add</translation>
    </message>
    <message>
        <location filename="../forms/skiprange.ui" line="212"/>
        <location filename="../src/ui/SkipRange.cpp" line="86"/>
        <source>길이</source>
        <translation>Duration</translation>
    </message>
    <message>
        <location filename="../forms/skiprange.ui" line="234"/>
        <source>초</source>
        <translation> Seconds</translation>
    </message>
    <message>
        <location filename="../forms/skiprange.ui" line="288"/>
        <source>확인</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../forms/skiprange.ui" line="304"/>
        <source>취소</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../src/ui/SkipRange.cpp" line="88"/>
        <source>사용</source>
        <translation>Use</translation>
    </message>
    <message>
        <location filename="../src/ui/SkipRange.cpp" line="117"/>
        <source>%1초</source>
        <translation>%1 Seconds</translation>
    </message>
    <message>
        <location filename="../src/ui/SkipRange.cpp" line="229"/>
        <source>이미 해당 범위가 존재합니다</source>
        <translation>The range already exists</translation>
    </message>
</context>
<context>
    <name>Socket</name>
    <message>
        <location filename="../src/net/Socket.cpp" line="294"/>
        <source>접속이 끊겼습니다</source>
        <translation>Disconnected</translation>
    </message>
</context>
<context>
    <name>SubtitleDirectory</name>
    <message>
        <location filename="../forms/subtitledirectory.ui" line="14"/>
        <source>자막 / 가사 검색 디렉토리</source>
        <translation>Subtitle / Lyrics Search Directory</translation>
    </message>
    <message>
        <location filename="../forms/subtitledirectory.ui" line="20"/>
        <source>목록</source>
        <translation>List</translation>
    </message>
    <message>
        <location filename="../forms/subtitledirectory.ui" line="69"/>
        <source>등록된 디렉토리에서도 자막 / 가사를 검색합니다</source>
        <translation>Search the subtitles or lyrics in below directories</translation>
    </message>
    <message>
        <location filename="../forms/subtitledirectory.ui" line="93"/>
        <source>추가</source>
        <translation>Add</translation>
    </message>
    <message>
        <location filename="../forms/subtitledirectory.ui" line="109"/>
        <source>선택 삭제</source>
        <translation>Delete Selection</translation>
    </message>
    <message>
        <location filename="../forms/subtitledirectory.ui" line="125"/>
        <source>모두 삭제</source>
        <translation>Delete All</translation>
    </message>
    <message>
        <location filename="../forms/subtitledirectory.ui" line="154"/>
        <source>선택 위로</source>
        <translation>Move Up Selection</translation>
    </message>
    <message>
        <location filename="../forms/subtitledirectory.ui" line="170"/>
        <source>선택 아래로</source>
        <translation>Move Down Selection</translation>
    </message>
    <message>
        <location filename="../forms/subtitledirectory.ui" line="205"/>
        <source>등록된 디렉토리 우선 검색</source>
        <translation>Priority Search</translation>
    </message>
    <message>
        <location filename="../forms/subtitledirectory.ui" line="231"/>
        <source>적용</source>
        <translation>Apply</translation>
    </message>
    <message>
        <location filename="../forms/subtitledirectory.ui" line="247"/>
        <source>취소</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../src/ui/SubtitleDirectory.cpp" line="114"/>
        <source>삭제</source>
        <translation>Delete</translation>
    </message>
    <message>
        <location filename="../src/ui/SubtitleDirectory.cpp" line="122"/>
        <source>맨 위로</source>
        <translation>Move to the Most Top</translation>
    </message>
    <message>
        <location filename="../src/ui/SubtitleDirectory.cpp" line="128"/>
        <source>맨 아래로</source>
        <translation>Move to the Most Bottom</translation>
    </message>
    <message>
        <location filename="../src/ui/SubtitleDirectory.cpp" line="136"/>
        <source>위로</source>
        <translation>Move Up</translation>
    </message>
    <message>
        <location filename="../src/ui/SubtitleDirectory.cpp" line="142"/>
        <source>아래로</source>
        <translation>Move Down</translation>
    </message>
</context>
<context>
    <name>TextEncodeSetting</name>
    <message>
        <location filename="../forms/textencodesetting.ui" line="19"/>
        <source>인코딩 설정</source>
        <translation>Encoding Setting</translation>
    </message>
    <message>
        <location filename="../forms/textencodesetting.ui" line="25"/>
        <source>인코딩</source>
        <translation>Encoding</translation>
    </message>
    <message>
        <location filename="../forms/textencodesetting.ui" line="74"/>
        <source>확인</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../forms/textencodesetting.ui" line="90"/>
        <source>취소</source>
        <translation>Cancel</translation>
    </message>
</context>
<context>
    <name>Trackbar</name>
    <message>
        <location filename="../src/ui/Trackbar.cpp" line="88"/>
        <source>재생 위치</source>
        <translation>Playback Position</translation>
    </message>
</context>
<context>
    <name>UpdaterDelegate</name>
    <message>
        <location filename="../src/delegate/UpdaterDelegate.cpp" line="22"/>
        <source>업데이트 확인 주기 (%1)</source>
        <translation>Update Check Cycle (%1)</translation>
    </message>
</context>
<context>
    <name>UserAspectRatio</name>
    <message>
        <location filename="../forms/useraspectratio.ui" line="14"/>
        <source>화면 비율 사용자 지정</source>
        <translation>Custom Screen Aspect Ratio</translation>
    </message>
    <message>
        <location filename="../forms/useraspectratio.ui" line="30"/>
        <source>넓이</source>
        <translation>Width</translation>
    </message>
    <message>
        <location filename="../forms/useraspectratio.ui" line="70"/>
        <source>높이</source>
        <translation>Height</translation>
    </message>
    <message>
        <location filename="../forms/useraspectratio.ui" line="115"/>
        <source>확인</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../forms/useraspectratio.ui" line="144"/>
        <source>취소</source>
        <translation>Cancel</translation>
    </message>
</context>
<context>
    <name>VideoRenderer</name>
    <message>
        <location filename="../src/media/VideoRenderer.cpp" line="1060"/>
        <source>가사 있음</source>
        <translation>Lyrics exists</translation>
    </message>
    <message>
        <location filename="../src/media/VideoRenderer.cpp" line="1062"/>
        <source>자막 있음</source>
        <translation>Subtitle exists</translation>
    </message>
    <message>
        <location filename="../src/media/VideoRenderer.cpp" line="1067"/>
        <source>가사 없음</source>
        <translation>Lyrics does not exist</translation>
    </message>
    <message>
        <location filename="../src/media/VideoRenderer.cpp" line="1069"/>
        <source>자막 없음</source>
        <translation>Subtitle does not exist</translation>
    </message>
    <message>
        <location filename="../src/media/VideoRenderer.cpp" line="1075"/>
        <source>파일 이름 : </source>
        <translation>File Name : </translation>
    </message>
    <message>
        <location filename="../src/media/VideoRenderer.cpp" line="1092"/>
        <source>재생 위치 : </source>
        <translation>Position : </translation>
    </message>
    <message>
        <location filename="../src/media/VideoRenderer.cpp" line="1108"/>
        <source>파일 포맷 : </source>
        <translation>File Format : </translation>
    </message>
    <message>
        <location filename="../src/media/VideoRenderer.cpp" line="1115"/>
        <source>CPU 사용률 : </source>
        <translation>CPU Usage : </translation>
    </message>
    <message>
        <location filename="../src/media/VideoRenderer.cpp" line="1127"/>
        <source>메모리 사용량 : </source>
        <translation>Using Memory Size : </translation>
    </message>
    <message>
        <location filename="../src/media/VideoRenderer.cpp" line="1142"/>
        <source>DTV 신호 감도 : </source>
        <translation>DTV Signal Strength : </translation>
    </message>
    <message>
        <location filename="../src/media/VideoRenderer.cpp" line="1156"/>
        <source>비디오 코덱 : </source>
        <translation>Video Codec : </translation>
    </message>
    <message>
        <location filename="../src/media/VideoRenderer.cpp" line="1167"/>
        <source>하드웨어 디코더 : </source>
        <translation>Hardware Video Decoder : </translation>
    </message>
    <message>
        <location filename="../src/media/VideoRenderer.cpp" line="1175"/>
        <location filename="../src/media/VideoRenderer.cpp" line="1299"/>
        <source>입력 : </source>
        <translation>Input : </translation>
    </message>
    <message>
        <location filename="../src/media/VideoRenderer.cpp" line="1190"/>
        <location filename="../src/media/VideoRenderer.cpp" line="1313"/>
        <source>출력 : </source>
        <translation>Output : </translation>
    </message>
    <message>
        <location filename="../src/media/VideoRenderer.cpp" line="1209"/>
        <source>색상 변환 : </source>
        <translation>Color Conversion : </translation>
    </message>
    <message>
        <location filename="../src/media/VideoRenderer.cpp" line="1222"/>
        <location filename="../src/media/VideoRenderer.cpp" line="1233"/>
        <source>프레임 : </source>
        <translation>Frames : </translation>
    </message>
    <message>
        <location filename="../src/media/VideoRenderer.cpp" line="1255"/>
        <source>오디오 코덱 : </source>
        <translation>Audio Codec : </translation>
    </message>
    <message>
        <location filename="../src/media/VideoRenderer.cpp" line="1266"/>
        <source>S/PDIF 오디오 장치 : </source>
        <translation>S/PDIF Audio Device : </translation>
    </message>
    <message>
        <location filename="../src/media/VideoRenderer.cpp" line="1273"/>
        <source>인코딩 사용</source>
        <translation>Use Encoding</translation>
    </message>
    <message>
        <location filename="../src/media/VideoRenderer.cpp" line="1331"/>
        <source>가사 코덱 : </source>
        <translation>Lyrics Codec : </translation>
    </message>
    <message>
        <location filename="../src/media/VideoRenderer.cpp" line="1333"/>
        <source>자막 코덱 : </source>
        <translation>Subtitle Codec : </translation>
    </message>
</context>
<context>
    <name>ViewEPG</name>
    <message>
        <location filename="../forms/viewepg.ui" line="14"/>
        <source>채널 편성표</source>
        <translation>Program Guide</translation>
    </message>
    <message>
        <location filename="../forms/viewepg.ui" line="20"/>
        <source>정보</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../forms/viewepg.ui" line="26"/>
        <source>채널</source>
        <translation>Channel</translation>
    </message>
    <message>
        <location filename="../forms/viewepg.ui" line="40"/>
        <source>제목</source>
        <translation>Title</translation>
    </message>
    <message>
        <location filename="../forms/viewepg.ui" line="54"/>
        <source>방송 시간</source>
        <translation>Period</translation>
    </message>
    <message>
        <location filename="../forms/viewepg.ui" line="80"/>
        <source>현재 시각 :</source>
        <translation>Current Date Time :</translation>
    </message>
    <message>
        <location filename="../forms/viewepg.ui" line="113"/>
        <source>닫기</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="../src/ui/ViewEPG.cpp" line="39"/>
        <location filename="../src/ui/ViewEPG.cpp" line="40"/>
        <location filename="../src/ui/ViewEPG.cpp" line="41"/>
        <source>업데이트 중...</source>
        <translation>Now updating...</translation>
    </message>
</context>
</TS>
