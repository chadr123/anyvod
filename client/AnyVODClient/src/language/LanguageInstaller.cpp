﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "LanguageInstaller.h"

#include <QApplication>

const QString LanguageInstaller::LANG_PREFIX = "anyvod";
const QString LanguageInstaller::LANG_DIR = "./languages";

LanguageInstaller::LanguageInstaller()
{

}

LanguageInstaller& LanguageInstaller::getInstance()
{
    static LanguageInstaller instance;

    return instance;
}

void LanguageInstaller::install(const QString &lang)
{
    QApplication::removeTranslator(&this->m_langQt);
    QApplication::removeTranslator(&this->m_langQtBase);
    QApplication::removeTranslator(&this->m_langAnyVOD);

    QLocale english(QLocale::English);

    if (english.name().split('_').first() != lang)
    {
        this->installLanguage(this->m_langQt, "qt_%1", lang);
        this->installLanguage(this->m_langQtBase, "qtbase_%1", lang);
    }

    this->installLanguage(this->m_langAnyVOD, LANG_PREFIX + "_%1", lang);
}

void LanguageInstaller::installLanguage(QTranslator &translator, const QString &fileTemplate, const QString &lang)
{
    const QString localName = QLocale::system().name();
    const QString localLang = localName.split('_').first();
    bool loaded = translator.load(fileTemplate.arg(lang), LANG_DIR);

    if (!loaded)
        loaded = translator.load(fileTemplate.arg(localLang), LANG_DIR);

    if (!loaded)
        loaded = translator.load(fileTemplate.arg(localName), LANG_DIR);

    if (loaded)
        QApplication::installTranslator(&translator);
}
