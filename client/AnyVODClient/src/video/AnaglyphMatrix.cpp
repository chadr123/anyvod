﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "AnaglyphMatrix.h"

AnaglyphMatrix::AnaglyphMatrix()
{

}

QPair<QMatrix3x3, QMatrix3x3> AnaglyphMatrix::getMatrix(AnyVODEnums::Video3DMethod method, AnyVODEnums::AnaglyphAlgorithm algo)
{
    float matFirst[3][3];
    float matSecond[3][3];

    switch (method)
    {
        case AnyVODEnums::V3M_RED_CYAN_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_RED_CYAN_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_RED_CYAN_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_RED_CYAN_TOP_BOTTOM_BOTTOM_PRIOR:
        {
            switch (algo)
            {
                case AnyVODEnums::AGA_GRAY:
                {
                    matFirst[0][0] = 0.299f;
                    matFirst[0][1] = 0.587f;
                    matFirst[0][2] = 0.114f;
                    matFirst[1][0] = 0.0f;
                    matFirst[1][1] = 0.0f;
                    matFirst[1][2] = 0.0f;
                    matFirst[2][0] = 0.0f;
                    matFirst[2][1] = 0.0f;
                    matFirst[2][2] = 0.0f;

                    matSecond[0][0] = 0.0f;
                    matSecond[0][1] = 0.0f;
                    matSecond[0][2] = 0.0f;
                    matSecond[1][0] = 0.299f;
                    matSecond[1][1] = 0.587f;
                    matSecond[1][2] = 0.114f;
                    matSecond[2][0] = 0.299f;
                    matSecond[2][1] = 0.587f;
                    matSecond[2][2] = 0.114f;

                    break;
                }
                case AnyVODEnums::AGA_COLORED:
                {
                    matFirst[0][0] = 1.0f;
                    matFirst[0][1] = 0.0f;
                    matFirst[0][2] = 0.0f;
                    matFirst[1][0] = 0.0f;
                    matFirst[1][1] = 0.0f;
                    matFirst[1][2] = 0.0f;
                    matFirst[2][0] = 0.0f;
                    matFirst[2][1] = 0.0f;
                    matFirst[2][2] = 0.0f;

                    matSecond[0][0] = 0.0f;
                    matSecond[0][1] = 0.0f;
                    matSecond[0][2] = 0.0f;
                    matSecond[1][0] = 0.0f;
                    matSecond[1][1] = 1.0f;
                    matSecond[1][2] = 0.0f;
                    matSecond[2][0] = 0.0f;
                    matSecond[2][1] = 0.0f;
                    matSecond[2][2] = 1.0f;

                    break;
                }
                case AnyVODEnums::AGA_HALF_COLORED:
                {
                    matFirst[0][0] = 0.299f;
                    matFirst[0][1] = 0.587f;
                    matFirst[0][2] = 0.114f;
                    matFirst[1][0] = 0.0f;
                    matFirst[1][1] = 0.0f;
                    matFirst[1][2] = 0.0f;
                    matFirst[2][0] = 0.0f;
                    matFirst[2][1] = 0.0f;
                    matFirst[2][2] = 0.0f;

                    matSecond[0][0] = 0.0f;
                    matSecond[0][1] = 0.0f;
                    matSecond[0][2] = 0.0f;
                    matSecond[1][0] = 0.0f;
                    matSecond[1][1] = 1.0f;
                    matSecond[1][2] = 0.0f;
                    matSecond[2][0] = 0.0f;
                    matSecond[2][1] = 0.0f;
                    matSecond[2][2] = 1.0f;

                    break;
                }
                case AnyVODEnums::AGA_DUBOIS:
                {
                    matFirst[0][0] = 0.437f;
                    matFirst[0][1] = 0.449f;
                    matFirst[0][2] = 0.164f;
                    matFirst[1][0] = -0.062f;
                    matFirst[1][1] = -0.062f;
                    matFirst[1][2] = -0.024f;
                    matFirst[2][0] = -0.048f;
                    matFirst[2][1] = -0.050f;
                    matFirst[2][2] = -0.017f;

                    matSecond[0][0] = -0.011f;
                    matSecond[0][1] = -0.032f;
                    matSecond[0][2] = -0.007f;
                    matSecond[1][0] = 0.377f;
                    matSecond[1][1] = 0.761f;
                    matSecond[1][2] = 0.009f;
                    matSecond[2][0] = -0.026f;
                    matSecond[2][1] = -0.093f;
                    matSecond[2][2] = 1.234f;

                    break;
                }
                default:
                {
                    break;
                }
            }

            break;
        }
        case AnyVODEnums::V3M_GREEN_MAGENTA_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_GREEN_MAGENTA_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_GREEN_MAGENTA_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_GREEN_MAGENTA_TOP_BOTTOM_BOTTOM_PRIOR:
        {
            switch (algo)
            {
                case AnyVODEnums::AGA_GRAY:
                {
                    matFirst[0][0] = 0.0f;
                    matFirst[0][1] = 0.0f;
                    matFirst[0][2] = 0.0f;
                    matFirst[1][0] = 0.299f;
                    matFirst[1][1] = 0.587f;
                    matFirst[1][2] = 0.114f;
                    matFirst[2][0] = 0.0f;
                    matFirst[2][1] = 0.0f;
                    matFirst[2][2] = 0.0f;

                    matSecond[0][0] = 0.299f;
                    matSecond[0][1] = 0.587f;
                    matSecond[0][2] = 0.114f;
                    matSecond[1][0] = 0.0f;
                    matSecond[1][1] = 0.0f;
                    matSecond[1][2] = 0.0f;
                    matSecond[2][0] = 0.299f;
                    matSecond[2][1] = 0.587f;
                    matSecond[2][2] = 0.114f;

                    break;
                }
                case AnyVODEnums::AGA_COLORED:
                {
                    matFirst[0][0] = 0.0f;
                    matFirst[0][1] = 0.0f;
                    matFirst[0][2] = 0.0f;
                    matFirst[1][0] = 0.0f;
                    matFirst[1][1] = 1.0f;
                    matFirst[1][2] = 0.0f;
                    matFirst[2][0] = 0.0f;
                    matFirst[2][1] = 0.0f;
                    matFirst[2][2] = 0.0f;

                    matSecond[0][0] = 1.0f;
                    matSecond[0][1] = 0.0f;
                    matSecond[0][2] = 0.0f;
                    matSecond[1][0] = 0.0f;
                    matSecond[1][1] = 0.0f;
                    matSecond[1][2] = 0.0f;
                    matSecond[2][0] = 0.0f;
                    matSecond[2][1] = 0.0f;
                    matSecond[2][2] = 1.0f;

                    break;
                }
                case AnyVODEnums::AGA_HALF_COLORED:
                {
                    matFirst[0][0] = 0.0f;
                    matFirst[0][1] = 0.0f;
                    matFirst[0][2] = 0.0f;
                    matFirst[1][0] = 0.299f;
                    matFirst[1][1] = 0.587f;
                    matFirst[1][2] = 0.114f;
                    matFirst[2][0] = 0.0f;
                    matFirst[2][1] = 0.0f;
                    matFirst[2][2] = 0.0f;

                    matSecond[0][0] = 1.0f;
                    matSecond[0][1] = 0.0f;
                    matSecond[0][2] = 0.0f;
                    matSecond[1][0] = 0.0f;
                    matSecond[1][1] = 0.0f;
                    matSecond[1][2] = 0.0f;
                    matSecond[2][0] = 0.0f;
                    matSecond[2][1] = 0.0f;
                    matSecond[2][2] = 1.0f;

                    break;
                }
                case AnyVODEnums::AGA_DUBOIS:
                {
                    matFirst[0][0] = -0.062f;
                    matFirst[0][1] = -0.158f;
                    matFirst[0][2] = -0.039f;
                    matFirst[1][0] = 0.284f;
                    matFirst[1][1] = 0.668f;
                    matFirst[1][2] = 0.143f;
                    matFirst[2][0] = -0.015f;
                    matFirst[2][1] = -0.027f;
                    matFirst[2][2] = 0.021f;

                    matSecond[0][0] = 0.529f;
                    matSecond[0][1] = 0.705f;
                    matSecond[0][2] = 0.024f;
                    matSecond[1][0] = -0.016f;
                    matSecond[1][1] = -0.015f;
                    matSecond[1][2] = -0.065f;
                    matSecond[2][0] = 0.009f;
                    matSecond[2][1] = 0.075f;
                    matSecond[2][2] = 0.937f;

                    break;
                }
                default:
                {
                    break;
                }
            }

            break;
        }
        case AnyVODEnums::V3M_YELLOW_BLUE_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_YELLOW_BLUE_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_YELLOW_BLUE_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_YELLOW_BLUE_TOP_BOTTOM_BOTTOM_PRIOR:
        {
            switch (algo)
            {
                case AnyVODEnums::AGA_GRAY:
                {
                    matFirst[0][0] = 0.299f;
                    matFirst[0][1] = 0.587f;
                    matFirst[0][2] = 0.114f;
                    matFirst[1][0] = 0.299f;
                    matFirst[1][1] = 0.587f;
                    matFirst[1][2] = 0.114f;
                    matFirst[2][0] = 0.0f;
                    matFirst[2][1] = 0.0f;
                    matFirst[2][2] = 0.0f;

                    matSecond[0][0] = 0.0f;
                    matSecond[0][1] = 0.0f;
                    matSecond[0][2] = 0.0f;
                    matSecond[1][0] = 0.0f;
                    matSecond[1][1] = 0.0f;
                    matSecond[1][2] = 0.0f;
                    matSecond[2][0] = 0.299f;
                    matSecond[2][1] = 0.587f;
                    matSecond[2][2] = 0.114f;

                    break;
                }
                case AnyVODEnums::AGA_COLORED:
                {
                    matFirst[0][0] = 1.0f;
                    matFirst[0][1] = 0.0f;
                    matFirst[0][2] = 0.0f;
                    matFirst[1][0] = 0.0f;
                    matFirst[1][1] = 1.0f;
                    matFirst[1][2] = 0.0f;
                    matFirst[2][0] = 0.0f;
                    matFirst[2][1] = 0.0f;
                    matFirst[2][2] = 0.0f;

                    matSecond[0][0] = 0.0f;
                    matSecond[0][1] = 0.0f;
                    matSecond[0][2] = 0.0f;
                    matSecond[1][0] = 0.0f;
                    matSecond[1][1] = 0.0f;
                    matSecond[1][2] = 0.0f;
                    matSecond[2][0] = 0.0f;
                    matSecond[2][1] = 0.0f;
                    matSecond[2][2] = 1.0f;

                    break;
                }
                case AnyVODEnums::AGA_HALF_COLORED:
                {
                    matFirst[0][0] = 1.0f;
                    matFirst[0][1] = 0.0f;
                    matFirst[0][2] = 0.0f;
                    matFirst[1][0] = 0.0f;
                    matFirst[1][1] = 1.0f;
                    matFirst[1][2] = 0.0f;
                    matFirst[2][0] = 0.0f;
                    matFirst[2][1] = 0.0f;
                    matFirst[2][2] = 0.0f;

                    matSecond[0][0] = 0.0f;
                    matSecond[0][1] = 0.0f;
                    matSecond[0][2] = 0.0f;
                    matSecond[1][0] = 0.0f;
                    matSecond[1][1] = 0.0f;
                    matSecond[1][2] = 0.0f;
                    matSecond[2][0] = 0.299f;
                    matSecond[2][1] = 0.587f;
                    matSecond[2][2] = 0.114f;

                    break;
                }
                case AnyVODEnums::AGA_DUBOIS:
                {
                    matFirst[0][0] = 1.062f;
                    matFirst[0][1] = -0.205f;
                    matFirst[0][2] = 0.299f;
                    matFirst[1][0] = -0.026f;
                    matFirst[1][1] = 0.908f;
                    matFirst[1][2] = 0.068f;
                    matFirst[2][0] = -0.038f;
                    matFirst[2][1] = -0.173f;
                    matFirst[2][2] = 0.022f;

                    matSecond[0][0] = -0.016f;
                    matSecond[0][1] = -0.123f;
                    matSecond[0][2] = -0.017f;
                    matSecond[1][0] = 0.006f;
                    matSecond[1][1] = 0.062f;
                    matSecond[1][2] = -0.017f;
                    matSecond[2][0] = 0.094f;
                    matSecond[2][1] = 0.185f;
                    matSecond[2][2] = 0.911f;

                    break;
                }
                default:
                {
                    break;
                }
            }

            break;
        }
        case AnyVODEnums::V3M_RED_BLUE_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_RED_BLUE_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_RED_BLUE_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_RED_BLUE_TOP_BOTTOM_BOTTOM_PRIOR:
        {
            matFirst[0][0] = 0.299f;
            matFirst[0][1] = 0.587f;
            matFirst[0][2] = 0.114f;
            matFirst[1][0] = 0.0f;
            matFirst[1][1] = 0.0f;
            matFirst[1][2] = 0.0f;
            matFirst[2][0] = 0.0f;
            matFirst[2][1] = 0.0f;
            matFirst[2][2] = 0.0f;

            matSecond[0][0] = 0.0f;
            matSecond[0][1] = 0.0f;
            matSecond[0][2] = 0.0f;
            matSecond[1][0] = 0.0f;
            matSecond[1][1] = 0.0f;
            matSecond[1][2] = 0.0f;
            matSecond[2][0] = 0.299f;
            matSecond[2][1] = 0.587f;
            matSecond[2][2] = 0.114f;

            break;
        }
        case AnyVODEnums::V3M_RED_GREEN_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_RED_GREEN_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_RED_GREEN_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_RED_GREEN_TOP_BOTTOM_BOTTOM_PRIOR:
        {
            matFirst[0][0] = 0.299f;
            matFirst[0][1] = 0.587f;
            matFirst[0][2] = 0.114f;
            matFirst[1][0] = 0.0f;
            matFirst[1][1] = 0.0f;
            matFirst[1][2] = 0.0f;
            matFirst[2][0] = 0.0f;
            matFirst[2][1] = 0.0f;
            matFirst[2][2] = 0.0f;

            matSecond[0][0] = 0.0f;
            matSecond[0][1] = 0.0f;
            matSecond[0][2] = 0.0f;
            matSecond[1][0] = 0.299f;
            matSecond[1][1] = 0.587f;
            matSecond[1][2] = 0.114f;
            matSecond[2][0] = 0.0f;
            matSecond[2][1] = 0.0f;
            matSecond[2][2] = 0.0f;

            break;
        }
        default:
        {
            break;
        }
    }

    return qMakePair(QMatrix3x3(&matFirst[0][0]), QMatrix3x3(&matSecond[0][0]));
}
