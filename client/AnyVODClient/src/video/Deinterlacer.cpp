﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "Deinterlacer.h"
#include "utils/PixelUtils.h"

#include <QMutexLocker>
#include <QDebug>

extern "C"
{
# include <libavutil/opt.h>
# include <libavutil/imgutils.h>

# include <libavfilter/buffersrc.h>
# include <libavfilter/buffersink.h>
}

Deinterlacer::Deinterlacer() :
    m_method(AnyVODEnums::DM_AUTO),
    m_algorithm(AnyVODEnums::DA_BLEND),
    m_useIVTC(false)
{
#ifdef Q_OS_MAC
    this->m_group = dispatch_group_create();
    this->m_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
#endif
}

Deinterlacer::~Deinterlacer()
{
    this->unInitFilter();

#ifdef Q_OS_MAC
    dispatch_release(this->m_group);
    this->m_group = nullptr;
#endif
}

bool Deinterlacer::setCodec(AVCodecContext *codec, AVPixelFormat pixFormat, const AVRational &timeBase, const AVRational &frameRate)
{
    QMutexLocker lock(&this->m_deintLock);

    if (!this->isAVFilter())
        return true;

    this->unInitFilter();

    this->m_deint = Filter();
    this->m_deint.graph = avfilter_graph_alloc();

    if (!this->m_deint.graph)
        return false;

    this->m_deint.graph->scale_sws_opts = av_strdup("flags=neighbor");

    QString deintPreName;
    QString deintPreParam;

    QString deintName;
    QString deintParam;

    QString deintPostName;
    QString deintPostParam;

    this->getDeintNameAndParam(&deintPreName, &deintPreParam, &deintName, &deintParam, &deintPostName, &deintPostParam);

    const AVFilter *in = avfilter_get_by_name("buffer");
    const AVFilter *pre = avfilter_get_by_name(deintPreName.toLatin1());
    const AVFilter *deint = avfilter_get_by_name(deintName.toLatin1());
    const AVFilter *post = avfilter_get_by_name(deintPostName.toLatin1());
    const AVFilter *out = avfilter_get_by_name("buffersink");
    QString inParam;
    AVPixelFormat pixFormats[] = {pixFormat, AV_PIX_FMT_NONE};

    inParam = QString("video_size=%1x%2:pix_fmt=%3:time_base=%4/%5:frame_rate=%6/%7:pixel_aspect=%8/%9")
            .arg(codec->width)
            .arg(codec->height)
            .arg(pixFormat)
            .arg(timeBase.num)
            .arg(timeBase.den)
            .arg(frameRate.num)
            .arg(frameRate.den)
            .arg(codec->sample_aspect_ratio.num)
            .arg(codec->sample_aspect_ratio.den);

    if (avfilter_graph_create_filter(&this->m_deint.in, in, "in", inParam.toLatin1(), nullptr, this->m_deint.graph) < 0)
        return false;

    if (pre && avfilter_graph_create_filter(&this->m_deint.pre, pre, "pre", deintPreParam.toLatin1(), nullptr, this->m_deint.graph) < 0)
        return false;

    if (avfilter_graph_create_filter(&this->m_deint.deint, deint, "deint", deintParam.toLatin1(), nullptr, this->m_deint.graph) < 0)
        return false;

    if (post && avfilter_graph_create_filter(&this->m_deint.post, post, "post", deintPostParam.toLatin1(), nullptr, this->m_deint.graph) < 0)
        return false;

    if (avfilter_graph_create_filter(&this->m_deint.out, out, "out", nullptr, nullptr, this->m_deint.graph) < 0)
        return false;

    if (av_opt_set_int_list(this->m_deint.out, "pix_fmts", pixFormats, AV_PIX_FMT_NONE, AV_OPT_SEARCH_CHILDREN) < 0)
        return false;

    if (pre && post)
    {
        if (avfilter_link(this->m_deint.in, 0, this->m_deint.pre, 0) < 0)
            return false;

        if (avfilter_link(this->m_deint.pre, 0, this->m_deint.deint, 0) < 0)
            return false;

        if (avfilter_link(this->m_deint.deint, 0, this->m_deint.post, 0) < 0)
            return false;

        if (avfilter_link(this->m_deint.post, 0, this->m_deint.out, 0) < 0)
            return false;
    }
    else
    {
        if (avfilter_link(this->m_deint.in, 0, this->m_deint.deint, 0) < 0)
            return false;

        if (avfilter_link(this->m_deint.deint, 0, this->m_deint.out, 0) < 0)
            return false;
    }

    if (avfilter_graph_config(this->m_deint.graph, nullptr) < 0)
        return false;

    return true;
}

bool Deinterlacer::deinterlace(AVFrame *first, bool *getFirstFrame, AVFrame *second, bool *getSecondFrame, AVFrame *frame)
{
    QMutexLocker lock(&this->m_deintLock);

    this->deint(first, getFirstFrame, second, getSecondFrame, frame, this->m_deint);

    return true;
}

bool Deinterlacer::isAVFilter() const
{
    switch (this->m_algorithm)
    {
        case AnyVODEnums::DA_YADIF:
        case AnyVODEnums::DA_YADIF_BOB:
        case AnyVODEnums::DA_W3FDIF:
        case AnyVODEnums::DA_KERNDEINT:
        case AnyVODEnums::DA_MCDEINT:
        case AnyVODEnums::DA_BWDIF:
        case AnyVODEnums::DA_BWDIF_BOB:
            return true;
        default:
            return false;
    }
}

void Deinterlacer::unInitFilter()
{
    if (this->m_deint.graph)
        avfilter_graph_free(&this->m_deint.graph);
}

void Deinterlacer::deint(AVFrame *first, bool *getFirstFrame, AVFrame *second, bool *getSecondFrame,
                         AVFrame *frame, const Filter &filter)
{
    class localPtr
    {
    public:
        localPtr() :
            m_first(av_frame_alloc()),
            m_second(av_frame_alloc())
        {

        }

        ~localPtr()
        {
            if (this->m_first)
                av_frame_free(&this->m_first);

            if (this->m_second)
                av_frame_free(&this->m_second);
        }

        AVFrame *m_first;
        AVFrame *m_second;
    }l;

    AVFrame firstPic;
    AVFrame secondPic;

    *getFirstFrame = false;
    *getSecondFrame = false;

    if (!filter.in || !filter.out)
        return;

    if (av_buffersrc_add_frame_flags(filter.in, frame, AV_BUFFERSRC_FLAG_KEEP_REF) < 0)
        return;

    if (av_buffersink_get_frame(filter.out, l.m_first) < 0)
        return;

    memcpy(firstPic.data, l.m_first->data, sizeof(firstPic.data));
    memcpy(firstPic.linesize, l.m_first->linesize, sizeof(firstPic.linesize));
    av_image_copy(first->data, first->linesize, (const uint8_t**)firstPic.data, firstPic.linesize, (AVPixelFormat)frame->format, frame->width, frame->height);

    *getFirstFrame = true;

    if (av_buffersink_get_frame(filter.out, l.m_second) < 0)
        return;

    memcpy(secondPic.data, l.m_second->data, sizeof(secondPic.data));
    memcpy(secondPic.linesize, l.m_second->linesize, sizeof(secondPic.linesize));
    av_image_copy(second->data, second->linesize, (const uint8_t**)secondPic.data, secondPic.linesize, (AVPixelFormat)frame->format, frame->width, frame->height);

    *getSecondFrame = true;
}

void Deinterlacer::getDeintNameAndParam(QString *preName, QString *preParam, QString *name, QString *param, QString *postName, QString *postParam) const
{
    switch (this->m_algorithm)
    {
        case AnyVODEnums::DA_YADIF:
        {
            *name = "yadif";
            *param = "mode=send_frame:parity=auto:deint=all";

            break;
        }
        case AnyVODEnums::DA_YADIF_BOB:
        {
            *name = "yadif";
            *param = "mode=send_field:parity=auto:deint=all";

            break;
        }
        case AnyVODEnums::DA_W3FDIF:
        {
            *name = "w3fdif";
            *param = "filter=simple:deint=all";

            break;
        }
        case AnyVODEnums::DA_KERNDEINT:
        {
            *name = "kerndeint";
            *param = "thresh=10";

            break;
        }
        case AnyVODEnums::DA_MCDEINT:
        {
            *name = "mcdeint";
            *param = "mode=fast:parity=tff";

            break;
        }
        case AnyVODEnums::DA_BWDIF:
        {
            *name = "bwdif";
            *param = "mode=send_frame:parity=auto:deint=all";

            break;
        }
        case AnyVODEnums::DA_BWDIF_BOB:
        {
            *name = "bwdif";
            *param = "mode=send_field:parity=auto:deint=all";

            break;
        }
        default:
        {
            break;
        }
    }

    if (this->isUseIVTC())
    {
        *preName = "fieldmatch";
        *preParam = "mode=pc_n:order=auto:ppsrc=0:field=auto:mchroma=1:scthresh=12.0:combmatch=full:combdbg=none:cthresh=9:chroma=0";

        *postName = "decimate";
        *postParam = "cycle=5:dupthresh=1.1:scthresh=15:ppsrc=0:chroma=1";
    }
}

bool Deinterlacer::deinterlace(const AVFrame *first, const AVFrame *second, int height, AVPixelFormat format) const
{
    bool doubler = false;

    if (!PixelUtils::is8bitFormat(format))
        return false;

    const AVPixFmtDescriptor *desc = av_pix_fmt_desc_get(format);
    int height2 = AV_CEIL_RSHIFT(height, desc->log2_chroma_h);
    int planeCount = av_pix_fmt_count_planes(format);

    switch (this->m_algorithm)
    {
        case AnyVODEnums::DA_BLEND:
        {
            this->blend(first, height, height2, format, planeCount);
            break;
        }
        case AnyVODEnums::DA_BOB:
        {
            this->bob(first, second, height, height2, format, planeCount);
            doubler = true;

            break;
        }
        default:
        {
            break;
        }
    }

    return doubler;
}

void Deinterlacer::setMethod(AnyVODEnums::DeinterlaceMethod method)
{
    this->m_method = method;
}

void Deinterlacer::setAlgorithm(AnyVODEnums::DeinterlaceAlgorithm algorithm)
{
    this->m_algorithm = algorithm;
}

AnyVODEnums::DeinterlaceMethod Deinterlacer::getMethod() const
{
    return this->m_method;
}

AnyVODEnums::DeinterlaceAlgorithm Deinterlacer::getAlgorithm() const
{
    return this->m_algorithm;
}

void Deinterlacer::useIVTC(bool use)
{
    this->m_useIVTC = use;
}

bool Deinterlacer::isUseIVTC() const
{
    return this->m_useIVTC;
}

void Deinterlacer::blendPlane(uint8_t *data, int linesize, int height) const
{
    for (int y = 0; y < height - 1; y++)
    {
        uint8_t *nextData = data + linesize;

        for (int x = 0; x < linesize; x++)
            data[x] = (data[x] + nextData[x]) / 2;

        data = nextData;
    }
}

void Deinterlacer::splitPicturePlane(const uint8_t * const first, uint8_t *second, int linesize, int height) const
{
    const uint8_t *src = first;
    uint8_t *dst = second;
    int next = linesize * 2;

    src += linesize;
    dst += linesize;

    for (int i = 0; i < height / 2; i++)
    {
        memcpy(dst, src, linesize);

        src += next;
        dst += next;
    }
}

void Deinterlacer::interpolatePlane(uint8_t *ret, int linesize, int height, bool secondPlane) const
{
    uint8_t *first = ret + (secondPlane ? linesize : 0);
    uint8_t *middle = first + linesize;
    uint8_t *last = middle + linesize;

    if (secondPlane)
        memcpy(ret, first, linesize);

    for (int y = 0; y < height / 2 - 1; y++)
    {
        for (int x = 0; x < linesize; x++)
            middle[x] = (first[x] + last[x]) / 2;

        first = last;
        middle = first + linesize;
        last = middle + linesize;
    }

    if (!secondPlane)
        memcpy(middle, first, linesize);
}

void Deinterlacer::blend(const AVFrame *first, int height, int height2, AVPixelFormat format, int planeCount) const
{
#ifdef Q_OS_MAC
    dispatch_group_async(this->m_group, this->m_queue,
                         ^{
                             this->blendPlane(first->data[0], first->linesize[0], height);
                         }
                         );

    if (format != AV_PIX_FMT_GRAY8)
    {
        for (int i = 1; i < planeCount; i++)
        {
            dispatch_group_async(this->m_group, this->m_queue,
                                 ^{
                                     this->blendPlane(first->data[i], first->linesize[i], height2);
                                 }
                                 );
        }
    }

    dispatch_group_wait(this->m_group, DISPATCH_TIME_FOREVER);
#else
    #pragma omp parallel sections
    {
        #pragma omp section
        {
            this->blendPlane(first->data[0], first->linesize[0], height);
        }

        #pragma omp section
        {
            if (format != AV_PIX_FMT_GRAY8)
            {
                #pragma omp parallel for
                for (int i = 1; i < planeCount; i++)
                    this->blendPlane(first->data[i], first->linesize[i], height2);
            }
        }
    }
#endif
}

void Deinterlacer::bob(const AVFrame *first, const AVFrame *second, int height, int height2, AVPixelFormat format, int planeCount) const
{
#ifdef Q_OS_MAC
    dispatch_group_async(this->m_group, this->m_queue,
                         ^{
                             this->splitPicturePlane(first->data[0], second->data[0], first->linesize[0], height);
                             this->interpolatePlane(first->data[0], first->linesize[0], height, false);
                             this->interpolatePlane(second->data[0], second->linesize[0], height, true);
                         }
                         );

    if (format != AV_PIX_FMT_GRAY8)
    {
        for (int i = 1; i < planeCount; i++)
        {
            dispatch_group_async(this->m_group, this->m_queue,
                                 ^{
                                     this->splitPicturePlane(first->data[i], second->data[i], first->linesize[i], height2);
                                     this->interpolatePlane(first->data[i], first->linesize[i], height2, false);
                                     this->interpolatePlane(second->data[i], second->linesize[i], height2, true);
                                 }
                                 );
        }
    }

    dispatch_group_wait(this->m_group, DISPATCH_TIME_FOREVER);
#else
    #pragma omp parallel sections
    {
        #pragma omp section
        {
            this->splitPicturePlane(first->data[0], second->data[0], first->linesize[0], height);
            this->interpolatePlane(first->data[0], first->linesize[0], height, false);
            this->interpolatePlane(second->data[0], second->linesize[0], height, true);
        }

        #pragma omp section
        {
            if (format != AV_PIX_FMT_GRAY8)
            {
                #pragma omp parallel for
                for (int i = 1; i < planeCount; i++)
                {
                    this->splitPicturePlane(first->data[i], second->data[i], first->linesize[i], height2);
                    this->interpolatePlane(first->data[i], first->linesize[i], height2, false);
                    this->interpolatePlane(second->data[i], second->linesize[i], height2, true);
                }
            }
        }
    }
#endif
}
