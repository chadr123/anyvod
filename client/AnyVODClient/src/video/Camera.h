﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <QMatrix4x4>
#include <QVector3D>
#include <QPointF>
#include <QMutex>

class Camera
{
private:
    Camera();

public:
    static Camera& getInstance();

public:
    void lock();
    void unlock();

    void setAngles(const QPointF &angles);
    QPointF getAngles();

    QMatrix4x4 getMatrix();

    void moveLeftRight(qreal value);
    void moveForwardBackward(qreal value);

    void resetAngles();
    void resetPosition();
    QVector3D getPosition();

private:
    void calMatrix();
    void calMove(const QVector3D &base, qreal value);

private:
    QMatrix4x4 m_mat;
    QVector3D m_pos;
    QVector3D m_direction;
    QVector3D m_right;
    QVector3D m_up;
    QPointF m_angles;
    QPointF m_adjustAngles;
    QMutex m_lock;
};
