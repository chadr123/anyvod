﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "PrimaryMatrix.h"

PrimaryMatrix::PrimaryMatrix(const float primaries[3][2], const float whitePoint[2])
{
    float mat[3][3];
    float matInv[3][3];
    float by = this->calcBy(primaries, whitePoint);
    float gy = this->calcGy(primaries, whitePoint, by);
    float ry = this->calcRy(by, gy);

    mat[0][0] = ry * primaries[0][0] / primaries[0][1];
    mat[0][1] = gy * primaries[1][0] / primaries[1][1];
    mat[0][2] = by * primaries[2][0] / primaries[2][1];

    mat[1][0] = ry;
    mat[1][1] = gy;
    mat[1][2] = by;

    mat[2][0] = ry / primaries[0][1] * (1 - primaries[0][0] - primaries[0][1]);
    mat[2][1] = gy / primaries[1][1] * (1 - primaries[1][0] - primaries[1][1]);
    mat[2][2] = by / primaries[2][1] * (1 - primaries[2][0] - primaries[2][1]);

    for (int i = 0; i < 3; ++i)
    {
      for (int j = 0; j < 3; ++j)
          matInv[i][j] = mat[j][i];
    }

    memcpy(this->data(), matInv, sizeof(matInv));
}

QMatrix3x3 PrimaryMatrix::invert() const
{
    float src[3][3];
    float dst[3][3];

    memcpy(src, this->data(), sizeof(src));

    float det = this->calculateDeterminant<3>(src);
    float inverseDet = 1.0f / det;

    for (int j = 0; j < 3; j++)
    {
        for (int i = 0; i < 3; i++)
        {
            float minor = this->calculateMinor<3>(src, j, i);
            float factor = ((i + j) % 2 == 1) ? -1.0f : 1.0f;
            float cofactor = minor * factor;

            dst[i][j] = inverseDet * cofactor;
        }
    }

    float dstInv[3][3];

    for (int i = 0; i < 3; ++i)
    {
      for (int j = 0; j < 3; ++j)
          dstInv[i][j] = dst[j][i];
    }

    return QMatrix3x3((float*)dstInv);
}

float PrimaryMatrix::calcBy(const float p[3][2], const float w[2]) const
{
    float val = ((1 - w[0]) / w[1] - (1 - p[0][0]) / p[0][1]) * (p[1][0] / p[1][1] - p[0][0] / p[0][1]) -
            (w[0] / w[1] - p[0][0] / p[0][1]) * ((1 - p[1][0]) / p[1][1] - (1 - p[0][0]) / p[0][1]);

    val /= ((1 - p[2][0]) / p[2][1] - (1 - p[0][0]) / p[0][1]) * (p[1][0] / p[1][1] - p[0][0] / p[0][1]) -
            (p[2][0] / p[2][1] - p[0][0] / p[0][1]) * ((1 - p[1][0]) / p[1][1] - (1 - p[0][0]) / p[0][1]);

    return val;
}

float PrimaryMatrix::calcGy(const float p[3][2], const float w[2], const float by) const
{
    float val = w[0] / w[1] - p[0][0]/p[0][1] - by * (p[2][0] / p[2][1] - p[0][0] / p[0][1]);

    val /= p[1][0] / p[1][1] - p[0][0] / p[0][1];

    return val;
}

float PrimaryMatrix::calcRy(const float by, const float gy) const
{
    return 1.0 - gy - by;
}

template <int order>
float PrimaryMatrix::calculateDeterminant(float src[order][order]) const
{
    float det = 0.0f;

    for (int i = 0; i < order; i++)
    {
        float minor = this->calculateMinor<order>(src, 0, i);
        float factor = (i % 2 == 1) ? -1.0f : 1.0f;

        det += factor * src[0][i] * minor;
    }

    return det;
}

template <int>
float PrimaryMatrix::calculateDeterminant(float src[2][2]) const
{
    return src[0][0] * src[1][1] - src[0][1] * src[1][0];
}

template <int order>
float PrimaryMatrix::calculateMinor(float src[order][order], int row, int col) const
{
    float sub[order - 1][order - 1];

    this->getSubMatrix<order>(sub, src, row, col);

    return this->calculateDeterminant<order - 1>(sub);
}

template<int order>
void PrimaryMatrix::getSubMatrix(float dest[order - 1][order - 1], float src[order][order], int row, int col) const
{
    int colCount = 0;
    int rowCount = 0;

    for (int i = 0; i < order; i++)
    {
        if (i != row)
        {
            colCount = 0;

            for (int j = 0; j < order; j++)
            {
                if (j != col)
                {
                    dest[rowCount][colCount] = src[i][j];
                    colCount++;
                }
            }

            rowCount++;
        }
    }
}
