﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "Font.h"
#include "core/Common.h"

#include <QVector2D>
#include <QPainter>
#include <QFontMetrics>

#if defined Q_OS_MOBILE
# include <QOpenGLFunctions>
#endif

#include <QDebug>

Font::Font() :
    m_fontSize(-1),
    m_outlineSize(0),
#if defined Q_OS_MOBILE
    m_gl(nullptr),
#endif
    m_textureSize(0),
    m_cacheMode(false)
{
    this->m_outlineTable[0].x = -1;
    this->m_outlineTable[0].y = -1;

    this->m_outlineTable[1].x = 1;
    this->m_outlineTable[1].y = -1;

    this->m_outlineTable[2].x = 1;
    this->m_outlineTable[2].y = 1;

    this->m_outlineTable[3].x = -1;
    this->m_outlineTable[3].y = 1;

    this->m_outlineTable[4].x = -1;
    this->m_outlineTable[4].y = 0;

    this->m_outlineTable[5].x = 0;
    this->m_outlineTable[5].y = -1;

    this->m_outlineTable[6].x = 1;
    this->m_outlineTable[6].y = 0;

    this->m_outlineTable[7].x = 0;
    this->m_outlineTable[7].y = 1;
}

Font::~Font()
{
#if !defined Q_OS_MOBILE
    this->clear();
#endif
}

#if defined Q_OS_MOBILE
void Font::setGL(QOpenGLFunctions *gl)
{
    this->m_gl = gl;
}
#endif

void Font::setOrtho(const QMatrix4x4 &ortho)
{
    this->m_ortho = ortho;
}

void Font::setCacheMode(bool enable)
{
    this->m_cacheMode = enable;
}

bool Font::isCacheMode() const
{
    return this->m_cacheMode;
}

bool Font::willBeInvalidate(int outlineSize) const
{
    return this->m_fontSize != this->m_font.pixelSize() || this->m_outlineSize != outlineSize;
}

QFont& Font::getQFont()
{
    return this->m_font;
}

int Font::renderText(ShaderCompositer::ShaderType type,
                     const QPoint &pos, const QString &text, const Context &context,
                     int outlineSize, const QColor &outlineColor, float opaque,
                     AnyVODEnums::ScaleDirection direction, float scale, bool colorBlend)
{
    ShaderCompositer &shader = ShaderCompositer::getInstance();
    int pixSize = this->m_font.pixelSize();

    if (this->willBeInvalidate(outlineSize))
    {
        this->clear();
        this->createTexture();

        this->m_fontSize = pixSize;
        this->m_outlineSize = outlineSize;
    }

    int len = 0;
    bool enabledBlend = true;
    GLuint texture = 0;
    GLfloat italicX = 0.0f;
    QMatrix4x4 modelView;

    if (!this->m_cacheMode)
    {
        enabledBlend = GL_PREFIX glIsEnabled(GL_BLEND);

        GL_PREFIX glEnable(GL_BLEND);

        if (colorBlend)
            GL_PREFIX glBlendFunc(GL_SRC_COLOR, GL_ONE_MINUS_SRC_COLOR);
        else
            GL_PREFIX glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        modelView.translate(pos.x(), pos.y(), 0.0f);
    }

    if (context.italic)
        italicX = pixSize * 0.4252f;

    if (!this->m_cacheMode)
    {
        QVector4D vColor(context.color.redF(), context.color.greenF(), context.color.blueF(), opaque);

        shader.setRenderData(type, this->m_ortho, modelView, nullptr, nullptr, QRectF(), vColor, AV_PIX_FMT_NONE);
    }

    for (int i = 0; i < text.count(); i++)
    {
        bool reboundTexture = false;
        const Glyph &glyph = this->getGlyph(text[i], context, outlineColor, outlineSize, &reboundTexture);

        if (!this->m_cacheMode)
        {
            int width;
            int height;
            int orgWidth;

            switch (direction)
            {
                case AnyVODEnums::SD_ALL:
                {
                    width = glyph.imgSize.width() * scale;
                    height = glyph.imgSize.height() * scale;
                    orgWidth = glyph.size.width() * scale;

                    break;
                }
                case AnyVODEnums::SD_WIDTH:
                {
                    width = glyph.imgSize.width() * scale;
                    height = glyph.imgSize.height();
                    orgWidth = glyph.size.width() * scale;

                    break;
                }
                case AnyVODEnums::SD_HEIGHT:
                {
                    width = glyph.imgSize.width();
                    height = glyph.imgSize.height() * scale;
                    orgWidth = glyph.size.width();

                    break;
                }
                default:
                {
                    width = glyph.imgSize.width();
                    height = glyph.imgSize.height();
                    orgWidth = glyph.size.width();

                    break;
                }
            }

            if (texture != glyph.texture || reboundTexture)
            {
                texture = glyph.texture;
                GL_PREFIX glBindTexture(GL_TEXTURE_2D, texture);
            }

            QVector3D vertices[] =
            {
                QVector3D(0.0f, height, 0.0f),
                QVector3D(width, height, 0.0f),
                QVector3D(italicX, 0.0f, 0.0f),
                QVector3D(width + italicX, 0.0f, 0.0f)
            };
            QVector2D texCoords[] =
            {
                QVector2D(glyph.texCoord[0].x(), glyph.texCoord[1].y()),
                QVector2D(glyph.texCoord[1].x(), glyph.texCoord[1].y()),
                QVector2D(glyph.texCoord[0].x(), glyph.texCoord[0].y()),
                QVector2D(glyph.texCoord[1].x(), glyph.texCoord[0].y())
            };

            shader.updateRenderData(type, modelView, vertices, texCoords, AV_PIX_FMT_NONE);

            GL_PREFIX glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

            modelView.translate(orgWidth, 0.0f, 0.0f);
        }

        len += glyph.size.width();

        if (context.italic && i == text.count() - 1)
            len += italicX;
    }

    if (!this->m_cacheMode)
    {
       if (!enabledBlend)
            GL_PREFIX glDisable(GL_BLEND);
    }

    GL_PREFIX glBindTexture(GL_TEXTURE_2D, 0);

    return len;
}

void Font::createTexture()
{
    GLuint tex;

    GL_PREFIX glGenTextures(1, &tex);
    GL_PREFIX glBindTexture(GL_TEXTURE_2D, tex);

    GL_PREFIX glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    GL_PREFIX glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    GL_PREFIX glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    GL_PREFIX glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    if (this->m_textureSize <= 0)
    {
        GLint maxSize;

        GL_PREFIX glGetIntegerv(GL_MAX_TEXTURE_SIZE, &maxSize);

        if (maxSize > DEFAULT_MIN_TEXTURE_SIZE)
            maxSize = DEFAULT_MIN_TEXTURE_SIZE;

        this->m_clearValue = QImage(maxSize, maxSize, QImage::Format_ARGB32);
        this->m_clearValue.fill(Qt::transparent);

        this->m_textureSize = maxSize;
    }

    GL_PREFIX glEnable(GL_TEXTURE_2D);
    GL_PREFIX glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
    GL_PREFIX glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, this->m_textureSize, this->m_textureSize, 0, GL_RGBA, GL_UNSIGNED_BYTE, this->m_clearValue.constBits());

    GL_PREFIX glBindTexture(GL_TEXTURE_2D, 0);

    this->m_textures.append(tex);
}

void Font::drawOutlinedGlyph(QPainter &painter, int x, int y, const QChar &c, const QColor &outlineColor, int outline) const
{
    painter.setPen(outlineColor);

    for (int i = 0; i < 8; i++)
    {
        QPoint point(x + this->m_outlineTable[i].x * outline, y + this->m_outlineTable[i].y * outline);

        painter.drawText(point, c);
    }

    painter.setPen(Qt::white);
    painter.drawText(x, y, c);
}

const Font::Glyph& Font::getGlyph(const QChar &c, const Context &key, const QColor &outlineColor, int outlineSize, bool *reboundTexture)
{
    QHash<Context, QHash<QChar, Glyph> >::iterator i = this->m_glyphs.find(key);

    if (i != this->m_glyphs.end())
    {
        QHash<QChar, Glyph> &glyphs = i.value();
        QHash<QChar, Glyph>::iterator j = glyphs.find(c);

        *reboundTexture = false;

        if (j != glyphs.end())
            return j.value();
    }

    this->m_font.setBold(key.bold);

    const int outlineMargin = 2;
    QPainter imgPainter;
    QFontMetrics fm(this->m_font);
    int outlineTexSize = outlineSize * 2 * outlineMargin;
    int width = fm.horizontalAdvance(c);
    int height = fm.height();
    int imgWidth = width + outlineTexSize;
    int imgHeight = height + outlineTexSize;
    QImage img(imgWidth, imgHeight, QImage::Format_ARGB32);

    img.fill(Qt::transparent);

    imgPainter.begin(&img);

    imgPainter.setRenderHint(QPainter::Antialiasing);
    imgPainter.setFont(this->m_font);

    this->drawOutlinedGlyph(imgPainter, outlineSize, fm.ascent() + outlineSize, c, outlineColor, outlineSize);

    imgPainter.setRenderHint(QPainter::Antialiasing, false);

    if (key.underline || key.strike)
        imgPainter.setPen(QPen(Qt::white, fm.lineWidth()));

    if (key.underline)
    {
        int yPos = imgHeight - fm.lineWidth() - 1;

        imgPainter.drawLine(QPoint(0, yPos), QPoint(imgWidth, yPos));
    }

    if (key.strike)
    {
        int yPos = imgHeight - fm.lineWidth() - imgHeight / 2;

        imgPainter.drawLine(QPoint(0, yPos), QPoint(imgWidth, yPos));
    }

    imgPainter.end();

    if (this->m_texPos.x() + imgWidth > this->m_textureSize)
    {
        this->m_texPos.rx() = 0;
        this->m_texPos.ry() += imgHeight;
    }

    if (this->m_texPos.y() + imgHeight > this->m_textureSize)
    {
        this->createTexture();
        this->m_texPos.rx() = 0;
        this->m_texPos.ry() = 0;
    }

    GLuint tex = this->m_textures.last();

    GL_PREFIX glBindTexture(GL_TEXTURE_2D, tex);

    GL_PREFIX glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
    GL_PREFIX glTexSubImage2D(GL_TEXTURE_2D, 0, this->m_texPos.x(), this->m_texPos.y(), imgWidth, imgHeight, GL_RGBA, GL_UNSIGNED_BYTE, img.constBits());

    GL_PREFIX glBindTexture(GL_TEXTURE_2D, 0);

    QHash<QChar, Glyph> &glyphs = this->m_glyphs[key];
    Glyph &glyph = glyphs[c];

    glyph.texture = tex;
    glyph.size = QSize(width, height);
    glyph.imgSize = QSize(imgWidth, imgHeight);

    glyph.texCoord[0].setX((qreal)this->m_texPos.x() / this->m_textureSize);
    glyph.texCoord[0].setY((qreal)this->m_texPos.y() / this->m_textureSize);

    glyph.texCoord[1].setX((qreal)(this->m_texPos.x() + imgWidth) / this->m_textureSize);
    glyph.texCoord[1].setY((qreal)(this->m_texPos.y() + imgHeight) / this->m_textureSize);

    this->m_texPos.rx() += imgWidth;

    *reboundTexture = true;

    return glyph;
}

void Font::clear()
{
    for (int i = 0; i < this->m_textures.count(); i++)
        GL_PREFIX glDeleteTextures(1, &this->m_textures[i]);

    this->m_textures.clear();
    this->m_glyphs.clear();
    this->m_fontSize = 0;
    this->m_texPos = QPoint(0, 0);
}
