﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "core/AnyVODEnums.h"
#include "ColorConversion.h"

#include <QHash>

extern "C"
{
# include <libavutil/pixfmt.h>
}

class QMatrix4x4;
class QVector2D;
class QOpenGLShaderProgram;

class ShaderCompositer
{
private:
    ShaderCompositer();
    ~ShaderCompositer();

public:
    enum ShaderType
    {
        ST_SCREEN = -1,
        ST_SIMPLE = 0,
        ST_SUBTITLE_INTERLACE,
        ST_SUBTITLE_CHECKER_BOARD,
        ST_SUBTITLE_ANAGLYPH,
        ST_BARREL_DISTORTION,
        ST_PINCUSHION_DISTORTION,
        ST_LINE,
        ST_COUNT
    };

public:
    static ShaderCompositer& getInstance();

public:
    void initShaders();
    void deInitShaders();

    void setRenderData(ShaderType type, const QMatrix4x4 &proj, const QMatrix4x4 &modelView,
                       const QVector3D *vertices, const QVector2D *texCoords, const QRectF &renderRect,
                       const QVector4D &color, AVPixelFormat pixFormat);
    void updateRenderData(ShaderType type, const QMatrix4x4 &modelView,
                          const QVector3D *vertices, const QVector2D *texCoords, AVPixelFormat pixFormat);

    void setTextureSampler(ShaderType type, int index, AVPixelFormat pixFormat);

    bool startScreen(const QSizeF &surfaceSize, const QSizeF &screenSize,
                     double clock, double lumAvg, AVPixelFormat format, const FrameMetaData &frameMeta);
    void endScreen(AVPixelFormat pixFormat);

    bool startSimple();
    void endSimple();

    bool startLine();
    void endLine();

    bool startSubtitleInterlace(bool firstFrame);
    void endSubtitleInterlace();

    bool startSubtitleCheckerBoard(bool firstFrame);
    void endSubtitleCheckerBoard();

    bool startSubtitleAnaglyph(bool firstFrame);
    void endSubtitleAnaglyph();

    bool startBarrelDistortion(const QVector2D &texRange, const QVector2D &lensCenterOffset,
                               const QVector2D &coefficients, float fillScale);
    void endBarrelDistortion();

    bool startPincushionDistortion(const QVector2D &texRange, const QVector2D &lensCenterOffset,
                                   const QVector2D &coefficients, float fillScale);
    void endPincushionDistortion();

    bool addShader(const QString &filePath);
    bool deleteShader(const QString &filePath);
    void clearShaders();

    void getShaderList(QStringList *ret);
    void getFragmentSource(QString *ret) const;
    void getVertexSource(QString *ret) const;

    bool build();
    void getLog(QString *ret) const;

    void setHue(double hue);
    double getHue() const;

    void setSaturation(double saturation);
    double getSaturation() const;

    void setContrast(double contrast);
    double getContrast() const;

    void setBrightness(double brightness);
    double getBrightness() const;

    void useSharply(bool use);
    bool isUsingSharply() const;

    void useSharpen(bool use);
    bool isUsingSharpen() const;

    void useSoften(bool use);
    bool isUsingSoften() const;

    void useLeftRightInvert(bool use);
    bool isUsingLeftRightInvert() const;

    void useTopBottomInvert(bool use);
    bool isUsingTopBottomInvert() const;

    void use360Degree(bool use);
    bool is360Degree() const;

    void useColorConversion(bool use);
    bool isUsingColorConversion() const;

    void set3DMethod(AnyVODEnums::Video3DMethod method3D);
    AnyVODEnums::Video3DMethod get3DMethod() const;

    void setAnaglyphAlgorithm(AnyVODEnums::AnaglyphAlgorithm algoritm);
    AnyVODEnums::AnaglyphAlgorithm getAnaglyphAlgorithm() const;

    void setVideo360Type(AnyVODEnums::Video360Type type);
    AnyVODEnums::Video360Type getVideo360Type() const;

    bool canUseAnaglyphAlgorithm() const;

    void setup3D(bool sideBySide, bool leftOrTop);

    void getAnaglyphMatrix(float matFirst[3][3], float matSecond[3][3]) const;

    bool getLeftOrTop() const;
    bool isCubeMap() const;

private:
    enum VertexShaderLocation
    {
        VSL_VERTEX,
        VSL_TEXTURE_COORD,
        VSL_COUNT
    };

    struct DefaultParam
    {
        DefaultParam()
        {
            hue = 0.0;
            saturation = 1.0;
            contrast = 1.0;
            brightness = 1.0;
            useSharply = false;
            useSharpen = false;
            useSoften = false;
            useLeftRightInvert = false;
            useTopBottomInvert = false;
            interlaced = false;
            leftOrTop = false;
            rowOrCol = false;
            sideBySide = false;
            anaglyph = false;
            checker = false;
            use360Degree = false;
            useColorConversion = false;
            method3D = AnyVODEnums::V3M_NONE;
            anaglyphAlgorithm = AnyVODEnums::AGA_DEFAULT;
            video360Type = AnyVODEnums::V3T_EQR;
        }

        double hue;
        double saturation;
        double contrast;
        double brightness;
        bool useSharply;
        bool useSharpen;
        bool useSoften;
        bool useLeftRightInvert;
        bool useTopBottomInvert;
        bool interlaced;
        bool leftOrTop;
        bool rowOrCol;
        bool sideBySide;
        bool anaglyph;
        bool checker;
        bool use360Degree;
        bool useColorConversion;
        AnyVODEnums::Video3DMethod method3D;
        AnyVODEnums::AnaglyphAlgorithm anaglyphAlgorithm;
        AnyVODEnums::Video360Type video360Type;
    };

    struct ShaderItem
    {
        QString path;
        QString content;
    };

private:
    void makeEntry(QString *ret) const;
    void addEntry(const QString &entry, QString *ret) const;
    void getFuncs(QString *ret) const;

    bool startShader(ShaderType type);
    void endShader(ShaderType type);

    int findShader(const QString &path) const;
    QOpenGLShaderProgram* getShader(ShaderType type) const;
    QOpenGLShaderProgram* getShader(ShaderType type, AVPixelFormat pixFormat) const;

    void setRenderData(QOpenGLShaderProgram *shader, const QMatrix4x4 &proj, const QMatrix4x4 &modelView,
                       const QVector3D *vertices, const QVector2D *texCoords, const QRectF &renderRect,
                       const QVector4D &color);
    void updateRenderData(QOpenGLShaderProgram *shader, const QMatrix4x4 &modelView, const QVector3D *vertices, const QVector2D *texCoords);

    void unsetRenderData(QOpenGLShaderProgram *shader);
    void setTextureSampler(QOpenGLShaderProgram *shader, int index);
    bool buildShader(QOpenGLShaderProgram *shader, const QString &code);
    bool buildShader(QOpenGLShaderProgram *shader, const QString &code, bool screen);
    bool appendScreenShader(const QString &base, AVPixelFormat format);
    void clearScreenShaders();
    QOpenGLShaderProgram* findScreenShader(AVPixelFormat format) const;

    QString applyPixelDecoder(const QString &code) const;
    QString applyPixelDecoder(const QString &code, AVPixelFormat format) const;
    QString applyTexCoordDecoder(const QString &code, bool screen) const;

    void destroyShader(QOpenGLShaderProgram **object);

private:
    QHash<AVPixelFormat, QOpenGLShaderProgram*> m_shaderScreens;
    QOpenGLShaderProgram *m_shaders[ST_COUNT];
    QList<ShaderItem> m_shaderSources;
    DefaultParam m_defaultParam;
    QString m_fragmentSource;
    QString m_vertexSource;
    QVector3D m_coefsDst;
    ColorConversion::Info m_colorconversionInfo;

private:
    static const QString ENTRY_HEADER;
    static const QString ENTRY_FOOTER;
};
