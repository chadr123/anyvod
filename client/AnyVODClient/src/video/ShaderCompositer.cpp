﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "ShaderCompositer.h"
#include "core/Common.h"
#include "Sphere.h"
#include "Cube.h"
#include "BaseShaderPrograms.h"
#include "AnaglyphMatrix.h"

#include <QMatrix4x4>
#include <QVector2D>
#include <QOpenGLShaderProgram>
#include <QFile>
#include <QDebug>

const QString ShaderCompositer::ENTRY_HEADER = "  ";
const QString ShaderCompositer::ENTRY_FOOTER = "(texel, coord);\n";

ShaderCompositer::ShaderCompositer()
{
    Sphere::init();

    if (this->isCubeMap())
        Cube::init(this->m_defaultParam.video360Type);

    this->deInitShaders();
}

ShaderCompositer::~ShaderCompositer()
{
    this->deInitShaders();

    Sphere::deInit();
    Cube::deInit();
}

ShaderCompositer& ShaderCompositer::getInstance()
{
    static ShaderCompositer instance;

    return instance;
}

void ShaderCompositer::initShaders()
{
    for (int i = 0; i < ST_COUNT; i++)
        this->m_shaders[i] = new QOpenGLShaderProgram;
}

void ShaderCompositer::deInitShaders()
{
    for (int i = 0; i < ST_COUNT; i++)
        this->destroyShader(&this->m_shaders[i]);

    this->clearScreenShaders();
}

bool ShaderCompositer::getLeftOrTop() const
{
    return this->m_defaultParam.leftOrTop;
}

bool ShaderCompositer::startScreen(const QSizeF &surfaceSize, const QSizeF &screenSize,
                                   double clock, double lumAvg, AVPixelFormat format, const FrameMetaData &frameMeta)
{
    bool success = false;
    QOpenGLShaderProgram *shader = this->findScreenShader(format);

    if (shader && shader->isLinked())
        success = shader->bind();

    if (success)
    {
#if !defined Q_OS_RASPBERRY_PI
        float satMore;

        if (this->m_defaultParam.useColorConversion)
            satMore = 0.3f;
        else
            satMore = 0.0f;

        shader->setUniformValue("AnyVOD_Hue", (float)this->m_defaultParam.hue);
        shader->setUniformValue("AnyVOD_Saturation", (float)this->m_defaultParam.saturation + satMore);
        shader->setUniformValue("AnyVOD_Contrast", (float)this->m_defaultParam.contrast);
        shader->setUniformValue("AnyVOD_Brightness", (float)this->m_defaultParam.brightness);
#endif
        shader->setUniformValue("AnyVOD_TexWidth", (float)surfaceSize.width());
        shader->setUniformValue("AnyVOD_TexHeight", (float)surfaceSize.height());
        shader->setUniformValue("AnyVOD_Width", (float)surfaceSize.width());
        shader->setUniformValue("AnyVOD_Height", (float)surfaceSize.height());
        shader->setUniformValue("AnyVOD_Clock", (float)clock);
        shader->setUniformValue("AnyVOD_LumAvg", QVector3D(lumAvg, lumAvg, lumAvg));

        shader->setUniformValue("AnyVOD_useLeftRightInvert", this->m_defaultParam.useLeftRightInvert);
        shader->setUniformValue("AnyVOD_useTopBottomInvert", this->m_defaultParam.useTopBottomInvert);

        QVector2D screenCoord = QVector2D(screenSize.width(), screenSize.height());

        if (this->m_defaultParam.interlaced)
        {
            shader->setUniformValue("AnyVOD_leftOrTop", this->m_defaultParam.leftOrTop);
            shader->setUniformValue("AnyVOD_rowOrCol", this->m_defaultParam.rowOrCol);
            shader->setUniformValue("AnyVOD_sideBySide", this->m_defaultParam.sideBySide);
            shader->setUniformValue("AnyVOD_screenCoord", screenCoord);
        }

        if (this->m_defaultParam.checker)
        {
            shader->setUniformValue("AnyVOD_leftOrTop", this->m_defaultParam.leftOrTop);
            shader->setUniformValue("AnyVOD_sideBySide", this->m_defaultParam.sideBySide);
            shader->setUniformValue("AnyVOD_screenCoord", screenCoord);
        }

        if (this->m_defaultParam.anaglyph)
        {
            shader->setUniformValue("AnyVOD_leftOrTop", this->m_defaultParam.leftOrTop);
            shader->setUniformValue("AnyVOD_sideBySide", this->m_defaultParam.sideBySide);
            shader->setUniformValue("AnyVOD_screenCoord", screenCoord);

            QPair<QMatrix3x3, QMatrix3x3> matPair = AnaglyphMatrix::getMatrix(this->m_defaultParam.method3D,
                                                                              this->m_defaultParam.anaglyphAlgorithm);

            shader->setUniformValue("AnyVOD_anaglyphFirstMatrix", matPair.first);
            shader->setUniformValue("AnyVOD_anaglyphSecondMatrix", matPair.second);
        }

        if (this->m_defaultParam.useColorConversion)
        {
            shader->setUniformValue("AnyVOD_primaryMatrix", this->m_colorconversionInfo.matPrimary);
            shader->setUniformValue("AnyVOD_gammaSrc", this->m_colorconversionInfo.gammaSrc);
            shader->setUniformValue("AnyVOD_gammaDstInv", 1.0f / this->m_colorconversionInfo.gammaDst);

            shader->setUniformValue("AnyVOD_coefsDst", this->m_coefsDst);
            shader->setUniformValue("AnyVOD_toneParam", ColorConversion::getTone(frameMeta));
        }
    }

    return success;
}

void ShaderCompositer::setRenderData(ShaderCompositer::ShaderType type, const QMatrix4x4 &proj, const QMatrix4x4 &modelView,
                                     const QVector3D *vertices, const QVector2D *texCoords, const QRectF &renderRect,
                                     const QVector4D &color, AVPixelFormat pixFormat)
{
    QOpenGLShaderProgram *shader = this->getShader(type, pixFormat);

    if (!shader)
        return;

    this->setRenderData(shader, proj, modelView, vertices, texCoords, renderRect, color);
}

void ShaderCompositer::updateRenderData(ShaderCompositer::ShaderType type, const QMatrix4x4 &modelView,
                                        const QVector3D *vertices, const QVector2D *texCoords, AVPixelFormat pixFormat)
{
    QOpenGLShaderProgram *shader = this->getShader(type, pixFormat);

    if (!shader)
        return;

    this->updateRenderData(shader, modelView, vertices, texCoords);
}

void ShaderCompositer::setTextureSampler(ShaderCompositer::ShaderType type, int index, AVPixelFormat pixFormat)
{
    QOpenGLShaderProgram *shader = this->getShader(type, pixFormat);

    if (!shader)
        return;

    this->setTextureSampler(shader, index);
}

void ShaderCompositer::endScreen(AVPixelFormat pixFormat)
{
    QOpenGLShaderProgram *shader = this->findScreenShader(pixFormat);

    this->unsetRenderData(shader);
    shader->release();
}

bool ShaderCompositer::startSimple()
{
    return this->startShader(ST_SIMPLE);
}

void ShaderCompositer::endSimple()
{
    this->endShader(ST_SIMPLE);
}

bool ShaderCompositer::startLine()
{
    return this->startShader(ST_LINE);
}

void ShaderCompositer::endLine()
{
    this->endShader(ST_LINE);
}

bool ShaderCompositer::startSubtitleInterlace(bool firstFrame)
{
    bool success = this->startShader(ST_SUBTITLE_INTERLACE);

    if (success)
    {
        QOpenGLShaderProgram *shader = this->getShader(ST_SUBTITLE_INTERLACE);

        shader->setUniformValue("AnyVOD_firstFrame", firstFrame);
        shader->setUniformValue("AnyVOD_rowOrCol", this->m_defaultParam.rowOrCol);
    }

    return success;
}

void ShaderCompositer::endSubtitleInterlace()
{
    this->endShader(ST_SUBTITLE_INTERLACE);
}

bool ShaderCompositer::startSubtitleCheckerBoard(bool firstFrame)
{
    bool success = this->startShader(ST_SUBTITLE_CHECKER_BOARD);

    if (success)
        this->getShader(ST_SUBTITLE_CHECKER_BOARD)->setUniformValue("AnyVOD_firstFrame", firstFrame);

    return success;
}

void ShaderCompositer::endSubtitleCheckerBoard()
{
    this->endShader(ST_SUBTITLE_CHECKER_BOARD);
}

bool ShaderCompositer::startSubtitleAnaglyph(bool firstFrame)
{
    bool success = this->startShader(ST_SUBTITLE_ANAGLYPH);

    if (success)
    {
        QPair<QMatrix3x3, QMatrix3x3> matPair = AnaglyphMatrix::getMatrix(this->m_defaultParam.method3D,
                                                                          this->m_defaultParam.anaglyphAlgorithm);


        this->getShader(ST_SUBTITLE_ANAGLYPH)->setUniformValue("AnyVOD_anaglyphMatrix", firstFrame ? matPair.second : matPair.first);
    }

    return success;
}

void ShaderCompositer::endSubtitleAnaglyph()
{
    this->endShader(ST_SUBTITLE_ANAGLYPH);
}

bool ShaderCompositer::startBarrelDistortion(const QVector2D &texRange, const QVector2D &lensCenterOffset,
                                             const QVector2D &coefficients, float fillScale)
{
    bool success = this->startShader(ST_BARREL_DISTORTION);

    if (success)
    {
        QOpenGLShaderProgram *shader = this->getShader(ST_BARREL_DISTORTION);

        shader->setUniformValue("AnyVOD_texRange", texRange);
        shader->setUniformValue("AnyVOD_lensCenterOffset", lensCenterOffset);
        shader->setUniformValue("AnyVOD_coefficients", coefficients);
        shader->setUniformValue("AnyVOD_fillScale", fillScale);
    }

    return success;
}

void ShaderCompositer::endBarrelDistortion()
{
    this->endShader(ST_BARREL_DISTORTION);
}

bool ShaderCompositer::startPincushionDistortion(const QVector2D &texRange, const QVector2D &lensCenterOffset, const QVector2D &coefficients, float fillScale)
{
    bool success = this->startShader(ST_PINCUSHION_DISTORTION);

    if (success)
    {
        QOpenGLShaderProgram *shader = this->getShader(ST_PINCUSHION_DISTORTION);

        shader->setUniformValue("AnyVOD_texRange", texRange);
        shader->setUniformValue("AnyVOD_lensCenterOffset", lensCenterOffset);
        shader->setUniformValue("AnyVOD_coefficients", coefficients);
        shader->setUniformValue("AnyVOD_fillScale", fillScale);
    }

    return success;
}

void ShaderCompositer::endPincushionDistortion()
{
    this->endShader(ST_PINCUSHION_DISTORTION);
}

bool ShaderCompositer::addShader(const QString &filePath)
{
    if (this->findShader(filePath) >= 0)
        return false;

    QFile file(filePath);

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return false;

    QTextStream stream(&file);
    ShaderItem item;

    item.path = filePath;
    item.content = stream.readAll();

    this->m_shaderSources.append(item);

    return true;
}

bool ShaderCompositer::deleteShader(const QString &filePath)
{
    int index = this->findShader(filePath);

    if (index < 0)
        return false;

    this->m_shaderSources.removeAt(index);

    return true;
}

void ShaderCompositer::clearShaders()
{
    this->m_shaderSources.clear();
}

void ShaderCompositer::getShaderList(QStringList *ret)
{
    for (int i = 0; i < this->m_shaderSources.count(); i++)
        ret->append(this->m_shaderSources[i].path);
}

void ShaderCompositer::getFragmentSource(QString *ret) const
{
    *ret = this->m_fragmentSource;
}

void ShaderCompositer::getVertexSource(QString *ret) const
{
    *ret = this->m_vertexSource;
}

bool ShaderCompositer::build()
{
    QString entries;
    QString source = BaseShaderPrograms::getShaderFrame();
    QString funcs;
    bool success = true;

    this->makeEntry(&entries);
    this->getFuncs(&funcs);

    funcs.append(BaseShaderPrograms::getEffects());
    funcs.append(BaseShaderPrograms::getDefaultLibrary());

    if (this->m_defaultParam.interlaced)
        funcs.append(BaseShaderPrograms::getInterlace());

    if (this->m_defaultParam.checker)
        funcs.append(BaseShaderPrograms::getCheckerBoard());

    if (this->m_defaultParam.anaglyph)
        funcs.append(BaseShaderPrograms::getAnaglyph());

    source.replace(BaseShaderPrograms::FUNCS_SIG, funcs);
    source.replace(BaseShaderPrograms::ENTRY_SIG, entries);

    this->m_fragmentSource = this->applyPixelDecoder(source, AV_PIX_FMT_NONE);
    this->m_fragmentSource = this->applyTexCoordDecoder(this->m_fragmentSource, true);

    this->m_vertexSource = BaseShaderPrograms::getVertexFrame();

    this->clearScreenShaders();

    success &= this->appendScreenShader(source, AV_PIX_FMT_NONE);
    success &= this->appendScreenShader(source, AV_PIX_FMT_YUV420P);
    success &= this->appendScreenShader(source, AV_PIX_FMT_NV12);
    success &= this->appendScreenShader(source, AV_PIX_FMT_NV21);
    success &= this->appendScreenShader(source, AV_PIX_FMT_YUYV422);
    success &= this->appendScreenShader(source, AV_PIX_FMT_UYVY422);
    success &= this->appendScreenShader(source, AV_PIX_FMT_YVYU422);
    success &= this->appendScreenShader(source, AV_PIX_FMT_P010);
    success &= this->appendScreenShader(source, AV_PIX_FMT_P016);
    success &= this->appendScreenShader(source, AV_PIX_FMT_YUV420P9);
    success &= this->appendScreenShader(source, AV_PIX_FMT_YUV420P10);
    success &= this->appendScreenShader(source, AV_PIX_FMT_YUV420P12);
    success &= this->appendScreenShader(source, AV_PIX_FMT_YUV420P14);
    success &= this->appendScreenShader(source, AV_PIX_FMT_YUV420P16);

    success &= this->buildShader(this->getShader(ST_SIMPLE), this->applyPixelDecoder(BaseShaderPrograms::getSimple()));

    this->buildShader(this->getShader(ST_SUBTITLE_INTERLACE), this->applyPixelDecoder(BaseShaderPrograms::getSubtitleInterlace()));
    this->buildShader(this->getShader(ST_SUBTITLE_CHECKER_BOARD), this->applyPixelDecoder(BaseShaderPrograms::getSubtitleCheckerBoard()));
    this->buildShader(this->getShader(ST_SUBTITLE_ANAGLYPH), this->applyPixelDecoder(BaseShaderPrograms::getSubtitleAnaglyph()));
    this->buildShader(this->getShader(ST_BARREL_DISTORTION), this->applyPixelDecoder(BaseShaderPrograms::getBarrelDistortion()));
    this->buildShader(this->getShader(ST_PINCUSHION_DISTORTION), this->applyPixelDecoder(BaseShaderPrograms::getPincushionDistortion()));
    this->buildShader(this->getShader(ST_LINE), this->applyPixelDecoder(BaseShaderPrograms::getLine()));

    if (this->m_defaultParam.useColorConversion)
    {
        this->m_colorconversionInfo = ColorConversion::getColorConversionInfo(AVCOL_PRI_BT2020, AVCOL_PRI_BT709);
        this->m_coefsDst = ColorConversion::getRGBYuvCoefs(AVCOL_SPC_BT709);
    }

    return success;
}

void ShaderCompositer::getLog(QString *ret) const
{
    *ret = this->findScreenShader(AV_PIX_FMT_NONE)->log();
}

void ShaderCompositer::addEntry(const QString &entry, QString *ret) const
{
    ret->append(ENTRY_HEADER);
    ret->append(entry);
    ret->append(ENTRY_FOOTER);
}

void ShaderCompositer::getFuncs(QString *ret) const
{
    for (int i = 0; i < this->m_shaderSources.count(); i++)
        ret->append(this->m_shaderSources[i].content + "\n");
}

bool ShaderCompositer::startShader(ShaderCompositer::ShaderType type)
{
    bool success = false;
    QOpenGLShaderProgram *shader = this->getShader(type);

    if (!shader)
        return success;

    if (shader->isLinked())
        success = shader->bind();

    return success;
}

void ShaderCompositer::endShader(ShaderCompositer::ShaderType type)
{
    QOpenGLShaderProgram *shader = this->getShader(type);

    if (!shader)
        return;

    this->unsetRenderData(shader);
    shader->release();
}

int ShaderCompositer::findShader(const QString &path) const
{
    for (int i = 0; i < this->m_shaderSources.count(); i++)
    {
        if (this->m_shaderSources[i].path == path)
            return i;
    }

    return -1;
}

QOpenGLShaderProgram* ShaderCompositer::getShader(ShaderCompositer::ShaderType type) const
{
    return this->getShader(type, AV_PIX_FMT_NONE);
}

QOpenGLShaderProgram* ShaderCompositer::getShader(ShaderCompositer::ShaderType type, AVPixelFormat pixFormat) const
{
    if (type == ST_SCREEN)
        return this->findScreenShader(pixFormat);
    else
        return this->m_shaders[type];
}

void ShaderCompositer::setRenderData(QOpenGLShaderProgram *shader, const QMatrix4x4 &proj, const QMatrix4x4 &modelView,
                                     const QVector3D *vertices, const QVector2D *texCoords, const QRectF &renderRect,
                                     const QVector4D &color)
{
    shader->setUniformValue("AnyVOD_projection", proj);
    shader->setUniformValue("AnyVOD_inColor", color);

    if (!renderRect.isNull())
        shader->setUniformValue("AnyVOD_RenderRect", QVector4D(renderRect.left(), renderRect.top(), renderRect.width(), renderRect.height()));

    this->updateRenderData(shader, modelView, vertices, texCoords);
}

void ShaderCompositer::updateRenderData(QOpenGLShaderProgram *shader, const QMatrix4x4 &modelView,
                                        const QVector3D *vertices, const QVector2D *texCoords)
{
    shader->setUniformValue("AnyVOD_modelView", modelView);

    if (vertices)
    {
        shader->enableAttributeArray(VSL_VERTEX);
        shader->setAttributeArray(VSL_VERTEX, vertices);
    }

    if (texCoords)
    {
        shader->enableAttributeArray(VSL_TEXTURE_COORD);
        shader->setAttributeArray(VSL_TEXTURE_COORD, texCoords);
    }
}

void ShaderCompositer::unsetRenderData(QOpenGLShaderProgram *shader)
{
    shader->disableAttributeArray(VSL_VERTEX);
    shader->disableAttributeArray(VSL_TEXTURE_COORD);
}

void ShaderCompositer::setTextureSampler(QOpenGLShaderProgram *shader, int index)
{
    QString name = "AnyVOD_tex" + QString::number(index);
    int loc = shader->uniformLocation(name);

    shader->setUniformValue(loc, index);
}

bool ShaderCompositer::buildShader(QOpenGLShaderProgram *shader, const QString &code)
{
    return this->buildShader(shader, code, false);
}

bool ShaderCompositer::buildShader(QOpenGLShaderProgram *shader, const QString &code, bool screen)
{
    QString fregment = this->applyTexCoordDecoder(code, screen);

    shader->removeAllShaders();

    if (!shader->addCacheableShaderFromSourceCode(QOpenGLShader::Vertex, this->m_vertexSource))
        return false;

    shader->bindAttributeLocation("AnyVOD_vertex", VSL_VERTEX);

    if (!this->isCubeMap())
        shader->bindAttributeLocation("AnyVOD_inTexCoords", VSL_TEXTURE_COORD);

    if (!shader->addCacheableShaderFromSourceCode(QOpenGLShader::Fragment, fregment))
        return false;

    return shader->link();
}

QString ShaderCompositer::applyPixelDecoder(const QString &code, AVPixelFormat format) const
{
    QString base = code;

    base.replace(BaseShaderPrograms::PIXEL_DECODER_SIG, BaseShaderPrograms::getPixelDecoder(format));

    return base;
}

QString ShaderCompositer::applyTexCoordDecoder(const QString &code, bool screen) const
{
    QString base = code;

    if (this->isCubeMap() && screen)
        base.replace(BaseShaderPrograms::TEX_COORD_DECODER_SIG, BaseShaderPrograms::getTexCoordDecoder(this->m_defaultParam.video360Type));
    else
        base.replace(BaseShaderPrograms::TEX_COORD_DECODER_SIG, BaseShaderPrograms::getDefaultTexCoordDecoder());

    return base;
}

void ShaderCompositer::destroyShader(QOpenGLShaderProgram **object)
{
    if (*object)
    {
        delete *object;
        *object = nullptr;
    }
}

bool ShaderCompositer::appendScreenShader(const QString &base, AVPixelFormat format)
{
    QOpenGLShaderProgram *shader = new QOpenGLShaderProgram;
    bool success;

    success = this->buildShader(shader, this->applyPixelDecoder(base, format), true);

    if (success)
        this->m_shaderScreens[format] = shader;
    else
        delete shader;

    return success;
}

void ShaderCompositer::clearScreenShaders()
{
    for (const QOpenGLShaderProgram *shader : qAsConst(this->m_shaderScreens))
        delete shader;

    this->m_shaderScreens.clear();
}

QOpenGLShaderProgram* ShaderCompositer::findScreenShader(AVPixelFormat format) const
{
    QOpenGLShaderProgram *shader = this->m_shaderScreens[format];

    if (!shader)
        shader = this->m_shaderScreens[AV_PIX_FMT_NONE];

    return shader;
}

QString ShaderCompositer::applyPixelDecoder(const QString &code) const
{
    return this->applyPixelDecoder(code, AV_PIX_FMT_NONE);
}

bool ShaderCompositer::isCubeMap() const
{
    if (!this->is360Degree())
        return false;

    switch (this->m_defaultParam.video360Type)
    {
        case AnyVODEnums::V3T_CUBEMAP_2X3:
        case AnyVODEnums::V3T_EAC_2X3:
        case AnyVODEnums::V3T_EAC_3X2:
            return true;
        default:
            return false;
    }
}

void ShaderCompositer::makeEntry(QString *ret) const
{
    QStringList shaders;

    if (this->m_defaultParam.useColorConversion)
        this->addEntry("AnyVOD_colorConversion", ret);

    if (this->m_defaultParam.interlaced)
        this->addEntry("AnyVOD_interlace3D", ret);

    if (this->m_defaultParam.checker)
        this->addEntry("AnyVOD_checkerBoard", ret);

    if (this->m_defaultParam.useLeftRightInvert || this->m_defaultParam.useTopBottomInvert)
        this->addEntry("AnyVOD_invert", ret);

    if (this->m_defaultParam.use360Degree)
        this->addEntry("AnyVOD_use360Degree", ret);

    for (int i = 0; i < this->m_shaderSources.count(); i++)
        shaders.append(this->m_shaderSources[i].content);

    for (int i = 0; i < shaders.count(); i++)
    {
        QString &shader = shaders[i];
        QTextStream stream(&shader);

        while (!stream.atEnd())
        {
            const QString line = stream.readLine().trimmed();

            if (!line.startsWith(COMMENT_PREFIX))
                continue;

            const QString entrySig = "entry";
            QString content = line;

            content = content.remove(COMMENT_PREFIX).trimmed();

            if (!content.startsWith(entrySig, Qt::CaseInsensitive))
                continue;

            int index = line.indexOf(entrySig);

            if (index == -1)
                continue;

            this->addEntry(line.mid(index + entrySig.count()).trimmed(), ret);
        }
    }

    if (this->m_defaultParam.useSharply)
        this->addEntry("AnyVOD_sharply", ret);

    if (this->m_defaultParam.useSharpen)
        this->addEntry("AnyVOD_sharpen", ret);

    if (this->m_defaultParam.useSoften)
        this->addEntry("AnyVOD_soften", ret);

#if !defined Q_OS_RASPBERRY_PI
    this->addEntry("AnyVOD_hue", ret);
    this->addEntry("AnyVOD_saturation_contrast", ret);
    this->addEntry("AnyVOD_brightness", ret);
#endif

    if (this->m_defaultParam.anaglyph)
        this->addEntry("AnyVOD_anaglyph", ret);
 }

void ShaderCompositer::setHue(double hue)
{
    this->m_defaultParam.hue = hue;
}

double ShaderCompositer::getHue() const
{
    return this->m_defaultParam.hue;
}

void ShaderCompositer::setSaturation(double saturation)
{
    this->m_defaultParam.saturation = saturation;
}

double ShaderCompositer::getSaturation() const
{
    return this->m_defaultParam.saturation;
}

void ShaderCompositer::setContrast(double contrast)
{
    this->m_defaultParam.contrast = contrast;
}

double ShaderCompositer::getContrast() const
{
    return this->m_defaultParam.contrast;
}

void ShaderCompositer::setBrightness(double brightness)
{
    this->m_defaultParam.brightness = brightness;
}

double ShaderCompositer::getBrightness() const
{
    return this->m_defaultParam.brightness;
}

void ShaderCompositer::useSharply(bool use)
{
    this->m_defaultParam.useSharply = use;
#if !defined Q_OS_MOBILE
    this->build();
#endif
}

bool ShaderCompositer::isUsingSharply() const
{
    return this->m_defaultParam.useSharply;
}

void ShaderCompositer::useSharpen(bool use)
{
    this->m_defaultParam.useSharpen = use;
#if !defined Q_OS_MOBILE
    this->build();
#endif
}

bool ShaderCompositer::isUsingSharpen() const
{
    return this->m_defaultParam.useSharpen;
}

void ShaderCompositer::useSoften(bool use)
{
    this->m_defaultParam.useSoften = use;
#if !defined Q_OS_MOBILE
    this->build();
#endif
}

bool ShaderCompositer::isUsingSoften() const
{
    return this->m_defaultParam.useSoften;
}

void ShaderCompositer::useLeftRightInvert(bool use)
{
    this->m_defaultParam.useLeftRightInvert = use;
#if !defined Q_OS_MOBILE
    this->build();
#endif
}

bool ShaderCompositer::isUsingLeftRightInvert() const
{
    return this->m_defaultParam.useLeftRightInvert;
}

void ShaderCompositer::useTopBottomInvert(bool use)
{
    this->m_defaultParam.useTopBottomInvert = use;
#if !defined Q_OS_MOBILE
    this->build();
#endif
}

bool ShaderCompositer::isUsingTopBottomInvert() const
{
    return this->m_defaultParam.useTopBottomInvert;
}

void ShaderCompositer::use360Degree(bool use)
{
    this->m_defaultParam.use360Degree = use;
#if !defined Q_OS_MOBILE
    this->build();
#endif
}

bool ShaderCompositer::is360Degree() const
{
    return this->m_defaultParam.use360Degree;
}

void ShaderCompositer::useColorConversion(bool use)
{
    this->m_defaultParam.useColorConversion = use;
#if !defined Q_OS_MOBILE
    this->build();
#endif
}

bool ShaderCompositer::isUsingColorConversion() const
{
    return this->m_defaultParam.useColorConversion;
}

void ShaderCompositer::set3DMethod(AnyVODEnums::Video3DMethod method3D)
{
    this->m_defaultParam.method3D = method3D;
    this->m_defaultParam.interlaced = false;
    this->m_defaultParam.anaglyph = false;
    this->m_defaultParam.checker = false;

    switch (method3D)
    {
        case AnyVODEnums::V3M_ROW_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_ROW_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_ROW_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_ROW_TOP_BOTTOM_BOTTOM_PRIOR:
        {
            this->m_defaultParam.interlaced = true;
            this->m_defaultParam.rowOrCol = true;

            break;
        }
        case AnyVODEnums::V3M_COL_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_COL_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_COL_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_COL_TOP_BOTTOM_BOTTOM_PRIOR:
        {
            this->m_defaultParam.interlaced = true;
            this->m_defaultParam.rowOrCol = false;

            break;
        }
        case AnyVODEnums::V3M_RED_CYAN_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_RED_CYAN_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_RED_CYAN_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_RED_CYAN_TOP_BOTTOM_BOTTOM_PRIOR:
        case AnyVODEnums::V3M_GREEN_MAGENTA_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_GREEN_MAGENTA_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_GREEN_MAGENTA_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_GREEN_MAGENTA_TOP_BOTTOM_BOTTOM_PRIOR:
        case AnyVODEnums::V3M_YELLOW_BLUE_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_YELLOW_BLUE_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_YELLOW_BLUE_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_YELLOW_BLUE_TOP_BOTTOM_BOTTOM_PRIOR:
        case AnyVODEnums::V3M_RED_BLUE_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_RED_BLUE_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_RED_BLUE_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_RED_BLUE_TOP_BOTTOM_BOTTOM_PRIOR:
        case AnyVODEnums::V3M_RED_GREEN_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_RED_GREEN_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_RED_GREEN_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_RED_GREEN_TOP_BOTTOM_BOTTOM_PRIOR:
        {
            this->m_defaultParam.anaglyph = true;
            break;
        }
        case AnyVODEnums::V3M_CHECKER_BOARD_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_CHECKER_BOARD_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_CHECKER_BOARD_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_CHECKER_BOARD_TOP_BOTTOM_BOTTOM_PRIOR:
        {
            this->m_defaultParam.checker = true;
            break;
        }
        default:
        {
            break;
        }
    }

#if !defined Q_OS_MOBILE
    this->build();
#endif
}

AnyVODEnums::Video3DMethod ShaderCompositer::get3DMethod() const
{
    return this->m_defaultParam.method3D;
}

void ShaderCompositer::setAnaglyphAlgorithm(AnyVODEnums::AnaglyphAlgorithm algoritm)
{
    this->m_defaultParam.anaglyphAlgorithm = algoritm;
}

AnyVODEnums::AnaglyphAlgorithm ShaderCompositer::getAnaglyphAlgorithm() const
{
    return this->m_defaultParam.anaglyphAlgorithm;
}

void ShaderCompositer::setVideo360Type(AnyVODEnums::Video360Type type)
{
    Cube::deInit();
    Cube::init(type);

    this->m_defaultParam.video360Type = type;
#if !defined Q_OS_MOBILE
    this->build();
#endif
}

AnyVODEnums::Video360Type ShaderCompositer::getVideo360Type() const
{
    return this->m_defaultParam.video360Type;
}

bool ShaderCompositer::canUseAnaglyphAlgorithm() const
{
    switch (this->m_defaultParam.method3D)
    {
        case AnyVODEnums::V3M_RED_BLUE_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_RED_BLUE_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_RED_BLUE_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_RED_BLUE_TOP_BOTTOM_BOTTOM_PRIOR:
        case AnyVODEnums::V3M_RED_GREEN_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_RED_GREEN_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_RED_GREEN_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_RED_GREEN_TOP_BOTTOM_BOTTOM_PRIOR:
            return false;
        default:
            break;
    }

    return true;
}

void ShaderCompositer::setup3D(bool sideBySide, bool leftOrTop)
{
    this->m_defaultParam.sideBySide = sideBySide;
    this->m_defaultParam.leftOrTop = leftOrTop;
}
