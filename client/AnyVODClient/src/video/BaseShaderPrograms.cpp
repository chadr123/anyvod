﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "BaseShaderPrograms.h"

const QString BaseShaderPrograms::ENTRY_SIG = "ENTRIES";
const QString BaseShaderPrograms::FUNCS_SIG = "FUNCS";
const QString BaseShaderPrograms::PIXEL_DECODER_SIG = "PIXEL_DECODER";
const QString BaseShaderPrograms::TEX_COORD_DECODER_SIG = "TEX_COORD_DECODER_SIG";

BaseShaderPrograms::BaseShaderPrograms()
{

}

QString BaseShaderPrograms::getVertexFrame()
{
    return QString(R"(
        uniform mat4 AnyVOD_projection;
        uniform mat4 AnyVOD_modelView;

        attribute vec4 AnyVOD_vertex;

        attribute vec2 AnyVOD_inTexCoords;
        varying vec2 AnyVOD_outTexCoords;

        uniform vec4 AnyVOD_inColor;
        varying vec4 AnyVOD_outColor;

        void main()
        {
          AnyVOD_outTexCoords = AnyVOD_inTexCoords;
          AnyVOD_outColor = AnyVOD_inColor;

          gl_Position = AnyVOD_projection * AnyVOD_modelView * AnyVOD_vertex;
        }

        )"
    );
}

QString BaseShaderPrograms::getShaderFrame()
{
    return BaseShaderPrograms::getShaderLibrary() + QString(R"(
        uniform float AnyVOD_Hue;
        uniform float AnyVOD_Saturation;
        uniform float AnyVOD_Contrast;
        uniform float AnyVOD_Brightness;

        uniform float AnyVOD_Width;
        uniform float AnyVOD_Height;
        uniform float AnyVOD_Clock;
        uniform vec3 AnyVOD_LumAvg;

        uniform bool AnyVOD_useLeftRightInvert;
        uniform bool AnyVOD_useTopBottomInvert;

        uniform bool AnyVOD_leftOrTop;
        uniform bool AnyVOD_rowOrCol;
        uniform bool AnyVOD_sideBySide;
        uniform vec2 AnyVOD_screenCoord;
        uniform mat3 AnyVOD_anaglyphFirstMatrix;
        uniform mat3 AnyVOD_anaglyphSecondMatrix;

        vec2 AnyVOD_leftSBS(in vec2 coord)
        {
          return vec2(coord.s / 2.0, coord.t);
        }

        vec2 AnyVOD_rightSBS(in vec2 coord)
        {
          return vec2((AnyVOD_screenCoord.s + coord.s) / 2.0, coord.t);
        }

        vec2 AnyVOD_topTAB(in vec2 coord)
        {
          return vec2(coord.s, coord.t / 2.0);
        }

        vec2 AnyVOD_bottomTAB(in vec2 coord)
        {
          return vec2(coord.s, (AnyVOD_screenCoord.t + coord.t) / 2.0);
        }

        void AnyVOD_leftOrRight(inout vec2 coord, in bool left)
        {
          if (left)
            coord = AnyVOD_leftSBS(coord);
          else
            coord = AnyVOD_rightSBS(coord);
        }

        void AnyVOD_topOrBottom(inout vec2 coord, in bool top)
        {
          if (top)
            coord = AnyVOD_topTAB(coord);
          else
            coord = AnyVOD_bottomTAB(coord);
        }

        void AnyVOD_use360Degree(inout vec4 texel, inout vec2 coord)
        {
          coord.t = 1.0 - coord.t;

          texel = AnyVOD_getTexel(coord);
        }
        )"
        + FUNCS_SIG +
        R"(
        void main()
        {
          vec2 coord;
          vec4 texel;

          coord = AnyVOD_getCoord();
          texel = AnyVOD_getColor() * AnyVOD_getTexel(coord);
        )"
        + ENTRY_SIG +
        R"(
          gl_FragColor = texel;
        }

        )"
    );
}

QString BaseShaderPrograms::getEffects()
{
    return QString(R"(
        void AnyVOD_sharply(inout vec4 texel, inout vec2 coord)
        {
          const float effect_width = 1.6;
          const float positive = 2.0;
          const float negative = -0.125;

          float dx = effect_width / AnyVOD_Width;
          float dy = effect_width / AnyVOD_Height;

          vec4 c1 = AnyVOD_getTexel(coord + vec2(-dx, -dy)) * negative;
          vec4 c2 = AnyVOD_getTexel(coord + vec2(0.0, -dy)) * negative;
          vec4 c3 = AnyVOD_getTexel(coord + vec2(-dx, 0.0)) * negative;
          vec4 c4 = AnyVOD_getTexel(coord + vec2(dx, 0.0)) * negative;
          vec4 c5 = AnyVOD_getTexel(coord + vec2(0.0, dy)) * negative;
          vec4 c6 = AnyVOD_getTexel(coord + vec2(dx, dy)) * negative;
          vec4 c7 = AnyVOD_getTexel(coord + vec2(-dx, dy)) * negative;
          vec4 c8 = AnyVOD_getTexel(coord + vec2(dx, -dy)) * negative;
          vec4 c9 = texel * positive;

          texel = c1 + c2 + c3 + c4 + c5 + c6 + c7 + c8 + c9;
        }

        void AnyVOD_sharpen(inout vec4 texel, inout vec2 coord)
        {
          const float threshold = 0.0009;
          const float intensity = 0.5;
          const vec4 mask = vec4(0.299, 0.587, 0.114, 0.0);

          float dx = 4.0 / AnyVOD_Width;
          float dy = 4.0 / AnyVOD_Height;

          vec4 c1 = AnyVOD_getTexel(coord + vec2(-dx, dy));
          vec4 c2 = AnyVOD_getTexel(coord + vec2(0.0, dy));
          vec4 c3 = AnyVOD_getTexel(coord + vec2(dx, dy));
          vec4 c4 = AnyVOD_getTexel(coord + vec2(-dx, 0.0));
          vec4 c5 = AnyVOD_getTexel(coord + vec2(0.0, 0.0));
          vec4 c6 = AnyVOD_getTexel(coord + vec2(dx, 0.0));
          vec4 c7 = AnyVOD_getTexel(coord + vec2(-dx, -dy));
          vec4 c8 = AnyVOD_getTexel(coord + vec2(0.0, -dy));
          vec4 c9 = AnyVOD_getTexel(coord + vec2(dx, -dy));

          vec4 c10 = (2.0 * (c2 + c4 + c6 + c8) + (c1 + c3 + c7 + c9) + 4.0 * c5) / 16.0;
          float result = abs(dot(c10 - c5, mask));
          vec4 c11 = vec4(result, result, result, result);
          vec4 c0;

          if (c11.x < threshold)
           c0 = c5;
          else
           c0 = c5 + intensity * (c5 - c10);

          if ((dot(c1, mask) < 0.067 && dot(c2, mask) < 0.067 && dot(c3, mask) > -0.5005) ||
               (dot(c7, mask) < 0.067 && dot(c8, mask) < 0.067 && dot(c9, mask) > 0.5005))
          {
            c0 = c5;
          }

          texel = c0;
        }

        void AnyVOD_soften(inout vec4 texel, inout vec2 coord)
        {
          const float threshold = 0.3;
          const float inc = 0.6;

          float dx = 1.0 / AnyVOD_Width;
          float dy = 1.0 / AnyVOD_Height;
          vec4 total = texel;
          float n = 1.0;
          vec4 c[8];

          c[0] = AnyVOD_getTexel(coord + vec2(-dx, -dy));
          c[1] = AnyVOD_getTexel(coord + vec2(0.0, -dy));
          c[2] = AnyVOD_getTexel(coord + vec2(dx, -dy));
          c[3] = AnyVOD_getTexel(coord + vec2(-dx, 0.0));
          c[4] = AnyVOD_getTexel(coord + vec2(dx, 0.0));
          c[5] = AnyVOD_getTexel(coord + vec2(-dx, dy));
          c[6] = AnyVOD_getTexel(coord + vec2(0.0, dy));
          c[7] = AnyVOD_getTexel(coord + vec2(dx, dy));

          for (int i = 0; i < 7; i++)
          {
            if (length(texel - c[i]) < threshold)
            {
              total += c[i] * inc;
              n += inc;
            }
          }

          texel = total / n;
        }

        void AnyVOD_invert(inout vec4 texel, inout vec2 coord)
        {
          if (AnyVOD_useLeftRightInvert)
            coord.s = 1.0 - coord.s;

          if (AnyVOD_useTopBottomInvert)
            coord.t = 1.0 - coord.t;

          texel = AnyVOD_getTexel(coord);
        }

        )"
    );
}

QString BaseShaderPrograms::getInterlace()
{
    return QString(R"(
        void AnyVOD_interlace3DTABSub(in bool rowEven, inout vec2 coord)
        {
          if (rowEven)
            AnyVOD_topOrBottom(coord, AnyVOD_leftOrTop);
          else
            AnyVOD_topOrBottom(coord, !AnyVOD_leftOrTop);
        }

        void AnyVOD_interlace3DSBSSub(in bool rowEven, inout vec2 coord)
        {
          if (rowEven)
            AnyVOD_leftOrRight(coord, AnyVOD_leftOrTop);
          else
            AnyVOD_leftOrRight(coord, !AnyVOD_leftOrTop);
        }

        void AnyVOD_interlace3DSub(inout vec2 coord, in int fragCoord)
        {
          if (AnyVOD_sideBySide)
            AnyVOD_interlace3DSBSSub(AnyVOD_isEven(fragCoord), coord);
          else
            AnyVOD_interlace3DTABSub(AnyVOD_isEven(fragCoord), coord);
        }

        void AnyVOD_interlace3D(inout vec4 texel, inout vec2 coord)
        {
          ivec2 ticoord = ivec2(gl_FragCoord.xy);

          if (AnyVOD_rowOrCol)
            AnyVOD_interlace3DSub(coord, ticoord.y);
          else
            AnyVOD_interlace3DSub(coord, ticoord.x);

          texel = AnyVOD_getTexel(coord);
        }

        )"
    );
}

QString BaseShaderPrograms::getCheckerBoard()
{
    return QString(R"(
        void AnyVOD_checkerBoardTABSub(in bool rowEven, in int colCoord, inout vec2 coord)
        {
          if (rowEven)
            AnyVOD_topOrBottom(coord, AnyVOD_isEven(colCoord));
          else
            AnyVOD_topOrBottom(coord, !AnyVOD_isEven(colCoord));
        }

        void AnyVOD_checkerBoardSBSSub(in bool rowEven, in int colCoord, inout vec2 coord)
        {
          if (rowEven)
            AnyVOD_leftOrRight(coord, AnyVOD_isEven(colCoord));
          else
            AnyVOD_leftOrRight(coord, !AnyVOD_isEven(colCoord));
        }

        void AnyVOD_checkerBoardSub(inout vec2 coord, in ivec2 fragCoord)
        {
          if (AnyVOD_sideBySide)
          {
            if (AnyVOD_leftOrTop)
              AnyVOD_checkerBoardSBSSub(AnyVOD_isEven(fragCoord.y), fragCoord.x, coord);
            else
              AnyVOD_checkerBoardSBSSub(!AnyVOD_isEven(fragCoord.y), fragCoord.x, coord);
          }
          else
          {
            if (AnyVOD_leftOrTop)
              AnyVOD_checkerBoardTABSub(AnyVOD_isEven(fragCoord.y), fragCoord.x, coord);
            else
              AnyVOD_checkerBoardTABSub(!AnyVOD_isEven(fragCoord.y), fragCoord.x, coord);
          }
        }

        void AnyVOD_checkerBoard(inout vec4 texel, inout vec2 coord)
        {
          ivec2 ticoord = ivec2(gl_FragCoord.xy);

          AnyVOD_checkerBoardSub(coord, ticoord);
          texel = AnyVOD_getTexel(coord);
        }

        )"
    );
}

QString BaseShaderPrograms::getAnaglyph()
{
    return QString(R"(
        void AnyVOD_splitScreenSBSSub(in vec2 coord, out vec2 firstCoord, out vec2 secondCoord)
        {
          firstCoord = AnyVOD_leftSBS(coord);
          secondCoord = AnyVOD_rightSBS(coord);
        }

        void AnyVOD_splitScreenTABSub(in vec2 coord, out vec2 firstCoord, out vec2 secondCoord)
        {
          firstCoord = AnyVOD_topTAB(coord);
          secondCoord = AnyVOD_bottomTAB(coord);
        }

        void AnyVOD_splitScreen(inout vec4 texel, inout vec2 coord, out vec4 firstTexel, out vec4 secondTexel)
        {
          vec2 firstCoord;
          vec2 secondCoord;

          if (AnyVOD_sideBySide)
          {
            if (AnyVOD_leftOrTop)
              AnyVOD_splitScreenSBSSub(coord, firstCoord, secondCoord);
            else
              AnyVOD_splitScreenSBSSub(coord, secondCoord, firstCoord);
          }
          else
          {
            if (AnyVOD_leftOrTop)
              AnyVOD_splitScreenTABSub(coord, firstCoord, secondCoord);
            else
              AnyVOD_splitScreenTABSub(coord, secondCoord, firstCoord);
          }

          firstTexel = AnyVOD_getTexel(firstCoord);
          secondTexel = AnyVOD_getTexel(secondCoord);
        }

        void AnyVOD_anaglyph(inout vec4 texel, inout vec2 coord)
        {
          vec4 firstTexel;
          vec4 secondTexel;

          AnyVOD_splitScreen(texel, coord, firstTexel, secondTexel);

          texel.rgb = AnyVOD_anaglyphFirstMatrix * firstTexel.rgb + AnyVOD_anaglyphSecondMatrix * secondTexel.rgb;
          texel.a = firstTexel.a;

          texel = clamp(texel, 0.0, 1.0);
        }

        )"
    );
}

QString BaseShaderPrograms::getSubtitleInterlace()
{
    return BaseShaderPrograms::getShaderLibrary() + QString(R"(
        uniform bool AnyVOD_firstFrame;
        uniform bool AnyVOD_rowOrCol;

        void AnyVOD_interlace3DSubtitleSub(inout vec4 texel, in int fragCoord, in vec2 coord)
        {
          if (AnyVOD_isEven(fragCoord))
          {
            if (AnyVOD_firstFrame)
               texel = AnyVOD_getColor() * AnyVOD_getTexel(coord);
            else
               texel = vec4(0.0, 0.0, 0.0, 0.0);
          }
          else
          {
            if (!AnyVOD_firstFrame)
               texel = AnyVOD_getColor() * AnyVOD_getTexel(coord);
            else
               texel = vec4(0.0, 0.0, 0.0, 0.0);
          }
        }

        void AnyVOD_interlace3DSubtitle(inout vec4 texel, in vec2 coord)
        {
          ivec2 ticoord = ivec2(gl_FragCoord.xy);

          if (AnyVOD_rowOrCol)
            AnyVOD_interlace3DSubtitleSub(texel, ticoord.y, coord);
          else
            AnyVOD_interlace3DSubtitleSub(texel, ticoord.x, coord);
        }

        void main()
        {
          AnyVOD_interlace3DSubtitle(gl_FragColor, AnyVOD_getCoord());
        }

        )"
    );
}

QString BaseShaderPrograms::getSubtitleCheckerBoard()
{
    return BaseShaderPrograms::getShaderLibrary() + QString(R"(
        uniform bool AnyVOD_firstFrame;

        void AnyVOD_checkerBoardSubtitleSub(inout vec4 texel, in ivec2 fragCoord, in vec2 coord)
        {
          if (AnyVOD_isEven(fragCoord.y))
          {
            if (AnyVOD_isEven(fragCoord.x))
            {
              if (AnyVOD_firstFrame)
                 texel = AnyVOD_getColor() * AnyVOD_getTexel(coord);
              else
                 texel = vec4(0.0, 0.0, 0.0, 0.0);
            }
            else
            {
              if (!AnyVOD_firstFrame)
                 texel = AnyVOD_getColor() * AnyVOD_getTexel(coord);
              else
                 texel = vec4(0.0, 0.0, 0.0, 0.0);
            }
          }
          else
          {
            if (AnyVOD_isEven(fragCoord.x))
            {
              if (!AnyVOD_firstFrame)
                 texel = AnyVOD_getColor() * AnyVOD_getTexel(coord);
              else
                 texel = vec4(0.0, 0.0, 0.0, 0.0);
            }
            else
            {
              if (AnyVOD_firstFrame)
                 texel = AnyVOD_getColor() * AnyVOD_getTexel(coord);
              else
                 texel = vec4(0.0, 0.0, 0.0, 0.0);
            }
          }
        }

        void AnyVOD_checkerBoardSubtitle(inout vec4 texel, in vec2 coord)
        {
          ivec2 ticoord = ivec2(gl_FragCoord.xy);

          AnyVOD_checkerBoardSubtitleSub(texel, ticoord, coord);
        }

        void main()
        {
          AnyVOD_checkerBoardSubtitle(gl_FragColor, AnyVOD_getCoord());
        }

        )"
    );
}

QString BaseShaderPrograms::getSubtitleAnaglyph()
{
    return BaseShaderPrograms::getShaderLibrary() + QString(R"(
        uniform mat3 AnyVOD_anaglyphMatrix;

        void main()
        {
          vec4 texel;

          texel = AnyVOD_getTexel(AnyVOD_getCoord());

          texel.rgb = AnyVOD_anaglyphMatrix * texel.rgb;
          texel = AnyVOD_getColor() * clamp(texel, 0.0, 1.0);

          gl_FragColor = texel;
        }

        )"
    );
}

QString BaseShaderPrograms::getBarrelDistortion()
{
    return BaseShaderPrograms::getShaderLibrary() + QString(R"(
        uniform vec2 AnyVOD_texRange;
        uniform vec2 AnyVOD_lensCenterOffset;
        uniform vec2 AnyVOD_coefficients;
        uniform float AnyVOD_fillScale;

        float AnyVOD_distortionScale(in vec2 offset)
        {
          // Refers to https://en.wikipedia.org/wiki/Distortion_(optics)#Software_correction
          vec2 offsetSquared = offset * offset;
          float radiusSquared = offsetSquared.x + offsetSquared.y;
          float distortionScale =
            1.0 +
            AnyVOD_coefficients[0] * radiusSquared +
            AnyVOD_coefficients[1] * radiusSquared * radiusSquared;

          return distortionScale;
        }

        vec2 AnyVOD_texCoordsToDistortionCoords(in vec2 texCoord)
        {
          //Convert the texture coordinates from \"0 to 1\" to \"-1 to 1\"
          vec2 result = texCoord * 2.0 - 1.0;

          //Convert from using the center of the screen as the origin to
          //using the lens center as the origin
          result -= AnyVOD_lensCenterOffset;

          return result;
        }

        vec2 AnyVOD_distortionCoordsToTexCoords(in vec2 offset)
        {
          //Scale the distorted result so that we fill the desired amount of pixel real-estate
          vec2 result = offset / AnyVOD_fillScale;

          //Convert from using the lens center as the origin to
          //using the screen center as the origin
          result += AnyVOD_lensCenterOffset;

          // Convert the texture coordinates from \"-1 to 1\" to \"0 to 1\"
          result += 1.0;
          result /= 2.0;

          return result;
        }

        void main()
        {
          vec4 texel;
          vec2 offset = AnyVOD_texCoordsToDistortionCoords(AnyVOD_getCoord());
          float scale = AnyVOD_distortionScale(offset);
          vec2 distortedOffset = offset * scale;
          vec2 actualTextureCoords = AnyVOD_distortionCoordsToTexCoords(distortedOffset) * AnyVOD_texRange;
          vec2 clamped = clamp(actualTextureCoords, vec2(0.0, 0.0), AnyVOD_texRange);

          if (!all(equal(clamped, actualTextureCoords)))
            texel = vec4(0.0, 0.0, 0.0, 1.0);
          else
            texel = AnyVOD_getTexel(actualTextureCoords);

          texel = AnyVOD_getColor() * clamp(texel, 0.0, 1.0);

          gl_FragColor = texel;
        }

        )"
    );
}

QString BaseShaderPrograms::getPincushionDistortion()
{
    return BaseShaderPrograms::getShaderLibrary() + QString(R"(
        uniform vec2 AnyVOD_texRange;
        uniform vec2 AnyVOD_lensCenterOffset;
        uniform vec2 AnyVOD_coefficients;
        uniform float AnyVOD_fillScale;

        float AnyVOD_distortionScale(in vec2 offset)
        {
          // Refers to https://en.wikipedia.org/wiki/Distortion_(optics)#Software_correction
          vec2 offsetSquared = offset * offset;
          float radiusSquared = offsetSquared.x + offsetSquared.y;
          float distortionScale =
            1.0 +
            AnyVOD_coefficients[0] * radiusSquared +
            AnyVOD_coefficients[1] * radiusSquared * radiusSquared;

          return distortionScale;
        }

        vec2 AnyVOD_texCoordsToDistortionCoords(in vec2 texCoord)
        {
          //Convert the texture coordinates from \"0 to 1\" to \"-1 to 1\"
          vec2 result = texCoord * 2.0 - 1.0;

          //Convert from using the center of the screen as the origin to
          //using the lens center as the origin
          result -= AnyVOD_lensCenterOffset;

          return result;
        }

        vec2 AnyVOD_distortionCoordsToTexCoords(in vec2 offset)
        {
          //Scale the distorted result so that we fill the desired amount of pixel real-estate
          vec2 result = offset / AnyVOD_fillScale;

          //Convert from using the lens center as the origin to
          //using the screen center as the origin
          result += AnyVOD_lensCenterOffset;

          // Convert the texture coordinates from \"-1 to 1\" to \"0 to 1\"
          result += 1.0;
          result /= 2.0;

          return result;
        }

        void main()
        {
          vec4 texel;
          vec2 offset = AnyVOD_texCoordsToDistortionCoords(AnyVOD_getCoord());
          float scale = AnyVOD_distortionScale(offset);
          vec2 distortedOffset = offset / scale;
          vec2 actualTextureCoords = AnyVOD_distortionCoordsToTexCoords(distortedOffset) * AnyVOD_texRange;
          vec2 clamped = clamp(actualTextureCoords, vec2(0.0, 0.0), AnyVOD_texRange);

          if (!all(equal(clamped, actualTextureCoords)))
            texel = vec4(0.0, 0.0, 0.0, 1.0);
          else
            texel = AnyVOD_getTexel(actualTextureCoords);

          texel = AnyVOD_getColor() * clamp(texel, 0.0, 1.0);

          gl_FragColor = texel;
        }

        )"
    );
}

QString BaseShaderPrograms::getSimple()
{
    return BaseShaderPrograms::getShaderLibrary() + QString(R"(
        void main()
        {
          vec4 texel;

          texel = AnyVOD_getColor() * AnyVOD_getTexel(AnyVOD_getCoord());

          gl_FragColor = texel;
        }

        )"
    );
}

QString BaseShaderPrograms::getLine()
{
    return BaseShaderPrograms::getShaderLibrary() + QString(R"(
        void main()
        {
          gl_FragColor = AnyVOD_getColor();
        }

        )"
    );
}

QString BaseShaderPrograms::getDefaultLibrary()
{
    return QString(R"(
        vec3 AnyVOD_rgbTohsl(in vec3 color)
        {
          vec3 hsl;

          float minValue = min(min(color.r, color.g), color.b);
          float maxValue = max(max(color.r, color.g), color.b);
          float delta = maxValue - minValue;
          float halfDelta = delta / 2.0;

          hsl.z = (maxValue + minValue) / 2.0;

          if (delta == 0.0)
          {
            hsl.x = -1.0;
            hsl.y = 0.0;
          }
          else
          {
            if (hsl.z < 0.5)
              hsl.y = delta / (maxValue + minValue);
            else
              hsl.y = delta / (2.0 - maxValue - minValue);

            float deltaR = (((maxValue - color.r) / 6.0) + halfDelta) / delta;
            float deltaG = (((maxValue - color.g) / 6.0) + halfDelta) / delta;
            float deltaB = (((maxValue - color.b) / 6.0) + halfDelta) / delta;

            if (color.r == maxValue)
              hsl.x = deltaB - deltaG;
            else if (color.g == maxValue)
              hsl.x = (1.0 / 3.0) + deltaR - deltaB;
            else if (color.b == maxValue)
              hsl.x = (2.0 / 3.0) + deltaG - deltaR;
            else
              hsl.x = 0.0;

            if (hsl.x < 0.0)
              hsl.x += 1.0;
            else if (hsl.x > 1.0)
              hsl.x -= 1.0;
          }

          return hsl;
        }

        float AnyVOD_hueToValue(in float f1, in float f2, in float hue)
        {
          if (hue < 0.0)
            hue += 1.0;
          else if (hue > 1.0)
            hue -= 1.0;

          float res;

          if ((6.0 * hue) < 1.0)
            res = f1 + (f2 - f1) * 6.0 * hue;
          else if ((2.0 * hue) < 1.0)
            res = f2;
          else if ((3.0 * hue) < 2.0)
            res = f1 + (f2 - f1) * ((2.0 / 3.0) - hue) * 6.0;
          else
            res = f1;

          return res;
        }

        vec3 AnyVOD_hslTorgb(in vec3 hsl)
        {
          vec3 rgb;

          if (hsl.y == 0.0)
          {
            rgb = vec3(hsl.z);
          }
          else
          {
            float f1;
            float f2;

            if (hsl.z < 0.5)
              f2 = hsl.z * (1.0 + hsl.y);
            else
              f2 = (hsl.z + hsl.y) - (hsl.y * hsl.z);

            f1 = 2.0 * hsl.z - f2;

            rgb.r = AnyVOD_hueToValue(f1, f2, hsl.x + (1.0 / 3.0));
            rgb.g = AnyVOD_hueToValue(f1, f2, hsl.x);
            rgb.b = AnyVOD_hueToValue(f1, f2, hsl.x - (1.0 / 3.0));
          }

          return rgb;
        }

        void AnyVOD_hue(inout vec4 texel, inout vec2 coord)
        {
          if (AnyVOD_Hue == 0.0)
            return;

          vec3 hsl = AnyVOD_rgbTohsl(texel.rgb);

          hsl.x += AnyVOD_Hue;
          texel.rgb = AnyVOD_hslTorgb(hsl);
        }

        void AnyVOD_saturation_contrast(inout vec4 texel, inout vec2 coord)
        {
          if (AnyVOD_Saturation == 1.0 && AnyVOD_Contrast == 1.0)
            return;

          const vec3 lumCoeff = vec3(0.2125, 0.7154, 0.0721);
          vec3 intensity = vec3(dot(texel.rgb, lumCoeff));
          vec3 satColor = mix(intensity, texel.rgb, AnyVOD_Saturation);
          vec3 conColor = mix(AnyVOD_LumAvg, satColor, AnyVOD_Contrast);

          conColor = clamp(conColor, 0.0, 1.0);
          texel.rgb = conColor;
        }

        void AnyVOD_brightness(inout vec4 texel, inout vec2 coord)
        {
          if (AnyVOD_Brightness == 1.0)
            return;

          texel.rgb *= AnyVOD_Brightness;
          texel = clamp(texel, 0.0, 1.0);
        }

        )"
    );
}

QString BaseShaderPrograms::getPixelDecoder(AVPixelFormat format)
{
    switch (format)
    {
        case AV_PIX_FMT_YUV420P:
        case AV_PIX_FMT_YUV420P16:
            return "AnyVOD_YUV420P(coord)";
        case AV_PIX_FMT_NV12:
            return "AnyVOD_NV12(coord)";
        case AV_PIX_FMT_NV21:
            return "AnyVOD_NV21(coord)";
        case AV_PIX_FMT_YUYV422:
            return "AnyVOD_YUYV422(coord)";
        case AV_PIX_FMT_UYVY422:
            return "AnyVOD_UYVY422(coord)";
        case AV_PIX_FMT_YVYU422:
            return "AnyVOD_YVYU422(coord)";
        case AV_PIX_FMT_P010:
        case AV_PIX_FMT_P016:
            return "AnyVOD_P01X(coord)";
        case AV_PIX_FMT_YUV420P9:
            return "AnyVOD_YUV420P9(coord)";
        case AV_PIX_FMT_YUV420P10:
            return "AnyVOD_YUV420P10(coord)";
        case AV_PIX_FMT_YUV420P12:
            return "AnyVOD_YUV420P12(coord)";
        case AV_PIX_FMT_YUV420P14:
            return "AnyVOD_YUV420P14(coord)";
        default:
            return "texture2D(AnyVOD_tex0, coord)";
    }
}

QString BaseShaderPrograms::getTexCoordDecoder(AnyVODEnums::Video360Type type)
{
    switch (type)
    {
        case AnyVODEnums::V3T_CUBEMAP_2X3:
            return "AnyVOD_getCubeMap2X3TexCoord()";
        case AnyVODEnums::V3T_EAC_2X3:
            return "AnyVOD_getEAC2X3TexCoord()";
        case AnyVODEnums::V3T_EAC_3X2:
            return "AnyVOD_getEAC3X2TexCoord()";
        default:
            return BaseShaderPrograms::getDefaultTexCoordDecoder();
    }
}

QString BaseShaderPrograms::getDefaultTexCoordDecoder()
{
    return "AnyVOD_getNormalTexCoord()";
}

QString BaseShaderPrograms::getShaderLibrary()
{
    return QString(
#if defined Q_OS_MACOS
        R"(
        #extension GL_EXT_gpu_shader4 : enable
        )"
#endif
#if defined Q_OS_MOBILE
        R"(
        precision highp float;
        precision highp int;
        precision highp sampler2D;
        )"
#endif
        R"(
        varying vec2 AnyVOD_outTexCoords;
        varying vec4 AnyVOD_outColor;

        uniform sampler2D AnyVOD_tex0;
        uniform sampler2D AnyVOD_tex1;
        uniform sampler2D AnyVOD_tex2;

        uniform float AnyVOD_TexWidth;
        uniform float AnyVOD_TexHeight;
        uniform vec4 AnyVOD_RenderRect;

        uniform mat3 AnyVOD_primaryMatrix;
        uniform float AnyVOD_gammaDstInv;
        uniform float AnyVOD_gammaSrc;
        uniform vec3 AnyVOD_coefsDst;
        uniform float AnyVOD_toneParam;

        const float AnyVOD_PI = 3.1415926535897932384626433832795;
        const float AnyVOD_INV_PI = 1.0 / AnyVOD_PI;
        const float AnyVOD_INV_2PI = 2.0 / AnyVOD_PI;

        bool AnyVOD_isEven(in int value)
        {
        )"
#if defined Q_OS_MACOS
        R"(
          return value % 2 == 0;
        )"
#else
        R"(
          return mod(float(value), 2.0) == 0.0;
        )"
#endif
        R"(
        }

        float AnyVOD_TransformCoord(vec2 domain, vec2 target, float val)
        {
          float unit = 1.0 / (domain[1] - domain[0]);

          return target[0] + (target[1] - target[0]) * (val - domain[0]) * unit;
        }

        vec4 AnyVOD_GetRGBFromYUV(in float y, in float u, in float v)
        {
          y = 1.1643 * (y - 0.0625);
          u -= 0.5;
          v -= 0.5;

          float r = y + 1.5958 * v;
          float g = y - 0.39173 * u - 0.81290 * v;
          float b = y + 2.017 * u;

          return clamp(vec4(r, g, b, 1.0), 0.0, 1.0);
        }

        vec4 AnyVOD_YUV420P(in vec2 coord)
        {
          float y = texture2D(AnyVOD_tex0, coord).r;
          float u = texture2D(AnyVOD_tex1, coord).r;
          float v = texture2D(AnyVOD_tex2, coord).r;

          return AnyVOD_GetRGBFromYUV(y, u, v);
        }

        vec4 AnyVOD_YUV420P9(in vec2 coord)
        {
          float y = texture2D(AnyVOD_tex0, coord).r * 128.0;
          float u = texture2D(AnyVOD_tex1, coord).r * 128.0;
          float v = texture2D(AnyVOD_tex2, coord).r * 128.0;

          return AnyVOD_GetRGBFromYUV(y, u, v);
        }

        vec4 AnyVOD_YUV420P10(in vec2 coord)
        {
          float y = texture2D(AnyVOD_tex0, coord).r * 64.0;
          float u = texture2D(AnyVOD_tex1, coord).r * 64.0;
          float v = texture2D(AnyVOD_tex2, coord).r * 64.0;

          return AnyVOD_GetRGBFromYUV(y, u, v);
        }

        vec4 AnyVOD_YUV420P12(in vec2 coord)
        {
          float y = texture2D(AnyVOD_tex0, coord).r * 16.0;
          float u = texture2D(AnyVOD_tex1, coord).r * 16.0;
          float v = texture2D(AnyVOD_tex2, coord).r * 16.0;

          return AnyVOD_GetRGBFromYUV(y, u, v);
        }

        vec4 AnyVOD_YUV420P14(in vec2 coord)
        {
          float y = texture2D(AnyVOD_tex0, coord).r * 4.0;
          float u = texture2D(AnyVOD_tex1, coord).r * 4.0;
          float v = texture2D(AnyVOD_tex2, coord).r * 4.0;

          return AnyVOD_GetRGBFromYUV(y, u, v);
        }

        vec4 AnyVOD_NV12(in vec2 coord)
        {
          vec4 uv = texture2D(AnyVOD_tex1, coord);
          float y = texture2D(AnyVOD_tex0, coord).r;
          float u = uv.r;
          float v = uv.a;

          return AnyVOD_GetRGBFromYUV(y, u, v);
        }

        vec4 AnyVOD_NV21(in vec2 coord)
        {
          vec4 uv = texture2D(AnyVOD_tex1, coord);
          float y = texture2D(AnyVOD_tex0, coord).r;
          float u = uv.a;
          float v = uv.r;

          return AnyVOD_GetRGBFromYUV(y, u, v);
        }

        vec4 AnyVOD_P01X(in vec2 coord)
        {
          vec4 uv = texture2D(AnyVOD_tex1, coord);
          float y = texture2D(AnyVOD_tex0, coord).r;
          float u = uv.r;
          float v = uv.g;

          return AnyVOD_GetRGBFromYUV(y, u, v);
        }

        vec4 AnyVOD_YUYV422(in vec2 coord)
        {
          vec4 yuyv = texture2D(AnyVOD_tex0, coord);

          float y;
          float u;
          float v;

          if (AnyVOD_isEven(int(gl_FragCoord.x)))
          {
            y = yuyv.r;
            u = yuyv.g;
            v = yuyv.a;
          }
          else
          {
            vec2 preCoord = vec2(coord.s - (1.0 / AnyVOD_TexWidth), coord.t);
            vec4 preYuyv = texture2D(AnyVOD_tex0, preCoord);

            y = preYuyv.b;
            u = yuyv.g;
            v = yuyv.a;
          }

          return AnyVOD_GetRGBFromYUV(y, u, v);
        }

        vec4 AnyVOD_UYVY422(in vec2 coord)
        {
          vec4 uyvy = texture2D(AnyVOD_tex0, coord);

          float y;
          float u;
          float v;

          if (AnyVOD_isEven(int(gl_FragCoord.x)))
          {
            y = uyvy.g;
            u = uyvy.r;
            v = uyvy.b;
          }
          else
          {
            vec2 preCoord = vec2(coord.s - (1.0 / AnyVOD_TexWidth), coord.t);
            vec4 preUyvy = texture2D(AnyVOD_tex0, preCoord);

            y = preUyvy.a;
            u = uyvy.r;
            v = uyvy.b;
          }

          return AnyVOD_GetRGBFromYUV(y, u, v);
        }

        vec4 AnyVOD_YVYU422(in vec2 coord)
        {
          vec4 yvyu = texture2D(AnyVOD_tex0, coord);

          float y;
          float u;
          float v;

          if (AnyVOD_isEven(int(gl_FragCoord.x)))
          {
            y = yvyu.r;
            u = yvyu.a;
            v = yvyu.g;
          }
          else
          {
            vec2 preCoord = vec2(coord.s - (1.0 / AnyVOD_TexWidth), coord.t);
            vec4 preYvyu = texture2D(AnyVOD_tex0, preCoord);

            y = preYvyu.b;
            u = yvyu.a;
            v = yvyu.g;
          }

          return AnyVOD_GetRGBFromYUV(y, u, v);
        }

        vec4 AnyVOD_getColor()
        {
          return AnyVOD_outColor;
        }

        vec2 AnyVOD_getNormalTexCoord()
        {
          return AnyVOD_outTexCoords;
        }

        vec2 AnyVOD_getCubeMap2X3TexCoord()
        {
          vec2 onePixel = vec2(1.0 / AnyVOD_TexWidth, 1.0 / AnyVOD_TexHeight);
          vec4 texFaceX;
          vec3 texFaceY;
          vec2 transformedCoord = AnyVOD_outTexCoords;
          vec2 orgTextureRangeX;
          vec2 orgTextureRangeY;

          texFaceX[0] = AnyVOD_RenderRect[1];
          texFaceX[1] = AnyVOD_RenderRect[1] + AnyVOD_RenderRect[3] / 3.0;
          texFaceX[2] = AnyVOD_RenderRect[1] + AnyVOD_RenderRect[3] / 3.0 * 2.0;
          texFaceX[3] = AnyVOD_RenderRect[1] + AnyVOD_RenderRect[3];

          texFaceY[0] = AnyVOD_RenderRect[0];
          texFaceY[1] = AnyVOD_RenderRect[0] + AnyVOD_RenderRect[2] / 2.0;
          texFaceY[2] = AnyVOD_RenderRect[0] + AnyVOD_RenderRect[2];

          if (AnyVOD_outTexCoords.s >= texFaceX[2])
            orgTextureRangeX = vec2(texFaceX[2], texFaceX[3]);
          else if (AnyVOD_outTexCoords.s >= texFaceX[1])
            orgTextureRangeX = vec2(texFaceX[1], texFaceX[2]);
          else
            orgTextureRangeX = vec2(texFaceX[0], texFaceX[1]);

          if (AnyVOD_outTexCoords.t >= texFaceY[1])
            orgTextureRangeY = vec2(texFaceY[1], texFaceY[2]);
          else
            orgTextureRangeY = vec2(texFaceY[0], texFaceY[1]);

          if (transformedCoord.s <= orgTextureRangeX[0] + onePixel.s)
            transformedCoord.s += onePixel.s;
          else if (transformedCoord.s >= orgTextureRangeX[1] - onePixel.s)
            transformedCoord.s -= onePixel.s;

          if (transformedCoord.t <= orgTextureRangeY[0] + onePixel.t)
            transformedCoord.t += onePixel.t;
          else if (transformedCoord.t >= orgTextureRangeY[1] - onePixel.t)
            transformedCoord.t -= onePixel.t;

          return transformedCoord;
        }

        vec2 AnyVOD_getEAC2X3TexCoord()
        {
          const vec2 eacCoordRange = vec2(-1.0, 1.0);
          const vec2 texCoordRange = vec2(0.0, 1.0);

          vec2 onePixel = vec2(1.0 / AnyVOD_TexWidth, 1.0 / AnyVOD_TexHeight);
          vec4 texFaceX;
          vec3 texFaceY;
          vec2 transformedCoord;
          vec2 orgTextureRangeX;
          vec2 orgTextureRangeY;

          texFaceX[0] = AnyVOD_RenderRect[1];
          texFaceX[1] = AnyVOD_RenderRect[1] + AnyVOD_RenderRect[3] / 3.0;
          texFaceX[2] = AnyVOD_RenderRect[1] + AnyVOD_RenderRect[3] / 3.0 * 2.0;
          texFaceX[3] = AnyVOD_RenderRect[1] + AnyVOD_RenderRect[3];

          texFaceY[0] = AnyVOD_RenderRect[0];
          texFaceY[1] = AnyVOD_RenderRect[0] + AnyVOD_RenderRect[2] / 2.0;
          texFaceY[2] = AnyVOD_RenderRect[0] + AnyVOD_RenderRect[2];

        // Apply EAC transform
          if (AnyVOD_outTexCoords.s >= texFaceX[2])
            orgTextureRangeX = vec2(texFaceX[2], texFaceX[3]);
          else if (AnyVOD_outTexCoords.s >= texFaceX[1])
            orgTextureRangeX = vec2(texFaceX[1], texFaceX[2]);
          else
            orgTextureRangeX = vec2(texFaceX[0], texFaceX[1]);

          if (AnyVOD_outTexCoords.t >= texFaceY[1])
            orgTextureRangeY = vec2(texFaceY[1], texFaceY[2]);
          else
            orgTextureRangeY = vec2(texFaceY[0], texFaceY[1]);

        // Scaling coords by the coordinates following the range from -1.0 to 1.0.
          float px = AnyVOD_TransformCoord(orgTextureRangeX, eacCoordRange, AnyVOD_outTexCoords.s);
          float py = AnyVOD_TransformCoord(orgTextureRangeY, eacCoordRange, AnyVOD_outTexCoords.t);

          float qu = AnyVOD_INV_2PI * atan(px) + 0.5;
          float qv = AnyVOD_INV_2PI * atan(py) + 0.5;

        // Re-scaling coords by original coordinates ranges
          transformedCoord.s = AnyVOD_TransformCoord(texCoordRange, orgTextureRangeX, qu);
          transformedCoord.t = AnyVOD_TransformCoord(texCoordRange, orgTextureRangeY, qv);

          if (transformedCoord.s <= orgTextureRangeX[0] + onePixel.s)
            transformedCoord.s += onePixel.s;
          else if (transformedCoord.s >= orgTextureRangeX[1] - onePixel.s)
            transformedCoord.s -= onePixel.s;

          if (transformedCoord.t <= orgTextureRangeY[0] + onePixel.t)
            transformedCoord.t += onePixel.t;
          else if (transformedCoord.t >= orgTextureRangeY[1] - onePixel.t)
            transformedCoord.t -= onePixel.t;

          return transformedCoord;
        }

        vec2 AnyVOD_getEAC3X2TexCoord()
        {
          const vec2 eacCoordRange = vec2(-1.0, 1.0);
          const vec2 texCoordRange = vec2(0.0, 1.0);

          vec2 onePixel = vec2(1.0 / AnyVOD_TexWidth, 1.0 / AnyVOD_TexHeight);
          vec3 texFaceX;
          vec4 texFaceY;
          vec2 transformedCoord;
          vec2 orgTextureRangeX;
          vec2 orgTextureRangeY;

          texFaceX[0] = AnyVOD_RenderRect[0];
          texFaceX[1] = AnyVOD_RenderRect[0] + AnyVOD_RenderRect[2] / 2.0;
          texFaceX[2] = AnyVOD_RenderRect[0] + AnyVOD_RenderRect[2];

          texFaceY[0] = AnyVOD_RenderRect[1];
          texFaceY[1] = AnyVOD_RenderRect[1] + AnyVOD_RenderRect[3] / 3.0;
          texFaceY[2] = AnyVOD_RenderRect[1] + AnyVOD_RenderRect[3] / 3.0 * 2.0;
          texFaceY[3] = AnyVOD_RenderRect[1] + AnyVOD_RenderRect[3];

        // Apply EAC transform
          if (AnyVOD_outTexCoords.s >= texFaceX[1])
            orgTextureRangeX = vec2(texFaceX[1], texFaceX[2]);
          else
            orgTextureRangeX = vec2(texFaceX[0], texFaceX[1]);

          if (AnyVOD_outTexCoords.t >= texFaceY[2])
            orgTextureRangeY = vec2(texFaceY[2], texFaceY[3]);
          else if (AnyVOD_outTexCoords.t >= texFaceY[1])
            orgTextureRangeY = vec2(texFaceY[1], texFaceY[2]);
          else
            orgTextureRangeY = vec2(texFaceY[0], texFaceY[1]);

        // Scaling coords by the coordinates following the range from -1.0 to 1.0.
          float px = AnyVOD_TransformCoord(orgTextureRangeX, eacCoordRange, AnyVOD_outTexCoords.s);
          float py = AnyVOD_TransformCoord(orgTextureRangeY, eacCoordRange, AnyVOD_outTexCoords.t);

          float qu = AnyVOD_INV_2PI * atan(px) + 0.5;
          float qv = AnyVOD_INV_2PI * atan(py) + 0.5;

        // Re-scaling coords by original coordinates ranges
          transformedCoord.s = AnyVOD_TransformCoord(texCoordRange, orgTextureRangeX, qu);
          transformedCoord.t = AnyVOD_TransformCoord(texCoordRange, orgTextureRangeY, qv);

          if (transformedCoord.s <= orgTextureRangeX[0] + onePixel.s)
            transformedCoord.s += onePixel.s;
          else if (transformedCoord.s >= orgTextureRangeX[1] - onePixel.s)
            transformedCoord.s -= onePixel.s;

          if (transformedCoord.t <= orgTextureRangeY[0] + onePixel.t)
            transformedCoord.t += onePixel.t;
          else if (transformedCoord.t >= orgTextureRangeY[1] - onePixel.t)
            transformedCoord.t -= onePixel.t;

          return transformedCoord;
        }

        float AnyVOD_toneMap(float val)
        {
          return val * (1.0 + val / (AnyVOD_toneParam * AnyVOD_toneParam)) / (1.0 + val);
        }

        void AnyVOD_colorConversion(inout vec4 texel, inout vec2 coord)
        {
          texel.rgb = pow(max(vec3(0.0), texel.rgb), vec3(AnyVOD_gammaSrc));
          texel.rgb = max(vec3(0.0), AnyVOD_primaryMatrix * texel.rgb);
          texel.rgb = pow(texel.rgb, vec3(AnyVOD_gammaDstInv));

          float luma = dot(texel.rgb, AnyVOD_coefsDst);

          texel.rgb *= AnyVOD_toneMap(luma) / luma;
        }

        vec4 AnyVOD_getTexel(in vec2 coord)
        {
          return )" + PIXEL_DECODER_SIG + R"(;
        }

        vec2 AnyVOD_getCoord()
        {
          return )" + TEX_COORD_DECODER_SIG + R"(;
        }

        )"
    );
}
