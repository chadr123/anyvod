﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "core/FrameMetaData.h"

#include <QMatrix3x3>
#include <QVector3D>

extern "C"
{
# include <libavutil/pixfmt.h>
}

class ColorConversion
{
private:
    ColorConversion();

public:
    struct Info
    {
        QMatrix3x3 matPrimary;
        float gammaSrc;
        float gammaDst;
    };

public:
    static Info getColorConversionInfo(AVColorPrimaries src, AVColorPrimaries dst);
    static QVector3D getRGBYuvCoefs(AVColorSpace colspace);
    static float getTone(const FrameMetaData &frameMeta);

private:
    struct Primaries
    {
        float primaries[3][2];
        float whitePoint[2];
    };

    struct ConvYCbCr
    {
        float kr;
        float kb;
    };

private:
    static const Primaries PRIMARIES_BT709;
    static const Primaries PRIMARIES_BT2020;
    static const ConvYCbCr CONV_BT709YCBCR;
    static const ConvYCbCr CONV_BT2020YCBCR;
};
