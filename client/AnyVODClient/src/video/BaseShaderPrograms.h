﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "../core/AnyVODEnums.h"

#include <QString>

extern "C"
{
# include <libavutil/pixfmt.h>
}

class BaseShaderPrograms
{
private:
    BaseShaderPrograms();

public:
    static QString getVertexFrame();
    static QString getShaderFrame();

    static QString getEffects();
    static QString getInterlace();
    static QString getCheckerBoard();
    static QString getAnaglyph();

    static QString getSubtitleInterlace();
    static QString getSubtitleCheckerBoard();
    static QString getSubtitleAnaglyph();
    static QString getBarrelDistortion();
    static QString getPincushionDistortion();

    static QString getSimple();
    static QString getLine();

    static QString getDefaultLibrary();

    static QString getPixelDecoder(AVPixelFormat format);
    static QString getTexCoordDecoder(AnyVODEnums::Video360Type type);
    static QString getDefaultTexCoordDecoder();

public:
    static const QString ENTRY_SIG;
    static const QString FUNCS_SIG;
    static const QString PIXEL_DECODER_SIG;
    static const QString TEX_COORD_DECODER_SIG;

private:
    static QString getShaderLibrary();
};
