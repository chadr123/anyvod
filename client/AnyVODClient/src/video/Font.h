﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "ShaderCompositer.h"

#include <QMatrix4x4>
#include <QPointF>
#include <QHash>
#include <QVector>
#include <QColor>
#include <QFont>
#include <QSize>
#include <QImage>

#include <qopengl.h>

#if defined Q_OS_MOBILE
class QOpenGLFunctions;
#endif

class Font
{
public:
    struct Context
    {
        Context(const QColor &_color, bool _bold = false, bool _underline = false, bool _italic = false, bool _strike = false)
        {
            color = _color;
            bold = _bold;
            underline = _underline;
            italic = _italic;
            strike = _strike;
        }

        bool bold;
        bool underline;
        bool italic;
        bool strike;
        QColor color;
    };

    Font();
    ~Font();

#if defined Q_OS_MOBILE
    void setGL(QOpenGLFunctions *gl);
#endif
    void setOrtho(const QMatrix4x4 &ortho);

    void clear();

    void setCacheMode(bool enable);
    bool isCacheMode() const;
    bool willBeInvalidate(int outlineSize) const;

    QFont& getQFont();
    int renderText(ShaderCompositer::ShaderType type,
                   const QPoint &pos, const QString &text, const Context &context,
                   int outlineSize, const QColor &outlineColor, float opaque,
                   AnyVODEnums::ScaleDirection direction, float scale, bool colorBlend);

private:
    struct Glyph
    {
        GLuint texture;
        QSize size;
        QSize imgSize;
        QPointF texCoord[2];
    };

    struct OutlineOffset
    {
        int x;
        int y;
    };

private:
    void createTexture();
    void drawOutlinedGlyph(QPainter &painter, int x, int y, const QChar &c, const QColor &outlineColor, int outline) const;
    const Glyph& getGlyph(const QChar &c, const Context &key, const QColor &outlineColor, int outlineSize, bool *reboundTexture);

private:
    int m_fontSize;
    int m_outlineSize;
    QFont m_font;
    QHash<Context, QHash<QChar, Glyph> > m_glyphs;
    QVector<GLuint> m_textures;
    QPoint m_texPos;
    OutlineOffset m_outlineTable[8];
#if defined Q_OS_MOBILE
    QOpenGLFunctions *m_gl;
#endif
    QMatrix4x4 m_ortho;
    int m_textureSize;
    QImage m_clearValue;
    bool m_cacheMode;
};

inline bool operator == (const Font::Context &r, const Font::Context &l)
{
    return r.bold == l.bold && r.underline == l.underline && r.strike == l.strike && r.color == l.color;
}

inline uint qHash(const Font::Context &key)
{
    uint effect = key.bold << 0 | key.underline << 1 | key.strike << 2;

    return qHash(key.color.rgba()) ^ effect;
}
