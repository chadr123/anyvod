﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "ScreenCaptureHelper.h"

#include <QStandardPaths>

const int ScreenCaptureHelper::DEFAULT_QUALITY_VALUE = -1;

ScreenCaptureHelper::ScreenCaptureHelper() :
#ifdef Q_OS_MOBILE
    m_ext("JPG"),
#else
    m_ext("jpg"),
#endif
    m_capture(false),
    m_captureOrg(false),
    m_captureCount(0),
    m_totalCount(0),
    m_quality(DEFAULT_QUALITY_VALUE),
    m_captureSize(1, 1),
    m_paused(false)
{

}

void ScreenCaptureHelper::start()
{
    this->m_capture = true;
    this->m_captureCount = this->m_totalCount;
}

void ScreenCaptureHelper::stop()
{
    this->m_capture = false;
    this->m_captureCount = 0;
    this->m_totalCount = 0;
}

bool ScreenCaptureHelper::isStarted() const
{
    return this->m_capture;
}

bool ScreenCaptureHelper::isRemained() const
{
    return this->m_captureCount > 0;
}

int ScreenCaptureHelper::getCapturedCount() const
{
    return this->m_totalCount - this->m_captureCount;
}

void ScreenCaptureHelper::reduceRemainedCount()
{
    this->m_captureCount--;
}

void ScreenCaptureHelper::setTotalCount(int count)
{
    this->m_totalCount = count;
}

int ScreenCaptureHelper::getTotalCount() const
{
    return this->m_totalCount;
}

void ScreenCaptureHelper::markPaused()
{
    this->m_paused = true;
}

void ScreenCaptureHelper::unmarkPaused()
{
    this->m_paused = false;
}

bool ScreenCaptureHelper::isPaused() const
{
    return this->m_paused;
}

void ScreenCaptureHelper::setCaptureByOriginalSize(bool on)
{
    this->m_captureOrg = on;
}

bool ScreenCaptureHelper::isCaptureByOriginalSize() const
{
    return this->m_captureOrg;
}

void ScreenCaptureHelper::setSavePath(const QString &path)
{
    this->m_savePath = path;
}

QString ScreenCaptureHelper::getSavePath() const
{
    return this->m_savePath;
}

void ScreenCaptureHelper::setExtension(const QString &ext)
{
    this->m_ext = ext;
}

QString ScreenCaptureHelper::getExtention() const
{
    return this->m_ext;
}

void ScreenCaptureHelper::setSize(const QSize &size)
{
    this->m_captureSize = size;
}

QSize ScreenCaptureHelper::getSize() const
{
    return this->m_captureSize;
}

void ScreenCaptureHelper::setQuality(int quality)
{
    this->m_quality = quality;
}

void ScreenCaptureHelper::resetToDefaultQuality()
{
    this->m_quality = DEFAULT_QUALITY_VALUE;
}

int ScreenCaptureHelper::getQuality() const
{
    return this->m_quality;
}

bool ScreenCaptureHelper::isDefaultQuality() const
{
    return this->m_quality == DEFAULT_QUALITY_VALUE;
}

QString ScreenCaptureHelper::getDefaultSavePath()
{
#ifdef Q_OS_MOBILE
    return QStandardPaths::writableLocation(QStandardPaths::PicturesLocation) + "/AnyVOD/";
#else
    const auto locations = QStandardPaths::standardLocations(QStandardPaths::DataLocation);

    return QString("%1/capture").arg(locations.first());
#endif
}
