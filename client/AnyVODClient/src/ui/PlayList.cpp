﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "ui_playlist.h"
#include "PlayList.h"
#include "MainWindowInterface.h"
#include "ScreenExplorer.h"
#include "Screen.h"
#include "core/FileIcons.h"
#include "core/FileExtensions.h"
#include "utils/SeparatingUtils.h"
#include "utils/MessageBoxUtils.h"
#include "utils/ConvertingUtils.h"
#include "utils/RemoteFileUtils.h"
#include "utils/DeviceUtils.h"
#include "factories/MainWindowFactory.h"
#include "media/MediaPlayer.h"
#include "setting/PlayListSettingLoader.h"
#include "setting/PlayListSettingSaver.h"
#include "parsers/playlist/PlayListParserInterface.h"
#include "parsers/playlist/PlayListParserGenerator.h"
#include "pickers/URLPicker.h"

#include "../../../../common/types.h"

#include <QFileInfo>
#include <QIcon>
#include <QMenu>
#include <QDropEvent>
#include <QScrollBar>
#include <QDir>
#include <QApplication>
#include <QClipboard>
#include <QTextStream>
#include <QMutexLocker>

const QEvent::Type PlayList::UPDATE_ITEM_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type PlayList::UPDATE_PARENT_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type PlayList::UPDATE_INC_COUNT = (QEvent::Type)QEvent::registerEventType();
const QSize PlayList::DEFAULT_PLAYLIST_SIZE = QSize(400, 290);
const int PlayList::UPDATE_PLAYLIST_TIME = 5000;
const int PlayList::MAX_RETRY_COUNT = 5;

QDataStream& operator << (QDataStream &out, const PlayItem &item)
{
    out << item.path;
    out << item.extraData.duration;
    out << item.extraData.totalFrame;
    out << item.extraData.valid;
    out << item.unique;
    out << item.title;
    out << item.totalTime;
    out << item.itemUpdated;
    out << item.extraData.userData;
    out << item.retry;

    return out;
}

QDataStream& operator >> (QDataStream &in, PlayItem &item)
{
    in >> item.path;
    in >> item.extraData.duration;
    in >> item.extraData.totalFrame;
    in >> item.extraData.valid;
    in >> item.unique;
    in >> item.title;
    in >> item.totalTime;
    in >> item.itemUpdated;
    in >> item.extraData.userData;
    in >> item.retry;

    if (item.unique.isNull())
        item.unique = QUuid::createUuid();

    return in;
}

PlayList::PlayList(QWidget *parent) :
    ListWidgetDialog(parent, Qt::Tool),
    ui(new Ui::PlayList),
    m_isInit(false),
    m_updater(this),
    m_updateTimerID(0),
    m_clearPlayListOnExit(false),
    m_currentPlayingIndex(-1),
    m_delShort(QKeySequence(Qt::Key_Delete), this, SLOT(deleteSelection()), nullptr),
    m_moveUpShort(QKeySequence(Qt::Key_Up | Qt::AltModifier), this, SLOT(moveUp()), nullptr),
    m_moveDownShort(QKeySequence(Qt::Key_Down | Qt::AltModifier), this, SLOT(moveDown()), nullptr),
    m_moveToTopShort(QKeySequence(Qt::Key_Home | Qt::AltModifier), this, SLOT(moveToTop()), nullptr),
    m_moveToBottomShort(QKeySequence(Qt::Key_End | Qt::AltModifier), this, SLOT(moveToBottom()), nullptr),
    m_viewPathShort(QKeySequence(Qt::Key_V | Qt::AltModifier), this, SLOT(viewPath()), nullptr),
    m_copyPathShort(QKeySequence(Qt::Key_C | Qt::ControlModifier), this, SLOT(copyPath()), nullptr),
    m_screenExplorerShort(QKeySequence(Qt::Key_E | Qt::AltModifier), this, SLOT(screenExplorer()), nullptr)
{
    ui->setupUi(this);

#ifdef Q_OS_MAC
    const auto btns = this->findChildren<QPushButton*>();

    for (QPushButton *btn : btns)
        btn->setAttribute(Qt::WA_LayoutUsesWidgetRect);

    const auto lists = this->findChildren<QListWidget*>();

    for (QListWidget *list : lists)
    {
        QFont font = list->font();

        font.setPointSizeF(12);
        list->setFont(font);
    }
#endif

    PlayListSettingLoader(this).load();

    this->setAcceptDrops(true);
    this->m_updateTimerID = this->startTimer(UPDATE_PLAYLIST_TIME);
}

PlayList::~PlayList()
{
    this->stop();

    if (this->m_clearPlayListOnExit)
        this->clearPlayList();

    delete ui;
}

QListWidgetItem* PlayList::createItem(QFileInfo &info, PlayItem &playItem) const
{
    QListWidgetItem *item = new QListWidgetItem;
    QIcon icon;
    QString fileName = info.fileName();
    QString filePath = info.filePath();

    if (DeviceUtils::determinDevice(filePath) || DeviceUtils::determinIsDeviceHaveDuration(filePath))
        playItem.path = QString::fromUtf8(playItem.extraData.userData);
    else
        playItem.path = SeparatingUtils::adjustNetworkPath(filePath);

    playItem.unique = QUuid::createUuid();

    if (RemoteFileUtils::determinRemoteProtocol(playItem.path) && !RemoteFileUtils::determinRemoteFile(playItem.path))
        icon = FileIcons::getInstance().getExtentionIcon("htm");
    else
        icon = FileIcons::getInstance().getExtentionIcon(info.suffix());

    if (playItem.title.isEmpty())
        item->setText(fileName);
    else
        item->setText(playItem.title);

    item->setIcon(icon);
    item->setData(Qt::UserRole, QVariant::fromValue(playItem));

    bool diplayFileName = true;

    if (RemoteFileUtils::determinRemoteProtocol(playItem.path) && !DeviceUtils::determinDevice(playItem.path) && !DeviceUtils::determinIsDeviceHaveDuration(playItem.path))
    {
        char path[MAX_FILEPATH_CHAR_SIZE];
        QString pathPart;

        av_url_split(nullptr, 0, nullptr, 0, nullptr, 0, nullptr, path, sizeof(path), playItem.path.toUtf8().constData());
        pathPart = QString::fromUtf8(path);

        diplayFileName = !QFileInfo(pathPart).suffix().isEmpty();
    }

    QString path;

    if (DeviceUtils::determinDevice(playItem.path) || DeviceUtils::determinIsDeviceHaveDuration(playItem.path))
        path = playItem.title;
    else if (fileName.isEmpty() || !diplayFileName)
        path = playItem.path;
    else
        path = fileName;

    this->setItemToolTip(item, path, playItem.totalTime);

    return item;
}

void PlayList::startUpdateThread()
{
    if (!this->m_updater.isRunning())
    {
        this->m_updater.setStop(false);
        this->m_updater.start();
    }
}

void PlayList::setItemToolTip(QListWidgetItem *item, const QString &text, double totalTime) const
{
    QString time;

    if (totalTime > 0.0)
    {
        ConvertingUtils::getTimeString(totalTime, ConvertingUtils::TIME_HH_MM_SS, &time);

        time = " (" + time + ")";
    }

    item->setToolTip(text + time);
}

int PlayList::findURLPickerIndex(const QString &quality, const QString &mime, const QVector<URLPickerInterface::Item> &items) const
{
    for (int i = 0; i < items.count(); i++)
    {
        const URLPickerInterface::Item &item = items[i];

        if (item.quality == quality && item.mimeType == mime)
            return i;
    }

    return 0;
}

void PlayList::setPlayList(const QVector<PlayItem> &list)
{
    this->clearPlayList();
    this->addPlayList(list);
    this->resetCurrentPlayingIndex();
}

void PlayList::setPlayListWithoutParentUpdate(const QVector<PlayItem> &list)
{
    this->clearPlayList();
    this->addPlayListWithoutParentWindowUpdate(list);
}

bool PlayList::addPlayList(const QVector<PlayItem> &list)
{
    return this->addPlayList(list, false);
}

bool PlayList::addPlayListWithoutParentWindowUpdate(const QVector<PlayItem> &list)
{
    return this->addPlayList(list, true);
}

void PlayList::getPlayList(QVector<PlayItem> *ret)
{
    QMutexLocker locker(&this->m_getPlayListMutex);

    for (int i = 0; i < this->getCount(); i++)
        ret->append(this->getPlayItem(i));
}

int PlayList::getCount() const
{
    return this->ui->list->count();
}

bool PlayList::exist() const
{
    return this->getCount() > 0;
}

QString PlayList::getFileName(int index) const
{
    if (this->exist() && index >= 0 && index < this->getCount())
        return this->ui->list->item(index)->text();
    else
        return QString();
}

QString PlayList::getCurrentFileName() const
{
    return this->getFileName(this->m_currentPlayingIndex);
}

PlayItem PlayList::getPlayItem(int index) const
{
    return this->ui->list->item(index)->data(Qt::UserRole).value<PlayItem>();
}

PlayItem PlayList::getCurrentPlayItem() const
{
    return this->getPlayItem(this->m_currentPlayingIndex);
}

int PlayList::findIndex(const QUuid &unique) const
{
    for (int i = 0; i < this->getCount(); i++)
    {
        PlayItem item = this->getPlayItem(i);

        if (item.unique == unique)
            return i;
    }

    return -1;
}

void PlayList::clearPlayList()
{
    this->stop();
    this->ui->list->clear();
}

void PlayList::stop()
{
    this->m_updater.setStop(true);
    this->m_updater.wait();
}

void PlayList::saveSettings()
{
    PlayListSettingSaver(this).save();
}

int PlayList::updateURLPickerData(const QUuid &unique, const QVector<URLPickerInterface::Item> &items)
{
    int index = this->findIndex(unique);

    if (index >= 0)
    {
        QListWidgetItem *item = this->ui->list->item(index);
        PlayItem data = item->data(Qt::UserRole).value<PlayItem>();

        data.urlPickerData = items;
        data.urlPickerIndex = this->findURLPickerIndex(this->m_lastURLPickerQuality, this->m_lastURLPickerMime, items);

        if (data.urlPickerIndex == 0)
        {
            this->m_lastURLPickerMime.clear();
            this->m_lastURLPickerQuality.clear();
        }

        item->setData(Qt::UserRole, QVariant::fromValue(data));

        return data.urlPickerIndex;
    }

    return 0;
}

void PlayList::selectItemOption(int index, QItemSelectionModel::SelectionFlags option)
{
    this->ui->list->setCurrentRow(index, option);
}

void PlayList::selectItem(int index)
{
    this->selectItemOption(index, QItemSelectionModel::ClearAndSelect);
}

void PlayList::dragEnterEvent(QDragEnterEvent *event)
{
    MainWindowFactory::getInstance()->processDragEnterEvent(event);
}

void PlayList::dropEvent(QDropEvent *event)
{
    QDropEvent e(event->posF(), event->dropAction(),
                 event->mimeData(), event->mouseButtons(),
                 event->keyboardModifiers() | Qt::ControlModifier,
                 event->type());

    MainWindowFactory::getInstance()->processDropEvent(&e);
    event->accept();
}

void PlayList::contextMenuEvent(QContextMenuEvent *)
{
    QMenu menu;
    QListWidget *listWidget = this->ui->list;
    const QList<QListWidgetItem*> &list = this->sortByRow(listWidget->selectedItems(), *listWidget);
    int startRow;
    int endRow;

    if (list.empty())
    {
        startRow = -1;
        endRow = 1;
    }
    else
    {
        startRow = listWidget->row(list.first()) - 1;
        endRow = listWidget->row(list.last()) + 1;
    }

    QAction *action = nullptr;

    action = menu.addAction(tr("삭제"));
    action->setShortcut(this->m_delShort.key());
    action->setShortcutVisibleInContextMenu(true);
    action->setEnabled(list.count() > 0);
    connect(action, &QAction::triggered, this, &PlayList::deleteSelection);

    menu.addSeparator();

    action = menu.addAction(tr("맨 위로"));
    action->setShortcut(this->m_moveToTopShort.key());
    action->setShortcutVisibleInContextMenu(true);
    action->setEnabled(startRow >= 0);
    connect(action, &QAction::triggered, this, &PlayList::moveToTop);

    action = menu.addAction(tr("맨 아래로"));
    action->setShortcut(this->m_moveToBottomShort.key());
    action->setShortcutVisibleInContextMenu(true);
    action->setEnabled(endRow < listWidget->count());
    connect(action, &QAction::triggered, this, &PlayList::moveToBottom);

    menu.addSeparator();

    action = menu.addAction(tr("위로"));
    action->setShortcut(this->m_moveUpShort.key());
    action->setShortcutVisibleInContextMenu(true);
    action->setEnabled(startRow >= 0);
    connect(action, &QAction::triggered, this, &PlayList::moveUp);

    action = menu.addAction(tr("아래로"));
    action->setShortcut(this->m_moveDownShort.key());
    action->setShortcutVisibleInContextMenu(true);
    action->setEnabled(endRow < listWidget->count());
    connect(action, &QAction::triggered, this, &PlayList::moveDown);

    menu.addSeparator();

    action = menu.addAction(tr("파일 경로 보기"));
    action->setShortcut(this->m_viewPathShort.key());
    action->setShortcutVisibleInContextMenu(true);
    action->setEnabled(listWidget->selectedItems().count() == 1);
    connect(action, &QAction::triggered, this, &PlayList::viewPath);

    action = menu.addAction(tr("파일 경로 복사"));
    action->setShortcut(this->m_copyPathShort.key());
    action->setShortcutVisibleInContextMenu(true);
    action->setEnabled(listWidget->selectedItems().count() == 1);
    connect(action, &QAction::triggered, this, &PlayList::copyPath);

    bool enable = false;
    QAction *subAction = nullptr;
    QMenu *subMenu = nullptr;

    subMenu = menu.addMenu(tr("다른 화질"));

    if (listWidget->selectedItems().count() == 1)
    {
        const QList<QListWidgetItem*> &list = listWidget->selectedItems();
        const PlayItem &playItem = list.first()->data(Qt::UserRole).value<PlayItem>();

        enable = !playItem.urlPickerData.isEmpty();

        for (int i = 0; i < playItem.urlPickerData.count(); i++)
        {
            const URLPickerInterface::Item &item = playItem.urlPickerData[i];

            subAction = subMenu->addAction(item.desc);
            subAction->setCheckable(true);
            subAction->setChecked(playItem.urlPickerIndex == i);
            connect(subAction, &QAction::triggered, [this, i]() { this->selectOtherQuality(i); });
        }
    }

    subMenu->setEnabled(enable);

    menu.addSeparator();

    enable = false;

    if (listWidget->selectedItems().count() == 1)
    {
        PlayItem data = list[0]->data(Qt::UserRole).value<PlayItem>();
        bool movieExt = FileExtensions::MOVIE_EXTS_LIST.contains(QFileInfo(data.path).suffix(), Qt::CaseInsensitive);

        if ((!RemoteFileUtils::determinRemoteFile(data.path) && !DeviceUtils::determinDevice(data.path) && !DeviceUtils::determinIsDeviceHaveDuration(data.path) && movieExt) ||
                (!RemoteFileUtils::determinRemoteFile(data.path) && RemoteFileUtils::determinRemoteProtocol(data.path)))
            enable = true;
    }

    action = menu.addAction(tr("장면 탐색"));
    action->setShortcut(this->m_screenExplorerShort.key());
    action->setShortcutVisibleInContextMenu(true);
    action->setEnabled(enable);
    connect(action, &QAction::triggered, this, &PlayList::screenExplorer);

    Screen *screen = Screen::getInstance();

    screen->setDisableHideCusor(true);
    menu.exec(QCursor::pos());
    screen->setDisableHideCusor(false);
}

void PlayList::resizeEvent(QResizeEvent *)
{
    if (!this->m_isInit)
    {
        QWidget *parent = this->parentWidget();
        QPoint pos(parent->x() + parent->frameGeometry().width(), parent->y());
        int height = parent->size().height();

        if (height < DEFAULT_PLAYLIST_SIZE.height())
            height = DEFAULT_PLAYLIST_SIZE.height();

        this->move(pos);
        this->resize(QSize(this->size().width(), height));

        this->m_isInit = true;
    }
}

void PlayList::changeEvent(QEvent *event)
{
    switch (event->type())
    {
        case QEvent::LanguageChange:
        {
            this->ui->retranslateUi(this);
            event->accept();

            break;
        }
        default:
        {
            break;
        }
    }
}

void PlayList::customEvent(QEvent *event)
{
    if (event->type() == UPDATE_ITEM_EVENT)
    {
        UpdateItemEvent *e = (UpdateItemEvent*)event;
        int index = this->findIndex(e->getUnique());

        if (index >= 0)
        {
            QListWidgetItem *item = this->ui->list->item(index);
            PlayItem data = item->data(Qt::UserRole).value<PlayItem>();
            QString title = e->getTitle();
            QString toolTip = item->toolTip();

            data.title = title;
            data.totalTime = e->getTotalTime();
            data.itemUpdated = true;

            if (!title.isEmpty())
                item->setText(data.title);

            if (data.totalTime > 0.0)
                this->setItemToolTip(item, toolTip, data.totalTime);

            item->setData(Qt::UserRole, QVariant::fromValue(data));
        }
    }
    else if (event->type() == UPDATE_INC_COUNT)
    {
        UpdateIncCountEvent *e = (UpdateIncCountEvent*)event;
        int index = this->findIndex(e->getUnique());

        if (index >= 0)
        {
            QListWidgetItem *item = this->ui->list->item(index);
            PlayItem data = item->data(Qt::UserRole).value<PlayItem>();

            data.retry++;

            if (data.retry > MAX_RETRY_COUNT)
                data.itemUpdated = true;

            item->setData(Qt::UserRole, QVariant::fromValue(data));
        }
    }
    else if (event->type() == UPDATE_PARENT_EVENT)
    {
        this->updateParentWindow(true);
    }
    else
    {
        QDialog::customEvent(event);
    }
}

void PlayList::timerEvent(QTimerEvent *event)
{
    if (event->timerId() == this->m_updateTimerID)
        this->startUpdateThread();
}

void PlayList::deleteItem(int index)
{
    QListWidget *list = this->ui->list;
    int scroll = list->verticalScrollBar()->value();
    QListWidgetItem *item = list->takeItem(index);
    int oldIndex = index;

    this->stop();

    delete item;
    list->verticalScrollBar()->setValue(scroll);
    this->updateParentWindow(true);

    if (index >= list->count())
        index = list->count() - 1;
    else if (index < 0)
        index = 0;

    this->selectItem(index);
    this->m_currentPlayingIndex = oldIndex;
}

bool PlayList::selectOtherQualityByCurrentItem(int quality)
{
    const QList<QListWidgetItem*> &list = this->ui->list->selectedItems();

    if (list.count() == 1)
    {
        PlayItem playItem = list[0]->data(Qt::UserRole).value<PlayItem>();
        MainWindowInterface *window = MainWindowFactory::getInstance();

        if (playItem.urlPickerData.isEmpty() || playItem.urlPickerIndex == quality)
            return false;

        playItem.urlPickerIndex = quality;

        this->m_lastURLPickerMime = playItem.urlPickerData[quality].mimeType;
        this->m_lastURLPickerQuality = playItem.urlPickerData[quality].quality;

        list[0]->setData(Qt::UserRole, QVariant::fromValue(playItem));

        window->playAt(this->ui->list->currentRow());
        window->activateWindow();

        return true;
    }

    return false;
}

void PlayList::setFixItemSize(bool fix)
{
    this->ui->list->setUniformItemSizes(fix);
}

void PlayList::setClearPlayListOnExit(bool clear)
{
    this->m_clearPlayListOnExit = clear;
}

bool PlayList::isClearPlayListOnExit() const
{
    return this->m_clearPlayListOnExit;
}

int PlayList::getCurrentPlayingIndex() const
{
    return this->m_currentPlayingIndex;
}

void PlayList::setRandomCurrentPlayingIndex()
{
    this->m_currentPlayingIndex = rand() % this->getCount();
}

void PlayList::resetCurrentPlayingIndex()
{
    this->m_currentPlayingIndex = -1;
}

void PlayList::decreaseCurrentPlayingIndex()
{
    this->m_currentPlayingIndex--;
}

void PlayList::increaseCurrentPlayingIndex()
{
    this->m_currentPlayingIndex++;
}

void PlayList::setCurrentPlayingIndex(int index)
{
    this->m_currentPlayingIndex = index;
}

bool PlayList::setCurrentPlayingIndexByUnique(const QUuid &unique)
{
    if (unique == QUuid())
    {
        this->resetCurrentPlayingIndex();
        return false;
    }
    else
    {
        this->m_currentPlayingIndex = this->findIndex(unique);
        return true;
    }
}

void PlayList::adjustPlayIndex()
{
    if (this->m_currentPlayingIndex >= this->getCount())
        this->m_currentPlayingIndex = this->getCount() - 1;
    else if (this->m_currentPlayingIndex < 0)
        this->m_currentPlayingIndex = 0;
    else if (this->getCount() <= 0)
        this->resetCurrentPlayingIndex();
}

void PlayList::moveItems(const QList<QListWidgetItem*> &list, int rowTo, bool dirBottom)
{
    QListWidget *listWidget = this->ui->list;
    QList<QListWidgetItem*> sorted = this->sortByRow(list, *listWidget);
    int scroll = listWidget->verticalScrollBar()->value();
    int curIndex = -1;

    if (sorted.count() > 0)
    {
        if (dirBottom)
            curIndex = listWidget->row(sorted.first());
        else
            curIndex = listWidget->row(sorted.last());
    }

    this->disbleUpdates();

    for (int i = 0; i < sorted.count(); i++)
    {
        if (rowTo >= 0 && rowTo <= listWidget->count())
        {
            QListWidgetItem *item = sorted[i];
            int row = listWidget->row(item);

            item = listWidget->takeItem(row);
            listWidget->insertItem(rowTo, item);
        }

        if (!dirBottom)
            rowTo++;
    }

    this->enableUpdates();

    listWidget->verticalScrollBar()->setValue(scroll);
    this->updateParentWindow(true);

    if (curIndex >= listWidget->count())
        curIndex = listWidget->count() - 1;

    if (curIndex >= 0)
        this->selectItem(curIndex);
}

bool PlayList::addPlayList(const QVector<PlayItem> &list, bool withoutParentUpdate)
{
    bool added = false;

    this->disbleUpdates();

    for (int i = 0; i < list.count(); i++)
    {
        PlayItem playItem = list[i];
        QFileInfo info(playItem.path);
        QString suffix = info.suffix();
        PlayListParserGenerator gen;
        PlayListParserInterface *parser = gen.getParser(suffix);

        if (parser)
        {
            QVector<PlayListParserInterface::PlayListItem> fileList;

            parser->setRoot(info.absolutePath() + QDir::separator());

            if (parser->open(playItem.path))
            {
                parser->getFileList(&fileList);
                parser->close();

                for (const PlayListParserInterface::PlayListItem &item : qAsConst(fileList))
                {
                    QFileInfo fileInfo(item.path);
                    PlayItem fileItem;

                    fileItem.title = item.title;
                    fileItem.itemUpdated = true;

                    this->ui->list->addItem(this->createItem(fileInfo, fileItem));
                    added = true;
                }
            }
            else if (!parser->isSuccess())
            {
                playItem.itemUpdated = true;

                this->ui->list->addItem(this->createItem(info, playItem));
                added = true;
            }

            delete parser;
        }
        else
        {
            this->ui->list->addItem(this->createItem(info, playItem));
            added = true;
        }
    }

    this->enableUpdates();

    if (!withoutParentUpdate)
        this->updateParentWindow(true);

    this->startUpdateThread();

    return added;
}

void PlayList::updateParentWindow(bool updateTitle)
{
    MainWindowInterface *window = MainWindowFactory::getInstance();
    int curIndex = this->m_currentPlayingIndex;

    if (this->setCurrentPlayingIndexByUnique(window->getUniqueInPlayer()))
    {
        if (this->m_currentPlayingIndex < 0)
            this->m_currentPlayingIndex = curIndex - 1;
    }

    this->adjustPlayIndex();
    window->updateNavigationButtons();

    if (updateTitle && MediaPlayer::getInstance().isOpened())
        window->updateWindowTitle();
}

void PlayList::disbleUpdates()
{
    QListWidget *listWidget = this->ui->list;

    listWidget->setUpdatesEnabled(false);
}

void PlayList::enableUpdates()
{
    QListWidget *listWidget = this->ui->list;

    listWidget->setUpdatesEnabled(true);
}

void PlayList::deleteSelection()
{
    QListWidget *list = this->ui->list;
    const QList<QListWidgetItem*> &selectedItems = this->sortByRow(list->selectedItems(), *list);
    int scroll = list->verticalScrollBar()->value();
    QListWidgetItem *curItem = list->item(this->m_currentPlayingIndex);
    bool updateTitle = true;
    int curIndex = -1;

    this->stop();

    if (selectedItems.count() > 0)
        curIndex = list->row(selectedItems.first());

    this->disbleUpdates();

    for (int i = 0; i < selectedItems.count(); i++)
    {
        QListWidgetItem *item = selectedItems[i];

        if (item == curItem)
            updateTitle = false;

        delete item;
    }

    this->enableUpdates();

    list->verticalScrollBar()->setValue(scroll);
    this->updateParentWindow(updateTitle);

    if (curIndex >= list->count())
        curIndex = list->count() - 1;

    if (curIndex >= 0)
        this->selectItem(curIndex);
}

void PlayList::moveToTop()
{
    this->moveItems(this->ui->list->selectedItems(), 0, false);
}

void PlayList::moveToBottom()
{
    this->moveItems(this->ui->list->selectedItems(), this->ui->list->count() - 1, true);
}

void PlayList::moveUp()
{
    QListWidget *listWidget = this->ui->list;
    const QList<QListWidgetItem*> &list = this->sortByRow(listWidget->selectedItems(), *listWidget);
    int scroll = listWidget->verticalScrollBar()->value();

    this->disbleUpdates();

    if (list.count() > 0)
    {
        for (int i = 0; i < list.count(); i++)
        {
            int select = listWidget->row(list[i]);

            if (select <= 0)
            {
                this->enableUpdates();
                return;
            }

            QListWidgetItem *item = listWidget->takeItem(select);

            listWidget->insertItem(select - 1, item);
            item->setSelected(true);
        }
    }

    this->enableUpdates();

    listWidget->verticalScrollBar()->setValue(scroll);
    this->updateParentWindow(true);
    this->selectItemOption(listWidget->row(list.first()), QItemSelectionModel::Current);
}

void PlayList::moveDown()
{
    QListWidget *listWidget = this->ui->list;
    const QList<QListWidgetItem*> &list = this->sortByRow(listWidget->selectedItems(), *listWidget);
    int scroll = listWidget->verticalScrollBar()->value();

    this->disbleUpdates();

    if (list.count() > 0)
    {
        for (int i = list.count() - 1; i >= 0; i--)
        {
            int select = listWidget->row(list[i]);

            if (select >= listWidget->count() - 1)
            {
                this->enableUpdates();
                return;
            }

            QListWidgetItem *item = listWidget->takeItem(select);

            listWidget->insertItem(select + 1, item);
            item->setSelected(true);
        }
    }

    this->enableUpdates();

    listWidget->verticalScrollBar()->setValue(scroll);
    this->updateParentWindow(true);
    this->selectItemOption(listWidget->row(list.last()), QItemSelectionModel::Current);
}

void PlayList::viewPath()
{
    const QList<QListWidgetItem*> &list = this->ui->list->selectedItems();

    if (list.count() == 1)
    {
        const PlayItem &playItem = list[0]->data(Qt::UserRole).value<PlayItem>();
        QString path = playItem.path;

        if (!RemoteFileUtils::determinRemoteProtocol(path))
            path = QDir::toNativeSeparators(path);

        MessageBoxUtils::informationMessageBox(this, path);
    }
}

void PlayList::copyPath()
{
    QClipboard *clipboard = QApplication::clipboard();
    const QList<QListWidgetItem*> &list = this->ui->list->selectedItems();
    QString pathList;
    QTextStream stream(&pathList);

    for (QListWidgetItem *item : list)
    {
        const PlayItem &data = item->data(Qt::UserRole).value<PlayItem>();

        if (RemoteFileUtils::determinRemoteProtocol(data.path))
            stream << data.path << Qt::endl;
        else
            stream << QDir::toNativeSeparators(data.path) << Qt::endl;
    }

    clipboard->setText(pathList);
}

void PlayList::screenExplorer()
{
    const QList<QListWidgetItem*> &list = this->ui->list->selectedItems();

    if (list.count() == 1)
    {
        const PlayItem &playItem = list[0]->data(Qt::UserRole).value<PlayItem>();
        QString path;
        bool movieExt = FileExtensions::MOVIE_EXTS_LIST.contains(QFileInfo(playItem.path).suffix(), Qt::CaseInsensitive);
        bool enable = false;

        if ((!RemoteFileUtils::determinRemoteFile(playItem.path) && movieExt) ||
                RemoteFileUtils::determinRemoteProtocol(playItem.path) ||
                !DeviceUtils::determinDevice(playItem.path) ||
                !DeviceUtils::determinIsDeviceHaveDuration(playItem.path))
            enable = true;

        if (!enable)
            return;

        QVector<URLPickerInterface::Item> urls;

        if (playItem.urlPickerData.isEmpty())
        {
            URLPicker picker;

            urls = picker.pickURL(playItem.path);
        }
        else
        {
            urls = playItem.urlPickerData;
        }

        if (urls.isEmpty())
            path = playItem.path;
        else
            path = urls[playItem.urlPickerIndex].url;

        ScreenExplorer dlg(path, this->ui->list->currentRow(), this->window());

        dlg.exec();
    }
}

void PlayList::selectOtherQuality(int quality)
{
    this->selectOtherQualityByCurrentItem(quality);
}

void PlayList::toggleClearPlayListOnExit()
{
    this->setClearPlayListOnExit(!this->isClearPlayListOnExit());
}

void PlayList::on_list_itemActivated(QListWidgetItem *)
{
    MainWindowInterface *window = MainWindowFactory::getInstance();

    window->playAt(this->ui->list->currentRow());
    window->activateWindow();
}
