﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "UserAspectRatio.h"
#include "ui_useraspectratio.h"

UserAspectRatio::UserAspectRatio(double width, double height, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UserAspectRatio),
    m_size(1.0, 1.0)
{
    ui->setupUi(this);

#ifdef Q_OS_MAC
    const auto btns = this->findChildren<QPushButton*>();

    for (QPushButton *btn : btns)
        btn->setAttribute(Qt::WA_LayoutUsesWidgetRect);
#endif

    this->ui->width->setValue(width);
    this->ui->height->setValue(height);
}

UserAspectRatio::~UserAspectRatio()
{
    delete ui;
}

void UserAspectRatio::getSize(QSizeF *ret) const
{
    *ret = this->m_size;
}

void UserAspectRatio::changeEvent(QEvent *event)
{
    switch (event->type())
    {
        case QEvent::LanguageChange:
        {
            this->ui->retranslateUi(this);
            event->accept();

            break;
        }
        default:
        {
            break;
        }
    }
}

void UserAspectRatio::on_ok_clicked()
{
    this->m_size = QSizeF(this->ui->width->value(), this->ui->height->value());
    this->accept();
}
