﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "media/MediaPresenter.h"

#include <QVector>
#include <QDialog>
#include <QShortcut>

namespace Ui
{
    class SkipRange;
}

class QTreeWidgetItem;

class SkipRange : public QDialog
{
    Q_OBJECT
public:
    explicit SkipRange(QVector<MediaPresenter::Range> *ret, QWidget *parent = nullptr);
    ~SkipRange();

private slots:
    void on_ok_clicked();
    void on_add_clicked();
    void on_del_clicked();
    void on_startTime_timeChanged(const QTime &date);
    void on_length_valueChanged(int value);
    void on_skipRanges_itemClicked(QTreeWidgetItem *item, int);
    void on_skipRanges_itemActivated(QTreeWidgetItem *item, int);

private:
    virtual void changeEvent(QEvent *event);

private:
    void initHeader();
    void pushBack(const MediaPresenter::Range &range);
    void changeRange(const QTime &time, int length);
    bool existRange(const MediaPresenter::Range &range) const;

private:
    Ui::SkipRange *ui;
    QVector<MediaPresenter::Range> *m_ranges;
    QShortcut m_delShort;
};
