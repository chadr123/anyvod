﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "core/TextureInfo.h"
#include "core/Lyrics.h"
#include "media/MediaPlayer.h"
#include "video/ScreenCaptureHelper.h"

#include <QGLWidget>
#include <QEvent>
#include <QElapsedTimer>

class ShaderCompositer;
class QOpenGLFramebufferObject;

class Screen : public QGLWidget
{
    Q_OBJECT
public:
    static const QEvent::Type PLAYING_EVENT;
    static const QEvent::Type EMPTY_BUFFER_EVENT;
    static const QEvent::Type AUDIO_OPTION_DESC_EVENT;
    static const QEvent::Type AUDIO_SUBTITLE_EVENT;
    static const QEvent::Type ABORT_EVENT;
    static const QEvent::Type NONE_PLAYING_DESC_EVENT;
    static const QEvent::Type STATUS_CHANGE_EVENT;

public:
    class PlayingEvent : public QEvent
    {
    public:
        PlayingEvent() :
            QEvent(PLAYING_EVENT)
        {

        }
    };

    class EmptyBufferEvent : public QEvent
    {
    public:
        EmptyBufferEvent(bool empty) :
            QEvent(EMPTY_BUFFER_EVENT),
            m_empty(empty)
        {

        }

        bool isEmpty() const { return this->m_empty; }

    private:
        bool m_empty;
    };

    class ShowAudioOptionDescEvent : public QEvent
    {
    public:
        ShowAudioOptionDescEvent(const QString &desc, bool show) :
            QEvent(AUDIO_OPTION_DESC_EVENT),
            m_desc(desc),
            m_show(show)
        {

        }

        QString getDesc() const { return this->m_desc; }
        bool getShow() const { return this->m_show; }

    private:
        QString m_desc;
        bool m_show;
    };

    class AudioSubtitleEvent : public QEvent
    {
    public:
        AudioSubtitleEvent(const QVector<Lyrics> &lines) :
            QEvent(AUDIO_SUBTITLE_EVENT),
            m_lines(lines)
        {

        }

        QVector<Lyrics> getLines() const { return this->m_lines; }

    private:
        QVector<Lyrics> m_lines;
    };

    class AbortEvent : public QEvent
    {
    public:
        AbortEvent(int reason) :
            QEvent(ABORT_EVENT),
            m_reason(reason)
        {

        }

        int getReason() const { return this->m_reason; }

    private:
        int m_reason;
    };

    class NonePlayingDescEvent : public QEvent
    {
    public:
        NonePlayingDescEvent(const QString &desc) :
            QEvent(NONE_PLAYING_DESC_EVENT),
            m_desc(desc)
        {

        }

        QString getDesc() const { return this->m_desc; }

    private:
        QString m_desc;
    };

    class StatusChangeEvent : public QEvent
    {
    public:
        StatusChangeEvent(const MediaPlayer::Status status) :
            QEvent(STATUS_CHANGE_EVENT),
            m_status(status)
        {

        }

        MediaPlayer::Status getStatus() const { return this->m_status; }

    private:
        MediaPlayer::Status m_status;
    };

    Screen(QWidget *parent = nullptr, Qt::WindowFlags flags = Qt::WindowFlags());
    ~Screen();

    void initTextures();
    void tearDown();

    void showContextMenu();

    ScreenCaptureHelper& getCaptureHelper();

    void toggleUseVSync();
    void setUseVSync(bool use);
    bool isUseVSync() const;

    void setDisableHideCusor(bool disable);
    bool isHDRSupported() const;
    bool isHDRTextureSupported() const;

    void setLastVisible(bool visible);
    bool getLastVisible() const;

    void setLastSize(const QSize &size);
    QSize getLastSize() const;

    void setFullScreenMode(bool enable);
    void markAsReady();

public:
    static Screen* getInstance();

signals:
    void screenUpdated();

protected:
    virtual void customEvent(QEvent *event);
    virtual void contextMenuEvent(QContextMenuEvent *);
    virtual void paintGL();
    virtual void timerEvent(QTimerEvent *event);
    virtual void mouseMoveEvent(QMouseEvent *event);
    virtual void mousePressEvent(QMouseEvent *event);
    virtual void mouseReleaseEvent(QMouseEvent *event);
    virtual void wheelEvent(QWheelEvent *event);
    virtual bool event(QEvent *event);
    virtual void initializeGL();
    virtual void resizeGL(int width, int height);

private:
    static void playingCallback(void *userData);
    static void statusChangedCallback(void *userData);
    static void emptyBufferCallback(void *userData, bool empty);
    static void showAudioOptionDescCallback(void *userData, const QString &desc, bool show);
    static void audioSubtitleCallback(void *userData, const QVector<Lyrics> &lines);
    static void paintCallback(void *userData);
    static void abortCallback(void *userData, int reason);
    static void nonePlayingDescCallback(void *userData, const QString &desc);

private:
#ifdef Q_OS_WIN
    void initOpenGLFunctions();
#endif

    void setPlayerCallbacks();
    void unSetPlayerCallbacks();
    void setPlayerCallbacksWith(void *listener);

    void genTexture(TextureID type);
    void deleteTexture(TextureID type);

    void genOffScreen(int size);

    void setWaitVisible(bool visible);
    void renderWait();

    void updateMovie(bool visible);

    void capturePrologue();
    void captureEpilogue(bool captureBound);
    void renderCapturePicture(const QRect &picArea, const QSizeF &texSize);

public:
    static const QSize DEFAULT_SCREEN_SIZE;
    static const int SCREEN_TOP_OFFSET;

private:
    static QString OBJECT_NAME;
    static const int CURSOR_TIME;

private:
    MediaPlayer &m_player;
    int m_cursorTimer;
    QElapsedTimer m_waitTimer;
    int m_waitCounter;
    bool m_visibleWait;
    bool m_disableHideCursor;
    bool m_useVSync;
    bool m_render;
    bool m_initGLUnits;
#ifdef Q_OS_WIN
    bool m_initOpenGLFunctions;
#endif
    bool m_isReady;
    bool m_isFullScreening;
    bool m_lastVisible;
    QSize m_lastSize;
    QOpenGLFramebufferObject *m_offScreen;
    ShaderCompositer &m_shader;
    TextureInfo m_texInfo[TEX_COUNT];
    ScreenCaptureHelper m_captureHelper;
};
