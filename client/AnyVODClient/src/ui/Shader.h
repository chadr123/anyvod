﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "ListWidgetDialog.h"

#include <QItemSelectionModel>
#include <QShortcut>

namespace Ui
{
    class Shader;
}

class ShaderCompositer;

class Shader : public ListWidgetDialog
{
    Q_OBJECT
public:
    explicit Shader(QWidget *parent = nullptr);
    ~Shader();

public:
    static const QString SHADER_EXTS;
    static const QStringList SHADER_EXTS_LIST;

private:
    void selectItemOption(int index, QItemSelectionModel::SelectionFlags option);
    void selectItem(int index);
    void addShaderList(const QStringList &paths);
    void moveItems(const QList<QListWidgetItem*> &list, int rowTo, bool dirBottom);

private:
    virtual void dragEnterEvent(QDragEnterEvent *event);
    virtual void dropEvent(QDropEvent *event);
    virtual void contextMenuEvent(QContextMenuEvent *);
    virtual void changeEvent(QEvent *event);

private slots:
    void deleteSelection();
    void moveToTop();
    void moveToBottom();
    void moveUp();
    void moveDown();

private slots:
    void on_add_clicked();
    void on_apply_clicked();

private:
    Ui::Shader *ui;
    ShaderCompositer &m_compositer;
    QShortcut m_delShort;
    QShortcut m_moveUpShort;
    QShortcut m_moveDownShort;
    QShortcut m_moveToTopShort;
    QShortcut m_moveToBottomShort;
};
