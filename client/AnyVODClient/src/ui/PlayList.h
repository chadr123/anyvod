﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "PlayListUpdater.h"
#include "ListWidgetDialog.h"
#include "core/PlayItem.h"
#include "pickers/URLPickerInterface.h"

#include <QList>
#include <QVariant>
#include <QUuid>
#include <QShortcut>
#include <QItemSelectionModel>
#include <QEvent>
#include <QMutex>

class QFileInfo;

namespace Ui
{
    class PlayList;
}

class PlayList : public ListWidgetDialog
{
    Q_OBJECT
public:
    static const QEvent::Type UPDATE_ITEM_EVENT;
    static const QEvent::Type UPDATE_PARENT_EVENT;
    static const QEvent::Type UPDATE_INC_COUNT;

public:
    class UpdateItemEvent : public QEvent
    {
    public:
        UpdateItemEvent(const QString &title, double totalTime, const QUuid &unique) :
            QEvent(UPDATE_ITEM_EVENT),
            m_title(title),
            m_totalTime(totalTime),
            m_unique(unique)
        {

        }

        QString getTitle() const { return this->m_title; }
        double getTotalTime() const { return this->m_totalTime; }
        QUuid getUnique() const { return this->m_unique; }

    private:
        QString m_title;
        double m_totalTime;
        QUuid m_unique;
    };

    class UpdateParentEvent : public QEvent
    {
    public:
        UpdateParentEvent() :
            QEvent(UPDATE_PARENT_EVENT)
        {

        }
    };

    class UpdateIncCountEvent : public QEvent
    {
    public:
        UpdateIncCountEvent(const QUuid &unique) :
            QEvent(UPDATE_INC_COUNT),
            m_unique(unique)
        {

        }

        QUuid getUnique() const { return this->m_unique; }

    private:
        QUuid m_unique;
    };

public:
    explicit PlayList(QWidget *parent = nullptr);
    ~PlayList();

    void setPlayList(const QVector<PlayItem> &list);
    void setPlayListWithoutParentUpdate(const QVector<PlayItem> &list);
    bool addPlayList(const QVector<PlayItem> &list);
    void getPlayList(QVector<PlayItem> *ret);
    void clearPlayList();
    void stop();

    void saveSettings();

    int updateURLPickerData(const QUuid &unique, const QVector<URLPickerInterface::Item> &items);

    int getCount() const;
    bool exist() const;

    QString getFileName(int index) const;
    QString getCurrentFileName() const;

    PlayItem getPlayItem(int index) const;
    PlayItem getCurrentPlayItem() const;

    int findIndex(const QUuid &unique) const;

    void selectItemOption(int index, QItemSelectionModel::SelectionFlags option);
    void selectItem(int index);
    void deleteItem(int index);

    bool selectOtherQualityByCurrentItem(int quality);

    void setFixItemSize(bool fix);

    void setClearPlayListOnExit(bool clear);
    bool isClearPlayListOnExit() const;

    int getCurrentPlayingIndex() const;

    void setCurrentPlayingIndex(int index);
    void setRandomCurrentPlayingIndex();
    bool setCurrentPlayingIndexByUnique(const QUuid &unique);
    void resetCurrentPlayingIndex();

    void decreaseCurrentPlayingIndex();
    void increaseCurrentPlayingIndex();

    void adjustPlayIndex();

private:
    virtual void dragEnterEvent(QDragEnterEvent *event);
    virtual void dropEvent(QDropEvent *event);
    virtual void resizeEvent(QResizeEvent *);
    virtual void contextMenuEvent(QContextMenuEvent *);
    virtual void changeEvent(QEvent *event);
    virtual void customEvent(QEvent *event);
    virtual void timerEvent(QTimerEvent *event);

public slots:
    void deleteSelection();
    void moveToTop();
    void moveToBottom();
    void moveUp();
    void moveDown();
    void viewPath();
    void copyPath();
    void screenExplorer();
    void selectOtherQuality(int quality);
    void toggleClearPlayListOnExit();

private slots:
    void on_list_itemActivated(QListWidgetItem *);

private:
    QListWidgetItem* createItem(QFileInfo &info, PlayItem &playItem) const;
    void moveItems(const QList<QListWidgetItem*> &list, int rowTo, bool dirBottom);
    bool addPlayListWithoutParentWindowUpdate(const QVector<PlayItem> &list);
    bool addPlayList(const QVector<PlayItem> &list, bool withoutParentUpdate);

    void disbleUpdates();
    void enableUpdates();

    void updateParentWindow(bool updateTitle);
    void startUpdateThread();
    void setItemToolTip(QListWidgetItem *item, const QString &text, double totalTime) const;
    int findURLPickerIndex(const QString &quality, const QString &mime, const QVector<URLPickerInterface::Item> &items) const;

private:
    static const QSize DEFAULT_PLAYLIST_SIZE;
    static const int UPDATE_PLAYLIST_TIME;
    static const int MAX_RETRY_COUNT;

private:
    Ui::PlayList *ui;
    bool m_isInit;
    PlayListUpdater m_updater;
    int m_updateTimerID;
    bool m_clearPlayListOnExit;
    int m_currentPlayingIndex;
    QString m_lastURLPickerQuality;
    QString m_lastURLPickerMime;
    QMutex m_getPlayListMutex;
    QShortcut m_delShort;
    QShortcut m_moveUpShort;
    QShortcut m_moveDownShort;
    QShortcut m_moveToTopShort;
    QShortcut m_moveToBottomShort;
    QShortcut m_viewPathShort;
    QShortcut m_copyPathShort;
    QShortcut m_screenExplorerShort;
};

Q_DECLARE_METATYPE(PlayItem)
QDataStream& operator << (QDataStream &out, const PlayItem &item);
QDataStream& operator >> (QDataStream &in, PlayItem &item);
