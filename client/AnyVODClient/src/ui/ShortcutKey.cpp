﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "ShortcutKey.h"
#include "setting/ShortcutKeySettingLoader.h"
#include "setting/ShortcutKeySettingSaver.h"
#include "ui/MainWindowInterface.h"
#include "ui/PlayList.h"
#include "delegate/ScreenDelegate.h"
#include "delegate/UpdaterDelegate.h"
#include "delegate/MediaPlayerDelegate.h"
#include "delegate/ShaderCompositerDelegate.h"
#include "factories/MainWindowFactory.h"

#include <QShortcut>
#include <QCoreApplication>

ShortcutKey::ShortcutKey() :
    m_mainWindow(MainWindowFactory::getInstance()),
    m_screenDelegate(ScreenDelegate::getInstance()),
    m_updaterDelegate(UpdaterDelegate::getInstance()),
    m_playerDelegate(MediaPlayerDelegate::getInstance()),
    m_shaderDelegate(ShaderCompositerDelegate::getInstance())
{
    this->setupKey();
    this->loadFromSetting();

    connect(qApp, &QCoreApplication::aboutToQuit, this, &ShortcutKey::saveToSetting);
}

ShortcutKey::~ShortcutKey()
{
    //No need to delete key here because these are deleted by parent.
}

ShortcutKey& ShortcutKey::getInstance()
{
    static ShortcutKey instance;

    return instance;
}

void ShortcutKey::loadFromSetting()
{
    ShortcutKeySettingLoader(this).load();
}

void ShortcutKey::saveToSetting()
{
    ShortcutKeySettingSaver(this).save();
}

void ShortcutKey::setupKey()
{
    this->setupMainWindowKey();
    this->setupScreenKey();
    this->setupUpdaterKey();
    this->setupMediaPlayerKey();
    this->setupShaderKey();
    this->setupPlayListKey();

#ifdef Q_OS_LINUX
    this->m_shortcut[AnyVODEnums::SK_MOST_TOP].shortcut->setEnabled(false);
#endif
}

void ShortcutKey::setupMainWindowKey()
{
    this->createMainWindowShortcutItem(AnyVODEnums::SK_LOGIN, tr("로그인"), Qt::Key_I, &MainWindowInterface::login);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_LOGOUT, tr("로그아웃"), Qt::Key_O | Qt::AltModifier, &MainWindowInterface::logout);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_MOST_TOP, tr("항상 위"), Qt::Key_T, &MainWindowInterface::mostTop);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_OPEN_REMOTE_FILE_LIST, tr("원격 파일 목록 열기"), Qt::Key_R, &MainWindowInterface::openRemoteFileList);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_OPEN_EXTERNAL, tr("외부 열기"), Qt::Key_E, &MainWindowInterface::openExternal);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_OPEN, tr("열기"), Qt::Key_P, &MainWindowInterface::open);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_CLOSE, tr("닫기"), Qt::Key_C | Qt::AltModifier, &MainWindowInterface::close);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_EXIT, tr("종료"), Qt::Key_F4 | Qt::AltModifier, &MainWindowInterface::exit);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_OPEN_EQUALIZER_SETTING, tr("이퀄라이저 설정"), Qt::Key_E | Qt::AltModifier, &MainWindowInterface::openEqualizerSetting);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_OPEN_PLAY_LIST, tr("재생 목록 열기"), Qt::Key_L, &MainWindowInterface::openPlayList);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_TOGGLE, tr("재생 / 일시정지"), Qt::Key_Space, &MainWindowInterface::toggle);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_TOGGLE_PLAY_MEDIA, tr("재생 / 일시정지(재생)"), Qt::Key_MediaPlay, &MainWindowInterface::toggle);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_TOGGLE_PAUSE_MEDIA, tr("재생 / 일시정지(일시정지)"), Qt::Key_MediaPause, &MainWindowInterface::toggle);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_TOGGLE_PLAY_PAUSE_MEDIA, tr("재생 / 일시정지(토글)"), Qt::Key_MediaTogglePlayPause, &MainWindowInterface::toggle);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_FULL_SCREEN_RETURN, tr("전체 화면"), Qt::Key_Return, &MainWindowInterface::fullScreen);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_FULL_SCREEN_ENTER, tr("전체 화면(추가)"), Qt::Key_Enter, &MainWindowInterface::fullScreen);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_PREV, tr("이전 파일"), Qt::Key_PageUp, &MainWindowInterface::prev);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_PREV_MEDIA, tr("이전 파일(추가)"), Qt::Key_MediaPrevious, &MainWindowInterface::prev);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_NEXT, tr("다음 파일"), Qt::Key_PageDown, &MainWindowInterface::next);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_NEXT_MEDIA, tr("다음 파일(추가)"), Qt::Key_MediaNext, &MainWindowInterface::next);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_VOLUME_UP, tr("소리 크게"), Qt::Key_Up, &MainWindowInterface::volumeUp);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_VOLUME_UP_MEDIA, tr("소리 크게(추가)"), Qt::Key_VolumeUp, &MainWindowInterface::volumeUp);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_VOLUME_DOWN, tr("소리 작게"), Qt::Key_Down, &MainWindowInterface::volumeDown);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_VOLUME_DOWN_MEDIA, tr("소리 작게(추가)"), Qt::Key_VolumeDown, &MainWindowInterface::volumeDown);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_MUTE, tr("음소거"), Qt::Key_M, &MainWindowInterface::mute);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_MUTE_MEDIA, tr("음소거(추가)"), Qt::Key_VolumeMute, &MainWindowInterface::mute);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_INFO, tr("정보"), Qt::Key_F1, &MainWindowInterface::info);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_SHOW_CONTROL_BAR, tr("컨트롤바 보이기"), Qt::Key_Tab, &MainWindowInterface::showControlBar);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_INC_OPAQUE, tr("투명도 증가"), Qt::Key_X, &MainWindowInterface::incOpaque);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_DEC_OPAQUE, tr("투명도 감소"), Qt::Key_Z, &MainWindowInterface::decOpaque);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_MAX_OPAQUE, tr("최대 투명도"), Qt::Key_X | Qt::AltModifier, &MainWindowInterface::maxOpaque);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_MIN_OPAQUE, tr("최소 투명도"), Qt::Key_Z | Qt::AltModifier, &MainWindowInterface::minOpaque);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_SKIP_RANGE_SETTING, tr("재생 스킵 설정"), Qt::Key_Y, &MainWindowInterface::openSkipRange);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_OPEN_SHADER_COMPOSITER, tr("셰이더 조합"), Qt::Key_K, &MainWindowInterface::openShaderCompositer);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_OPEN_CUSTOM_SHORTCUTS, tr("단축 키 설정"), Qt::Key_unknown, &MainWindowInterface::openCustomShortcut);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_ADD_TO_PLATLIST, tr("재생 목록에 추가"), Qt::Key_P | Qt::ControlModifier, &MainWindowInterface::addToPlayList);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_USER_ASPECT_RATIO_ORDER, tr("화면 비율 순차 선택"), Qt::Key_R | Qt::ControlModifier, &MainWindowInterface::userAspectRatioOrder);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_USER_ASPECT_RATIO, tr("화면 비율 사용자 지정"), Qt::Key_unknown, &MainWindowInterface::openUserAspectRatio);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_SCREEN_SIZE_HALF, tr("화면 크기 0.5배"), Qt::Key_1 | Qt::AltModifier, &MainWindowInterface::screenSizeHalf);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_SCREEN_SIZE_NORMAL, tr("화면 크기 1배"), Qt::Key_2 | Qt::AltModifier, &MainWindowInterface::screenSizeNormal);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_SCREEN_SIZE_NORMAL_HALF, tr("화면 크기 1.5배"), Qt::Key_3 | Qt::AltModifier, &MainWindowInterface::screenSizeNormalHalf);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_SCREEN_SIZE_DOUBLE, tr("화면 크기 2배"), Qt::Key_4 | Qt::AltModifier, &MainWindowInterface::screenSizeDouble);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_SCREEN_EXPLORER, tr("장면 탐색"), Qt::Key_E | Qt::ControlModifier | Qt::AltModifier, &MainWindowInterface::openScreenExplorer);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_SUBTITLE_DIRECTORY, tr("검색 디렉토리"), Qt::Key_unknown, &MainWindowInterface::openSubtitleDirectory);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_IMPORT_FONTS, tr("자막 폰트 가져오기"), Qt::Key_unknown, &MainWindowInterface::openImportFonts);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_OPEN_DEVICE, tr("장치 열기"), Qt::Key_unknown, &MainWindowInterface::openDevice);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_ADD_DTV_CHANNEL_TO_PLAYLIST, tr("재생 목록에 DTV 채널 추가"), Qt::Key_unknown, &MainWindowInterface::addDTVChannelToPlaylist);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_OPEN_DTV_SCAN_CHANNEL, tr("DTV 채널 검색"), Qt::Key_unknown, &MainWindowInterface::openScanDTVChannel);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_VIEW_EPG, tr("채널 편성표"), Qt::Key_unknown, &MainWindowInterface::viewEPG);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_STOP, tr("정지"), Qt::Key_S | Qt::AltModifier, &MainWindowInterface::stop);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_STOP_MEDIA, tr("정지(추가)"), Qt::Key_MediaStop, &MainWindowInterface::stop);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_SAVE_SETTINGS, tr("설정 저장하기"), Qt::Key_unknown, &MainWindowInterface::saveSettingsToFile);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_LOAD_SETTINGS, tr("설정 불러오기"), Qt::Key_unknown, &MainWindowInterface::loadSettingsToFile);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_OPEN_AUDIO_CD, tr("오디오 시디 열기"), Qt::Key_unknown, &MainWindowInterface::openAudioCD);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_OPEN_FILE_ASSOCIATION, tr("확장자 연결"), Qt::Key_unknown, &MainWindowInterface::openFileAssociation);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_OPEN_TEXT_ENCODING, tr("인코딩 설정"), Qt::Key_unknown, &MainWindowInterface::openTextEncoding);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_OPEN_SERVER_SETTING, tr("서버 설정"), Qt::Key_unknown, &MainWindowInterface::openServerSetting);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_ADD_RADIO_CHANNEL_TO_PLAYLIST, tr("재생 목록에 라디오 채널 추가"), Qt::Key_unknown, &MainWindowInterface::addRadioChannelToPlaylist);
    this->createMainWindowShortcutItem(AnyVODEnums::SK_OPEN_RADIO_SCAN_CHANNEL, tr("라디오 채널 검색"), Qt::Key_unknown, &MainWindowInterface::openScanRadioChannel);
}

void ShortcutKey::setupScreenKey()
{
    this->createScreenShortcutItem(AnyVODEnums::SK_CAPTURE_SINGLE, tr("한 장 캡쳐"), Qt::Key_C | Qt::ControlModifier, &ScreenDelegate::captureSingle);
    this->createScreenShortcutItem(AnyVODEnums::SK_CAPTURE_MULTIPLE, tr("여러 장 캡쳐"), Qt::Key_C | Qt::ControlModifier | Qt::AltModifier, &ScreenDelegate::captureMultiple);
    this->createScreenShortcutItem(AnyVODEnums::SK_SELECT_CAPTURE_EXT_ORDER, tr("캡쳐 확장자 순차 선택"), Qt::Key_E | Qt::ControlModifier, &ScreenDelegate::captureExtOrder);
    this->createScreenShortcutItem(AnyVODEnums::SK_OPEN_CAPTURE_DIRECTORY, tr("저장 디렉토리 열기"), Qt::Key_unknown, &ScreenDelegate::openCaptureSaveDir);
    this->createScreenShortcutItem(AnyVODEnums::SK_SELECT_CAPTURE_DIRECTORY, tr("저장 디렉토리 설정"), Qt::Key_unknown, &ScreenDelegate::selectCaptureSaveDir);
    this->createScreenShortcutItem(AnyVODEnums::SK_USE_VSYNC, tr("수직 동기화 사용"), Qt::Key_unknown, &ScreenDelegate::useVSync);
    this->createScreenShortcutItem(AnyVODEnums::SK_USE_HDR, tr("HDR 사용"), Qt::Key_H, &ScreenDelegate::useHDR);
}

void ShortcutKey::setupUpdaterKey()
{
    this->createUpdaterShortcutItem(AnyVODEnums::SK_CHECK_UPDATE_GAP, tr("업데이트 확인 주기 순차 선택"), Qt::Key_unknown, &UpdaterDelegate::updateGapOrder);
}

void ShortcutKey::setupMediaPlayerKey()
{
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_AUDIO_NORMALIZE, tr("노멀라이저 사용"), Qt::Key_N, &MediaPlayerDelegate::audioNormalize);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_AUDIO_EQUALIZER, tr("이퀄라이저 사용"), Qt::Key_Q, &MediaPlayerDelegate::audioEqualizer);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_RESET_SUBTITLE_POSITION, tr("자막 기본 위치"), Qt::Key_Home, &MediaPlayerDelegate::resetSubtitlePosition);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_UP_SUBTITLE_POSITION, tr("자막 위치 위로"), Qt::Key_Up | Qt::AltModifier, &MediaPlayerDelegate::upSubtitlePosition);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_DOWN_SUBTITLE_POSITION, tr("자막 위치 아래로"), Qt::Key_Down | Qt::AltModifier, &MediaPlayerDelegate::downSubtitlePosition);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_LEFT_SUBTITLE_POSITION, tr("자막 위치 왼쪽으로"), Qt::Key_Left | Qt::AltModifier, &MediaPlayerDelegate::leftSubtitlePosition);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_RIGHT_SUBTITLE_POSITION, tr("자막 위치 오른쪽으로"), Qt::Key_Right | Qt::AltModifier, &MediaPlayerDelegate::rightSubtitlePosition);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_REWIND_5, tr("5초 뒤로"), Qt::Key_Left, &MediaPlayerDelegate::rewind5);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_FORWARD_5, tr("5초 앞으로"), Qt::Key_Right, &MediaPlayerDelegate::forward5);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_REWIND_30, tr("30초 뒤로"), Qt::Key_Left | Qt::ControlModifier, &MediaPlayerDelegate::rewind30);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_FORWARD_30, tr("30초 앞으로"), Qt::Key_Right | Qt::ControlModifier, &MediaPlayerDelegate::forward30);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_REWIND_60, tr("1분 뒤로"), Qt::Key_Left | Qt::ShiftModifier, &MediaPlayerDelegate::rewind60);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_FORWARD_60, tr("1분 앞으로"), Qt::Key_Right | Qt::ShiftModifier, &MediaPlayerDelegate::forward60);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_GOTO_BEGIN, tr("처음으로 이동"), Qt::Key_Backspace, &MediaPlayerDelegate::gotoBegin);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_PLAY_ORDER, tr("재생 순서 순차 선택"), Qt::Key_P | Qt::AltModifier, &MediaPlayerDelegate::playOrder);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_SUBTITLE_TOGGLE, tr("보이기"), Qt::Key_H | Qt::AltModifier, &MediaPlayerDelegate::subtitleToggle);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_PREV_SUBTITLE_SYNC, tr("싱크 느리게"), Qt::Key_Comma, &MediaPlayerDelegate::prevSubtitleSync);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_NEXT_SUBTITLE_SYNC, tr("싱크 빠르게"), Qt::Key_Period, &MediaPlayerDelegate::nextSubtitleSync);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_RESET_SUBTITLE_SYNC, tr("싱크 초기화"), Qt::Key_Slash, &MediaPlayerDelegate::resetSubtitleSync);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_SUBTITLE_LANGUAGE_ORDER, tr("자막 언어 순차 선택"), Qt::Key_L | Qt::AltModifier, &MediaPlayerDelegate::subtitleLanguageOrder);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_AUDIO_ORDER, tr("음성 순차 선택"), Qt::Key_V | Qt::AltModifier, &MediaPlayerDelegate::audioOrder);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_DETAIL, tr("재생 정보"), Qt::Key_F2, &MediaPlayerDelegate::detail);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_DEINTERLACE_METHOD_ORDER, tr("디인터레이스 순차 선택"), Qt::Key_D | Qt::AltModifier, &MediaPlayerDelegate::deinterlaceMethodOrder);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_DEINTERLACE_ALGORITHM_ORDER, tr("디인터레이스 알고리즘 순차 선택"), Qt::Key_A | Qt::AltModifier, &MediaPlayerDelegate::deinterlaceAlgorithmOrder);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_PREV_AUDIO_SYNC, tr("싱크 느리게"), Qt::Key_A, &MediaPlayerDelegate::prevAudioSync);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_NEXT_AUDIO_SYNC, tr("싱크 빠르게"), Qt::Key_S, &MediaPlayerDelegate::nextAudioSync);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_RESET_AUDIO_SYNC, tr("싱크 초기화"), Qt::Key_D, &MediaPlayerDelegate::resetAudioSync);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_SUBTITLE_HALIGN_ORDER, tr("자막 가로 정렬 방법 순차 선택"), Qt::Key_N | Qt::AltModifier, &MediaPlayerDelegate::subtitleHAlignOrder);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_REPEAT_RANGE_START, tr("구간 반복 시작"), Qt::Key_BracketLeft, &MediaPlayerDelegate::repeatRangeStart);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_REPEAT_RANGE_END, tr("구간 반복 끝"), Qt::Key_BracketRight, &MediaPlayerDelegate::repeatRangeEnd);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_REPEAT_RANGE_ENABLE, tr("구간 반복 활성화"), Qt::Key_Backslash, &MediaPlayerDelegate::repeatRangeEnable);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_SEEK_KEYFRAME, tr("키프레임 단위로 이동"), Qt::Key_K | Qt::AltModifier, &MediaPlayerDelegate::seekKeyFrame);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_SKIP_OPENING, tr("오프닝 스킵 사용"), Qt::Key_R | Qt::AltModifier, &MediaPlayerDelegate::skipOpening);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_SKIP_ENDING, tr("엔딩 스킵 사용"), Qt::Key_T | Qt::AltModifier, &MediaPlayerDelegate::skipEnding);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_USE_SKIP_RANGE, tr("재생 스킵 사용"), Qt::Key_Y | Qt::AltModifier, &MediaPlayerDelegate::useSkipRange);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_PREV_FRAME, tr("이전 프레임으로 이동"), Qt::Key_F, &MediaPlayerDelegate::prevFrame);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_NEXT_FRAME, tr("다음 프레임으로 이동"), Qt::Key_G, &MediaPlayerDelegate::nextFrame);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_SLOWER_PLAYBACK, tr("재생 속도 느리게"), Qt::Key_C, &MediaPlayerDelegate::slowerPlayback);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_FASTER_PLAYBACK, tr("재생 속도 빠르게"), Qt::Key_V, &MediaPlayerDelegate::fasterPlayback);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_NORMAL_PLAYBACK, tr("재생 속도 초기화"), Qt::Key_B, &MediaPlayerDelegate::normalPlayback);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_LOWER_MUSIC, tr("음악 줄임"), Qt::Key_M | Qt::ShiftModifier, &MediaPlayerDelegate::lowerMusic);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_LOWER_VOICE, tr("음성 줄임"), Qt::Key_V | Qt::ShiftModifier, &MediaPlayerDelegate::lowerVoice);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_HIGHER_VOICE, tr("음성 강조"), Qt::Key_H | Qt::ShiftModifier, &MediaPlayerDelegate::higherVoice);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_INC_SUBTITLE_OPAQUE, tr("투명도 증가"), Qt::Key_X | Qt::ControlModifier, &MediaPlayerDelegate::incSubtitleOpaque);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_DEC_SUBTITLE_OPAQUE, tr("투명도 감소"), Qt::Key_Z | Qt::ControlModifier, &MediaPlayerDelegate::decSubtitleOpaque);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_AUDIO_DEVICE_ORDER, tr("소리 출력 장치 순차 선택"), Qt::Key_unknown, &MediaPlayerDelegate::audioDeviceOrder);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_ENABLE_SEARCH_SUBTITLE, tr("자막 찾기 켜기"), Qt::Key_S | Qt::ControlModifier | Qt::AltModifier, &MediaPlayerDelegate::enableSearchSubtitle);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_ENABLE_SEARCH_LYRICS, tr("가사 찾기 켜기"), Qt::Key_L | Qt::ControlModifier | Qt::AltModifier, &MediaPlayerDelegate::enableSearchLyrics);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_SUBTITLE_VALIGN_ORDER, tr("자막 세로 정렬 방법 순차 선택"), Qt::Key_N | Qt::ControlModifier | Qt::AltModifier, &MediaPlayerDelegate::subtitleVAlignOrder);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_INC_SUBTITLE_SIZE, tr("자막 크기 증가"), Qt::Key_Up | Qt::ControlModifier, &MediaPlayerDelegate::incSubtitleSize);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_DEC_SUBTITLE_SIZE, tr("자막 크기 감소"), Qt::Key_Down | Qt::ControlModifier, &MediaPlayerDelegate::decSubtitleSize);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_RESET_SUBTITLE_SIZE, tr("자막 크기 초기화"), Qt::Key_Home | Qt::ControlModifier, &MediaPlayerDelegate::resetSubtitleSize);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_RESET_SUBTITLE_OPAQUE, tr("투명도 초기화"), Qt::Key_C | Qt::ShiftModifier, &MediaPlayerDelegate::resetSubtitleOpaque);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_USE_HW_DECODER, tr("하드웨어 디코더 사용"), Qt::Key_unknown, &MediaPlayerDelegate::useHWDecoder);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_USE_SPDIF, tr("S/PDIF 출력 사용"), Qt::Key_S | Qt::AltModifier | Qt::ShiftModifier, &MediaPlayerDelegate::useSPDIF);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_SPDIF_SAMPLE_RATE_ORDER, tr("S/PDIF 샘플 속도 순차 선택"), Qt::Key_unknown, &MediaPlayerDelegate::spdifSampleRateOrder);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_USE_PBO, tr("고속 렌더링 사용"), Qt::Key_unknown, &MediaPlayerDelegate::usePBO);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_3D_VIDEO_METHOD_ORDER, tr("3D 영상 출력 방법 순차 선택"), Qt::Key_unknown, &MediaPlayerDelegate::method3DOrder);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_PREV_CHAPTER, tr("이전 챕터로 이동"), Qt::Key_PageUp | Qt::ControlModifier, &MediaPlayerDelegate::prevChapter);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_NEXT_CHAPTER, tr("다음 챕터로 이동"), Qt::Key_PageDown | Qt::ControlModifier, &MediaPlayerDelegate::nextChapter);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_CLOSE_EXTERNAL_SUBTITLE, tr("외부 닫기"), Qt::Key_unknown, &MediaPlayerDelegate::closeExternalSubtitle);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_SHOW_ALBUM_JACKET, tr("앨범 자켓 보기"), Qt::Key_J | Qt::ControlModifier, &MediaPlayerDelegate::showAlbumJacket);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_LAST_PLAY, tr("재생 위치 기억"), Qt::Key_unknown, &MediaPlayerDelegate::lastPlay);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_SAVE_SUBTITLE, tr("저장"), Qt::Key_unknown, &MediaPlayerDelegate::saveSubtitle);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_SAVE_AS_SUBTITLE, tr("다른 이름으로 저장"), Qt::Key_unknown, &MediaPlayerDelegate::saveAsSubtitle);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_SPDIF_AUDIO_DEVICE_ORDER, tr("S/PDIF 소리 출력 장치 순차 선택"), Qt::Key_unknown, &MediaPlayerDelegate::audioSPDIFDeviceOrder);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_REPEAT_RANGE_START_BACK_100MS, tr("시작 위치 0.1초 뒤로 이동"), Qt::Key_BracketLeft | Qt::ControlModifier, &MediaPlayerDelegate::repeatRangeStartBack100MS);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_REPEAT_RANGE_START_FORW_100MS, tr("시작 위치 0.1초 앞으로 이동"), Qt::Key_BracketRight | Qt::ControlModifier, &MediaPlayerDelegate::repeatRangeStartForw100MS);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_REPEAT_RANGE_END_BACK_100MS, tr("끝 위치 0.1초 뒤로 이동"), Qt::Key_BracketLeft | Qt::AltModifier, &MediaPlayerDelegate::repeatRangeEndBack100MS);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_REPEAT_RANGE_END_FORW_100MS, tr("끝 위치 0.1초 앞으로 이동"), Qt::Key_BracketRight | Qt::AltModifier, &MediaPlayerDelegate::repeatRangeEndForw100MS);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_REPEAT_RANGE_BACK_100MS, tr("구간 반복 0.1초 뒤로 이동"), Qt::Key_BracketLeft | Qt::AltModifier | Qt::ControlModifier, &MediaPlayerDelegate::repeatRangeBack100MS);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_REPEAT_RANGE_FORW_100MS, tr("구간 반복 0.1초 앞으로 이동"), Qt::Key_BracketRight | Qt::AltModifier | Qt::ControlModifier, &MediaPlayerDelegate::repeatRangeForw100MS);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_USE_FRAME_DROP, tr("프레임 드랍 사용"), Qt::Key_unknown, &MediaPlayerDelegate::useFrameDrop);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_USE_SPDIF_ENCODING_ORDER, tr("인코딩 순차 선택"), Qt::Key_A | Qt::AltModifier | Qt::ShiftModifier, &MediaPlayerDelegate::useSPDIFEncodingOrder);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_3D_SUBTITLE_METHOD_ORDER, tr("3D 자막 출력 방법 순차 선택"), Qt::Key_unknown, &MediaPlayerDelegate::method3DSubtitleOrder);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_USE_3D_FULL, tr("3D 전체 해상도 사용"), Qt::Key_unknown, &MediaPlayerDelegate::use3DFull);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_RESET_3D_SUBTITLE_OFFSET, tr("3D 자막 기본 위치"), Qt::Key_Home | Qt::ShiftModifier | Qt::ControlModifier, &MediaPlayerDelegate::reset3DSubtitleOffset);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_UP_3D_SUBTITLE_OFFSET, tr("3D 자막 위치 가깝게(세로)"), Qt::Key_Up | Qt::ShiftModifier | Qt::ControlModifier, &MediaPlayerDelegate::up3DSubtitleOffset);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_DOWN_3D_SUBTITLE_OFFSET, tr("3D 자막 위치 멀게(세로)"), Qt::Key_Down | Qt::ShiftModifier | Qt::ControlModifier, &MediaPlayerDelegate::down3DSubtitleOffset);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_LEFT_3D_SUBTITLE_OFFSET, tr("3D 자막 위치 가깝게(가로)"), Qt::Key_Left | Qt::ShiftModifier | Qt::ControlModifier, &MediaPlayerDelegate::left3DSubtitleOffset);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_RIGHT_3D_SUBTITLE_OFFSET, tr("3D 자막 위치 멀게(가로)"), Qt::Key_Right | Qt::ShiftModifier | Qt::ControlModifier, &MediaPlayerDelegate::right3DSubtitleOffset);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_SCREEN_ROTATION_DEGREE_ORDER, tr("화면 회전 각도 순차 선택"), Qt::Key_R | Qt::AltModifier | Qt::ControlModifier, &MediaPlayerDelegate::screenRotationDegreeOrder);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_HISTOGRAM_EQ, tr("히스토그램 이퀄라이저"), Qt::Key_H | Qt::ControlModifier, &MediaPlayerDelegate::histEQ);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_HIGH_QUALITY_3D_DENOISE, tr("3D 노이즈 제거"), Qt::Key_N | Qt::ControlModifier, &MediaPlayerDelegate::hq3DDenoise);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_SEARCH_SUBTITLE_COMPLEX, tr("고급 검색"), Qt::Key_unknown, &MediaPlayerDelegate::useSearchSubtitleComplex);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_DEBAND, tr("디밴드"), Qt::Key_D | Qt::ShiftModifier | Qt::ControlModifier, &MediaPlayerDelegate::deband);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_ATA_DENOISE, tr("적응 시간 평균 노이즈 제거"), Qt::Key_A | Qt::AltModifier | Qt::ControlModifier, &MediaPlayerDelegate::ataDenoise);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_OW_DENOISE, tr("Overcomplete Wavelet 노이즈 제거"), Qt::Key_O | Qt::ControlModifier, &MediaPlayerDelegate::owDenoise);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_USE_BUFFERING_MODE, tr("버퍼링 모드 사용"), Qt::Key_B | Qt::ControlModifier, &MediaPlayerDelegate::useBufferingMode);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_UP_VR_PINCUSHION_COEFFICIENTS_K1, tr("360도 영상 왜곡 보정 값 증가(K1)"), Qt::Key_unknown, &MediaPlayerDelegate::pincushionUpK1);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_DOWN_VR_PINCUSHION_COEFFICIENTS_K1, tr("360도 영상 왜곡 보정 값 감소(K1)"), Qt::Key_unknown, &MediaPlayerDelegate::pincushionDownK1);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_UP_VR_PINCUSHION_COEFFICIENTS_K2, tr("360도 영상 왜곡 보정 값 증가(K2)"), Qt::Key_unknown, &MediaPlayerDelegate::pincushionUpK2);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_DOWN_VR_PINCUSHION_COEFFICIENTS_K2, tr("360도 영상 왜곡 보정 값 감소(K2)"), Qt::Key_unknown, &MediaPlayerDelegate::pincushionDownK2);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_USE_GPU_CONVERT, tr("GPU 디코딩 사용"), Qt::Key_unknown, &MediaPlayerDelegate::useGPUConvert);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_AUTO_SAVE_SEARCH_LYRICS, tr("찾은 가사 자동 저장"), Qt::Key_unknown, &MediaPlayerDelegate::autoSaveSearchLyrics);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_VR_INPUT_SOURCE_ORDER, tr("입력 영상 출력 방법 순차 선택"), Qt::Key_unknown, &MediaPlayerDelegate::vrInputSourceOrder);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_UP_VR_LENS_CENTER_X, tr("렌즈 센터 값 증가(좌/우)"), Qt::Key_unknown, &MediaPlayerDelegate::upLensCenterX);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_DOWN_VR_LENS_CENTER_X, tr("렌즈 센터 값 감소(좌/우)"), Qt::Key_unknown, &MediaPlayerDelegate::downLensCenterX);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_UP_VR_LENS_CENTER_Y, tr("렌즈 센터 값 증가(상/하)"), Qt::Key_unknown, &MediaPlayerDelegate::upLensCenterY);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_DOWN_VR_LENS_CENTER_Y, tr("렌즈 센터 감소(상/하)"), Qt::Key_unknown, &MediaPlayerDelegate::downLensCenterY);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_UP_VIRTUAL_3D_DEPTH, tr("가상 3D 입체감 증가"), Qt::Key_unknown, &MediaPlayerDelegate::upVirtual3DDepth);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_DOWN_VIRTUAL_3D_DEPTH, tr("가상 3D 입체감 감소"), Qt::Key_unknown, &MediaPlayerDelegate::downVirtual3DDepth);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_UP_VR_BARREL_COEFFICIENTS_K1, tr("VR 평면 영상 왜곡 보정 값 증가(K1)"), Qt::Key_unknown, &MediaPlayerDelegate::barrelUpK1);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_DOWN_VR_BARREL_COEFFICIENTS_K1, tr("VR 평면 영상 왜곡 보정 값 감소(K1)"), Qt::Key_unknown, &MediaPlayerDelegate::barrelDownK1);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_UP_VR_BARREL_COEFFICIENTS_K2, tr("VR 평면 영상 왜곡 보정 값 증가(K2)"), Qt::Key_unknown, &MediaPlayerDelegate::barrelUpK2);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_DOWN_VR_BARREL_COEFFICIENTS_K2, tr("VR 평면 영상 왜곡 보정 값 감소(K2)"), Qt::Key_unknown, &MediaPlayerDelegate::barrelDownK2);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_DISTORION_ADJUST_MODE_ORDER, tr("왜곡 보정 모드 순차 선택"), Qt::Key_unknown, &MediaPlayerDelegate::distortionAdjustModeOrder);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_VR_USE_DISTORTION, tr("왜곡 보정 사용"), Qt::Key_unknown, &MediaPlayerDelegate::useDistortion);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_VIDEO_NORMALIZE, tr("노멀라이즈"), Qt::Key_N | Qt::ShiftModifier | Qt::ControlModifier, &MediaPlayerDelegate::videoNormalize);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_NLMEANS_DENOISE, tr("Non-local Means 노이즈 제거"), Qt::Key_M | Qt::ControlModifier, &MediaPlayerDelegate::nlMeansDenoise);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_VAGUE_DENOISE, tr("Vague 노이즈 제거"), Qt::Key_V | Qt::ControlModifier, &MediaPlayerDelegate::vagueDenoise);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_HW_DECODER_ORDER, tr("하드웨어 디코더 순차 선택"), Qt::Key_unknown, &MediaPlayerDelegate::hwDecoderOrder);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_AUTO_COLOR_CONVERSION, tr("색상 자동 보정 사용"), Qt::Key_unknown, &MediaPlayerDelegate::useAutoColorConversion);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_DEBLOCK, tr("디블록"), Qt::Key_B | Qt::ShiftModifier | Qt::ControlModifier, &MediaPlayerDelegate::deblock);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_USE_IVTC, tr("역텔레시네 사용"), Qt::Key_unknown, &MediaPlayerDelegate::useIVTC);
    this->createMediaPlayerShortcutItem(AnyVODEnums::SK_VIDEO_ORDER, tr("영상 순차 선택"), Qt::Key_unknown, &MediaPlayerDelegate::videoOrder);
}

void ShortcutKey::setupShaderKey()
{
    this->createShaderShortcutItem(AnyVODEnums::SK_RESET_VIDEO_ATTRIBUTE, tr("영상 속성 초기화"), Qt::Key_1, &ShaderCompositerDelegate::resetVideoAttribute);
    this->createShaderShortcutItem(AnyVODEnums::SK_BRIGHTNESS_DOWN, tr("영상 밝기 감소"), Qt::Key_2, &ShaderCompositerDelegate::brightnessDown);
    this->createShaderShortcutItem(AnyVODEnums::SK_BRIGHTNESS_UP, tr("영상 밝기 증가"), Qt::Key_3, &ShaderCompositerDelegate::brightnessUp);
    this->createShaderShortcutItem(AnyVODEnums::SK_SATURATION_DOWN, tr("영상 채도 감소"), Qt::Key_4, &ShaderCompositerDelegate::saturationDown);
    this->createShaderShortcutItem(AnyVODEnums::SK_SATURATION_UP, tr("영상 채도 증가"), Qt::Key_5, &ShaderCompositerDelegate::saturationUp);
    this->createShaderShortcutItem(AnyVODEnums::SK_HUE_DOWN, tr("영상 색상 감소"), Qt::Key_6, &ShaderCompositerDelegate::hueDown);
    this->createShaderShortcutItem(AnyVODEnums::SK_HUE_UP, tr("영상 색상 증가"), Qt::Key_7, &ShaderCompositerDelegate::hueUp);
    this->createShaderShortcutItem(AnyVODEnums::SK_CONTRAST_DOWN, tr("영상 대비 감소"), Qt::Key_8, &ShaderCompositerDelegate::contrastDown);
    this->createShaderShortcutItem(AnyVODEnums::SK_CONTRAST_UP, tr("영상 대비 증가"), Qt::Key_9, &ShaderCompositerDelegate::contrastUp);
    this->createShaderShortcutItem(AnyVODEnums::SK_SHARPLY, tr("날카롭게"), Qt::Key_A | Qt::ControlModifier, &ShaderCompositerDelegate::sharply);
    this->createShaderShortcutItem(AnyVODEnums::SK_SHARPEN, tr("선명하게"), Qt::Key_S | Qt::ControlModifier, &ShaderCompositerDelegate::sharpen);
    this->createShaderShortcutItem(AnyVODEnums::SK_SOFTEN, tr("부드럽게"), Qt::Key_D | Qt::ControlModifier, &ShaderCompositerDelegate::soften);
    this->createShaderShortcutItem(AnyVODEnums::SK_LEFT_RIGHT_INVERT, tr("좌우 반전"), Qt::Key_F | Qt::ControlModifier, &ShaderCompositerDelegate::leftRightInvert);
    this->createShaderShortcutItem(AnyVODEnums::SK_TOP_BOTTOM_INVERT, tr("상하 반전"), Qt::Key_G | Qt::ControlModifier, &ShaderCompositerDelegate::topBottomInvert);
    this->createShaderShortcutItem(AnyVODEnums::SK_ANAGLYPH_ALGORITHM_ORDER, tr("애너글리프 알고리즘 순차 선택"), Qt::Key_unknown, &ShaderCompositerDelegate::anaglyphAlgorithmOrder);
    this->createShaderShortcutItem(AnyVODEnums::SK_USE_360_DEGREE, tr("360도 영상 사용"), Qt::Key_unknown, &ShaderCompositerDelegate::use360Degree);
    this->createShaderShortcutItem(AnyVODEnums::SK_PROJECTION_TYPE_ORDER, tr("프로젝션 순차 선택"), Qt::Key_unknown, &ShaderCompositerDelegate::projectionTypeOrder);
}

void ShortcutKey::setupPlayListKey()
{
    this->createPlayListShortcutItem(AnyVODEnums::SK_CLEAR_PLAYLIST, tr("종료 시 재생 목록 비움"), Qt::Key_unknown, &PlayList::toggleClearPlayListOnExit);
}

void ShortcutKey::deleteKey()
{
    for (int i = 0; i < AnyVODEnums::SK_COUNT; i++)
    {
        delete this->m_shortcut[i].shortcut;
        this->m_shortcut[i].shortcut = nullptr;
    }
}

template <typename T1, typename T2>
void ShortcutKey::createShortcutItem(AnyVODEnums::ShortcutKey keyName, const QString &desc, int keySequence, T1 receiver, T2 slot)
{
    QShortcut *shortcut = new QShortcut(QKeySequence(keySequence), this->m_mainWindow);

    connect(shortcut, &QShortcut::activated, receiver, slot);
    this->m_shortcut[keyName] = ShortcutItem(desc, shortcut);
}

void ShortcutKey::createMainWindowShortcutItem(AnyVODEnums::ShortcutKey keyName, const QString &desc, int keySequence, void (MainWindowInterface::*slot)())
{
    this->createShortcutItem(keyName, desc, keySequence, this->m_mainWindow, slot);
}

void ShortcutKey::createScreenShortcutItem(AnyVODEnums::ShortcutKey keyName, const QString &desc, int keySequence, void (ScreenDelegate::*slot)())
{
    this->createShortcutItem(keyName, desc, keySequence, &this->m_screenDelegate, slot);
}

void ShortcutKey::createUpdaterShortcutItem(AnyVODEnums::ShortcutKey keyName, const QString &desc, int keySequence, void (UpdaterDelegate::*slot)())
{
    this->createShortcutItem(keyName, desc, keySequence, &this->m_updaterDelegate, slot);
}

void ShortcutKey::createMediaPlayerShortcutItem(AnyVODEnums::ShortcutKey keyName, const QString &desc, int keySequence, void (MediaPlayerDelegate::*slot)())
{
    this->createShortcutItem(keyName, desc, keySequence, &this->m_playerDelegate, slot);
}

void ShortcutKey::createShaderShortcutItem(AnyVODEnums::ShortcutKey keyName, const QString &desc, int keySequence, void (ShaderCompositerDelegate::*slot)())
{
    this->createShortcutItem(keyName, desc, keySequence, &this->m_shaderDelegate, slot);
}

void ShortcutKey::createPlayListShortcutItem(AnyVODEnums::ShortcutKey keyName, const QString &desc, int keySequence, void (PlayList::*slot)())
{
    this->createShortcutItem(keyName, desc, keySequence, &this->m_mainWindow->getPlayList(), slot);
}

void ShortcutKey::reloadKey()
{
    this->deleteKey();
    this->setupKey();
}

QShortcut* ShortcutKey::getKey(AnyVODEnums::ShortcutKey key) const
{
    return this->m_shortcut[key].shortcut;
}

QString ShortcutKey::getShortcutKeyDesc(AnyVODEnums::ShortcutKey key) const
{
    return this->m_shortcut[key].desc;
}

QKeySequence ShortcutKey::getShortcutKey(AnyVODEnums::ShortcutKey key) const
{
    if (this->m_shortcut[key].shortcut)
        return this->m_shortcut[key].shortcut->key();
    else
        return QKeySequence();
}

bool ShortcutKey::isShortcutKeyEnabled(AnyVODEnums::ShortcutKey key) const
{
    if (this->m_shortcut[key].shortcut)
        return this->m_shortcut[key].shortcut->isEnabled();
    else
        return false;
}

void ShortcutKey::setShortcutKey(AnyVODEnums::ShortcutKey key, QKeySequence seq)
{
    if (this->m_shortcut[key].shortcut)
        this->m_shortcut[key].shortcut->setKey(seq);
}
