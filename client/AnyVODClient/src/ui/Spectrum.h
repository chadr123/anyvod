﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <stdint.h>
#include <QFrame>
#include <QElapsedTimer>

#include <bass/bass.h>

class Spectrum : public QFrame
{
    Q_OBJECT
public:
    enum SpectrumType
    {
        ST_WAVE = 0,
        ST_NORMAL_FFT,
        ST_AVERAGE_FFT,
        ST_3D_FFT,
        ST_COUNT
    };

public:
    explicit Spectrum(QWidget *parent = nullptr);
    virtual ~Spectrum();

    void init(HSTREAM stream);
    void deInit();
    void changeHandle(HSTREAM stream);

    void pause();
    void resume();
    bool isPaused() const;

    void updateSpectrum();

    void setType(SpectrumType type);
    SpectrumType getType() const;

protected:
    virtual void paintEvent(QPaintEvent *event);
    virtual void mousePressEvent(QMouseEvent *event);
    virtual void mouseReleaseEvent(QMouseEvent *event);
    virtual void resizeEvent(QResizeEvent *);

private:
    void allocBitmap();
    void freeBitmap();
    void clearBitmap();
    void drawSpectrum();

private:
    SpectrumType m_type;
    HSTREAM m_stream;
    int m_spectrumPos;
    uint32_t *m_pixels;
    int m_pixelsWidth;
    int m_pixelsHeight;
    bool m_pause;
    QElapsedTimer m_renderThrottle;

public:
    static const int SPECTRUM_ELAPSE;
};
