﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "CustomShortcut.h"
#include "ui_customshortcut.h"
#include "ShortcutEdit.h"
#include "ShortcutKey.h"
#include "utils/MessageBoxUtils.h"

#include <QLabel>

const QString CustomShortcut::PREFIX = "SK_";

CustomShortcut::CustomShortcut(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CustomShortcut)
{
    ui->setupUi(this);

#ifdef Q_OS_MAC
    const auto btns = this->findChildren<QPushButton*>();

    for (QPushButton *btn : btns)
        btn->setAttribute(Qt::WA_LayoutUsesWidgetRect);
#endif

    this->initKey();
    this->ui->tab->setCurrentIndex(0);
}

CustomShortcut::~CustomShortcut()
{
    delete ui;
}

void CustomShortcut::customEvent(QEvent *event)
{
    if (event->type() == ShortcutEdit::COMPLETE_EVENT)
    {
        ShortcutEdit::CompleteEvent *e = (ShortcutEdit::CompleteEvent*)event;
        QKeySequence seq = e->getkey();
        ShortcutEdit *sender = e->getSender();
        QString name = sender->objectName();
        int index;

        name.remove(PREFIX);
        index = name.toInt();

        for (int i = 0; i < AnyVODEnums::SK_COUNT; i++)
        {
            Item &item = this->m_table[i];

            if (!seq.isEmpty() && item.seq == seq && i != index)
            {
                MessageBoxUtils::criticalMessageBox(this, tr("\"%1\"와 중복 되는 단축키 입니다").arg(item.desc));
                sender->setText(this->m_table[index].seq.toString());

                return;
            }
        }

        this->m_table[index].seq = seq;
    }
    else if (event->type() == ShortcutEdit::CLEAR_EVENT)
    {
        ShortcutEdit::ClearEvent *e = (ShortcutEdit::ClearEvent*)event;
        ShortcutEdit *sender = e->getSender();
        QString name = sender->objectName();
        int index;

        name.remove(PREFIX);
        index = name.toInt();

        this->m_table[index].seq = QKeySequence();
    }
}

void CustomShortcut::changeEvent(QEvent *event)
{
    switch (event->type())
    {
        case QEvent::LanguageChange:
        {
            this->ui->retranslateUi(this);
            event->accept();

            break;
        }
        default:
        {
            break;
        }
    }
}

void CustomShortcut::initKey()
{
    const ShortcutKey &shortcutKey = ShortcutKey::getInstance();

    for (int i = 0; i < AnyVODEnums::SK_COUNT; i++)
    {
        Item &item = this->m_table[i];

        item.desc = shortcutKey.getShortcutKeyDesc((AnyVODEnums::ShortcutKey)i);
        item.seq = shortcutKey.getShortcutKey((AnyVODEnums::ShortcutKey)i);
    }

    this->createMainTab();
    this->createDTVTab();
    this->createRadioTab();
    this->createScreenTab();
    this->createPlayTab();
    this->createSubtitleTab();
    this->createAudioTab();
}

void CustomShortcut::addShortcutItem(QFormLayout *layout, AnyVODEnums::ShortcutKey key)
{
    QLabel *label = new QLabel(layout->parentWidget());
    ShortcutEdit *edit = new ShortcutEdit(layout->parentWidget());
    Item &item = this->m_table[key];

    label->setText(item.desc);

    edit->setText(item.seq.toString());
    edit->setObjectName(QString("%1%2").arg(PREFIX).arg(key));

    layout->addRow(label, edit);
}

void CustomShortcut::clearLayout(QFormLayout *layout)
{
    QLayoutItem *child;

    while (layout->count() > 0 && (child = layout->takeAt(0)) != nullptr)
    {
        delete child->widget();
        delete child;
    }
}

void CustomShortcut::createMainTab()
{
    QFormLayout *layout = this->ui->mainLayout;

    this->clearLayout(layout);

    this->addShortcutItem(layout, AnyVODEnums::SK_MOST_TOP);
    this->addShortcutItem(layout, AnyVODEnums::SK_LOGIN);
    this->addShortcutItem(layout, AnyVODEnums::SK_OPEN_REMOTE_FILE_LIST);
    this->addShortcutItem(layout, AnyVODEnums::SK_LOGOUT);
    this->addShortcutItem(layout, AnyVODEnums::SK_OPEN_SERVER_SETTING);
    this->addShortcutItem(layout, AnyVODEnums::SK_OPEN);
    this->addShortcutItem(layout, AnyVODEnums::SK_OPEN_EXTERNAL);
    this->addShortcutItem(layout, AnyVODEnums::SK_OPEN_DEVICE);
    this->addShortcutItem(layout, AnyVODEnums::SK_OPEN_AUDIO_CD);
    this->addShortcutItem(layout, AnyVODEnums::SK_ADD_TO_PLATLIST);
    this->addShortcutItem(layout, AnyVODEnums::SK_OPEN_PLAY_LIST);
    this->addShortcutItem(layout, AnyVODEnums::SK_CLOSE);
    this->addShortcutItem(layout, AnyVODEnums::SK_INFO);
    this->addShortcutItem(layout, AnyVODEnums::SK_DETAIL);
    this->addShortcutItem(layout, AnyVODEnums::SK_SHOW_CONTROL_BAR);
    this->addShortcutItem(layout, AnyVODEnums::SK_OPEN_CUSTOM_SHORTCUTS);
    this->addShortcutItem(layout, AnyVODEnums::SK_CHECK_UPDATE_GAP);

#if defined Q_OS_WIN || defined Q_OS_MAC
    this->addShortcutItem(layout, AnyVODEnums::SK_OPEN_FILE_ASSOCIATION);
#endif

    this->addShortcutItem(layout, AnyVODEnums::SK_LOAD_SETTINGS);
    this->addShortcutItem(layout, AnyVODEnums::SK_SAVE_SETTINGS);
    this->addShortcutItem(layout, AnyVODEnums::SK_CLEAR_PLAYLIST);
    this->addShortcutItem(layout, AnyVODEnums::SK_EXIT);
}

void CustomShortcut::createDTVTab()
{
    QFormLayout *layout = this->ui->dtvLayout;

    this->clearLayout(layout);

    this->addShortcutItem(layout, AnyVODEnums::SK_ADD_DTV_CHANNEL_TO_PLAYLIST);
    this->addShortcutItem(layout, AnyVODEnums::SK_OPEN_DTV_SCAN_CHANNEL);
    this->addShortcutItem(layout, AnyVODEnums::SK_VIEW_EPG);
}

void CustomShortcut::createRadioTab()
{
    QFormLayout *layout = this->ui->radioLayout;

    this->clearLayout(layout);

    this->addShortcutItem(layout, AnyVODEnums::SK_ADD_RADIO_CHANNEL_TO_PLAYLIST);
    this->addShortcutItem(layout, AnyVODEnums::SK_OPEN_RADIO_SCAN_CHANNEL);
}

void CustomShortcut::createScreenTab()
{
    QFormLayout *layout = this->ui->screenLayout;

    this->clearLayout(layout);

    this->addShortcutItem(layout, AnyVODEnums::SK_DEC_OPAQUE);
    this->addShortcutItem(layout, AnyVODEnums::SK_INC_OPAQUE);
    this->addShortcutItem(layout, AnyVODEnums::SK_MIN_OPAQUE);
    this->addShortcutItem(layout, AnyVODEnums::SK_MAX_OPAQUE);
    this->addShortcutItem(layout, AnyVODEnums::SK_SCREEN_SIZE_HALF);
    this->addShortcutItem(layout, AnyVODEnums::SK_SCREEN_SIZE_NORMAL);
    this->addShortcutItem(layout, AnyVODEnums::SK_SCREEN_SIZE_NORMAL_HALF);
    this->addShortcutItem(layout, AnyVODEnums::SK_SCREEN_SIZE_DOUBLE);
    this->addShortcutItem(layout, AnyVODEnums::SK_USER_ASPECT_RATIO_ORDER);
    this->addShortcutItem(layout, AnyVODEnums::SK_USER_ASPECT_RATIO);
    this->addShortcutItem(layout, AnyVODEnums::SK_RESET_VIDEO_ATTRIBUTE);
    this->addShortcutItem(layout, AnyVODEnums::SK_BRIGHTNESS_DOWN);
    this->addShortcutItem(layout, AnyVODEnums::SK_BRIGHTNESS_UP);
    this->addShortcutItem(layout, AnyVODEnums::SK_SATURATION_DOWN);
    this->addShortcutItem(layout, AnyVODEnums::SK_SATURATION_UP);
    this->addShortcutItem(layout, AnyVODEnums::SK_HUE_DOWN);
    this->addShortcutItem(layout, AnyVODEnums::SK_HUE_UP);
    this->addShortcutItem(layout, AnyVODEnums::SK_CONTRAST_DOWN);
    this->addShortcutItem(layout, AnyVODEnums::SK_CONTRAST_UP);
    this->addShortcutItem(layout, AnyVODEnums::SK_SHARPLY);
    this->addShortcutItem(layout, AnyVODEnums::SK_SHARPEN);
    this->addShortcutItem(layout, AnyVODEnums::SK_SOFTEN);
    this->addShortcutItem(layout, AnyVODEnums::SK_HISTOGRAM_EQ);
    this->addShortcutItem(layout, AnyVODEnums::SK_DEBAND);
    this->addShortcutItem(layout, AnyVODEnums::SK_DEBLOCK);
    this->addShortcutItem(layout, AnyVODEnums::SK_VIDEO_NORMALIZE);
    this->addShortcutItem(layout, AnyVODEnums::SK_HIGH_QUALITY_3D_DENOISE);
    this->addShortcutItem(layout, AnyVODEnums::SK_ATA_DENOISE);
    this->addShortcutItem(layout, AnyVODEnums::SK_OW_DENOISE);
    this->addShortcutItem(layout, AnyVODEnums::SK_NLMEANS_DENOISE);
    this->addShortcutItem(layout, AnyVODEnums::SK_VAGUE_DENOISE);
    this->addShortcutItem(layout, AnyVODEnums::SK_LEFT_RIGHT_INVERT);
    this->addShortcutItem(layout, AnyVODEnums::SK_TOP_BOTTOM_INVERT);
    this->addShortcutItem(layout, AnyVODEnums::SK_SELECT_CAPTURE_EXT_ORDER);
    this->addShortcutItem(layout, AnyVODEnums::SK_CAPTURE_SINGLE);
    this->addShortcutItem(layout, AnyVODEnums::SK_CAPTURE_MULTIPLE);
    this->addShortcutItem(layout, AnyVODEnums::SK_SELECT_CAPTURE_DIRECTORY);
    this->addShortcutItem(layout, AnyVODEnums::SK_OPEN_CAPTURE_DIRECTORY);
    this->addShortcutItem(layout, AnyVODEnums::SK_OPEN_SHADER_COMPOSITER);
    this->addShortcutItem(layout, AnyVODEnums::SK_DEINTERLACE_METHOD_ORDER);
    this->addShortcutItem(layout, AnyVODEnums::SK_DEINTERLACE_ALGORITHM_ORDER);
    this->addShortcutItem(layout, AnyVODEnums::SK_USE_IVTC);
    this->addShortcutItem(layout, AnyVODEnums::SK_FULL_SCREEN_RETURN);
    this->addShortcutItem(layout, AnyVODEnums::SK_FULL_SCREEN_ENTER);
    this->addShortcutItem(layout, AnyVODEnums::SK_HW_DECODER_ORDER);
    this->addShortcutItem(layout, AnyVODEnums::SK_USE_HW_DECODER);
    this->addShortcutItem(layout, AnyVODEnums::SK_USE_PBO);
    this->addShortcutItem(layout, AnyVODEnums::SK_USE_GPU_CONVERT);
    this->addShortcutItem(layout, AnyVODEnums::SK_USE_HDR);
    this->addShortcutItem(layout, AnyVODEnums::SK_USE_VSYNC);
    this->addShortcutItem(layout, AnyVODEnums::SK_SHOW_ALBUM_JACKET);
    this->addShortcutItem(layout, AnyVODEnums::SK_3D_VIDEO_METHOD_ORDER);
    this->addShortcutItem(layout, AnyVODEnums::SK_USE_3D_FULL);
    this->addShortcutItem(layout, AnyVODEnums::SK_ANAGLYPH_ALGORITHM_ORDER);
    this->addShortcutItem(layout, AnyVODEnums::SK_USE_FRAME_DROP);
    this->addShortcutItem(layout, AnyVODEnums::SK_SCREEN_ROTATION_DEGREE_ORDER);
    this->addShortcutItem(layout, AnyVODEnums::SK_PROJECTION_TYPE_ORDER);
    this->addShortcutItem(layout, AnyVODEnums::SK_USE_360_DEGREE);
    this->addShortcutItem(layout, AnyVODEnums::SK_UP_VR_PINCUSHION_COEFFICIENTS_K1);
    this->addShortcutItem(layout, AnyVODEnums::SK_DOWN_VR_PINCUSHION_COEFFICIENTS_K1);
    this->addShortcutItem(layout, AnyVODEnums::SK_UP_VR_PINCUSHION_COEFFICIENTS_K2);
    this->addShortcutItem(layout, AnyVODEnums::SK_DOWN_VR_PINCUSHION_COEFFICIENTS_K2);
    this->addShortcutItem(layout, AnyVODEnums::SK_VR_INPUT_SOURCE_ORDER);
    this->addShortcutItem(layout, AnyVODEnums::SK_UP_VR_LENS_CENTER_X);
    this->addShortcutItem(layout, AnyVODEnums::SK_DOWN_VR_LENS_CENTER_X);
    this->addShortcutItem(layout, AnyVODEnums::SK_UP_VR_LENS_CENTER_Y);
    this->addShortcutItem(layout, AnyVODEnums::SK_DOWN_VR_LENS_CENTER_Y);
    this->addShortcutItem(layout, AnyVODEnums::SK_UP_VIRTUAL_3D_DEPTH);
    this->addShortcutItem(layout, AnyVODEnums::SK_DOWN_VIRTUAL_3D_DEPTH);
    this->addShortcutItem(layout, AnyVODEnums::SK_UP_VR_BARREL_COEFFICIENTS_K1);
    this->addShortcutItem(layout, AnyVODEnums::SK_DOWN_VR_BARREL_COEFFICIENTS_K1);
    this->addShortcutItem(layout, AnyVODEnums::SK_UP_VR_BARREL_COEFFICIENTS_K2);
    this->addShortcutItem(layout, AnyVODEnums::SK_DOWN_VR_BARREL_COEFFICIENTS_K2);
    this->addShortcutItem(layout, AnyVODEnums::SK_DISTORION_ADJUST_MODE_ORDER);
    this->addShortcutItem(layout, AnyVODEnums::SK_VR_USE_DISTORTION);
    this->addShortcutItem(layout, AnyVODEnums::SK_AUTO_COLOR_CONVERSION);
    this->addShortcutItem(layout, AnyVODEnums::SK_VIDEO_ORDER);
}

void CustomShortcut::createPlayTab()
{
    QFormLayout *layout = this->ui->playLayout;

    this->clearLayout(layout);

    this->addShortcutItem(layout, AnyVODEnums::SK_TOGGLE);
    this->addShortcutItem(layout, AnyVODEnums::SK_TOGGLE_PLAY_MEDIA);
    this->addShortcutItem(layout, AnyVODEnums::SK_TOGGLE_PAUSE_MEDIA);
    this->addShortcutItem(layout, AnyVODEnums::SK_TOGGLE_PLAY_PAUSE_MEDIA);
    this->addShortcutItem(layout, AnyVODEnums::SK_STOP);
    this->addShortcutItem(layout, AnyVODEnums::SK_STOP_MEDIA);
    this->addShortcutItem(layout, AnyVODEnums::SK_LAST_PLAY);
    this->addShortcutItem(layout, AnyVODEnums::SK_SEEK_KEYFRAME);
    this->addShortcutItem(layout, AnyVODEnums::SK_PREV_FRAME);
    this->addShortcutItem(layout, AnyVODEnums::SK_NEXT_FRAME);
    this->addShortcutItem(layout, AnyVODEnums::SK_REWIND_5);
    this->addShortcutItem(layout, AnyVODEnums::SK_FORWARD_5);
    this->addShortcutItem(layout, AnyVODEnums::SK_REWIND_30);
    this->addShortcutItem(layout, AnyVODEnums::SK_FORWARD_30);
    this->addShortcutItem(layout, AnyVODEnums::SK_REWIND_60);
    this->addShortcutItem(layout, AnyVODEnums::SK_FORWARD_60);
    this->addShortcutItem(layout, AnyVODEnums::SK_GOTO_BEGIN);
    this->addShortcutItem(layout, AnyVODEnums::SK_PREV_CHAPTER);
    this->addShortcutItem(layout, AnyVODEnums::SK_NEXT_CHAPTER);
    this->addShortcutItem(layout, AnyVODEnums::SK_PREV);
    this->addShortcutItem(layout, AnyVODEnums::SK_PREV_MEDIA);
    this->addShortcutItem(layout, AnyVODEnums::SK_NEXT);
    this->addShortcutItem(layout, AnyVODEnums::SK_NEXT_MEDIA);
    this->addShortcutItem(layout, AnyVODEnums::SK_REPEAT_RANGE_START);
    this->addShortcutItem(layout, AnyVODEnums::SK_REPEAT_RANGE_END);
    this->addShortcutItem(layout, AnyVODEnums::SK_REPEAT_RANGE_ENABLE);
    this->addShortcutItem(layout, AnyVODEnums::SK_REPEAT_RANGE_START_BACK_100MS);
    this->addShortcutItem(layout, AnyVODEnums::SK_REPEAT_RANGE_START_FORW_100MS);
    this->addShortcutItem(layout, AnyVODEnums::SK_REPEAT_RANGE_END_BACK_100MS);
    this->addShortcutItem(layout, AnyVODEnums::SK_REPEAT_RANGE_END_FORW_100MS);
    this->addShortcutItem(layout, AnyVODEnums::SK_REPEAT_RANGE_BACK_100MS);
    this->addShortcutItem(layout, AnyVODEnums::SK_REPEAT_RANGE_FORW_100MS);
    this->addShortcutItem(layout, AnyVODEnums::SK_NORMAL_PLAYBACK);
    this->addShortcutItem(layout, AnyVODEnums::SK_SLOWER_PLAYBACK);
    this->addShortcutItem(layout, AnyVODEnums::SK_FASTER_PLAYBACK);
    this->addShortcutItem(layout, AnyVODEnums::SK_PLAY_ORDER);
    this->addShortcutItem(layout, AnyVODEnums::SK_SKIP_OPENING);
    this->addShortcutItem(layout, AnyVODEnums::SK_SKIP_ENDING);
    this->addShortcutItem(layout, AnyVODEnums::SK_USE_SKIP_RANGE);
    this->addShortcutItem(layout, AnyVODEnums::SK_SKIP_RANGE_SETTING);
    this->addShortcutItem(layout, AnyVODEnums::SK_SCREEN_EXPLORER);
    this->addShortcutItem(layout, AnyVODEnums::SK_USE_BUFFERING_MODE);
}

void CustomShortcut::createSubtitleTab()
{
    QFormLayout *layout = this->ui->subtitleLayout;

    this->clearLayout(layout);

    this->addShortcutItem(layout, AnyVODEnums::SK_SUBTITLE_TOGGLE);
    this->addShortcutItem(layout, AnyVODEnums::SK_CLOSE_EXTERNAL_SUBTITLE);
    this->addShortcutItem(layout, AnyVODEnums::SK_INC_SUBTITLE_OPAQUE);
    this->addShortcutItem(layout, AnyVODEnums::SK_DEC_SUBTITLE_OPAQUE);
    this->addShortcutItem(layout, AnyVODEnums::SK_RESET_SUBTITLE_OPAQUE);
    this->addShortcutItem(layout, AnyVODEnums::SK_INC_SUBTITLE_SIZE);
    this->addShortcutItem(layout, AnyVODEnums::SK_DEC_SUBTITLE_SIZE);
    this->addShortcutItem(layout, AnyVODEnums::SK_RESET_SUBTITLE_SIZE);
    this->addShortcutItem(layout, AnyVODEnums::SK_PREV_SUBTITLE_SYNC);
    this->addShortcutItem(layout, AnyVODEnums::SK_NEXT_SUBTITLE_SYNC);
    this->addShortcutItem(layout, AnyVODEnums::SK_RESET_SUBTITLE_SYNC);
    this->addShortcutItem(layout, AnyVODEnums::SK_RESET_SUBTITLE_POSITION);
    this->addShortcutItem(layout, AnyVODEnums::SK_UP_SUBTITLE_POSITION);
    this->addShortcutItem(layout, AnyVODEnums::SK_DOWN_SUBTITLE_POSITION);
    this->addShortcutItem(layout, AnyVODEnums::SK_LEFT_SUBTITLE_POSITION);
    this->addShortcutItem(layout, AnyVODEnums::SK_RIGHT_SUBTITLE_POSITION);
    this->addShortcutItem(layout, AnyVODEnums::SK_SUBTITLE_HALIGN_ORDER);
    this->addShortcutItem(layout, AnyVODEnums::SK_SUBTITLE_VALIGN_ORDER);
    this->addShortcutItem(layout, AnyVODEnums::SK_SUBTITLE_LANGUAGE_ORDER);
    this->addShortcutItem(layout, AnyVODEnums::SK_OPEN_TEXT_ENCODING);
    this->addShortcutItem(layout, AnyVODEnums::SK_ENABLE_SEARCH_SUBTITLE);
    this->addShortcutItem(layout, AnyVODEnums::SK_ENABLE_SEARCH_LYRICS);
    this->addShortcutItem(layout, AnyVODEnums::SK_AUTO_SAVE_SEARCH_LYRICS);
    this->addShortcutItem(layout, AnyVODEnums::SK_SAVE_SUBTITLE);
    this->addShortcutItem(layout, AnyVODEnums::SK_SAVE_AS_SUBTITLE);
    this->addShortcutItem(layout, AnyVODEnums::SK_3D_SUBTITLE_METHOD_ORDER);
    this->addShortcutItem(layout, AnyVODEnums::SK_RESET_3D_SUBTITLE_OFFSET);
    this->addShortcutItem(layout, AnyVODEnums::SK_UP_3D_SUBTITLE_OFFSET);
    this->addShortcutItem(layout, AnyVODEnums::SK_DOWN_3D_SUBTITLE_OFFSET);
    this->addShortcutItem(layout, AnyVODEnums::SK_LEFT_3D_SUBTITLE_OFFSET);
    this->addShortcutItem(layout, AnyVODEnums::SK_RIGHT_3D_SUBTITLE_OFFSET);
    this->addShortcutItem(layout, AnyVODEnums::SK_IMPORT_FONTS);
    this->addShortcutItem(layout, AnyVODEnums::SK_SEARCH_SUBTITLE_COMPLEX);
}

void CustomShortcut::createAudioTab()
{
    QFormLayout *layout = this->ui->audioLayout;

    this->clearLayout(layout);

    this->addShortcutItem(layout, AnyVODEnums::SK_AUDIO_DEVICE_ORDER);
    this->addShortcutItem(layout, AnyVODEnums::SK_USE_SPDIF);
    this->addShortcutItem(layout, AnyVODEnums::SK_USE_SPDIF_ENCODING_ORDER);
    this->addShortcutItem(layout, AnyVODEnums::SK_SPDIF_SAMPLE_RATE_ORDER);
    this->addShortcutItem(layout, AnyVODEnums::SK_SPDIF_AUDIO_DEVICE_ORDER);
    this->addShortcutItem(layout, AnyVODEnums::SK_AUDIO_NORMALIZE);
    this->addShortcutItem(layout, AnyVODEnums::SK_AUDIO_EQUALIZER);
    this->addShortcutItem(layout, AnyVODEnums::SK_OPEN_EQUALIZER_SETTING);
    this->addShortcutItem(layout, AnyVODEnums::SK_VOLUME_UP);
    this->addShortcutItem(layout, AnyVODEnums::SK_VOLUME_UP_MEDIA);
    this->addShortcutItem(layout, AnyVODEnums::SK_VOLUME_DOWN);
    this->addShortcutItem(layout, AnyVODEnums::SK_VOLUME_DOWN_MEDIA);
    this->addShortcutItem(layout, AnyVODEnums::SK_MUTE);
    this->addShortcutItem(layout, AnyVODEnums::SK_MUTE_MEDIA);
    this->addShortcutItem(layout, AnyVODEnums::SK_PREV_AUDIO_SYNC);
    this->addShortcutItem(layout, AnyVODEnums::SK_NEXT_AUDIO_SYNC);
    this->addShortcutItem(layout, AnyVODEnums::SK_RESET_AUDIO_SYNC);
    this->addShortcutItem(layout, AnyVODEnums::SK_AUDIO_ORDER);
    this->addShortcutItem(layout, AnyVODEnums::SK_LOWER_VOICE);
    this->addShortcutItem(layout, AnyVODEnums::SK_HIGHER_VOICE);
    this->addShortcutItem(layout, AnyVODEnums::SK_LOWER_MUSIC);
}

void CustomShortcut::on_ok_clicked()
{
    for (int i = 0; i < AnyVODEnums::SK_COUNT; i++)
        ShortcutKey::getInstance().setShortcutKey(AnyVODEnums::ShortcutKey(i), this->m_table[i].seq);

    this->accept();
}

void CustomShortcut::on_reset_clicked()
{
    if (MessageBoxUtils::questionMessageBox(this, tr("단축키를 초기화 하시겠습니까?")))
    {
        ShortcutKey::getInstance().reloadKey();
        this->initKey();
    }
}
