﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#ifndef UNICODE
# define UNICODE
#endif

#ifndef _UNICODE
# define _UNICODE
#endif

#include "PlayListUpdater.h"
#include "PlayList.h"
#include "pickers/URLPicker.h"
#include "utils/StringUtils.h"
#include "utils/RemoteFileUtils.h"
#include "utils/DeviceUtils.h"

#include <MediaInfoDLL/MediaInfo.h>

#include <QVector>
#include <QCoreApplication>
#include <QDebug>

using namespace MediaInfoDLL;

PlayListUpdater::PlayListUpdater(PlayList *parent) :
    m_parent(parent),
    m_stop(false)
{

}

void PlayListUpdater::setStop(bool stop)
{
    this->m_stop = stop;
}

void PlayListUpdater::run()
{
    QVector<PlayItem> list;
    bool updated = false;

    this->m_parent->getPlayList(&list);
    this->m_parent->setFixItemSize(true);

    for (const PlayItem &item : qAsConst(list))
    {
        if (this->m_stop)
            break;

        if (item.itemUpdated)
            continue;

        bool itemUpdated = false;
        QString title;
        QString totalTime;

        if (RemoteFileUtils::determinRemoteProtocol(item.path) && !RemoteFileUtils::determinRemoteFile(item.path))
        {
            URLPicker picker;
            QVector<URLPickerInterface::Item> urls;

            urls = picker.pickURL(item.path);

            if (!urls.isEmpty())
            {
                title = urls.first().title;
                itemUpdated = updated = true;
            }
        }
        else
        {
            MediaInfo mi;

            if (!DeviceUtils::determinDevice(item.path) && mi.Open(item.path.toStdWString()) == 1)
            {
                QString format = QString::fromStdWString(mi.Get(Stream_General, 0, __T("Format"))).trimmed();

                title = QString::fromStdWString(mi.Get(Stream_General, 0, __T("Title"))).trimmed();

                if (format.toLower() == "mpeg audio" && StringUtils::isLocal8bit(title))
                    title = QString::fromLocal8Bit(title.toLatin1());

                totalTime = QString::fromStdWString(mi.Get(Stream_General, 0, __T("Duration")));

                itemUpdated = updated = true;

                mi.Close();
            }
            else if (DeviceUtils::determinDevice(item.path) || DeviceUtils::determinIsDeviceHaveDuration(item.path))
            {
                title = item.title;
                itemUpdated = updated = true;
            }
        }

        if (itemUpdated)
            QCoreApplication::postEvent(this->m_parent, new PlayList::UpdateItemEvent(title, totalTime.toDouble() / 1000.0, item.unique));
        else
            QCoreApplication::postEvent(this->m_parent, new PlayList::UpdateIncCountEvent(item.unique));
    }

    this->m_parent->setFixItemSize(false);

    if (updated)
        QCoreApplication::postEvent(this->m_parent, new PlayList::UpdateParentEvent());
}
