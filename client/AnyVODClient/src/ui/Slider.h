﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <QSlider>

class Slider : public QSlider
{
    Q_OBJECT
public:
    explicit Slider(QWidget *parent = nullptr);

    bool isDragging() const;

protected:
    bool getPosition(const QPoint &pos, int *ret) const;
    int getPixelPosition(const int pos) const;

    void setDrag(bool drag);

private:
    int pick(const QPoint &pt) const;
    void getStyleOption(int *sliderMin, int *sliderMax, bool *upsideDown) const;

    int pixelPosToRangeValue(int pos) const;
    int rangeValueToPixelPos(int pos) const;

protected:
    virtual void mousePressEvent(QMouseEvent *ev);
    virtual void mouseReleaseEvent(QMouseEvent *ev);

private:
    bool m_dragging;
};
