﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <QDialog>

namespace Ui
{
    class MultipleCapture;
}

class Screen;
class MediaPlayer;

class MultipleCapture : public QDialog
{
    Q_OBJECT
public:
    explicit MultipleCapture(Screen *screen, QWidget *parent = nullptr);
    ~MultipleCapture();

private:
    virtual void timerEvent(QTimerEvent *event);
    virtual void changeEvent(QEvent *event);

private:
    int getTotalCount() const;

private:
    static const int CAPTURE_CHECK_TIME;

private slots:
    void on_changePath_clicked();
    void on_adjustResolution_clicked();
    void on_startCapture_clicked();
    void on_stopCapture_clicked();
    void on_captureWidth_editingFinished();
    void on_captureHeight_editingFinished();

private:
    Ui::MultipleCapture *ui;
    Screen *m_screen;
    MediaPlayer &m_player;
    bool m_captureWidthPrio;
};
