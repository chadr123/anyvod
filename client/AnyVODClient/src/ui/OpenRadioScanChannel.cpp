﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "OpenRadioScanChannel.h"
#include "ui_openradioscanchannel.h"
#include "ui/MainWindow.h"
#include "media/MediaPlayer.h"
#include "utils/MessageBoxUtils.h"
#include "utils/ConvertingUtils.h"

#include <QDebug>

OpenRadioScanChannel::OpenRadioScanChannel(RadioReader &reader, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::OpenRadioScanChannel),
    m_reader(reader),
    m_stop(false)
{
    ui->setupUi(this);

#ifdef Q_OS_MAC
    const auto btns = this->findChildren<QPushButton*>();

    for (QPushButton *btn : btns)
        btn->setAttribute(Qt::WA_LayoutUsesWidgetRect);
#endif

    QVector<RadioReaderInterface::AdapterInfo> adapters;

    this->m_reader.getAdapterList(&adapters);

    for (const RadioReaderInterface::AdapterInfo &info : qAsConst(adapters))
        this->ui->adapter->addItem(info.name, info.data.index);

    for (int i = RadioReaderInterface::DT_NONE + 1; i < RadioReaderInterface::DT_COUNT; i++)
    {
        QString name = this->m_reader.demodulatorTypeToString((RadioReaderInterface::DemodulatorType)i);

        if (!name.isEmpty())
            this->ui->type->addItem(name, i);
    }

    for (int i = QLocale::AnyCountry + 1; i < QLocale::LastCountry; i++)
    {
        QString name = QLocale::countryToString((QLocale::Country)i);

        if (!name.isEmpty())
            this->ui->country->addItem(name, i);
    }

    QLocale::Country country;
    RadioReaderInterface::DemodulatorType type;

    this->m_reader.getChannelCountry(&country, &type);

    this->ui->country->setCurrentText(QLocale::countryToString(country));
    this->ui->type->setCurrentText(this->m_reader.demodulatorTypeToString(type));

    this->updateChannelRange();

    this->ui->currentChannel->setText("0,");
    this->ui->scannedChannels->setText(QString::number(0));

    this->ui->progress->reset();
    this->ui->signalStrength->reset();

    this->initHeader();

    QVector<RadioReader::ChannelInfo> channels;

    this->m_reader.getScannedChannels(&channels);

    for (const RadioReader::ChannelInfo &info : qAsConst(channels))
        this->addChannel(info);
}

OpenRadioScanChannel::~OpenRadioScanChannel()
{
    delete ui;
}

void OpenRadioScanChannel::initHeader()
{
    QTreeWidget *channels = this->ui->channels;
    QStringList headers;

    headers.append(tr("순서"));
    headers.append(tr("신호 감도"));
    headers.append(tr("주파수"));

    channels->setColumnCount(headers.count());

    channels->setColumnWidth(0, 80);
    channels->setColumnWidth(1, 120);

    channels->setHeaderLabels(headers);
    channels->header()->setSectionsMovable(false);
}

void OpenRadioScanChannel::addChannel(const RadioReader::ChannelInfo &info)
{
    QStringList row;

    row.append(QString::number(info.channel));
    row.append(QString("%1%").arg(this->m_reader.getSignalStrengthInPercentage(info.signal)));
    row.append(QString("%1Hz").arg(ConvertingUtils::valueToString(info.freq)));

    this->ui->channels->addTopLevelItem(new QTreeWidgetItem((QTreeWidgetItem*)nullptr, row));
    this->m_channels.append(info);
}

bool OpenRadioScanChannel::existScannedChannel(int channel)
{
    for (const RadioReader::ChannelInfo &info : qAsConst(this->m_channels))
    {
        if (info.channel == channel)
            return true;
    }

    return false;
}

void OpenRadioScanChannel::changeEvent(QEvent *event)
{
    switch (event->type())
    {
        case QEvent::LanguageChange:
        {
            this->ui->retranslateUi(this);
            this->initHeader();

            event->accept();

            break;
        }
        default:
        {
            break;
        }
    }
}

void OpenRadioScanChannel::updateChannelRange()
{
    QVector<RadioChannelMap::Channel> channels;
    RadioReaderInterface::DemodulatorType type = (RadioReaderInterface::DemodulatorType)this->ui->type->currentData().toInt();

    if (this->m_reader.setChannelCountry((QLocale::Country)this->ui->country->currentData().toInt(), type))
        this->m_reader.getChannels(&channels);

    if (channels.isEmpty())
    {
        this->ui->start->setMinimum(0);
        this->ui->start->setMaximum(0);

        this->ui->end->setMinimum(0);
        this->ui->end->setMaximum(0);

        this->ui->start->setValue(0);
        this->ui->end->setValue(0);
    }
    else
    {
        int first = channels.first().channel;
        int last = channels.last().channel;

        this->ui->start->setMinimum(first);
        this->ui->start->setMaximum(last);

        this->ui->end->setMinimum(first);
        this->ui->end->setMaximum(last);

        this->ui->start->setValue(first);
        this->ui->end->setValue(last);
    }
}

void OpenRadioScanChannel::on_save_clicked()
{
    this->m_reader.setScannedChannels(this->m_channels);
    this->accept();
}

void OpenRadioScanChannel::on_scan_clicked()
{
    QVector<RadioChannelMap::Channel> channels;
    long adapter = this->ui->adapter->currentData().toInt();
    RadioReaderInterface::DemodulatorType type = (RadioReaderInterface::DemodulatorType)this->ui->type->currentData().toInt();
    QLocale::Country country = (QLocale::Country)this->ui->country->currentData().toInt();

    if (this->m_reader.setChannelCountry(country, type))
        this->m_reader.getChannels(&channels);

    if (channels.isEmpty())
    {
        MessageBoxUtils::criticalMessageBox(this, tr("채널 정보가 없습니다."));
        return;
    }

    int start = this->ui->start->value();
    int end = this->ui->end->value();

    if (start > end)
    {
        MessageBoxUtils::criticalMessageBox(this, tr("시작 채널이 종료 채널보다 큽니다."));
        return;
    }

    RadioChannelMap::Channel startInfo = channels.first();
    RadioChannelMap::Channel endInfo = channels.last();

    if (start < startInfo.channel || end > endInfo.channel)
    {
        MessageBoxUtils::criticalMessageBox(this, tr("채널 범위가 맞지 않습니다."));
        return;
    }

    this->ui->signalStrength->setMinimum(RadioReader::MIN_SIGNAL_STRENGTH_LINEAR);
    this->ui->signalStrength->setMaximum(RadioReader::MAX_SIGNAL_STRENGTH_LINEAR);
    this->ui->signalStrength->setValue(this->ui->signalStrength->minimum());

    this->ui->channels->clear();

    this->ui->currentChannel->setText("0,");
    this->ui->scannedChannels->setText(QString::number(0));

    this->ui->scan->setEnabled(false);
    this->ui->adapter->setEnabled(false);
    this->ui->type->setEnabled(false);
    this->ui->start->setEnabled(false);
    this->ui->end->setEnabled(false);
    this->ui->country->setEnabled(false);
    this->ui->save->setEnabled(false);
    this->ui->stopScan->setEnabled(true);

    this->m_channels.clear();

    long curAdapter;
    RadioReaderInterface::DemodulatorType curType;

    this->m_reader.getAdapter(&curAdapter, &curType);

    if (curAdapter == adapter)
        MediaPlayer::getInstance().close();

    int startChannelIndex = 0;
    int endChannelIndex = 0;

    for (int i = 0; i < channels.count(); i++)
    {
        if (channels[i].channel == start)
        {
            startChannelIndex = i;
            break;
        }
    }

    for (int i = channels.count() - 1; i >= 0; i--)
    {
        if (channels[i].channel == end)
        {
            endChannelIndex = i;
            break;
        }
    }

    this->ui->progress->setMinimum(startChannelIndex);
    this->ui->progress->setMaximum(endChannelIndex);
    this->ui->progress->reset();

    qApp->processEvents();
    qApp->processEvents();

    if (!this->m_reader.prepareScanning(adapter, type))
    {
        MessageBoxUtils::criticalMessageBox(this, tr("라디오 장치를 열 수 없습니다."));
        return;
    }

    for (int i = startChannelIndex; i <= endChannelIndex && i < channels.count(); i++)
    {
        const RadioChannelMap::Channel &channel = channels[i];
        RadioReader::ChannelInfo info;

        qApp->processEvents();
        qApp->processEvents();

        if (!this->existScannedChannel(channel.channel))
        {
            bool ok = this->m_reader.scan(country, channel, i, &info);

            if (ok)
            {
                info.index = i;
                info.country = country;
                info.freq = channel.freq;

                this->addChannel(info);
            }

            this->ui->signalStrength->setValue(this->m_reader.getSignalStrengthInLinear(info.signal));

            this->ui->currentChannel->setText(QString("%1(%2Hz),").arg(info.channel).arg(ConvertingUtils::valueToString(channel.freq)));
            this->ui->scannedChannels->setText(QString::number(this->m_channels.count()));
        }

        this->ui->progress->setValue(i);

        if (this->m_stop)
            break;
    }

    this->m_reader.finishScanning();

    this->m_stop = false;

    this->ui->signalStrength->reset();

    this->ui->scan->setEnabled(true);
    this->ui->adapter->setEnabled(true);
    this->ui->type->setEnabled(true);
    this->ui->start->setEnabled(true);
    this->ui->end->setEnabled(true);
    this->ui->country->setEnabled(true);
    this->ui->save->setEnabled(true);
    this->ui->stopScan->setEnabled(false);
}

void OpenRadioScanChannel::on_OpenRadioScanChannel_finished(int result)
{
    (void)result;

    this->m_stop = true;
}

void OpenRadioScanChannel::on_stopScan_clicked()
{
    this->m_stop = true;
}
