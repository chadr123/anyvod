﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "SubtitleDirectory.h"
#include "ui_subtitledirectory.h"

#include <QScrollBar>
#include <QFileInfo>
#include <QFileDialog>
#include <QUrl>
#include <QMenu>
#include <QMimeData>
#include <QDragEnterEvent>
#include <QDropEvent>

SubtitleDirectory::SubtitleDirectory(const QStringList &paths, bool prior, QWidget *parent) :
    ListWidgetDialog(parent),
    ui(new Ui::SubtitleDirectory),
    m_delShort(QKeySequence(Qt::Key_Delete), this, SLOT(deleteSelection()), nullptr),
    m_moveUpShort(QKeySequence(Qt::Key_Up | Qt::AltModifier), this, SLOT(moveUp()), nullptr),
    m_moveDownShort(QKeySequence(Qt::Key_Down | Qt::AltModifier), this, SLOT(moveDown()), nullptr),
    m_moveToTopShort(QKeySequence(Qt::Key_Home | Qt::AltModifier), this, SLOT(moveToTop()), nullptr),
    m_moveToBottomShort(QKeySequence(Qt::Key_End | Qt::AltModifier), this, SLOT(moveToBottom()), nullptr)
{
    ui->setupUi(this);

#ifdef Q_OS_MAC
    const auto btns = this->findChildren<QPushButton*>();

    for (QPushButton *btn : btns)
        btn->setAttribute(Qt::WA_LayoutUsesWidgetRect);

    const auto groups = this->findChildren<QGroupBox*>();

    for (QGroupBox *group : groups)
    {
        QFont font = group->font();

        font.setPointSizeF(13);
        group->setFont(font);
    }
#endif

    this->ui->prior->setChecked(prior);

    this->addDirectoryList(paths);
    this->setAcceptDrops(true);
}

SubtitleDirectory::~SubtitleDirectory()
{
    delete ui;
}

void SubtitleDirectory::getResult(QStringList *list, bool *prior) const
{
    *list = this->m_dirList;
    *prior = this->ui->prior->isChecked();
}

void SubtitleDirectory::dragEnterEvent(QDragEnterEvent *event)
{
    const QMimeData *mime = event->mimeData();

    if (mime->hasUrls())
        event->accept();
}

void SubtitleDirectory::dropEvent(QDropEvent *event)
{
    const QMimeData *mime = event->mimeData();

    if (mime->hasUrls())
    {
        const QList<QUrl> urls = mime->urls();
        QStringList paths;

        for (int i = 0; i < urls.count(); i++)
        {
            QString path = urls[i].toLocalFile();
            QFileInfo info(path);

            if (info.isDir())
                paths.append(path);
        }

        this->addDirectoryList(paths);
    }
}

void SubtitleDirectory::contextMenuEvent(QContextMenuEvent *)
{
    QMenu menu;
    QListWidget *listWidget = this->ui->list;
    const QList<QListWidgetItem*> &list = this->sortByRow(listWidget->selectedItems(), *listWidget);
    int startRow;
    int endRow;

    if (list.empty())
    {
        startRow = -1;
        endRow = 1;
    }
    else
    {
        startRow = listWidget->row(list.first()) - 1;
        endRow = listWidget->row(list.last()) + 1;
    }

    QAction *action = nullptr;

    action = menu.addAction(tr("삭제"));
    action->setShortcut(this->m_delShort.key());
    action->setShortcutVisibleInContextMenu(true);
    action->setEnabled(list.count() > 0);
    connect(action, &QAction::triggered, this, &SubtitleDirectory::deleteSelection);

    menu.addSeparator();

    action = menu.addAction(tr("맨 위로"));
    action->setShortcut(this->m_moveToTopShort.key());
    action->setShortcutVisibleInContextMenu(true);
    action->setEnabled(startRow >= 0);
    connect(action, &QAction::triggered, this, &SubtitleDirectory::moveToTop);

    action = menu.addAction(tr("맨 아래로"));
    action->setShortcut(this->m_moveToBottomShort.key());
    action->setShortcutVisibleInContextMenu(true);
    action->setEnabled(endRow < listWidget->count());
    connect(action, &QAction::triggered, this, &SubtitleDirectory::moveToBottom);

    menu.addSeparator();

    action = menu.addAction(tr("위로"));
    action->setShortcut(this->m_moveUpShort.key());
    action->setShortcutVisibleInContextMenu(true);
    action->setEnabled(startRow >= 0);
    connect(action, &QAction::triggered, this, &SubtitleDirectory::moveUp);

    action = menu.addAction(tr("아래로"));
    action->setShortcut(this->m_moveDownShort.key());
    action->setShortcutVisibleInContextMenu(true);
    action->setEnabled(endRow < listWidget->count());
    connect(action, &QAction::triggered, this, &SubtitleDirectory::moveDown);

    menu.exec(QCursor::pos());
}

void SubtitleDirectory::changeEvent(QEvent *event)
{
    switch (event->type())
    {
        case QEvent::LanguageChange:
        {
            this->ui->retranslateUi(this);
            event->accept();

            break;
        }
        default:
        {
            break;
        }
    }
}

void SubtitleDirectory::selectItemOption(int index, QItemSelectionModel::SelectionFlags option)
{
    this->ui->list->setCurrentRow(index, option);
}

void SubtitleDirectory::selectItem(int index)
{
    this->selectItemOption(index, QItemSelectionModel::ClearAndSelect);
}

void SubtitleDirectory::addDirectoryList(const QStringList &paths)
{
    for (int i = 0; i < paths.count(); i++)
    {
        QString path = paths[i];
        QListWidgetItem *item = new QListWidgetItem;

        if (path.length() != 0 && path[path.length() - 1] != QDir::separator())
            path.append(QDir::separator());

        path = QDir::toNativeSeparators(path);

        item->setText(path);
        item->setData(Qt::UserRole, path);
        item->setToolTip(path);

        this->ui->list->addItem(item);
    }
}

void SubtitleDirectory::moveToTop()
{
    this->moveItems(this->ui->list->selectedItems(), 0, false);
}

void SubtitleDirectory::moveToBottom()
{
    this->moveItems(this->ui->list->selectedItems(), this->ui->list->count() - 1, true);
}

void SubtitleDirectory::moveUp()
{
    QListWidget *listWidget = this->ui->list;
    const QList<QListWidgetItem*> &list = this->sortByRow(listWidget->selectedItems(), *listWidget);
    int scroll = listWidget->verticalScrollBar()->value();

    if (list.count() > 0)
    {
        for (int i = 0; i < list.count(); i++)
        {
            int select = listWidget->row(list[i]);

            if (select <= 0)
                return;

            QListWidgetItem *item = listWidget->takeItem(select);

            listWidget->insertItem(select - 1, item);
            item->setSelected(true);
        }
    }

    listWidget->verticalScrollBar()->setValue(scroll);
    this->selectItemOption(listWidget->row(list.first()), QItemSelectionModel::Current);
}

void SubtitleDirectory::moveDown()
{
    QListWidget *listWidget = this->ui->list;
    const QList<QListWidgetItem*> &list = this->sortByRow(listWidget->selectedItems(), *listWidget);
    int scroll = listWidget->verticalScrollBar()->value();

    if (list.count() > 0)
    {
        for (int i = list.count() - 1; i >= 0; i--)
        {
            int select = listWidget->row(list[i]);

            if (select >= listWidget->count() - 1)
                return;

            QListWidgetItem *item = listWidget->takeItem(select);

            listWidget->insertItem(select + 1, item);
            item->setSelected(true);
        }
    }

    listWidget->verticalScrollBar()->setValue(scroll);
    this->selectItemOption(listWidget->row(list.last()), QItemSelectionModel::Current);
}

void SubtitleDirectory::moveItems(const QList<QListWidgetItem*> &list, int rowTo, bool dirBottom)
{
    QListWidget *listWidget = this->ui->list;
    QList<QListWidgetItem*> sorted = this->sortByRow(list, *listWidget);
    int scroll = listWidget->verticalScrollBar()->value();
    int curIndex = -1;

    if (sorted.count() > 0)
    {
        if (dirBottom)
            curIndex = listWidget->row(sorted.first());
        else
            curIndex = listWidget->row(sorted.last());
    }

    for (int i = 0; i < sorted.count(); i++)
    {
        if (rowTo >= 0 && rowTo <= listWidget->count())
        {
            QListWidgetItem *item = sorted[i];
            int row = listWidget->row(item);

            item = listWidget->takeItem(row);
            listWidget->insertItem(rowTo, item);
        }

        if (!dirBottom)
            rowTo++;
    }

    listWidget->verticalScrollBar()->setValue(scroll);

    if (curIndex >= listWidget->count())
        curIndex = listWidget->count() - 1;

    if (curIndex >= 0)
        this->selectItem(curIndex);
}

void SubtitleDirectory::deleteSelection()
{
    QListWidget *list = this->ui->list;
    const QList<QListWidgetItem*> &selectedItems = this->sortByRow(list->selectedItems(), *list);
    int scroll = list->verticalScrollBar()->value();
    int curIndex = -1;

    if (selectedItems.count() > 0)
        curIndex = list->row(selectedItems.first());

    for (int i = 0; i < selectedItems.count(); i++)
    {
        QListWidgetItem *item = selectedItems[i];

        delete item;
    }

    list->verticalScrollBar()->setValue(scroll);

    if (curIndex >= list->count())
        curIndex = list->count() - 1;

    if (curIndex >= 0)
        this->selectItem(curIndex);
}

void SubtitleDirectory::on_add_clicked()
{
    QString dir = QFileDialog::getExistingDirectory(this);

    this->addDirectoryList(QStringList(dir));
}

void SubtitleDirectory::on_apply_clicked()
{
    Ui::SubtitleDirectory *ctrl = this->ui;

    for (int i = 0; i < ctrl->list->count(); i++)
    {
        QString path = ctrl->list->item(i)->data(Qt::UserRole).toString();

        this->m_dirList.append(path);
    }

    this->accept();
}
