﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <QDialog>

namespace Ui
{
    class Popup;
}

class Popup : public QDialog
{
    Q_OBJECT
public:
    explicit Popup(QWidget *parent = nullptr);
    ~Popup();

    void showText(const QString &text, const QString &url);

private:
    void killAllTimers();

private:
    virtual void timerEvent(QTimerEvent *event);
    virtual void hideEvent(QHideEvent *);
    virtual void changeEvent(QEvent *event);

private slots:
    void on_text_linkActivated(const QString &link);

private:
    Ui::Popup *ui;
    int m_showTimer;
    int m_showingTimer;
    int m_hideTimer;

private:
    static const int SHOW_TIME;
    static const int SHOWING_TIME;
    static const int HIDE_TIME;
};
