﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "Spectrum.h"
#include "utils/MathUtils.h"

#ifndef Q_OS_MAC
# include <malloc.h>
#endif

#include <cmath>

#include <QPainter>
#include <QPaintEvent>
#include <QImage>
#include <QDebug>

const int Spectrum::SPECTRUM_ELAPSE = 25;

Spectrum::Spectrum(QWidget *parent) :
    QFrame(parent),
    m_type(ST_AVERAGE_FFT),
    m_stream(0),
    m_spectrumPos(0),
    m_pixels(nullptr),
    m_pixelsWidth(0),
    m_pixelsHeight(0),
    m_pause(false)
{

}

Spectrum::~Spectrum()
{
    this->freeBitmap();
}

void Spectrum::allocBitmap()
{
    QRect rect = this->contentsRect();
    int count  = rect.width() * rect.height();

    this->freeBitmap();
    this->m_pixels = new uint32_t[count];
    memset(this->m_pixels, 0, count * sizeof(uint32_t));
    this->m_pixelsWidth = rect.width();
    this->m_pixelsHeight = rect.height();
}

void Spectrum::freeBitmap()
{
    delete[] this->m_pixels;
    this->m_pixels = nullptr;
    this->m_pixelsWidth = 0;
    this->m_pixelsHeight = 0;
}

void Spectrum::clearBitmap()
{
    if (!this->m_pixels)
        return;

    QRgb rgb = this->palette().window().color().rgb();

    for (int i = 0; i < this->m_pixelsHeight * this->m_pixelsWidth; i++)
        this->m_pixels[i] = rgb;
}

void Spectrum::init(HSTREAM stream)
{
    this->m_stream = stream;
    this->m_spectrumPos = 0;
    this->allocBitmap();
    this->m_renderThrottle.restart();
}

void Spectrum::deInit()
{
    this->m_stream = 0;
    this->freeBitmap();
}

void Spectrum::changeHandle(HSTREAM stream)
{
    this->m_stream = stream;
}

void Spectrum::pause()
{
    this->m_pause = true;
}

void Spectrum::resume()
{
    this->m_pause = false;
}

bool Spectrum::isPaused() const
{
    return this->m_pause;
}

void Spectrum::setType(SpectrumType type)
{
    this->m_type = type;
}

Spectrum::SpectrumType Spectrum::getType() const
{
    return this->m_type;
}

void Spectrum::updateSpectrum()
{
    this->drawSpectrum();
    this->update();
}

void Spectrum::drawSpectrum()
{
    const int baseHue = 120;

    if (this->m_stream != 0 && this->m_pixels)
    {
        int height = this->m_pixelsHeight;
        int width = this->m_pixelsWidth;
        int count = width * height;
        BASS_CHANNELINFO ci;

        if (!BASS_ChannelGetInfo(this->m_stream, &ci))
            ci.chans = 1;

        if (this->m_type == ST_WAVE)
        {
            int y = 0;
            int size = 0;
            float *buf = nullptr;

            size = ci.chans * width * sizeof(float);
            buf = (float*)alloca(size);

            if ((int)BASS_ChannelGetData(this->m_stream, buf, size | BASS_DATA_FLOAT) != -1)
            {
                memset(this->m_pixels, 0, count * sizeof(uint32_t));

                for (DWORD channel = 0; channel < ci.chans; channel++)
                {
                    QColor color;

                    switch (channel)
                    {
                    case 0:
                        color = QColor(255, 0, 0);
                        break;
                    case 1:
                        color = QColor(0, 255, 0);
                        break;
                    case 2:
                        color = QColor(0, 0, 255);
                        break;
                    case 3:
                        color = QColor(255, 255, 255);
                        break;
                    case 4:
                        color = QColor(127, 255, 255);
                        break;
                    case 5:
                        color = QColor(255, 127, 255);
                        break;
                    case 6:
                        color = QColor(255, 255, 127);
                        break;
                    case 7:
                        color = QColor(63, 127, 127);
                        break;
                    default:
                        color = QColor(127, 63, 127);
                        break;
                    }

                    for (int x = 0; x < width; x++)
                    {
                        int v = (1 - buf[x * ci.chans + channel]) * height / 2; // invert and scale to fit display

                        if (v < 0)
                            v = 0;
                        else if (v >= height)
                            v = height - 1;

                        if (x == 0)
                            y = v;

                        do
                        { // draw line from previous sample...
                          if (y < v)
                              y++;
                          else if (y > v)
                              y--;

                          this->m_pixels[y * width + x] = color.rgb();

                        } while (y != v);
                    }
                }
            }
            else
            {
                this->clearBitmap();
            }
        }
        else
        {
            int fftCount;
            int fftFlag;

            if (width <= 128)
            {
                fftCount = 128;
                fftFlag = BASS_DATA_FFT256;
            }
            else if (width <= 256)
            {
                fftCount = 256;
                fftFlag = BASS_DATA_FFT512;
            }
            else if (width <= 512)
            {
                fftCount = 512;
                fftFlag = BASS_DATA_FFT1024;
            }
            else if (width <= 1024)
            {
                fftCount = 1024;
                fftFlag = BASS_DATA_FFT2048;
            }
            else if (width <= 2048)
            {
                fftCount = 2048;
                fftFlag = BASS_DATA_FFT4096;
            }
            else if (width <= 4096)
            {
                fftCount = 4096;
                fftFlag = BASS_DATA_FFT8192;
            }
            else
            {
                fftCount = 8192;
                fftFlag = BASS_DATA_FFT16384;
            }

            float fft[fftCount];
            double scale = sqrt((double)ci.chans) * 1.5;

            memset(fft, 0, sizeof(fft));

            if ((int)BASS_ChannelGetData(this->m_stream, fft, fftFlag) != -1) // get the FFT data
            {
                if (this->m_type == ST_NORMAL_FFT)
                {
                    int y = 0;
                    int y1 = 0;

                    memset(this->m_pixels, 0, count * sizeof(uint32_t));

                    for (int x = 0; x < width / 2 && x < fftCount / 2; x++)
                    {
                        y = sqrt(fft[x + 1]) * scale * height - 4; // scale it (sqrt to make low values more visible)

                        if (y > height)
                            y = height; // cap it
                        else if (y < 0)
                            y = 0;

                        if (x && (y1 = (y + y1) / 2)) // interpolate from previous to make the display smoother
                        {
                            while (--y1 >= 0)
                            {
                                QColor color;

                                color.setHsv(baseHue - MathUtils::mapTo(height, baseHue, y1 + 1), 255, 219);
                                this->m_pixels[y1 * width + x * 2 - 1] = color.rgb();
                            }
                        }

                        y1 = y;

                        while (--y >= 0)
                        {
                            QColor color;

                            color.setHsv(baseHue - MathUtils::mapTo(height, baseHue, y + 1), 255, 219);
                            this->m_pixels[y * width + x * 2] = color.rgb(); // draw level
                        }
                    }
                }
                else if (this->m_type == ST_AVERAGE_FFT)
                { // logarithmic, acumulate & average bins
                    int b0 = 0;
                    int barWidth = 6;
                    int bands = width / barWidth;
                    int prevY = 0;
                    QColor foreColor = this->palette().windowText().color();

                    memset(this->m_pixels, 0, count * sizeof(uint32_t));

                    for (int x = 0; x < bands; x++)
                    {
                        float peak = 0.0f;
                        int b1 = pow(2, x * 10.0 / (bands - 1));

                        if (b1 > fftCount - 1)
                            b1 = fftCount - 1;

                        if (b1 <= b0)
                            b1 = b0 + 1; // make sure it uses at least 1 FFT bin

                        for (; b0 < b1 && b0 < fftCount; b0++)
                        {
                            if (b0 + 1 < fftCount && peak < fft[b0 + 1])
                                peak = fft[b0 + 1];
                        }

                        int y = sqrt(peak) * scale * height - 4; // scale it (sqrt to make low values more visible)

                        if (y > height)
                            y = height; // cap it
                        else if (y < 0)
                            y = 0;

                        int curY = y - 1;

                        if (--y >= 0)
                        {
                            uint32_t *p = this->m_pixels + (y * width + x * barWidth);

                            for (int i = 0; i < barWidth; i++)
                                p[i] = foreColor.rgb();

                            if (x > 0)
                                p[-1] = foreColor.rgb();
                            else if (x == 0)
                                p[0] = foreColor.rgb(); // draw border
                        }

                        while (--y >= 0)
                        {
                            QColor color;
                            uint32_t *p = this->m_pixels + (y * width + x * barWidth);

                            color.setHsv(baseHue - MathUtils::mapTo(height, baseHue, y + 1), 255, 219);

                            for (int i = 0; i < barWidth - 1; i++)
                                p[i] = color.rgb(); // draw bar

                            if (prevY <= y && x > 0)
                                p[-1] = foreColor.rgb(); // draw border
                            else if (x == 0)
                                p[0] = foreColor.rgb(); // draw border

                            p[barWidth - 1] = foreColor.rgb(); // draw border
                        }

                        prevY = curY;
                    }

                    for (int i = 0; i < width; i++)
                        this->m_pixels[i] = foreColor.rgb();
                }
                else
                { // "3D"
                    for (int x = 0; x < height && x < fftCount; x++)
                    {
                        int y = sqrt(fft[x + 1]) * scale * height - 4; // scale it (sqrt to make low values more visible)

                        if (y > height)
                            y = height; // cap it
                        else if (y < 0)
                            y = 0;

                        QColor color;

                        color.setHsv(baseHue - MathUtils::mapTo(height, baseHue, y), 255, abs(MathUtils::mapTo(height, 255, y)));

                        this->m_pixels[x * width + this->m_spectrumPos] = color.rgb(); // plot it
                    }

                    // move marker onto next position
                    if (!this->m_pause)
                    {
                        if (this->m_renderThrottle.elapsed() >= SPECTRUM_ELAPSE)
                        {
                            this->m_spectrumPos = (this->m_spectrumPos + 1) % width;
                            this->m_renderThrottle.restart();
                        }
                    }

                    for (int x = 0; x < height; x++)
                        this->m_pixels[x * width + this->m_spectrumPos] = this->palette().windowText().color().rgb();
                }
            }
            else
            {
                this->clearBitmap();
            }
        }
    }
    else
    {
        this->clearBitmap();
    }
}

void Spectrum::resizeEvent(QResizeEvent *)
{
    if (this->m_type == ST_3D_FFT)
    {
        if (this->m_pixels)
        {
            const QSize oldSize(this->m_pixelsWidth, this->m_pixelsHeight);
            int oldCount = oldSize.height() * oldSize.width();
            uint32_t *p = new uint32_t[oldCount];

            memcpy(p, this->m_pixels, oldCount * sizeof(uint32_t));
            this->allocBitmap();

            QRect rect = this->contentsRect();
            QImage oldImage((uchar*)p, oldSize.width(), oldSize.height(), QImage::Format_ARGB32);
            QImage newImage((uchar*)this->m_pixels, rect.width(), rect.height(), QImage::Format_ARGB32);
            QPainter newImagePainter(&newImage);

            newImagePainter.drawImage(0, 0, oldImage);

            if (this->m_spectrumPos >= this->m_pixelsWidth)
                this->m_spectrumPos = 0;

            delete[] p;
        }
        else
        {
            this->allocBitmap();
            this->m_spectrumPos = 0;
        }
    }
    else
    {
        this->allocBitmap();
    }
}

void Spectrum::mousePressEvent(QMouseEvent *event)
{
    event->accept();
}

void Spectrum::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton && this->m_stream != 0)
    {
        this->m_type = (SpectrumType)(this->m_type + 1);

        if (this->m_type == ST_3D_FFT)
        {
            memset(this->m_pixels, 0, this->m_pixelsWidth * this->m_pixelsHeight * sizeof(uint32_t));
            this->m_spectrumPos = 0;
        }

        if (this->m_type >= ST_COUNT)
            this->m_type = ST_WAVE;
    }
}

void Spectrum::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);

    this->drawSpectrum();

    painter.setBackground(Qt::transparent);
    painter.eraseRect(event->rect());

    if (this->m_pixels)
    {
        QImage spectrum((uchar*)this->m_pixels, this->m_pixelsWidth, this->m_pixelsHeight, QImage::Format_ARGB32);

        painter.drawImage(this->contentsRect().topLeft(), spectrum.mirrored());
    }

    QFrame::paintEvent(event);
}
