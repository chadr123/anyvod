﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "Trackbar.h"
#include "utils/ChapterInfoUtils.h"
#include "utils/ConvertingUtils.h"

#include <limits>
#include <QMouseEvent>
#include <QPainter>
#include <QDebug>

using namespace std;

Trackbar::Trackbar(QWidget *parent) :
    Slider(parent)
{
    this->setMouseTracking(true);

    this->setAttribute(Qt::WA_TouchPadAcceptSingleTouchEvents);
    this->setAttribute(Qt::WA_AcceptTouchEvents);
    this->setAttribute(Qt::WA_NoMousePropagation);
}

void Trackbar::setChapters(const QVector<ChapterInfo> &chapters)
{
    this->m_chapters = chapters;
}

void Trackbar::clearChapters()
{
    this->m_chapters.clear();
    this->update();
}

void Trackbar::mousePressEvent(QMouseEvent *ev)
{
    if (ev->button() == Qt::LeftButton)
    {
        int pos;

        this->getPosition(ev->pos(), &pos);

        this->setSliderDown(true);
        this->setSliderPosition(pos);

        this->setDrag(true);

        ev->accept();
    }

    QSlider::mousePressEvent(ev);
}

void Trackbar::mouseReleaseEvent(QMouseEvent *ev)
{
    QSlider::mouseReleaseEvent(ev);

    if (ev->button() == Qt::LeftButton)
    {
        this->processMouseRelease();

        ev->accept();
    }
}

void Trackbar::mouseMoveEvent(QMouseEvent *ev)
{
    QString time;
    QString chapter;
    QString length;

    if (this->maximum() <= 0)
    {
        time = tr("재생 위치");
    }
    else
    {
        int pos = 0;
        ChapterInfo info;

        this->getPosition(ev->pos(), &pos);
        ConvertingUtils::getTimeString(pos, ConvertingUtils::TIME_HH_MM_SS, &time);

        if (this->findChapter(pos, &info))
        {
            int len;

            if (info.end == numeric_limits<int>::max())
                info.end = this->maximum();

            len = info.end - info.start;
            ConvertingUtils::getTimeString(len, ConvertingUtils::TIME_HH_MM_SS, &length);

            length = " (" + length + ")";
            chapter = info.desc;
        }
    }

    if (!chapter.isEmpty())
        chapter = " - " + chapter;

    this->setToolTip(time + chapter + length);

    QSlider::mouseMoveEvent(ev);
}

void Trackbar::paintEvent(QPaintEvent *ev)
{
    QPainter painter;
    const int penSize = 2;
    const int lineHeight = 5;
    const int offset = penSize / 2;

    painter.begin(this);

    painter.setPen(QPen(this->palette().windowText().color(), penSize));

    for (int i = 0; i < this->m_chapters.count(); i++)
    {
        const ChapterInfo &info = this->m_chapters[i];
        int pixel = this->getPixelPosition(info.start);
        QPoint start;
        QPoint end;

        start.setX(pixel + offset);
        start.setY(this->height() - lineHeight);

        end.setX(pixel + offset);
        end.setY(this->height());

        painter.drawLine(start, end);
    }

    painter.end();

    Slider::paintEvent(ev);
}

bool Trackbar::event(QEvent *event)
{
    switch (event->type())
    {
        case QEvent::TouchBegin:
            break;
        case QEvent::TouchEnd:
        case QEvent::TouchCancel:
            this->processMouseRelease();
            break;
        default:
            return Slider::event(event);
    }

    event->accept();

    return true;
}

bool Trackbar::findChapter(double pos, ChapterInfo *ret) const
{
    int index = ChapterInfoUtils::findChapter(pos, this->m_chapters);

    if (index >= 0)
    {
        *ret = this->m_chapters[index];
        return true;
    }

    return false;
}

void Trackbar::processMouseRelease()
{
    if (this->isSliderDown())
        this->setSliderDown(false);

    this->setDrag(false);
}
