﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "ScreenExplorer.h"
#include "ui_screenexplorer.h"
#include "MainWindowInterface.h"
#include "ScreenExplorerItem.h"
#include "media/MediaPlayer.h"
#include "media/FrameExtractor.h"
#include "utils/TimeUtils.h"
#include "factories/MainWindowFactory.h"

#include <QListWidget>
#include <QResizeEvent>
#include <QDebug>

const QEvent::Type ScreenExplorer::UPDATE_ITEM_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type ScreenExplorer::UPDATE_PARENT_EVENT = (QEvent::Type)QEvent::registerEventType();

ScreenExplorer::ScreenExplorer(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ScreenExplorer),
    m_player(MediaPlayer::getInstance()),
    m_updater(this),
    m_stopAddingItem(false),
    m_currentPlayingIndex(-1)
{
    ui->setupUi(this);

#ifdef Q_OS_MAC
    const auto btns = this->findChildren<QPushButton*>();

    for (QPushButton *btn : btns)
        btn->setAttribute(Qt::WA_LayoutUsesWidgetRect);
#endif

    this->ui->step->setTimeRange(QTime(0, 0, 1), QTime::fromMSecsSinceStartOfDay(this->m_player.getDuration() * 1000));
    this->ui->step->setTime(QTime(0, 5, 0));
    this->ui->step->setCurrentSection(QDateTimeEdit::NoSection);
}

ScreenExplorer::ScreenExplorer(const QString &filePath, int currentIndex, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ScreenExplorer),
    m_player(MediaPlayer::getInstance()),
    m_updater(this),
    m_stopAddingItem(false),
    m_currentPlayingIndex(currentIndex)
{
    ui->setupUi(this);

#ifdef Q_OS_MAC
    const auto btns = this->findChildren<QPushButton*>();

    for (QPushButton *btn : btns)
        btn->setAttribute(Qt::WA_LayoutUsesWidgetRect);

    const auto groups = this->findChildren<QGroupBox*>();

    for (QGroupBox *group : groups)
    {
        QFont font = group->font();

        font.setPointSizeF(13);
        group->setFont(font);
    }
#endif

    FrameExtractor ex;
    double duration = 1.0;

    if (ex.open(filePath, this->m_player.isUsingAutoColorConversion()))
        duration = ex.getDuration();

    ex.close();

    this->m_filePath = filePath;

    this->ui->step->setTimeRange(QTime(0, 0, 1), QTime::fromMSecsSinceStartOfDay(duration * 1000));
    this->ui->step->setTime(QTime(0, 5, 0));
    this->ui->step->setCurrentSection(QDateTimeEdit::NoSection);
}

ScreenExplorer::~ScreenExplorer()
{
    delete ui;
}

void ScreenExplorer::updateScreen()
{
    if (this->m_updater.isRunning())
    {
        this->m_stopAddingItem = true;
        this->m_updater.stopUpdate();
    }

    this->ui->screens->clear();

    QString filePath = this->m_filePath.isEmpty() ? this->m_player.getFilePath() : this->m_filePath;

    this->m_updater.startUpdate(filePath, -this->ui->step->time().secsTo(TimeUtils::ZERO_TIME), this->m_player.isUsingAutoColorConversion());
}

void ScreenExplorer::on_screens_itemActivated(QListWidgetItem *item)
{
    double time = item->data(Qt::UserRole).toDouble();

    if (this->m_player.getFilePath() != this->m_filePath && this->m_currentPlayingIndex != -1)
    {
        MainWindowInterface *window = MainWindowFactory::getInstance();
        bool goLast = this->m_player.isGotoLastPos();

        this->m_player.enableGotoLastPos(!goLast, true);
        window->playAt(this->m_currentPlayingIndex);
        this->m_player.enableGotoLastPos(goLast, true);
    }

    this->m_player.seek(time, true);
}

void ScreenExplorer::customEvent(QEvent *event)
{
    if (event->type() == UPDATE_ITEM_EVENT)
    {
        if (this->m_stopAddingItem)
            return;

        UpdateItemEvent *e = (UpdateItemEvent*)event;
        QListWidget *list = this->ui->screens;
        QListWidgetItem *widgetItem = new QListWidgetItem;
        ScreenExplorerItem *screenItem = new ScreenExplorerItem(e->getTime(), e->getRotation(), e->getScreen(), list);

        widgetItem->setSizeHint(screenItem->sizeHint());
        widgetItem->setData(Qt::UserRole, e->getTime());

        list->addItem(widgetItem);
        list->setItemWidget(widgetItem, screenItem);
    }
    else if (event->type() == UPDATE_PARENT_EVENT)
    {
        this->m_stopAddingItem = false;
    }
    else
    {
        QDialog::customEvent(event);
    }
}

void ScreenExplorer::resizeEvent(QResizeEvent *event)
{
    if (event->oldSize().height() != event->size().height())
        this->updateScreen();
}

void ScreenExplorer::changeEvent(QEvent *event)
{
    switch (event->type())
    {
        case QEvent::LanguageChange:
        {
            this->ui->retranslateUi(this);
            event->accept();

            break;
        }
        default:
        {
            break;
        }
    }
}

void ScreenExplorer::on_ScreenExplorer_accepted()
{
    this->m_updater.stopUpdate();
}

void ScreenExplorer::on_ScreenExplorer_rejected()
{
    this->m_updater.stopUpdate();
}

void ScreenExplorer::on_step_timeChanged(const QTime &time)
{
    (void)time;

    this->updateScreen();
}
