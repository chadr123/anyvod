﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <QDialog>

class QSettings;

namespace Ui
{
    class FileAssociation;
}

class FileAssociation : public QDialog
{
    Q_OBJECT
public:
    explicit FileAssociation(QWidget *parent = nullptr);
    ~FileAssociation();

private:
    virtual void changeEvent(QEvent *event);

private slots:
    void on_videoDefault_clicked();
    void on_audioDefault_clicked();
    void on_subtitleDefault_clicked();
    void on_playlistDefault_clicked();
    void on_apply_clicked();

private:
    Ui::FileAssociation *ui;
    QStringList m_orgExts;
    QSettings &m_settings;
};
