﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "Popup.h"
#include "ui_popup.h"
#include "MainWindowInterface.h"
#include "core/Common.h"
#include "utils/FloatPointUtils.h"
#include "factories/MainWindowFactory.h"

#include <QDesktopServices>
#include <QUrl>
#include <QDebug>

const int Popup::SHOW_TIME = 40;
const int Popup::SHOWING_TIME = 4000;
const int Popup::HIDE_TIME = 40;

Popup::Popup(QWidget *parent) :
    QDialog(parent, Qt::Tool),
    ui(new Ui::Popup),
    m_showTimer(0),
    m_showingTimer(0),
    m_hideTimer(0)
{
    ui->setupUi(this);
}

Popup::~Popup()
{
    delete ui;
}

void Popup::killAllTimers()
{
    if (this->m_showTimer > 0)
    {
        this->killTimer(this->m_showTimer);
        this->m_showTimer = 0;
    }

    if (this->m_showingTimer > 0)
    {
        this->killTimer(this->m_showingTimer);
        this->m_showingTimer = 0;
    }

    if (this->m_hideTimer > 0)
    {
        this->killTimer(this->m_hideTimer);
        this->m_hideTimer = 0;
    }
}

void Popup::showText(const QString &text, const QString &url)
{
    QString newLined = text;
    QUrl encode(url, QUrl::StrictMode);
    QString encoded = encode.toEncoded();

    newLined.replace(NEW_LINE, "<br>");
    this->ui->text->setText(QString("<a href='%1'>%2</a>").arg(encoded, newLined));

    this->killAllTimers();

    this->setWindowOpacity(0.0);
    this->m_showTimer = this->startTimer(SHOW_TIME);

    this->show();

    MainWindowFactory::getInstance()->movePopup();
}

void Popup::timerEvent(QTimerEvent *event)
{
    if (event->timerId() == this->m_showTimer)
    {
        double opaque = this->windowOpacity();

        if (opaque >= 1.0)
        {
            this->killTimer(this->m_showTimer);
            this->m_showTimer = 0;

            this->m_showingTimer = this->startTimer(SHOWING_TIME);
        }
        else
        {
            this->setWindowOpacity(opaque + 0.06);
        }
    }
    else if (event->timerId() == this->m_showingTimer)
    {
        this->killTimer(this->m_showingTimer);
        this->m_showingTimer = 0;

        this->m_hideTimer = this->startTimer(HIDE_TIME);
    }
    else if (event->timerId() == this->m_hideTimer)
    {
        double opaque = this->windowOpacity();

        if (FloatPointUtils::zeroDouble(opaque) <= 0.0)
        {
            this->killTimer(this->m_hideTimer);
            this->m_hideTimer = 0;

            this->hide();
        }
        else
        {
            this->setWindowOpacity(opaque - 0.06);
        }
    }
}

void Popup::hideEvent(QHideEvent *)
{
    this->killAllTimers();
}

void Popup::changeEvent(QEvent *event)
{
    switch (event->type())
    {
        case QEvent::LanguageChange:
        {
            this->ui->retranslateUi(this);
            event->accept();

            break;
        }
        default:
        {
            break;
        }
    }
}

void Popup::on_text_linkActivated(const QString &link)
{
    QDesktopServices::openUrl(QUrl(link));
}
