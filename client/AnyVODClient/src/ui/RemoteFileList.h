﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "../../../../common/types.h"

#include <QDialog>
#include <QShortcut>
#include <QStack>

namespace Ui
{
    class RemoteFileList;
}

struct PlayItem;
class QTreeWidgetItem;

class RemoteFileList : public QDialog
{
    Q_OBJECT
public:
    explicit RemoteFileList(QWidget *parent = nullptr);
    ~RemoteFileList();

    void setFileList(QList<ANYVOD_FILE_ITEM> &list, const QString &currentPath);
    void clearFileList();

private:
    virtual void showEvent(QShowEvent *);
    virtual void resizeEvent(QResizeEvent *);
    virtual void contextMenuEvent(QContextMenuEvent *);
    virtual void changeEvent(QEvent *event);

public slots:
    void addPlayList();

private slots:
    void on_filelist_itemActivated(QTreeWidgetItem *item, int);
    void on_refresh_clicked();

private:
    void initHeader();
    void adjustFileListHeaderSize();
    void getAnyVODUrl(const QString &parent, const QString &fileName, const QString &ext, QString *ret);
    void getFilePaths(const QString &parent, QVector<PlayItem> *ret);
    void loadFileList(const QString &path);
    int setFileListSub(QList<ANYVOD_FILE_ITEM> &list, FILE_ITEM_TYPE type);
    bool isRemotePlaying();
    bool containDirectory(const QList<QTreeWidgetItem*> &list);
    void disbleUpdates();
    void enableUpdates();

public:
    static const QString ROOT_DIR;

private:
    static const QSize DEFAULT_REMOTEFILELIST_SIZE;
    static const QString GOTO_PARENT_DIR;

private:
    struct Scroll
    {
        int value;
        int max;
        QString name;
    };

private:
    Ui::RemoteFileList *ui;
    QString m_currentPath;
    bool m_isInit;
    QShortcut m_addShort;
    QStack<Scroll> m_dirScroll;
};
