﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "TimeEdit.h"

TimeEdit::TimeEdit(QWidget *parent) :
    QTimeEdit(parent)
{
    this->setWrapping(true);
}

void TimeEdit::stepBy(int steps)
{
    QTime time = this->time();

    switch (this->currentSection())
    {
        case QTimeEdit::MSecSection:
        {
            if (time.hour() == 0 && time.minute() == 0 && time.second() == 0 && time.msec() == 0 && steps < 0)
                return;

            if (time.hour() == 23 && time.minute() == 59 && time.second() == 59 && time.msec() == 999 && steps > 0)
                return;

            time = time.addMSecs(steps);
            break;
        }
        case QTimeEdit::SecondSection:
        {
            if (time.hour() == 0 && time.minute() == 0 && time.second() == 0 && steps < 0)
                return;

            if (time.hour() == 23 && time.minute() == 59 && time.second() == 59 && steps > 0)
                return;

            time = time.addSecs(steps);
            break;
        }
        case QTimeEdit::MinuteSection:
        {
            if (time.hour() == 0 && time.minute() == 0 && steps < 0)
                return;

            if (time.hour() == 23 && time.minute() == 59 && steps > 0)
                return;

            time = time.addSecs(60 * steps);
            break;
        }
        case QTimeEdit::HourSection:
        {
            if (time.hour() == 0 && steps < 0)
                return;

            if (time.hour() == 23 && steps > 0)
                return;

            time = time.addSecs(60 * 60 * steps);
            break;
        }
        default:
        {
            QTimeEdit::stepBy(steps);
            return;
        }
    }

    this->setTime(time);
}
