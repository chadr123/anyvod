﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "Equalizer.h"
#include "ui_equalizer.h"
#include "media/MediaPlayer.h"
#include "core/Common.h"
#include "utils/MessageBoxUtils.h"
#include "setting/Settings.h"
#include "setting/SettingsFactory.h"

#include <QMenu>
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QTextStream>
#include <QInputDialog>

const QString Equalizer::EQUALIZER_ITEM_DELIMITER = "/";

QDataStream& operator << (QDataStream &out, const Equalizer::EqualizerItem &item)
{
    out << item.gain;

    return out;
}

QDataStream& operator >> (QDataStream &in, Equalizer::EqualizerItem &item)
{
    in >> item.gain;

    return in;
}

Equalizer::Equalizer(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Equalizer),
    m_player(MediaPlayer::getInstance()),
    m_preAmp(0.0f),
    m_prevUseEqualizer(false)
{
    ui->setupUi(this);

#ifdef Q_OS_MAC
    const auto btns = this->findChildren<QPushButton*>();

    for (QPushButton *btn : btns)
        btn->setAttribute(Qt::WA_LayoutUsesWidgetRect);
#endif

    Ui::Equalizer *ctrl = this->ui;

    this->m_preAmp = this->m_player.getPreAmp();
    ctrl->preamp->setValue(this->dBToValue(this->m_preAmp));
    ctrl->preamp_value->setText(QString().asprintf("%.1f", this->m_preAmp));

    for (int i = 0; i < this->m_player.getBandCount(); i++)
    {
        EqualizerItem gain;

        gain.gain = this->m_player.getEqualizerGain(i);

        this->m_gains.append(gain);

        QSlider *slider = (QSlider*)this->findChild<QSlider*>(QString("band%1").arg(i));
        QLabel *label = (QLabel*)this->findChild<QLabel*>(QString("value%1").arg(i));

        slider->setValue(this->dBToValue(gain.gain));
        label->setText(QString().asprintf("%.1f", gain.gain));
    }

    ctrl->useEqualizer->setChecked(this->m_player.isUsingEqualizer() && !this->m_player.isUseSPDIF());
    ctrl->useEqualizer->setEnabled(!this->m_player.isUseSPDIF());

    this->m_prevUseEqualizer = ctrl->useEqualizer->isChecked();

    this->initDefaultNames();
    this->initPreset();
}

Equalizer::~Equalizer()
{
    delete ui;
}

void Equalizer::initDefaultNames() const
{
    tr("기본값");
    tr("클래식");
    tr("베이스");
    tr("베이스 & 트레블");
    tr("트레블");
    tr("헤드폰");
    tr("홀");
    tr("소프트 락");
    tr("클럽");
    tr("댄스");
    tr("라이브");
    tr("파티");
    tr("팝");
    tr("레게");
    tr("락");
    tr("스카");
    tr("소프트");
    tr("테크노");
    tr("보컬");
    tr("재즈");
}

float Equalizer::valueTodB(int value)
{
    return (float)value / 10.0f;
}

int Equalizer::dBToValue(float dB)
{
    return (int)ceil(dB * 10.0f);
}

void Equalizer::canceled()
{
    this->m_player.setPreAmp(this->m_preAmp);

    for (int i = 0; i < this->m_gains.count(); i++)
        this->m_player.setEqualizerGain(i, this->m_gains[i].gain);

    if (this->ui->useEqualizer->isChecked() != this->m_prevUseEqualizer)
        this->m_player.useEqualizer(this->m_prevUseEqualizer);
}

void Equalizer::changeEvent(QEvent *event)
{
    switch (event->type())
    {
        case QEvent::LanguageChange:
        {
            this->ui->retranslateUi(this);
            event->accept();

            break;
        }
        default:
        {
            break;
        }
    }
}

void Equalizer::on_reset_clicked()
{
    Ui::Equalizer *ctrl = this->ui;

    ctrl->preamp->setValue(0);
    ctrl->preamp_value->setText(QString().asprintf("%.1f", 0.0f));

    this->on_preamp_valueChanged(0);

    for (int i = 0; i < this->m_player.getBandCount(); i++)
    {
        QSlider *slider = (QSlider*)this->findChild<QSlider*>(QString("band%1").arg(i));
        QLabel *label = (QLabel*)this->findChild<QLabel*>(QString("value%1").arg(i));

        slider->setValue(0);
        label->setText(QString().asprintf("%.1f", 0.0f));
    }

    this->on_band0_valueChanged(0);
    this->on_band1_valueChanged(0);
    this->on_band2_valueChanged(0);
    this->on_band3_valueChanged(0);
    this->on_band4_valueChanged(0);
    this->on_band5_valueChanged(0);
    this->on_band6_valueChanged(0);
    this->on_band7_valueChanged(0);
    this->on_band8_valueChanged(0);
    this->on_band9_valueChanged(0);
}

void Equalizer::on_preamp_valueChanged(int value)
{
    this->ui->preamp_value->setText(QString().asprintf("%.1f", this->valueTodB(value)));
    this->m_player.setPreAmp(this->valueTodB(value));
}

void Equalizer::on_band0_valueChanged(int value)
{
    this->ui->value0->setText(QString().asprintf("%.1f", this->valueTodB(value)));
    this->m_player.setEqualizerGain(0, this->valueTodB(value));
}

void Equalizer::on_band1_valueChanged(int value)
{
    this->ui->value1->setText(QString().asprintf("%.1f", this->valueTodB(value)));
    this->m_player.setEqualizerGain(1, this->valueTodB(value));
}

void Equalizer::on_band2_valueChanged(int value)
{
    this->ui->value2->setText(QString().asprintf("%.1f", this->valueTodB(value)));
    this->m_player.setEqualizerGain(2, this->valueTodB(value));
}

void Equalizer::on_band3_valueChanged(int value)
{
    this->ui->value3->setText(QString().asprintf("%.1f", this->valueTodB(value)));
    this->m_player.setEqualizerGain(3, this->valueTodB(value));
}

void Equalizer::on_band4_valueChanged(int value)
{
    this->ui->value4->setText(QString().asprintf("%.1f", this->valueTodB(value)));
    this->m_player.setEqualizerGain(4, this->valueTodB(value));
}

void Equalizer::on_band5_valueChanged(int value)
{
    this->ui->value5->setText(QString().asprintf("%.1f", this->valueTodB(value)));
    this->m_player.setEqualizerGain(5, this->valueTodB(value));
}

void Equalizer::on_band6_valueChanged(int value)
{
    this->ui->value6->setText(QString().asprintf("%.1f", this->valueTodB(value)));
    this->m_player.setEqualizerGain(6, this->valueTodB(value));
}

void Equalizer::on_band7_valueChanged(int value)
{
    this->ui->value7->setText(QString().asprintf("%.1f", this->valueTodB(value)));
    this->m_player.setEqualizerGain(7, this->valueTodB(value));
}

void Equalizer::on_band8_valueChanged(int value)
{
    this->ui->value8->setText(QString().asprintf("%.1f", this->valueTodB(value)));
    this->m_player.setEqualizerGain(8, this->valueTodB(value));
}

void Equalizer::on_band9_valueChanged(int value)
{
    this->ui->value9->setText(QString().asprintf("%.1f", this->valueTodB(value)));
    this->m_player.setEqualizerGain(9, this->valueTodB(value));
}

void Equalizer::on_preset_clicked()
{
    this->presetMenu((void (Equalizer::*)(int))&Equalizer::selectPreset);
}

void Equalizer::on_del_preset_clicked()
{
    this->presetMenu((void (Equalizer::*)(int))&Equalizer::deletePreset);
}

void Equalizer::on_add_preset_clicked()
{
    bool ok;
    QString name = QInputDialog::getText(this, tr("프리셋 추가"), tr("프리셋 이름 :"), QLineEdit::Normal, QString(), &ok);

    if (ok && !name.isEmpty())
    {
        Preset preset;
        EqualizerItem gain;

        preset.desc = name;

        for (int i = 0; i < this->m_player.getBandCount(); i++)
        {
            QSlider *slider = (QSlider*)this->findChild<QSlider*>(QString("band%1").arg(i));

            gain.gain = this->valueTodB(slider->value());
            preset.gains.append(gain);
        }

        this->m_presets.append(preset);
    }
}

void Equalizer::on_save_preset_clicked()
{
    this->presetMenu(&Equalizer::savePreset);
}

void Equalizer::on_useEqualizer_clicked(bool checked)
{
    this->m_player.useEqualizer(checked);
}

void Equalizer::savePresetToFile()
{
    QString settingPath = SettingsFactory::getSettingPath();
    QDir d(settingPath);

    if (!d.exists())
    {
        d.mkpath("tmp");
        d.rmdir("tmp");
    }

    QFile file(settingPath + SETTING_EQUALIZER_FILE_NAME);

    if (file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream stream(&file);

        stream.setCodec("UTF-8");
        stream.setGenerateByteOrderMark(true);

        for (int i = 0; i < this->m_presets.count(); i++)
        {
            Preset &preset = this->m_presets[i];

            stream << preset.desc;

            for (int i = 0; i < preset.gains.count(); i++)
                stream << EQUALIZER_ITEM_DELIMITER << preset.gains[i].gain;

            stream << Qt::endl;
        }
    }
}

void Equalizer::presetMenu(void (Equalizer::*slot)(int))
{
    QMenu menu;
    int count = this->m_presets.count();
    bool check = (void (Equalizer::*)(int))&Equalizer::selectPreset == slot;

    if (count > 0)
    {
        for (int i = 0; i < count; i++)
        {
            Preset &preset = this->m_presets[i];
            QString name = preset.desc;
            QAction *subAction = menu.addAction(name.replace("&", "&&"));

            if (check)
            {
                bool same = true;

                for (int j = 0; j < preset.gains.count() && j < this->m_player.getBandCount(); j++)
                {
                    QSlider *slider = (QSlider*)this->findChild<QSlider*>(QString("band%1").arg(j));
                    float gain = this->valueTodB(slider->value());

                    same &= preset.gains[j].gain == gain;
                }

                subAction->setCheckable(true);
                subAction->setChecked(same);
            }

            connect(subAction, &QAction::triggered, [this, slot, i]() { (this->*slot)(i); });
        }
    }
    else
    {
        menu.addAction(tr("프리셋이 없습니다"));
    }

    menu.exec(QCursor::pos());
}

void Equalizer::initPreset()
{
    this->parsePreset(&this->m_presets);
}

void Equalizer::parsePreset(QVector<Preset> *ret)
{
    QString filePath;

    if (QFileInfo::exists(SettingsFactory::getSettingPath() + SETTING_EQUALIZER_FILE_NAME))
        filePath = SettingsFactory::getSettingPath() + SETTING_EQUALIZER_FILE_NAME;
    else
        filePath = SETTING_EQUALIZER_FILE_NAME;

    QFile file(filePath);
    int maxBand = this->m_player.getBandCount();

    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream stream(&file);

        while (!stream.atEnd())
        {
            QString line = stream.readLine();
            Preset preset;
            EqualizerItem gain;

            line = line.trimmed();

            if (line.length() == 0)
                continue;

            if (line.startsWith(COMMENT_PREFIX))
                continue;

            QStringList pair = line.split(EQUALIZER_ITEM_DELIMITER);

            if (pair.length() < maxBand + 1)
                continue;

            preset.desc = tr(pair[0].toUtf8());

            for (int i = 0; i < maxBand; i++)
            {
                gain.gain = pair[i+1].toFloat();
                preset.gains.append(gain);
            }

            ret->append(preset);
        }
    }
}

void Equalizer::selectPreset(int index)
{
    if (index < 0 || index >= this->m_presets.count())
        return;

    Preset &preset = this->m_presets[index];

    for (int i = 0; i < this->m_player.getBandCount(); i++)
    {
        QSlider *slider = (QSlider*)this->findChild<QSlider*>(QString("band%1").arg(i));
        QLabel *label = (QLabel*)this->findChild<QLabel*>(QString("value%1").arg(i));
        float gain = preset.gains[i].gain;

        slider->setValue(this->dBToValue(gain));
        label->setText(QString().asprintf("%.1f", gain));
        this->m_player.setEqualizerGain(i, gain);
    }
}

void Equalizer::deletePreset(int index)
{
    if (index < 0 || index >= this->m_presets.count())
        return;

    if (MessageBoxUtils::questionMessageBox(this, tr("삭제 하시겠습니까?")))
        this->m_presets.remove(index);
}

void Equalizer::savePreset(int index)
{
    if (index < 0 || index >= this->m_presets.count())
        return;

    Preset &preset = this->m_presets[index];

    for (int i = 0; i < this->m_player.getBandCount(); i++)
    {
        QSlider *slider = (QSlider*)this->findChild<QSlider*>(QString("band%1").arg(i));

        preset.gains[i].gain = this->valueTodB(slider->value());
    }
}
