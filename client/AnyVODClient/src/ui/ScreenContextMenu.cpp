﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "ScreenContextMenu.h"
#include "ShortcutKey.h"
#include "MainWindowInterface.h"
#include "Screen.h"
#include "UserAspectRatios.h"
#include "core/EnumsTranslator.h"
#include "core/FileExtensions.h"
#include "media/MediaPlayer.h"
#include "media/AudioRenderer.h"
#include "net/Updater.h"
#include "factories/MainWindowFactory.h"
#include "ui/RemoteFileList.h"
#include "ui/PlayList.h"
#include "device/DTVReader.h"
#include "device/RadioReader.h"
#include "audio/SPDIF.h"
#include "audio/SPDIFSampleRates.h"
#include "video/ShaderCompositer.h"
#include "video/ScreenCaptureHelper.h"
#include "delegate/ScreenDelegate.h"
#include "delegate/UpdaterDelegate.h"
#include "delegate/MediaPlayerDelegate.h"
#include "delegate/ShaderCompositerDelegate.h"
#include "utils/DeviceUtils.h"
#include "language/GlobalLanguage.h"
#include "language/LanguageInstaller.h"

#include <QDir>
#include <QDebug>

ScreenContextMenu::ScreenContextMenu(Screen *screen) :
    m_mainWindow(MainWindowFactory::getInstance()),
    m_screen(screen),
    m_player(MediaPlayer::getInstance()),
    m_shortcutKey(ShortcutKey::getInstance()),
    m_enums(EnumsTranslator::getInstance()),
    m_shader(ShaderCompositer::getInstance()),
    m_screenDelegate(ScreenDelegate::getInstance()),
    m_updaterDelegate(UpdaterDelegate::getInstance()),
    m_playerDelegate(MediaPlayerDelegate::getInstance()),
    m_shaderDelegate(ShaderCompositerDelegate::getInstance())
{

}

void ScreenContextMenu::show()
{
    this->m_menu.clear();
    this->createContextMenu();
    this->m_menu.exec(QCursor::pos());
}

void ScreenContextMenu::createScreenRotationDegreeMenu(QMenu *menu)
{
    QMenu *subMenu = nullptr;
    QAction *subAction = nullptr;
    AnyVODEnums::ScreenRotationDegree degree = this->m_player.getScreenRotationDegree();

    subMenu = menu->addMenu(tr("화면 회전 각도"));

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_SCREEN_ROTATION_DEGREE_ORDER));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_SCREEN_ROTATION_DEGREE_ORDER));
    subAction->setShortcutVisibleInContextMenu(true);
    connect(subAction, &QAction::triggered, &this->m_playerDelegate, &MediaPlayerDelegate::screenRotationDegreeOrder);

    const QStringList names({tr("사용 안 함"), tr("90도"), tr("180도"), tr("270도")});

    for (int i = AnyVODEnums::SRD_NONE; i < AnyVODEnums::SRD_COUNT; i++)
    {
        const QString &name = names[i];
        AnyVODEnums::ScreenRotationDegree value = (AnyVODEnums::ScreenRotationDegree)i;

        subAction = subMenu->addAction(name);
        subAction->setCheckable(true);
        subAction->setChecked(degree == value);
        connect(subAction, &QAction::triggered, [this, value]() { this->m_playerDelegate.selectScreenRotationDegree(value); });
    }
}

void ScreenContextMenu::createVideoMenu(QMenu *menu)
{
    QMenu *subMenu = nullptr;
    QAction *subAction = nullptr;

    subMenu = menu->addMenu(tr("영상"));

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_VIDEO_ORDER));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_VIDEO_ORDER));
    subAction->setShortcutVisibleInContextMenu(true);
    connect(subAction, &QAction::triggered, &this->m_playerDelegate, &MediaPlayerDelegate::videoOrder);

    QVector<VideoStreamInfo> videoInfo;
    int curVideoIndex = this->m_player.getCurrentVideoStreamIndex();

    this->m_player.getVideoStreamInfo(&videoInfo);

    if (videoInfo.count() > 0)
    {
        for (int i = 0; i < videoInfo.count(); i++)
        {
            VideoStreamInfo &info = videoInfo[i];

            subAction = subMenu->addAction(videoInfo[i].name);
            subAction->setCheckable(true);
            subAction->setChecked(curVideoIndex == info.index);
            connect(subAction, &QAction::triggered, [this, info]() { this->m_playerDelegate.selectVideoStream(info.index); });
        }
    }
    else
    {
       subMenu->setDisabled(true);
    }
}

void ScreenContextMenu::create3DMenu(QMenu *menu)
{
    QMenu *subMenu = nullptr;
    QAction *subAction = nullptr;
    AnyVODEnums::Video3DMethod method = this->m_player.get3DMethod();

    subMenu = menu->addMenu(tr("3D 영상 설정"));

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_USE_3D_FULL));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_USE_3D_FULL));
    subAction->setShortcutVisibleInContextMenu(true);
    subAction->setEnabled(method != AnyVODEnums::V3M_NONE);
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player.isUse3DFull());
    connect(subAction, &QAction::triggered, &this->m_playerDelegate, &MediaPlayerDelegate::use3DFull);

    subMenu->addSeparator();

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_3D_VIDEO_METHOD_ORDER));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_3D_VIDEO_METHOD_ORDER));
    subAction->setShortcutVisibleInContextMenu(true);
    connect(subAction, &QAction::triggered, &this->m_playerDelegate, &MediaPlayerDelegate::method3DOrder);

    const QVector<AnyVODEnums::Video3DMethod> methodList =
    {
        AnyVODEnums::V3M_NONE,
        AnyVODEnums::V3M_SEPARATOR,
        AnyVODEnums::V3M_HALF_LEFT, AnyVODEnums::V3M_HALF_RIGHT,
        AnyVODEnums::V3M_HALF_TOP, AnyVODEnums::V3M_HALF_BOTTOM,
        AnyVODEnums::V3M_SEPARATOR,
        AnyVODEnums::V3M_FULL_LEFT_RIGHT, AnyVODEnums::V3M_FULL_TOP_BOTTOM
    };

    this->addActionsFor3DVideo(subMenu, methodList);

    subMenu->addSeparator();

    this->create3DPageFlipMenu(subMenu);
    this->create3DInterlaceMenu(subMenu);
    this->create3DCheckerBoardMenu(subMenu);
    this->create3DAnaglyphMenu(subMenu);
}

void ScreenContextMenu::create3DCheckerBoardMenu(QMenu *menu)
{
    QMenu *subMenu = menu->addMenu(tr("체커 보드"));

    const QVector<AnyVODEnums::Video3DMethod> methodList =
    {
        AnyVODEnums::V3M_CHECKER_BOARD_LEFT_RIGHT_LEFT_PRIOR, AnyVODEnums::V3M_CHECKER_BOARD_LEFT_RIGHT_RIGHT_PRIOR,
        AnyVODEnums::V3M_CHECKER_BOARD_TOP_BOTTOM_TOP_PRIOR, AnyVODEnums::V3M_CHECKER_BOARD_TOP_BOTTOM_BOTTOM_PRIOR
    };

    this->addActionsFor3DVideo(subMenu, methodList);
}

void ScreenContextMenu::create3DPageFlipMenu(QMenu *menu)
{
    QMenu *subMenu = menu->addMenu(tr("페이지 플리핑"));

    const QVector<AnyVODEnums::Video3DMethod> methodList =
    {
        AnyVODEnums::V3M_PAGE_FLIP_LEFT_RIGHT_LEFT_PRIOR, AnyVODEnums::V3M_PAGE_FLIP_LEFT_RIGHT_RIGHT_PRIOR,
        AnyVODEnums::V3M_PAGE_FLIP_TOP_BOTTOM_TOP_PRIOR, AnyVODEnums::V3M_PAGE_FLIP_TOP_BOTTOM_BOTTOM_PRIOR
    };

    this->addActionsFor3DVideo(subMenu, methodList);
}

void ScreenContextMenu::create3DInterlaceMenu(QMenu *menu)
{
    QMenu *subMenu = menu->addMenu(tr("인터레이스"));

    const QVector<AnyVODEnums::Video3DMethod> methodList =
    {
        AnyVODEnums::V3M_ROW_LEFT_RIGHT_LEFT_PRIOR, AnyVODEnums::V3M_ROW_LEFT_RIGHT_RIGHT_PRIOR,
        AnyVODEnums::V3M_ROW_TOP_BOTTOM_TOP_PRIOR, AnyVODEnums::V3M_ROW_TOP_BOTTOM_BOTTOM_PRIOR,
        AnyVODEnums::V3M_SEPARATOR,
        AnyVODEnums::V3M_COL_LEFT_RIGHT_LEFT_PRIOR, AnyVODEnums::V3M_COL_LEFT_RIGHT_RIGHT_PRIOR,
        AnyVODEnums::V3M_COL_TOP_BOTTOM_TOP_PRIOR, AnyVODEnums::V3M_COL_TOP_BOTTOM_BOTTOM_PRIOR
    };

    this->addActionsFor3DVideo(subMenu, methodList);
}

void ScreenContextMenu::create3DAnaglyphMenu(QMenu *menu)
{
    QMenu *subMenu = nullptr;
    QAction *subAction = nullptr;
    AnyVODEnums::AnaglyphAlgorithm anaAlgorithm = this->m_shader.getAnaglyphAlgorithm();

    subMenu = menu->addMenu(tr("애너글리프"));

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_ANAGLYPH_ALGORITHM_ORDER));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_ANAGLYPH_ALGORITHM_ORDER));
    subAction->setShortcutVisibleInContextMenu(true);
    connect(subAction, &QAction::triggered, &this->m_shaderDelegate, &ShaderCompositerDelegate::anaglyphAlgorithmOrder);

    for (int i = AnyVODEnums::AGA_DEFAULT; i < AnyVODEnums::AGA_COUNT; i++)
    {
        AnyVODEnums::AnaglyphAlgorithm value = (AnyVODEnums::AnaglyphAlgorithm)i;

        subAction = subMenu->addAction(this->m_enums.getAnaglyphAlgoritmDesc(value));
        subAction->setEnabled(this->m_shader.canUseAnaglyphAlgorithm());
        subAction->setCheckable(true);
        subAction->setChecked(anaAlgorithm == value);
        connect(subAction, &QAction::triggered, [this, value]() { this->m_shaderDelegate.selectAnaglyphAlgorithm(value); });
    }

    subMenu->addSeparator();

    const QVector<AnyVODEnums::Video3DMethod> methodList =
    {
        AnyVODEnums::V3M_RED_CYAN_LEFT_RIGHT_LEFT_PRIOR, AnyVODEnums::V3M_RED_CYAN_LEFT_RIGHT_RIGHT_PRIOR,
        AnyVODEnums::V3M_RED_CYAN_TOP_BOTTOM_TOP_PRIOR, AnyVODEnums::V3M_RED_CYAN_TOP_BOTTOM_BOTTOM_PRIOR,
        AnyVODEnums::V3M_SEPARATOR,
        AnyVODEnums::V3M_GREEN_MAGENTA_LEFT_RIGHT_LEFT_PRIOR, AnyVODEnums::V3M_GREEN_MAGENTA_LEFT_RIGHT_RIGHT_PRIOR,
        AnyVODEnums::V3M_GREEN_MAGENTA_TOP_BOTTOM_TOP_PRIOR, AnyVODEnums::V3M_GREEN_MAGENTA_TOP_BOTTOM_BOTTOM_PRIOR,
        AnyVODEnums::V3M_SEPARATOR,
        AnyVODEnums::V3M_YELLOW_BLUE_LEFT_RIGHT_LEFT_PRIOR, AnyVODEnums::V3M_YELLOW_BLUE_LEFT_RIGHT_RIGHT_PRIOR,
        AnyVODEnums::V3M_YELLOW_BLUE_TOP_BOTTOM_TOP_PRIOR, AnyVODEnums::V3M_YELLOW_BLUE_TOP_BOTTOM_BOTTOM_PRIOR,
        AnyVODEnums::V3M_SEPARATOR,
        AnyVODEnums::V3M_RED_BLUE_LEFT_RIGHT_LEFT_PRIOR, AnyVODEnums::V3M_RED_BLUE_LEFT_RIGHT_RIGHT_PRIOR,
        AnyVODEnums::V3M_RED_BLUE_TOP_BOTTOM_TOP_PRIOR, AnyVODEnums::V3M_RED_BLUE_TOP_BOTTOM_BOTTOM_PRIOR,
        AnyVODEnums::V3M_SEPARATOR,
        AnyVODEnums::V3M_RED_GREEN_LEFT_RIGHT_LEFT_PRIOR, AnyVODEnums::V3M_RED_GREEN_LEFT_RIGHT_RIGHT_PRIOR,
        AnyVODEnums::V3M_RED_GREEN_TOP_BOTTOM_TOP_PRIOR, AnyVODEnums::V3M_RED_GREEN_TOP_BOTTOM_BOTTOM_PRIOR
    };

    this->addActionsFor3DVideo(subMenu, methodList);
}

void ScreenContextMenu::createHWDecoderMenu(QMenu *menu)
{
    QMenu *subMenu = nullptr;
    QAction *subAction = nullptr;
    int index = this->m_player.getHWDecoderIndex();

    subMenu = menu->addMenu(tr("하드웨어 디코더"));

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_USE_HW_DECODER));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_USE_HW_DECODER));
    subAction->setShortcutVisibleInContextMenu(true);
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player.isUseHWDecoder());
    connect(subAction, &QAction::triggered, &this->m_playerDelegate, &MediaPlayerDelegate::useHWDecoder);

    subMenu->addSeparator();

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_HW_DECODER_ORDER));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_HW_DECODER_ORDER));
    subAction->setShortcutVisibleInContextMenu(true);
    connect(subAction, &QAction::triggered, &this->m_playerDelegate, &MediaPlayerDelegate::hwDecoderOrder);

    for (int i = 0; i < this->m_player.getHWDecoderCount(); i++)
    {
        QString name = this->m_player.getHWDecoderName(i);

        subAction = subMenu->addAction(name);
        subAction->setCheckable(true);
        subAction->setChecked(index == i);
        connect(subAction, &QAction::triggered, [this, i]() { this->m_playerDelegate.selectHWDecoder(i); });
    }
}

void ScreenContextMenu::createScreenRatioMenu(QMenu *menu)
{
    QMenu *subMenu = menu->addMenu(tr("화면 크기"));

    const auto slotList =
    {
        qMakePair(AnyVODEnums::SK_SCREEN_SIZE_HALF, &MainWindowInterface::screenSizeHalf),
        qMakePair(AnyVODEnums::SK_SCREEN_SIZE_NORMAL, &MainWindowInterface::screenSizeNormal),
        qMakePair(AnyVODEnums::SK_SCREEN_SIZE_NORMAL_HALF, &MainWindowInterface::screenSizeNormalHalf),
        qMakePair(AnyVODEnums::SK_SCREEN_SIZE_DOUBLE, &MainWindowInterface::screenSizeDouble),
    };

    for (const auto &slot : slotList)
    {
        QAction *subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(slot.first));

        subAction->setShortcut(this->m_shortcutKey.getShortcutKey(slot.first));
        subAction->setShortcutVisibleInContextMenu(true);
        subAction->setEnabled(this->m_player.isEnabledVideo());
        connect(subAction, &QAction::triggered, this->m_mainWindow, slot.second);
    }
}

void ScreenContextMenu::createUserAspectRatioMenu(QMenu *menu)
{
    QMenu *subMenu = nullptr;
    QAction *subAction = nullptr;
    UserAspectRatios ratios = this->m_mainWindow->getUserAspectRatios();

    subMenu = menu->addMenu(tr("화면 비율"));

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_USER_ASPECT_RATIO_ORDER));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_USER_ASPECT_RATIO_ORDER));
    subAction->setShortcutVisibleInContextMenu(true);
    connect(subAction, &QAction::triggered, this->m_mainWindow, &MainWindowInterface::userAspectRatioOrder);

    for (int i = 0; i < ratios.getCount(); i++)
    {
        UserAspectRatios::Item item = ratios.getRatio(i);
        QString title;

        if (item.isInvalid())
            title = tr("사용 안 함");
        else if (item.isFullScreen())
            title = tr("화면 채우기");
        else if (ratios.isUserRatio(i))
            title = tr("사용자 지정 (%1:%2)").arg(item.width).arg(item.height);
        else
            title = QString("%1:%2").arg(item.width).arg(item.height);

        subAction = subMenu->addAction(title);
        subAction->setCheckable(true);
        subAction->setChecked(ratios.isSelectedRatio(i));
        connect(subAction, &QAction::triggered, [this, i]() { this->m_mainWindow->selectUserAspectRatio(i); });
    }

    subMenu->addSeparator();

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_USER_ASPECT_RATIO));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_USER_ASPECT_RATIO));
    subAction->setShortcutVisibleInContextMenu(true);
    connect(subAction, &QAction::triggered, this->m_mainWindow, &MainWindowInterface::openUserAspectRatio);
}

void ScreenContextMenu::createDeinterlaceMenu(QMenu *menu)
{
    QMenu *subMenu = nullptr;
    QAction *subAction = nullptr;
    AnyVODEnums::DeinterlaceMethod deMethod = this->m_player.getDeinterlaceMethod();

    subMenu = menu->addMenu(tr("디인터레이스"));

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_DEINTERLACE_METHOD_ORDER));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_DEINTERLACE_METHOD_ORDER));
    subAction->setShortcutVisibleInContextMenu(true);
    connect(subAction, &QAction::triggered, &this->m_playerDelegate, &MediaPlayerDelegate::deinterlaceMethodOrder);

    const QStringList names({tr("자동 판단"), tr("항상 사용"), tr("사용 안 함")});

    for (int i = AnyVODEnums::DM_AUTO; i < AnyVODEnums::DM_COUNT; i++)
    {
        const QString &name = names[i];
        AnyVODEnums::DeinterlaceMethod value = (AnyVODEnums::DeinterlaceMethod)i;

        subAction = subMenu->addAction(name);
        subAction->setCheckable(true);
        subAction->setChecked(deMethod == value);
        connect(subAction, &QAction::triggered, [this, value]() { this->m_playerDelegate.selectDeinterlacerMethod(value); });
    }

    AnyVODEnums::DeinterlaceAlgorithm deAlgorithm = this->m_player.getDeinterlaceAlgorithm();

    subMenu = subMenu->addMenu(tr("알고리즘"));

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_USE_IVTC));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_USE_IVTC));
    subAction->setCheckable(true);
    subAction->setEnabled(this->m_player.isDeinterlaceAVFilter());
    subAction->setChecked(this->m_player.isUseDeinterlaceIVTC());
    connect(subAction, &QAction::triggered, &this->m_playerDelegate, &MediaPlayerDelegate::useIVTC);

    subMenu->addSeparator();

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_DEINTERLACE_ALGORITHM_ORDER));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_DEINTERLACE_ALGORITHM_ORDER));
    subAction->setShortcutVisibleInContextMenu(true);
    connect(subAction, &QAction::triggered, &this->m_playerDelegate, &MediaPlayerDelegate::deinterlaceAlgorithmOrder);

    for (int i = AnyVODEnums::DA_BLEND; i < AnyVODEnums::DA_COUNT; i++)
    {
        AnyVODEnums::DeinterlaceAlgorithm value = (AnyVODEnums::DeinterlaceAlgorithm)i;

        subAction = subMenu->addAction(this->m_enums.getDeinterlaceDesc(value));
        subAction->setCheckable(true);
        subAction->setChecked(deAlgorithm == value);
        connect(subAction, &QAction::triggered, [this, value]() { this->m_playerDelegate.selectDeinterlacerAlgorithem(value); });
    }
}

void ScreenContextMenu::createCaptureMenu(QMenu *menu)
{
    QMenu *subMenu = nullptr;
    QAction *subAction = nullptr;
    ScreenCaptureHelper &capHelper = this->m_screen->getCaptureHelper();
    QString curExt = capHelper.getExtention().toUpper();

    subMenu = menu->addMenu(tr("캡쳐"));

    subMenu->addSeparator();

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_SELECT_CAPTURE_EXT_ORDER));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_SELECT_CAPTURE_EXT_ORDER));
    subAction->setShortcutVisibleInContextMenu(true);
    connect(subAction, &QAction::triggered, &this->m_screenDelegate, &ScreenDelegate::captureExtOrder);

    for (int i = 0; i < FileExtensions::CAPTURE_FORMAT_LIST.count(); i++)
    {
        QString ext = FileExtensions::CAPTURE_FORMAT_LIST.at(i).toUpper();

        subAction = subMenu->addAction(ext);
        subAction->setCheckable(true);
        subAction->setChecked(curExt == ext);
        connect(subAction, &QAction::triggered, [this, ext]() { this->m_screenDelegate.selectCaptureExt(ext); });
    }

    subMenu->addSeparator();

    const auto captureSlots =
    {
        qMakePair(AnyVODEnums::SK_CAPTURE_SINGLE, &ScreenDelegate::captureSingle),
        qMakePair(AnyVODEnums::SK_CAPTURE_MULTIPLE, &ScreenDelegate::captureMultiple),
    };

    for (const auto &slot : captureSlots)
    {
        subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(slot.first));
        subAction->setShortcut(this->m_shortcutKey.getShortcutKey(slot.first));
        subAction->setShortcutVisibleInContextMenu(true);
        subAction->setEnabled(this->m_player.canCapture());
        connect(subAction, &QAction::triggered, &this->m_screenDelegate, slot.second);
    }

    subMenu->addSeparator();

    const auto captureDirSlots =
    {
        qMakePair(AnyVODEnums::SK_SELECT_CAPTURE_DIRECTORY, &ScreenDelegate::selectCaptureSaveDir),
        qMakePair(AnyVODEnums::SK_OPEN_CAPTURE_DIRECTORY, &ScreenDelegate::openCaptureSaveDir),
    };

    for (const auto &slot : captureDirSlots)
    {
        subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(slot.first));
        subAction->setShortcut(this->m_shortcutKey.getShortcutKey(slot.first));
        subAction->setShortcutVisibleInContextMenu(true);
        connect(subAction, &QAction::triggered, &this->m_screenDelegate, slot.second);
    }
}

void ScreenContextMenu::createVideoAttributeMenu(QMenu *menu)
{
    QMenu *subMenu = menu->addMenu(tr("영상 속성"));

    const auto slotList =
    {
        qMakePair(AnyVODEnums::SK_RESET_VIDEO_ATTRIBUTE, &ShaderCompositerDelegate::resetVideoAttribute),
        qMakePair(AnyVODEnums::SK_BRIGHTNESS_DOWN, &ShaderCompositerDelegate::brightnessDown),
        qMakePair(AnyVODEnums::SK_BRIGHTNESS_UP, &ShaderCompositerDelegate::brightnessUp),
        qMakePair(AnyVODEnums::SK_SATURATION_DOWN, &ShaderCompositerDelegate::saturationDown),
        qMakePair(AnyVODEnums::SK_SATURATION_UP, &ShaderCompositerDelegate::saturationUp),
        qMakePair(AnyVODEnums::SK_HUE_DOWN, &ShaderCompositerDelegate::hueDown),
        qMakePair(AnyVODEnums::SK_HUE_UP, &ShaderCompositerDelegate::hueUp),
        qMakePair(AnyVODEnums::SK_CONTRAST_DOWN, &ShaderCompositerDelegate::contrastDown),
        qMakePair(AnyVODEnums::SK_CONTRAST_UP, &ShaderCompositerDelegate::contrastUp),
    };

    for (const auto &slot : slotList)
    {
        QAction *subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(slot.first));

        subAction->setShortcut(this->m_shortcutKey.getShortcutKey(slot.first));
        subAction->setShortcutVisibleInContextMenu(true);
        subAction->setEnabled(this->m_player.isEnabledVideo());
        connect(subAction, &QAction::triggered, &this->m_shaderDelegate, slot.second);
    }
}

void ScreenContextMenu::createVideoEffectMenu(QMenu *menu)
{
    QMenu *subMenu = menu->addMenu(tr("영상 효과"));

    const auto shaderAttributes =
    {
        this->makeActionAttribute(AnyVODEnums::SK_SHARPLY, &ShaderCompositerDelegate::sharply, this->m_shader.isUsingSharply()),
        this->makeActionAttribute(AnyVODEnums::SK_SHARPEN, &ShaderCompositerDelegate::sharpen, this->m_shader.isUsingSharpen()),
        this->makeActionAttribute(AnyVODEnums::SK_SOFTEN, &ShaderCompositerDelegate::soften, this->m_shader.isUsingSoften()),
        this->makeActionAttribute(AnyVODEnums::SK_LEFT_RIGHT_INVERT, &ShaderCompositerDelegate::leftRightInvert, this->m_shader.isUsingLeftRightInvert()),
        this->makeActionAttribute(AnyVODEnums::SK_TOP_BOTTOM_INVERT, &ShaderCompositerDelegate::topBottomInvert, this->m_shader.isUsingTopBottomInvert())
    };

    for (const auto &attribute : shaderAttributes)
    {
        QAction *subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(attribute.slot.first));

        subAction->setEnabled(this->m_player.isEnabledVideo());
        subAction->setCheckable(true);
        subAction->setChecked(attribute.checked);
        subAction->setShortcut(this->m_shortcutKey.getShortcutKey(attribute.slot.first));
        subAction->setShortcutVisibleInContextMenu(true);
        connect(subAction, &QAction::triggered, &this->m_shaderDelegate, attribute.slot.second);
    }

    const FilterGraph &graph = this->m_player.getFilterGraph();
    const auto playerAttributes =
    {
        this->makeActionAttribute(AnyVODEnums::SK_HISTOGRAM_EQ, &MediaPlayerDelegate::histEQ, graph.getEnableHistEQ()),
        this->makeActionAttribute(AnyVODEnums::SK_DEBAND, &MediaPlayerDelegate::deband, graph.getEnableDeBand()),
        this->makeActionAttribute(AnyVODEnums::SK_DEBLOCK, &MediaPlayerDelegate::deblock, graph.getEnableDeBlock()),
        this->makeActionAttribute(AnyVODEnums::SK_VIDEO_NORMALIZE, &MediaPlayerDelegate::videoNormalize, graph.getEnableNormalize()),
        this->makeActionAttribute(AnyVODEnums::SK_HIGH_QUALITY_3D_DENOISE, &MediaPlayerDelegate::hq3DDenoise, graph.getEnableHighQuality3DDenoise()),
        this->makeActionAttribute(AnyVODEnums::SK_ATA_DENOISE, &MediaPlayerDelegate::ataDenoise, graph.getEnableATADenoise()),
        this->makeActionAttribute(AnyVODEnums::SK_OW_DENOISE, &MediaPlayerDelegate::owDenoise, graph.getEnableOWDenoise()),
        this->makeActionAttribute(AnyVODEnums::SK_NLMEANS_DENOISE, &MediaPlayerDelegate::nlMeansDenoise, graph.getEnableNLMeansDenoise()),
        this->makeActionAttribute(AnyVODEnums::SK_VAGUE_DENOISE, &MediaPlayerDelegate::vagueDenoise, graph.getEnableVagueDenoise()),
    };

    for (const auto &attribute : playerAttributes)
    {
        QAction *subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(attribute.slot.first));

        subAction->setEnabled(this->m_player.isEnabledVideo());
        subAction->setCheckable(true);
        subAction->setChecked(attribute.checked);
        subAction->setShortcut(this->m_shortcutKey.getShortcutKey(attribute.slot.first));
        subAction->setShortcutVisibleInContextMenu(true);
        connect(subAction, &QAction::triggered, &this->m_playerDelegate, attribute.slot.second);
    }
}

void ScreenContextMenu::createAdjustDistortionModeMenu(QMenu *menu)
{
    QMenu *subMenu = nullptr;
    QAction *subAction = nullptr;
    AnyVODEnums::DistortionAdjustMode curMode = this->m_player.getDistortionAdjustMode();

    subMenu = menu->addMenu(tr("왜곡 보정 모드"));

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_DISTORION_ADJUST_MODE_ORDER));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_DISTORION_ADJUST_MODE_ORDER));
    subAction->setShortcutVisibleInContextMenu(true);
    connect(subAction, &QAction::triggered, &this->m_playerDelegate, &MediaPlayerDelegate::distortionAdjustModeOrder);

    for (int i = AnyVODEnums::DAM_NONE; i < AnyVODEnums::DAM_COUNT; i++)
    {
        AnyVODEnums::DistortionAdjustMode value = (AnyVODEnums::DistortionAdjustMode)i;

        subAction = subMenu->addAction(this->m_enums.getDistortionAdjustModeDesc(value));
        subAction->setCheckable(true);
        subAction->setChecked(curMode == value);
        connect(subAction, &QAction::triggered, [this, value]() { this->m_playerDelegate.selectDistortionAdjustMode(value); });
    }
}

void ScreenContextMenu::create360DegreeMenu(QMenu *menu)
{
    QMenu *subMenu = nullptr;
    QAction *subAction = nullptr;

    subMenu = menu->addMenu(tr("360도 영상"));

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_USE_360_DEGREE));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_USE_360_DEGREE));
    subAction->setShortcutVisibleInContextMenu(true);
    subAction->setCheckable(true);
    subAction->setChecked(this->m_shader.is360Degree());
    connect(subAction, &QAction::triggered, &this->m_shaderDelegate, &ShaderCompositerDelegate::use360Degree);

    this->createProjectionTypeMenu(subMenu);

    const auto slotList =
    {
        qMakePair(AnyVODEnums::SK_UP_VR_PINCUSHION_COEFFICIENTS_K1, &MediaPlayerDelegate::pincushionUpK1),
        qMakePair(AnyVODEnums::SK_DOWN_VR_PINCUSHION_COEFFICIENTS_K1, &MediaPlayerDelegate::pincushionDownK1),
        qMakePair(AnyVODEnums::SK_UP_VR_PINCUSHION_COEFFICIENTS_K2, &MediaPlayerDelegate::pincushionUpK2),
        qMakePair(AnyVODEnums::SK_DOWN_VR_PINCUSHION_COEFFICIENTS_K2, &MediaPlayerDelegate::pincushionDownK2),
    };

    for (const auto &slot : slotList)
    {
        subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(slot.first));
        subAction->setShortcut(this->m_shortcutKey.getShortcutKey(slot.first));
        subAction->setShortcutVisibleInContextMenu(true);
        connect(subAction, &QAction::triggered, &this->m_playerDelegate, slot.second);
    }
}

void ScreenContextMenu::createVRMenu(QMenu *menu)
{
    QMenu *subMenu = nullptr;
    QAction *subAction = nullptr;
    AnyVODEnums::VRInputSource curSource = this->m_player.getVRInputSource();

    subMenu = menu->addMenu(tr("VR 평면 영상"));

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_VR_INPUT_SOURCE_ORDER));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_VR_INPUT_SOURCE_ORDER));
    subAction->setShortcutVisibleInContextMenu(true);
    connect(subAction, &QAction::triggered, &this->m_playerDelegate, &MediaPlayerDelegate::vrInputSourceOrder);

    for (int i = AnyVODEnums::VRI_NONE; i < AnyVODEnums::VRI_COUNT; i++)
    {
        AnyVODEnums::VRInputSource value = (AnyVODEnums::VRInputSource)i;

        subAction = subMenu->addAction(this->m_enums.getVRInputSourceDesc(value));
        subAction->setCheckable(true);
        subAction->setChecked(curSource == value);
        connect(subAction, &QAction::triggered, [this, value]() { this->m_playerDelegate.selectVRInputSource(value); });
    }

    subMenu->addSeparator();

    const auto slotList =
    {
        qMakePair(AnyVODEnums::SK_UP_VIRTUAL_3D_DEPTH, &MediaPlayerDelegate::upVirtual3DDepth),
        qMakePair(AnyVODEnums::SK_DOWN_VIRTUAL_3D_DEPTH, &MediaPlayerDelegate::downVirtual3DDepth),
        qMakePair(AnyVODEnums::SK_NONE, (void (MediaPlayerDelegate::*)())nullptr),
        qMakePair(AnyVODEnums::SK_UP_VR_LENS_CENTER_X, &MediaPlayerDelegate::upLensCenterX),
        qMakePair(AnyVODEnums::SK_DOWN_VR_LENS_CENTER_X, &MediaPlayerDelegate::downLensCenterX),
        qMakePair(AnyVODEnums::SK_UP_VR_LENS_CENTER_Y, &MediaPlayerDelegate::upLensCenterY),
        qMakePair(AnyVODEnums::SK_DOWN_VR_LENS_CENTER_Y, &MediaPlayerDelegate::downLensCenterX),
        qMakePair(AnyVODEnums::SK_NONE, (void (MediaPlayerDelegate::*)())nullptr),
        qMakePair(AnyVODEnums::SK_UP_VR_BARREL_COEFFICIENTS_K1, &MediaPlayerDelegate::barrelUpK1),
        qMakePair(AnyVODEnums::SK_DOWN_VR_BARREL_COEFFICIENTS_K1, &MediaPlayerDelegate::barrelDownK1),
        qMakePair(AnyVODEnums::SK_UP_VR_BARREL_COEFFICIENTS_K2, &MediaPlayerDelegate::barrelUpK2),
        qMakePair(AnyVODEnums::SK_DOWN_VR_BARREL_COEFFICIENTS_K2, &MediaPlayerDelegate::barrelDownK2),
    };

    for (const auto &slot : slotList)
    {
        if (slot.first == AnyVODEnums::SK_NONE)
        {
            subMenu->addSeparator();
        }
        else
        {
            subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(slot.first));
            subAction->setShortcut(this->m_shortcutKey.getShortcutKey(slot.first));
            subAction->setShortcutVisibleInContextMenu(true);
            connect(subAction, &QAction::triggered, &this->m_playerDelegate, slot.second);
        }
    }
}

void ScreenContextMenu::createProjectionTypeMenu(QMenu *menu)
{
    QMenu *subMenu = nullptr;
    QAction *subAction = nullptr;
    AnyVODEnums::Video360Type curType = this->m_shader.getVideo360Type();

    subMenu = menu->addMenu(tr("프로젝션 종류"));

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_PROJECTION_TYPE_ORDER));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_PROJECTION_TYPE_ORDER));
    subAction->setShortcutVisibleInContextMenu(true);
    connect(subAction, &QAction::triggered, &this->m_shaderDelegate, &ShaderCompositerDelegate::projectionTypeOrder);

    for (int i = AnyVODEnums::V3T_EQR; i < AnyVODEnums::V3T_COUNT; i++)
    {
        AnyVODEnums::Video360Type value = (AnyVODEnums::Video360Type)i;

        subAction = subMenu->addAction(this->m_enums.getProjectionTypeDesc(value));
        subAction->setCheckable(true);
        subAction->setChecked(curType == value);
        connect(subAction, &QAction::triggered, [this, value]() { this->m_shaderDelegate.selectProjectionType(value); });
    }
}

void ScreenContextMenu::createDefaultLanguageMenu()
{
    QMenu *subMenu = nullptr;
    QAction *subAction = nullptr;
    QString curLang = GlobalLanguage::getInstance().getLanguage();
    QDir dir(LanguageInstaller::LANG_DIR);
    QStringList fileNames = dir.entryList(QStringList(QString("%1_*.qm").arg(LanguageInstaller::LANG_PREFIX)));

    subMenu = this->m_menu.addMenu(tr("Language"));

    for (int i = 0; i < fileNames.count(); i++)
    {
        QString lang = fileNames[i];

        lang.truncate(lang.lastIndexOf('.'));
        lang.remove(0, lang.indexOf('_') + 1);

        QLocale locale(lang);
        QString desc = QString("%1 (%2)").arg(locale.nativeLanguageName(), locale.nativeCountryName());

        subAction = subMenu->addAction(desc);
        subAction->setCheckable(true);
        subAction->setChecked(curLang == lang);
        connect(subAction, &QAction::triggered, [this, lang, desc]() { this->m_mainWindow->selectLanguage(lang, desc); });
    }
}

void ScreenContextMenu::createUpdateGapMenu()
{
    QMenu *subMenu = nullptr;
    QAction *subAction = nullptr;
    AnyVODEnums::CheckUpdateGap curGap = Updater::getInstance().getUpdateGap();

    subMenu = this->m_menu.addMenu(tr("업데이트 확인 주기"));

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_CHECK_UPDATE_GAP));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_CHECK_UPDATE_GAP));
    subAction->setShortcutVisibleInContextMenu(true);
    connect(subAction, &QAction::triggered, &this->m_updaterDelegate, &UpdaterDelegate::updateGapOrder);

    for (int i = AnyVODEnums::CUG_NONE; i < AnyVODEnums::CUG_COUNT; i++)
    {
        AnyVODEnums::CheckUpdateGap value = (AnyVODEnums::CheckUpdateGap)i;

        subAction = subMenu->addAction(this->m_enums.getUpdateGapDesc(value));
        subAction->setCheckable(true);
        subAction->setChecked(curGap == value);
        connect(subAction, &QAction::triggered, [this, value]() { this->m_updaterDelegate.selectUpdateGap(value); });
    }
}

void ScreenContextMenu::createScreenMenu()
{
    QMenu *subMenu = nullptr;
    QAction *subAction = nullptr;

    subMenu = this->m_menu.addMenu(tr("화면"));

    const auto slotList =
    {
        qMakePair(AnyVODEnums::SK_DEC_OPAQUE, &MainWindowInterface::decOpaque),
        qMakePair(AnyVODEnums::SK_INC_OPAQUE, &MainWindowInterface::incOpaque),
        qMakePair(AnyVODEnums::SK_MIN_OPAQUE, &MainWindowInterface::minOpaque),
        qMakePair(AnyVODEnums::SK_MAX_OPAQUE, &MainWindowInterface::maxOpaque),
    };

    for (const auto &slot : slotList)
    {
        subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(slot.first));
        subAction->setShortcut(this->m_shortcutKey.getShortcutKey(slot.first));
        subAction->setShortcutVisibleInContextMenu(true);
        connect(subAction, &QAction::triggered, this->m_mainWindow, slot.second);
    }

    subMenu->addSeparator();

    QList<QKeySequence> fullScreenShortcuts;

    fullScreenShortcuts.append(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_FULL_SCREEN_RETURN));
    fullScreenShortcuts.append(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_FULL_SCREEN_ENTER));

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_FULL_SCREEN_RETURN));
    subAction->setShortcuts(fullScreenShortcuts);
    subAction->setShortcutVisibleInContextMenu(true);
    subAction->setCheckable(true);
    subAction->setChecked(this->m_mainWindow->isFullScreen());
    connect(subAction, &QAction::triggered, this->m_mainWindow, &MainWindowInterface::fullScreen);

    this->createScreenRatioMenu(subMenu);
    this->createUserAspectRatioMenu(subMenu);
    this->createScreenRotationDegreeMenu(subMenu);
    this->createVideoMenu(subMenu);

    subMenu->addSeparator();

    this->createVideoAttributeMenu(subMenu);
    this->createVideoEffectMenu(subMenu);

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_AUTO_COLOR_CONVERSION));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_AUTO_COLOR_CONVERSION));
    subAction->setShortcutVisibleInContextMenu(true);
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player.isUsingAutoColorConversion());
    connect(subAction, &QAction::triggered, &this->m_playerDelegate, &MediaPlayerDelegate::useAutoColorConversion);

    subMenu->addSeparator();

    this->createCaptureMenu(subMenu);

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_OPEN_SHADER_COMPOSITER));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_OPEN_SHADER_COMPOSITER));
    subAction->setShortcutVisibleInContextMenu(true);
    subAction->setEnabled(this->m_player.isEnabledVideo());
    connect(subAction, &QAction::triggered, this->m_mainWindow, &MainWindowInterface::openShaderCompositer);

    this->createDeinterlaceMenu(subMenu);

    subMenu->addSeparator();

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_VR_USE_DISTORTION));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_VR_USE_DISTORTION));
    subAction->setShortcutVisibleInContextMenu(true);
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player.isUseDistortion());
    connect(subAction, &QAction::triggered, &this->m_playerDelegate, &MediaPlayerDelegate::useDistortion);

    this->createAdjustDistortionModeMenu(subMenu);
    this->createVRMenu(subMenu);
    this->create360DegreeMenu(subMenu);

    subMenu->addSeparator();

    this->createHWDecoderMenu(subMenu);

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_USE_PBO));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_USE_PBO));
    subAction->setShortcutVisibleInContextMenu(true);
    subAction->setEnabled(this->m_player.isUsablePBO());
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player.isUsePBO());
    connect(subAction, &QAction::triggered, &this->m_playerDelegate, &MediaPlayerDelegate::usePBO);

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_USE_GPU_CONVERT));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_USE_GPU_CONVERT));
    subAction->setShortcutVisibleInContextMenu(true);
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player.isUseGPUConvert());
    connect(subAction, &QAction::triggered, &this->m_playerDelegate, &MediaPlayerDelegate::useGPUConvert);

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_USE_HDR));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_USE_HDR));
    subAction->setShortcutVisibleInContextMenu(true);
    subAction->setEnabled(this->m_screen->isHDRSupported());
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player.isUseHDR());
    connect(subAction, &QAction::triggered, &this->m_screenDelegate, &ScreenDelegate::useHDR);

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_USE_VSYNC));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_USE_VSYNC));
    subAction->setShortcutVisibleInContextMenu(true);
    subAction->setCheckable(true);
    subAction->setChecked(this->m_screen->isUseVSync());
    connect(subAction, &QAction::triggered, &this->m_screenDelegate, &ScreenDelegate::useVSync);

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_USE_FRAME_DROP));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_USE_FRAME_DROP));
    subAction->setShortcutVisibleInContextMenu(true);
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player.isUseFrameDrop());
    connect(subAction, &QAction::triggered, &this->m_playerDelegate, &MediaPlayerDelegate::useFrameDrop);

    subMenu->addSeparator();

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_SHOW_ALBUM_JACKET));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_SHOW_ALBUM_JACKET));
    subAction->setShortcutVisibleInContextMenu(true);
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player.isShowAlbumJacket());
    connect(subAction, &QAction::triggered, &this->m_playerDelegate, &MediaPlayerDelegate::showAlbumJacket);

    this->create3DMenu(subMenu);
}

void ScreenContextMenu::createPlayOrderMenu(QMenu *menu)
{
    QMenu *subMenu = nullptr;
    QAction *subAction = nullptr;
    AnyVODEnums::PlayingMethod method = this->m_player.getPlayingMethod();

    subMenu = menu->addMenu(tr("재생 순서"));

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_PLAY_ORDER));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_PLAY_ORDER));
    subAction->setShortcutVisibleInContextMenu(true);
    connect(subAction, &QAction::triggered, &this->m_playerDelegate, &MediaPlayerDelegate::playOrder);

    const QStringList names({tr("전체 순차 재생"), tr("전체 반복 재생"), tr("한 개 재생"), tr("한 개 반복 재생"), tr("무작위 재생")});

    for (int i = AnyVODEnums::PM_TOTAL; i < AnyVODEnums::PM_COUNT; i++)
    {
        const QString &name = names[i];
        AnyVODEnums::PlayingMethod value = (AnyVODEnums::PlayingMethod)i;

        subAction = subMenu->addAction(name);
        subAction->setCheckable(true);
        subAction->setChecked(method == value);
        connect(subAction, &QAction::triggered, [this, value]() { this->m_playerDelegate.selectPlayingMethod(value); });
    }
}

void ScreenContextMenu::createSkipRange(QMenu *menu)
{
    QMenu *subMenu = nullptr;
    QAction *subAction = nullptr;

    subMenu = menu->addMenu(tr("재생 스킵"));

    const auto attributes =
    {
        this->makeActionAttribute(AnyVODEnums::SK_SKIP_OPENING, &MediaPlayerDelegate::skipOpening, this->m_player.getSkipOpening()),
        this->makeActionAttribute(AnyVODEnums::SK_SKIP_ENDING, &MediaPlayerDelegate::skipEnding, this->m_player.getSkipEnding()),
        this->makeActionAttribute(AnyVODEnums::SK_USE_SKIP_RANGE, &MediaPlayerDelegate::useSkipRange, this->m_player.getUseSkipRange()),
    };

    for (const auto &attribute : attributes)
    {
        subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(attribute.slot.first));
        subAction->setShortcut(this->m_shortcutKey.getShortcutKey(attribute.slot.first));
        subAction->setShortcutVisibleInContextMenu(true);
        subAction->setCheckable(true);
        subAction->setChecked(attribute.checked);
        connect(subAction, &QAction::triggered, &this->m_playerDelegate, attribute.slot.second);
    }

    subMenu->addSeparator();

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_SKIP_RANGE_SETTING));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_SKIP_RANGE_SETTING));
    subAction->setShortcutVisibleInContextMenu(true);
    connect(subAction, &QAction::triggered, this->m_mainWindow, &MainWindowInterface::openSkipRange);
}

void ScreenContextMenu::createRepeatRange(QMenu *menu)
{
    QMenu *subMenu = nullptr;
    QAction *subAction = nullptr;

    subMenu = menu->addMenu(tr("구간 반복"));

    const auto slotList =
    {
        qMakePair(AnyVODEnums::SK_REPEAT_RANGE_START, &MediaPlayerDelegate::repeatRangeStart),
        qMakePair(AnyVODEnums::SK_REPEAT_RANGE_END, &MediaPlayerDelegate::repeatRangeEnd),
        qMakePair(AnyVODEnums::SK_NONE, (void (MediaPlayerDelegate::*)())nullptr),
        qMakePair(AnyVODEnums::SK_REPEAT_RANGE_START_BACK_100MS, &MediaPlayerDelegate::repeatRangeStartBack100MS),
        qMakePair(AnyVODEnums::SK_REPEAT_RANGE_START_FORW_100MS, &MediaPlayerDelegate::repeatRangeStartForw100MS),
        qMakePair(AnyVODEnums::SK_NONE, (void (MediaPlayerDelegate::*)())nullptr),
        qMakePair(AnyVODEnums::SK_REPEAT_RANGE_END_BACK_100MS, &MediaPlayerDelegate::repeatRangeEndBack100MS),
        qMakePair(AnyVODEnums::SK_REPEAT_RANGE_END_FORW_100MS, &MediaPlayerDelegate::repeatRangeEndForw100MS),
        qMakePair(AnyVODEnums::SK_NONE, (void (MediaPlayerDelegate::*)())nullptr),
        qMakePair(AnyVODEnums::SK_REPEAT_RANGE_BACK_100MS, &MediaPlayerDelegate::repeatRangeBack100MS),
        qMakePair(AnyVODEnums::SK_REPEAT_RANGE_FORW_100MS, &MediaPlayerDelegate::repeatRangeForw100MS),
    };

    for (const auto &slot : slotList)
    {
        if (slot.first == AnyVODEnums::SK_NONE)
        {
            subMenu->addSeparator();
        }
        else
        {
            subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(slot.first));
            subAction->setShortcut(this->m_shortcutKey.getShortcutKey(slot.first));
            subAction->setShortcutVisibleInContextMenu(true);
            subAction->setEnabled(this->m_player.hasDuration());
            connect(subAction, &QAction::triggered, &this->m_playerDelegate, slot.second);
        }
    }

    subMenu->addSeparator();

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_REPEAT_RANGE_ENABLE));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_REPEAT_RANGE_ENABLE));
    subAction->setShortcutVisibleInContextMenu(true);
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player.getRepeatEnable());
    connect(subAction, &QAction::triggered, &this->m_playerDelegate, &MediaPlayerDelegate::repeatRangeEnable);
}

void ScreenContextMenu::createPlaybackMenu()
{
    QMenu *subMenu = nullptr;
    QAction *subAction = nullptr;

    subMenu = this->m_menu.addMenu(tr("재생"));

    QList<AnyVODEnums::ShortcutKey> toggleShortcuts;
    QList<AnyVODEnums::ShortcutKey> stopShortcuts;

    toggleShortcuts.append(AnyVODEnums::SK_TOGGLE);
    toggleShortcuts.append(AnyVODEnums::SK_TOGGLE_PLAY_MEDIA);
    toggleShortcuts.append(AnyVODEnums::SK_TOGGLE_PAUSE_MEDIA);
    toggleShortcuts.append(AnyVODEnums::SK_TOGGLE_PLAY_PAUSE_MEDIA);

    stopShortcuts.append(AnyVODEnums::SK_STOP);
    stopShortcuts.append(AnyVODEnums::SK_STOP_MEDIA);

    const auto attributesForMainWindow1 =
    {
        this->makeActionAttributeWithKeys(toggleShortcuts, &MainWindowInterface::toggle, !DeviceUtils::determinDevice(this->m_player.getFilePath())),
        this->makeActionAttributeWithKeys(stopShortcuts, &MainWindowInterface::stop, this->m_player.isPlayOrPause()),
    };

    for (const auto &attribute : attributesForMainWindow1)
    {
        subAction = subMenu->addAction(attribute.desc);
        subAction->setShortcuts(attribute.slot.first);
        subAction->setShortcutVisibleInContextMenu(true);
        subAction->setEnabled(attribute.checked);
        connect(subAction, &QAction::triggered, this->m_mainWindow, attribute.slot.second);
    }

    const auto attributes1 =
    {
        this->makeActionAttribute(AnyVODEnums::SK_LAST_PLAY, &MediaPlayerDelegate::lastPlay, this->m_player.isGotoLastPos()),
        this->makeActionAttribute(AnyVODEnums::SK_NONE, (void (MediaPlayerDelegate::*)())nullptr, false),
        this->makeActionAttribute(AnyVODEnums::SK_USE_BUFFERING_MODE, &MediaPlayerDelegate::useBufferingMode, this->m_player.isUseBufferingMode()),
        this->makeActionAttribute(AnyVODEnums::SK_SEEK_KEYFRAME, &MediaPlayerDelegate::seekKeyFrame, this->m_player.isSeekKeyFrame()),
    };

    for (const auto &attribute : attributes1)
    {
        if (attribute.slot.first == AnyVODEnums::SK_NONE)
        {
            subMenu->addSeparator();
        }
        else
        {
            subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(attribute.slot.first));
            subAction->setShortcut(this->m_shortcutKey.getShortcutKey(attribute.slot.first));
            subAction->setShortcutVisibleInContextMenu(true);
            subAction->setCheckable(true);
            subAction->setChecked(attribute.checked);
            connect(subAction, &QAction::triggered, &this->m_playerDelegate, attribute.slot.second);
        }
    }

    const auto attributes2 =
    {
        this->makeActionAttribute(AnyVODEnums::SK_PREV_FRAME, &MediaPlayerDelegate::prevFrame, this->m_player.canMoveFrame()),
        this->makeActionAttribute(AnyVODEnums::SK_NEXT_FRAME, &MediaPlayerDelegate::nextFrame, this->m_player.canMoveFrame()),
        this->makeActionAttribute(AnyVODEnums::SK_NONE, (void (MediaPlayerDelegate::*)())nullptr, false),
        this->makeActionAttribute(AnyVODEnums::SK_REWIND_5, &MediaPlayerDelegate::rewind5, this->m_player.hasDuration()),
        this->makeActionAttribute(AnyVODEnums::SK_FORWARD_5, &MediaPlayerDelegate::forward5, this->m_player.hasDuration()),
        this->makeActionAttribute(AnyVODEnums::SK_REWIND_30, &MediaPlayerDelegate::rewind30, this->m_player.hasDuration()),
        this->makeActionAttribute(AnyVODEnums::SK_FORWARD_30, &MediaPlayerDelegate::forward30, this->m_player.hasDuration()),
        this->makeActionAttribute(AnyVODEnums::SK_REWIND_60, &MediaPlayerDelegate::rewind60, this->m_player.hasDuration()),
        this->makeActionAttribute(AnyVODEnums::SK_FORWARD_60, &MediaPlayerDelegate::forward60, this->m_player.hasDuration()),
        this->makeActionAttribute(AnyVODEnums::SK_GOTO_BEGIN, &MediaPlayerDelegate::gotoBegin, this->m_player.hasDuration()),
        this->makeActionAttribute(AnyVODEnums::SK_NONE, (void (MediaPlayerDelegate::*)())nullptr, false),
        this->makeActionAttribute(AnyVODEnums::SK_PREV_CHAPTER, &MediaPlayerDelegate::prevChapter, this->m_player.canMoveChapter()),
        this->makeActionAttribute(AnyVODEnums::SK_NEXT_CHAPTER, &MediaPlayerDelegate::nextChapter, this->m_player.canMoveChapter()),
    };

    for (const auto &attribute : attributes2)
    {
        if (attribute.slot.first == AnyVODEnums::SK_NONE)
        {
            subMenu->addSeparator();
        }
        else
        {
            subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(attribute.slot.first));
            subAction->setShortcut(this->m_shortcutKey.getShortcutKey(attribute.slot.first));
            subAction->setShortcutVisibleInContextMenu(true);
            subAction->setEnabled(attribute.checked);
            connect(subAction, &QAction::triggered, &this->m_playerDelegate, attribute.slot.second);
        }
    }

    subMenu->addSeparator();

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_SCREEN_EXPLORER));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_SCREEN_EXPLORER));
    subAction->setShortcutVisibleInContextMenu(true);
    subAction->setEnabled(this->m_player.hasDuration() && !this->m_player.isAudio() && !this->m_player.isRemoteFile());
    connect(subAction, &QAction::triggered, this->m_mainWindow, &MainWindowInterface::openScreenExplorer);

    subMenu->addSeparator();

    QList<AnyVODEnums::ShortcutKey> prevShortcuts;
    QList<AnyVODEnums::ShortcutKey> nextShortcuts;

    prevShortcuts.append(AnyVODEnums::SK_PREV);
    prevShortcuts.append(AnyVODEnums::SK_PREV_MEDIA);

    nextShortcuts.append(AnyVODEnums::SK_NEXT);
    nextShortcuts.append(AnyVODEnums::SK_NEXT_MEDIA);

    const auto attributesForMainWindow2 =
    {
        this->makeActionAttributeWithKeys(prevShortcuts, &MainWindowInterface::prev, this->m_mainWindow->isPrevEnabled()),
        this->makeActionAttributeWithKeys(nextShortcuts, &MainWindowInterface::next, this->m_mainWindow->isNextEnabled()),
    };

    for (const auto &attribute : attributesForMainWindow2)
    {
        subAction = subMenu->addAction(attribute.desc);
        subAction->setShortcuts(attribute.slot.first);
        subAction->setShortcutVisibleInContextMenu(true);
        subAction->setEnabled(attribute.checked);
        connect(subAction, &QAction::triggered, this->m_mainWindow, attribute.slot.second);
    }

    subMenu->addSeparator();

    this->createRepeatRange(subMenu);

    subMenu->addSeparator();

    const auto slotList =
    {
        qMakePair(AnyVODEnums::SK_NORMAL_PLAYBACK, &MediaPlayerDelegate::normalPlayback),
        qMakePair(AnyVODEnums::SK_SLOWER_PLAYBACK, &MediaPlayerDelegate::slowerPlayback),
        qMakePair(AnyVODEnums::SK_FASTER_PLAYBACK, &MediaPlayerDelegate::fasterPlayback),
    };

    for (const auto &slot : slotList)
    {
        subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(slot.first));
        subAction->setShortcut(this->m_shortcutKey.getShortcutKey(slot.first));
        subAction->setShortcutVisibleInContextMenu(true);
        subAction->setEnabled(this->m_player.isTempoUsable());
        connect(subAction, &QAction::triggered, &this->m_playerDelegate, slot.second);
    }

    subMenu->addSeparator();

    this->createPlayOrderMenu(subMenu);
    this->createSkipRange(subMenu);
}

void ScreenContextMenu::createAlignMenu(QMenu *menu)
{
    QAction *subAction = nullptr;
    QMenu *subMenu = nullptr;
    AnyVODEnums::HAlignMethod halign = this->m_player.getHAlign();
    AnyVODEnums::VAlignMethod valign = this->m_player.getVAlign();

    subMenu = menu->addMenu(tr("정렬"));

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_SUBTITLE_HALIGN_ORDER));
    subAction->setEnabled(this->m_player.isAlignable());
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_SUBTITLE_HALIGN_ORDER));
    subAction->setShortcutVisibleInContextMenu(true);
    connect(subAction, &QAction::triggered, &this->m_playerDelegate, &MediaPlayerDelegate::subtitleHAlignOrder);

    const QStringList hNames({tr("기본 정렬"), tr("자동 정렬"), tr("왼쪽 정렬"), tr("가운데 정렬"), tr("오른쪽 정렬")});

    for (int i = AnyVODEnums::HAM_NONE; i < AnyVODEnums::HAM_COUNT; i++)
    {
        const QString &name = hNames[i];
        AnyVODEnums::HAlignMethod value = (AnyVODEnums::HAlignMethod)i;

        subAction = subMenu->addAction(name);
        subAction->setEnabled(this->m_player.isAlignable());
        subAction->setCheckable(true);
        subAction->setChecked(halign == value);
        connect(subAction, &QAction::triggered, [this, value]() { this->m_playerDelegate.selectSubtitleHAlignMethod(value); });
    }

    subMenu->addSeparator();

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_SUBTITLE_VALIGN_ORDER));
    subAction->setEnabled(this->m_player.isAlignable());
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_SUBTITLE_VALIGN_ORDER));
    subAction->setShortcutVisibleInContextMenu(true);
    connect(subAction, &QAction::triggered, &this->m_playerDelegate, &MediaPlayerDelegate::subtitleVAlignOrder);

    const QStringList vNames = QStringList({tr("기본 정렬"), tr("상단 정렬"), tr("가운데 정렬"), tr("하단 정렬")});

    for (int i = AnyVODEnums::VAM_NONE; i < AnyVODEnums::VAM_COUNT; i++)
    {
        const QString &name = vNames[i];
        AnyVODEnums::VAlignMethod value = (AnyVODEnums::VAlignMethod)i;

        subAction = subMenu->addAction(name);
        subAction->setEnabled(this->m_player.isAlignable());
        subAction->setCheckable(true);
        subAction->setChecked(valign == value);
        connect(subAction, &QAction::triggered, [this, value]() { this->m_playerDelegate.selectSubtitleVAlignMethod(value); });
    }
}

void ScreenContextMenu::createLanguageMenu(QMenu *menu)
{
    QAction *subAction = nullptr;
    QMenu *subMenu = nullptr;
    QStringList subtitleClasses;
    QString currentClassName;

    this->m_player.getSubtitleClasses(&subtitleClasses);
    this->m_player.getCurrentSubtitleClass(&currentClassName);

    subMenu = menu->addMenu(tr("언어"));

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_SUBTITLE_LANGUAGE_ORDER));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_SUBTITLE_LANGUAGE_ORDER));
    subAction->setShortcutVisibleInContextMenu(true);
    connect(subAction, &QAction::triggered, &this->m_playerDelegate, &MediaPlayerDelegate::subtitleLanguageOrder);

    if (subtitleClasses.count() > 0)
    {
        for (int i = 0; i < subtitleClasses.count(); i++)
        {
            QString &className = subtitleClasses[i];

            subAction = subMenu->addAction(subtitleClasses[i]);
            subAction->setCheckable(true);
            subAction->setChecked(currentClassName == className);
            connect(subAction, &QAction::triggered, [this, className]() { this->m_playerDelegate.selectSubtitleClass(className); });
        }
    }
    else
    {
       subMenu->setDisabled(true);
    }
}

void ScreenContextMenu::createPositionMenu(QMenu *menu)
{
    QMenu *subMenu = menu->addMenu(tr("위치"));

    const auto slotList =
    {
        qMakePair(AnyVODEnums::SK_RESET_SUBTITLE_POSITION, &MediaPlayerDelegate::resetSubtitlePosition),
        qMakePair(AnyVODEnums::SK_UP_SUBTITLE_POSITION, &MediaPlayerDelegate::upSubtitlePosition),
        qMakePair(AnyVODEnums::SK_DOWN_SUBTITLE_POSITION, &MediaPlayerDelegate::downSubtitlePosition),
        qMakePair(AnyVODEnums::SK_LEFT_SUBTITLE_POSITION, &MediaPlayerDelegate::leftSubtitlePosition),
        qMakePair(AnyVODEnums::SK_RIGHT_SUBTITLE_POSITION, &MediaPlayerDelegate::rightSubtitlePosition),
    };

    for (const auto &slot : slotList)
    {
        QAction *subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(slot.first));

        subAction->setEnabled(this->m_player.isSubtitleMoveable());
        subAction->setShortcut(this->m_shortcutKey.getShortcutKey(slot.first));
        subAction->setShortcutVisibleInContextMenu(true);
        connect(subAction, &QAction::triggered, &this->m_playerDelegate, slot.second);
    }
}

void ScreenContextMenu::createSubtitleMenu()
{
    QAction *subAction = nullptr;
    QMenu *subMenu = nullptr;

    subMenu = this->m_menu.addMenu(tr("자막 / 가사"));

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_SUBTITLE_TOGGLE));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_SUBTITLE_TOGGLE));
    subAction->setShortcutVisibleInContextMenu(true);
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player.isShowSubtitle());
    connect(subAction, &QAction::triggered, &this->m_playerDelegate, &MediaPlayerDelegate::subtitleToggle);

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_CLOSE_EXTERNAL_SUBTITLE));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_CLOSE_EXTERNAL_SUBTITLE));
    subAction->setShortcutVisibleInContextMenu(true);
    subAction->setEnabled(this->m_player.existExternalSubtitle());
    connect(subAction, &QAction::triggered, &this->m_playerDelegate, &MediaPlayerDelegate::closeExternalSubtitle);

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_IMPORT_FONTS));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_IMPORT_FONTS));
    subAction->setShortcutVisibleInContextMenu(true);
    connect(subAction, &QAction::triggered, this->m_mainWindow, &MainWindowInterface::openImportFonts);

    subMenu->addSeparator();

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_SEARCH_SUBTITLE_COMPLEX));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_SEARCH_SUBTITLE_COMPLEX));
    subAction->setShortcutVisibleInContextMenu(true);
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player.getSearchSubtitleComplex());
    connect(subAction, &QAction::triggered, &this->m_playerDelegate, &MediaPlayerDelegate::useSearchSubtitleComplex);

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_SUBTITLE_DIRECTORY));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_SUBTITLE_DIRECTORY));
    subAction->setShortcutVisibleInContextMenu(true);
    connect(subAction, &QAction::triggered, this->m_mainWindow, &MainWindowInterface::openSubtitleDirectory);

    subMenu->addSeparator();

    const auto attributes1 =
    {
        this->makeActionAttribute(AnyVODEnums::SK_INC_SUBTITLE_OPAQUE, &MediaPlayerDelegate::incSubtitleOpaque, this->m_player.existSubtitle()),
        this->makeActionAttribute(AnyVODEnums::SK_DEC_SUBTITLE_OPAQUE, &MediaPlayerDelegate::decSubtitleOpaque, this->m_player.existSubtitle()),
        this->makeActionAttribute(AnyVODEnums::SK_RESET_SUBTITLE_OPAQUE, &MediaPlayerDelegate::resetSubtitleOpaque, this->m_player.existSubtitle()),
        this->makeActionAttribute(AnyVODEnums::SK_NONE, (void (MediaPlayerDelegate::*)())nullptr, false),
        this->makeActionAttribute(AnyVODEnums::SK_INC_SUBTITLE_SIZE, &MediaPlayerDelegate::incSubtitleSize, this->m_player.isAlignable()),
        this->makeActionAttribute(AnyVODEnums::SK_DEC_SUBTITLE_SIZE, &MediaPlayerDelegate::decSubtitleSize, this->m_player.isAlignable()),
        this->makeActionAttribute(AnyVODEnums::SK_RESET_SUBTITLE_SIZE, &MediaPlayerDelegate::resetSubtitleSize, this->m_player.isAlignable()),
        this->makeActionAttribute(AnyVODEnums::SK_NONE, (void (MediaPlayerDelegate::*)())nullptr, false),
        this->makeActionAttribute(AnyVODEnums::SK_PREV_SUBTITLE_SYNC, &MediaPlayerDelegate::prevSubtitleSync, this->m_player.existSubtitle()),
        this->makeActionAttribute(AnyVODEnums::SK_NEXT_SUBTITLE_SYNC, &MediaPlayerDelegate::nextSubtitleSync, this->m_player.existSubtitle()),
        this->makeActionAttribute(AnyVODEnums::SK_RESET_SUBTITLE_SYNC, &MediaPlayerDelegate::resetSubtitleSync, this->m_player.existSubtitle()),
    };

    for (const auto &attribute : attributes1)
    {
        if (attribute.slot.first == AnyVODEnums::SK_NONE)
        {
            subMenu->addSeparator();
        }
        else
        {
            subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(attribute.slot.first));
            subAction->setShortcut(this->m_shortcutKey.getShortcutKey(attribute.slot.first));
            subAction->setShortcutVisibleInContextMenu(true);
            subAction->setEnabled(attribute.checked);
            connect(subAction, &QAction::triggered, &this->m_playerDelegate, attribute.slot.second);
        }
    }

    subMenu->addSeparator();

    this->createPositionMenu(subMenu);
    this->createAlignMenu(subMenu);
    this->createLanguageMenu(subMenu);

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_OPEN_TEXT_ENCODING));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_OPEN_TEXT_ENCODING));
    subAction->setShortcutVisibleInContextMenu(true);
    connect(subAction, &QAction::triggered, this->m_mainWindow, &MainWindowInterface::openTextEncoding);

    subMenu->addSeparator();

    const auto attributes2 =
    {
        this->makeActionAttribute(AnyVODEnums::SK_ENABLE_SEARCH_SUBTITLE, &MediaPlayerDelegate::enableSearchSubtitle, this->m_player.isEnableSearchSubtitle()),
        this->makeActionAttribute(AnyVODEnums::SK_ENABLE_SEARCH_LYRICS, &MediaPlayerDelegate::enableSearchLyrics, this->m_player.isEnableSearchSubtitle()),
        this->makeActionAttribute(AnyVODEnums::SK_AUTO_SAVE_SEARCH_LYRICS, &MediaPlayerDelegate::autoSaveSearchLyrics, this->m_player.isAutoSaveSearchLyrics()),
    };

    for (const auto &attribute : attributes2)
    {
        subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(attribute.slot.first));
        subAction->setShortcut(this->m_shortcutKey.getShortcutKey(attribute.slot.first));
        subAction->setShortcutVisibleInContextMenu(true);
        subAction->setCheckable(true);
        subAction->setChecked(attribute.checked);
        connect(subAction, &QAction::triggered, &this->m_playerDelegate, attribute.slot.second);
    }

    subMenu->addSeparator();

    const auto slotList =
    {
        qMakePair(AnyVODEnums::SK_SAVE_SUBTITLE, &MediaPlayerDelegate::saveSubtitle),
        qMakePair(AnyVODEnums::SK_SAVE_AS_SUBTITLE, &MediaPlayerDelegate::saveAsSubtitle),
    };

    for (const auto &slot : slotList)
    {
        subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(slot.first));
        subAction->setShortcut(this->m_shortcutKey.getShortcutKey(slot.first));
        subAction->setShortcutVisibleInContextMenu(true);
        subAction->setEnabled(this->m_player.existFileSubtitle());
        connect(subAction, &QAction::triggered, &this->m_playerDelegate, slot.second);
    }

    subMenu->addSeparator();

    this->create3DSubtitleMenu(subMenu);
}

void ScreenContextMenu::create3DSubtitleMenu(QMenu *menu)
{
    QMenu *subMenu = nullptr;
    QAction *subAction = nullptr;
    AnyVODEnums::Subtitle3DMethod method = this->m_player.getSubtitle3DMethod();

    subMenu = menu->addMenu(tr("3D 자막 설정"));

    const auto slotList =
    {
        qMakePair(AnyVODEnums::SK_RESET_3D_SUBTITLE_OFFSET, &MediaPlayerDelegate::reset3DSubtitleOffset),
        qMakePair(AnyVODEnums::SK_UP_3D_SUBTITLE_OFFSET, &MediaPlayerDelegate::up3DSubtitleOffset),
        qMakePair(AnyVODEnums::SK_DOWN_3D_SUBTITLE_OFFSET, &MediaPlayerDelegate::down3DSubtitleOffset),
        qMakePair(AnyVODEnums::SK_LEFT_3D_SUBTITLE_OFFSET, &MediaPlayerDelegate::left3DSubtitleOffset),
        qMakePair(AnyVODEnums::SK_RIGHT_3D_SUBTITLE_OFFSET, &MediaPlayerDelegate::right3DSubtitleOffset),
    };

    for (const auto &slot : slotList)
    {
        subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(slot.first));
        subAction->setEnabled(this->m_player.is3DSubtitleMoveable());
        subAction->setShortcut(this->m_shortcutKey.getShortcutKey(slot.first));
        connect(subAction, &QAction::triggered, &this->m_playerDelegate, slot.second);
    }

    subMenu->addSeparator();

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_3D_SUBTITLE_METHOD_ORDER));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_3D_SUBTITLE_METHOD_ORDER));
    connect(subAction, &QAction::triggered, &this->m_playerDelegate, &MediaPlayerDelegate::method3DSubtitleOrder);

    for (int i = AnyVODEnums::S3M_NONE; i < AnyVODEnums::S3M_COUNT; i++)
    {
        AnyVODEnums::Subtitle3DMethod value = (AnyVODEnums::Subtitle3DMethod)i;

        subAction = subMenu->addAction(this->m_enums.getSubtitle3DMethodDesc(value));
        subAction->setCheckable(true);
        subAction->setChecked(method == value);
        connect(subAction, &QAction::triggered, [this, value]() { this->m_playerDelegate.select3DSubtitleMethod(value); });
    }
}

void ScreenContextMenu::createSPDIFEncodingMenu(QMenu *menu)
{
    QMenu *subMenu = nullptr;
    QAction *subAction = nullptr;
    AnyVODEnums::SPDIFEncodingMethod method = this->m_player.getSPDIFEncodingMethod();

    subMenu = menu->addMenu(tr("인코딩 사용"));

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_USE_SPDIF_ENCODING_ORDER));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_USE_SPDIF_ENCODING_ORDER));
    subAction->setShortcutVisibleInContextMenu(true);
    connect(subAction, &QAction::triggered, &this->m_playerDelegate, &MediaPlayerDelegate::useSPDIFEncodingOrder);

    const QStringList names({tr("사용 안 함"), "AC3", "DTS"});

    for (int i = AnyVODEnums::SEM_NONE; i < AnyVODEnums::SEM_COUNT; i++)
    {
        const QString &name = names[i];
        AnyVODEnums::SPDIFEncodingMethod value = (AnyVODEnums::SPDIFEncodingMethod)i;

        subAction = subMenu->addAction(name);
        subAction->setCheckable(true);
        subAction->setChecked(method == value);
        connect(subAction, &QAction::triggered, [this, value]() { this->m_playerDelegate.selectSPDIFEncodingMethod(value); });
    }
}

void ScreenContextMenu::createDTVMenu()
{
    QMenu *subMenu = this->m_menu.addMenu(tr("DTV 열기"));
    const DTVReader &reader = DTVReader::getInstance();

    const auto attributes =
    {
        this->makeActionAttribute(AnyVODEnums::SK_ADD_DTV_CHANNEL_TO_PLAYLIST, &MainWindowInterface::addDTVChannelToPlaylist, reader.isSupport()),
        this->makeActionAttribute(AnyVODEnums::SK_OPEN_DTV_SCAN_CHANNEL, &MainWindowInterface::openScanDTVChannel, reader.isSupport()),
        this->makeActionAttribute(AnyVODEnums::SK_NONE, (void (MainWindowInterface::*)())nullptr, false),
        this->makeActionAttribute(AnyVODEnums::SK_VIEW_EPG, &MainWindowInterface::viewEPG, reader.isSupport() && reader.isOpened()),
    };

    for (const auto &attribute : attributes)
    {
        if (attribute.slot.first == AnyVODEnums::SK_NONE)
        {
            subMenu->addSeparator();
        }
        else
        {
            QAction *subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(attribute.slot.first));

            subAction->setShortcut(this->m_shortcutKey.getShortcutKey(attribute.slot.first));
            subAction->setShortcutVisibleInContextMenu(true);
            subAction->setEnabled(attribute.checked);
            connect(subAction, &QAction::triggered, this->m_mainWindow, attribute.slot.second);
        }
    }
}

void ScreenContextMenu::createRadioMenu()
{
    QMenu *subMenu = this->m_menu.addMenu(tr("라디오 열기"));
    const RadioReader &reader = RadioReader::getInstance();

    const auto attributes =
    {
        this->makeActionAttribute(AnyVODEnums::SK_ADD_RADIO_CHANNEL_TO_PLAYLIST, &MainWindowInterface::addRadioChannelToPlaylist, reader.isSupport()),
        this->makeActionAttribute(AnyVODEnums::SK_OPEN_RADIO_SCAN_CHANNEL, &MainWindowInterface::openScanRadioChannel, reader.isSupport()),
        this->makeActionAttribute(AnyVODEnums::SK_NONE, (void (MainWindowInterface::*)())nullptr, false),
    };

    for (const auto &attribute : attributes)
    {
        if (attribute.slot.first == AnyVODEnums::SK_NONE)
        {
            subMenu->addSeparator();
        }
        else
        {
            QAction *subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(attribute.slot.first));

            subAction->setShortcut(this->m_shortcutKey.getShortcutKey(attribute.slot.first));
            subAction->setShortcutVisibleInContextMenu(true);
            subAction->setEnabled(attribute.checked);
            connect(subAction, &QAction::triggered, this->m_mainWindow, attribute.slot.second);
        }
    }
}

void ScreenContextMenu::createSPDIFAudioDeviceMenu(QMenu *menu)
{
    QMenu *subMenu = nullptr;
    QAction *subAction = nullptr;
    QStringList audioDevices;
    int curDevice;

    this->m_player.getSPDIFAudioDevices(&audioDevices);
    curDevice = this->m_player.getCurrentSPDIFAudioDevice();

    subMenu = menu->addMenu(tr("소리 출력 장치"));

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_SPDIF_AUDIO_DEVICE_ORDER));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_SPDIF_AUDIO_DEVICE_ORDER));
    subAction->setShortcutVisibleInContextMenu(true);
    connect(subAction, &QAction::triggered, &this->m_playerDelegate, &MediaPlayerDelegate::audioSPDIFDeviceOrder);

    subAction = subMenu->addAction(tr("기본 장치"));
    subAction->setCheckable(true);
    subAction->setChecked(curDevice == -1);
    connect(subAction, &QAction::triggered, [this]() { this->m_playerDelegate.selectSPDIFAudioDevice(-1); });

    for (int i = 0; i < audioDevices.count(); i++)
    {
        subAction = subMenu->addAction(audioDevices[i]);
        subAction->setCheckable(true);
        subAction->setChecked(curDevice == i);
        connect(subAction, &QAction::triggered, [this, i]() { this->m_playerDelegate.selectSPDIFAudioDevice(i); });
    }
}

void ScreenContextMenu::createSPDIFMenu(QMenu *menu)
{
    QMenu *subMenu = nullptr;
    QAction *subAction = nullptr;
    const SPDIFSampleRates &rates = SPDIFSampleRates::getInstance();

    subMenu = menu->addMenu(tr("S/PDIF 설정"));
    subMenu->setEnabled(SPDIF::getInstance().isAvailable());

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_USE_SPDIF));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_USE_SPDIF));
    subAction->setShortcutVisibleInContextMenu(true);
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player.isUseSPDIF());
    connect(subAction, &QAction::triggered, &this->m_playerDelegate, &MediaPlayerDelegate::useSPDIF);

    this->createSPDIFEncodingMenu(subMenu);
    this->createSPDIFAudioDeviceMenu(subMenu);

    subMenu->addSeparator();

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_SPDIF_SAMPLE_RATE_ORDER));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_SPDIF_SAMPLE_RATE_ORDER));
    subAction->setShortcutVisibleInContextMenu(true);
    connect(subAction, &QAction::triggered, &this->m_playerDelegate, &MediaPlayerDelegate::spdifSampleRateOrder);

    for (int i = 0; i < rates.getCount(); i++)
    {
        int item = rates.getSampleRate(i);
        QString title;

        if (rates.isDefaultSampleRate(item))
            title = tr("기본 속도");
        else
            title = QString("%1kHz").arg(item / 1000.0f, 0, 'f', 1);

        subAction = subMenu->addAction(title);
        subAction->setCheckable(true);
        subAction->setChecked(rates.getSelectedSampleRate() == item);
        connect(subAction, &QAction::triggered, [this, item]() { this->m_playerDelegate.selectSPDIFSampleRate(item); });
    }
}

void ScreenContextMenu::createAudioEffectMenu(QMenu *menu)
{
    QMenu *subMenu = menu->addMenu(tr("소리 효과"));

    const auto attributes =
    {
        this->makeActionAttribute(AnyVODEnums::SK_LOWER_VOICE, &MediaPlayerDelegate::lowerVoice, this->m_player.isUsingLowerVoice()),
        this->makeActionAttribute(AnyVODEnums::SK_HIGHER_VOICE, &MediaPlayerDelegate::higherVoice, this->m_player.isUsingHigherVoice()),
        this->makeActionAttribute(AnyVODEnums::SK_LOWER_MUSIC, &MediaPlayerDelegate::lowerMusic, this->m_player.isUsingLowerMusic()),
    };

    for (const auto &attribute : attributes)
    {
        QAction *subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(attribute.slot.first));

        subAction->setCheckable(true);
        subAction->setChecked(attribute.checked);
        subAction->setShortcut(this->m_shortcutKey.getShortcutKey(attribute.slot.first));
        subAction->setShortcutVisibleInContextMenu(true);
        subAction->setEnabled(!this->m_player.isUseSPDIF());
        connect(subAction, &QAction::triggered, &this->m_playerDelegate, attribute.slot.second);
    }
}

void ScreenContextMenu::createAudioMenu(QMenu *menu)
{
    QMenu *subMenu = nullptr;
    QAction *subAction = nullptr;

    subMenu = menu->addMenu(tr("음성"));

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_AUDIO_ORDER));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_AUDIO_ORDER));
    subAction->setShortcutVisibleInContextMenu(true);
    connect(subAction, &QAction::triggered, &this->m_playerDelegate, &MediaPlayerDelegate::audioOrder);

    QVector<AudioStreamInfo> audioInfo;
    int curAudioIndex = this->m_player.getCurrentAudioStreamIndex();

    this->m_player.getAudioStreamInfo(&audioInfo);

    if (audioInfo.count() > 0)
    {
        for (int i = 0; i < audioInfo.count(); i++)
        {
            AudioStreamInfo &info = audioInfo[i];

            subAction = subMenu->addAction(audioInfo[i].name);
            subAction->setCheckable(true);
            subAction->setChecked(curAudioIndex == info.index);
            connect(subAction, &QAction::triggered, [this, info]() { this->m_playerDelegate.selectAudioStream(info.index); });
        }
    }
    else
    {
       subMenu->setDisabled(true);
    }
}

void ScreenContextMenu::createAudioDeviceMenu(QMenu *menu)
{
    QMenu *subMenu = nullptr;
    QAction *subAction = nullptr;
    QStringList audioDevices = AudioRenderer::getDevices();
    int curDevice = this->m_player.getCurrentAudioDevice();

    subMenu = menu->addMenu(tr("소리 출력 장치"));

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_AUDIO_DEVICE_ORDER));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_AUDIO_DEVICE_ORDER));
    subAction->setShortcutVisibleInContextMenu(true);
    connect(subAction, &QAction::triggered, &this->m_playerDelegate, &MediaPlayerDelegate::audioDeviceOrder);

    subAction = subMenu->addAction(tr("기본 장치"));
    subAction->setCheckable(true);
    subAction->setChecked(curDevice == -1);
    connect(subAction, &QAction::triggered, [this]() { this->m_playerDelegate.selectAudioDevice(-1); });

    for (int i = 0; i < audioDevices.count(); i++)
    {
        subAction = subMenu->addAction(audioDevices[i]);
        subAction->setCheckable(true);
        subAction->setChecked(curDevice == i);
        connect(subAction, &QAction::triggered, [this, i]() { this->m_playerDelegate.selectAudioDevice(i); });
    }
}

void ScreenContextMenu::createSoundMenu()
{
    QMenu *subMenu = nullptr;
    QAction *subAction = nullptr;

    subMenu = this->m_menu.addMenu(tr("소리"));

    this->createAudioDeviceMenu(subMenu);
    this->createSPDIFMenu(subMenu);

    subMenu->addSeparator();

    const auto attributes =
    {
        this->makeActionAttribute(AnyVODEnums::SK_AUDIO_NORMALIZE, &MediaPlayerDelegate::audioNormalize, this->m_player.isUsingNormalizer()),
        this->makeActionAttribute(AnyVODEnums::SK_AUDIO_EQUALIZER, &MediaPlayerDelegate::audioEqualizer, this->m_player.isUsingEqualizer()),
    };

    for (const auto &attribute : attributes)
    {
        subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(attribute.slot.first));
        subAction->setCheckable(true);
        subAction->setChecked(attribute.checked);
        subAction->setShortcut(this->m_shortcutKey.getShortcutKey(attribute.slot.first));
        subAction->setShortcutVisibleInContextMenu(true);
        subAction->setEnabled(!this->m_player.isUseSPDIF());
        connect(subAction, &QAction::triggered, &this->m_playerDelegate, attribute.slot.second);
    }

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_OPEN_EQUALIZER_SETTING));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_OPEN_EQUALIZER_SETTING));
    subAction->setShortcutVisibleInContextMenu(true);
    subAction->setEnabled(!this->m_player.isUseSPDIF());
    connect(subAction, &QAction::triggered, this->m_mainWindow, &MainWindowInterface::openEqualizerSetting);

    subMenu->addSeparator();

    QList<AnyVODEnums::ShortcutKey> volUpShortcuts;
    QList<AnyVODEnums::ShortcutKey> volDownShortcuts;

    volUpShortcuts.append(AnyVODEnums::SK_VOLUME_UP);
    volUpShortcuts.append(AnyVODEnums::SK_VOLUME_UP_MEDIA);

    volDownShortcuts.append(AnyVODEnums::SK_VOLUME_DOWN);
    volDownShortcuts.append(AnyVODEnums::SK_VOLUME_DOWN_MEDIA);

    const auto attributesWithKeys =
    {
        this->makeActionAttributeWithKeys(volUpShortcuts, &MainWindowInterface::volumeUp),
        this->makeActionAttributeWithKeys(volDownShortcuts, &MainWindowInterface::volumeDown),
    };

    for (const auto &attribute : attributesWithKeys)
    {
        subAction = subMenu->addAction(attribute.desc);
        subAction->setShortcuts(attribute.slot.first);
        subAction->setShortcutVisibleInContextMenu(true);
        subAction->setEnabled(!this->m_player.isUseSPDIF());
        connect(subAction, &QAction::triggered, this->m_mainWindow, attribute.slot.second);
    }

    QList<QKeySequence> muteShortcuts;

    muteShortcuts.append(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_MUTE));
    muteShortcuts.append(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_MUTE_MEDIA));

    subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_MUTE));
    subAction->setShortcuts(muteShortcuts);
    subAction->setShortcutVisibleInContextMenu(true);
    subAction->setCheckable(true);
    subAction->setChecked(this->m_mainWindow->isMute());
    subAction->setEnabled(!this->m_player.isUseSPDIF());
    connect(subAction, &QAction::triggered, this->m_mainWindow, &MainWindowInterface::mute);

    subMenu->addSeparator();

    const auto slotList =
    {
        qMakePair(AnyVODEnums::SK_PREV_AUDIO_SYNC, &MediaPlayerDelegate::prevAudioSync),
        qMakePair(AnyVODEnums::SK_NEXT_AUDIO_SYNC, &MediaPlayerDelegate::nextAudioSync),
        qMakePair(AnyVODEnums::SK_RESET_AUDIO_SYNC, &MediaPlayerDelegate::resetAudioSync),
    };

    for (const auto &slot : slotList)
    {
        subAction = subMenu->addAction(this->m_shortcutKey.getShortcutKeyDesc(slot.first));
        subAction->setShortcut(this->m_shortcutKey.getShortcutKey(slot.first));
        subAction->setShortcutVisibleInContextMenu(true);
        connect(subAction, &QAction::triggered, &this->m_playerDelegate, slot.second);
    }

    subMenu->addSeparator();

    this->createAudioMenu(subMenu);
    this->createAudioEffectMenu(subMenu);
}

void ScreenContextMenu::createContextMenu()
{
    QAction *subAction = nullptr;

    const auto attributesWithEnabled =
    {
        this->makeActionAttribute(AnyVODEnums::SK_MOST_TOP, &MainWindowInterface::mostTop, this->m_mainWindow->isMostTop(), this->m_shortcutKey.isShortcutKeyEnabled(AnyVODEnums::SK_MOST_TOP)),
        this->makeActionAttribute(AnyVODEnums::SK_NONE, (void (MainWindowInterface::*)())nullptr, false, false),
        this->makeActionAttribute(AnyVODEnums::SK_OPEN_REMOTE_FILE_LIST, &MainWindowInterface::openRemoteFileList, this->m_mainWindow->getRemoteFileList().isVisible(), this->m_mainWindow->isLogined()),
    };

    for (const auto &attribute : attributesWithEnabled)
    {
        if (attribute.slot.first == AnyVODEnums::SK_NONE)
        {
            this->m_menu.addSeparator();
        }
        else
        {
            subAction = this->m_menu.addAction(this->m_shortcutKey.getShortcutKeyDesc(attribute.slot.first));
            subAction->setShortcut(this->m_shortcutKey.getShortcutKey(attribute.slot.first));
            subAction->setShortcutVisibleInContextMenu(true);
            subAction->setCheckable(true);
            subAction->setChecked(attribute.checked);
            subAction->setEnabled(attribute.enabled);
            connect(subAction, &QAction::triggered, this->m_mainWindow, attribute.slot.second);
        }
    }

    const auto attributes1 =
    {
        this->makeActionAttribute(AnyVODEnums::SK_LOGIN, &MainWindowInterface::login, !this->m_mainWindow->isLogined()),
        this->makeActionAttribute(AnyVODEnums::SK_LOGOUT, &MainWindowInterface::logout, this->m_mainWindow->isLogined()),
    };

    for (const auto &attribute : attributes1)
    {
        subAction = this->m_menu.addAction(this->m_shortcutKey.getShortcutKeyDesc(attribute.slot.first));
        subAction->setShortcut(this->m_shortcutKey.getShortcutKey(attribute.slot.first));
        subAction->setShortcutVisibleInContextMenu(true);
        subAction->setEnabled(attribute.checked);
        connect(subAction, &QAction::triggered, this->m_mainWindow, attribute.slot.second);
    }

    subAction = this->m_menu.addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_OPEN_SERVER_SETTING));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_OPEN_SERVER_SETTING));
    subAction->setShortcutVisibleInContextMenu(true);
    connect(subAction, &QAction::triggered, this->m_mainWindow, &MainWindowInterface::openServerSetting);

    this->m_menu.addSeparator();

    const auto slotList1 =
    {
        qMakePair(AnyVODEnums::SK_OPEN, (void (MainWindowInterface::*)())&MainWindowInterface::open),
        qMakePair(AnyVODEnums::SK_OPEN_EXTERNAL, &MainWindowInterface::openExternal),
        qMakePair(AnyVODEnums::SK_OPEN_DEVICE, &MainWindowInterface::openDevice),
        qMakePair(AnyVODEnums::SK_OPEN_AUDIO_CD, &MainWindowInterface::openAudioCD),
    };

    for (const auto &slot : slotList1)
    {
        subAction = this->m_menu.addAction(this->m_shortcutKey.getShortcutKeyDesc(slot.first));
        subAction->setShortcut(this->m_shortcutKey.getShortcutKey(slot.first));
        subAction->setShortcutVisibleInContextMenu(true);
        connect(subAction, &QAction::triggered, this->m_mainWindow, slot.second);
    }

    this->createDTVMenu();
    this->createRadioMenu();

    this->m_menu.addSeparator();

    subAction = this->m_menu.addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_ADD_TO_PLATLIST));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_ADD_TO_PLATLIST));
    subAction->setShortcutVisibleInContextMenu(true);
    connect(subAction, &QAction::triggered, this->m_mainWindow, (void (MainWindowInterface::*)())&MainWindowInterface::addToPlayList);

    subAction = this->m_menu.addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_OPEN_PLAY_LIST));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_OPEN_PLAY_LIST));
    subAction->setShortcutVisibleInContextMenu(true);
    subAction->setCheckable(true);
    subAction->setChecked(this->m_mainWindow->getPlayList().isVisible());
    connect(subAction, &QAction::triggered, this->m_mainWindow, &MainWindowInterface::openPlayList);

    subAction = this->m_menu.addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_CLOSE));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_CLOSE));
    subAction->setShortcutVisibleInContextMenu(true);
    subAction->setEnabled(this->m_player.isOpened());
    connect(subAction, &QAction::triggered, this->m_mainWindow, &MainWindowInterface::close);

    this->m_menu.addSeparator();

    this->createScreenMenu();
    this->createPlaybackMenu();
    this->createSubtitleMenu();
    this->createSoundMenu();

    this->m_menu.addSeparator();

    subAction = this->m_menu.addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_INFO));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_INFO));
    subAction->setShortcutVisibleInContextMenu(true);
    connect(subAction, &QAction::triggered, this->m_mainWindow, &MainWindowInterface::info);

    subAction = this->m_menu.addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_DETAIL));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_DETAIL));
    subAction->setShortcutVisibleInContextMenu(true);
    subAction->setEnabled(this->m_player.isEnabledVideo());
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player.isShowDetail());
    connect(subAction, &QAction::triggered, &this->m_playerDelegate, &MediaPlayerDelegate::detail);

    subAction = this->m_menu.addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_SHOW_CONTROL_BAR));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_SHOW_CONTROL_BAR));
    subAction->setShortcutVisibleInContextMenu(true);
    subAction->setEnabled(this->m_mainWindow->canShowControlBar());
    subAction->setCheckable(true);
    subAction->setChecked(this->m_mainWindow->getControlBar().isVisible());
    connect(subAction, &QAction::triggered, this->m_mainWindow, &MainWindowInterface::showControlBar);

    subAction = this->m_menu.addAction(this->m_shortcutKey.getShortcutKeyDesc(AnyVODEnums::SK_CLEAR_PLAYLIST));
    subAction->setShortcut(this->m_shortcutKey.getShortcutKey(AnyVODEnums::SK_CLEAR_PLAYLIST));
    subAction->setShortcutVisibleInContextMenu(true);
    subAction->setCheckable(true);
    subAction->setChecked(this->m_mainWindow->getPlayList().isClearPlayListOnExit());
    connect(subAction, &QAction::triggered, &this->m_mainWindow->getPlayList(), &PlayList::toggleClearPlayListOnExit);

    this->createDefaultLanguageMenu();
    this->createUpdateGapMenu();

    this->m_menu.addSeparator();

    const auto slotList2 =
    {
        qMakePair(AnyVODEnums::SK_LOAD_SETTINGS, &MainWindowInterface::loadSettingsToFile),
        qMakePair(AnyVODEnums::SK_SAVE_SETTINGS, &MainWindowInterface::saveSettingsToFile),
        qMakePair(AnyVODEnums::SK_NONE, (void (MainWindowInterface::*)())nullptr),
        qMakePair(AnyVODEnums::SK_OPEN_CUSTOM_SHORTCUTS, &MainWindowInterface::openCustomShortcut),
        qMakePair(AnyVODEnums::SK_OPEN_FILE_ASSOCIATION, &MainWindowInterface::openFileAssociation),
        qMakePair(AnyVODEnums::SK_NONE, (void (MainWindowInterface::*)())nullptr),
        qMakePair(AnyVODEnums::SK_EXIT, &MainWindowInterface::exit),
    };

    for (const auto &slot : slotList2)
    {
        if (slot.first == AnyVODEnums::SK_NONE)
        {
            this->m_menu.addSeparator();
        }
        else
        {
            subAction = this->m_menu.addAction(this->m_shortcutKey.getShortcutKeyDesc(slot.first));
            subAction->setShortcut(this->m_shortcutKey.getShortcutKey(slot.first));
            subAction->setShortcutVisibleInContextMenu(true);
            connect(subAction, &QAction::triggered, this->m_mainWindow, slot.second);
        }
    }
}

void ScreenContextMenu::addActionsFor3DVideo(QMenu *subMenu, const QVector<AnyVODEnums::Video3DMethod> &methodList) const
{
    AnyVODEnums::Video3DMethod method = this->m_player.get3DMethod();

    for (AnyVODEnums::Video3DMethod candidate : methodList)
    {
        if (candidate == AnyVODEnums::V3M_SEPARATOR)
        {
            subMenu->addSeparator();
        }
        else
        {
            QAction *subAction;

            if (candidate == AnyVODEnums::V3M_NONE)
            {
                subAction = subMenu->addAction(this->m_enums.get3DMethodDesc(candidate));
            }
            else
            {
                subAction = subMenu->addAction(QString("%1 (%2)")
                                               .arg(this->m_enums.get3DMethodDesc(candidate),
                                                    this->m_enums.get3DMethodCategoryDesc(candidate)));
            }

            subAction->setCheckable(true);
            subAction->setChecked(method == candidate);
            connect(subAction, &QAction::triggered, [this, candidate]() { this->m_playerDelegate.select3DMethod(candidate); });
        }
    }
}

template <typename T>
ScreenContextMenu::ActionAttribute<T> ScreenContextMenu::makeActionAttribute(AnyVODEnums::ShortcutKey key, T slot, bool checked, bool enabled) const
{
    return ActionAttribute<T>(qMakePair(key, slot), checked, enabled);
}

template <typename T>
ScreenContextMenu::ActionAttribute<T> ScreenContextMenu::makeActionAttribute(AnyVODEnums::ShortcutKey key, T slot, bool checked) const
{
    return this->makeActionAttribute(key, slot, checked, false);
}

template <typename T>
ScreenContextMenu::ActionAttributeWithKeys<T> ScreenContextMenu::makeActionAttributeWithKeys(QList<AnyVODEnums::ShortcutKey> keys, T slot, bool checked) const
{
    QList<QKeySequence> keySequence;

    for (AnyVODEnums::ShortcutKey key : keys)
        keySequence.append(this->m_shortcutKey.getShortcutKey(key));

    return ActionAttributeWithKeys<T>(qMakePair(keySequence, slot), this->m_shortcutKey.getShortcutKeyDesc(keys.first()), checked);
}

template <typename T>
ScreenContextMenu::ActionAttributeWithKeys<T> ScreenContextMenu::makeActionAttributeWithKeys(QList<AnyVODEnums::ShortcutKey> keys, T slot) const
{
    return this->makeActionAttributeWithKeys(keys, slot, false);
}
