﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "URLPickerInterface.h"

class QBuffer;
class QJsonObject;
class QUrlQuery;

class YouTubeURLPicker : public URLPickerInterface
{
public:
    YouTubeURLPicker();
    virtual ~YouTubeURLPicker();

    virtual bool isSupported(const QString &url) const;

    virtual QVector<URLPickerInterface::Item> pickURL(const QString &url);

    virtual bool getAudioStreamByMimeType(const QString &url, const QString &mimeType, QString *ret) const;
    virtual bool getProperAudioStreamByMimeType(const QVector<URLPickerInterface::Item> &items, const QString &mimeType, QString *ret) const;

    bool getJSon(const QString &content, const QString &id, QJsonObject *obj) const;

public:
    static const QString BASE_URL;
    static const QString INFO_URL;
    static const QString EMBED_URL;
    static const QString GOOGLE_API_URL;
    static const QString YOUTUBE_API_URL;
    static const QString OPTIONS;

private:
    static const QString MULTI_FEED_ITEM_SIG;

private:
    QString adjustURL(const QString &url) const;

    bool getVideoInfo(const QString &content, const QString &id, QString *ret) const;
    QVector<URLPickerInterface::Item> parseVideoContent(const QString &content, const QString &id, bool isMultiFeedItem);
    QVector<URLPickerInterface::Item> parsePlayListContent(const QString &url);
    QVector<URLPickerInterface::Item> parseEtcContent(const QString &url);
    QVector<URLPickerInterface::Item> getMultiFeedItems(const QJsonObject &multiCamera, const QString &id, const QString &title);

    QStringList getVideoIdsFromTabs(const QJsonObject &data) const;
    QStringList getVideoIdsFromPlayList(const QJsonObject &renderer) const;

    QUrlQuery buildContinuation(const QJsonObject &renderer) const;
    QUrlQuery buildContinuation(const QString &continuation, const QString &clickTrackingParams) const;

    QString decryptNCode(const QString &js, const QString &n) const;
    QString decryptSignature(const QString &sig, const QString &js) const;
    QString extractTitle(const QJsonObject &videoDetails, const QJsonObject &videoInfo, const QString &content) const;

    QString getMultiFeedItemOption() const;
    QString getOrgURL(const QString &url) const;
    QString getUrl(const QString &js, const QJsonObject &fmtItem) const;
    QUrlQuery getUrlData(const QJsonObject &fmtItem) const;
    QString getUrlWithNCode(const QString &js, const QString &url) const;

    QString getAPIUrl(const QString &type) const;
    QString getAPIBody(const QString &args, bool ageGated = false) const;

    bool getJS(const QString &content, QBuffer *js) const;
    bool findJSon(const QString &content, const QString &start, const QString &eval, QJsonObject *obj) const;
    QString getCipher(const QJsonObject &fmtItem) const;
    bool hasCipher(const QJsonObject &fmtItem) const;

private:
    bool m_recurseState;
};
