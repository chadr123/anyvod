﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "YouTubeURLPicker.h"
#include "net/HttpDownloader.h"
#include "utils/ConvertingUtils.h"

#include <QBuffer>
#include <QUrl>
#include <QUrlQuery>
#include <QJSEngine>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QXmlStreamReader>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QDebug>

const QString YouTubeURLPicker::BASE_URL = "https://www.youtube.com/watch?has_verified=1&bpctr=9999999999&v=";
const QString YouTubeURLPicker::EMBED_URL = "https://www.youtube.com/embed/";
const QString YouTubeURLPicker::INFO_URL = "https://www.youtube.com/get_video_info?html5=1&";
const QString YouTubeURLPicker::GOOGLE_API_URL = "https://youtube.googleapis.com/v/";
const QString YouTubeURLPicker::YOUTUBE_API_URL = "https://www.youtube.com/youtubei/v1/";
const QString YouTubeURLPicker::OPTIONS = "&disable_polymer=true";
const QString YouTubeURLPicker::MULTI_FEED_ITEM_SIG = "multi_feed_item_sig";

/*
 http://www.youtube.com/watch?v=BaW_jenozKc
 http://www.youtube.com/watch?v=UxxajLWwzqY
 http://www.youtube.com/watch?v=a9LDPn-MO4I
 https://www.youtube.com/watch?v=IB3lcPjvWLA
 https://www.youtube.com/watch?v=nfWlot6h_JM
 https://www.youtube.com/watch?v=T4XJQO3qol8
 http://youtube.com/watch?v=HtVdAasjOgU
 http://www.youtube.com/watch?v=__2ABJjxzNo
 http://www.youtube.com/watch?v=lqQg6PlCWgI
 https://www.youtube.com/watch?v=_b-2C3KPAM0
 https://www.youtube.com/watch?v=0LQuPmWc-nk
 https://www.youtube.com/watch?v=DNZUKm0ApEM
 https://www.youtube.com/watch?v=LGjt291COa0
 https://www.youtube.com/watch?v=UxxajLWwzqY
 https://www.youtube.com/watch?v=pqIv3e5eBeo
 https://www.youtube.com/watch?v=WMCIdxt-tF0
 https://www.youtube.com/watch?v=oL276M79mYE
 https://www.youtube.com/watch?v=coYw-eVU0Ks
 https://www.youtube.com/watch?v=mp_B4s9z_l0&list=PLyOJ5ZNIl-zBuedDlwSMrtg_3pK7EAaai
 https://www.youtube.com/ANNnewsCH/live
 https://www.youtube.com/watch?v=ZKo4mb8g6Gc
 --age-gate
 https://youtube.com/watch?v=Tq92D6wQ1mg
 https://www.youtube.com/watch?v=07FYdnEawAQ
 http://www.youtube.com/watch?v=6kLq3WMV1nU
 https://youtube.com/watch?v=HsUATh_Nc2U
*/

YouTubeURLPicker::YouTubeURLPicker() :
    m_recurseState(false)
{

}

YouTubeURLPicker::~YouTubeURLPicker()
{

}

bool YouTubeURLPicker::isSupported(const QString &url) const
{
    return url.contains("youtu", Qt::CaseInsensitive);
}

QVector<URLPickerInterface::Item> YouTubeURLPicker::pickURL(const QString &url)
{
    QString to = url;
    QVector<URLPickerInterface::Item> ret;

    to = this->adjustURL(to);

    if (to.isEmpty())
        return ret;

    QUrl u = QUrl(to);
    QUrlQuery q(u.query());
    QString list = q.queryItemValue("list");

    if (!list.isEmpty() && !to.contains("/playlist", Qt::CaseInsensitive))
        to = u.scheme() + "://" + u.host() + "/playlist?list=" + list;

    if (to.contains("/watch", Qt::CaseInsensitive))
    {
        HttpDownloader downloader;
        QBuffer buffer;

        if (!downloader.download(to + OPTIONS, buffer))
        {
            to = to.replace("https", "http", Qt::CaseInsensitive);

            downloader.download(to + OPTIONS, buffer);
        }

        QString org = buffer.buffer();
        QString content;
        QUrlQuery query(QUrl(to).query());
        QString id = query.queryItemValue("v");
        bool isMultiFeedItem = QVariant(query.queryItemValue(MULTI_FEED_ITEM_SIG)).toBool();

        if (this->getVideoInfo(org, id, &content))
            ret = this->parseVideoContent(content, id, isMultiFeedItem);
    }
    else if (to.contains("/playlist", Qt::CaseInsensitive))
    {
        this->m_playListTitle.clear();
        this->m_playListURL.clear();

        ret = this->parsePlayListContent(to);
    }
    else
    {
        ret = this->parseEtcContent(to);
    }

    this->m_recurseState = false;

    return ret;
}

QString YouTubeURLPicker::adjustURL(const QString &url) const
{
    QString id;
    QUrl u = QUrl(url);
    QUrlQuery q(u.query());
    QString list = q.queryItemValue("list");

    if (url.contains("/watch", Qt::CaseInsensitive) ||
            url.contains("/playlist", Qt::CaseInsensitive))
    {
        QString adjusted = url;
        int index = adjusted.indexOf("http");

        if (index > 0)
            adjusted = adjusted.mid(index);

        return adjusted;
    }
    else if (url.contains("/v/", Qt::CaseInsensitive) ||
             url.contains("/embed/", Qt::CaseInsensitive) ||
             url.contains("youtu.be", Qt::CaseInsensitive))
    {
        int index = url.lastIndexOf("/") + 1;

        id = url.mid(index);
    }
    else if (url.endsWith("/live", Qt::CaseInsensitive))
    {
        return url;
    }
    else
    {
        return id;
    }

    return BASE_URL + id + (!list.isEmpty() ? "&list=" + list : QString());
}

QVector<URLPickerInterface::Item> YouTubeURLPicker::parseVideoContent(const QString &content, const QString &id, bool isMultiFeedItem)
{
    QVector<URLPickerInterface::Item> ret;
    QJsonObject json;

    if (!this->getJSon(content, id, &json))
        return ret;

    QJsonObject videoInfo = json["args"].toObject();
    QJsonObject playerResponse = QJsonDocument::fromJson(videoInfo["player_response"].toString().toUtf8()).object();
    QJsonObject videoDetails = playerResponse["videoDetails"].toObject();
    QJsonObject multiCamera = playerResponse["multicamera"].toObject();
    QString title = this->extractTitle(videoDetails, videoInfo, content);

    if (!isMultiFeedItem && !multiCamera.isEmpty())
        return this->getMultiFeedItems(multiCamera, id, title);

    QJsonObject streamingData = playerResponse["streamingData"].toObject();
    const QJsonArray fmtMap = streamingData["formats"].toArray();
    const QJsonArray adaptiveFmt = streamingData["adaptiveFormats"].toArray();
    bool isLive = videoInfo["livestream"].toString().trimmed() == "1" ||
                  videoInfo["live_playback"].toString().trimmed() == "1" ||
                  videoDetails["isLive"].toBool();

    if (!isLive)
    {
        QBuffer js;

        if (!this->getJS(content, &js))
        {
            HttpDownloader d;
            QBuffer embeded;

            if (!d.download(EMBED_URL + id, embeded))
                return ret;

            if (!this->getJS(embeded.buffer(), &js))
                return ret;
        }

        for (const QJsonValue &value : fmtMap)
        {
            URLPickerInterface::Item item;
            QJsonObject fmtItem = value.toObject();
            QString url = this->getUrl(js.buffer(), fmtItem);
            QString quality = fmtItem["quality"].toString();
            QString itag = QString::number(fmtItem["itag"].toInt());
            QString mimeType = fmtItem["mimeType"].toString();
            QStringList mimeTypeList = mimeType.split(";", Qt::SkipEmptyParts);

            if (mimeTypeList.length() == 2)
            {
                QString mime = mimeTypeList[0].toLatin1();
                QString codec = mimeTypeList[1].remove("codecs").remove("=").remove("\"").replace(",", "/").trimmed();

                item.desc = quality + "(" + mime.split("/", Qt::SkipEmptyParts).at(1) + ", " + codec + ")";
                item.shortDesc = item.desc;
                item.mimeType = mime;
            }

            if (this->hasCipher(fmtItem))
            {
                QUrlQuery q = this->getUrlData(fmtItem);
                QString decryptedSig = q.queryItemValue("sig");
                QString sigName = q.queryItemValue("sp");

                if (sigName.isEmpty())
                    sigName = "signature";

                if (decryptedSig.isEmpty())
                {
                    QString encryptedSig = q.queryItemValue("s");

                    decryptedSig = this->decryptSignature(encryptedSig, js.buffer());
                }

                if (!decryptedSig.isEmpty())
                    url += "&" + sigName + "=" + decryptedSig;
            }

            item.title = title;
            item.orgUrl = this->getOrgURL(BASE_URL + id);
            item.url = url;
            item.id = id;
            item.quality = quality + itag;
            item.noAudio = false;

            ret.append(item);
        }

        std::sort(ret.begin(), ret.end());

        QString dashmpd;

        if (!adaptiveFmt.isEmpty())
        {
            dashmpd = videoInfo["dashmpd"].toString();

            if (dashmpd.isEmpty())
                dashmpd = streamingData["dashManifestUrl"].toString();

            if (dashmpd.isEmpty())
            {
                HttpDownloader d;
                QBuffer data;
                QUrlQuery query;
                int sts = json["sts"].toInt(-1);

                if (sts < 0)
                    sts = json["sts"].toString("-1").toInt();

                if (sts < 0)
                {
                    QRegularExpression exp(R"(sts\s*:\s*(\d+))");
                    QRegularExpressionMatch match = exp.match(js.buffer());

                    if (match.hasMatch())
                    {
                        bool ok = false;

                        sts = match.captured(1).toInt(&ok);

                        if (!ok)
                            sts = -1;
                    }
                }

                if (sts >= 0)
                    query.addQueryItem("sts", QString::number(sts));

                query.addQueryItem("video_id", id);
                query.addQueryItem("eurl", GOOGLE_API_URL + id);

                if (d.download(INFO_URL + query.query(), data))
                {
                    query.setQuery(data.buffer());

                    dashmpd = QUrl::fromPercentEncoding(query.queryItemValue("dashmpd").toLatin1());
                }
            }

            QString sig;
            QString sigPattern = R"(/s/([\w\.]+))";
            QRegularExpression sigExp(sigPattern);
            QRegularExpressionMatch match = sigExp.match(dashmpd);

            if (match.hasMatch())
            {
                sig = match.captured(1);
                sig = this->decryptSignature(sig, js.buffer());

                dashmpd = dashmpd.replace(QRegularExpression(sigPattern), "/signature/" + sig);
            }
        }

        for (const QJsonValue &value : adaptiveFmt)
        {
            QJsonObject fmtItem = value.toObject();
            QString url = this->getUrl(js.buffer(), fmtItem);

            if (fmtItem["type"].toString() == "FORMAT_STREAM_TYPE_OTF")
                continue;

            URLPickerInterface::Item item;
            QString quality = fmtItem["qualityLabel"].toString();
            QString itag = QString::number(fmtItem["itag"].toInt());
            QString mimeType = fmtItem["mimeType"].toString();
            QStringList mimeTypeList = mimeType.split(";", Qt::SkipEmptyParts);
            unsigned long long bitrate = fmtItem["bitrate"].toInt();

            if (mimeTypeList.length() == 2)
            {
                QString mime = mimeTypeList[0].toLatin1();
                QString codec = mimeTypeList[1].remove("codecs").remove("=").remove("\"").replace(",", "/").trimmed();
                QStringList types = mime.split("/", Qt::SkipEmptyParts);
                QString shortCodec = codec.split(".").first();

                if (types[0] == "video")
                {
                    QJsonObject colorInfo = fmtItem["colorInfo"].toObject();
                    QString primaries = colorInfo["primaries"].toString();
                    QString size = QString("%1x%2").arg(fmtItem["width"].toInt()).arg(fmtItem["height"].toInt());
                    QString fps = QString::number(fmtItem["fps"].toInt()) + "fps";
                    QString hdr = primaries.toUpper() == "COLOR_PRIMARIES_BT2020" ? "HDR " : "";

                    item.desc = QString("%1 %2 %3 %4 %5(%6, %7)")
                            .arg(types[0], ConvertingUtils::sizeToString(bitrate), size, fps, hdr, types[1], codec);
                    item.shortDesc = QString("%1 %2 %3 %4 %5(%6, %7)")
                            .arg(types[0], ConvertingUtils::sizeToString(bitrate), size, fps, hdr, types[1], shortCodec);
                }
                else
                {
                    item.desc = QString("%1 %2 (%3, %4)")
                            .arg(types[0], ConvertingUtils::sizeToString(bitrate), types[1], codec);
                    item.shortDesc = QString("%1 %2 (%3, %4)")
                            .arg(types[0], ConvertingUtils::sizeToString(bitrate), types[1], shortCodec);
                }

                item.mimeType = mime;
            }

            if (this->hasCipher(fmtItem))
            {
                QUrlQuery q = this->getUrlData(fmtItem);
                QString encryptedSig = q.queryItemValue("s");
                QString sigName = q.queryItemValue("sp");

                if (sigName.isEmpty())
                    sigName = "signature";

                if (encryptedSig.isEmpty())
                    encryptedSig = q.queryItemValue("sig");

                encryptedSig = this->decryptSignature(encryptedSig, js.buffer());

                if (!encryptedSig.isEmpty())
                    url += "&" + sigName + "=" + encryptedSig;
            }

            if (!url.contains("ratebypass"))
                url += "&ratebypass=yes";

            item.title = title;
            item.orgUrl = this->getOrgURL(BASE_URL + id);
            item.url = url;
            item.id = id;
            item.dashmpd = dashmpd;
            item.quality = quality + itag;
            item.noAudio = true;

            ret.append(item);
        }
    }

    if (ret.isEmpty())
    {
        QString url = streamingData["hlsManifestUrl"].toString();

        if (url.isEmpty())
        {
            QString previewID = videoInfo["ypc_vid"].toString();

            if (!previewID.isEmpty() && !this->m_recurseState)
            {
                this->m_recurseState = true;
                ret = this->pickURL(BASE_URL + previewID);
            }
        }
        else
        {
            URLPickerInterface::Item item;

            url = url.replace("\\/", "/").replace("\\u0026", "&");

            item.title = title;
            item.orgUrl = this->getOrgURL(BASE_URL + id);
            item.id = id;

            ret.clear();
            this->getFormatListFromM3U8(url, true, item, &ret);
        }
    }

    if (isMultiFeedItem)
    {
        for (URLPickerInterface::Item &item : ret)
            item.orgUrl += this->getMultiFeedItemOption();
    }

    this->m_isPlayList = false;

    return ret;
}

QString YouTubeURLPicker::extractTitle(const QJsonObject &videoDetails, const QJsonObject &videoInfo, const QString &content) const
{
    QString title = videoInfo["title"].toString().trimmed();

    if (title.isEmpty())
        title = videoDetails["title"].toString();

    if (title.isEmpty())
    {
        QStringList patterns;

        patterns.append(R"(<meta\s+name\s*=\s*["]?title["]?\s*content\s*=\s*["]?(.*[^"])["]?\s*>)");
        patterns.append(R"(<meta\s+property\s*=\s*["]?og:title["]?\s*content\s*=\s*["]?(.*[^"])["]?\s*>)");

        for (const QString &pattern : patterns)
        {
            QRegularExpression exp(pattern, QRegularExpression::InvertedGreedinessOption);
            QRegularExpressionMatch match = exp.match(content);

            if (match.hasMatch())
            {
                title = match.captured(1).trimmed();
                title = title.remove("\"").remove('\r').remove('\n');

                title = this->escapeHtml(title);

                break;
            }
        }
    }

    return title;
}

QVector<URLPickerInterface::Item> YouTubeURLPicker::parsePlayListContent(const QString &url)
{
    QVector<URLPickerInterface::Item> ret;
    HttpDownloader downloader;
    QBuffer mainPage;

    if (!downloader.download(url + OPTIONS, mainPage))
        return ret;

    QString data = mainPage.buffer();
    QRegularExpression exp(R"(<title>(.*)<\/title>)");
    QRegularExpressionMatch match = exp.match(data);

    if (match.hasMatch())
    {
        QString title = match.captured(1).trimmed();

        title.remove('\r').remove('\n');

        this->m_playListTitle = this->escapeHtml(title);
    }

    QJsonObject initObject;

    if (!this->findJSon(data, "var ytInitialData", "ytInitialData", &initObject))
        return ret;

    QStringList vidList = this->getVideoIdsFromTabs(initObject);

    for (const QString &vid : qAsConst(vidList))
    {
        URLPickerInterface::Item item;

        item.url = BASE_URL + vid;
        item.orgUrl = this->getOrgURL(item.url);
        item.id = vid;

        ret.append(item);
    }

    this->m_isPlayList = true;
    this->m_playListURL = url;

    return ret;
}

QVector<URLPickerInterface::Item> YouTubeURLPicker::parseEtcContent(const QString &url)
{
    HttpDownloader downloader;
    QBuffer mainPage;

    if (!downloader.download(url, mainPage))
        return {};

    QString data = mainPage.buffer();
    QJsonObject initObject;

    if (!this->findJSon(data, "var ytInitialData", "ytInitialData", &initObject))
        return {};

    QString newVid = initObject["currentVideoEndpoint"].toObject()["watchEndpoint"].toObject()["videoId"].toString();

    if (newVid.isEmpty())
        return {};

    QUrl u = QUrl(url);

    return this->pickURL(u.scheme() + "://" + u.host() + "/watch?v=" + newVid);
}

QString YouTubeURLPicker::decryptSignature(const QString &sig, const QString &js) const
{
    QString ret;
    QString funcName;
    QStringList funcSigPatterns;
    QJSEngine script;

    funcSigPatterns.append(R"(\b[cs]\s*&&\s*[adf]\.set\([^,]+\s*,\s*encodeURIComponent\s*\(\s*([a-zA-Z0-9]+)\()");
    funcSigPatterns.append(R"(\b[a-zA-Z0-9]+\s*&&\s*[a-zA-Z0-9]+\.set\([^,]+\s*,\s*encodeURIComponent\s*\(\s*([a-zA-Z0-9]+)\()");
    funcSigPatterns.append(R"(\bm=([a-zA-Z0-9]{2,})\(decodeURIComponent\(h\.s\)\))");
    funcSigPatterns.append(R"(\bc&&\(c=([a-zA-Z0-9]{2,})\(decodeURIComponent\(c\)\))");
    funcSigPatterns.append(R"(\b([a-zA-Z0-9]{2,})\s*=\s*function\(\s*a\s*\)\s*{\s*a\s*=\s*a\.split\(\s*""\s*\);[a-zA-Z0-9]{2}\.[a-zA-Z0-9]{2}\(a,\d+\))");
    funcSigPatterns.append(R"(\b([a-zA-Z0-9]{2,})\s*=\s*function\(\s*a\s*\)\s*{\s*a\s*=\s*a\.split\(\s*""\s*\))");
    funcSigPatterns.append(R"(([a-zA-Z0-9]+)\s*=\s*function\(\s*a\s*\)\s*{\s*a\s*=\s*a\.split\(\s*""\s*\))");
    // Obsolete patterns
    funcSigPatterns.append(R"((["'])signature\1\s*,\s*([a-zA-Z0-9]+)\()");
    funcSigPatterns.append(R"(\.sig\|\|([a-zA-Z0-9]+)\()");
    funcSigPatterns.append(R"(yt\.akamaized\.net/\)\s*\|\|\s*.*?\s*[cs]\s*&&\s*[adf]\.set\([^,]+\s*,\s*(?:encodeURIComponent\s*\()?\s*([a-zA-Z0-9]+)\()");
    funcSigPatterns.append(R"(\b[cs]\s*&&\s*[adf]\.set\([^,]+\s*,\s*([a-zA-Z0-9]+)\()");
    funcSigPatterns.append(R"(\b[a-zA-Z0-9]+\s*&&\s*[a-zA-Z0-9]+\.set\([^,]+\s*,\s*([a-zA-Z0-9]+)\()");
    funcSigPatterns.append(R"(\bc\s*&&\s*a\.set\([^,]+\s*,\s*\([^)]*\)\s*\(\s*([a-zA-Z0-9]+)\()");
    funcSigPatterns.append(R"(\bc\s*&&\s*[a-zA-Z0-9]+\.set\([^,]+\s*,\s*\([^)]*\)\s*\(\s*([a-zA-Z0-9]+)\()");

    for (const QString &funcSig : funcSigPatterns)
    {
        QRegularExpression exp(funcSig);
        QRegularExpressionMatch match = exp.match(js);

        if (!match.hasMatch())
            continue;

        funcName = match.captured(1);

        break;
    }

    if (funcName.isEmpty())
        return ret;

    QJSValue func;
    QJSValueList v;
    QString decCode;
    //"var Go=function(a){a=a.split(\"\");Fo.sH(a,2);Fo.TU(a,28);Fo.TU(a,44);Fo.TU(a,26);Fo.TU(a,40);Fo.TU(a,64);Fo.TR(a,26);Fo.sH(a,1);return a.join(\"\")};";
    QStringList funcPatterns;
    QString funcCode;
    //var Fo={TR:function(a){a.reverse()},TU:function(a,b){var c=a[0];a[0]=a[b%a.length];a[b]=c},sH:function(a,b){a.splice(0,b)}};
    QString helperPattern = R"([ ,]%1=\{[\s\w\d;`~!@#$%^&*_,\.\-:=\"\{\}\[\]\(\)\?]+\};)";
    QString helperCode;
    QString helperName;

    funcPatterns.append(R"([ ,]%1=function\([\w,]+\)\{[\s\w\d;`~!@#$%^&*_,\.\-:=\"\{\}\[\]\(\)\?]+\}[ ,])");
    funcPatterns.append(R"(%1=function\([\w,]+\)\{[\s\w\d;`~!@#$%^&*_,\.\-:=\"\{\}\[\]\(\)\?]+\};)");
    funcPatterns.append(R"(function %1\([\w]+\)\{[\s\w\d;`~!@#$%^&*_,\.\-:=\"\{\}\[\]\(\)\?]+\};)");

    for (const QString &pattern : funcPatterns)
    {
        QRegularExpression exp(pattern.arg(QRegularExpression::escape(funcName)),
                               QRegularExpression::InvertedGreedinessOption);
        QRegularExpressionMatch match = exp.match(js);

        if (!match.hasMatch())
            continue;

        funcCode = match.captured();
        funcCode = funcCode.mid(funcCode.indexOf(funcName));

        if (funcCode.endsWith(','))
        {
            funcCode = funcCode.mid(0, funcCode.length() - 1);
            funcCode.append(";");
        }

        break;
    }

    QRegularExpression exp;
    QRegularExpressionMatch match;

    exp.setPattern(R"(;(..)\...\()");
    match = exp.match(funcCode);

    if (!match.hasMatch())
        return ret;

    helperName = match.captured(1);

    exp.setPattern(helperPattern.arg(QRegularExpression::escape(helperName)));
    exp.setPatternOptions(QRegularExpression::InvertedGreedinessOption);
    match = exp.match(js);

    if (!match.hasMatch())
        return ret;

    helperCode = match.captured();

    decCode = helperCode + funcCode;

    v << sig;

    script.evaluate(decCode);
    func = script.evaluate(funcName);

    ret = func.call(v).toString();

    return ret;
}

bool YouTubeURLPicker::getVideoInfo(const QString &content, const QString &id, QString *ret) const
{
    bool ok = true;
    QJsonObject json;

    if (!this->getJSon(content, id, &json))
        return ret;

    QJsonObject videoInfo = json["args"].toObject();
    QJsonObject playerResponse = QJsonDocument::fromJson(videoInfo["player_response"].toString().toUtf8()).object();
    bool isAgeGate = playerResponse["playabilityStatus"].toObject()["status"].toString() == "LOGIN_REQUIRED";

    if (isAgeGate)
    {
        HttpDownloader d;
        QBuffer responseData;
        QPair<QByteArray, QByteArray> header("content-type", "application/json");

        if (!d.download(this->getAPIUrl("player"), responseData, {header}, this->getAPIBody(QString(R"("videoId": "%1")").arg(id), true).toUtf8()))
            return false;

        QString playerResponse = responseData.buffer();

        if (playerResponse.isEmpty())
        {
            playerResponse = "\"player_response\":\"{}\"";
        }
        else
        {
            QJsonDocument doc;
            QJsonObject object;

            object["player_response"] = playerResponse;
            doc.setObject(object);

            playerResponse = QString::fromUtf8(doc.toJson(QJsonDocument::Compact));
        }

        QRegularExpression exp(R"(<meta\s+name\s*=\s*["]?title["]?\s*content\s*=\s*["]?.*[^"]["]?\s*>)", QRegularExpression::InvertedGreedinessOption);
        QRegularExpressionMatch match = exp.match(content);
        QString title;

        if (match.hasMatch())
            title = match.captured();

        QString total;

        total = R"(<script>
                var ytplayer = ytplayer || {};ytplayer.config =
                {
                  "args": %1
                };
                </script>)";
        total = total.arg(playerResponse);

        *ret = total + title;
    }
    else
    {
       *ret = content;
    }

    return ok;
}

QVector<URLPickerInterface::Item> YouTubeURLPicker::getMultiFeedItems(const QJsonObject &multiCamera, const QString &id, const QString &title)
{
    QVector<URLPickerInterface::Item> ret;
    const QStringList metadataList = multiCamera["playerLegacyMulticameraRenderer"].toObject()["metadataList"].toString().split(",");

    for (const QString &metadata : metadataList)
    {
        QUrlQuery query(metadata);
        QString feedID = query.queryItemValue("id");
        URLPickerInterface::Item item;

        item.url = BASE_URL + feedID + this->getMultiFeedItemOption();
        item.orgUrl = this->getOrgURL(item.url) + this->getMultiFeedItemOption();
        item.id = feedID;

        ret.append(item);
    }

    this->m_playListTitle = title;
    this->m_playListURL = this->getOrgURL(BASE_URL + id);
    this->m_isPlayList = true;

    return ret;
}

QStringList YouTubeURLPicker::getVideoIdsFromPlayList(const QJsonObject &renderer) const
{
    QStringList vidList;
    const QJsonArray contents = renderer["contents"].toArray();

    for (const QJsonValue &value : contents)
    {
        QJsonObject renderer = value.toObject()["playlistVideoRenderer"].toObject();

        if (renderer.isEmpty())
            renderer = value.toObject()["playlistPanelVideoRenderer"].toObject();

        if (!renderer.isEmpty())
            vidList.append(renderer["videoId"].toString());
    }

    return vidList;
}

QStringList YouTubeURLPicker::getVideoIdsFromTabs(const QJsonObject &data) const
{
    QStringList vidList;
    const QJsonArray tabs = data["contents"].toObject()["twoColumnBrowseResultsRenderer"].toObject()["tabs"].toArray();
    QJsonObject selectedTabRenderer;

    for (const QJsonValue &value : tabs)
    {
        QJsonObject tabRenderer = value.toObject()["tabRenderer"].toObject();

        if (tabRenderer["selected"].toBool())
        {
            selectedTabRenderer = tabRenderer;
            break;
        }
    }

    if (selectedTabRenderer.isEmpty())
        return vidList;

    QJsonObject sectionListRenderer = selectedTabRenderer["content"].toObject()["sectionListRenderer"].toObject();

    if (sectionListRenderer.isEmpty())
        return vidList;

    QUrlQuery continuation;
    const QJsonArray contents = sectionListRenderer["contents"].toArray();

    for (const QJsonValue &value : contents)
    {
        QJsonObject itemSectionRenderer = value.toObject()["itemSectionRenderer"].toObject();

        if (itemSectionRenderer.isEmpty())
            continue;

        const QJsonArray contents = itemSectionRenderer["contents"].toArray();

        for (const QJsonValue &value : contents)
        {
            QJsonObject renderer = value.toObject()["playlistVideoListRenderer"].toObject();

            if (!renderer.isEmpty())
            {
                vidList += this->getVideoIdsFromPlayList(renderer);
                continuation = this->buildContinuation(renderer);
                break;
            }
        }

        if (continuation.isEmpty())
            continuation = this->buildContinuation(itemSectionRenderer);
    }

    if (continuation.isEmpty())
        continuation = this->buildContinuation(sectionListRenderer);

    HttpDownloader d;

    while (!continuation.isEmpty())
    {
        QPair<QByteArray, QByteArray> clientName("x-youtube-client-name", "1");
        QPair<QByteArray, QByteArray> clientVersion("x-youtube-client-version", "2.20201112.04.01");
        QPair<QByteArray, QByteArray> contentType("content-type", "application/json");
        QBuffer data;
        int count = 0;
        const int retries = 3;
        QNetworkReply::NetworkError error;
        QString args(
            R"(
                "continuation": "%1",
                "clickTracking": {
                    "clickTrackingParams": "%2"
                }
            )"
        );
        QByteArray body = this->getAPIBody(args.arg(continuation.queryItemValue("continuation"), continuation.queryItemValue("itct"))).toUtf8();

        do
        {
            d.download(this->getAPIUrl("browse"), data, {clientName, clientVersion, contentType}, body);
            error = d.error();

            if (error == QNetworkReply::NoError)
                break;

            count++;
        } while (count <= retries);

        if (error != QNetworkReply::NoError)
            break;

        QJsonObject response = QJsonDocument::fromJson(data.buffer()).object();

        if (response.isEmpty())
            break;

        QJsonObject continuationContents = response["continuationContents"].toObject();

        if (!continuationContents.isEmpty())
        {
            QJsonObject renderer = continuationContents["playlistVideoListContinuation"].toObject();

            if (!renderer.isEmpty())
            {
                vidList += this->getVideoIdsFromPlayList(renderer);
                continuation = this->buildContinuation(renderer);
            }
        }

        QJsonArray onResponseReceivedActions = response["onResponseReceivedActions"].toArray();

        if (!onResponseReceivedActions.isEmpty())
        {
            QJsonArray continuationItems = onResponseReceivedActions[0].toObject()["appendContinuationItemsAction"].toObject()["continuationItems"].toArray();

            if (!continuationItems.isEmpty())
            {
                QJsonObject continuationItem = continuationItems[0].toObject();
                QJsonObject renderer = continuationItem["playlistVideoRenderer"].toObject();

                if (renderer.isEmpty())
                    renderer = continuationItem["playlistPanelVideoRenderer"].toObject();

                if (!renderer.isEmpty())
                {
                    renderer["contents"] = continuationItems;

                    vidList += this->getVideoIdsFromPlayList(renderer);
                    continuation = this->buildContinuation(renderer);
                }
            }
        }
    }

    return vidList;
}

QUrlQuery YouTubeURLPicker::buildContinuation(const QJsonObject &renderer) const
{
    QJsonArray continuations = renderer["continuations"].toArray();

    if (!continuations.isEmpty())
    {
        QJsonObject nextContinuation = continuations[0].toObject()["nextContinuationData"].toObject();

        return this->buildContinuation(nextContinuation["continuation"].toString(), nextContinuation["clickTrackingParams"].toString());
    }

    const QJsonArray list = renderer["contents"].toArray() + renderer["items"].toArray();

    for (const QJsonValue &value : list)
    {
        QJsonObject endpoint = value.toObject()["continuationItemRenderer"].toObject()["continuationEndpoint"].toObject();

        if (endpoint.isEmpty())
            continue;

        return this->buildContinuation(endpoint["continuationCommand"].toObject()["token"].toString(), endpoint["clickTrackingParams"].toString());
    }

    return QUrlQuery();
}

QUrlQuery YouTubeURLPicker::buildContinuation(const QString &continuation, const QString &clickTrackingParams) const
{
    QUrlQuery query;

    if (continuation.isEmpty())
        return query;

    query.addQueryItem("ctoken", continuation);
    query.addQueryItem("continuation", continuation);

    if (!clickTrackingParams.isEmpty())
        query.addQueryItem("itct", clickTrackingParams);

    return query;
}

QString YouTubeURLPicker::decryptNCode(const QString &js, const QString &n) const
{
    QString ret;
    QString funcName;
    QStringList funcNCodePatterns;
    int funcIndex;

    funcNCodePatterns.append(R"(\s*&&\s*\(b\s*=\s*a\.get\(\s*"n"\s*\)\s*\)\s*&&\s*\(\s*b\s*=([a-zA-Z0-9]+)(\[(?<index>\d+)\])?\(\s*[a-zA-Z0-9]\s*\))");

    for (const QString &funcNCode : funcNCodePatterns)
    {
        QRegularExpression exp(funcNCode);
        QRegularExpressionMatch match = exp.match(js);

        if (!match.hasMatch())
            continue;

        bool okIndex;
        funcIndex = match.captured("index").toInt(&okIndex);
        funcName = match.captured(1);

        if (okIndex)
        {
            QRegularExpression funcListExp(QString(R"(var %1\s*=\s*\[(.+?)\];)").arg(QRegularExpression::escape(funcName)));
            QRegularExpressionMatch funcListMatch = funcListExp.match(js);

            if (!funcListMatch.hasMatch())
                continue;

            QStringList funcList = funcListMatch.captured(1).split(",");

            if (funcIndex <= funcList.size() - 1)
                funcName = funcList[funcIndex].trimmed();
        }

        break;
    }

    if (funcName.isEmpty())
        return ret;

    QJSValue func;
    QJSValueList v;
    QStringList funcPatterns;
    QString funcCode;

    funcPatterns.append(R"(%1=function\([\w,]+\)\{[\s\w\d;`+~!@#$%^&*_,\.\-:=\"\{\}\[\]\(\)\?]+\};)");

    for (const QString &pattern : funcPatterns)
    {
        QRegularExpression exp(pattern.arg(QRegularExpression::escape(funcName)),
                               QRegularExpression::InvertedGreedinessOption);
        QRegularExpressionMatch match = exp.match(js);

        if (!match.hasMatch())
            continue;

        funcCode = match.captured();
        funcCode = funcCode.mid(funcCode.indexOf(funcName));

        if (funcCode.endsWith(','))
        {
            funcCode = funcCode.mid(0, funcCode.length() - 1);
            funcCode.append(";");
        }

        break;
    }

    v << n;

    QJSEngine script;

    script.evaluate(funcCode);
    func = script.evaluate(funcName);

    ret = func.call(v).toString();

    return ret;
}

QString YouTubeURLPicker::getOrgURL(const QString &url) const
{
    QUrl orgUrl(url);
    QUrlQuery query(orgUrl.query());
    QString v = query.queryItemValue("v");

    query.clear();
    query.addQueryItem("v", v);

    orgUrl.setQuery(query);

    return orgUrl.toString();
}

QString YouTubeURLPicker::getMultiFeedItemOption() const
{
    return "&" + MULTI_FEED_ITEM_SIG + "=true";
}

bool YouTubeURLPicker::getJS(const QString &content, QBuffer *js) const
{
    QString jsUrl;
    HttpDownloader d;
    QStringList patterns;

    patterns.append(R"("jsUrl"\s*:\s*("[^"]+"))");
    patterns.append(R"(<script[^>]+\bsrc=("[^"]+")[^>]+\bname=["\']player_ias/base)");
    patterns.append(R"("assets":.+?"js":\s*("[^"]+"))");

    for (const QString &pattern : patterns)
    {
        QRegularExpression exp(pattern);
        QRegularExpressionMatch match = exp.match(content);

        if (!match.hasMatch())
            continue;

        jsUrl = match.captured(1);
        break;
    }

    jsUrl.replace("\\/", "/");
    jsUrl.replace("\"", "");
    jsUrl.replace("'", "");

    if (jsUrl.startsWith('/'))
        jsUrl.prepend("//www.youtube.com");

    jsUrl.prepend("https:");

    return d.download(jsUrl, *js);
}

bool YouTubeURLPicker::findJSon(const QString &content, const QString &start, const QString &eval, QJsonObject *obj) const
{
    int index;
    int end;

    index = content.indexOf(start);

    if (index < 0)
        return false;

    end = content.indexOf("</script>", index);

    if (end < 0)
        return false;

    QString js = content.mid(index, end - index);
    QJSEngine script;
    QJSValue ytplayer;

    script.evaluate(js);
    ytplayer = script.evaluate(eval);

    QVariant json = ytplayer.toVariant();
    QJsonDocument doc = QJsonDocument::fromVariant(json);

    if (!doc.isObject())
        return false;

    *obj = doc.object();

    return true;
}

QString YouTubeURLPicker::getUrl(const QString &js, const QJsonObject &fmtItem) const
{
    QString url = fmtItem["url"].toString();

    if (!url.isEmpty())
        return this->getUrlWithNCode(js, url);

    QString cipher = this->getCipher(fmtItem);

    if (cipher.isEmpty())
        return url;

    QUrlQuery urlData = this->getUrlData(fmtItem);

    url = QUrl::fromPercentEncoding(urlData.queryItemValue("url").toLatin1());

    return this->getUrlWithNCode(js, url);
}

QUrlQuery YouTubeURLPicker::getUrlData(const QJsonObject &fmtItem) const
{
    QString url = fmtItem["url"].toString();

    if (url.isEmpty())
        url = this->getCipher(fmtItem);

    return this->hasCipher(fmtItem) ? QUrlQuery(url) : QUrlQuery(QUrl(url));
}

QString YouTubeURLPicker::getUrlWithNCode(const QString &js, const QString &url) const
{
    QUrl orgUrl(url);
    QUrlQuery query(orgUrl.query());
    QString n = query.queryItemValue("n");

    if (!n.isEmpty())
    {
        n = this->decryptNCode(js, n);

        query.removeQueryItem("n");
        query.addQueryItem("n", n);

        orgUrl.setQuery(query);
    }

    return orgUrl.toString();
}

QString YouTubeURLPicker::getAPIUrl(const QString &type) const
{
    return YOUTUBE_API_URL + type + "?key=AIzaSyAO_FJ2SlqU8Q4STEHLGCilw_Y9_11qcW8";
}

QString YouTubeURLPicker::getAPIBody(const QString &args, bool ageGated) const
{
    if (ageGated)
    {
        QString body(
            R"({
                "context": {
                    "client": {
                        "clientName": "ANDROID",
                        "clientVersion": "16.20",
                        "clientScreen": "EMBED",
                    },
                    "thirdParty": {
                        "embedUrl": "https://google.com"
                    }
                },
                %1
            })"
        );

        return body.arg(args);
    }
    else
    {
        QString body(
            R"({
                "context": {
                    "client": {
                        "clientName": "WEB",
                        "clientVersion": "2.20201021.03.00"
                    }
                },
                %1
            })"
        );

        return body.arg(args);
    }
}

QString YouTubeURLPicker::getCipher(const QJsonObject &fmtItem) const
{
    QString cipher = fmtItem["cipher"].toString().trimmed();

    if (cipher.isEmpty())
        cipher = fmtItem["signatureCipher"].toString().trimmed();

    return cipher;
}

bool YouTubeURLPicker::hasCipher(const QJsonObject &fmtItem) const
{
    return !this->getCipher(fmtItem).isEmpty();
}

bool YouTubeURLPicker::getAudioStreamByMimeType(const QString &url, const QString &mimeType, QString *ret) const
{
    HttpDownloader d;
    QXmlStreamReader xml;
    QBuffer dash;
    QStringList videoMime = mimeType.split("/", Qt::SkipEmptyParts);

    if (videoMime.length() != 2)
        return false;

    if (videoMime[0].toLower() == "audio")
        return false;

    if (!dash.open(QIODevice::ReadWrite))
        return false;

    if (!d.download(url, dash))
        return false;

    if (!dash.reset())
        return false;

    xml.setDevice(&dash);

    QStringList audioUrls;

    while (!xml.atEnd())
    {
        if (!xml.readNextStartElement())
            continue;

        QString elem = xml.name().toString();

        if (elem != "AdaptationSet")
            continue;

        QXmlStreamAttributes attr = xml.attributes();
        QString mime = attr.value("mimeType").toString();
        QStringList audioMime = mime.split("/", Qt::SkipEmptyParts);

        if (audioMime.length() != 2)
            continue;

        if (audioMime[0].toLower() != "audio" || videoMime[1].toLower() != audioMime[1].toLower())
            continue;

        while (!xml.atEnd())
        {
            if (!xml.readNextStartElement())
                continue;

            elem = xml.name().toString();

            if (elem == "AdaptationSet")
                break;

            if (elem != "BaseURL")
                continue;

            audioUrls.append(xml.readElementText());
        }
    }

    dash.close();

    *ret = audioUrls.last();

    return true;
}

bool YouTubeURLPicker::getProperAudioStreamByMimeType(const QVector<URLPickerInterface::Item> &items, const QString &mimeType, QString *ret) const
{
    QStringList videoMime = mimeType.split("/", Qt::SkipEmptyParts);

    if (videoMime.length() != 2)
        return false;

    if (videoMime[0].toLower() == "audio")
        return false;

    QString other;

    for (const Item &item : items)
    {
        QStringList audioMime = item.mimeType.split("/", Qt::SkipEmptyParts);

        if (audioMime.length() != 2)
            continue;

        if (other.isEmpty() && audioMime[0].toLower() == "audio")
            other = item.url;

        if (audioMime[0].toLower() != "audio" || videoMime[1].toLower() != audioMime[1].toLower())
            continue;

        *ret = item.url;

        break;
    }

    if (ret->isEmpty())
        *ret = other;

    return true;
}

bool YouTubeURLPicker::getJSon(const QString &content, const QString &id, QJsonObject *obj) const
{
    if (this->findJSon(content, "var ytplayer", "ytplayer.config", obj))
        return true;

    if (this->findJSon(content, "var ytInitialPlayerResponse", "ytInitialPlayerResponse", obj))
    {
        QJsonObject newObj;
        QJsonObject videoInfo;
        QString playerResponse = QJsonDocument(*obj).toJson().trimmed();

        if (playerResponse.isEmpty())
        {
            HttpDownloader d;
            QBuffer data;
            QPair<QByteArray, QByteArray> header("content-type", "application/json");

            if (!d.download(this->getAPIUrl("player"), data, {header}, this->getAPIBody(QString(R"("videoId": "%1")").arg(id)).toUtf8()))
                return false;

            playerResponse = data.buffer();
        }

        videoInfo.insert("player_response", QJsonValue(playerResponse));
        newObj.insert("args", videoInfo);

        *obj = newObj;

        return true;
    }

    return false;
}
