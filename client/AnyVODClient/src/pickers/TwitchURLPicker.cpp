﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "TwitchURLPicker.h"
#include "net/HttpDownloader.h"

#include <QRegularExpression>
#include <QUrlQuery>
#include <QBuffer>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QRandomGenerator>
#include <QDebug>

const QString TwitchURLPicker::CLIENT_ID = "kimne78kx3ncx6brgo4mv6wki5h1ko";

const QString TwitchURLPicker::USHER_BASE = "https://usher.ttvnw.net/";
const QString TwitchURLPicker::GQL_BASE = "https://gql.twitch.tv/gql/";

//https://www.twitch.tv/videos/904906329
//https://www.twitch.tv/om0322/video/904906329
//https://www.twitch.tv/om0322/v/904906329
//http://player.twitch.tv/?video=v462712336
//https://clips.twitch.tv/FaintLightGullWholeWheat
//https://clips.twitch.tv/VictoriousTallMouseCopyThis
//https://www.twitch.tv/sergeynixon/clip/StormyThankfulSproutFutureMan
//https://www.twitch.tv/om0322
//https://www.twitch.tv/overwatchleague_kr
//https://www.twitch.tv/overwatchleague_kr?sr=a
//http://www.twitch.tv/overwatchleague_kr#profile-0
//https://player.twitch.tv/?channel=overwatchleague_kr
//http://www.twitch.tv/riotgames
//https://www.twitch.tv/kimmyungwun
//https://www.twitch.tv/videos/477092126
//https://www.twitch.tv/kimmyungwun/clip/SingleProudPistachioPogChamp?filter=clips&range=7d&sort=time
//https://clips.twitch.tv/embed?clip=InquisitiveBreakableYogurtJebaited
//https://www.twitch.tv/f1reaoe/clip/CautiousEnticingSardinePJSugar
//https://www.twitch.tv/SpeedrunHypeTV
//https://www.twitch.tv/reckful
//https://clips.twitch.tv/ShyAltruisticCheeseTooSpicy-T9tbKcc01auwU1a0

TwitchURLPicker::TwitchURLPicker()
{

}

TwitchURLPicker::~TwitchURLPicker()
{

}

bool TwitchURLPicker::isSupported(const QString &url) const
{
    return url.contains("twitch.tv", Qt::CaseInsensitive);
}

QVector<URLPickerInterface::Item> TwitchURLPicker::pickURL(const QString &url)
{
    QVector<URLPickerInterface::Item> items;
    QString to = url;
    QUrl u = QUrl(to);
    QUrlQuery q(u.query());
    int sharpIndex = to.indexOf("#");

    if (sharpIndex >= 0)
        to = to.mid(0, sharpIndex);

    if (to.contains("/videos/", Qt::CaseInsensitive))
    {
        QRegularExpression exp(R"(twitch\.tv/videos/(\d+))");
        QRegularExpressionMatch match = exp.match(to);

        if (match.hasMatch())
        {
            QString id = match.captured(1);

            items = this->getVideoList(to, this->removeQuery(id));
        }
    }
    else if (to.contains("/video/", Qt::CaseInsensitive))
    {
        QRegularExpression exp(R"(twitch\.tv/.*/video/(\d+))");
        QRegularExpressionMatch match = exp.match(to);

        if (match.hasMatch())
        {
            QString id = match.captured(1);

            items = this->getVideoList(to, this->removeQuery(id));
        }
    }
    else if (to.contains("/v/", Qt::CaseInsensitive))
    {
        QRegularExpression exp(R"(twitch\.tv/.*/v/(\d+))");
        QRegularExpressionMatch match = exp.match(to);

        if (match.hasMatch())
        {
            QString id = match.captured(1);

            items = this->getVideoList(to, this->removeQuery(id));
        }
    }
    else if (to.contains("player.twitch.tv", Qt::CaseInsensitive))
    {
        QString id = q.queryItemValue("video");

        if (id.isEmpty())
        {
            id = q.queryItemValue("channel");
            items = this->getStreamList(to, id);
        }
        else
        {
            items = this->getVideoList(to, id.mid(1));
        }
    }
    else if (to.contains("clips.twitch.tv", Qt::CaseInsensitive))
    {
        QRegularExpression exp(R"(twitch\.tv/(.*))");
        QRegularExpressionMatch match = exp.match(to);

        if (match.hasMatch())
        {
            QString id = match.captured(1);

            if (id.startsWith("embed?", Qt::CaseInsensitive))
                id = q.queryItemValue("clip");
            else
                id = this->removeQuery(id);

            items = this->getClipList(to, id);
        }
    }
    else if (to.contains("/clip/", Qt::CaseInsensitive))
    {
        QRegularExpression exp(R"(twitch\.tv/.*/clip/(.*))");
        QRegularExpressionMatch match = exp.match(to);

        if (match.hasMatch())
        {
            QString id = match.captured(1);

            items = this->getClipList(to, this->removeQuery(id));
        }
    }
    else
    {
        QRegularExpression exp(R"(twitch\.tv/(.*))");
        QRegularExpressionMatch match = exp.match(to);

        if (match.hasMatch())
        {
            QString id = match.captured(1);

            items = this->getStreamList(to, this->removeQuery(id));
        }
    }

    return items;
}

bool TwitchURLPicker::getAudioStreamByMimeType(const QString &url, const QString &mimeType, QString *ret) const
{
    (void)url;
    (void)mimeType;
    (void)ret;

    return false;
}

bool TwitchURLPicker::getProperAudioStreamByMimeType(const QVector<URLPickerInterface::Item> &items, const QString &mimeType, QString *ret) const
{
    (void)items;
    (void)mimeType;
    (void)ret;

    return false;
}

QJsonObject TwitchURLPicker::getAccessToken(const QString &type, const QString &name, const QString &id) const
{
    QString method = type + "PlaybackAccessToken";
    QString body(
        R"({"query": "{
                %1(
                    %2: \"%3\",
                        params: {
                            platform: \"_\",
                            playerBackend: \"mediaplayer\",
                            playerType: \"site\"
                        }
                  ) {
                    value
                    signature
                }
            }"
        })"
    );

    return this->downloadGQL(body.arg(method, name, id)).object()["data"].toObject()[method].toObject();
}

QJsonDocument TwitchURLPicker::downloadGQL(const QString &payload) const
{
    HttpDownloader d;
    QBuffer data;

    if (!d.download(GQL_BASE, data, {this->getClientIDHeader()}, QJsonDocument::fromJson(payload.toUtf8()).toJson()))
        return QJsonDocument();

    return QJsonDocument::fromJson(data.buffer());
}

QPair<QByteArray, QByteArray> TwitchURLPicker::getClientIDHeader() const
{
    return QPair<QByteArray, QByteArray>("Client-ID", CLIENT_ID.toLatin1());
}

QString TwitchURLPicker::removeQuery(const QString id) const
{
    int queryIndex = id.indexOf("?");

    if (queryIndex >= 0)
        return id.mid(0, queryIndex);
    else
        return id;
}

QVector<URLPickerInterface::Item> TwitchURLPicker::getVideoList(const QString &url, const QString &id)
{
    QVector<URLPickerInterface::Item> items;
    QString body(
        R"([{"operationName": "VideoMetadata",
             "variables": {"channelLogin": "",
                           "videoID": "%1"},
             "extensions": {"persistedQuery":
                                {"version": 1,
                                 "sha256Hash": "226edb3e692509f727fd56821f5653c05740242c82b0388883e0c0e75dcbf687"}
                           }
           }])"
    );
    QJsonObject info = this->downloadGQL(body.arg(id)).array().at(0)["data"]["video"].toObject();

    if (info.isEmpty())
        return items;

    QString title = info["title"].toString();
    QJsonObject accessToken = this->getAccessToken("video", "id", id);

    if (accessToken.isEmpty())
        return items;

    QUrlQuery query;
    URLPickerInterface::Item refItem;

    query.addQueryItem("allow_source", "true");
    query.addQueryItem("allow_audio_only", "true");
    query.addQueryItem("allow_spectre", "true");
    query.addQueryItem("player", "twitchweb");
    query.addQueryItem("nauthsig", accessToken["signature"].toString());
    query.addQueryItem("nauth", QUrl::toPercentEncoding(accessToken["value"].toString()));

    refItem.id = id;
    refItem.noAudio = false;
    refItem.orgUrl = url;
    refItem.title = title;

    this->getFormatListFromM3U8(USHER_BASE + "vod/" + id + ".m3u8?" + query.toString(QUrl::FullyEncoded), false, refItem, &items);

    for (URLPickerInterface::Item &item : items)
    {
        QRegularExpression exp(R"(\((.+)\))");
        QRegularExpressionMatch match = exp.match(item.desc);

        if (match.hasMatch())
        {
            QString cap = match.captured(1);
            QString codec = cap.split(",").first().split(".").first();

            item.desc.replace(exp, "(" + codec + ")");
        }

        item.desc.replace("  ", " ");
        item.shortDesc = item.desc;
    }

    QVector<URLPickerInterface::Item> audioOnly;
    QVector<URLPickerInterface::Item>::iterator i = items.begin();

    while (i != items.end())
    {
        if (i->desc.contains("audio_only"))
        {
            audioOnly.append(*i);
            i = items.erase(i);
        }
        else
        {
            i++;
        }
    }

    items.append(audioOnly);

    this->m_isPlayList = false;

    return items;
}

QVector<URLPickerInterface::Item> TwitchURLPicker::getClipList(const QString &url, const QString &id)
{
    QVector<URLPickerInterface::Item> items;
    QString body(
        R"([{"operationName": "VideoAccessToken_Clip",
             "variables": {"slug": "%1"},
             "extensions": {"persistedQuery":
                                {"version": 1,
                                 "sha256Hash": "36b89d2507fce29e5ca551df756d27c1cfe079e2609642b4390aa4c35796eb11"}
                           }
           }])"
    );
    QJsonObject clip = this->downloadGQL(body.arg(id)).array()[0].toObject()["data"].toObject()["clip"].toObject();
    URLPickerInterface::Item item;

    if (clip.isEmpty())
        return items;

    QJsonObject accessToken = clip["playbackAccessToken"].toObject();
    QString dataBody(
        R"({"query": "{
                clip(slug: \"%1\") {
                    title
                    videoQualities {
                        frameRate
                        quality
                        sourceURL
                    }
                }
            }"
        })"
    );
    QJsonDocument data = this->downloadGQL(dataBody.arg(id));

    if (!data.isEmpty())
    {
        QJsonObject clipInData = data.object()["data"].toObject()["clip"].toObject();

        if (!clipInData.isEmpty())
            clip = clipInData;
    }

    item.id = id;
    item.noAudio = false;
    item.orgUrl = url;
    item.title = clip["title"].toString();

    const QJsonArray options = clip["videoQualities"].toArray();

    for (const QJsonValue &option : options)
    {
        QJsonObject optionItem = option.toObject();
        QString quality = optionItem["quality"].toString();
        int fps = optionItem["frameRate"].toInt();
        QString source = optionItem["sourceURL"].toString();

        if (source.isEmpty())
            continue;

        bool ok;

        quality.toInt(&ok);

        if (ok)
            quality += "p";

        QUrl sourceURL(source);
        QUrlQuery sourceQuery(sourceURL.query());

        sourceQuery.addQueryItem("sig", accessToken["signature"].toString());
        sourceQuery.addQueryItem("token", QUrl::toPercentEncoding(accessToken["value"].toString()));

        sourceURL.setQuery(sourceQuery);

        item.url = QString(sourceURL.toEncoded());
        item.quality = quality + fps;
        item.desc = QString("%1 %2fps").arg(quality).arg(fps);
        item.shortDesc = item.desc;

        items.append(item);
    }

    this->m_isPlayList = false;

    return items;
}

QVector<URLPickerInterface::Item> TwitchURLPicker::getStreamList(const QString &url, const QString &id)
{
    QVector<URLPickerInterface::Item> items;
    QString body(
        R"([{"operationName": "StreamMetadata",
             "variables": {"channelLogin": "%1"},
             "extensions": {"persistedQuery":
                                {"version": 1,
                                 "sha256Hash": "1c719a40e481453e5c48d9bb585d971b8b372f8ebb105b17076722264dfa5b3e"}
                           }
           }])"
    );
    QJsonObject streamInfo = this->downloadGQL(body.arg(id)).array().at(0)["data"]["user"].toObject();

    if (streamInfo.isEmpty())
        return items;

    QJsonObject accessToken = this->getAccessToken("stream", "channelName", id);
    QUrlQuery query;
    URLPickerInterface::Item refItem;

    query.addQueryItem("allow_source", "true");
    query.addQueryItem("allow_audio_only", "true");
    query.addQueryItem("allow_spectre", "true");
    query.addQueryItem("playlist_include_framerate", "true");
    query.addQueryItem("segment_preference", "4");
    query.addQueryItem("player", "twitchweb");
    query.addQueryItem("p", QString::number(QRandomGenerator::global()->bounded(1000000, 10000000)));
    query.addQueryItem("sig", accessToken["signature"].toString());
    query.addQueryItem("token", QUrl::toPercentEncoding(accessToken["value"].toString()));

    refItem.id = id.toLower();
    refItem.noAudio = false;
    refItem.orgUrl = url;
    refItem.title = streamInfo["lastBroadcast"].toObject()["title"].toString();

    if (refItem.title.isEmpty())
        refItem.title = refItem.id;

    this->getFormatListFromM3U8(USHER_BASE + "api/channel/hls/" + refItem.id + ".m3u8?" + query.toString(QUrl::FullyEncoded), true, refItem, &items);

    for (URLPickerInterface::Item &item : items)
    {
        QRegularExpressionMatch match;
        QRegularExpression fpsExp(R"((\d+\.\d+fps .* ))");
        QRegularExpression codecExp(R"(\((.+)\))");

        match = fpsExp.match(item.desc);

        if (match.hasMatch())
        {
            QString fps = match.captured(1).split(" ", Qt::SkipEmptyParts).first().remove("fps");
            int fpsValue = (int)fps.toFloat();

            item.desc.replace(fpsExp, QString::number(fpsValue) + "fps ");
        }

        match = codecExp.match(item.desc);

        if (match.hasMatch())
        {
            QString cap = match.captured(1);
            QString codec = cap.split(",").first().split(".").first();

            item.desc.replace(codecExp, "(" + codec + ")");
        }

        item.desc.replace("  ", " ");
        item.shortDesc = item.desc;
    }

    this->m_isPlayList = false;

    return items;
}
