﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "KakaoTVURLPicker.h"
#include "net/HttpDownloader.h"

#include <QRegularExpression>
#include <QUrlQuery>
#include <QBuffer>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

//https://tv.kakao.com/v/389414706
//https://tv.kakao.com/l/10335964
//https://tv.kakao.com/channel/2654268/cliplink/303109700?playlistId=78363
//https://tv.kakao.com/channel/2654268/playlist/78362
//https://tv.kakao.com/channel/3193314/livelink/10335964
//https://tv.kakao.com/channel/3793528/cliplink/423114982
//https://tv.kakao.com/v/423114982

const QString KakaoTVURLPicker::CLIP_LINK_API = "http://tv.kakao.com/api/v1/app/cliplinks";
const QString KakaoTVURLPicker::LIVE_LINK_API = "http://tv.kakao.com/api/v1/app/livelinks";
const QString KakaoTVURLPicker::FT_CLIP_LINK_API = "http://tv.kakao.com/katz/v3/ft/cliplink";

const auto ITEM_SORT_FUNC = [] (const URLPickerInterface::Item &first, const URLPickerInterface::Item &second) -> bool
{
    QRegularExpression numberExp(R"((\d+))");
    int firstQuality = numberExp.match(first.desc).captured(1).toInt();
    int secondQuality = numberExp.match(second.desc).captured(1).toInt();

    return firstQuality > secondQuality;
};

KakaoTVURLPicker::KakaoTVURLPicker()
{

}

KakaoTVURLPicker::~KakaoTVURLPicker()
{

}

bool KakaoTVURLPicker::isSupported(const QString &url) const
{
    return url.contains("tv.kakao.com", Qt::CaseInsensitive);
}

QVector<URLPickerInterface::Item> KakaoTVURLPicker::pickURL(const QString &url)
{
    QVector<URLPickerInterface::Item> items;
    QString to = url;
    QUrl u = QUrl(to);
    QUrlQuery q(u.query());
    QString list = q.queryItemValue("playlistId");

    if (!list.isEmpty() && !to.contains("/playlist", Qt::CaseInsensitive))
        to = u.scheme() + "://" + u.host() + "/channel/" + this->getChannelID(to) + "/playlist/" + list;

    if (to.contains("/cliplink/", Qt::CaseInsensitive))
    {
        QRegularExpression exp(R"(tv\.kakao\.com/channel/(\d+)/cliplink/(\d+))");
        QRegularExpressionMatch match = exp.match(to);

        if (match.hasMatch())
        {
            QString id = match.captured(2);

            items = this->getVideoList(to, id);
        }
    }
    else if (to.contains("/v/", Qt::CaseInsensitive))
    {
        QRegularExpression exp(R"(tv\.kakao\.com/v/(\d+))");
        QRegularExpressionMatch match = exp.match(to);

        if (match.hasMatch())
        {
            QString id = match.captured(1);

            items = this->getVideoList(to, id);
        }
    }
    else if (to.contains("/livelink/", Qt::CaseInsensitive))
    {
        QRegularExpression exp(R"(tv\.kakao\.com/channel/(\d+)/livelink/(\d+))");
        QRegularExpressionMatch match = exp.match(to);

        if (match.hasMatch())
        {
            QString id = match.captured(2);

            items = this->getLiveList(to, id);
        }
    }
    else if (to.contains("/l/", Qt::CaseInsensitive))
    {
        QRegularExpression exp(R"(tv\.kakao\.com/l/(\d+))");
        QRegularExpressionMatch match = exp.match(to);

        if (match.hasMatch())
        {
            QString id = match.captured(1);

            items = this->getLiveList(to, id);
        }
    }
    else if (to.contains("/playlist/", Qt::CaseInsensitive))
    {
        items = this->getPlayList(to);
    }

    return items;
}

bool KakaoTVURLPicker::getAudioStreamByMimeType(const QString &url, const QString &mimeType, QString *ret) const
{
    (void)url;
    (void)mimeType;
    (void)ret;

    return false;
}

bool KakaoTVURLPicker::getProperAudioStreamByMimeType(const QVector<URLPickerInterface::Item> &items, const QString &mimeType, QString *ret) const
{
    (void)items;
    (void)mimeType;
    (void)ret;

    return false;
}

QString KakaoTVURLPicker::getChannelID(const QString &url) const
{
    QRegularExpression exp(R"(tv\.kakao\.com/channel/(\d+)/)");
    QRegularExpressionMatch match = exp.match(url);
    QString id;

    if (match.hasMatch())
        id = match.captured(1);

    return id;
}

QVector<URLPickerInterface::Item> KakaoTVURLPicker::getVideoList(const QString &url, const QString &id)
{
    QVector<URLPickerInterface::Item> items;
    QVector<QPair<QByteArray, QByteArray>> headers;
    HttpDownloader d;
    QUrl headerUrl("http://tv.kakao.com/embed/player/cliplink/" + id);
    QUrlQuery headerQuery;
    QUrlQuery commonQuery;
    QUrlQuery query;
    QBuffer buffer;

    headerQuery.addQueryItem("service", "kakao_tv");
    headerQuery.addQueryItem("autoplay", "1");
    headerQuery.addQueryItem("profile", "HIGH");
    headerQuery.addQueryItem("wmode", "transparent");

    headerUrl.setQuery(headerQuery);

    headers.append(QPair<QByteArray, QByteArray>("Referer", headerUrl.toEncoded()));

    commonQuery.addQueryItem("player", "monet_html5");
    commonQuery.addQueryItem("referer", url);
    commonQuery.addQueryItem("uuid", "");
    commonQuery.addQueryItem("service", "kakao_tv");
    commonQuery.addQueryItem("section", "channel");
    commonQuery.addQueryItem("dteType", "PC");

    query = commonQuery;

    query.addQueryItem("fields", "videoOutputList,clipLink,clip,channel,hasPlusFriend,-service,-tagList");

    if (!d.download(CLIP_LINK_API + "/" + id + "/impress?" + query.toString(QUrl::FullyEncoded), buffer, headers))
        return items;

    QJsonDocument doc = QJsonDocument::fromJson(buffer.buffer());

    if (!doc.isObject())
        return items;

    QString title;
    QString tid;
    URLPickerInterface::Item item;
    QJsonObject root = doc.object();
    QJsonObject clipLink = root["clipLink"].toObject();
    QJsonObject clip = clipLink["clip"].toObject();

    title = clip["title"].toString();

    if (title.isEmpty())
        title = clipLink["displayTitle"].toString();

    tid = root["tid"].toString();

    const QJsonArray formatList = clip["videoOutputList"].toArray();

    for (const QJsonValue &format : formatList)
    {
        QJsonObject formatObj = format.toObject();
        QString profileName = formatObj["profile"].toString();
        QUrlQuery query = commonQuery;
        QBuffer buffer;

        query.addQueryItem("tid", tid);
        query.addQueryItem("profile", profileName);

        if (!d.download(FT_CLIP_LINK_API + "/" + id + "/readyNplay?" + query.toString(QUrl::FullyEncoded), buffer, headers))
            continue;

        QJsonDocument doc = QJsonDocument::fromJson(buffer.buffer());

        if (!doc.isObject())
            continue;

        QJsonObject root = doc.object();
        QJsonObject videoLocation = root["videoLocation"].toObject();

        item.id = id;
        item.noAudio = false;
        item.orgUrl = url;
        item.title = title;
        item.quality = profileName;
        item.url = videoLocation["url"].toString();

        QString label = formatObj["label"].toString();

        if (label.isEmpty())
            item.shortDesc = profileName;
        else
            item.shortDesc = QString("%1 (%2)").arg(profileName, label);

        int width = formatObj["width"].toInt(-1);
        int height = formatObj["height"].toInt(-1);

        if (width > 0 && height > 0)
        {
            item.desc = QString("%1 (%2x%3)")
                        .arg(item.shortDesc)
                        .arg(width)
                        .arg(height);
        }
        else
        {
            item.desc = item.shortDesc;
        }

        item.shortDesc = item.desc;

        items.append(item);
    }

    std::sort(items.begin(), items.end(), ITEM_SORT_FUNC);
    this->m_isPlayList = false;

    return items;
}

QVector<URLPickerInterface::Item> KakaoTVURLPicker::getLiveList(const QString &url, const QString &id)
{
    QVector<URLPickerInterface::Item> items;
    QVector<URLPickerInterface::Item> videoItems;
    QVector<URLPickerInterface::Item> audioItems;
    QVector<QPair<QByteArray, QByteArray>> headers;
    HttpDownloader d;
    QUrl headerUrl("http://tv.kakao.com/embed/player/livelink/" + id);
    QUrlQuery headerQuery;
    QUrlQuery commonQuery;
    QUrlQuery query;
    QBuffer buffer;

    headerQuery.addQueryItem("service", "kakao_tv");
    headerQuery.addQueryItem("autoplay", "1");
    headerQuery.addQueryItem("wmode", "transparent");

    headerUrl.setQuery(headerQuery);

    headers.append(QPair<QByteArray, QByteArray>("Referer", headerUrl.toEncoded()));

    commonQuery.addQueryItem("player", "monet_flash");
    commonQuery.addQueryItem("referer", url);
    commonQuery.addQueryItem("uuid", "");
    commonQuery.addQueryItem("service", "kakao_tv");
    commonQuery.addQueryItem("section", "kakao_tv");
    commonQuery.addQueryItem("dteType", "PC");
    commonQuery.addQueryItem("fulllevels", "liveLink");

    query = commonQuery;

    query.addQueryItem("fields", "user,thumbnailUri,ccuCount,needPassword,canDonation,liveProfileList");

    if (!d.download(LIVE_LINK_API + "/" + id + "/impress?" + query.toString(QUrl::FullyEncoded), buffer, headers))
        return items;

    QJsonDocument doc = QJsonDocument::fromJson(buffer.buffer());

    if (!doc.isObject())
        return items;

    QString tid;
    URLPickerInterface::Item item;
    QJsonObject root = doc.object();
    QJsonObject liveLink = root["liveLink"].toObject();
    QJsonObject live = liveLink["live"].toObject();
    const QJsonArray liveProfileList = live["liveProfileList"].toArray();
    QString title = live["title"].toString();
    QString liveType = live["liveType"].toString();

    if (title.isEmpty())
        title = liveLink["displayTitle"].toString();

    tid = root["tid"].toString();

    for (const QJsonValue &profile : liveProfileList)
    {
        QJsonObject profileObj = profile.toObject();
        QString profileName = profileObj["profile"].toString();
        bool isAudio = profile["audio"].toBool();
        QJsonArray contentTypeList = profileObj["contentTypeList"].toArray();
        QUrlQuery query;
        QBuffer buffer;
        QJsonDocument doc;
        QJsonObject root;
        QString contentType;
        const QStringList supportedType({"HLS", "RTSP", "RTMP", "LINEAR"});

        for (const QString &type : supportedType)
        {
            if (contentTypeList.contains(QJsonValue(type)))
            {
                contentType = type;
                break;
            }
        }

        if (contentType.isEmpty())
            continue;

        query = commonQuery;

        query.addQueryItem("tid", tid);
        query.addQueryItem("profile", profileName);
        query.addQueryItem("contentType", contentType);

        if (!d.download(LIVE_LINK_API + "/" + id + "/raw?" + query.toString(QUrl::FullyEncoded), buffer, headers))
            return items;

        doc = QJsonDocument::fromJson(buffer.buffer());

        if (!doc.isObject())
            return items;

        root = doc.object();

        QJsonObject videoLocation = root["videoLocation"].toObject();
        QString videoUrl = videoLocation["url"].toString();

        item.id = id;
        item.noAudio = false;
        item.orgUrl = url;
        item.title = title;
        item.quality = profileName;

        if (contentType.toUpper() == "LINEAR" && liveType.toUpper() == "LINEAR")
            item.url = this->markLive(this->getUrlFromLinear(videoUrl));
        else
            item.url = this->markLive(videoUrl);

        QString label = profileObj["label"].toString();

        if (label.isEmpty())
            item.shortDesc = profileName;
        else
            item.shortDesc = QString("%1 (%2)").arg(profileName, label);

        int width = profileObj["width"].toInt(-1);
        int height = profileObj["height"].toInt(-1);

        if (width >= 0 && height >= 0 && !isAudio)
        {
            item.desc = QString("%1 (%2x%3 %4fps)")
                        .arg(item.shortDesc)
                        .arg(width)
                        .arg(height)
                        .arg(profileObj["fps"].toInt());
        }
        else
        {
            item.desc = item.shortDesc;
        }

        if (isAudio)
            audioItems.append(item);
        else
            videoItems.append(item);
    }

    std::sort(videoItems.begin(), videoItems.end(), ITEM_SORT_FUNC);
    std::sort(audioItems.begin(), audioItems.end(), ITEM_SORT_FUNC);

    items = videoItems + audioItems;
    this->m_isPlayList = false;

    return items;
}

QVector<URLPickerInterface::Item> KakaoTVURLPicker::getPlayList(const QString &url)
{
    QVector<URLPickerInterface::Item> items;
    HttpDownloader d;
    QBuffer buffer;

    this->m_playListTitle.clear();
    this->m_playListURL.clear();

    if (!d.download(url, buffer))
        return items;

    QString contents = buffer.buffer();
    QString title;
    QRegularExpression exp(R"(<strong\s*class\s*=\s*["']?tit_playlist.*["']?><span\s*class\s*=\s*["']?.*["']?>.*<\/span>(.*)<\/strong>)");
    QRegularExpressionMatch match = exp.match(contents);

    if (match.hasMatch())
        title = match.captured(1);

    exp.setPattern(R"(<a\s*href\s*=\s*["']?(.*)["']?\s*class\s*=\s*["']?link_item["']?>)");

    QRegularExpressionMatchIterator i = exp.globalMatch(contents);
    QUrl redirectedURL(d.getRedirectURL().isEmpty() ? url : d.getRedirectURL());

    while (i.hasNext())
    {
        QRegularExpressionMatch match = i.next();
        URLPickerInterface::Item item;
        QString captured = match.captured(1);

        if (captured.contains("/playlist/"))
            continue;

        QUrl link(captured);

        if (link.host().isEmpty())
        {
            link.setScheme(redirectedURL.scheme());
            link.setHost(redirectedURL.host());
        }

        QRegularExpression exp(R"(/channel/.*/.*/(\d+))");
        QRegularExpressionMatch idMatch = exp.match(link.toString());

        if (!idMatch.hasMatch())
            continue;

        QString vid = idMatch.captured(1);
        QUrl withoutQuery(link);

        withoutQuery.setQuery(QUrlQuery());

        item.url = withoutQuery.toString();
        item.orgUrl = item.url;
        item.id = vid;

        items.append(item);
    }

    this->m_playListTitle = this->escapeHtml(title);
    this->m_playListURL = url;

    this->m_isPlayList = true;

    return items;
}

QString KakaoTVURLPicker::getUrlFromLinear(const QString &url) const
{
    HttpDownloader d;
    QBuffer buffer;

    if (!d.download(url, buffer))
        return QString();

    QJsonDocument doc = QJsonDocument::fromJson(buffer.buffer());

    if (!doc.isObject())
        return QString();

    QJsonObject root = doc.object();
    QJsonObject data = root["data"].toObject();

    return data["mediaurl"].toString();
}
