﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "URLPicker.h"
#include "YouTubeURLPicker.h"
#include "KakaoTVURLPicker.h"
#include "NaverTVURLPicker.h"
#include "TwitchURLPicker.h"

#include <QVector>

URLPicker::URLPicker() :
    m_picker(nullptr)
{

}

URLPicker::~URLPicker()
{
    delete this->m_picker;
}

QVector<URLPickerInterface::Item> URLPicker::pickURL(const QString &url)
{
    if (!this->initPicker(url))
        return QVector<URLPickerInterface::Item>();

    return this->m_picker->pickURL(url);
}

bool URLPicker::getAudioStreamByMimeType(const QString &pickerURL, const QString &url, const QString &mimeType, QString *ret)
{
    if (!this->initPicker(pickerURL))
        return false;

    return this->m_picker->getAudioStreamByMimeType(url, mimeType, ret);
}

bool URLPicker::getProperAudioStreamByMimeType(const QString &pickerURL, const QVector<URLPickerInterface::Item> &items, const QString &mimeType, QString *ret)
{
    if (!this->initPicker(pickerURL))
        return false;

    return this->m_picker->getProperAudioStreamByMimeType(items, mimeType, ret);
}

bool URLPicker::isPlayList() const
{
    if (!this->isVaild())
        return false;

    return this->m_picker->isPlayList();
}

QString URLPicker::getPlayListTitle() const
{
    if (!this->isVaild())
        return QString();

    return this->m_picker->getPlayListTitle();
}

QString URLPicker::getPlayListURL() const
{
    if (!this->isVaild())
        return QString();

    return this->m_picker->getPlayListURL();
}

bool URLPicker::isVaild() const
{
    return this->m_picker != nullptr;
}

bool URLPicker::initPicker(const QString &pickerURL)
{
    if (this->isVaild())
    {
        delete this->m_picker;
        this->m_picker = nullptr;
    }

    this->m_picker = this->getPicker(pickerURL);

    return this->m_picker != nullptr;
}

URLPickerInterface* URLPicker::getPicker(const QString &url) const
{
    {
        YouTubeURLPicker picker;

        if (picker.isSupported(url))
            return new YouTubeURLPicker();
    }

    {
        KakaoTVURLPicker picker;

        if (picker.isSupported(url))
            return new KakaoTVURLPicker();
    }

    {
        NaverTVURLPicker picker;

        if (picker.isSupported(url))
            return new NaverTVURLPicker();
    }

    {
        TwitchURLPicker picker;

        if (picker.isSupported(url))
            return new TwitchURLPicker();
    }

    return nullptr;
}
