﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "URLPickerInterface.h"

#include <QVector>

class QJsonObject;
class QJsonDocument;

class TwitchURLPicker : public URLPickerInterface
{
public:
    TwitchURLPicker();
    virtual ~TwitchURLPicker();

    virtual bool isSupported(const QString &url) const;

    virtual QVector<URLPickerInterface::Item> pickURL(const QString &url);

    virtual bool getAudioStreamByMimeType(const QString &url, const QString &mimeType, QString *ret) const;
    virtual bool getProperAudioStreamByMimeType(const QVector<URLPickerInterface::Item> &items, const QString &mimeType, QString *ret) const;

private:
    QJsonObject getAccessToken(const QString &type, const QString &name, const QString &id) const;
    QJsonDocument downloadGQL(const QString &payload) const;

    QPair<QByteArray, QByteArray> getClientIDHeader() const;

    QString removeQuery(const QString id) const;

    QVector<URLPickerInterface::Item> getVideoList(const QString &url, const QString &id);
    QVector<URLPickerInterface::Item> getClipList(const QString &url, const QString &id);
    QVector<URLPickerInterface::Item> getStreamList(const QString &url, const QString &id);

private:
    static const QString CLIENT_ID;
    static const QString USHER_BASE;
    static const QString GQL_BASE;
};
