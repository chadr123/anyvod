﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "URLPickerInterface.h"

class KakaoTVURLPicker : public URLPickerInterface
{
public:
    KakaoTVURLPicker();
    virtual ~KakaoTVURLPicker();

    virtual bool isSupported(const QString &url) const;

    virtual QVector<URLPickerInterface::Item> pickURL(const QString &url);

    virtual bool getAudioStreamByMimeType(const QString &url, const QString &mimeType, QString *ret) const;
    virtual bool getProperAudioStreamByMimeType(const QVector<URLPickerInterface::Item> &items, const QString &mimeType, QString *ret) const;

private:
    QString getChannelID(const QString &url) const;
    QVector<URLPickerInterface::Item> getVideoList(const QString &url, const QString &id);
    QVector<URLPickerInterface::Item> getLiveList(const QString &url, const QString &id);
    QVector<URLPickerInterface::Item> getPlayList(const QString &url);
    QString getUrlFromLinear(const QString &url) const;

private:
    static const QString CLIP_LINK_API;
    static const QString LIVE_LINK_API;
    static const QString FT_CLIP_LINK_API;
};
