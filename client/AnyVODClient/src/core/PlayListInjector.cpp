﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "PlayListInjector.h"
#include "Common.h"

#include <QSharedMemory>

const QString PlayListInjector::SHARE_MEM_KEY = QString(APP_NAME) + "__ShareMem__";
const int PlayListInjector::SHARE_MEM_SIZE = 2048;

PlayListInjector::PlayListInjector()
{

}

bool PlayListInjector::inject(const QString &paths)
{
    QSharedMemory mem(SHARE_MEM_KEY);

    if (mem.attach() || mem.isAttached())
    {
        if (!mem.lock())
            return false;

        char *data = (char*)mem.data();
        QStringList list = QString::fromUtf8(data).split(NEW_LINE, Qt::SkipEmptyParts);
        QByteArray bytes = paths.toUtf8();

        list.append(QString::fromUtf8(bytes));

        QByteArray joined = list.join(NEW_LINE).toUtf8();

        memset(data, 0, SHARE_MEM_SIZE);
        memcpy(data, joined.data(), joined.size());

        mem.unlock();

        return true;
    }

    return false;
}
