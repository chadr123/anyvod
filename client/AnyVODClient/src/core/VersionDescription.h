﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <QtGlobal>

class VersionDescription
{
private:
    VersionDescription();

public:
    static QString get();
    static QString getAppNameWithVersion();

private:
    static QString getNameWithVersion(const QString &name, const QString &version);
    static QString getFFmpegLibVersion(const QString &name, uint version);
    static QString getBASSLibVersion(const QString &name, uint version);

    static QString getAppVersion();
    static QString getBuildDateTime();
    static QString getAuthor();

    static QString getQtVersion();
    static QString getFFmpegVersion();

    static QString getASSVersion();

    static QString getAVCodecVersion();
    static QString getAVFormatVersion();
#ifndef Q_OS_MOBILE
    static QString getAVDeviceVersion();
#endif
    static QString getAVFilterVersion();
    static QString getAVUtilVersion();
    static QString getSWScaleVersion();
    static QString getSWResampleVersion();

    static QString getBASSVersion();
    static QString getBASSFxVersion();
    static QString getBASSMixVersion();

#ifndef Q_OS_MOBILE
    static QString getMediaInfoVersion();
#endif
    static QString getZLibVersion();
};
