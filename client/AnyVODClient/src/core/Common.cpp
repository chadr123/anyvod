﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "Common.h"
#include "../../../../common/version.h"

#include <QString>

const char ORG_NAME[] = COMPANY;
const char APP_NAME[] = "AnyVOD";

const char PROTOCOL_POSTFIX[] = "://";
const char COMMENT_PREFIX[] = "//";

const int TAB_SIZE = 4;
const QString TABS = QString(" ").repeated(TAB_SIZE);

const int DEFAULT_MIN_TEXTURE_SIZE = 1024;

const double MICRO_SECOND = 1000000.0;

const int MAX_AUDIO_QUEUE_SIZE = 45 * 256 * 1024;
const int MAX_VIDEO_QUEUE_SIZE = 30 * 256 * 1024;
const int MAX_SUBTITLE_QUEUE_SIZE = 15 * 16 * 1024;

#if defined Q_OS_MOBILE
const char ICON_PATH[] = ASSETS_PREFIX"/qml/AnyVODMobileClient/assets/big_icon.png";
#else
const char ICON_PATH[] = ":/icons/logo.png";
#endif

const unsigned long long KILO = 1024ull;
const unsigned long long MEGA = KILO * KILO;
const unsigned long long GIGA = MEGA * KILO;

const unsigned long long KILO_10E3 = 1000ull;
const unsigned long long MEGA_10E3 = KILO_10E3 * KILO_10E3;
const unsigned long long GIGA_10E3 = MEGA_10E3 * KILO_10E3;

#if defined Q_PROCESSOR_ARM_32
const QString ARCH_NAME = "ARM";
#elif defined Q_PROCESSOR_ARM_64
const QString ARCH_NAME = "ARM64";
#elif defined Q_PROCESSOR_X86_32
const QString ARCH_NAME = "X86";
#elif defined Q_PROCESSOR_X86_64
const QString ARCH_NAME = "X86_64";
#else
const QString ARCH_NAME = "UNKNOWN_ARCH";
#endif

const QString LIVE = "live+";
const QString NEW_LINE = "\n";
