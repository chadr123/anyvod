﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "VersionDescription.h"
#include "Version.h"
#include "Common.h"

#ifndef UNICODE
# define UNICODE
#endif

#ifndef _UNICODE
# define _UNICODE
#endif

#ifndef Q_OS_MOBILE
# include <MediaInfoDLL/MediaInfo.h>
#endif

#include <zlib.h>
#include <bass/bass.h>
#include <bass/bass_fx.h>
#include <bass/bassmix.h>
#include <ass/ass.h>

extern "C"
{
# include <libswscale/swscale.h>
# include <libswresample/swresample.h>
# include <libavdevice/avdevice.h>
# include <libavfilter/avfilter.h>
}

VersionDescription::VersionDescription()
{

}

QString VersionDescription::getAppVersion()
{
    return VersionDescription::getAppNameWithVersion().append(NEW_LINE);
}

QString VersionDescription::getBuildDateTime()
{
    return QString("Built on %1 at %2").arg(__DATE__, __TIME__).append(NEW_LINE);
}

QString VersionDescription::getAuthor()
{
    return QString("Author : DongRyeol Cha (chadr@dcple.com)").append(NEW_LINE).append(NEW_LINE);
}

QString VersionDescription::getQtVersion()
{
    return VersionDescription::getNameWithVersion("Qt", qVersion());
}

QString VersionDescription::getFFmpegVersion()
{
    return VersionDescription::getNameWithVersion("FFmpeg", av_version_info());
}

QString VersionDescription::getASSVersion()
{
    uint versionInt = ass_library_version();

    return VersionDescription::getNameWithVersion("ass", Version::makeVersion(Version::VersionNumber(
                                                                                  versionInt >> 28,
                                                                                  ((versionInt >> 24 & 0x0f) * 10) + (versionInt >> 20 & 0x0f),
                                                                                  versionInt >> 12 & 0x0f,
                                                                                  0)
                                                                              ));
}

QString VersionDescription::getAVCodecVersion()
{
    return VersionDescription::getFFmpegLibVersion("avcodec", avcodec_version());
}

QString VersionDescription::getAVFormatVersion()
{
    return VersionDescription::getFFmpegLibVersion("avformat", avformat_version());
}

#ifndef Q_OS_MOBILE
QString VersionDescription::getAVDeviceVersion()
{
    return VersionDescription::getFFmpegLibVersion("avdevice", avdevice_version());
}
#endif

QString VersionDescription::getAVFilterVersion()
{
    return VersionDescription::getFFmpegLibVersion("avfilter", avfilter_version());
}

QString VersionDescription::getAVUtilVersion()
{
    return VersionDescription::getFFmpegLibVersion("avutil", avutil_version());
}

QString VersionDescription::getSWScaleVersion()
{
    return VersionDescription::getFFmpegLibVersion("swscale", swscale_version());
}

QString VersionDescription::getSWResampleVersion()
{
    return VersionDescription::getFFmpegLibVersion("swresample", swresample_version());
}

QString VersionDescription::getBASSFxVersion()
{
    return VersionDescription::getBASSLibVersion("bass_fx", BASS_FX_GetVersion());
}

QString VersionDescription::getBASSMixVersion()
{
    return VersionDescription::getBASSLibVersion("bassmix", BASS_Mixer_GetVersion());
}

QString VersionDescription::getBASSVersion()
{
    return VersionDescription::getBASSLibVersion("bass", BASS_GetVersion());
}

#ifndef Q_OS_MOBILE
QString VersionDescription::getMediaInfoVersion()
{
    return QString::fromStdWString(MediaInfoDLL::MediaInfo::Option_Static(__T("Info_Version"), __T(""))).remove('v').append(NEW_LINE);
}
#endif

QString VersionDescription::getZLibVersion()
{
    return VersionDescription::getNameWithVersion("zlib", zlibVersion());
}

QString VersionDescription::get()
{
    QString desc;

    desc.append(VersionDescription::getAppVersion());
    desc.append(VersionDescription::getBuildDateTime());
    desc.append(VersionDescription::getAuthor());

    desc.append(VersionDescription::getQtVersion());
    desc.append(VersionDescription::getFFmpegVersion());

    desc.append(VersionDescription::getASSVersion());

    desc.append(VersionDescription::getAVCodecVersion());
    desc.append(VersionDescription::getAVFormatVersion());
#ifndef Q_OS_MOBILE
    desc.append(VersionDescription::getAVDeviceVersion());
#endif
    desc.append(VersionDescription::getAVFilterVersion());
    desc.append(VersionDescription::getAVUtilVersion());
    desc.append(VersionDescription::getSWScaleVersion());
    desc.append(VersionDescription::getSWResampleVersion());

    desc.append(VersionDescription::getBASSVersion());
    desc.append(VersionDescription::getBASSFxVersion());
    desc.append(VersionDescription::getBASSMixVersion());

#ifndef Q_OS_MOBILE
    desc.append(VersionDescription::getMediaInfoVersion());
#endif
    desc.append(VersionDescription::getZLibVersion());

    return desc;
}

QString VersionDescription::getAppNameWithVersion()
{
    return QString(APP_NAME) + " " + Version().withArchName().withBuildType().build();
}

QString VersionDescription::getNameWithVersion(const QString &name, const QString &version)
{
    return QString("%1 - %2").arg(name, version).append(NEW_LINE);
}

QString VersionDescription::getFFmpegLibVersion(const QString &name, uint version)
{
    return VersionDescription::getNameWithVersion(name, Version::makeVersion(Version::VersionNumber(
                                                                                 version >> 16,
                                                                                 version >> 8 & 0xff,
                                                                                 version & 0xff,
                                                                                 0)
                                                                             ));
}

QString VersionDescription::getBASSLibVersion(const QString &name, uint version)
{
    return VersionDescription::getNameWithVersion(name, Version::makeVersion(Version::VersionNumber(
                                                                                 version >> 24,
                                                                                 version >> 16 & 0xff,
                                                                                 version >> 8 & 0xff,
                                                                                 version & 0xff)
                                                                             ));
}
