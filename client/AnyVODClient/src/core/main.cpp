﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "AnyVOD.h"
#include "Common.h"
#include "FileAssociationInstaller.h"
#include "Version.h"
#include "FileExtensions.h"
#include "PlayListInjector.h"
#include "setting/Settings.h"
#include "setting/SettingsFactory.h"
#include "setting/SettingBackupAndRestore.h"
#include "ui/MainWindow.h"
#include "ui/PlayList.h"
#include "ui/Equalizer.h"
#include "media/MediaPresenter.h"
#include "media/LastPlay.h"
#include "net/Updater.h"
#include "net/NetworkProxy.h"
#include "device/DTVReader.h"
#include "device/RadioReader.h"

#ifdef Q_OS_MAC
# include "utils/MacUtils.h"
#endif

#include <QCoreApplication>
#include <QGLFormat>
#include <QDir>
#include <QSettings>
#include <QDebug>

int main(int argc, char *argv[])
{
#ifdef Q_OS_WIN
    qputenv("QT_OPENGL", QByteArray("desktop"));
#endif

    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication::setAttribute(Qt::AA_DisableWindowContextHelpButton);

    qRegisterMetaTypeStreamOperators<PlayItem>("PlayItem");
    qRegisterMetaTypeStreamOperators<Equalizer::EqualizerItem>("Equalizer::EqualizerItem");
    qRegisterMetaTypeStreamOperators<MediaPresenter::Range>("MediaPresenter::Range");
    qRegisterMetaTypeStreamOperators<LastPlay::Item>("LastPlay::Item");
    qRegisterMetaTypeStreamOperators<DTVReader::ChannelInfo>("DTVReader::ChannelInfo");
    qRegisterMetaTypeStreamOperators<RadioReader::ChannelInfo>("RadioReader::ChannelInfo");

#ifdef Q_OS_MAC
    MacUtils::useAquaMode();

    {
        QCoreApplication tmp(argc, argv);
        QDir dir(tmp.applicationDirPath());

        dir.cdUp();

        dir.cd("PlugIns");
        dir.cd("platforms");

        qputenv("QT_QPA_PLATFORM_PLUGIN_PATH", dir.absolutePath().toLatin1());
    }
#endif

    AnyVOD a(argc, argv);

    a.setApplicationName(APP_NAME);
    a.setApplicationVersion(Version().build());
    a.setOrganizationDomain(ORG_NAME);
    a.setOrganizationName(ORG_NAME);
    a.setWindowIcon(QIcon(ICON_PATH));

#ifdef QT_NO_DEBUG
    QDir::setCurrent(a.applicationDirPath());
#endif

    SettingBackupAndRestore::restoreSettings();
    NetworkProxy::setupProxy();

    const QSettings &settings = SettingsFactory::getInstance(SettingsFactory::ST_GLOBAL);
    QGLFormat format = QGLFormat::defaultFormat();
    bool useVSync = settings.value(SETTING_USE_VSYNC, false).toBool();
    bool useHDR = settings.value(SETTING_USE_HDR, false).toBool();

    format.setSwapInterval(useVSync ? 1 : 0);

    int bits;

    if (useHDR)
        bits = 16;
    else
        bits = 8;

    format.setRedBufferSize(bits);
    format.setGreenBufferSize(bits);
    format.setBlueBufferSize(bits);
    format.setAlphaBufferSize(bits);

    QGLFormat::setDefaultFormat(format);

    Updater::getInstance().fixUpdateGap();

#ifdef Q_OS_MAC
    QDir dir(a.applicationDirPath());

    dir.cdUp();
    dir.cd("PlugIns");

    a.addLibraryPath(dir.absolutePath());
#endif

    QStringList cmdList = a.arguments();
    QStringList starting;

    if (cmdList.count() > 1)
    {
        if (cmdList[1] == "-d")
        {
            return 0;
        }
        else if (cmdList[1] == "-i")
        {
            FileAssociationInstaller::installFileAssociation(FileExtensions::ALL_EXTS_LIST);

            return 0;
        }
        else if (cmdList[1] == "-u")
        {
            FileAssociationInstaller::uninstallFileAssociation(FileExtensions::ALL_EXTS_LIST);

            return 0;
        }
        else
        {
            cmdList.removeFirst();

            QString movies = cmdList.join(NEW_LINE);

            if (PlayListInjector::inject(movies))
                return 0;
            else
                starting = cmdList;
        }
    }

    MediaPresenter::init();
    srand(time(nullptr));

    MainWindow w(starting);
    int ret;

    w.show();
    ret = a.exec();

    MediaPresenter::deInit();

    return ret;
}
