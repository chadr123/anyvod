﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "SystemResourceUsage.h"
#include "Common.h"

#include <QCoreApplication>
#include <QFile>
#include <QTextStream>

#ifdef Q_OS_WIN
# define WIN32_LEAN_AND_MEAN

# ifndef UNICODE
#  define UNICODE
# endif

# ifndef _UNICODE
#  define _UNICODE
# endif

# include <windows.h>
# include <psapi.h>
#endif

#ifdef Q_OS_MAC
# include <mach/task_info.h>
# include <mach/task.h>
# include <mach/mach_init.h>
# include <mach/mach_traps.h>
# include <mach/mach_port.h>
# include <mach/vm_map.h>
# include <mach/thread_act.h>
# include <mach/host_info.h>
# include <mach/mach_host.h>
#endif

SystemResourceUsage::SystemResourceUsage() :
    m_lastProcessKernelTime(0),
    m_lastProcessUserTime(0),
    m_lastSystemKernelTime(0),
    m_lastSystemUserTime(0)
{

}

SystemResourceUsage& SystemResourceUsage::getInstance()
{
    static SystemResourceUsage instance;

    return instance;
}

float SystemResourceUsage::getCPUUsage()
{
    qint64 pid = QCoreApplication::applicationPid();
    float usage = 0.0f;
    uint64_t processKernelNow = 0;
    uint64_t processUserNow = 0;
    uint64_t systemKernelNow = 0;
    uint64_t systemUserNow = 0;
    uint64_t processKernelElapsed = 0;
    uint64_t processUserElapsed = 0;
    uint64_t systemKernelElapsed = 0;
    uint64_t systemUserElapsed = 0;
    uint64_t totalProcessElapsed = 0;
    uint64_t totalSystemElapsed = 0;

#ifdef Q_OS_WIN
    ULARGE_INTEGER int64;
    FILETIME dummy;
    FILETIME processFileTimeKernelNow;
    FILETIME processFileTimeUserNow;
    FILETIME systemFileTimeKernelNow;
    FILETIME systemFileTimeUserNow;
    HANDLE pHandle = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);

    if (GetProcessTimes(pHandle, &dummy, &dummy, &processFileTimeKernelNow, &processFileTimeUserNow) &&
            GetSystemTimes(&dummy, &systemFileTimeKernelNow, &systemFileTimeUserNow))
    {
        int64.LowPart = processFileTimeKernelNow.dwLowDateTime;
        int64.HighPart = processFileTimeKernelNow.dwHighDateTime;
        processKernelNow = int64.QuadPart;

        int64.LowPart = processFileTimeUserNow.dwLowDateTime;
        int64.HighPart = processFileTimeUserNow.dwHighDateTime;
        processUserNow = int64.QuadPart;

        int64.LowPart = systemFileTimeKernelNow.dwLowDateTime;
        int64.HighPart = systemFileTimeKernelNow.dwHighDateTime;
        systemKernelNow = int64.QuadPart;

        int64.LowPart = systemFileTimeUserNow.dwLowDateTime;
        int64.HighPart = systemFileTimeUserNow.dwHighDateTime;
        systemUserNow = int64.QuadPart;
    }
    else
    {
        if (pHandle)
            CloseHandle(pHandle);

        return -1.0f;
    }

    if (pHandle)
        CloseHandle(pHandle);

#elif defined Q_OS_LINUX
    QFile processStat(QString("/proc/%1/stat").arg(pid));
    QFile systemStat("/proc/stat");

    if (!processStat.open(QIODevice::ReadOnly))
        return -1.0f;

    if (!systemStat.open(QIODevice::ReadOnly))
        return -1.0f;

    QTextStream data;
    QString line;
    int idum;
    QString sdum;

    data.setDevice(&processStat);
    line = data.readLine();
    data.setString(&line);

    data >> idum >> sdum >> sdum;
    data >> idum >> idum >> idum;
    data >> idum >> idum >> idum;
    data >> idum >> idum >> idum >> idum;

    data >> processUserNow;
    data >> processKernelNow;

    data.setDevice(&systemStat);
    line = data.readLine();

    while (true)
    {
        QTextStream cpuData;

        line = data.readLine();

        if (!line.startsWith("cpu"))
            break;

        cpuData.setString(&line);

        cpuData >> sdum;

        uint64_t cpuTimes;
        uint64_t cpuKernelNow = 0;

        for (int i = 0; i < 10; i++)
        {
            cpuData >> cpuTimes;
            cpuKernelNow += cpuTimes;
        }

        this->m_lastCPUKernelTime[sdum] = cpuKernelNow;
    }

    const auto values = this->m_lastCPUKernelTime.values();

    for (uint64_t cpuKernelTimes : values)
        systemKernelNow += cpuKernelTimes;

    systemUserNow = 0;

#elif defined Q_OS_MAC
    (void)pid;
    thread_array_t threads;
    mach_msg_type_number_t count;

    if (task_threads(mach_task_self(), &threads, &count) != KERN_SUCCESS)
        return -1.0f;

    kern_return_t kr = KERN_SUCCESS;
    mach_msg_type_number_t infoCount = THREAD_BASIC_INFO_COUNT;

    for (mach_msg_type_number_t i = 0; i < count; i++)
    {
        thread_basic_info_data_t info;

        kr = thread_info(threads[i], THREAD_BASIC_INFO, (thread_info_t)&info, &infoCount);

        if (kr != KERN_SUCCESS)
            break;

        usage += (float)info.cpu_usage / TH_USAGE_SCALE;
    }

    for (mach_msg_type_number_t i = 0; i < count; i++)
        mach_port_deallocate(mach_task_self(), threads[i]);

    vm_deallocate(mach_task_self(), (vm_address_t)threads, sizeof(thread_t) * count);

    if (kr != KERN_SUCCESS)
        usage = -1.0f;

    host_basic_info_data_t hostInfo;

    infoCount = HOST_BASIC_INFO_COUNT;

    if (host_info(mach_host_self(), HOST_BASIC_INFO, (host_info_t)&hostInfo, &infoCount) != KERN_SUCCESS)
        return -1.0f;

    if (hostInfo.avail_cpus <= 0)
        hostInfo.avail_cpus = 1;

    return usage * 100.0f / hostInfo.avail_cpus;

#endif

    processKernelElapsed = processKernelNow - this->m_lastProcessKernelTime;
    processUserElapsed = processUserNow - this->m_lastProcessUserTime;
    systemKernelElapsed = systemKernelNow - this->m_lastSystemKernelTime;
    systemUserElapsed = systemUserNow - this->m_lastSystemUserTime;
    totalProcessElapsed = processKernelElapsed + processUserElapsed;
    totalSystemElapsed = systemKernelElapsed + systemUserElapsed;

    if (totalSystemElapsed > 0)
        usage = 100.0f * totalProcessElapsed / totalSystemElapsed;

    this->m_lastProcessKernelTime = processKernelNow;
    this->m_lastProcessUserTime = processUserNow;
    this->m_lastSystemKernelTime = systemKernelNow;
    this->m_lastSystemUserTime = systemUserNow;

    return usage;
}

uint64_t SystemResourceUsage::getUsingMemSize() const
{
    qint64 pid = QCoreApplication::applicationPid();
    uint64_t ret = 0;

#ifdef Q_OS_WIN
    HANDLE pHandle = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
    PROCESS_MEMORY_COUNTERS pmc;

    if (GetProcessMemoryInfo(pHandle, &pmc, sizeof(pmc)))
        ret = pmc.WorkingSetSize;

    if (pHandle)
        CloseHandle(pHandle);

#elif defined Q_OS_LINUX
    QFile processStatus(QString("/proc/%1/status").arg(pid));

    if (!processStatus.open(QIODevice::ReadOnly))
        return ret;

    QTextStream data;
    QString line;

    data.setDevice(&processStatus);

    while (true)
    {
        line = data.readLine();

        if (line.isNull())
            break;

        if (line.startsWith("VmRSS"))
        {
            QTextStream rssLine;
            QString size;

            rssLine.setString(&line);
            rssLine >> size >> size;

            ret = size.toULongLong() * KILO;
            break;
        }
    }

#elif defined Q_OS_MAC
    (void)pid;
    mach_msg_type_number_t outCount = MACH_TASK_BASIC_INFO_COUNT;
    mach_task_basic_info_data_t taskInfo;

    memset(&taskInfo, 0, sizeof(taskInfo));

    if (task_info(mach_task_self(), MACH_TASK_BASIC_INFO, (task_info_t)&taskInfo, &outCount) == KERN_SUCCESS)
        ret = taskInfo.resident_size;

#endif

    return ret;
}
