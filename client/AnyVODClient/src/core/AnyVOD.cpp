﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "AnyVOD.h"
#include "PlayListInjector.h"
#include "factories/MainWindowFactory.h"
#include "media/MediaPlayer.h"

#include <QFileOpenEvent>
#include <QApplicationStateChangeEvent>
#include <QDebug>

AnyVOD::AnyVOD(int &argc, char **argv) :
    QApplication(argc, argv),
    m_pausedByInactive(false)
{
#ifdef Q_OS_MAC
    this->m_appleRemote.init();
#endif
}

AnyVOD::~AnyVOD()
{
#ifdef Q_OS_MAC
    this->m_appleRemote.deInit();
#endif
}

bool AnyVOD::event(QEvent *event)
{
    if (event->type() == QEvent::FileOpen)
    {
        QFileOpenEvent *ev = (QFileOpenEvent*)event;

        return PlayListInjector::inject(ev->file());
    }
    else if (event->type() == QEvent::ApplicationStateChange)
    {
        QApplicationStateChangeEvent *ev = (QApplicationStateChangeEvent*)event;

        if (!MainWindowFactory::getInstance())
            return QApplication::event(event);

#ifdef Q_OS_MAC
        MediaPlayer &player = MediaPlayer::getInstance();

        if (ev->applicationState() == Qt::ApplicationActive)
        {
            if (player.isVideo() && player.getStatus() == MediaPlayer::Paused && this->m_pausedByInactive)
            {
                player.resume();
                this->m_pausedByInactive = false;
            }

            this->m_appleRemote.start();
        }
        else
        {
            this->m_appleRemote.stop();

            if (player.isVideo() && player.getStatus() == MediaPlayer::Playing && !this->m_pausedByInactive)
            {
                player.pause();
                this->m_pausedByInactive = true;
            }
        }

        return true;
#else
        (void)ev;
        return QApplication::event(event);
#endif
    }
    else
    {
        return QApplication::event(event);
    }
}
