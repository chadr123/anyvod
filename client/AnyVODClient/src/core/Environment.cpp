﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "Environment.h"
#include "Common.h"
#include "setting/Settings.h"

#include <QFontDatabase>
#include <QFileInfo>
#include <QTextStream>

Environment::Environment() :
    m_fontFilePath("./fonts/NanumGothicBold.ttf"),
    m_skinFilePath("./skins/default.skin"),
    m_shaderDirectoryPath("./shaders"),
    m_fontSize(20),
    m_subtitleOutlineSize(2)
{
    this->load();
}

const Environment& Environment::getInstance()
{
    static Environment instance;

    return instance;
}

QString Environment::getFontFilePath() const
{
    return this->m_fontFilePath;
}

QString Environment::getFontFamily() const
{
    return this->m_fontFamily;
}

QString Environment::getSkinFilePath() const
{
    return this->m_skinFilePath;
}

QString Environment::getShaderDirectoryPath() const
{
    return this->m_shaderDirectoryPath;
}

uint8_t Environment::getFontSize() const
{
    return this->m_fontSize;
}

uint8_t Environment::getSubtitleOutlineSize() const
{
    return this->m_subtitleOutlineSize;
}

void Environment::load()
{
    QMap<QString, QString> list;

    if (!this->parsePair(ENV_FILENAME, &list))
        return;

    QMap<QString, QString>::iterator i = list.begin();

    for (; i != list.end(); i++)
    {
        QString key = i.key().toLower();

        if (key == ENV_FONT_PATH_NAME)
            this->m_fontFilePath = i.value();
        else if (key == ENV_FONT_SIZE_NAME)
            this->m_fontSize = i.value().toUShort();
        else if (key == ENV_FONT_SUBTITLE_OUTLINE_SIZE_NAME)
            this->m_subtitleOutlineSize = i.value().toUShort();
        else if (key == ENV_SKIN_NAME)
            this->m_skinFilePath = i.value();
        else if (key == ENV_SHADER_NAME)
            this->m_shaderDirectoryPath = i.value();
    }

    int id = QFontDatabase::addApplicationFont(QFileInfo(this->m_fontFilePath).absoluteFilePath());

    if (id >= 0)
    {
        QStringList families = QFontDatabase::applicationFontFamilies(id);

        if (families.count() > 0)
            this->m_fontFamily = families[0];
    }

}

bool Environment::parsePair(const QString &filePath, QMap<QString, QString> *ret)
{
    QFile file(filePath);

    if (!file.open(QIODevice::ReadOnly))
        return false;

    QTextStream stream(&file);

    while (!stream.atEnd())
    {
        QString line = stream.readLine();

        line = line.trimmed();

        if (line.length() == 0)
            continue;

        if (line.startsWith(COMMENT_PREFIX))
            continue;

        QStringList pair = line.split(ENV_ITEM_DELIMITER);

        if (pair.length() < 2)
            continue;

        (*ret)[pair[0]] = pair[1];
    }

    return true;
}
