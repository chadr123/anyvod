﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "FileIcons.h"
#include "utils/SeparatingUtils.h"

#include <QTemporaryFile>
#include <QFileIconProvider>
#include <QDir>

FileIcons::FileIcons()
{

}

FileIcons::~FileIcons()
{
    this->release();
}

FileIcons& FileIcons::getInstance()
{
    static FileIcons instance;

    return instance;
}

QIcon FileIcons::findIcon(const QString &hint, bool isDir)
{
    static QFileIconProvider icons;

    if (isDir)
    {
        return icons.icon(QFileIconProvider::Folder);
    }
    else
    {
        if (this->m_cache.contains(hint))
            return icons.icon(QFileInfo(this->m_cache[hint]->fileName()));

        QString tmpFilePath = QDir::tempPath();

        SeparatingUtils::appendDirSeparator(&tmpFilePath);
        tmpFilePath += "XXXXXX.";
        tmpFilePath += hint;

        QTemporaryFile *tmpFile = new QTemporaryFile(tmpFilePath);
        QIcon icon;

        if (tmpFile->open())
        {
            tmpFile->close();
            icon = icons.icon(QFileInfo(tmpFile->fileName()));
        }
        else
        {
            icon = icons.icon(QFileIconProvider::File);
        }

        this->m_cache[hint] = tmpFile;

        return icon;
    }
}

QIcon FileIcons::getExtentionIcon(const QString &ext)
{
    return this->findIcon(ext, false);
}

QIcon FileIcons::getDirectoryIcon()
{
    return this->findIcon(QString(), true);
}

void FileIcons::release()
{
    const auto values = this->m_cache.values();

    for (const auto *file : values)
        delete file;
}
