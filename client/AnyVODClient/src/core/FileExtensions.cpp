﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "FileExtensions.h"
#include "../../../../common/types.h"

const QString FileExtensions::MOVIE_EXTS = QString::fromStdWString(VIDEO_EXTENSION);
const QString FileExtensions::AUDIO_EXTS = QString::fromStdWString(AUDIO_EXTENSION);
const QString FileExtensions::SUBTITLE_EXTS = QString::fromStdWString(SUBTITLE_EXTENSION);
const QString FileExtensions::PLAYLIST_EXTS = QString::fromStdWString(PLAYLIST_EXTENSION);
const QString FileExtensions::FONT_EXTS = QString::fromStdWString(FONT_EXTENSION);
const QString FileExtensions::CAPTURE_FORMAT_EXTS = QString::fromStdWString(CAPTURE_FORMAT_EXTENSION);
const QStringList FileExtensions::MOVIE_EXTS_LIST = MOVIE_EXTS.split(" ", Qt::SkipEmptyParts);
const QStringList FileExtensions::AUDIO_EXTS_LIST = AUDIO_EXTS.split(" ", Qt::SkipEmptyParts);
const QStringList FileExtensions::SUBTITLE_EXTS_LIST = SUBTITLE_EXTS.split(" ", Qt::SkipEmptyParts);
const QStringList FileExtensions::PLAYLIST_EXTS_LIST = PLAYLIST_EXTS.split(" ", Qt::SkipEmptyParts);
const QStringList FileExtensions::MEDIA_EXTS_LIST = MOVIE_EXTS_LIST + AUDIO_EXTS_LIST + PLAYLIST_EXTS_LIST;
const QStringList FileExtensions::ALL_EXTS_LIST = MOVIE_EXTS_LIST + AUDIO_EXTS_LIST + SUBTITLE_EXTS_LIST + PLAYLIST_EXTS_LIST;
const QStringList FileExtensions::FONT_EXTS_LIST = FONT_EXTS.split(" ", Qt::SkipEmptyParts);
const QStringList FileExtensions::CAPTURE_FORMAT_LIST = CAPTURE_FORMAT_EXTS.split(" ", Qt::SkipEmptyParts);

FileExtensions::FileExtensions()
{

}
