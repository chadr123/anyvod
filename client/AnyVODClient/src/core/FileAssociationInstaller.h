﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <QCoreApplication>

#ifdef Q_OS_WIN
# define WIN32_LEAN_AND_MEAN
# include <Windows.h>
#endif

#if defined Q_OS_MACOS
# include <ApplicationServices/ApplicationServices.h>
#endif

class FileAssociationInstaller
{
    Q_DECLARE_TR_FUNCTIONS(FileAssociationInstaller)
private:
    FileAssociationInstaller();

public:
    static void installFileAssociation(const QStringList &list);
    static void uninstallFileAssociation(const QStringList &list);

private:
#ifdef Q_OS_WIN
    static void deleteRegKey(HKEY root, const QString &subKey, const QString &toDeleteKey);
#endif

#if defined Q_OS_MACOS
    static void setDefaultExtension(const QStringList &list, CFStringRef bundleID, UInt32 role);
#endif

#ifdef Q_OS_LINUX
    static void getExtentionToMime(const QString &ext, QString *ret);
    static void setFileAssociation(const QStringList &extList, bool erase);
    static void setFileAssociationInternal(const QString &filePath, const QString &header, const QStringList &extList, bool erase);
#endif
};
