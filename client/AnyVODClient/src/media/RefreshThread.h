﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "Callbacks.h"

#include <QThread>
#include <QWaitCondition>
#include <QMutex>
#include <QQueue>

class RefreshThread : public QThread
{
    Q_OBJECT
public:
    explicit RefreshThread(QObject *parent);
    ~RefreshThread();

    void setVideoEnabled(bool enable);
    void setCallback(const PaintCallback &callback);
    void refreshTimer(int refreshTime);
    void stop();

protected:
    virtual void run();

private:
    volatile bool m_stop;

private:
    QWaitCondition m_wait;
    QMutex m_lock;
    QQueue<int> m_refresh;
    PaintCallback m_callback;
    bool m_isVideo;
};
