﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "SyncType.h"
#include "PacketQueue.h"
#include "VideoFrameQueue.h"
#include "Surface.h"

#include <QMutex>
#include <QSize>
#include <QVector>

extern "C"
{
# include <libavformat/avformat.h>
}

struct SwsContext;

struct SubtitleElement
{
    SubtitleElement()
    {
        memset(&subtitle, 0, sizeof(subtitle));
        pts = 0.0;
        nextUpdate = false;
    }

    bool operator < (const SubtitleElement &rhs) const
    {
        return pts < rhs.pts;
    }

    bool operator == (const SubtitleElement &rhs) const
    {
        return pts == rhs.pts;
    }

    AVSubtitle subtitle;
    double pts;
    bool nextUpdate;
};

struct ExternalClock
{
    ExternalClock()
    {
        base = 0;
    }

    int64_t base;
};

struct Seek
{
    Seek()
    {
        pauseSeeking = false;
        flushed = false;
        request = false;
        firstFrameAfterFlush = false;
        firstAudioAfterFlush = false;
        seekOnResume = false;
        readable = true;
        discard = false;
        discardCount = 0;
        flags = 0;
        pos = 0;
        inc = 0.0;
        discardTime = 0.0;
        videoDiscardStartTime = 0;
        videoDiscardDriftTime = 0;
        readDiscardStartTime = 0;
        readDiscardDriftTime = 0;
        requestPauseOnRender = false;
        pauseOnRenderCount = 0;
    }

    bool pauseSeeking;
    bool flushed;
    bool request;
    bool firstFrameAfterFlush;
    bool firstAudioAfterFlush;
    bool seekOnResume;
    bool readable;
    bool discard;
    int discardCount;
    int flags;
    int64_t pos;
    double time;
    double inc;
    double discardTime;
    int64_t videoDiscardStartTime;
    int64_t videoDiscardDriftTime;
    int64_t readDiscardStartTime;
    int64_t readDiscardDriftTime;
    bool requestPauseOnRender;
    int pauseOnRenderCount;
};

struct Stream
{
    Stream()
    {
        index = -1;
        clock = 0.0;
        clockOffset = 0.0;
        discard = false;
        discardCount = 0;
        stream = nullptr;
        ctx = nullptr;
    }

    int index;
    volatile double clock;
    double clockOffset;
    bool discard;
    int discardCount;
    AVStream *stream;
    AVCodecContext *ctx;
    PacketQueue queue;
    QMutex clockLock;
};

struct Video
{
    Video()
    {
        threadQuit = false;
        frameDrop = 0;
        tempo = 0.0f;
        startTime = 0;
        driftTime = 0;
        pixFormat = AV_PIX_FMT_NONE;
    }

    volatile bool threadQuit;

    Stream stream;
    int frameDrop;
    float tempo;
    int64_t startTime;
    int64_t driftTime;
    AVPixelFormat pixFormat;
};

struct Audio
{
    Stream stream;
};

struct Subtitle
{
    Subtitle()
    {
        threadQuit = false;
        requestReleaseQueue = false;
        isBitmap = false;
        seekFlags = 0;
        isASS = false;
    }

    volatile bool threadQuit;

    Stream stream;
    bool requestReleaseQueue;
    bool isBitmap;
    int seekFlags;
    bool isASS;
    QSize prevSize;
};

struct FrameTimer
{
    FrameTimer()
    {
        timer = 0;
        lastPTS = 0.0;
        lastRealPTS = 0.0;
        lastDelay = 0.0;
        lowFrameCounter = 0;
    }

    int64_t timer;
    double lastPTS;
    double lastRealPTS;
    double lastDelay;
    int lowFrameCounter;
};

struct SubtitleFrames
{
    QVector<SubtitleElement> items;
    QMutex lock;
};

struct FrameSize
{
    FrameSize()
    {
        width = 0;
        height = 0;
    }

    int width;
    int height;
};

struct Pause
{
    Pause()
    {
        pause = false;
        startTime = 0;
        driftTime = 0;
        lastPausedTime = 0.0;
    }

    bool pause;
    int64_t startTime;
    int64_t driftTime;
    double lastPausedTime;
};

struct MediaState
{
    MediaState()
    {
        format = nullptr;
        audioFormat = nullptr;
        syncType = SYNC_AUDIO_MASTER;

        quit = false;
        readThreadQuit = false;
        willBeEnd = false;

        videoConverter = nullptr;
        subtitleConverter = nullptr;

        streamChangeStartTime = 0;
        streamChangeDriftTime = 0;
    }

    AVFormatContext *format;
    AVFormatContext *audioFormat;

    SyncType syncType;

    ExternalClock externalClock;
    Seek seek;
    Audio audio;
    Video video;
    Subtitle subtitle;
    FrameTimer frameTimer;
    VideoFrameQueue videoFrames;
    SubtitleFrames subtitleFrames;
    FrameSize frameSize;

    volatile bool quit;
    volatile bool readThreadQuit;

    bool willBeEnd;

    int64_t streamChangeStartTime;
    int64_t streamChangeDriftTime;

    Pause pause;

    SwsContext *videoConverter;
    SwsContext *subtitleConverter;
};
