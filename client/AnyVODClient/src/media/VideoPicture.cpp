﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "VideoPicture.h"

#include <QSize>

extern "C"
{
# include <libavutil/imgutils.h>
# include <libswscale/swscale.h>
}

VideoPicture::VideoPicture() :
    m_orgWidth(0),
    m_orgHeight(0),
    m_pts(0.0),
    m_time(0),
    m_lumAvg(0),
    m_leftOrTop3D(false)
{

}

bool VideoPicture::create(int width, int height, int orgWidth, int orgHeight, AVPixelFormat format)
{
    if (this->m_surface.create(width, height, format))
    {
        this->m_orgHeight = orgHeight;
        this->m_orgWidth = orgWidth;

        return true;
    }

    return false;
}

bool VideoPicture::create(int width, int height, AVPixelFormat format)
{
    return this->create(width, height, width, height, format);
}

void VideoPicture::destory()
{
    this->m_surface.destory();

    this->m_orgWidth = 0;
    this->m_orgHeight = 0;
    this->m_pts = 0.0;
    this->m_time = 0;
    this->m_lumAvg = 0;
    this->m_leftOrTop3D = false;
    this->m_frameMetaData = FrameMetaData();
}

void VideoPicture::lock()
{
    this->m_lock.lock();
}

void VideoPicture::unlock()
{
    this->m_lock.unlock();
}

bool VideoPicture::isValid() const
{
    return this->m_surface.isValid();
}

bool VideoPicture::isLeftOrTop() const
{
    return this->m_leftOrTop3D;
}

void VideoPicture::addTime(uint32_t time)
{
    this->m_time += time;
}

uint32_t VideoPicture::getTime() const
{
    return this->m_time;
}

double VideoPicture::getPts() const
{
    return m_pts;
}

uint8_t VideoPicture::getLuminanceAverage() const
{
    return this->m_lumAvg;
}

FrameMetaData VideoPicture::getFrameMetaData() const
{
    return this->m_frameMetaData;
}

QSize VideoPicture::getOrgSize() const
{
    return QSize(this->m_orgWidth, this->m_orgHeight);
}

int VideoPicture::getOrgWidth() const
{
    return this->m_orgWidth;
}

int VideoPicture::getOrgHeight() const
{
    return this->m_orgHeight;
}

const Surface &VideoPicture::getSurface() const
{
    return this->m_surface;
}

void VideoPicture::copy(int width, int height, VideoPicture &dest) const
{
    if (this->m_surface.isValid())
    {
        this->copyMembers(width, height, dest);

        if (dest.m_surface.isValid())
        {
            av_image_copy(dest.m_surface.getPixels(), dest.m_surface.getLineSize(),
                          (const uint8_t**)this->m_surface.getPixels(), this->m_surface.getLineSize(),
                          dest.m_surface.getFormat(), width, height);
        }
    }
}

void VideoPicture::fillAlpha(uint8_t value)
{
    this->m_surface.fillAlpha(value);
}

void VideoPicture::resize(int width, int height, VideoPicture &dest) const
{
    if (this->m_surface.isValid())
    {
        this->copyMembers(width, height, dest);

        if (dest.m_surface.isValid())
        {
            SwsContext *converter = sws_getContext(this->m_surface.getWidth(), this->m_surface.getHeight(), this->m_surface.getFormat(),
                                                   width, height, dest.m_surface.getFormat(),
                                                   SWS_FAST_BILINEAR, nullptr, nullptr, nullptr);

            if (converter)
            {
                sws_scale(converter, this->m_surface.getPixels(), this->m_surface.getLineSize(), 0, this->m_surface.getHeight(),
                          dest.m_surface.getPixels(), dest.m_surface.getLineSize());

                sws_freeContext(converter);
            }
        }
    }

}

void VideoPicture::copyMembers(int width, int height, VideoPicture &dest) const
{
    if (width != dest.m_surface.getWidth() || height != dest.m_surface.getHeight())
    {
        if (dest.m_surface.isValid())
            dest.m_surface.destory();

        dest.m_surface.create(width, height, this->m_surface.getFormat());
    }

    dest.m_orgWidth = this->m_orgWidth;
    dest.m_orgHeight = this->m_orgHeight;
    dest.m_lumAvg = this->m_lumAvg;
    dest.m_pts = this->m_pts;
    dest.m_time = this->m_time;
    dest.m_leftOrTop3D = this->m_leftOrTop3D;
    dest.m_frameMetaData = this->m_frameMetaData;
}

void VideoPicture::setFrameMetaData(const FrameMetaData &frameMetaData)
{
    this->m_frameMetaData = frameMetaData;
}

void VideoPicture::setLeftOrTop3D(bool leftOrTop3D)
{
    this->m_leftOrTop3D = leftOrTop3D;
}

void VideoPicture::setLuminanceAverage(uint8_t lumAvg)
{
    this->m_lumAvg = lumAvg;
}

void VideoPicture::setTime(uint32_t time)
{
    this->m_time = time;
}

void VideoPicture::setPts(double pts)
{
    this->m_pts = pts;
}
