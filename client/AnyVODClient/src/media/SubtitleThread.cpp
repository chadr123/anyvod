﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "SubtitleThread.h"
#include "MediaPresenterInterface.h"
#include "MediaState.h"
#include "core/Common.h"
#include "parsers/subtitle/ASSParser.h"

#include <limits>

#include <QDebug>

using namespace std;

SubtitleThread::SubtitleThread(QObject *parent) :
    MediaThreadBase(parent),
    m_lastValidTime(0)
{

}

void SubtitleThread::run()
{
    Subtitle &subtitle = this->m_state->subtitle;
    SubtitleFrames &frames = this->m_state->subtitleFrames;
    QVector<volatile bool*> quits;

    quits.append(&this->m_state->quit);
    quits.append(&this->m_state->subtitle.threadQuit);

    this->m_lastValidTime = 0;

    while (true)
    {
        class localPtr
        {
        public:
            localPtr()
            {

            }

            ~localPtr()
            {
                av_packet_unref(this->packet.packet);
            }

            PacketQueue::Packet packet;
        }l;

        if (this->m_state->quit)
            break;

        if (this->m_state->subtitle.threadQuit)
            break;

        bool block = true;

        if (!subtitle.stream.queue.get(&l.packet, quits, &block))
            break;

        if (subtitle.stream.queue.isFlushPacket(&l.packet))
        {
            avcodec_flush_buffers(subtitle.stream.ctx);
            continue;
        }

        if (!l.packet.discard)
        {
            int decoded = 0;
            int success = 0;
            double pts = 0.0;
            SubtitleElement sub;

            if (l.packet.packet->pts != AV_NOPTS_VALUE)
                pts = av_q2d(subtitle.stream.stream->time_base) * l.packet.packet->pts;

            sub.pts = pts;

            frames.lock.lock();

            if (!frames.items.contains(sub))
            {
                success = avcodec_decode_subtitle2(subtitle.stream.ctx, &sub.subtitle, &decoded, l.packet.packet);

                if (success && decoded)
                {
                    if (sub.subtitle.num_rects > 0)
                    {
                        subtitle.isBitmap = sub.subtitle.rects[0]->type == SUBTITLE_BITMAP;
                        subtitle.isASS = sub.subtitle.rects[0]->type == SUBTITLE_ASS;

                        if (subtitle.isASS)
                        {
                            for (unsigned int i = 0; i < sub.subtitle.num_rects; i++)
                                this->m_presenter->getASSParser().parseSingle(sub.subtitle.rects[i]->ass);

                            avsubtitle_free(&sub.subtitle);
                            frames.lock.unlock();

                            continue;
                        }
                    }

                    frames.items.push_back(sub);

                    bool rebuild = IS_BIT_SET(subtitle.seekFlags, AVSEEK_FLAG_BACKWARD) && this->m_state->seek.firstFrameAfterFlush;

                    if (rebuild)
                    {
                        stable_sort(frames.items.begin(), frames.items.end());
                        subtitle.seekFlags = 0;
                    }

                    int count = frames.items.count();

                    if (count > 1)
                    {
                        if (rebuild)
                        {
                            for (int i = 0; i < count - 1; i++)
                                this->rebuildSubtitleTime(frames.items[i + 1], &frames.items[i]);
                        }
                        else
                        {
                            this->rebuildSubtitleTime(frames.items[count - 1], &frames.items[count - 2]);
                        }
                    }

                    SubtitleElement &last = frames.items.last();

                    if (last.subtitle.start_display_time == last.subtitle.end_display_time ||
                            last.subtitle.end_display_time == numeric_limits<uint32_t>::max())
                    {
                        last.subtitle.end_display_time = this->m_lastValidTime;
                        last.nextUpdate = true;
                    }
                    else
                    {
                        this->m_lastValidTime = max(last.subtitle.end_display_time, this->m_lastValidTime);
                    }
                }
            }

            frames.lock.unlock();
        }
        else
        {
            l.packet.discard = false;
        }
    }
}

bool SubtitleThread::rebuildSubtitleTime(const SubtitleElement &second, SubtitleElement *first) const
{
    if (first->subtitle.start_display_time == first->subtitle.end_display_time ||
            first->subtitle.end_display_time == numeric_limits<uint32_t>::max() ||
            first->nextUpdate)
    {
        double diff = second.pts - first->pts;

        if (diff < 0.0)
            return false;

        first->subtitle.end_display_time = diff * 1000.0;

        if (first->subtitle.end_display_time > this->m_lastValidTime && this->m_lastValidTime > 0)
        {
            first->subtitle.end_display_time = this->m_lastValidTime;
            return true;
        }
    }

    if (first->nextUpdate)
        first->nextUpdate = false;

    return true;
}
