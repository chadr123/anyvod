﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "Concurrent.h"

extern "C"
{
# include <libavformat/avformat.h>
}

class PacketQueue
{
public:
    struct Packet
    {
        Packet()
        {
            packet = av_packet_alloc();
            discard = false;
        }

        ~Packet()
        {
            av_packet_free(&packet);
        }

        bool isNullPacket() const
        {
            return packet->data == nullptr && packet->size == 0;
        }

        Packet& operator = (const Packet &rhs)
        {
            if (rhs.isNullPacket())
            {
                av_packet_free(&packet);
                packet = av_packet_alloc();
            }
            else
            {
                av_packet_ref(packet, rhs.packet);
            }

            discard = rhs.discard;

            return *this;
        }

        AVPacket *packet;
        bool discard;
    };

    PacketQueue();

    void init();
    void end();

    bool hasNullPacket();
    bool hasPacket();
    bool hasPacketInPercent(double ratio, double max);

    bool putFlushPacket();
    bool putNullPacket(int streamIndex);
    bool put(Packet *packet);

    int getBufferSizeInByte();
    bool get(Packet *packet, QVector<volatile bool*> &quits, bool *block);

    bool isFlushPacket(Packet *packet) const;
    void flush();

    void unlock();

private:
    struct PacketList
    {
        Packet pkt;
        PacketList *next;
    };

private:
    PacketList *m_first;
    PacketList *m_last;
    int m_byteSize;
    int m_nullPacketCount;
    Concurrent m_lock;
    Packet m_flushPacket;
};
