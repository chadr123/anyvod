﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "AudioRenderer.h"
#include "MediaPresenterInterface.h"
#include "MediaState.h"
#include "Detail.h"
#include "core/Common.h"
#include "audio/SPDIF.h"
#include "audio/SPDIFSampleRates.h"
#include "utils/FrameUtils.h"

#include <bass/bass_fx.h>
#include <bass/bassmix.h>

#include <algorithm>

extern "C"
{
# include <libavutil/audio_fifo.h>
# include <libswresample/swresample.h>
}

#include <QDebug>

using namespace std;

const int AudioRenderer::AUDIO_DIFF_AVG_NB = 20;
const int AudioRenderer::AUDIO_DIFF_THRESHOLD_FACTOR = 1024;
const int AudioRenderer::SAMPLE_CORRECTION_PERCENT_MAX = 10;
const int AudioRenderer::SPDIF_ENCODING_TMP_BUFFER_SIZE = 9;

AudioRenderer::AudioRenderer(MediaPresenterInterface *presenter) :
    m_presenter(presenter),
    m_state(nullptr),
    m_spdif(SPDIF::getInstance()),
#ifdef Q_OS_IOS
    m_iosNotify(nullptr),
#endif
    m_volume(this->getMaxVolume() / 2),
    m_isMute(false),
    m_bluetoothHeadsetConnected(false),
    m_useSPDIF(false),
    m_bluetoothHeadsetSync(0.0),
    m_audioSync(0.0),
    m_device(-1),
    m_SPIDFEncodingMethod(AnyVODEnums::SEM_NONE)
{

}

bool AudioRenderer::isOpened() const
{
    return this->m_context.handle != 0;
}

HSTREAM AudioRenderer::getHandle() const
{
    return this->m_context.handle;
}

bool AudioRenderer::start(MediaState *state, AVCodecContext *context, AVCodecID codecID)
{
    AVCodecContext *audioCodec = context;
    SPDIFEncoding &encoding = this->m_context.spdifEncoding;

    if (this->m_useSPDIF)
    {
        int sampleRate;
        int channelCount;
        AVSampleFormat format;
        AVCodecContext *spdifCodec;
        AVCodecID spdifCodecID;

        if (this->isUsingSPDIFEncoding())
        {
            AVCodec *encodingCodec;
            uint64_t channelLayout;

            switch (this->m_SPIDFEncodingMethod)
            {
                case AnyVODEnums::SEM_AC3:
                    spdifCodecID = AV_CODEC_ID_AC3;
                    break;
                case AnyVODEnums::SEM_DTS:
                    spdifCodecID = AV_CODEC_ID_DTS;
                    break;
                default:
                    return false;
            }

            encodingCodec = avcodec_find_encoder(spdifCodecID);
            encoding.encoder = avcodec_alloc_context3(encodingCodec);

            switch (this->m_SPIDFEncodingMethod)
            {
                case AnyVODEnums::SEM_AC3:
                    encoding.encoder->bit_rate = 640000;
                    channelLayout = av_get_default_channel_layout(encoding.encoder->channels);

                    break;
                case AnyVODEnums::SEM_DTS:
                    encoding.encoder->bit_rate = 768000;
                    encoding.encoder->strict_std_compliance = FF_COMPLIANCE_EXPERIMENTAL;
                    channelLayout = AV_CH_LAYOUT_5POINT1;

                    break;
                default:
                    channelLayout = av_get_default_channel_layout(encoding.encoder->channels);
                    break;
            }

            encoding.encoder->sample_fmt = encodingCodec->sample_fmts[0];
            encoding.encoder->sample_rate = 48000;
            encoding.encoder->channels = 6;
            encoding.encoder->channel_layout = channelLayout;
            encoding.encoder->time_base = (AVRational){ 1, encoding.encoder->sample_rate };

            if (avcodec_open2(encoding.encoder, encodingCodec, nullptr) < 0)
                return false;

            spdifCodec = encoding.encoder;
            audioCodec = spdifCodec;
        }
        else
        {
            spdifCodec = context;
            spdifCodecID = codecID;
        }

        this->getSPDIFParams(spdifCodec, &sampleRate, &channelCount, &format);

        this->m_useSPDIF = this->m_spdif.open(spdifCodecID, AudioRenderer::SPDIFCallback,
                                              spdifCodec->sample_rate, sampleRate, channelCount, 500, this);
    }

    if (!this->initAudio(audioCodec))
    {
        if (this->m_spdif.isOpened())
            this->m_spdif.close();
        else
            BASS_Free();

        return false;
    }

    this->m_context.spec.channelCount = context->channels;
    this->m_presenter->getRDetail().audioInputChannels = this->m_context.spec.channelCount;

    this->volume(this->m_volume);
    this->mute(this->m_isMute);

    if (this->m_spdif.isOpened())
    {
        if (this->isUsingSPDIFEncoding())
        {
            uint64_t inputLayout = av_get_default_channel_layout(context->channels);
            uint64_t outputLayout = audioCodec->channel_layout;
            AVSampleFormat srcFormat = context->sample_fmt;
            AVSampleFormat destFormat = audioCodec->sample_fmt;
            int inputSampleRate = context->sample_rate;
            int outputSampleRate = audioCodec->sample_rate;
            int outputChannels = audioCodec->channels;
            int frameSize = audioCodec->frame_size;

            this->m_context.audioConverter = swr_alloc_set_opts(nullptr, outputLayout, destFormat, outputSampleRate, inputLayout, srcFormat, inputSampleRate, 0, nullptr);

            if (this->m_context.audioConverter == nullptr)
                return false;

            if (swr_init(this->m_context.audioConverter) < 0)
                return false;

            encoding.fifo = av_audio_fifo_alloc(destFormat, outputChannels, 1);

            if (encoding.fifo == nullptr)
                return false;

            encoding.bufferSize = av_samples_alloc_array_and_samples(&encoding.buffers, nullptr, outputChannels, frameSize, destFormat, 1);

            if (encoding.bufferSize < 0)
                return false;

            encoding.tmpBufferSize = av_samples_alloc_array_and_samples(&encoding.tmpBuffers, nullptr, outputChannels, frameSize * SPDIF_ENCODING_TMP_BUFFER_SIZE, destFormat, 1);

            if (encoding.tmpBufferSize < 0)
                return false;

            encoding.frame = av_frame_alloc();

            if (encoding.frame == nullptr)
                return false;

            encoding.frame->format = destFormat;
            encoding.frame->nb_samples = frameSize;
            encoding.frame->channel_layout = audioCodec->channel_layout;

            av_frame_get_buffer(encoding.frame, 0);
            avcodec_fill_audio_frame(encoding.frame, outputChannels, destFormat, encoding.buffers[0], encoding.bufferSize, 1);
        }
    }
    else
    {
        uint64_t layout = av_get_default_channel_layout(context->channels);
        AVSampleFormat srcFormat = context->sample_fmt;
        AVSampleFormat destFormat = av_get_packed_sample_fmt(context->sample_fmt);
        int sampleRate = context->sample_rate;

        this->m_context.audioConverter = swr_alloc_set_opts(nullptr, layout, destFormat, sampleRate, layout, srcFormat, sampleRate, 0, nullptr);

        if (this->m_context.audioConverter == nullptr)
            return false;

        if (swr_init(this->m_context.audioConverter) < 0)
            return false;

        if (this->m_effect.useNormalizer)
            this->initNormalizer();

        if (this->m_effect.useEqualizer)
            this->initEqualizer();

        if (this->m_effect.useLowerMusic)
            this->initLowerMusic();

        if (this->m_effect.useLowerVoice)
            this->initLowerVoice();

        if (this->m_effect.useHigherVoice)
            this->initHigherVoice();
    }

    this->m_context.bufferSize = 0;
    this->m_context.bufferIndex = 0;

    this->m_context.diffAvgCoef = exp(log(0.01 / AUDIO_DIFF_AVG_NB));
    this->m_context.diffAvgCount = 0;
    this->m_context.diffThreshold = 2.0 * AUDIO_DIFF_THRESHOLD_FACTOR / context->sample_rate;

    this->m_state = state;

    return true;
}

void AudioRenderer::start()
{
    if (!BASS_IsStarted())
        BASS_Start();
}

void AudioRenderer::stop()
{
    if (this->m_spdif.isOpened())
    {
        this->m_spdif.close();
    }
    else
    {
        BASS_Stop();
        BASS_Free();
    }

    this->m_context.handle = 0;
    this->m_context.tempo = 0;

    if (this->m_context.audioConverter)
        swr_free(&this->m_context.audioConverter);

    SPDIFEncoding &encoding = this->m_context.spdifEncoding;

    if (encoding.frame)
        av_frame_free(&encoding.frame);

    if (encoding.encoder)
    {
        avcodec_close(encoding.encoder);
        encoding.encoder = nullptr;
    }

    if (encoding.fifo)
    {
        av_audio_fifo_free(encoding.fifo);
        encoding.fifo = nullptr;
    }

    if (encoding.buffers)
    {
        av_freep(&encoding.buffers[0]);
        av_freep(&encoding.buffers);

        encoding.bufferSize = 0;
    }

    if (encoding.tmpBuffers)
    {
        av_freep(&encoding.tmpBuffers[0]);
        av_freep(&encoding.tmpBuffers);

        encoding.tmpBufferSize = 0;
    }

    this->m_state = nullptr;
}

bool AudioRenderer::play()
{
    if (this->m_spdif.isOpened())
        return this->m_spdif.play();
    else
        return BASS_ChannelPlay(this->m_context.handle, true);
}

void AudioRenderer::pause()
{
    if (this->m_spdif.isOpened())
        this->m_spdif.pause();
    else
        BASS_ChannelPause(this->m_context.handle);
}

void AudioRenderer::resume()
{
    if (this->m_spdif.isOpened())
        this->m_spdif.resume();
    else
        BASS_ChannelPlay(this->m_context.handle, true);
}

template <typename T1, typename T2>
int AudioRenderer::downSampleDecode(AVCodecContext *codec, uint8_t *audioBuffer, int bufSize,
                                    double maximum, PacketQueue::Packet &packet, int *dataSize)
{
    T1 *targetBuffer = (T1*)audioBuffer;
    uint8_t *tmpBuffer = this->m_context.tmpBuffer;
    int decodedSize = bufSize;
    int len = this->decode(codec, &tmpBuffer, &decodedSize, &packet);
    int count = decodedSize / (int)sizeof(T2);

    for (int i = 0; i < count; i++)
        targetBuffer[i] = ((T2*)tmpBuffer)[i] / maximum;

    *dataSize = decodedSize / (sizeof(T2) / sizeof(T1));

    return len;
}

int AudioRenderer::decodeAsSPDIFEncoding(uint8_t *audioBuffer, int bufSize, AVCodecContext *codec, int *dataSize, PacketQueue::Packet *tmpPacket)
{
    SPDIFEncoding &encoding = this->m_context.spdifEncoding;
    AVCodecContext *encodingCodec = encoding.encoder;
    int sampleCount = 0;
    int len = 0;

    if (!encodingCodec)
        return len;

    if (av_audio_fifo_size(encoding.fifo) < encodingCodec->frame_size)
    {
        len = this->decodeAndSampleCount(codec, encoding.tmpBuffers, dataSize, &sampleCount, tmpPacket);

        if (len > 0 && *dataSize > 0)
            av_audio_fifo_write(encoding.fifo, (void**)encoding.tmpBuffers, sampleCount);
    }

    *dataSize = 0;

    if (av_audio_fifo_size(encoding.fifo) >= encodingCodec->frame_size)
    {
        int ret = 0;
        int gotPacket = 0;
        int wroteSize = 0;
        AVPacket *packet = av_packet_alloc();

        av_audio_fifo_read(encoding.fifo, (void**)encoding.buffers, encodingCodec->frame_size);

        while (true)
        {
            ret = FrameUtils::encodeFrame(encodingCodec, packet, encoding.frame, &gotPacket);

            if (ret == AVERROR(EAGAIN) && !gotPacket)
                continue;

            if (ret >= 0 && gotPacket)
            {
                this->m_spdif.setAudioBuffer(audioBuffer + *dataSize, bufSize - *dataSize);

                wroteSize = this->m_spdif.writePacket(*packet);
                *dataSize += wroteSize;

                av_packet_unref(packet);
            }

            if (ret == 0 && gotPacket)
                continue;

            break;
        }

        av_packet_free(&packet);
    }

    return len;
}

QStringList AudioRenderer::getDevices()
{
    BASS_DEVICEINFO info;
    QStringList list;

    for (int i = 0; BASS_GetDeviceInfo(i, &info); i++)
        list.append(QString::fromLocal8Bit(info.name));

    return list;
}

int AudioRenderer::decodeFrame(uint8_t *audioBuffer, int bufSize)
{
    MediaState *ms = this->m_state;
    Audio &audio = ms->audio;
    PacketQueue::Packet *packet = &this->m_context.packet;
    AVCodecContext *codec = audio.stream.ctx;
    QVector<volatile bool*> quits;

    quits.append(&ms->quit);

    while (true)
    {
        while (this->m_context.packetSize > 0)
        {
            int dataSize = bufSize;
            PacketQueue::Packet tmpPacket;
            int len = 0;

            tmpPacket.discard = this->m_context.packet.discard;
            tmpPacket.packet->data = this->m_context.packetData;
            tmpPacket.packet->size = this->m_context.packetSize;

            if (this->m_spdif.isOpened())
            {
                if (this->isUsingSPDIFEncoding())
                {
                    len = this->decodeAsSPDIFEncoding(audioBuffer, bufSize, codec, &dataSize, &tmpPacket);
                }
                else
                {
                    this->m_spdif.setAudioBuffer(audioBuffer, bufSize);
                    dataSize = this->m_spdif.writePacket(*tmpPacket.packet);
                    len = tmpPacket.packet->size;
                }

                if (dataSize < 0)
                    len = -1;
            }
            else
            {
                switch (this->m_context.spec.format)
                {
                    case AV_SAMPLE_FMT_S32:
                    case AV_SAMPLE_FMT_S32P:
                        len = this->downSampleDecode<float, int32_t>(codec, audioBuffer, bufSize, numeric_limits<int32_t>::max(), tmpPacket, &dataSize);
                        break;
                    case AV_SAMPLE_FMT_DBL:
                    case AV_SAMPLE_FMT_DBLP:
                        len = this->downSampleDecode<float, double>(codec, audioBuffer, bufSize, 1.0, tmpPacket, &dataSize);
                        break;
                    case AV_SAMPLE_FMT_S64:
                    case AV_SAMPLE_FMT_S64P:
                        len = this->downSampleDecode<float, int64_t>(codec, audioBuffer, bufSize, numeric_limits<int64_t>::max(), tmpPacket, &dataSize);
                        break;
                    default:
                        len = this->decode(codec, &audioBuffer, &dataSize, &tmpPacket);
                        break;
                }
            }

            if (len < 0 || (len == 0 && dataSize <= 0))
            {
                this->m_context.packetSize = 0;
                break;
            }

            this->m_context.packetData += len;
            this->m_context.packetSize -= len;

            if (dataSize <= 0)
                continue;

            if (!ms->seek.firstAudioAfterFlush)
            {
                ms->seek.firstAudioAfterFlush = true;

                if (ms->syncType == SYNC_AUDIO_MASTER)
                    ms->seek.readable = true;
            }

            this->m_presenter->getRDetail().audioOutputByteCount.fetchAndAddOrdered(dataSize);

            return dataSize;
        }

        av_packet_unref(packet->packet);

        if (ms->quit)
            return -1;

        if (this->m_presenter->isUseBufferingMode() && !this->m_presenter->isRemoteFile() && !this->m_presenter->isLive())
        {
            if (!ms->willBeEnd && (!audio.stream.queue.hasPacket() || (this->m_presenter->isVideo() && !ms->video.stream.queue.hasPacket())))
            {
                this->processEmptyAudio();
                return -1;
            }
        }

        bool block = false;

        if (!audio.stream.queue.get(packet, quits, &block))
            return -1;

        if (block)
        {
            if (this->m_context.isEmpty)
            {
                this->m_context.isEmpty = false;
                this->m_presenter->callEmptyCallback(false);
            }
        }
        else
        {
            this->processEmptyAudio();
            return -1;
        }

        if (audio.stream.queue.isFlushPacket(packet))
        {
            avcodec_flush_buffers(audio.stream.ctx);
            ms->seek.firstAudioAfterFlush = false;

            continue;
        }

        if (packet->discard)
        {
            packet->discard = false;
            continue;
        }

        this->m_presenter->getRDetail().audioInputByteCount.fetchAndAddOrdered(packet->packet->size);

        this->m_context.packetData = packet->packet->data;
        this->m_context.packetSize = packet->packet->size;

        if (packet->packet->pts != AV_NOPTS_VALUE)
        {
            double pts = av_q2d(audio.stream.stream->time_base) * packet->packet->pts;
            QMutexLocker locker(&audio.stream.clockLock);

            audio.stream.clock = pts - audio.stream.clockOffset;
        }

        this->m_presenter->callAudioSubtitleCallback();
    }
}

int AudioRenderer::decode(AVCodecContext *ctx, uint8_t **samples, int *frameSize, PacketQueue::Packet *pkt) const
{
    int sampleCount;

    return this->decodeAndSampleCount(ctx, samples, frameSize, &sampleCount, pkt);
}

int AudioRenderer::decodeAndSampleCount(AVCodecContext *ctx, uint8_t **samples, int *frameSize, int *sampleCount, PacketQueue::Packet *pkt) const
{
    const AudioSpec &spec = this->m_context.spec;
    AVFrame *frame = av_frame_alloc();
    int ret;
    int gotFrame = 0;
    int totalSize = *frameSize;
    bool isDelayed = false;

    ret = FrameUtils::decodeFrame(ctx, pkt->packet, frame, &gotFrame);

    if (pkt->isNullPacket() && ctx->codec->capabilities & AV_CODEC_CAP_DELAY)
        isDelayed = true;

    *frameSize = 0;

    if ((ret >= 0 || isDelayed) && gotFrame)
    {
        if (ctx->channels == spec.channelCount)
        {
            int dataSize;
            int outCount = totalSize / ctx->channels / av_get_bytes_per_sample(ctx->sample_fmt);

            *sampleCount = swr_convert(this->m_context.audioConverter,
                                       samples,
                                       outCount,
                                       (const uint8_t**)frame->extended_data,
                                       frame->nb_samples);

            if (*sampleCount < 0)
                dataSize = 0;
            else
                dataSize = av_samples_get_buffer_size(nullptr, ctx->channels, frame->nb_samples, ctx->sample_fmt, 1);

            *frameSize = dataSize;
        }
    }

    if (frame)
        av_frame_free(&frame);

    return ret;
}

int AudioRenderer::SPDIFCallback(void *buffer, int length, void *user)
{
    AudioRenderer *self = (AudioRenderer*)user;

    return (int)self->callback(HSTREAM(), buffer, (DWORD)length, user);
}

DWORD CALLBACK AudioRenderer::callback(HSTREAM, void *buffer, DWORD length, void *user)
{
    AudioRenderer *self = (AudioRenderer*)user;
    MediaState *ms = self->m_state;
    Audio &audio = ms->audio;
    DWORD oriLen = length;
    DWORD wrote = 0;
    uint8_t *stream = (uint8_t*)buffer;

    if (!ms->pause.pause)
    {
        while (length > 0)
        {
            if (self->m_context.bufferIndex >= self->m_context.bufferSize)
            {
                int audioSize = self->decodeFrame(self->m_context.audioBuffer, sizeof(self->m_context.audioBuffer));

                if (audioSize < 0)
                {
                    break;
                }
                else
                {
                    if (!self->m_spdif.isOpened())
                        audioSize = self->synchronizeAudio((int16_t*)self->m_context.audioBuffer, audioSize);

                    self->m_context.bufferSize = audioSize;
                }

                self->m_context.bufferIndex = 0;
            }

            int size = min(self->m_context.bufferSize - self->m_context.bufferIndex, (unsigned int)length);

            if (size <= 0)
                break;

            memcpy(stream, &self->m_context.audioBuffer[self->m_context.bufferIndex], size);

            length -= size;
            stream += size;
            self->m_context.bufferIndex += size;

            if (ms->quit)
                break;
        }

        wrote = oriLen - length;

        double amount = (double)wrote / self->m_context.spec.bytesPerSec;

        audio.stream.clockLock.lock();
        audio.stream.clock += amount;
        audio.stream.clockLock.unlock();
    }

    if (wrote <= 0 && !ms->willBeEnd)
    {
        AVCodecContext *ctx = audio.stream.ctx;

        if (ctx)
        {
            AVSampleFormat format = av_get_packed_sample_fmt(ctx->sample_fmt);
            int sampleCount = length / av_get_bytes_per_sample(format) / ctx->channels;

            av_samples_set_silence(&stream, 0, sampleCount, ctx->channels, format);

            wrote = length;
        }
    }

    return wrote;
}

void AudioRenderer::getSPDIFParams(const AVCodecContext *context, int *sampleRate, int *channelCount, AVSampleFormat *format)
{
    int selectedSampleRate = SPDIFSampleRates::getInstance().getSelectedSampleRate();

    this->m_spdif.getParams(context, sampleRate, channelCount, format);

    if (selectedSampleRate > 0)
        *sampleRate = selectedSampleRate;
}

bool AudioRenderer::initAudio(const AVCodecContext *context)
{
    Detail &detail = this->m_presenter->getRDetail();
    int sampleRate;
    int channelCount;
    AVSampleFormat format;

    if (this->m_spdif.isOpened())
    {
        this->getSPDIFParams(context, &sampleRate, &channelCount, &format);

        if (context->codec_id == AV_CODEC_ID_DTS)
        {
            if (context->profile == FF_PROFILE_DTS_HD_MA || context->profile == FF_PROFILE_DTS_HD_HRA)
                this->m_spdif.setHDRate(sampleRate * channelCount / 2);
        }

        this->m_spdif.setInterval(30);

        this->m_context.spec.latency = this->m_spdif.getLatency();
        this->m_context.spec.currentChannelCount = channelCount;

        this->m_spdif.getDeviceName(this->m_spdif.getDevice(), &detail.audioSPDIFOutputDevice);

        detail.audioOutputSampleRate = sampleRate;
        detail.audioOutputChannels = channelCount;
        detail.audioOutputBits = 16;
        detail.audioOutputType = "S/PDIF";
    }
    else
    {
        sampleRate = context->sample_rate;
        channelCount = context->channels;
        format = context->sample_fmt;

        DWORD flag = 0;

#ifdef Q_OS_ANDROID
        flag = BASS_DEVICE_FREQ;
#endif

        if (!BASS_Init(this->m_device, sampleRate, BASS_DEVICE_LATENCY | flag, nullptr, nullptr))
            return false;

        if (!BASS_SetConfig(BASS_CONFIG_UPDATEPERIOD, 30))
            return false;

        if (!BASS_SetConfig(BASS_CONFIG_BUFFER, 500))
            return false;

#ifdef Q_OS_IOS
        if (!BASS_SetConfig(BASS_CONFIG_IOS_MIXAUDIO, 0))
            return false;

        if (!BASS_SetConfig(BASS_CONFIG_IOS_NOCATEGORY, 1))
            return false;

        if (!BASS_SetConfigPtr(BASS_CONFIG_IOS_NOTIFY, (void*)this->m_iosNotify))
            return false;
#endif

        switch (format)
        {
            case AV_SAMPLE_FMT_U8:
            case AV_SAMPLE_FMT_U8P:
                flag = BASS_SAMPLE_8BITS;
                break;
            case AV_SAMPLE_FMT_S16:
            case AV_SAMPLE_FMT_S16P:
                flag = 0;
                break;
            case AV_SAMPLE_FMT_S32:
            case AV_SAMPLE_FMT_FLT:
            case AV_SAMPLE_FMT_DBL:
            case AV_SAMPLE_FMT_S32P:
            case AV_SAMPLE_FMT_FLTP:
            case AV_SAMPLE_FMT_DBLP:
            case AV_SAMPLE_FMT_S64:
            case AV_SAMPLE_FMT_S64P:
                flag = BASS_SAMPLE_FLOAT;
                break;
            default:
                return false;
        }

        BASS_INFO info;

        if (!BASS_GetInfo(&info))
            return false;

        this->m_context.handle = BASS_Mixer_StreamCreate(sampleRate, info.speakers, flag);

        if (this->m_context.handle == 0)
            return false;

        flag |= BASS_STREAM_DECODE;

        HSTREAM sourceStream = BASS_StreamCreate(sampleRate, channelCount, flag, AudioRenderer::callback, this);

        if (sourceStream == 0)
            return false;

        BASS_FX_GetVersion();

        HSTREAM tempo = BASS_FX_TempoCreate(sourceStream, BASS_STREAM_DECODE | BASS_FX_FREESOURCE);

        if (tempo)
        {
            sourceStream = tempo;
            this->m_context.tempo = sourceStream;
        }

        if (!BASS_Mixer_StreamAddChannel(this->m_context.handle, sourceStream, BASS_MIXER_DOWNMIX | BASS_STREAM_AUTOFREE))
            return false;

        BASS_CHANNELINFO cinfo;

        if (!BASS_ChannelGetInfo(this->m_context.handle, &cinfo))
            return false;

        this->m_context.spec.latency = (info.latency * 2 + BASS_GetConfig(BASS_CONFIG_BUFFER) + BASS_GetConfig(BASS_CONFIG_UPDATEPERIOD)) / 1000.0;
        this->m_context.spec.currentChannelCount = info.speakers;

        detail.audioInputSampleRate = sampleRate;

        detail.audioOutputSampleRate = cinfo.freq;
        detail.audioOutputChannels = cinfo.chans;

        if (IS_BIT_SET(cinfo.flags, BASS_SAMPLE_8BITS))
        {
            detail.audioOutputBits = 8;
            detail.audioOutputType = "U8";
        }
        else if (IS_BIT_SET(cinfo.flags, BASS_SAMPLE_FLOAT))
        {
            detail.audioOutputBits = 32;
            detail.audioOutputType = "FLT";
        }
        else
        {
            detail.audioOutputBits = 16;
            detail.audioOutputType = "S16";
        }
    }

    this->m_context.spec.bytesPerSec = channelCount * sampleRate * av_get_bytes_per_sample(format);
    this->m_context.spec.format = format;

    detail.audioInputSampleRate = sampleRate;

    return true;
}


bool AudioRenderer::initNormalizer()
{
    this->closeNormalizer();
    this->m_effect.damp = BASS_ChannelSetFX(this->m_context.handle, BASS_FX_BFX_DAMP, 4);

    if (!this->m_effect.damp)
        return false;

    this->m_effect.compressor = BASS_ChannelSetFX(this->m_context.handle, BASS_FX_BFX_COMPRESSOR2, 3);

    if (!this->m_effect.compressor)
    {
        this->closeNormalizer();
        return false;
    }

    return true;
}

void AudioRenderer::closeNormalizer()
{
    BASS_ChannelRemoveFX(this->m_context.handle, this->m_effect.damp);
    BASS_ChannelRemoveFX(this->m_context.handle, this->m_effect.compressor);

    this->m_effect.damp = 0;
    this->m_effect.compressor = 0;
}

bool AudioRenderer::initEqualizer()
{
    this->closeEqualizer();
    this->m_effect.eqaulizer = BASS_ChannelSetFX(this->m_context.handle, BASS_FX_BFX_PEAKEQ, 5);

    if (!this->m_effect.eqaulizer)
        return false;

    this->m_effect.preamp = BASS_ChannelSetFX(this->m_context.handle, BASS_FX_BFX_VOLUME, 6);

    if (!this->m_effect.preamp)
    {
        this->closeEqualizer();
        return false;
    }

    if (!this->setPreAmp(this->m_effect.preampValue))
    {
        this->closeEqualizer();
        return false;
    }

    QVector<Equalizer> eqValues = this->m_effect.equalizerValues;

    for (int i = 0; i < eqValues.count(); i++)
    {
        if (!this->setEqualizerGain(i, eqValues[i].gain))
        {
            this->closeEqualizer();
            return false;
        }
    }

    return true;
}

void AudioRenderer::closeEqualizer()
{
    BASS_ChannelRemoveFX(this->m_context.handle, this->m_effect.eqaulizer);
    BASS_ChannelRemoveFX(this->m_context.handle, this->m_effect.preamp);

    this->m_effect.eqaulizer = 0;
    this->m_effect.preamp = 0;
}

bool AudioRenderer::initLowerVoice()
{
    this->closeLowerVoice();

    this->m_effect.lowerVoice = BASS_ChannelSetFX(this->m_context.handle, BASS_FX_BFX_BQF, 1);

    if (!this->m_effect.lowerVoice)
        return false;

    BASS_BFX_BQF param;

    param.lFilter = BASS_BFX_BQF_NOTCH;
    param.fCenter = 531.0f;
    param.fBandwidth = 4.0f;
    param.lChannel = BASS_BFX_CHANALL;
    param.fGain = 0.0f;
    param.fS = 0.0f;
    param.fQ = 0.0f;

    if (BASS_FXSetParameters(this->m_effect.lowerVoice, &param) == FALSE)
    {
        this->closeLowerVoice();
        return false;
    }

    return true;
}

void AudioRenderer::closeLowerVoice()
{
    BASS_ChannelRemoveFX(this->m_context.handle, this->m_effect.lowerVoice);
    this->m_effect.lowerVoice = 0;
}

bool AudioRenderer::initHigherVoice()
{
    this->closeHigherVoice();

    this->m_effect.higherVoice = BASS_ChannelSetFX(this->m_context.handle, BASS_FX_BFX_PEAKEQ, 2);

    if (!this->m_effect.higherVoice)
        return false;

    BASS_BFX_PEAKEQ value;

    value.fBandwidth = this->m_effect.higherVoiceValue.octave;
    value.fCenter = this->m_effect.higherVoiceValue.center;
    value.fGain = this->m_effect.higherVoiceValue.gain;
    value.lBand = 0;
    value.lChannel = BASS_BFX_CHANALL;

    if (BASS_FXSetParameters(this->m_effect.higherVoice, &value) == FALSE)
    {
        this->closeHigherVoice();
        return false;
    }

    return true;
}

void AudioRenderer::closeHigherVoice()
{
    BASS_ChannelRemoveFX(this->m_context.handle, this->m_effect.higherVoice);
    this->m_effect.higherVoice = 0;
}

bool AudioRenderer::initLowerMusic()
{
    this->closeLowerMusic();

    this->m_effect.lowerMusic = BASS_ChannelSetFX(this->m_context.handle, BASS_FX_BFX_BQF, 0);

    if (!this->m_effect.lowerMusic)
    {
        this->closeLowerMusic();
        return false;
    }

    BASS_BFX_BQF param;

    param.lFilter = BASS_BFX_BQF_BANDPASS;
    param.fCenter = 531.0f;
    param.fBandwidth = 4.0f;
    param.lChannel = BASS_BFX_CHANALL;
    param.fGain = 0.0f;
    param.fS = 0.0f;
    param.fQ = 0.0f;

    if (BASS_FXSetParameters(this->m_effect.lowerMusic, &param) == FALSE)
    {
        this->closeLowerMusic();
        return false;
    }

    return true;
}

void AudioRenderer::closeLowerMusic()
{
    BASS_ChannelRemoveFX(this->m_context.handle, this->m_effect.lowerMusic);
    this->m_effect.lowerMusic = 0;
}

bool AudioRenderer::setPreAmp(float dB)
{
    this->m_effect.preampValue = dB;

    if (this->m_effect.preamp && this->isOpened())
    {
        BASS_BFX_VOLUME vol;

        vol.lChannel = 0;
        vol.fVolume = BASS_BFX_dB2Linear(dB);

        if (BASS_FXSetParameters(this->m_effect.preamp, &vol) == TRUE)
            return true;
    }
    else
    {
        return true;
    }

    return false;
}

float AudioRenderer::getPreAmp() const
{
    return this->m_effect.preampValue;
}

bool AudioRenderer::setEqualizerGain(int band, float gain)
{
    QVector<Equalizer> &values = this->m_effect.equalizerValues;

    if (band >= 0 && band < values.count())
    {
        Equalizer &eq = values[band];

        eq.gain = gain;

        if (this->m_effect.eqaulizer && this->isOpened())
        {
            BASS_BFX_PEAKEQ eqValue;

            eqValue.fBandwidth = eq.octave;
            eqValue.fCenter = eq.center;
            eqValue.fGain = eq.gain;
            eqValue.lBand = band;
            eqValue.lChannel = BASS_BFX_CHANALL;

            if (BASS_FXSetParameters(this->m_effect.eqaulizer, &eqValue) == TRUE)
                return true;
        }
        else
        {
            return true;
        }
    }

    return false;
}

float AudioRenderer::getEqualizerGain(int band) const
{
    const QVector<Equalizer> &values = this->m_effect.equalizerValues;

    if (band >= 0 && band < values.count())
        return values[band].gain;

    return 0.0f;
}

int AudioRenderer::getBandCount() const
{
    return this->m_effect.equalizerValues.count();
}

void AudioRenderer::useNormalizer(bool use)
{
    if (this->m_spdif.isOpened())
        return;

    this->m_effect.useNormalizer = use;

    if (this->isOpened())
    {
        if (use)
            this->initNormalizer();
        else
            this->closeNormalizer();
    }
}

bool AudioRenderer::isUsingNormalizer() const
{
    return this->m_effect.useNormalizer;
}

void AudioRenderer::useEqualizer(bool use)
{
    if (this->m_spdif.isOpened())
        return;

    this->m_effect.useEqualizer = use;

    if (this->isOpened())
    {
        if (use)
            this->initEqualizer();
        else
            this->closeEqualizer();
    }
}

bool AudioRenderer::isUsingEqualizer() const
{
    return this->m_effect.useEqualizer;
}

void AudioRenderer::useLowerVoice(bool use)
{
    if (this->m_spdif.isOpened())
        return;

    this->m_effect.useLowerVoice = use;

    if (this->isOpened())
    {
        if (use)
            this->initLowerVoice();
        else
            this->closeLowerVoice();
    }
}

bool AudioRenderer::isUsingLowerVoice() const
{
    return this->m_effect.useLowerVoice;
}

void AudioRenderer::useHigherVoice(bool use)
{
    if (this->m_spdif.isOpened())
        return;

    this->m_effect.useHigherVoice = use;

    if (this->isOpened())
    {
        if (use)
            this->initHigherVoice();
        else
            this->closeHigherVoice();
    }
}

bool AudioRenderer::isUsingHigherVoice() const
{
    return this->m_effect.useHigherVoice;
}

void AudioRenderer::useLowerMusic(bool use)
{
    if (this->m_spdif.isOpened())
        return;

    this->m_effect.useLowerMusic = use;

    if (this->isOpened())
    {
        if (use)
            this->initLowerMusic();
        else
            this->closeLowerMusic();
    }
}

bool AudioRenderer::isUsingLowerMusic() const
{
    return this->m_effect.useLowerMusic;
}

uint8_t AudioRenderer::getMaxVolume() const
{
    return 255;
}

void AudioRenderer::mute(bool mute)
{
    if (mute)
        this->volumeInternal(0);
    else
        this->volume(this->m_volume);

    this->m_isMute = mute;
}

void AudioRenderer::volumeInternal(uint8_t volume)
{
    if (!this->m_spdif.isOpened())
        BASS_ChannelSetAttribute(this->m_context.handle, BASS_ATTRIB_VOL, (float)volume / (float)this->getMaxVolume());
}

void AudioRenderer::volume(uint8_t volume)
{
    if (volume > this->getMaxVolume())
        this->m_volume = this->getMaxVolume();
    else
        this->m_volume = volume;

    this->volumeInternal(this->m_volume);
}

uint8_t AudioRenderer::getVolume() const
{
    return this->m_volume;
}

bool AudioRenderer::isTempoUsable() const
{
    return this->m_context.tempo != 0;
}

float AudioRenderer::getTempo() const
{
    float tempo = 0.0f;

    BASS_ChannelGetAttribute(this->m_context.tempo, BASS_ATTRIB_TEMPO, &tempo);

    return tempo;
}

void AudioRenderer::setTempo(float percent)
{
    BASS_ChannelSetAttribute(this->m_context.tempo, BASS_ATTRIB_TEMPO, percent);
}

double AudioRenderer::getClock()
{
    if (!this->m_state)
        return 0.0;

    Audio &audio = this->m_state->audio;
    QMutexLocker locker(&audio.stream.clockLock);
    double bluetoothSync;

    if (this->m_bluetoothHeadsetConnected)
        bluetoothSync = this->m_bluetoothHeadsetSync;
    else
        bluetoothSync = 0.0;

    return audio.stream.clock - this->m_audioSync - bluetoothSync - this->m_context.spec.latency;
}

int AudioRenderer::getRemainedDataSize() const
{
    DWORD len = 0;

    if (!this->m_spdif.isOpened())
        len = BASS_ChannelGetData(this->m_context.handle, nullptr, BASS_DATA_AVAILABLE);

    return (int)len;
}

#ifdef Q_OS_IOS
void AudioRenderer::setIOSNotifyCallback(IOSNOTIFYPROC *proc)
{
    this->m_iosNotify = proc;
}
#endif

void AudioRenderer::setSync(double value)
{
    this->m_audioSync = value;
}

double AudioRenderer::getSync() const
{
    return this->m_audioSync;
}

void AudioRenderer::setBluetoothHeadsetConnected(bool connected)
{
    this->m_bluetoothHeadsetConnected = connected;
}

bool AudioRenderer::getBluetoothHeadsetConnected() const
{
    return this->m_bluetoothHeadsetConnected;
}

void AudioRenderer::setBluetoothHeadsetSync(double sync)
{
    this->m_bluetoothHeadsetSync = sync;
}

double AudioRenderer::getBluetoothHeadsetSync() const
{
    return this->m_bluetoothHeadsetSync;
}

int AudioRenderer::synchronizeAudio(short *samples, int samplesSize)
{
    MediaState *ms = this->m_state;

    if (ms->syncType != SYNC_AUDIO_MASTER)
    {
        AVCodecContext *codec = ms->audio.stream.ctx;
        int n = av_get_bytes_per_sample(codec->sample_fmt) * this->m_context.spec.channelCount;
        double diff = this->getClock() - this->m_presenter->getMasterClock();

        if (diff < MediaPresenterInterface::NOSYNC_THRESHOLD)
        {
            this->m_context.diffComputation = diff + this->m_context.diffAvgCoef * this->m_context.diffComputation;

            if (this->m_context.diffAvgCount < AUDIO_DIFF_AVG_NB)
            {
                this->m_context.diffAvgCount++;
            }
            else
            {
                double avgDiff = this->m_context.diffComputation * (1.0 - this->m_context.diffAvgCoef);

                if (fabs(avgDiff) >= this->m_context.diffThreshold)
                {
                    int wantedSize = samplesSize + ((int)(diff * codec->sample_rate) * n);
                    int minSize = samplesSize * ((100 - SAMPLE_CORRECTION_PERCENT_MAX) / 100);
                    int maxSize = samplesSize * ((100 + SAMPLE_CORRECTION_PERCENT_MAX) / 100);

                    if (wantedSize < minSize)
                        wantedSize = minSize;
                    else if (wantedSize > maxSize)
                        wantedSize = maxSize;

                    if (wantedSize < samplesSize)
                    {
                        samplesSize = wantedSize;
                    }
                    else if (wantedSize > samplesSize)
                    {
                        int nb = samplesSize - wantedSize;
                        uint8_t *samplesEnd = (uint8_t *)samples + samplesSize - n;
                        uint8_t *q = samplesEnd + n;

                        while (nb > 0)
                        {
                            memcpy(q, samplesEnd, n);
                            q += n;
                            nb -= n;
                        }

                        samplesSize = wantedSize;
                    }
                }
            }
        }
        else
        {
            this->m_context.diffAvgCount = 0;
            this->m_context.diffComputation = 0;
        }
    }

    return samplesSize;
}

void AudioRenderer::processEmptyAudio()
{
    MediaState *ms = this->m_state;

    if (!ms->pause.pause && !ms->willBeEnd && this->m_presenter->isRemoteProtocol() && !this->m_context.isEmpty)
    {
        this->m_context.isEmpty = true;
        this->m_presenter->callEmptyCallback(true);
    }
}

void AudioRenderer::setDevice(int device)
{
    this->m_device = device;
}

int AudioRenderer::getCurrentDevice() const
{
    return this->m_device;
}

bool AudioRenderer::isUsingSPDIFEncoding() const
{
    return this->m_SPIDFEncodingMethod != AnyVODEnums::SEM_NONE;
}

void AudioRenderer::useSPDIF(bool enable)
{
    this->m_useSPDIF = enable;
}

bool AudioRenderer::isUseSPDIF() const
{
    return this->m_useSPDIF;
}

void AudioRenderer::setSPDIFEncodingMethod(AnyVODEnums::SPDIFEncodingMethod method)
{
    this->m_SPIDFEncodingMethod = method;
}

AnyVODEnums::SPDIFEncodingMethod AudioRenderer::getSPDIFEncodingMethod() const
{
    return this->m_SPIDFEncodingMethod;
}
