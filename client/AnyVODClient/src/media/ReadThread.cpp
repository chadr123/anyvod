﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "ReadThread.h"
#include "MediaState.h"
#include "MediaPresenterInterface.h"
#include "core/Common.h"
#include "audio/SPDIF.h"

extern "C"
{
#if defined Q_OS_MOBILE
    int avio_feof(AVIOContext *s);
#endif
}

#include <QDebug>

const int ReadThread::PAUSE_REFRESH_DELAY = 200;
const int ReadThread::READ_CONTINUE_DELAY = 200;
const int ReadThread::CHECK_END_DELAY = 40;
const int ReadThread::MAX_READ_RETRY_COUNT = 5;

ReadThread::ReadThread(QObject *parent) :
    MediaThreadBase(parent)
{

}

void ReadThread::setEndedCallback(const EventCallback &callback)
{
    this->m_ended = callback;
}

bool ReadThread::readAudio() const
{
    Audio &audio = this->m_state->audio;
    PacketQueue::Packet packet;
    int ret = av_read_frame(this->m_state->audioFormat, packet.packet);

    if (ret >= 0)
    {
        if (packet.packet->stream_index == audio.stream.index)
            audio.stream.queue.put(&packet);
        else
            av_packet_unref(packet.packet);

        return true;
    }
    else
    {
        if (ret == AVERROR_EOF || avio_feof(this->m_state->audioFormat->pb))
            return false;
        else
            return true;
    }
}

void ReadThread::run()
{
    Video &video = this->m_state->video;
    Audio &audio = this->m_state->audio;
    Subtitle &subtitle = this->m_state->subtitle;
    Seek &seek = this->m_state->seek;
    Pause &pause = this->m_state->pause;
    bool videoDelayPut = false;
    bool audioDelayPut = false;
    bool callEnded = false;
    int readFailCount = MAX_READ_RETRY_COUNT;
    bool moreReadAudio = true;
    const int invalidAudioSizeThreshold = 100;

    while (true)
    {
        if (this->m_state->quit)
            break;

        if (seek.seekOnResume && !pause.pause)
        {
            moreReadAudio = true;
            seek.seekOnResume = false;

            this->m_presenter->seek();
        }

        if (seek.request)
        {
            if (this->m_presenter->isAudio() && this->m_presenter->isRemoteFile() && pause.pause)
            {
                seek.seekOnResume = true;
                seek.request = false;
            }
            else
            {
                moreReadAudio = true;
                this->m_presenter->seek();
            }
        }

        if (this->m_state->readThreadQuit)
            break;

        if (pause.pause)
            this->msleep(PAUSE_REFRESH_DELAY);

        if (audio.stream.queue.getBufferSizeInByte() >= MAX_AUDIO_QUEUE_SIZE ||
                video.stream.queue.getBufferSizeInByte() >= MAX_VIDEO_QUEUE_SIZE ||
                subtitle.stream.queue.getBufferSizeInByte() >= MAX_SUBTITLE_QUEUE_SIZE)
        {
            if (this->m_state->audioFormat)
            {
                if (audio.stream.queue.getBufferSizeInByte() < MAX_AUDIO_QUEUE_SIZE)
                {
                    if (moreReadAudio)
                        moreReadAudio = this->readAudio();
                    else
                        this->msleep(READ_CONTINUE_DELAY);

                    continue;
                }

                if (subtitle.stream.queue.getBufferSizeInByte() >= MAX_SUBTITLE_QUEUE_SIZE ||
                        video.stream.queue.getBufferSizeInByte() >= MAX_VIDEO_QUEUE_SIZE)
                {
                    this->msleep(READ_CONTINUE_DELAY);
                    continue;
                }
            }
            else
            {
                this->msleep(READ_CONTINUE_DELAY);
                continue;
            }
        }

        PacketQueue::Packet packet;
        int ret = av_read_frame(this->m_state->format, packet.packet);

        if (ret < 0)
        {
            if (ret == AVERROR_EOF || avio_feof(this->m_state->format->pb))
            {
                bool ended = false;

                this->m_state->willBeEnd = true;
                this->m_state->seek.readable = true;

                if (!videoDelayPut && video.stream.stream && video.stream.ctx->codec &&
                        video.stream.ctx->codec->capabilities & AV_CODEC_CAP_DELAY)
                {
                    video.stream.queue.putNullPacket(video.stream.index);
                    videoDelayPut = true;
                }

                if (!audioDelayPut && audio.stream.stream && audio.stream.ctx->codec &&
                        audio.stream.ctx->codec->capabilities & AV_CODEC_CAP_DELAY)
                {
                    audio.stream.queue.putNullPacket(audio.stream.index);
                    audioDelayPut = true;
                }

                if (audio.stream.stream)
                    this->m_presenter->startAudio();

                if (this->m_presenter->isEnabledVideo())
                {
                    if (audio.stream.stream && video.stream.stream)
                    {
                        int len = this->m_presenter->getRemainedAudioDataSize();

                        if (!audio.stream.queue.hasPacket() && !video.stream.queue.hasPacket() &&
                             (this->m_presenter->isAudio() || this->m_state->videoFrames.getSizeWithoutLock() <= 0) && len < invalidAudioSizeThreshold)
                        {
                            ended = true;
                        }
                    }
                    else if (video.stream.stream)
                    {
                        if (!video.stream.queue.hasPacket() && this->m_state->videoFrames.getSizeWithoutLock() <= 0)
                            ended = true;
                    }
                }
                else
                {
                    int len = this->m_presenter->getRemainedAudioDataSize();

                    if (!audio.stream.queue.hasPacket() && len < invalidAudioSizeThreshold)
                        ended = true;
                }

                if (ended && !callEnded)
                {
                    if (this->m_ended.callback)
                        this->m_ended.callback(this->m_ended.userData);

                    callEnded = true;
                }

                this->msleep(CHECK_END_DELAY);
            }
            else
            {
                if (AVERROR(EAGAIN) == ret)
                {
                    readFailCount = MAX_READ_RETRY_COUNT;
                }
                else
                {
                    readFailCount--;

                    if (readFailCount <= 0)
                    {
                        this->m_presenter->abort(ret);
                        break;
                    }
                }
            }

            continue;
        }

        bool audioStreamDiscard = false;

        if (seek.discard && !seek.request)
        {
            Stream *stream = this->m_presenter->getStream(packet.packet->stream_index);

            if (stream)
            {
                if (packet.packet->dts != AV_NOPTS_VALUE)
                {
                    double pts = av_q2d(stream->stream->time_base) * packet.packet->dts;

                    pts -= this->m_presenter->getAudioClockOffset();

                    if (pts < seek.discardTime)
                    {
                        packet.discard = true;
                        stream->discardCount++;
                    }
                    else
                    {
                        stream->discard = false;
                        audioStreamDiscard = true;
                    }
                }
                else
                {
                    packet.discard = true;
                    stream->discardCount++;
                }
            }

            if (!video.stream.discard && !audio.stream.discard)
            {
                if (audio.stream.index != -1 && (video.stream.discardCount <= 0 || this->m_presenter->isAudio()))
                    this->m_presenter->resumeAudio();

                seek.readDiscardDriftTime += this->m_presenter->getAbsoluteClock() - seek.readDiscardStartTime;
                seek.discard = false;
            }

            if (this->m_presenter->isAudio() && packet.packet->stream_index == video.stream.index)
                packet.discard = false;
        }

        if (packet.packet->stream_index == video.stream.index)
            video.stream.queue.put(&packet);
        else if (packet.packet->stream_index == audio.stream.index && !this->m_state->audioFormat)
            audio.stream.queue.put(&packet);
        else if (packet.packet->stream_index == subtitle.stream.index)
            subtitle.stream.queue.put(&packet);
        else
            av_packet_unref(packet.packet);

        if (this->m_state->audioFormat)
        {
            if (moreReadAudio)
                moreReadAudio = this->readAudio();

            if (audioStreamDiscard)
                audio.stream.discard = false;
        }
    }
}
