﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "Surface.h"
#include "Callbacks.h"
#include "core/UserAspectRatioInfo.h"
#include "video/Font.h"
#include "parsers/subtitle/SAMIParser.h"

#include <QCoreApplication>
#include <QVector2D>
#include <QMutex>

#include <ass/ass.h>

class QOpenGLFramebufferObject;
#if defined Q_OS_MOBILE
class QOpenGLFunctions;
#endif

struct TextureInfo;
struct MediaState;
struct AVSubtitle;

class Surface;
class VideoPicture;
class MediaPresenterInterface;

class VideoRenderer
{
    Q_DECLARE_TR_FUNCTIONS(VideoRenderer)
public:
    VideoRenderer(MediaPresenterInterface *presenter);

    void setup();
    void tearDown();

    void start(int width, int height, MediaState *state);
    void stop();

    bool resetScreen(const int width, const int height, TextureInfo *texInfo, bool inContext);
    void displayVideo(const VideoPicture &vp);

    void setDevicePixelRatio(double ratio);

#if defined Q_OS_MOBILE
    void setGL(QOpenGLFunctions *gl);
#endif

    void setFontFamily(const QString &family);
    QString getFontFamily() const;

    void setFontSize(int size);
    void setFontOutlineSize(int size);
    void setSubtitleMaxOutlineSize(int size);

    void setRotation(double rotation);
    double getRotation() const;

    void setSensorRotation(double rot);
    double getSensorRotation();

    AnyVODEnums::HAlignMethod getHAlign() const;
    void setHAlign(AnyVODEnums::HAlignMethod align);
    AnyVODEnums::VAlignMethod getVAlign() const;
    void setVAlign(AnyVODEnums::VAlignMethod align);

    void resetSubtitlePosition();
    void addVerticalSubtitlePosition(int pos);
    void addHorizontalSubtitlePosition(int pos);
    void setVerticalSubtitlePosition(int pos);
    void setHorizontalSubtitlePosition(int pos);
    int getVerticalSubtitlePosition() const;
    int getHorizontalSubtitlePosition() const;

    void reset3DSubtitleOffset();
    void addVertical3DSubtitleOffset(int pos);
    void addHorizontal3DSubtitleOffset(int pos);
    void setVertical3DSubtitleOffset(int pos);
    void setHorizontal3DSubtitleOffset(int pos);
    int getVertical3DSubtitleOffset() const;
    int getHorizontal3DSubtitleOffset() const;

    void resetVirtual3DDepth();
    void setVirtual3DDepth(qreal depth);
    qreal getVirtual3DDepth() const;

    void setCaptureMode(bool capture);
    bool getCaptureMode() const;

    void set3DMethod(AnyVODEnums::Video3DMethod method);
    AnyVODEnums::Video3DMethod get3DMethod() const;

    void setSubtitle3DMethod(AnyVODEnums::Subtitle3DMethod method);
    AnyVODEnums::Subtitle3DMethod getSubtitle3DMethod() const;

    void setVRInputSource(AnyVODEnums::VRInputSource source);
    AnyVODEnums::VRInputSource getVRInputSource() const;

    void setDistortionAdjustMode(AnyVODEnums::DistortionAdjustMode mode);
    AnyVODEnums::DistortionAdjustMode getDistortionAdjustMode() const;

    void setSubtitleOpaque(float opaque);
    float getSubtitleOpaque() const;

    void setSubtitleSize(float size);
    float getSubtitleSize() const;
    void scheduleRecomputeSubtitleSize();

    void useGPUConvert(bool use);
    bool isUseGPUConvert() const;

    void useHDR(bool use);
    bool isUseHDR() const;

    void setHDRTextureSupported(bool supported);
    bool isHDRTextureSupported() const;

    void setFormat(AVPixelFormat format);
    AVPixelFormat getFormat() const;

    int getOptionDescY() const;
    void setOptionDescY(int y);

    void setVerticalScreenOffset(int offset);
    void setHorizontalScreenOffset(int offset);

    void useDistortion(bool use);
    bool isUseDistortion() const;

    void setBarrelDistortionCoefficients(const QVector2D &coefficients);
    QVector2D getBarrelDistortionCoefficients() const;

    void setPincushionDistortionCoefficients(const QVector2D &coefficients);
    QVector2D getPincushionDistortionCoefficients() const;

    void setDistortionLensCenter(const QVector2D &lensCenter);
    QVector2D getDistortionLensCenter() const;

    void resetFOV();
    float getFOV() const;
    bool addFOV(float value);

    void setUserAspectRatio(UserAspectRatioInfo &ratio);
    void getUserAspectRatio(UserAspectRatioInfo *ret) const;

    void setMaxTextureSize(int size);
    int getMaxTextureSize() const;

    void useSubtitleCacheMode(bool use);
    bool isUseSubtitleCacheMode() const;

    void showOptionDesc(const QString &desc);
    bool isShowOptionDesc() const;

    void displayOptionDesc();
    void hideOptionDesc();
    bool isShowingOptionDesc() const;

    void clearFonts();
    void clearFrameBuffers();

    void useLowQualityMode(bool enable);
    bool isUseLowQualityMode() const;

    void computeFrameSize();

    void showDetail(bool show);
    bool isShowDetail() const;

    void showSubtitle(bool show);
    bool isShowSubtitle() const;

    void setScreenRotationDegree(AnyVODEnums::ScreenRotationDegree degree);
    AnyVODEnums::ScreenRotationDegree getScreenRotationDegree() const;

    void use3DFull(bool enable);
    bool isUse3DFull() const;

    void usePBO(bool enable);
    bool isUsePBO() const;
    bool isUsablePBO() const;
    bool isUsingPBO() const;
    bool isUseGPUConvert(AVPixelFormat format) const;

    bool getPictureRect(QRect *rect);
    QSize getSize() const;

    void scheduleInitTextures();

    void processSubtitleResize();

    void selectPixelFormat(AVPixelFormat format);
    void selectDefaultPixelFormat();

    void setRestartVideoCallback(RestartVideoCallback &callback);

private:
    enum AssRenderMethod
    {
        ASSRM_FILE,
        ASSRM_SINGLE,
        ASSRM_AV
    };

private:
    void initFrameBufferObject(QOpenGLFramebufferObject **object, int width, int height);
    void destroyFrameBufferObject(QOpenGLFramebufferObject **object);

    void checkVideoFrameSize(const VideoPicture &vp);
    void computeSubtitleSize();

    double getAspectRatioByVR(float widthScale, int codecWidth, float heightScale, int codecHeight) const;

    bool isSideBySide() const;
    bool isYUV(AVPixelFormat format) const;
    bool isHDRTexture(AVPixelFormat format) const;
    bool isYUVHDRTexture(AVPixelFormat format) const;
    bool isDistortionNecessary() const;

    AVPixelFormat getCompatibleFormat(AVPixelFormat format) const;
    AVPixelFormat getDefaultFormat() const;

    int findWordWrapPos(const QFontMetrics &fm, const QString &text) const;
    void applyWordWrap(const QFontMetrics &fm, SAMIParser::Paragraph *ret) const;
    bool needMaxWidth() const;
    void getSubtitleSize(const QFontMetrics &fm, const QString &text, QSize *ret) const;
    bool isLeftAlignLine(const QString &text) const;

    void prepareDistortion();
    void applyDistortion();
    void apply360DegreeDistortion();
    void applyVRDistortion();

    void prepareAnaglyph(bool distortionBound);
    void applyAnaglyph(bool distortionBound);

    void drawOptionDesc(const VideoPicture &vp);
    void drawDetail(const VideoPicture &vp);

    void drawVideo(const VideoPicture &vp);
    void drawFonts(const VideoPicture &vp);

    int drawOutlined(Font &font, const QPoint &pos, const QString &text,
                     int outline, const Font::Context &context, float opaque, const VideoPicture &vp);

    void drawSubtitles(const VideoPicture &vp);
    int drawSubtitleLine(int lineNum, int totalLineCount, const QString &text,
                         const QPoint &margin, const Font::Context &context, const QString &totalText, int maxWidth,
                         bool forcedLeft, AnyVODEnums::HAlignMethod halignOption, AnyVODEnums::VAlignMethod valignOption, const VideoPicture &vp);
    bool drawAVSubtitle(const QFontMetrics &fm, const QSize &canvasSize, const AVSubtitle *sp, const VideoPicture &vp);

    void drawASS(const AssRenderMethod method, const int32_t time, const VideoPicture &vp);
    void renderASS(ASS_Image *ass, bool blend, const VideoPicture &vp);
    void renderASSSub(ShaderCompositer::ShaderType type, const QRect &rect, const Surface &frame, bool blend, const QColor &color, bool updateTexture) const;
    void blendASS(ASS_Image *single, Surface &frame) const;

    void getSubtitlePositionOffset(QPoint *ret) const;
    void getSubtitlePositionOffsetByFrame(const QSize &org, QPoint *ret) const;

    bool preparePixelBuffers(ShaderCompositer::ShaderType type, TextureInfo &texInfo, const Surface &surface, GLvoid *pixels[PICTURE_MAX_PLANE]) const;
    void setupTextureAndSampler(ShaderCompositer::ShaderType type, const TextureInfo &texInfo, const Surface &surface) const;
    void updateTexture(ShaderCompositer::ShaderType type, TextureInfo &texInfo, const Surface &surface) const;
    void uploadTextureForGPUConverting(GLvoid *pixels[PICTURE_MAX_PLANE], const Surface &surface) const;
    void uploadTextureForNormal(ShaderCompositer::ShaderType type, TextureInfo &texInfo, GLvoid *pixels[PICTURE_MAX_PLANE], const Surface &surface) const;
    void uploadTexture(ShaderCompositer::ShaderType type, TextureInfo &texInfo, GLvoid *pixels[PICTURE_MAX_PLANE], const Surface &surface) const;
    void renderTexture(ShaderCompositer::ShaderType type, const QColor &color, const QMatrix4x4 &modelView,
                       const QRectF &renderRect, const QRect &rect, const Surface &surface, bool updateTexture) const;
    void cleanUpRender(ShaderCompositer::ShaderType type, const Surface &surface, bool updateTexture) const;
    void renderDistortionAdjustmentLines() const;

    void renderTexture(ShaderCompositer::ShaderType type, const QRect &rect,
                       TextureInfo &texInfo, const Surface &surface, const QRectF &renderRect,
                       const QColor &color, const QMatrix4x4 &modelView, bool updateTexture) const;

    void renderTexture2D(ShaderCompositer::ShaderType type, const QRect &rect, AVPixelFormat format,
                         const QMatrix4x4 &modelView, const QRectF &texCoord, const QColor &color) const;
    void renderTexture3D(ShaderCompositer::ShaderType type, const QRect &rect, AVPixelFormat format,
                         const QMatrix4x4 &modelView, const QRectF &renderRect, const QColor &color, bool leftOrTop) const;

    const QVector2D* getTexCoordsFor3D(bool leftOrTop, const QRectF &renderRect) const;
    bool get3DParameters(bool leftOrTop3D, const QSizeF &surfaceSize, QRectF *renderRect, bool *sideBySide, bool *leftOrTop);

    QPair<QMatrix4x4, QMatrix4x4> buildModelViewMatrices();

private:
    struct Context
    {
        Surface assFrame;
    };

public:
    static const qreal DEFAULT_VIRTUAL_3D_DEPTH;
    static const AVPixelFormat DEFAULT_PIX_FORMAT;

private:
    static const int DEFAULT_VIRT_SUBTITLE_RATIO;
    static const int DEFAULT_HORI_SUBTITLE_RATIO;

    static const float PERSPECTIVE_FOV_Y_DEFAULT;
    static const float PERSPECTIVE_FOV_Y_MIN;
    static const float PERSPECTIVE_FOV_Y_MAX;
    static const float PERSPECTIVE_ZNEAR;
    static const float PERSPECTIVE_ZFAR;

    static const QPoint DEFAULT_3D_SUBTITLE_OFFSET;
    static const QPoint DEFAULT_VR_SUBTITLE_OFFSET;
    static const AVPixelFormat DEFAULT_HDR_PIX_FORMAT;

private:
    MediaPresenterInterface *m_presenter;
    MediaState *m_state;
    Context m_context;
    int m_width;
    int m_height;
    AVPixelFormat m_format;
    Font m_font;
    bool m_showDetail;
    Font m_subtitleFont;
    int m_subtitleFontSize;
    bool m_showSubtitle;
    bool m_showOptionDesc;
    bool m_showingOptionDesc;
    QMutex m_optionDescMutex;
    QString m_optionDesc;
    QString m_fontFamily;
    int m_fontSize;
    int m_fontOutlineSize;
    int m_subtitleOutlineSize;
    int m_subtitleMaxOutlineSize;
    QPoint m_subtitlePosition;
    TextureInfo *m_texInfo;
    AnyVODEnums::HAlignMethod m_halign;
    AnyVODEnums::VAlignMethod m_valign;
    AnyVODEnums::Video3DMethod m_3dMethod;
    AnyVODEnums::Subtitle3DMethod m_3dSubtitleMethod;
    AnyVODEnums::VRInputSource m_vrInputSource;
    float m_subtitleOpaque;
    float m_subtitleSize;
    bool m_usePBO;
    AnyVODEnums::ScreenRotationDegree m_screenRotationDegree;
    bool m_use3DFull;
    int m_maxTextureSize;
    bool m_useGPUConvert;
    bool m_useHDR;
    bool m_hdrTextureSupported;
    QOpenGLFramebufferObject *m_anaglyphFrameBuffer;
    QOpenGLFramebufferObject *m_distortionFrameBuffer;
    QOpenGLFramebufferObject *m_leftDistortionFrameBuffer;
    QOpenGLFramebufferObject *m_rightDistortionFrameBuffer;
    QPoint m_3dSubtitleOffset;
    UserAspectRatioInfo m_userRatio;
#if defined Q_OS_MOBILE
    QOpenGLFunctions *m_gl;
#endif
    QMatrix4x4 m_ortho;
    double m_rotation;
    double m_sensorRotation;
    int m_optionDescY;
    bool m_scheduleRecomputeSubtitleSize;
    bool m_useSubtitleCacheMode;
    double m_devicePixelRatio;
    bool m_captureMode;
    bool m_useLowQualityMode;
    QMutex m_senserLocker;
    QMutex m_screenOffsetLocker;
    QPoint m_screenOffset;
    QSize m_videoPictureSize;
    bool m_useDistortion;
    QVector2D m_barrelDistortionCoefficients;
    QVector2D m_pincushionDistortionCoefficients;
    QVector2D m_distortionLensCenter;
    AnyVODEnums::DistortionAdjustMode m_distortionAdjustMode;
    qreal m_virtual3DDepth;
    float m_fov;
    RestartVideoCallback m_restartVideoCallback;
};
