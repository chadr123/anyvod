﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "PacketQueue.h"

#include <QMutexLocker>
#include <QDeadlineTimer>
#include <QVector>

PacketQueue::PacketQueue()
{
    this->init();
}

void PacketQueue::init()
{
    av_packet_from_data(this->m_flushPacket.packet, (uint8_t *)"FLUSH", 6);
    av_buffer_ref(this->m_flushPacket.packet->buf);

    this->m_first = nullptr;
    this->m_last = nullptr;
    this->m_byteSize = 0;
    this->m_nullPacketCount = 0;
}

bool PacketQueue::isFlushPacket(Packet *packet) const
{
    return !packet->isNullPacket() && memcmp(packet->packet->data, this->m_flushPacket.packet->data, 6) == 0;
}

int PacketQueue::getBufferSizeInByte()
{
    QMutexLocker locker(&this->m_lock.mutex);

    return this->m_byteSize;
}

bool PacketQueue::hasNullPacket()
{
    QMutexLocker locker(&this->m_lock.mutex);

    return this->m_nullPacketCount > 0;
}

bool PacketQueue::hasPacket()
{
    return this->hasNullPacket() || this->getBufferSizeInByte() > 0;
}

bool PacketQueue::hasPacketInPercent(double ratio, double max)
{
    return this->hasNullPacket() || (this->getBufferSizeInByte() / max) > ratio;
}

bool PacketQueue::putFlushPacket()
{
    return this->put(&this->m_flushPacket);
}

bool PacketQueue::putNullPacket(int streamIndex)
{
    Packet packet;

    packet.packet->stream_index = streamIndex;

    return this->put(&packet);
}

bool PacketQueue::put(Packet *packet)
{
    PacketList *packetList = new PacketList;

    if (packetList == nullptr)
        return false;

    packetList->pkt = *packet;
    packetList->next = nullptr;

    this->m_lock.mutex.lock();

    if (this->m_last == nullptr)
        this->m_first = packetList;
    else
        this->m_last->next = packetList;

    this->m_last = packetList;
    this->m_byteSize += packetList->pkt.packet->size;

    if (packet->isNullPacket())
        this->m_nullPacketCount++;

    this->m_lock.cond.wakeOne();
    this->m_lock.mutex.unlock();

    return true;
}

bool PacketQueue::get(Packet *packet, QVector<volatile bool*> &quits, bool *block)
{
    bool ret;

    this->m_lock.mutex.lock();

    while (true)
    {
        for (int i = 0; i < quits.count(); i++)
        {
            if (*quits[i])
            {
                ret = false;
                this->m_lock.mutex.unlock();

                return ret;
            }
        }

        PacketList *packetList = this->m_first;

        if (packetList != nullptr)
        {
            this->m_first = packetList->next;

            if (this->m_first == nullptr)
                this->m_last = nullptr;

            this->m_byteSize -= packetList->pkt.packet->size;
            *packet = packetList->pkt;

            if (packet->isNullPacket())
                this->m_nullPacketCount--;

            delete packetList;

            ret = true;
            *block = true;

            break;
        }
        else
        {
            if (*block)
            {
                this->m_lock.cond.wait(&this->m_lock.mutex);
            }
            else
            {
                *block = false;
                ret = true;
                this->m_lock.cond.wait(&this->m_lock.mutex, QDeadlineTimer(1));

                break;
            }
        }
    }

    this->m_lock.mutex.unlock();

    return ret;
}

void PacketQueue::flush()
{
    this->m_lock.mutex.lock();

    PacketList *next;

    for (PacketList *current = this->m_first; current != nullptr; current = next)
    {
        next = current->next;

        av_packet_unref(current->pkt.packet);
        delete current;
    }

    this->init();

    this->m_lock.cond.wakeOne();
    this->m_lock.mutex.unlock();
}

void PacketQueue::end()
{
    this->flush();
}

void PacketQueue::unlock()
{
    this->m_lock.mutex.lock();
    this->m_lock.cond.wakeOne();
    this->m_lock.mutex.unlock();
}
