﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "PacketQueue.h"
#include "core/AnyVODEnums.h"

#include <QVector>

#include <bass/bass.h>

extern "C"
{
# include <libavcodec/avcodec.h>
}

class MediaPresenterInterface;
class SPDIF;

struct MediaState;
struct SwrContext;
struct AVAudioFifo;

#define MAX_AUDIO_FRAME_SIZE (192000)
#define AUDIO_BUFFER_MULTIPLIER (4)

class AudioRenderer
{
public:
    AudioRenderer(MediaPresenterInterface *presenter);

    bool isOpened() const;
    HSTREAM getHandle() const;

    bool start(MediaState *state, AVCodecContext *context, AVCodecID codecID);
    void start();
    void stop();
    bool play();
    void pause();
    void resume();

    bool setPreAmp(float dB);
    float getPreAmp() const;

    bool setEqualizerGain(int band, float gain);
    float getEqualizerGain(int band) const;
    int getBandCount() const;

    void useNormalizer(bool use);
    bool isUsingNormalizer() const;

    void useEqualizer(bool use);
    bool isUsingEqualizer() const;

    void useLowerVoice(bool use);
    bool isUsingLowerVoice() const;

    void useHigherVoice(bool use);
    bool isUsingHigherVoice() const;

    void useLowerMusic(bool use);
    bool isUsingLowerMusic() const;

    void volume(uint8_t volume);
    void mute(bool mute);

    uint8_t getVolume() const;
    uint8_t getMaxVolume() const;

    bool isTempoUsable() const;
    float getTempo() const;
    void setTempo(float percent);

    double getClock();
    int getRemainedDataSize() const;

#ifdef Q_OS_IOS
    void setIOSNotifyCallback(IOSNOTIFYPROC *proc);
#endif

    void setSync(double value);
    double getSync() const;

    void setBluetoothHeadsetConnected(bool connected);
    bool getBluetoothHeadsetConnected() const;

    void setBluetoothHeadsetSync(double sync);
    double getBluetoothHeadsetSync() const;

    void setDevice(int device);
    int getCurrentDevice() const;

    bool isUsingSPDIFEncoding() const;

    void useSPDIF(bool enable);
    bool isUseSPDIF() const;

    void setSPDIFEncodingMethod(AnyVODEnums::SPDIFEncodingMethod method);
    AnyVODEnums::SPDIFEncodingMethod getSPDIFEncodingMethod() const;

private:
    struct Equalizer
    {
        Equalizer()
        {
            center = 0.0f;
            gain = 0.0f;
            octave = 1.0f;
        }

        float center;
        float gain;
        float octave;
    };

    struct Effect
    {
        Effect()
        {
            useNormalizer = false;
            useEqualizer = false;
            useLowerVoice = false;
            useHigherVoice = false;
            useLowerMusic = false;
            damp = 0;
            compressor = 0;
            eqaulizer = 0;
            preamp = 0;
            lowerVoice = 0;
            higherVoice = 0;
            lowerMusic = 0;

            preampValue = 0;

            Equalizer value;

            value.center = 31.25f;
            value.octave = 1.0f;
            equalizerValues.append(value);

            value.center = 62.5f;
            value.octave = 1.0f;
            equalizerValues.append(value);

            value.center = 125.0f;
            value.octave = 1.0f;
            equalizerValues.append(value);

            value.center = 250.0f;
            value.octave = 1.0f;
            equalizerValues.append(value);

            value.center = 500.0f;
            value.octave = 1.0f;
            equalizerValues.append(value);

            value.center = 1000.0f;
            value.octave = 1.0f;
            equalizerValues.append(value);

            value.center = 2000.0f;
            value.octave = 1.0f;
            equalizerValues.append(value);

            value.center = 4000.0f;
            value.octave = 1.0f;
            equalizerValues.append(value);

            value.center = 8000.0f;
            value.octave = 1.0f;
            equalizerValues.append(value);

            value.center = 16000.0f;
            value.octave = 1.0f;
            equalizerValues.append(value);

            higherVoiceValue.center = 531.0f;
            higherVoiceValue.octave = 4.0f;
            higherVoiceValue.gain = 10.0f;
        }

        bool useNormalizer;
        bool useEqualizer;
        bool useLowerVoice;
        bool useHigherVoice;
        bool useLowerMusic;
        HFX damp;
        HFX compressor;
        HFX eqaulizer;
        HFX preamp;
        HFX lowerVoice;
        HFX higherVoice;
        HFX lowerMusic;
        float preampValue;//dB
        QVector<Equalizer> equalizerValues;
        Equalizer higherVoiceValue;
    };

    struct AudioSpec
    {
        AudioSpec()
        {
            channelCount = 0;
            currentChannelCount = 0;
            bytesPerSec = 0;
            latency = 0.0;
            format = AV_SAMPLE_FMT_NONE;
        }

        int channelCount;
        int currentChannelCount;
        int bytesPerSec;
        double latency;
        AVSampleFormat format;
    };

    struct SPDIFEncoding
    {
        SPDIFEncoding()
        {
            frame = nullptr;
            encoder = nullptr;
            fifo = nullptr;
            buffers = nullptr;
            bufferSize = 0;
            tmpBuffers = nullptr;
            tmpBufferSize = 0;
        }

        AVFrame *frame;
        AVCodecContext *encoder;
        AVAudioFifo *fifo;
        uint8_t **buffers;
        uint8_t **tmpBuffers;
        int bufferSize;
        int tmpBufferSize;
    };

    struct DecodingContext
    {
        DecodingContext()
        {
            memset(audioBuffer, 0, sizeof(audioBuffer));
            bufferSize = 0;
            bufferIndex = 0;
            packetData = nullptr;
            packetSize = 0;
            diffComputation = 0.0;
            diffAvgCoef = 0.0;
            diffThreshold = 0.0;
            diffAvgCount = 0;
            handle = 0;
            tempo = 0;
            isEmpty = false;
            audioConverter = nullptr;
        }

        ~DecodingContext()
        {
            av_packet_unref(packet.packet);
        }

        DECLARE_ALIGNED(16, uint8_t, audioBuffer[MAX_AUDIO_FRAME_SIZE * AUDIO_BUFFER_MULTIPLIER]);
        DECLARE_ALIGNED(16, uint8_t, tmpBuffer[MAX_AUDIO_FRAME_SIZE * AUDIO_BUFFER_MULTIPLIER]);
        unsigned int bufferSize;
        unsigned int bufferIndex;
        PacketQueue::Packet packet;
        uint8_t *packetData;
        int packetSize;
        double diffComputation;
        double diffAvgCoef;
        double diffThreshold;
        int diffAvgCount;
        HSTREAM handle;
        HSTREAM tempo;
        AudioSpec spec;
        SPDIFEncoding spdifEncoding;
        bool isEmpty;
        SwrContext *audioConverter;
    };

private:
    void getSPDIFParams(const AVCodecContext *context, int *sampleRate, int *channelCount, AVSampleFormat *format);

    bool initAudio(const AVCodecContext *context);

    bool initNormalizer();
    void closeNormalizer();

    bool initEqualizer();
    void closeEqualizer();

    bool initLowerVoice();
    void closeLowerVoice();

    bool initHigherVoice();
    void closeHigherVoice();

    bool initLowerMusic();
    void closeLowerMusic();

    void volumeInternal(uint8_t volume);
    int synchronizeAudio(short *samples, int samplesSize);

    void processEmptyAudio();

    template <typename T1, typename T2>
    int downSampleDecode(AVCodecContext *codec, uint8_t *audioBuffer, int bufSize,
                         double maximum, PacketQueue::Packet &packet, int *dataSize);

    int decodeFrame(uint8_t *audioBuffer, int bufSize);
    int decode(AVCodecContext *ctx, uint8_t **samples, int *frameSize, PacketQueue::Packet *pkt) const;
    int decodeAndSampleCount(AVCodecContext *ctx, uint8_t **samples, int *frameSize, int *sampleCount, PacketQueue::Packet *pkt) const;
    int decodeAsSPDIFEncoding(uint8_t *audioBuffer, int bufSize, AVCodecContext *codec, int *dataSize, PacketQueue::Packet *tmpPacket);

public:
    static QStringList getDevices();

public:
    static int SPDIFCallback(void *buffer, int length, void *user);
    static DWORD CALLBACK callback(HSTREAM, void *buffer, DWORD length, void *user);

private:
    static const int AUDIO_DIFF_AVG_NB;
    static const int AUDIO_DIFF_THRESHOLD_FACTOR;
    static const int SAMPLE_CORRECTION_PERCENT_MAX;
    static const int SPDIF_ENCODING_TMP_BUFFER_SIZE;

private:
    MediaPresenterInterface *m_presenter;
    MediaState *m_state;
    SPDIF &m_spdif;
#ifdef Q_OS_IOS
    IOSNOTIFYPROC *m_iosNotify;
#endif
    uint8_t m_volume;
    bool m_isMute;
    bool m_bluetoothHeadsetConnected;
    bool m_useSPDIF;
    double m_bluetoothHeadsetSync;
    double m_audioSync;
    int m_device;
    AnyVODEnums::SPDIFEncodingMethod m_SPIDFEncodingMethod;
    Effect m_effect;
    DecodingContext m_context;
};
