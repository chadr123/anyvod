﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "VideoThread.h"
#include "MediaPresenterInterface.h"
#include "MediaState.h"
#include "Detail.h"
#include "core/Common.h"
#include "core/AnyVODEnums.h"
#include "utils/FrameUtils.h"
#include "audio/SPDIF.h"
#include "decoders/HWDecoder.h"
#include "video/Deinterlacer.h"
#include "video/FilterGraph.h"

#include <QDebug>

extern "C"
{
# include <libavutil/pixdesc.h>
# include <libavutil/imgutils.h>
}

VideoThread::VideoThread(QObject *parent) :
    MediaThreadBase(parent)
{

}

void VideoThread::run()
{
    Video &video = this->m_state->video;
    Seek &seek = this->m_state->seek;
    AVFrame *frame = av_frame_alloc();
    AVFrame *swFrame = av_frame_alloc();
    AVCodecContext *ctx = video.stream.ctx;
    AVFrame first;
    AVFrame second;
    AVPacket *retainedPacket = av_packet_alloc();
#ifndef Q_OS_ANDROID
    AVFrame hwPicture;
#endif
    QVector<volatile bool*> quits;
    bool firstDiscard = true;
    bool allocedPicture = false;
    bool getPacket = true;
    int lastWidth = 0;
    int lastHeight = 0;
#ifndef Q_OS_ANDROID
    bool hwAllocedPicture = false;
    int hwLastWidth = 0;
    int hwLastHeight = 0;
#endif

    quits.append(&this->m_state->quit);
    quits.append(&this->m_state->video.threadQuit);

    while (true)
    {
        class localPtr
        {
        public:
            localPtr()
            {

            }

            ~localPtr()
            {
                av_packet_unref(this->packet.packet);
            }

            PacketQueue::Packet packet;
        }l;

        if (this->m_state->quit)
            break;

        if (this->m_state->video.threadQuit)
            break;

        bool block = true;
        bool isEmptyBuffer = false;
        bool isVideoEmpty = false;

        if (!video.stream.queue.hasPacket() && !this->m_state->pause.pause && !this->m_state->willBeEnd)
        {
            if (!this->m_presenter->isAudio())
            {
                isVideoEmpty = true;
                video.startTime = this->m_presenter->getAbsoluteClock();

                if (this->m_presenter->isRemoteProtocol())
                {
                    isEmptyBuffer = true;
                    this->m_presenter->callEmptyCallback(isEmptyBuffer);

                    while (this->m_presenter->isUseBufferingMode() && !this->m_presenter->isLive())
                    {
                        if (this->m_state->quit)
                            break;

                        if (this->m_state->video.threadQuit)
                            break;

                        if (video.stream.queue.hasPacketInPercent(0.5, MAX_VIDEO_QUEUE_SIZE))
                            break;

                        QThread::msleep(MediaPresenterInterface::EMPTY_BUFFER_WAIT_DELAY);
                    }
                }
            }
        }

        if (getPacket)
        {
            if (!video.stream.queue.get(&l.packet, quits, &block))
                break;

            if (isEmptyBuffer)
                this->m_presenter->callEmptyCallback(false);

            if (isVideoEmpty)
                video.driftTime += this->m_presenter->getAbsoluteClock() - video.startTime;

            if (video.stream.queue.isFlushPacket(&l.packet))
            {
                bool hwOpened = this->m_presenter->getHWDecoder().isOpened();

                if (!hwOpened)
                    avcodec_flush_buffers(ctx);

                this->m_state->videoFrames.flush();

                if (hwOpened)
                    this->m_presenter->getHWDecoder().flushSurfaceQueue();

                seek.flushed = true;
                seek.firstFrameAfterFlush = false;

                continue;
            }

            this->m_presenter->getRDetail().videoInputByteCount.fetchAndAddOrdered(l.packet.packet->size);
        }

        int success = 0;
        int decoded = 0;
        bool isDelayed = false;

#ifdef Q_OS_ANDROID
        if (this->m_presenter->getHWDecoder().isOpened())
        {
            bool ret = this->m_presenter->getHWDecoder().decodePicture(*l.packet.packet, frame);

            success = ret ? 1 : 0;
            decoded = success;
        }
        else
        {
#endif
            if (!getPacket)
            {
                av_packet_ref(l.packet.packet, retainedPacket);
                av_packet_unref(retainedPacket);
            }

            success = FrameUtils::decodeFrame(ctx, l.packet.packet, frame, &decoded);

            if ((decoded && success == 0) || (!decoded && success == AVERROR(EAGAIN)))
            {
                av_packet_ref(retainedPacket, l.packet.packet);
                getPacket = false;
            }
            else
            {
                getPacket = true;
            }
#ifdef Q_OS_ANDROID
        }
#endif

        if (decoded &&
            (this->m_presenter->getRDetail().videoInputType.isEmpty() ||
            (this->m_presenter->getFormat() != ctx->pix_fmt && this->m_presenter->isUseGPUConvert(ctx->pix_fmt))))
        {
            AVPixelFormat pixFormat = this->m_presenter->getHWDecoder().isOpened() ? this->m_presenter->getHWDecoder().getFormat() : ctx->pix_fmt;
            const int bufSize = 128;
            char buf[bufSize] = {0, };
            QStringList pixFMT = QString(av_get_pix_fmt_string(buf, bufSize, pixFormat)).split(" ", Qt::SkipEmptyParts);

            this->m_presenter->getRDetail().videoInputType = pixFMT[0].toUpper();
            this->m_presenter->getRDetail().videoInputBits = pixFMT[2].toInt();

            if (this->m_presenter->isUseGPUConvert(pixFormat))
                this->m_presenter->setFormat(pixFormat);

            pixFMT = QString(av_get_pix_fmt_string(buf, bufSize, this->m_presenter->getFormat())).split(" ", Qt::SkipEmptyParts);

            this->m_presenter->getRDetail().videoOutputType = pixFMT[0].toUpper();
            this->m_presenter->getRDetail().videoOutputBits = pixFMT[2].toInt();

            this->m_presenter->getDeinterlacer().setCodec(video.stream.ctx, pixFormat, video.stream.stream->time_base, ctx->framerate);
            this->m_presenter->getFilterGraph().setCodec(video.stream.ctx, pixFormat, this->m_presenter->getFormat(), video.stream.stream->time_base);

            video.pixFormat = pixFormat;
        }

        if (decoded &&
            this->m_presenter->getHWDecoder().isOpened() &&
            this->m_presenter->getRDetail().videoHWDecoder.isEmpty())
        {
            const AVHWAccel *hwaccel = ctx->hwaccel;
            QString desc;
            QString decoder;

            this->m_presenter->getHWDecoder().getDecoderDesc(&desc);

            if (hwaccel)
                decoder = QString("%1 (%2)").arg(hwaccel->name, desc);
            else
                decoder = QString("%1").arg(desc);

            this->m_presenter->getRDetail().videoHWDecoder = decoder;

#ifndef Q_OS_ANDROID
            if (!hwaccel)
                this->m_presenter->getHWDecoder().close();
#endif
        }

        if (l.packet.isNullPacket() && ctx->codec && ctx->codec->capabilities & AV_CODEC_CAP_DELAY)
            isDelayed = true;

        if (l.packet.discard)
        {
            if (firstDiscard)
            {
                seek.videoDiscardStartTime = this->m_presenter->getAbsoluteClock();
                firstDiscard = false;
            }

            l.packet.discard = false;

            continue;
        }

        if (!firstDiscard)
        {
            firstDiscard = true;
            seek.videoDiscardDriftTime += this->m_presenter->getAbsoluteClock() - seek.videoDiscardStartTime;

            if (this->m_presenter->getCurrentAudioStreamIndex() != -1)
                this->m_presenter->resumeAudio();
        }

        if ((success < 0 && !isDelayed) || !decoded)
            continue;

        if (video.frameDrop > 0)
        {
            this->m_presenter->getRDetail().videoFrameDropCount.fetchAndAddOrdered(1);
            video.frameDrop--;

            continue;
        }

        FrameMetaData frameMeta;

        for (int i = 0; i < frame->nb_side_data; i++)
        {
            AVFrameSideData *data = frame->side_data[i];

            if (data->type == AV_FRAME_DATA_MASTERING_DISPLAY_METADATA)
            {
                memcpy(&frameMeta.mdm, data->data, data->size);
                frameMeta.hasMDM = true;
            }
            else if (data->type == AV_FRAME_DATA_CONTENT_LIGHT_LEVEL)
            {
                memcpy(&frameMeta.clm, data->data, data->size);
                frameMeta.hasCLM = true;
            }
        }

        bool deinterlace = false;
        bool doubler = false;
        bool getFirstFrame = false;
        bool getSecondFrame = false;
        bool isDTV = false;

#if !defined Q_OS_ANDROID && !defined Q_OS_IOS
        isDTV = this->m_presenter->isDTV();
#endif

        if ((this->m_presenter->getDeinterlacer().getMethod() == AnyVODEnums::DM_AUTO && (frame->interlaced_frame || isDTV)) ||
            this->m_presenter->getDeinterlacer().getMethod() == AnyVODEnums::DM_USE)
        {
            deinterlace = true;
        }

        if (!this->m_presenter->getRDetail().videoInterlaced)
            this->m_presenter->getRDetail().videoInterlaced = (bool)frame->interlaced_frame;

        this->m_presenter->getRDetail().videoDeinterlaced = deinterlace;

        AVFrame org;
        AVPixelFormat format;
#ifdef Q_OS_ANDROID
        if (this->m_presenter->getHWDecoder().isOpened())
        {
            format = this->m_presenter->getHWDecoder().getFormat();

            if (format == AV_PIX_FMT_NONE)
                continue;

            memcpy(org.data, frame->data, sizeof(org.data));
            memcpy(org.linesize, frame->linesize, sizeof(org.linesize));
        }
        else
#else
        if (this->m_presenter->getHWDecoder().isOpened())
        {
            format = this->m_presenter->getHWDecoder().getFormat();

            if (format == AV_PIX_FMT_NONE)
                continue;

            if (this->m_presenter->getHWDecoder().isUseDefaultGetBuffer())
            {
                if (!this->m_presenter->getHWDecoder().copyPicture(*frame, swFrame))
                    continue;

                memcpy(org.data, swFrame->data, sizeof(org.data));
                memcpy(org.linesize, swFrame->linesize, sizeof(org.linesize));
            }
            else
            {
                if (hwLastWidth != frame->width || hwLastHeight != frame->height)
                {
                    if (hwAllocedPicture)
                        av_freep(&hwPicture.data[0]);

                    int align;
#ifdef Q_OS_MAC
                    align = 64;
#else
                    align = 16;
#endif
                    av_image_alloc(hwPicture.data, hwPicture.linesize, FFALIGN(frame->width, align), FFALIGN(frame->height, align), format, 1);
                    hwAllocedPicture = true;

                    hwLastWidth = frame->width;
                    hwLastHeight = frame->height;
                }

                AVFrame raw;

                memcpy(raw.data, frame->data, sizeof(raw.data));
                memcpy(raw.linesize, frame->linesize, sizeof(raw.linesize));

                if (!this->m_presenter->getHWDecoder().copyPicture(raw, &hwPicture))
                    continue;

                memcpy(org.data, hwPicture.data, sizeof(org.data));
                memcpy(org.linesize, hwPicture.linesize, sizeof(org.linesize));
            }
        }
        else
#endif
        {
            memcpy(org.data, frame->data, sizeof(org.data));
            memcpy(org.linesize, frame->linesize, sizeof(org.linesize));

            format = ctx->pix_fmt;
        }

        double pts;

        pts = av_q2d(video.stream.stream->time_base) * (frame->best_effort_timestamp == AV_NOPTS_VALUE ? frame->pts : frame->best_effort_timestamp);
        pts = this->m_presenter->synchronizeVideo(frame, pts, video.stream.ctx->framerate);

        if (deinterlace)
        {
            if (lastWidth != frame->width || lastHeight != frame->height)
            {
                if (allocedPicture)
                {
                    av_freep(&first.data[0]);
                    av_freep(&second.data[0]);
                }

                av_image_alloc(first.data, first.linesize, frame->width, frame->height, format, 1);
                av_image_alloc(second.data, second.linesize, frame->width, frame->height, format, 1);

                allocedPicture = true;

                lastWidth = frame->width;
                lastHeight = frame->height;
            }

            if (this->m_presenter->getDeinterlacer().isAVFilter())
            {
                AVFrame *to;
                bool hwOpened = this->m_presenter->getHWDecoder().isOpened();

                if (hwOpened)
                {
                    to = av_frame_alloc();

                    to->width  = frame->width;
                    to->height = frame->height;
                    to->format = format;

                    av_frame_get_buffer(to, 16);
                    av_frame_copy_props(to, frame);

                    av_image_copy(to->data, to->linesize, (const uint8_t**)org.data, org.linesize, format, frame->width, frame->height);

                    to->interlaced_frame = frame->interlaced_frame;
                    to->top_field_first = frame->top_field_first;
                }
                else
                {
                    to = frame;
                }

                doubler = this->m_presenter->getDeinterlacer().deinterlace(&first, &getFirstFrame, &second, &getSecondFrame, to);

                if (hwOpened)
                    av_frame_free(&to);
            }
            else
            {
                av_image_copy(first.data, first.linesize, (const uint8_t**)org.data, org.linesize, format, frame->width, frame->height);

                doubler = this->m_presenter->getDeinterlacer().deinterlace(&first, &second, frame->height, format);

                getFirstFrame = true;

                if (doubler)
                    getSecondFrame = true;
            }
        }

        if (doubler)
        {
            if (getFirstFrame)
            {
                if (!this->m_presenter->convertPicture(frame->top_field_first ? first : second, pts, format, false, frameMeta))
                    break;
            }

            if (getSecondFrame)
            {
                if (this->m_state->seek.firstFrameAfterFlush && !this->m_state->seek.pauseSeeking)
                {
                    double diff = pts - this->m_state->frameTimer.lastRealPTS;
                    double nextPTS = pts + (diff / 2.0);

                    if (!this->m_presenter->convertPicture(frame->top_field_first ? second : first, nextPTS, format, false, frameMeta))
                        break;
                }
            }
        }
        else if (deinterlace)
        {
            if (getFirstFrame)
            {
                if (!this->m_presenter->convertPicture(first, pts, format, false, frameMeta))
                    break;
            }
        }
        else
        {
            bool pageFlip = false;
            bool leftOrTop = false;

            switch (this->m_presenter->get3DMethod())
            {
                case AnyVODEnums::V3M_PAGE_FLIP_LEFT_RIGHT_LEFT_PRIOR:
                case AnyVODEnums::V3M_PAGE_FLIP_TOP_BOTTOM_TOP_PRIOR:
                {
                    pageFlip = true;
                    leftOrTop = true;

                    break;
                }
                case AnyVODEnums::V3M_PAGE_FLIP_LEFT_RIGHT_RIGHT_PRIOR:
                case AnyVODEnums::V3M_PAGE_FLIP_TOP_BOTTOM_BOTTOM_PRIOR:
                {
                    pageFlip = true;
                    leftOrTop = false;

                    break;
                }
                default:
                {
                    break;
                }
            }

            if (pageFlip)
            {
                if (!this->m_presenter->convertPicture(org, pts, format, leftOrTop, frameMeta))
                    break;

                if (this->m_state->seek.firstFrameAfterFlush && !this->m_state->seek.pauseSeeking)
                {
                    double diff = pts - this->m_state->frameTimer.lastRealPTS;
                    double nextPTS = pts + (diff / 2.0);

                    if (!this->m_presenter->convertPicture(org, nextPTS, format, !leftOrTop, frameMeta))
                        break;
                }
            }
            else
            {
                if (!this->m_presenter->convertPicture(org, pts, format, false, frameMeta))
                    break;
            }
        }

        this->m_state->frameTimer.lastRealPTS = pts;
    }

    if (allocedPicture)
    {
        av_freep(&first.data[0]);
        av_freep(&second.data[0]);
    }

#ifndef Q_OS_ANDROID
    if (hwAllocedPicture)
        av_freep(&hwPicture.data[0]);
#endif

    av_packet_free(&retainedPacket);
    av_frame_free(&frame);
    av_frame_free(&swFrame);
}
