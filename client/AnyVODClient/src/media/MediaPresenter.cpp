﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#ifndef UNICODE
# define UNICODE
#endif

#ifndef _UNICODE
# define _UNICODE
#endif

#include "MediaPresenter.h"
#include "MediaState.h"
#include "Surface.h"
#include "FrameExtractor.h"
#include "core/Common.h"
#include "core/Lyrics.h"
#include "core/SystemResourceUsage.h"
#include "device/DTVReader.h"
#include "device/RadioReader.h"
#include "utils/SeparatingUtils.h"
#include "utils/PathUtils.h"
#include "utils/ConvertingUtils.h"
#include "utils/FloatPointUtils.h"
#include "utils/MediaTypeUtils.h"
#include "utils/StringUtils.h"
#include "utils/PixelUtils.h"
#include "utils/RemoteFileUtils.h"
#include "utils/DeviceUtils.h"
#include "net/VirtualFile.h"
#include "audio/SPDIF.h"
#include "parsers/playlist/CueParser.h"

extern "C"
{
# include <libavutil/time.h>
# include <libavutil/imgutils.h>
# include <libavutil/opt.h>

# include <libswscale/swscale.h>
#if !defined Q_OS_MOBILE
# include <libavdevice/avdevice.h>
#endif
}

#ifndef Q_OS_MAC
# include <omp.h>
#endif

#if !defined Q_OS_MOBILE
# include <MediaInfoDLL/MediaInfo.h>
#endif

#include <qmath.h>
#include <QDir>
#include <QTemporaryFile>

#include <QDebug>

#if !defined Q_OS_MOBILE
using namespace MediaInfoDLL;
#endif

const int MediaPresenter::OPTION_DESC_TIME = 3000;
const int MediaPresenter::WATCHER_DELAY = 500;
const int MediaPresenter::DEFAULT_REFRESH_DELAY = 40;
const int MediaPresenter::FIRST_REFRESH_DELAY = 1;
const int MediaPresenter::NO_AUDIO_ALBUM_JACKET_DELAY = 100;
const int MediaPresenter::FRAME_LOW_WARNING_THRESHOLD = 20;

const double MediaPresenter::SYNC_THRESHOLD_MULTI = 3.0;
const double MediaPresenter::SYNC_THRESHOLD = 0.008;

const QString MediaPresenter::SUBTITLE_CODEC_FORMAT = "%1 (%2)";

QDataStream& operator << (QDataStream &out, const MediaPresenter::Range &item)
{
    out << item.start;
    out << item.end;
    out << item.enable;

    return out;
}

QDataStream& operator >> (QDataStream &in, MediaPresenter::Range &item)
{
    in >> item.start;
    in >> item.end;
    in >> item.enable;

    return in;
}

#ifdef Q_OS_ANDROID
static void ff_log_callback(void*, int level, const char *fmt, va_list vl)
{
    if (level <= av_log_get_level())
    {
        char buf[1024];

        vsnprintf(buf, sizeof(buf), fmt, vl);
    }
}
#endif

MediaPresenter::MediaPresenter() :
    QThread(),
    m_forceExit(false),
    m_state(nullptr),
    m_subtitleSync(0.0),
    m_isRemoteFile(false),
    m_isRemoteProtocol(false),
    m_isDTV(false),
    m_isRadio(false),
    m_isDevice(false),
    m_isLive(false),
    m_lastVideoStream(-1),
    m_lastAudioStream(-1),
    m_lastSubtitleStream(-1),
    m_isAudioExt(false),
    m_seekKeyFrame(true),
    m_skipOpening(false),
    m_skipEnding(false),
    m_useSkipRange(false),
#if defined Q_OS_MOBILE
    m_useHWDecoder(true),
    m_useFrameDrop(true),
#else
    m_useHWDecoder(false),
    m_useFrameDrop(false),
#endif
    m_useBufferingMode(false),
    m_enableSearchSubtitle(false),
    m_enableSearchLyrics(false),
    m_autoSaveSearchLyrics(false),
    m_showAlbumJacket(true),
    m_videoThread(this),
    m_subtitleThread(this),
    m_readThread(this),
    m_refreshThread(this),
    m_audioRenderer(this),
    m_videoRenderer(this),
    m_controlLocker(QMutex::Recursive),
    m_subtitleType(ST_NONE),
    m_virtualFile(VirtualFile::getInstance()),
    m_dtvReader(DTVReader::getInstance()),
    m_radioReader(RadioReader::getInstance())
{
    this->reset3DSubtitleOffset();

    RestartVideoCallback rcb;

    rcb.callback = MediaPresenter::restartVideo;
    rcb.userData = this;
    this->m_videoRenderer.setRestartVideoCallback(rcb);
}

MediaPresenter::~MediaPresenter()
{
    this->close();
    this->clearFrameBuffers();
}

void MediaPresenter::init()
{
    av_log_set_level(AV_LOG_QUIET);

#ifdef Q_OS_ANDROID
    av_log_set_callback(ff_log_callback);
#endif

    ffurl_append_protocol(DTVReader::getProtocol());
    ffurl_append_protocol(RadioReader::getProtocol());
    ffurl_append_protocol(VirtualFile::getProtocol());

#if !defined Q_OS_MOBILE
    avdevice_register_all();
#endif

    avformat_network_init();
}

void MediaPresenter::deInit()
{
    avformat_network_deinit();
}

bool MediaPresenter::open(const QString &filePath, const QString &title, const ExtraPlayData &data,
                          const QString &fontFamily, const int fontSize, const int subtitleOutlineSize,
                          const QString &audioPath)
{
    this->m_filePath = filePath;
    this->m_audioPath = audioPath;
    this->m_isRemoteFile = RemoteFileUtils::determinRemoteFile(filePath);
    this->m_isRemoteProtocol = RemoteFileUtils::determinRemoteProtocol(filePath);
    this->m_isLive = filePath.startsWith(LIVE);
    this->m_realFilePath = SeparatingUtils::removeFFMpegSeparator(filePath);
    this->m_title = title;
    this->m_artist.clear();

#if !defined Q_OS_ANDROID && !defined Q_OS_IOS
    this->m_isDTV = DTVReader::determinDTV(filePath);
    this->m_isRadio = RadioReader::determinRadio(filePath);
    this->m_isDevice = DeviceUtils::determinDevice(filePath);
#endif

    if (!data.isValid())
    {
#if !defined Q_OS_MOBILE
        MediaInfo mi;

        if (!this->m_isDevice && !this->m_isRemoteProtocol && mi.Open(filePath.toStdWString()) == 1)
        {
            QString totalTime = QString::fromStdWString(mi.Get(Stream_General, 0, __T("Duration")));
            QString totalFrame = QString::fromStdWString(mi.Get(Stream_Video, 0, __T("FrameCount")));

            if (totalTime.toDouble() < 0.0)
                this->m_playData.duration = 0.0;
            else
                this->m_playData.duration = totalTime.toDouble() / 1000.0;

            this->m_playData.totalFrame = totalFrame.toUInt();

            if (this->m_playData.totalFrame <= 0)
            {
                QString frameRate = QString::fromStdWString(mi.Get(Stream_Video, 0, __T("FrameRate")));

                if (frameRate.isEmpty())
                    frameRate = QString::fromStdWString(mi.Get(Stream_Video, 0, __T("FrameRate_Original")));

                if (frameRate.toDouble() < 0)
                    this->m_playData.totalFrame = 0;
                else
                    this->m_playData.totalFrame = uint32_t(frameRate.toDouble() * this->m_playData.duration);
            }

            QString rotation = QString::fromStdWString(mi.Get(Stream_Video, 0, __T("Rotation")));

            this->m_videoRenderer.setRotation(rotation.toDouble());
            this->m_artist = QString::fromStdWString(mi.Get(Stream_General, 0, __T("Artist")));

            mi.Close();
        }
#endif
        this->m_playData.query = data.query;
        this->m_playData.userData = data.userData;
    }
    else
    {
        this->m_playData = data;
    }

    this->m_videoRenderer.setFontFamily(fontFamily);
    this->m_videoRenderer.setFontSize(fontSize);
    this->m_videoRenderer.setFontOutlineSize(1);
    this->m_videoRenderer.setSubtitleMaxOutlineSize(subtitleOutlineSize);

    QFileInfo fileInfo(this->m_realFilePath);

    this->m_isAudioExt = MediaTypeUtils::isExtension(fileInfo.suffix(), MT_AUDIO);

    return true;
}

void MediaPresenter::openRemoteSubtitle(const QString &filePath)
{
    this->m_virtualFile.loadSubtitle(filePath, this);
}

bool MediaPresenter::saveSubtitleAs(const QString &filePath)
{
    bool success = false;
    double sync = -this->m_subtitleSync;

    if (this->m_samiParser.isExist())
        success = this->m_samiParser.save(filePath, sync);
    else if (this->m_srtParser.isExist())
        success = this->m_srtParser.save(filePath, sync);
    else if (this->m_lrcParser.isExist())
        success = this->m_lrcParser.save(filePath, sync);
    else if (this->m_youtubeParser.isExist())
        success = this->m_youtubeParser.save(filePath, sync);
    else if (this->m_naverTVParser.isExist())
        success = this->m_naverTVParser.save(filePath, sync);

    return success;
}

bool MediaPresenter::saveSubtitle()
{
    return this->saveSubtitleAs(this->m_subtitleFilePath);
}

QString MediaPresenter::getSubtitlePath() const
{
    return this->m_subtitleFilePath;
}

bool MediaPresenter::openSubtitle(const QString &filePath, bool isExternal)
{
    bool success = false;
    QFileInfo info(filePath);
    QString ext = info.suffix().toLower();

    if (ext == "smi" || ext == "sami")
        success = this->openSAMI(filePath) || this->openSRT(filePath);
    else if (ext == "ass" || ext == "ssa")
        success = this->openASS(filePath);
    else if (ext == "srt")
        success = this->openSRT(filePath) || this->openSAMI(filePath);
    else if (ext == "lrc")
        success = this->openLRC(filePath);

    if (!success)
        success = this->openAVParser(filePath);

    if (!success && isExternal)
    {
        success = this->openYouTube(filePath);

        if (!success)
            this->openNaverTV(filePath);
    }

    this->m_subtitleFilePath = filePath;

    return success;
}

bool MediaPresenter::openSAMI(const QString &filePath)
{
    if (this->m_samiParser.open(filePath))
    {
        this->m_detail.subtitleCodec = "SAMI Parser";
        this->m_subtitleType = ST_SAMI;

        return true;
    }

    return false;
}

bool MediaPresenter::openASS(const QString &filePath)
{
    if (this->m_assParser.open(filePath))
    {
        QFileInfo info(filePath);

        if (info.suffix().toLower() == "ass")
            this->m_detail.subtitleCodec = "ASS Parser";
        else if (info.suffix().toLower() == "ssa")
            this->m_detail.subtitleCodec = "SSA Parser";
        else
            this->m_detail.subtitleCodec = "Unknown Parser";

        this->m_assParser.setDefaultFont(this->m_assFontFamily);
        this->m_subtitleType = ST_ASS;

        return true;
    }

    return false;
}

bool MediaPresenter::openSRT(const QString &filePath)
{
    if (this->m_srtParser.open(filePath))
    {
        this->m_detail.subtitleCodec = "SRT Parser";
        this->m_subtitleType = ST_SRT;

        return true;
    }

    return false;
}

bool MediaPresenter::openYouTube(const QString &path)
{
    if (this->m_youtubeParser.open(path))
    {
        this->m_detail.subtitleCodec = "YouTube Parser";
        this->m_subtitleType = ST_YOUTUBE;

        return true;
    }

    return false;
}

bool MediaPresenter::openNaverTV(const QString &path)
{
    if (this->m_naverTVParser.open(path))
    {
        this->m_detail.subtitleCodec = "NaverTV Parser";
        this->m_subtitleType = ST_NAVER_TV;

        return true;
    }

    return false;
}

bool MediaPresenter::isRemoteSubtitle(MediaPresenter::SubtitleType type) const
{
    return type == ST_YOUTUBE || type == ST_NAVER_TV;
}

bool MediaPresenter::openLRC(const QString &filePath)
{
    if (this->m_lrcParser.open(filePath))
    {
        this->m_detail.subtitleCodec = "LRC Parser";
        this->m_subtitleType = ST_LRC;

        return true;
    }

    return false;
}

bool MediaPresenter::openAVParser(const QString &filePath)
{
    QString fontPath;

    this->m_assParser.getFontPath(&fontPath);

    if (this->m_avParser.open(filePath, fontPath, this->m_videoRenderer.getFontFamily(), 0))
    {
        this->m_avParser.setCurrentIndex(0);
        this->m_detail.subtitleCodec = "AV Parser";
        this->m_subtitleType = ST_AV;

        return true;
    }

    return false;
}

void MediaPresenter::closeAllExternalSubtitles()
{
    this->m_assParser.close();
    this->m_samiParser.close();
    this->m_srtParser.close();
    this->m_youtubeParser.close();
    this->m_naverTVParser.close();
    this->m_lrcParser.close();
    this->m_avParser.close();

    this->m_subtitleFilePath.clear();
    this->m_subtitleType = ST_NONE;

    this->m_detail.subtitleBitmap = false;
    this->m_detail.subtitleValidColor = true;

    MediaState *ms = this->m_state;

    if (ms && ms->subtitle.stream.stream)
    {
        const AVCodec *codec = ms->subtitle.stream.ctx->codec;

        this->m_detail.subtitleCodec = QString(SUBTITLE_CODEC_FORMAT).arg(QString(codec->name).toUpper(), codec->long_name);
    }
}

void MediaPresenter::close()
{
    QMutexLocker locker(&this->m_controlLocker);

    this->m_forceExit = true;

    if (this->isRunning())
        this->wait();

    this->closeInternal();

    this->m_playData = ExtraPlayData();
}

void MediaPresenter::setDevicePixelRatio(double ratio)
{
    this->m_videoRenderer.setDevicePixelRatio(ratio);
}

void MediaPresenter::closeInternal()
{
    if (this->m_state)
        this->m_state->quit = true;

    this->m_forceExit = false;
    this->closeStream();
    this->closeAllExternalSubtitles();

    this->callEmptyCallbackInternal(false);
    this->m_isRemoteFile = false;
    this->m_videoStreamInfo.clear();
    this->m_audioStreamInfo.clear();
    this->m_subtitleStreamInfo.clear();
    this->m_detail = Detail();
    this->m_repeatRange = Range();
    this->m_GOMSubtitleURL.clear();
    this->m_videoRenderer.tearDown();
}

#if defined Q_OS_MOBILE
void MediaPresenter::setGL(QOpenGLFunctions *gl)
{
    this->m_videoRenderer.setGL(gl);
}
#endif

bool MediaPresenter::resetScreen(const int width, const int height, TextureInfo *texInfo, bool inContext)
{
    return this->m_videoRenderer.resetScreen(width, height, texInfo, inContext);
}

bool MediaPresenter::play()
{
    QMutexLocker locker(&this->m_controlLocker);
    bool success = false;

    success = this->openStream();

    if (success)
    {
        this->start();
    }
    else
    {
        this->m_state->quit = true;
        this->closeStream();
    }

    return success;
}

void MediaPresenter::pause()
{
    QMutexLocker locker(&this->m_controlLocker);
    MediaState *ms = this->m_state;

    if (ms && !ms->pause.pause)
    {
        ms->pause.pause = true;
        ms->pause.startTime = this->getAbsoluteClock();
        ms->pause.lastPausedTime = ms->frameTimer.lastPTS;

        this->m_audioRenderer.pause();
    }
}

void MediaPresenter::resume()
{
    QMutexLocker locker(&this->m_controlLocker);
    MediaState *ms = this->m_state;

    if (ms && ms->pause.pause)
    {
        ms->pause.driftTime += this->getAbsoluteClock() - ms->pause.startTime;
        ms->pause.pause = false;
        ms->seek.inc = 0.0;

        this->m_audioRenderer.resume();
    }

    if (ms)
        ms->willBeEnd = false;
}

void MediaPresenter::stop()
{
    this->close();
}

bool MediaPresenter::isPlayUserDataEmpty() const
{
    return this->m_playData.userData.isEmpty();
}

QString MediaPresenter::getTitle() const
{
    return this->m_title;
}

QString MediaPresenter::getArtist() const
{
    return this->m_artist;
}

void MediaPresenter::prevFrame(int count)
{
    QMutexLocker locker(&this->m_controlLocker);
    MediaState *ms = this->m_state;

    if (ms)
    {
        double clock = ms->pause.pause ? ms->pause.lastPausedTime : ms->frameTimer.lastPTS;
        double frameTime = this->getDuration() / this->m_detail.videoTotalFrame;
        double target = clock - (count + 1) * frameTime;

        if (!ms->pause.pause)
            this->pause();

        this->seekStream(target, 0.0, AVSEEK_FLAG_ANY | AVSEEK_FLAG_BACKWARD);
    }
}

void MediaPresenter::nextFrame(int count)
{
    MediaState *ms = this->m_state;

    if (ms)
    {
        ms->seek.requestPauseOnRender = true;
        ms->seek.pauseOnRenderCount = count;

        this->resume();
    }
}

bool MediaPresenter::render()
{
    QMutexLocker locker(&this->m_controlLocker);

    return this->update();
}

void MediaPresenter::setSubtitleSync(double value)
{
    this->m_subtitleSync = value;
}

double MediaPresenter::getSubtitleSync() const
{
    return this->m_subtitleSync;
}

void MediaPresenter::setAudioSync(double value)
{
    this->m_audioRenderer.setSync(value);
}

double MediaPresenter::getAudioSync() const
{
    return this->m_audioRenderer.getSync();
}

void MediaPresenter::showDetail(bool show)
{
    this->m_videoRenderer.showDetail(show);
}

bool MediaPresenter::isShowDetail() const
{
    return this->m_videoRenderer.isShowDetail();
}

const Detail& MediaPresenter::getDetail() const
{
    return this->m_detail;
}

bool MediaPresenter::setAudioDevice(int device)
{
    this->m_audioRenderer.setDevice(device);

    if (!SPDIF::getInstance().isOpened())
        return this->setAudioDeviceAfter();
    else
        return true;
}

int MediaPresenter::getCurrentAudioDevice() const
{
    return this->m_audioRenderer.getCurrentDevice();
}

bool MediaPresenter::setAudioDeviceAfter()
{
    if (this->resetAudioStream())
        return true;

    if (this->m_state)
        this->m_state->audio.stream.index = this->m_lastAudioStream;

    return false;
}

void MediaPresenter::getSPDIFAudioDevices(QStringList *ret)
{
    SPDIF::getInstance().getDeviceList(ret);
}

bool MediaPresenter::setSPDIFAudioDevice(int device)
{
    SPDIF &spdif = SPDIF::getInstance();

    if (spdif.setDevice(device))
    {
        if (spdif.isOpened())
            return this->setAudioDeviceAfter();
        else
            return true;
    }

    return false;
}

int MediaPresenter::getCurrentSPDIFAudioDevice() const
{
    return SPDIF::getInstance().getDevice();
}

void MediaPresenter::showSubtitle(bool show)
{
    this->m_videoRenderer.showSubtitle(show);
}

bool MediaPresenter::isShowSubtitle() const
{
    return this->m_videoRenderer.isShowSubtitle();
}

bool MediaPresenter::existFileSubtitle()
{
    return this->m_samiParser.isExist() ||
            this->m_srtParser.isExist() ||
            this->m_youtubeParser.isExist() ||
            this->m_naverTVParser.isExist() ||
            this->m_lrcParser.isExist();
}

bool MediaPresenter::existSubtitle()
{
    return (this->m_state && this->m_state->subtitle.stream.stream) ||
            this->m_samiParser.isExist() ||
            this->m_assParser.isExist() ||
            this->m_srtParser.isExist() ||
            this->m_youtubeParser.isExist() ||
            this->m_naverTVParser.isExist() ||
            this->m_lrcParser.isExist() ||
            this->m_avParser.isExist();
}

bool MediaPresenter::isDevice() const
{
    return this->m_isDevice;
}

bool MediaPresenter::existExternalSubtitle()
{
    return this->m_samiParser.isExist() ||
            this->m_assParser.isExist() ||
            this->m_srtParser.isExist() ||
            this->m_youtubeParser.isExist() ||
            this->m_naverTVParser.isExist() ||
            this->m_lrcParser.isExist() ||
            this->m_avParser.isExist();
}

bool MediaPresenter::isValidSubtitleWithoutClass()
{
    QString className;

    this->getCurrentSubtitleClass(&className);

    if (this->m_youtubeParser.isExist())
        return !className.isEmpty();

    return true;
}

double MediaPresenter::getRotation() const
{
    return this->m_videoRenderer.getRotation();
}

void MediaPresenter::setSensorRotation(double rot)
{
    this->m_videoRenderer.setSensorRotation(rot);
}

bool MediaPresenter::isAlignable()
{
    return (this->m_state && this->m_state->subtitle.stream.stream &&
            this->m_state->subtitle.stream.ctx->subtitle_header_size <= 0 &&
            this->m_state->subtitle.isBitmap == false) ||
            this->m_samiParser.isExist() ||
            this->m_youtubeParser.isExist() ||
            this->m_naverTVParser.isExist() ||
            this->m_srtParser.isExist();
}

AnyVODEnums::HAlignMethod MediaPresenter::getHAlign() const
{
    return this->m_videoRenderer.getHAlign();
}

void MediaPresenter::setHAlign(AnyVODEnums::HAlignMethod align)
{
    this->m_videoRenderer.setHAlign(align);
}

AnyVODEnums::VAlignMethod MediaPresenter::getVAlign() const
{
    return this->m_videoRenderer.getVAlign();
}

void MediaPresenter::setVAlign(AnyVODEnums::VAlignMethod align)
{
    this->m_videoRenderer.setVAlign(align);
}

bool MediaPresenter::existAudioSubtitle()
{
    return this->m_lrcParser.isExist();
}

bool MediaPresenter::existAudioSubtitleGender() const
{
    return this->m_lrcParser.isGenderExist();
}

void MediaPresenter::setSubtitleURL(const QString &url)
{
    this->m_GOMSubtitleURL = url;
}

void MediaPresenter::getSubtitleURL(QString *ret) const
{
    *ret = this->m_GOMSubtitleURL;
}

void MediaPresenter::setASSFontPath(const QString &path)
{
    this->m_assParser.setFontPath(path);
}

void MediaPresenter::setASSFontFamily(const QString &family)
{
    this->m_assFontFamily = family;
}

void MediaPresenter::getSubtitleClasses(QStringList *classNames)
{
    if (this->m_samiParser.isExist())
    {
        this->m_samiParser.getClassNames(classNames);
    }
    else if (this->m_youtubeParser.isExist())
    {
        this->m_youtubeParser.getLanguages(classNames);
    }
    else if (this->m_naverTVParser.isExist())
    {
        this->m_naverTVParser.getLanguages(classNames);
    }
    else if (this->m_avParser.isExist())
    {
        QVector<SubtitleStreamInfo> infos;

        this->m_avParser.getStreamInfos(&infos);

        for (int i = 0; i < infos.count(); i++)
            classNames->append(infos[i].name);
    }
    else
    {
        for (int i = 0; i < this->m_subtitleStreamInfo.count(); i++)
            classNames->append(this->m_subtitleStreamInfo[i].name);
    }
}

void MediaPresenter::getCurrentSubtitleClass(QString *className)
{
    if (this->m_samiParser.isExist())
    {
        this->m_samiParser.getDefaultClassName(className);
    }
    else if (this->m_youtubeParser.isExist())
    {
        this->m_youtubeParser.getDefaultLanguage(className);
    }
    else if (this->m_naverTVParser.isExist())
    {
        this->m_naverTVParser.getDefaultLanguage(className);
    }
    else if (this->m_avParser.isExist())
    {
        this->m_avParser.getCurrentName(className);
    }
    else
    {
        if (!this->m_state)
            return;

        QString name;

        for (int i = 0; i < this->m_subtitleStreamInfo.count(); i++)
        {
            if (this->m_subtitleStreamInfo[i].index == this->m_state->subtitle.stream.index)
            {
                name = this->m_subtitleStreamInfo[i].name;
                break;
            }
        }

        *className = name;
    }
}

bool MediaPresenter::setCurrentSubtitleClass(const QString &className)
{
    if (this->m_samiParser.isExist())
    {
        this->m_samiParser.setDefaultClassName(className);
        return true;
    }
    else if (this->m_youtubeParser.isExist())
    {
        this->m_youtubeParser.setDefaultLanguage(className);
        return true;
    }
    else if (this->m_naverTVParser.isExist())
    {
        this->m_naverTVParser.setDefaultLanguage(className);
        return true;
    }
    else if (this->m_avParser.isExist())
    {
        QVector<SubtitleStreamInfo> infos;
        unsigned int index = 0;

        this->m_avParser.getStreamInfos(&infos);

        for (int i = 0; i < infos.count(); i++)
        {
            if (infos[i].name == className)
            {
                index = infos[i].index;
                break;
            }
        }

        this->m_avParser.setCurrentIndex(index);

        return true;
    }
    else
    {
        if (!this->m_state)
            return false;

        int index = -1;

        for (int i = 0; i < this->m_subtitleStreamInfo.count(); i++)
        {
            if (this->m_subtitleStreamInfo[i].name == className)
            {
                index = this->m_subtitleStreamInfo[i].index;
                break;
            }
        }

        Subtitle &subtitle = this->m_state->subtitle;

        subtitle.stream.queue.flush();
        subtitle.requestReleaseQueue = true;

        return this->changeStream(index, subtitle.stream.index, false, &this->m_lastSubtitleStream, &subtitle.threadQuit);
    }
}

void MediaPresenter::resetSubtitlePosition()
{
    this->m_videoRenderer.resetSubtitlePosition();
}

void MediaPresenter::addVerticalSubtitlePosition(int pos)
{
    this->m_videoRenderer.addVerticalSubtitlePosition(pos);
}

void MediaPresenter::addHorizontalSubtitlePosition(int pos)
{
    this->m_videoRenderer.addHorizontalSubtitlePosition(pos);
}

void MediaPresenter::setVerticalSubtitlePosition(int pos)
{
    this->m_videoRenderer.setVerticalSubtitlePosition(pos);
}

void MediaPresenter::setHorizontalSubtitlePosition(int pos)
{
    this->m_videoRenderer.setHorizontalSubtitlePosition(pos);
}

int MediaPresenter::getVerticalSubtitlePosition() const
{
    return this->m_videoRenderer.getVerticalSubtitlePosition();
}

int MediaPresenter::getHorizontalSubtitlePosition() const
{
    return this->m_videoRenderer.getHorizontalSubtitlePosition();
}

void MediaPresenter::reset3DSubtitleOffset()
{
    this->m_videoRenderer.reset3DSubtitleOffset();
}

void MediaPresenter::addVertical3DSubtitleOffset(int pos)
{
    this->m_videoRenderer.addVertical3DSubtitleOffset(pos);
}

void MediaPresenter::addHorizontal3DSubtitleOffset(int pos)
{
    this->m_videoRenderer.addHorizontal3DSubtitleOffset(pos);
}

void MediaPresenter::setVertical3DSubtitleOffset(int pos)
{
    this->m_videoRenderer.setVertical3DSubtitleOffset(pos);
}

void MediaPresenter::setHorizontal3DSubtitleOffset(int pos)
{
    this->m_videoRenderer.setHorizontal3DSubtitleOffset(pos);
}

int MediaPresenter::getVertical3DSubtitleOffset() const
{
    return this->m_videoRenderer.getVertical3DSubtitleOffset();
}

int MediaPresenter::getHorizontal3DSubtitleOffset() const
{
    return this->m_videoRenderer.getHorizontal3DSubtitleOffset();
}

void MediaPresenter::resetVirtual3DDepth()
{
    this->m_videoRenderer.resetVirtual3DDepth();
}

void MediaPresenter::setVirtual3DDepth(qreal depth)
{
    this->m_videoRenderer.setVirtual3DDepth(depth);
}

qreal MediaPresenter::getVirtual3DDepth() const
{
    return this->m_videoRenderer.getVirtual3DDepth();
}

void MediaPresenter::setRepeatStart(double start)
{
    this->m_repeatRange.start = start;
}

void MediaPresenter::setRepeatEnd(double end)
{
    this->m_repeatRange.end = end;
}

void MediaPresenter::setRepeatEnable(bool enable)
{
    this->m_repeatRange.enable = enable;
}

bool MediaPresenter::getRepeatEnable() const
{
    return this->m_repeatRange.enable;
}

double MediaPresenter::getRepeatStart() const
{
    return this->m_repeatRange.start;
}

double MediaPresenter::getRepeatEnd() const
{
    return this->m_repeatRange.end;
}

void MediaPresenter::setCaptureMode(bool capture)
{
    this->m_videoRenderer.setCaptureMode(capture);
}

bool MediaPresenter::getCaptureMode() const
{
    return this->m_videoRenderer.getCaptureMode();
}

void MediaPresenter::setSeekKeyFrame(bool keyFrame)
{
    this->m_seekKeyFrame = keyFrame;
}

bool MediaPresenter::isSeekKeyFrame() const
{
    return this->m_seekKeyFrame;
}

void MediaPresenter::set3DMethod(AnyVODEnums::Video3DMethod method)
{
    this->m_videoRenderer.set3DMethod(method);
}

AnyVODEnums::Video3DMethod MediaPresenter::get3DMethod() const
{
    return this->m_videoRenderer.get3DMethod();
}

void MediaPresenter::setSubtitle3DMethod(AnyVODEnums::Subtitle3DMethod method)
{
    this->m_videoRenderer.setSubtitle3DMethod(method);
}

AnyVODEnums::Subtitle3DMethod MediaPresenter::getSubtitle3DMethod() const
{
    return this->m_videoRenderer.getSubtitle3DMethod();
}

void MediaPresenter::setVRInputSource(AnyVODEnums::VRInputSource source)
{
    this->m_videoRenderer.setVRInputSource(source);
}

AnyVODEnums::VRInputSource MediaPresenter::getVRInputSource() const
{
    return this->m_videoRenderer.getVRInputSource();
}

void MediaPresenter::setDistortionAdjustMode(AnyVODEnums::DistortionAdjustMode mode)
{
    this->m_videoRenderer.setDistortionAdjustMode(mode);
}

AnyVODEnums::DistortionAdjustMode MediaPresenter::getDistortionAdjustMode() const
{
    return this->m_videoRenderer.getDistortionAdjustMode();
}

void MediaPresenter::setSkipRanges(const QVector<Range> &ranges)
{
    this->m_skipRanges = ranges;
}

void MediaPresenter::getSkipRanges(QVector<Range> *ret) const
{
    *ret = this->m_skipRanges;
}

void MediaPresenter::setSkipOpening(bool skip)
{
    this->m_skipOpening = skip;
}

bool MediaPresenter::getSkipOpening() const
{
    return this->m_skipOpening;
}

void MediaPresenter::setSkipEnding(bool skip)
{
    this->m_skipEnding = skip;
}

bool MediaPresenter::getSkipEnding() const
{
    return this->m_skipEnding;
}

void MediaPresenter::setUseSkipRange(bool use)
{
    this->m_useSkipRange = use;
}

bool MediaPresenter::getUseSkipRange() const
{
    return this->m_useSkipRange;
}

double MediaPresenter::getOpeningSkipTime() const
{
    for (int i = 0; i < this->m_skipRanges.count(); i++)
    {
        if (this->m_skipRanges[i].start < 0.0)
            return this->m_skipRanges[i].end;
    }

    return 0.0;
}

double MediaPresenter::getEndingSkipTime() const
{
    for (int i = 0; i < this->m_skipRanges.count(); i++)
    {
        if (this->m_skipRanges[i].end < 0.0)
            return this->m_skipRanges[i].start;
    }

    return 0.0;
}

void MediaPresenter::useNormalizer(bool use)
{
    this->m_audioRenderer.useNormalizer(use);
}

bool MediaPresenter::isUsingNormalizer() const
{
    return this->m_audioRenderer.isUsingNormalizer();
}

void MediaPresenter::useEqualizer(bool use)
{
    this->m_audioRenderer.useEqualizer(use);
}

bool MediaPresenter::isUsingEqualizer() const
{
    return this->m_audioRenderer.isUsingEqualizer();
}

void MediaPresenter::useLowerVoice(bool use)
{
    this->m_audioRenderer.useLowerVoice(use);
}

bool MediaPresenter::isUsingLowerVoice() const
{
    return this->m_audioRenderer.isUsingLowerVoice();
}

void MediaPresenter::useHigherVoice(bool use)
{
    this->m_audioRenderer.useHigherVoice(use);
}

bool MediaPresenter::isUsingHigherVoice() const
{
    return this->m_audioRenderer.isUsingHigherVoice();
}

void MediaPresenter::useLowerMusic(bool use)
{
    this->m_audioRenderer.useLowerMusic(use);
}

bool MediaPresenter::isUsingLowerMusic() const
{
    return this->m_audioRenderer.isUsingLowerMusic();
}

void MediaPresenter::setSubtitleOpaque(float opaque)
{
    this->m_videoRenderer.setSubtitleOpaque(opaque);
}

float MediaPresenter::getSubtitleOpaque() const
{
    return this->m_videoRenderer.getSubtitleOpaque();
}

void MediaPresenter::setSubtitleSize(float size)
{
    this->m_videoRenderer.setSubtitleSize(size);
}

float MediaPresenter::getSubtitleSize() const
{
    return this->m_videoRenderer.getSubtitleSize();
}

void MediaPresenter::scheduleRecomputeSubtitleSize()
{
    this->m_videoRenderer.scheduleRecomputeSubtitleSize();
}

bool MediaPresenter::setPreAmp(float dB)
{
    return this->m_audioRenderer.setPreAmp(dB);
}

float MediaPresenter::getPreAmp() const
{
    return this->m_audioRenderer.getPreAmp();
}

bool MediaPresenter::setEqualizerGain(int band, float gain)
{
    return this->m_audioRenderer.setEqualizerGain(band, gain);
}

float MediaPresenter::getEqualizerGain(int band) const
{
    return this->m_audioRenderer.getEqualizerGain(band);
}

int MediaPresenter::getBandCount() const
{
    return this->m_audioRenderer.getBandCount();
}

bool MediaPresenter::isEnableSearchSubtitle() const
{
    return this->m_enableSearchSubtitle;
}

bool MediaPresenter::isEnableSearchLyrics() const
{
    return this->m_enableSearchLyrics;
}

void MediaPresenter::enableSearchSubtitle(bool enable)
{
    this->m_enableSearchSubtitle = enable;
}

void MediaPresenter::enableSearchLyrics(bool enable)
{
    this->m_enableSearchLyrics = enable;
}

void MediaPresenter::enableAutoSaveSearchLyrics(bool enable)
{
    this->m_autoSaveSearchLyrics = enable;
}

bool MediaPresenter::isAutoSaveSearchLyrics() const
{
    return this->m_autoSaveSearchLyrics;
}

void MediaPresenter::showAlbumJacket(bool show)
{
    this->m_showAlbumJacket = show;
}

bool MediaPresenter::isShowAlbumJacket() const
{
    return this->m_showAlbumJacket;
}

void MediaPresenter::useFrameDrop(bool enable)
{
    this->m_useFrameDrop = enable;
}

bool MediaPresenter::isUseFrameDrop() const
{
    return this->m_useFrameDrop;
}

void MediaPresenter::useBufferingMode(bool enable)
{
    this->m_useBufferingMode = enable;
}

bool MediaPresenter::isUseBufferingMode() const
{
    return this->m_useBufferingMode;
}

bool MediaPresenter::isLive() const
{
    return this->m_isLive;
}

void MediaPresenter::useGPUConvert(bool use)
{
    bool prev = this->m_videoRenderer.isUseGPUConvert();

    this->m_videoRenderer.useGPUConvert(use);

    if (prev != use)
    {
        if (this->isEnabledVideo())
            this->recover(this->getMasterClock());
    }
}

bool MediaPresenter::isUseGPUConvert() const
{
    return this->m_videoRenderer.isUseGPUConvert();
}

void MediaPresenter::useHDR(bool use)
{
    bool prev = this->m_videoRenderer.isUseHDR();

    this->m_videoRenderer.useHDR(use);

    if (prev != use)
    {
        if (this->isEnabledVideo())
            this->recover(this->getMasterClock());
    }
}

bool MediaPresenter::isUseHDR() const
{
    return this->m_videoRenderer.isUseHDR();
}

void MediaPresenter::setHDRTextureSupported(bool supported)
{
    this->m_videoRenderer.setHDRTextureSupported(supported);
}

bool MediaPresenter::isHDRTextureSupported() const
{
    return this->m_videoRenderer.isHDRTextureSupported();
}

void MediaPresenter::seek(double time, bool any)
{
    QMutexLocker locker(&this->m_controlLocker);

    if (this->m_state)
    {
        double incr = time - this->getCurrentPosition();
        int flag = (this->m_seekKeyFrame && !any) ? 0 : AVSEEK_FLAG_BACKWARD | AVSEEK_FLAG_ANY;

        this->seekStream(time, incr, flag);
    }
}

uint8_t MediaPresenter::getMaxVolume() const
{
    return this->m_audioRenderer.getMaxVolume();
}

void MediaPresenter::mute(bool mute)
{
    this->m_audioRenderer.mute(mute);
}

void MediaPresenter::volume(uint8_t volume)
{
    this->m_audioRenderer.volume(volume);
}

uint8_t MediaPresenter::getVolume() const
{
    return this->m_audioRenderer.getVolume();
}

double MediaPresenter::getDuration() const
{
    if (this->m_state)
        return this->m_playData.duration;
    else
        return 0.0;
}

double MediaPresenter::getCurrentPosition()
{
    if (this->m_state)
    {
        double duration = this->getDuration();
        double clock;

        clock = this->getMasterClock() + this->m_state->seek.inc;

        if (clock > duration)
            return duration;
        else
            return clock;
    }
    else
    {
        return 0.0;
    }
}

bool MediaPresenter::hasDuration() const
{
    return FloatPointUtils::zeroDouble(this->getDuration()) > 0.0;
}

bool MediaPresenter::IsHDRVideo() const
{
    if (this->isEnabledVideo())
    {
        AVCodecContext *context = this->m_state->video.stream.ctx;
        AVColorPrimaries prim = context->color_primaries;
        AVColorTransferCharacteristic trc = context->color_trc;

        return prim == AVCOL_PRI_BT2020 && (trc == AVCOL_TRC_BT2020_10 || trc == AVCOL_TRC_BT2020_12 || trc == AVCOL_TRC_SMPTE2084);
    }
    else
    {
        return false;
    }
}

double MediaPresenter::getAspectRatio(bool widthPrio) const
{
    MediaState *ms = this->m_state;
    double aspectRatio = 0.0;

    if (!ms)
        return 0.0;

    if (!ms->video.stream.stream)
        return 0.0;

    AVCodecContext *codec = ms->video.stream.ctx;

    if (!codec)
        return 0.0;

    if (codec->sample_aspect_ratio.num == 0)
    {
        aspectRatio = 0.0;
    }
    else
    {
        aspectRatio = av_q2d(codec->sample_aspect_ratio);

        if (widthPrio)
            aspectRatio *= (double)codec->height / (double)codec->width;
        else
            aspectRatio *= (double)codec->width / (double)codec->height;
    }

    if (aspectRatio <= 0.0)
    {
        if (widthPrio)
            aspectRatio = (double)codec->height / (double)codec->width;
        else
            aspectRatio = (double)codec->width / (double)codec->height;
    }

    return aspectRatio;
}

bool MediaPresenter::isEnabledVideo() const
{
    if (this->m_state && this->m_state->video.stream.stream)
        return true;
    else
        return false;
}

bool MediaPresenter::isAudio() const
{
    return (this->m_playData.totalFrame <= 100 && this->m_isAudioExt) || this->m_isRadio;
}

bool MediaPresenter::isVideo() const
{
    return this->isEnabledVideo() && !this->isAudio();
}

bool MediaPresenter::isTempoUsable() const
{
    if (this->m_state)
    {
        if (this->getCurrentAudioStreamIndex() >= 0)
            return this->m_audioRenderer.isTempoUsable();
        else
            return true;
    }

    return false;
}

float MediaPresenter::getTempo() const
{
    float tempo = 0.0f;

    if (this->isTempoUsable())
    {
        if (this->getCurrentAudioStreamIndex() >= 0)
            tempo = this->m_audioRenderer.getTempo();
        else
            tempo = this->m_state->video.tempo;
    }

    return tempo;
}

void MediaPresenter::setTempo(float percent)
{
    if (this->isTempoUsable())
    {
        if (this->getCurrentAudioStreamIndex() >= 0)
            this->m_audioRenderer.setTempo(percent);
        else
            this->m_state->video.tempo = percent;
    }
}

bool MediaPresenter::isRemoteFile() const
{
    return this->m_isRemoteFile;
}

ASSParser& MediaPresenter::getASSParser()
{
    return this->m_assParser;
}

YouTubeParser& MediaPresenter::getYoutubeParser()
{
    return this->m_youtubeParser;
}

NaverTVParser& MediaPresenter::getNaverTVParser()
{
    return this->m_naverTVParser;
}

AVParser& MediaPresenter::getAVParser()
{
    return this->m_avParser;
}

SAMIParser& MediaPresenter::getSAMIParser()
{
    return this->m_samiParser;
}

SRTParser& MediaPresenter::getSRTParser()
{
    return this->m_srtParser;
}

bool MediaPresenter::isRemoteProtocol() const
{
    return this->m_isRemoteProtocol;
}

bool MediaPresenter::isDTV() const
{
    return this->m_isDTV;
}

bool MediaPresenter::isRadio() const
{
    return this->m_isRadio;
}

bool MediaPresenter::changeStream(int newIndex, int oldIndex, bool isAudio, int *lastStreamIndex, volatile bool *quitFlag)
{
    QMutexLocker locker(&this->m_controlLocker);
    bool success = false;

    if (this->m_state == nullptr)
        return false;

    double clock = this->getMasterClock();

    this->m_state->streamChangeStartTime = this->getAbsoluteClock();
    this->m_state->readThreadQuit = true;

    if (this->m_readThread.isRunning())
        this->m_readThread.wait();

    if (quitFlag)
        *quitFlag = true;

    this->closeStreamComponent(oldIndex, isAudio);

    if (this->openStreamComponent(newIndex, isAudio))
    {
        if (newIndex == this->getCurrentAudioStreamIndex())
            this->m_audioRenderer.play();
        else if (newIndex == this->getCurrentVideoStreamIndex())
            this->m_videoRenderer.computeFrameSize();

        this->seekStream(clock, 0.0, AVSEEK_FLAG_ANY | AVSEEK_FLAG_BACKWARD);

        if (lastStreamIndex)
            *lastStreamIndex = newIndex;

        success = true;
    }

    this->startReadThread();

    this->m_state->streamChangeDriftTime += this->getAbsoluteClock() - this->m_state->streamChangeStartTime;

    return success;
}

int MediaPresenter::getCurrentVideoStreamIndex() const
{
    if (this->m_state)
        return this->m_state->video.stream.index;
    else
        return -1;
}

void MediaPresenter::getVideoStreamInfo(QVector<VideoStreamInfo> *ret) const
{
    *ret = this->m_videoStreamInfo;
}

bool MediaPresenter::changeVideoStream(int index)
{
    if (this->m_hwDecoder.isOpened())
    {
        this->m_lastVideoStream = index;
        return this->recover(this->getMasterClock());
    }
    else
    {
        return this->changeStream(index, this->getCurrentVideoStreamIndex(), false, &this->m_lastVideoStream, nullptr);
    }
}

int MediaPresenter::getCurrentAudioStreamIndex() const
{
    if (this->m_state)
        return this->m_state->audio.stream.index;
    else
        return -1;
}

void MediaPresenter::getAudioStreamInfo(QVector<AudioStreamInfo> *ret) const
{
    *ret = this->m_audioStreamInfo;
}

bool MediaPresenter::resetAudioStream()
{
    return this->changeAudioStream(this->getCurrentAudioStreamIndex());
}

bool MediaPresenter::changeAudioStream(int index)
{
    return this->changeStream(index, this->getCurrentAudioStreamIndex(),
                              this->isUseAudioPath(), &this->m_lastAudioStream, nullptr);
}

HSTREAM MediaPresenter::getAudioHandle() const
{
    return this->m_audioRenderer.getHandle();
}

Deinterlacer& MediaPresenter::getDeinterlacer()
{
    return this->m_deinterlacer;
}

void MediaPresenter::setDeinterlacerAlgorithm(AnyVODEnums::DeinterlaceAlgorithm algorithm)
{
    this->m_deinterlacer.setAlgorithm(algorithm);
    this->reloadDeinteralcerCodec();
}

void MediaPresenter::useDeinterlacerIVTC(bool use)
{
    this->m_deinterlacer.useIVTC(use);
    this->reloadDeinteralcerCodec();
}

FilterGraph& MediaPresenter::getFilterGraph()
{
    return this->m_filterGraph;
}

void MediaPresenter::setFormat(AVPixelFormat format)
{
    this->m_videoRenderer.setFormat(format);
}

AVPixelFormat MediaPresenter::getFormat() const
{
    return this->m_videoRenderer.getFormat();
}

bool MediaPresenter::configFilterGraph()
{
    if (this->m_state)
    {
        Video &video = this->m_state->video;

        return this->m_filterGraph.setCodec(video.stream.ctx, video.pixFormat, this->m_videoRenderer.getFormat(),
                                            video.stream.stream->time_base);
    }

    return false;
}

int MediaPresenter::getOptionDescY() const
{
    return this->m_videoRenderer.getOptionDescY();
}

void MediaPresenter::setOptionDescY(int y)
{
    this->m_videoRenderer.setOptionDescY(y);
}

void MediaPresenter::setVerticalScreenOffset(int offset)
{
    this->m_videoRenderer.setVerticalScreenOffset(offset);
}

void MediaPresenter::setHorizontalScreenOffset(int offset)
{
    this->m_videoRenderer.setHorizontalScreenOffset(offset);
}

void MediaPresenter::useDistortion(bool use)
{
    this->m_videoRenderer.useDistortion(use);
}

bool MediaPresenter::isUseDistortion() const
{
    return this->m_videoRenderer.isUseDistortion();
}

void MediaPresenter::setBarrelDistortionCoefficients(const QVector2D &coefficients)
{
    this->m_videoRenderer.setBarrelDistortionCoefficients(coefficients);
}

QVector2D MediaPresenter::getBarrelDistortionCoefficients() const
{
    return this->m_videoRenderer.getBarrelDistortionCoefficients();
}

void MediaPresenter::setPincushionDistortionCoefficients(const QVector2D &coefficients)
{
    this->m_videoRenderer.setPincushionDistortionCoefficients(coefficients);
}

QVector2D MediaPresenter::getPincushionDistortionCoefficients() const
{
    return this->m_videoRenderer.getPincushionDistortionCoefficients();
}

void MediaPresenter::setDistortionLensCenter(const QVector2D &lensCenter)
{
    this->m_videoRenderer.setDistortionLensCenter(lensCenter);
}

QVector2D MediaPresenter::getDistortionLensCenter() const
{
    return this->m_videoRenderer.getDistortionLensCenter();
}

QString MediaPresenter::getRealFilePath() const
{
    return this->m_realFilePath;
}

void MediaPresenter::setBluetoothHeadsetConnected(bool connected)
{
    this->m_audioRenderer.setBluetoothHeadsetConnected(connected);
}

bool MediaPresenter::getBluetoothHeadsetConnected() const
{
    return this->m_audioRenderer.getBluetoothHeadsetConnected();
}

void MediaPresenter::setBluetoothHeadsetSync(double sync)
{
    this->m_audioRenderer.setBluetoothHeadsetSync(sync);
}

double MediaPresenter::getBluetoothHeadsetSync() const
{
    return this->m_audioRenderer.getBluetoothHeadsetSync();
}

void MediaPresenter::resetFOV()
{
    this->m_videoRenderer.resetFOV();
}

float MediaPresenter::getFOV() const
{
    return this->m_videoRenderer.getFOV();
}

bool MediaPresenter::addFOV(float value)
{
    return this->m_videoRenderer.addFOV(value);
}

#ifdef Q_OS_IOS
void MediaPresenter::setIOSNotifyCallback(IOSNOTIFYPROC *proc)
{
    this->m_audioRenderer.setIOSNotifyCallback(proc);
}
#endif

void MediaPresenter::setUserAspectRatio(UserAspectRatioInfo &ratio)
{
    this->m_videoRenderer.setUserAspectRatio(ratio);
}

void MediaPresenter::getUserAspectRatio(UserAspectRatioInfo *ret) const
{
    this->m_videoRenderer.getUserAspectRatio(ret);
}

void MediaPresenter::setMaxTextureSize(int size)
{
    this->m_videoRenderer.setMaxTextureSize(size);
}

int MediaPresenter::getMaxTextureSize() const
{
    return this->m_videoRenderer.getMaxTextureSize();
}

void MediaPresenter::useSubtitleCacheMode(bool use)
{
    this->m_videoRenderer.useSubtitleCacheMode(use);
}

bool MediaPresenter::isUseSubtitleCacheMode() const
{
    return this->m_videoRenderer.isUseSubtitleCacheMode();
}

void MediaPresenter::showOptionDesc(const QString &desc)
{
    if (this->isRunning())
    {
        this->m_videoRenderer.showOptionDesc(desc);

        if (this->m_showAudioOptionDescCallback.callback && this->isAudio())
            this->m_showAudioOptionDescCallback.callback(this->m_showAudioOptionDescCallback.userData, desc, true);
    }
    else
    {
        if (this->m_nonePlayingDescCallback.callback)
            this->m_nonePlayingDescCallback.callback(this->m_nonePlayingDescCallback.userData, desc);
    }
}

void MediaPresenter::clearFonts()
{
    this->m_videoRenderer.clearFonts();
}

void MediaPresenter::clearFrameBuffers()
{
    this->m_videoRenderer.clearFrameBuffers();
}

void MediaPresenter::setStatusChangedCallback(EventCallback *playing, EventCallback *ended)
{
    if (playing)
        this->m_playing = *playing;

    if (ended)
    {
        this->m_ended = *ended;
        this->m_readThread.setEndedCallback(this->m_ended);
    }
}

void MediaPresenter::setEmptyBufferCallback(EmptyBufferCallback &callback)
{
    this->m_emptyBufferCallback = callback;
}

void MediaPresenter::setShowAudioOptionDescCallback(ShowAudioOptionDescCallback &callback)
{
    this->m_showAudioOptionDescCallback = callback;
}

void MediaPresenter::setAudioSubtitleCallback(AudioSubtitleCallback &callback)
{
    this->m_audioSubtitleCallback = callback;
}

void MediaPresenter::setPaintCallback(PaintCallback &callback)
{
    this->m_refreshThread.setCallback(callback);
}

void MediaPresenter::setAbortCallback(AbortCallback &callback)
{
    this->m_abortCallback = callback;
}

void MediaPresenter::setNonePlayingDescCallback(NonePlayingDescCallback &callback)
{
    this->m_nonePlayingDescCallback = callback;
}

void MediaPresenter::setRecoverCallback(EventCallback &callback)
{
    this->m_recoverCallback = callback;
}

HWDecoder& MediaPresenter::getHWDecoder()
{
    return this->m_hwDecoder;
}

void MediaPresenter::callEmptyCallback(bool show)
{
    this->callEmptyCallbackInternal(show);
}

void MediaPresenter::abort(int reason)
{
    if (this->m_abortCallback.callback)
        this->m_abortCallback.callback(this->m_abortCallback.userData, reason);
}

double MediaPresenter::getVideoClock()
{
    QMutexLocker locker(&this->m_state->video.stream.clockLock);

    return this->m_state->video.stream.clock;
}

double MediaPresenter::getExternalClock() const
{
    int64_t driftTime = this->m_state->pause.driftTime + this->m_state->video.driftTime
            + this->m_state->seek.videoDiscardDriftTime + this->m_state->seek.readDiscardDriftTime
            + this->m_state->streamChangeDriftTime;
    int64_t base = this->m_state->externalClock.base;

    return (this->getAbsoluteClock() - base - driftTime) / MICRO_SECOND;
}

double MediaPresenter::getMasterClock()
{
    if (this->m_state->syncType == SYNC_VIDEO_MASTER)
        return this->getVideoClock();
    else if (this->m_state->syncType == SYNC_AUDIO_MASTER)
        return this->m_audioRenderer.getClock();
    else
        return this->getExternalClock();
}

double MediaPresenter::getAudioClockOffset() const
{
    MediaState *ms = this->m_state;
    Audio &audio = ms->audio;

    return ms->syncType == SYNC_AUDIO_MASTER ? audio.stream.clockOffset : 0.0;
}

double MediaPresenter::frameNumberToClock(int number) const
{
    return (number * this->getDuration()) / this->m_detail.videoTotalFrame;
}

bool MediaPresenter::isValid() const
{
    return this->m_state != nullptr;
}

bool MediaPresenter::canCapture() const
{
    return this->isEnabledVideo() &&
            this->getVRInputSource() == AnyVODEnums::VRI_NONE &&
            !ShaderCompositer::getInstance().is360Degree();
}

int64_t MediaPresenter::getAbsoluteClock() const
{
    return av_gettime();
}

double MediaPresenter::calFrameDelay(double pts)
{
    FrameTimer &timer = this->m_state->frameTimer;
    double delay = pts - timer.lastPTS;

    if (delay <= 0.0 || delay >= 1.0)
        delay = timer.lastDelay;

    timer.lastDelay = delay;
    timer.lastPTS = pts;

    return delay;
}

void MediaPresenter::callAudioSubtitleCallback()
{
    if (this->m_audioSubtitleCallback.callback && this->existAudioSubtitle())
    {
        if (this->m_videoRenderer.isShowSubtitle())
        {
            Audio &audio = this->m_state->audio;
            QMutexLocker locker(&audio.stream.clockLock);
            uint32_t time = (int32_t)((audio.stream.clock + this->m_subtitleSync) * 1000);
            bool found = true;
            QVector<Lyrics> lines;
            Lyrics line;

            if (this->m_lrcParser.isExist())
            {
                if (this->m_lrcParser.get(time, &line))
                {
                    lines.append(line);

                    if (this->m_lrcParser.getNext(time, 1, &line))
                    {
                        lines.append(line);

                        if (this->m_lrcParser.getNext(time, 2, &line))
                            lines.append(line);
                    }
                }
            }
            else
            {
                found = false;
            }

            if (found && lines.count() > 0)
                this->m_audioSubtitleCallback.callback(this->m_audioSubtitleCallback.userData, lines);
        }
        else
        {
            this->m_audioSubtitleCallback.callback(this->m_audioSubtitleCallback.userData, QVector<Lyrics>());
        }
    }
}

void MediaPresenter::refreshSchedule(int delay)
{
    this->m_refreshThread.refreshTimer(delay);
}

bool MediaPresenter::getPictureRect(QRect *rect)
{
    return this->m_videoRenderer.getPictureRect(rect);
}

bool MediaPresenter::isUseGPUConvert(AVPixelFormat format) const
{
    return this->m_videoRenderer.isUseGPUConvert(format);
}

bool MediaPresenter::isUsingSPDIFEncoding() const
{
    return this->m_audioRenderer.isUsingSPDIFEncoding();
}

uint8_t MediaPresenter::getLuminanceAvg(uint8_t *data, int size, AVPixelFormat format) const
{
    if (PixelUtils::is8bitFormat(format) && size)
    {
        uint64_t avg = 0;
        int step = 4;

        for (int i = 0; i < size; i += step)
            avg += data[i];

        return (uint8_t)(avg / (size / step));
    }

    return 127;
}

void MediaPresenter::resizePicture(const VideoPicture &src, VideoPicture &dest) const
{
    int width = 0;
    int height = 0;

    this->getFrameSize(&width, &height);

    src.resize(width, height, dest);
}

void MediaPresenter::copyPicture(const VideoPicture &src, VideoPicture &dest) const
{
    int width = 0;
    int height = 0;

    this->getFrameSize(&width, &height);

    src.copy(width, height, dest);
}

bool MediaPresenter::getFrameSize(int *width, int *height) const
{
    MediaState *ms = this->m_state;

    if (ms && this->isEnabledVideo())
    {
        AVCodecContext *codec = ms->video.stream.ctx;

        if (codec)
        {
            *width = codec->width;
            *height = codec->height;

            return true;
        }
    }

    return false;
}

void MediaPresenter::updateVideoRefreshTimer()
{
    MediaState *ms = this->m_state;
    VideoFrameQueue &frames = ms->videoFrames;
    FrameTimer &timer = ms->frameTimer;
    Pause &pause = ms->pause;
    Seek &seek = ms->seek;
    Video &video = ms->video;
    VideoPicture &vp = frames.getReadingFrame();

    vp.lock();

    double pts = vp.getPts();
    double delay = this->calFrameDelay(pts);
    int frameDrop = 0;

    if (ms->syncType != SYNC_VIDEO_MASTER)
    {
        double incClock = 0.0;

        if (this->m_hwDecoder.isOpened())
            incClock += this->m_hwDecoder.getSurfaceQueueCount() * delay;

        if (this->m_filterGraph.hasFilters())
            incClock += this->m_filterGraph.getDelayCount() * delay;

        double diff = pts - (this->getMasterClock() + incClock);
        double syncThreshold = max(delay * SYNC_THRESHOLD_MULTI, SYNC_THRESHOLD);

        if (fabs(diff) < NOSYNC_THRESHOLD)
        {
            if (diff <= -syncThreshold)
            {
                if (frameDrop <= 0)
                {
                    if (timer.lowFrameCounter > FRAME_LOW_WARNING_THRESHOLD / 4)
                        frameDrop = (int)fabs(diff / delay);
                    else
                        frameDrop = 1;
                }

                delay = 0.0;
            }
            else if (diff >= syncThreshold)
            {
                delay *= 2.0;
            }
        }
    }
    else
    {
        if (video.tempo > 0.0)
            delay /= 1.0 + video.tempo / 100.0;
        else if (video.tempo < 0.0)
            delay *= (150.0 - video.tempo) / 100.0;
    }

    timer.timer += delay * MICRO_SECOND;

    int64_t driftTime = pause.driftTime + video.driftTime +
            seek.videoDiscardDriftTime + seek.readDiscardDriftTime + ms->streamChangeDriftTime;
    double actualDelay = (timer.timer - (this->getAbsoluteClock() - driftTime)) / MICRO_SECOND;

    if (delay <= 0.0 && timer.lowFrameCounter <= FRAME_LOW_WARNING_THRESHOLD)
        timer.lowFrameCounter++;
    else
        timer.lowFrameCounter = 0;

    if (timer.lowFrameCounter > FRAME_LOW_WARNING_THRESHOLD && !this->m_videoRenderer.isShowingOptionDesc())
        this->showOptionDesc(tr("프레임 저하가 일어나고 있습니다. 성능에 영향을 미치는 옵션 또는 수직 동기화를 꺼주세요."));

    int refresh = (int)(actualDelay * 1000 + 0.5);

    this->refreshSchedule(refresh);
    this->m_videoRenderer.displayVideo(vp);

    vp.unlock();

    frames.savePrevFrameIndex();

    if (this->m_useFrameDrop)
        video.frameDrop = frameDrop;
    else
        video.frameDrop = 0;

    if (seek.requestPauseOnRender)
    {
        if (--seek.pauseOnRenderCount <= 0)
        {
            seek.requestPauseOnRender = false;
            this->pause();
        }
    }
}

void MediaPresenter::restartVideo(void *userData, const QSize &newSize)
{
    if (!userData)
        return;

    MediaPresenter *parent = (MediaPresenter*)userData;

    parent->m_detail.videoInputSize = newSize;

    parent->m_videoRenderer.stop();
    parent->m_videoRenderer.start(newSize.width(), newSize.height(), parent->m_state);

    parent->m_assParser.setFrameSize(newSize.width(), newSize.height());
    parent->m_avParser.setFrameSize(newSize);
}

bool MediaPresenter::update()
{
    MediaState *ms = this->m_state;

    if (!ms)
        return false;

    this->m_videoRenderer.processSubtitleResize();

    Video &video = ms->video;
    VideoFrameQueue &frames = ms->videoFrames;

    if (!video.stream.stream)
    {
        this->refreshSchedule(NO_AUDIO_ALBUM_JACKET_DELAY);
#if defined Q_OS_MOBILE
        VideoPicture &audioPicture = frames.getAudioPicture();

        if (!audioPicture.isValid())
        {
            QSize size = this->m_videoRenderer.getSize();

            audioPicture.create(size.width(), size.height(), this->m_videoRenderer.getFormat());
        }

        this->m_videoRenderer.displayVideo(audioPicture);
        return true;
#else
        return false;
#endif
    }

    int size = frames.getSize();

    if (size == 0)
    {
        VideoPicture &audioPicture = frames.getAudioPicture();

        if (!this->isRemoteFile() && this->isAudio() && this->isEnabledVideo() && !audioPicture.isValid())
        {
            FrameExtractor ex;
            FrameExtractor::FrameItem item;
            AVPixelFormat format = VideoRenderer::DEFAULT_PIX_FORMAT;

            ex.setPixFormat(format);

            if (ex.open(this->m_filePath, false) && ex.getFrame(0.0, true, &item))
            {
                VideoPicture vp;
                const Surface &surface = vp.getSurface();

                vp.create(item.frame.width(), item.frame.height(), format);

                const uint8_t *buffers[4] = {item.buffer, };
                const int lineSizes[4] = {av_image_get_linesize(format, surface.getWidth(), 0), };

                av_image_copy(surface.getPixels(), surface.getLineSize(), buffers, lineSizes, format,
                              surface.getWidth(), surface.getHeight());

                vp.fillAlpha(numeric_limits<uint8_t>::max());

                this->resizePicture(vp, audioPicture);
                vp.destory();
            }

            if (!audioPicture.isValid())
            {
                QSize size = this->m_videoRenderer.getSize();

                audioPicture.create(size.width(), size.height(), this->m_videoRenderer.getFormat());
            }
        }

        if (!video.stream.queue.hasPacket() && this->isAudio())
        {
            this->refreshSchedule(DEFAULT_REFRESH_DELAY);
            this->m_videoRenderer.displayVideo(audioPicture);
        }
        else
        {
            VideoPicture &vp = frames.getPrevFrame();

            vp.lock();

            this->refreshSchedule(EMPTY_BUFFER_WAIT_DELAY);
            this->m_videoRenderer.displayVideo(vp);

            vp.unlock();
        }

        return true;
    }

    if (ms->pause.pause)
    {
        VideoPicture &vp = frames.getReadingFrame();

        vp.lock();

        this->refreshSchedule(DEFAULT_REFRESH_DELAY);
        this->m_videoRenderer.displayVideo(vp);

        vp.unlock();

        return true;
    }

    this->updateVideoRefreshTimer();

    if (ms->seek.flushed && ms->seek.pauseSeeking)
    {
        int maxCount;

#if defined Q_OS_WIN
        maxCount = 3;
#else
        maxCount = 1;
#endif
        if (ms->seek.discardCount++ >= maxCount)
        {
            ms->seek.flushed = false;
            ms->seek.pauseSeeking = false;
            ms->seek.discardCount = 0;

            this->pause();
        }
    }

    if (!ms->seek.firstFrameAfterFlush)
        ms->seek.firstFrameAfterFlush = true;

    frames.pop();

    return true;
}

void MediaPresenter::allocPicture()
{
    MediaState *ms = this->m_state;
    VideoFrameQueue &frames = ms->videoFrames;
    VideoPicture &vp = frames.getWritingFrame();

    if (vp.isValid())
        vp.destory();

    AVCodecContext *codec = ms->video.stream.ctx;
    int width = 0;
    int height = 0;

    this->getFrameSize(&width, &height);

    vp.create(width, height, codec->width, codec->width, this->m_videoRenderer.getFormat());
}

bool MediaPresenter::convertPicture(AVFrame &inFrame, double pts, AVPixelFormat format,
                                    bool leftOrTop3D, const FrameMetaData &frameMeta)
{
    MediaState *ms = this->m_state;
    VideoFrameQueue &frames = ms->videoFrames;
    QVector<volatile bool*> quitConditions;

    quitConditions.append(&ms->quit);
    quitConditions.append(&ms->video.threadQuit);

    frames.wait(quitConditions);

    if (ms->quit)
        return false;

    if (ms->video.threadQuit)
        return false;

    int destw = 0;
    int desth = 0;

    this->getFrameSize(&destw, &desth);

    VideoPicture &vp = frames.getWritingFrame();
    const Surface &surface = vp.getSurface();

    vp.lock();

    if (!vp.isValid() || surface.getWidth() != destw || surface.getHeight() != desth ||
        (surface.getFormat() != format && this->isUseGPUConvert(format)))
    {
        this->allocPicture();

        if (ms->quit)
        {
            vp.unlock();
            return false;
        }
    }

    if (vp.isValid())
    {
        AVCodecContext *codec = ms->video.stream.ctx;
        int w = codec->width;
        int h = codec->height;
        AVFrame *frame;
        bool hasFilters = this->m_filterGraph.hasFilters();
        AVFrame filterd;

        if (hasFilters)
        {
            if (!this->m_filterGraph.getFrame(w, h, format, inFrame, &filterd, &format))
            {
                vp.unlock();
                return true;
            }

            frame = &filterd;
        }
        else
        {
            frame = &inFrame;
        }

        AVPixelFormat realFormat;
        int realLineSize[AV_NUM_DATA_POINTERS] = {0, };
        int realHeight;

        if (this->isUseGPUConvert(format))
        {
            av_image_copy(surface.getPixels(), surface.getLineSize(),
                          (const uint8_t**)frame->data, frame->linesize, format, w, h);

            realFormat = format;
            realHeight = h;
            memcpy(realLineSize, frame->linesize, sizeof(frame->linesize));
        }
        else
        {
            ms->videoConverter = sws_getCachedContext(ms->videoConverter,
                                                      w, h, format,
                                                      destw, desth, this->m_videoRenderer.getFormat(),
                                                      SWS_POINT, nullptr, nullptr, nullptr);

            memcpy(realLineSize, surface.getLineSize(), sizeof(realLineSize));

            if (ms->videoConverter)
            {
                sws_scale(ms->videoConverter, frame->data, frame->linesize, 0, h,
                          surface.getPixels(), surface.getLineSize());
            }

            realFormat = this->m_videoRenderer.getFormat();
            realHeight = desth;
        }

        const AVPixFmtDescriptor *desc = av_pix_fmt_desc_get(realFormat);

        if (desc)
        {
            int height2 = AV_CEIL_RSHIFT(realHeight, desc->log2_chroma_h);

            this->m_detail.videoOutputByteCount.fetchAndAddOrdered(realLineSize[0] * realHeight);

            for (int i = 1; i < surface.getPlain(); i++)
                this->m_detail.videoOutputByteCount.fetchAndAddOrdered(realLineSize[i] * height2);
        }

        vp.setPts(pts);
        vp.setTime((pts + this->m_subtitleSync) * 1000);
        vp.setLuminanceAverage(this->getLuminanceAvg(frame->data[0], frame->linesize[0] * h, format));
        vp.setLeftOrTop3D(leftOrTop3D);
        vp.setFrameMetaData(frameMeta);

        vp.unlock();

        frames.push();

        if (hasFilters)
            av_freep(&filterd.data[0]);
    }
    else
    {
        vp.unlock();
    }

    return true;
}

void MediaPresenter::startAudio()
{
    this->m_audioRenderer.start();
}

void MediaPresenter::resumeAudio()
{
    this->m_audioRenderer.resume();
}

int MediaPresenter::getRemainedAudioDataSize() const
{
    return this->m_audioRenderer.getRemainedDataSize();
}

double MediaPresenter::synchronizeVideo(AVFrame *srcFrame, double pts, const AVRational timeBase)
{
    Video &video = this->m_state->video;
    Seek &seek = this->m_state->seek;
    QMutexLocker locker(&video.stream.clockLock);

    if (pts == 0.0)
    {
        pts = video.stream.clock;
    }
    else
    {
        pts -= this->getAudioClockOffset();
        video.stream.clock = pts;
    }

#if defined Q_OS_ANDROID
    if (!this->m_hwDecoder.isOpened())
    {
#endif
        double frameDelay = av_q2d(timeBase);

        frameDelay = srcFrame->repeat_pict * (frameDelay * 0.5);
        video.stream.clock += frameDelay;
#if defined Q_OS_ANDROID
    }
#endif

    if (this->m_state->syncType == SYNC_VIDEO_MASTER && seek.firstFrameAfterFlush)
        seek.readable = true;

    return pts;
}

int MediaPresenter::getBuffer(AVCodecContext *ctx, AVFrame *pic, int flag)
{
    int ret;
    MediaPresenter *parent = (MediaPresenter*)ctx->opaque;

    if (parent->m_hwDecoder.isOpened())
    {
        ret = parent->m_hwDecoder.getBuffer(pic) ? 0 : -1;

        if (ret < 0)
            parent->m_hwDecoder.close();
    }
    else
    {
        ret = avcodec_default_get_buffer2(ctx, pic, flag);
    }

    return ret;
}

AVPixelFormat MediaPresenter::getFormat(struct AVCodecContext *ctx, const AVPixelFormat *fmt)
{
    MediaPresenter *parent = (MediaPresenter*)ctx->opaque;
    bool hwOpened = parent->m_hwDecoder.isOpened();

    for (int i = 0; fmt[i] != AV_PIX_FMT_NONE; i++)
    {
        if (hwOpened && parent->m_hwDecoder.isDecodable(fmt[i]))
        {
            if (parent->m_hwDecoder.prepare(ctx))
                return fmt[i];
        }
    }

    if (hwOpened)
        parent->m_hwDecoder.close();

    return avcodec_default_get_format(ctx, fmt);
}

void MediaPresenter::closeStreamComponent(unsigned int index, bool isAudio)
{
    MediaState *ms = this->m_state;
    AVFormatContext *format = isAudio ? ms->audioFormat : ms->format;

    if (index >= format->nb_streams)
        return;

    AVCodecParameters *context = format->streams[index]->codecpar;

    switch (context->codec_type)
    {
        case AVMEDIA_TYPE_AUDIO:
        {
            Audio &audio = ms->audio;

            audio.stream.queue.unlock();

            this->m_audioRenderer.stop();

            audio.stream.queue.end();
            audio.stream.stream = nullptr;
            audio.stream.index = -1;

            if (audio.stream.ctx)
                avcodec_free_context(&audio.stream.ctx);

            break;
        }
        case AVMEDIA_TYPE_VIDEO:
        {
            VideoFrameQueue &videoFrames = ms->videoFrames;
            Video &video = ms->video;

            video.threadQuit = true;
            videoFrames.wakeUp();
            video.stream.queue.unlock();

            if (this->m_videoThread.isRunning())
                this->m_videoThread.wait();

            video.stream.queue.end();
            video.stream.stream = nullptr;
            video.stream.index = -1;

            this->m_videoRenderer.stop();

            this->releasePictures();

            if (this->m_hwDecoder.isOpened())
                this->m_hwDecoder.close();

            if (video.stream.ctx)
                avcodec_free_context(&video.stream.ctx);

            break;
        }
        case AVMEDIA_TYPE_SUBTITLE:
        {
            Subtitle &subtitle = ms->subtitle;

            subtitle.threadQuit = true;

            subtitle.stream.queue.unlock();

            if (this->m_subtitleThread.isRunning())
                this->m_subtitleThread.wait();

            subtitle.stream.queue.end();
            subtitle.stream.stream = nullptr;
            subtitle.stream.index = -1;

            this->releaseSubtitles();

            if (subtitle.stream.ctx)
                avcodec_free_context(&subtitle.stream.ctx);

            break;
        }
        default:
        {
            break;
        }
    }
}

Stream* MediaPresenter::getStream(int index)
{
    Stream *stream = nullptr;

    if (index == this->m_state->video.stream.index)
        stream = &this->m_state->video.stream;
    else if (index == this->m_state->audio.stream.index)
        stream = &this->m_state->audio.stream;
    else if (index == this->m_state->subtitle.stream.index)
        stream = &this->m_state->subtitle.stream;

    return stream;
}

Detail& MediaPresenter::getRDetail()
{
    return this->m_detail;
}

const char* MediaPresenter::findProfileName(const AVProfile *profiles, int profile) const
{
    if (profiles)
    {
        for (int i = 0; profiles[i].profile != FF_PROFILE_UNKNOWN; i++)
        {
            if (profiles[i].profile == profile)
                return profiles[i].name;
        }
    }

    return nullptr;
}

const AVCodec* MediaPresenter::tryInternalHWDecoder(AVCodecContext *context, const QString &postFix) const
{
    void *opaque = nullptr;
    const AVCodec *codec = av_codec_iterate(&opaque);

    while (codec)
    {
        if (codec->id == context->codec_id && QString::fromLatin1(codec->name).contains("_" + postFix))
        {
            if (avcodec_open2(context, codec, nullptr) == 0)
                return codec;
            else
                break;
        }

        codec = av_codec_iterate(&opaque);
    }

    return nullptr;
}

const AVCodec* MediaPresenter::tryCrystalHDDecoder(AVCodecContext *context) const
{
    return this->tryInternalHWDecoder(context, "crystalhd");
}

void MediaPresenter::reloadDeinteralcerCodec()
{
    if (!this->m_state)
        return;

    Video &video = this->m_state->video;

    this->m_deinterlacer.setCodec(video.stream.ctx, video.pixFormat, video.stream.stream->time_base, video.stream.ctx->framerate);
}

#if defined Q_OS_RASPBERRY_PI
const AVCodec* MediaPresenter::tryMMALDecoder(AVCodecContext *context) const
{
    return this->tryInternalHWDecoder(context, "mmal");
}

const AVCodec* MediaPresenter::tryV4L2M2MDecoder(AVCodecContext *context) const
{
    return this->tryInternalHWDecoder(context, "v4l2m2m");
}
#elif defined Q_OS_ANDROID
const AVCodec* MediaPresenter::tryMediaCodecDecoder(AVCodecContext *context) const
{
    (void)context;

    return nullptr;
    //return this->tryInternalHWDecoder(context, "mediacodec");
}
#endif

bool MediaPresenter::openStreamComponent(unsigned int index, bool isAudio)
{
    MediaState *ms = this->m_state;

    if (ms == nullptr)
        return false;

    AVFormatContext *format = isAudio ? ms->audioFormat : ms->format;

    if (index >= format->nb_streams)
        return false;

    AVCodecContext *context = avcodec_alloc_context3(nullptr);

    if (avcodec_parameters_to_context(context, format->streams[index]->codecpar) < 0)
    {
        avcodec_free_context(&context);
        return false;
    }

    context->pkt_timebase = format->streams[index]->time_base;

    const AVCodec *codec = nullptr;
    AVCodecID codecID = context->codec_id;
    bool internalHWAccel = false;

    if (this->m_useHWDecoder && context->codec_type == AVMEDIA_TYPE_VIDEO)
    {
        codec = this->tryCrystalHDDecoder(context);

        if (!codec)
        {
#if defined Q_OS_RASPBERRY_PI
            codec = this->tryMMALDecoder(context);

            if (!codec)
                codec = this->tryV4L2M2MDecoder(context);
#elif defined Q_OS_ANDROID
            codec = this->tryMediaCodecDecoder(context);
#endif
        }

        if (codec)
        {
            internalHWAccel = true;
        }
        else
        {
            if (avcodec_parameters_to_context(context, format->streams[index]->codecpar) < 0)
            {
                avcodec_free_context(&context);
                return false;
            }

            context->pkt_timebase = format->streams[index]->time_base;
            codecID = context->codec_id;
        }
    }

    if (!internalHWAccel)
    {
        codec = avcodec_find_decoder(codecID);

        if (!codec)
            return false;

        if (context->codec_type == AVMEDIA_TYPE_VIDEO)
        {
            context->get_buffer2 = MediaPresenter::getBuffer;
            context->get_format = MediaPresenter::getFormat;

            context->thread_type = FF_THREAD_SLICE | FF_THREAD_FRAME;
            context->thread_count = 0;

            context->opaque = this;

            if (this->m_useHWDecoder && !this->isAudio() && this->m_hwDecoder.open(context))
            {
                context->thread_type &= ~FF_THREAD_FRAME;
                context->slice_flags |= SLICE_FLAG_ALLOW_FIELD;
                context->strict_std_compliance = FF_COMPLIANCE_STRICT;
            }
        }

#if defined Q_OS_ANDROID
        if (!(context->codec_type == AVMEDIA_TYPE_VIDEO && this->m_hwDecoder.isOpened()))
        {
#endif
            if (avcodec_open2(context, codec, nullptr) < 0)
                return false;
#if defined Q_OS_ANDROID
        }
#endif
    }

    const int bufSize = 128;
    char buf[bufSize] = {0, };
    QString codecName;
    QString profile = this->findProfileName(codec->profiles, context->profile);

    profile = profile.isEmpty() ? "" : " " + profile;
    codecName = QString("%1 (%2%3)").arg(QString(codec->name).toUpper(), codec->long_name, profile);

    switch (context->codec_type)
    {
        case AVMEDIA_TYPE_AUDIO:
        {
            Audio &audio = ms->audio;
            QStringList sampleFMT = QString(av_get_sample_fmt_string(buf, bufSize, context->sample_fmt)).split(" ", Qt::SkipEmptyParts);

            this->m_detail.audioCodec = codecName;
            this->m_detail.audioCodecSimple = QString(codec->name).toUpper() + profile;
            this->m_detail.audioInputType = QString(sampleFMT[0]).toUpper();
            this->m_detail.audioInputBits = sampleFMT[1].toInt();

            bool isUseSPDIF = this->m_audioRenderer.isUseSPDIF();

            if (!this->m_audioRenderer.start(ms, context, codecID))
                return false;

            if (isUseSPDIF != this->m_audioRenderer.isUseSPDIF())
                this->showOptionDesc(tr("S/PDIF 출력 초기화를 실패 하였습니다. PCM 출력으로 전환합니다."));

            audio.stream.index = index;
            audio.stream.stream = format->streams[index];
            audio.stream.ctx = context;

            audio.stream.queue.init();

            if (audio.stream.stream->start_time != AV_NOPTS_VALUE)
                audio.stream.clockOffset = av_q2d(audio.stream.stream->time_base) * audio.stream.stream->start_time;

            break;
        }
        case AVMEDIA_TYPE_VIDEO:
        {
            Video &video = ms->video;
            FrameTimer &timer = ms->frameTimer;
            AVPixelFormat pixFormat;

            if (this->m_hwDecoder.isOpened())
                pixFormat = this->m_hwDecoder.getFormat();
            else
                pixFormat = context->pix_fmt;

            QStringList pixFMT = QString(av_get_pix_fmt_string(buf, bufSize, pixFormat)).split(" ", Qt::SkipEmptyParts);

            this->m_detail.videoCodec = codecName;

#if defined Q_OS_ANDROID || defined Q_OS_MAC
            if (!this->m_hwDecoder.isOpened())
#else
            if (!this->m_hwDecoder.isOpened() || !this->m_hwDecoder.isUseDefaultGetBuffer())
#endif
            {
                this->m_detail.videoInputType = pixFMT[0].toUpper();
                this->m_detail.videoInputBits = pixFMT[2].toInt();

                this->m_videoRenderer.selectPixelFormat(pixFormat);
            }
            else
            {
                this->m_videoRenderer.selectDefaultPixelFormat();
            }

            pixFMT = QString(av_get_pix_fmt_string(buf, bufSize, this->m_videoRenderer.getFormat())).split(" ", Qt::SkipEmptyParts);

            this->m_detail.videoOutputType = pixFMT[0].toUpper();
            this->m_detail.videoOutputBits = pixFMT[2].toInt();

            if (this->m_playData.totalFrame <= 0)
                this->m_playData.totalFrame = (int)format->streams[index]->nb_frames;

            video.stream.index = index;
            video.stream.stream = format->streams[index];
            video.stream.ctx = context;

            timer.timer = this->getAbsoluteClock();
            timer.lastDelay = 0.04;

            video.stream.queue.init();

            video.threadQuit = false;
            this->m_videoThread.setup(this->m_state, this);
            this->m_videoThread.start();

            this->restartVideo(this, QSize(context->width, context->height));

            this->m_deinterlacer.setCodec(video.stream.ctx, pixFormat, video.stream.stream->time_base, context->framerate);
            this->m_filterGraph.setCodec(video.stream.ctx, pixFormat, this->m_videoRenderer.getFormat(), video.stream.stream->time_base);

            video.pixFormat = pixFormat;

#ifndef Q_OS_MAC
            int maxThreads = QThread::idealThreadCount() + 1;

            if (maxThreads <= 0)
                maxThreads = 1;

            omp_set_num_threads(maxThreads);
#endif
            break;
        }
        case AVMEDIA_TYPE_SUBTITLE:
        {
            Subtitle &subtitle = ms->subtitle;

            if (!this->existSubtitle())
                this->m_detail.subtitleCodec = QString(SUBTITLE_CODEC_FORMAT).arg(QString(codec->name).toUpper(), codec->long_name);

            subtitle.stream.index = index;
            subtitle.stream.stream = format->streams[index];
            subtitle.stream.ctx = context;

            subtitle.stream.queue.init();

            QString header;

            if (context->subtitle_header_size)
                header = QString::fromUtf8((char*)context->subtitle_header, context->subtitle_header_size);

            this->m_assParser.setHeader(header);

            subtitle.threadQuit = false;

            this->m_subtitleThread.setup(this->m_state, this);
            this->m_subtitleThread.start();

            break;
        }
        default:
        {
            break;
        }
    }

    return true;
}

const QVector<ChapterInfo>& MediaPresenter::getChapters() const
{
    return this->m_chapters;
}

int MediaPresenter::decodingInterruptCallback(void *userData)
{
    MediaState *state = (MediaState*)userData;

    return state && state->quit;
}

void MediaPresenter::seek()
{
    MediaState *ms = this->m_state;
    Seek &seek = ms->seek;
    Video &video = ms->video;
    Audio &audio = ms->audio;
    Subtitle &subtitle = ms->subtitle;

    int index = -1;
    int64_t seekTarget = seek.pos;
    int64_t seekTargetAudio = seek.pos;

    if (video.stream.index >= 0 && !this->isAudio())
        index = video.stream.index;
    else if (audio.stream.index >= 0)
        index = audio.stream.index;
    else if (subtitle.stream.index >= 0)
        index = subtitle.stream.index;

    if (index >= 0)
    {
        AVRational r = {1, AV_TIME_BASE};

        seekTarget = av_rescale_q(seekTarget, r, ms->format->streams[index]->time_base);

        if (ms->audioFormat)
            seekTargetAudio = av_rescale_q(seekTargetAudio, r, ms->audioFormat->streams[audio.stream.index]->time_base);
    }

    int ret = av_seek_frame(ms->format, index, seekTarget, seek.flags);

    if (ret >= 0)
    {
        if (ms->audioFormat)
            av_seek_frame(ms->audioFormat, audio.stream.index, seekTargetAudio, seek.flags);

        if (audio.stream.index >= 0)
        {
            audio.stream.queue.flush();
            audio.stream.queue.putFlushPacket();
        }

        if (video.stream.index >= 0)
        {
            video.stream.queue.flush();
            video.stream.queue.putFlushPacket();
        }

        if (subtitle.stream.index >= 0)
        {
            subtitle.stream.queue.flush();
            subtitle.stream.queue.putFlushPacket();
        }
    }

    seek.request = false;

    if (this->m_audioSubtitleCallback.callback && this->existAudioSubtitle())
    {
        if (this->m_videoRenderer.isShowSubtitle())
        {
            Lyrics tmp;
            bool found = false;

            if (this->m_lrcParser.isExist())
                found = this->m_lrcParser.get(seek.time * 1000, &tmp);

            if (!found)
                this->m_audioSubtitleCallback.callback(this->m_audioSubtitleCallback.userData, QVector<Lyrics>());
        }
        else
        {
            this->m_audioSubtitleCallback.callback(this->m_audioSubtitleCallback.userData, QVector<Lyrics>());
        }
    }

    if (seek.pauseSeeking && ret >= 0)
        this->resume();
}

bool MediaPresenter::recover(double clock)
{
    QMutexLocker locker(&this->m_controlLocker);
    bool isPaused;
    QString subtitlePath = this->m_subtitleFilePath;
    QString audioPath = this->m_audioPath;
    ExtraPlayData playData = this->m_playData;
    Range repeatRange = this->m_repeatRange;
    float subtitleSync = this->m_subtitleSync;
    bool result = false;
    bool isTmpSubtitle = false;
    SubtitleType prevSubtitleType = this->m_subtitleType;
    bool isRemoteFile = this->m_isRemoteFile;

    if (this->m_state)
        isPaused = this->m_state->pause.pause;
    else
        isPaused = false;

    if (this->m_isRemoteProtocol)
    {
        if (!this->isRemoteSubtitle(this->m_subtitleType) &&
            this->m_subtitleType != ST_AV &&
            this->m_subtitleType != ST_ASS)
        {
            QString tmpFilePath = QDir::tempPath();
            QString ext = QFileInfo(subtitlePath).suffix();

            SeparatingUtils::appendDirSeparator(&tmpFilePath);
            tmpFilePath += "XXXXXX.";
            tmpFilePath += ext;

            QTemporaryFile tmpFile(tmpFilePath);

            tmpFile.setAutoRemove(false);

            if (tmpFile.open())
            {
                QString filePath = tmpFile.fileName();

                tmpFile.close();

                switch (this->m_subtitleType)
                {
                    case ST_SAMI:
                        this->m_samiParser.save(filePath, 0.0);
                        break;
                    case ST_SRT:
                        this->m_srtParser.save(filePath, 0.0);
                        break;
                    case ST_LRC:
                        this->m_lrcParser.save(filePath, 0.0);
                        break;
                    default:
                        break;
                }

                isTmpSubtitle = true;
                subtitlePath = filePath;
            }
        }
    }

    this->stop();

    this->m_playData = playData;
    this->m_repeatRange = repeatRange;
    this->m_subtitleSync = subtitleSync;
    this->m_audioPath = audioPath;
    this->m_isRemoteFile = isRemoteFile;

    this->m_videoRenderer.scheduleInitTextures();

    if (this->play())
    {
        if (!subtitlePath.isEmpty())
            this->openSubtitle(subtitlePath, this->isRemoteSubtitle(prevSubtitleType));

        if (isPaused)
            this->pause();

        if (isTmpSubtitle)
        {
            QFile f(subtitlePath);

            f.remove();
        }

        this->seekStream(clock, 0.0, AVSEEK_FLAG_ANY | AVSEEK_FLAG_BACKWARD);

        result = true;
    }

    if (this->m_recoverCallback.callback)
        this->m_recoverCallback.callback(this->m_recoverCallback.userData);

    return result;
}

bool MediaPresenter::isUseAudioPath() const
{
    return !this->m_audioPath.isEmpty();
}

void MediaPresenter::closeStream()
{
    MediaState *ms = this->m_state;

    if (ms)
    {
        if (this->m_readThread.isRunning())
            this->m_readThread.wait();

        if (ms->video.stream.index >= 0)
            this->closeStreamComponent(ms->video.stream.index, false);

        if (ms->audio.stream.index >= 0)
            this->closeStreamComponent(ms->audio.stream.index, this->isUseAudioPath());

        if (ms->subtitle.stream.index >= 0)
            this->closeStreamComponent(ms->subtitle.stream.index, false);

        if (ms->videoConverter)
            sws_freeContext(ms->videoConverter);

        if (ms->subtitleConverter)
            sws_freeContext(ms->subtitleConverter);

        if (ms->format)
            avformat_close_input(&ms->format);

        if (ms->audioFormat)
            avformat_close_input(&ms->audioFormat);

        this->m_audioPath.clear();

        this->m_assParser.deInit();
        this->m_chapters.clear();

        this->m_refreshThread.stop();

        delete this->m_state;
        this->m_state = nullptr;
    }

    this->m_subtitleSync = 0.0;
}

void MediaPresenter::useHWDecoder(bool enable)
{
    bool prev = this->m_useHWDecoder;

    this->changeUseHWDecoder(enable);

    if (prev != enable)
    {
        if (this->isEnabledVideo())
            this->recover(this->getMasterClock());
    }
}

bool MediaPresenter::isUseHWDecoder() const
{
    return this->m_useHWDecoder;
}

bool MediaPresenter::isOpenedHWDecoder() const
{
    return this->m_hwDecoder.isOpened();
}

void MediaPresenter::changeUseHWDecoder(bool enable)
{
    this->m_useHWDecoder = enable;
}

int MediaPresenter::getHWDecoderCount() const
{
    return this->m_hwDecoder.getDecoderCount();
}

bool MediaPresenter::setHWDecoderIndex(int index)
{
    int prev = this->m_hwDecoder.getDecoderIndex();

    if (this->m_hwDecoder.setDecoderIndex(index))
    {
        if (prev != index)
        {
            if (this->isEnabledVideo() && this->isUseHWDecoder())
                this->recover(this->getMasterClock());
        }

        return true;
    }

    return false;
}

int MediaPresenter::getHWDecoderIndex() const
{
    return this->m_hwDecoder.getDecoderIndex();
}

QString MediaPresenter::getHWDecoderName(int index) const
{
    return this->m_hwDecoder.getDecoderName(index);
}

void MediaPresenter::useLowQualityMode(bool enable)
{
    bool prev = this->m_videoRenderer.isUseLowQualityMode();

    this->m_videoRenderer.useLowQualityMode(enable);

    if (prev != enable)
    {
        if (this->isEnabledVideo())
            this->recover(this->getMasterClock());
    }
}

bool MediaPresenter::isUseLowQualityMode() const
{
    return this->m_videoRenderer.isUseLowQualityMode();
}

void MediaPresenter::useSPDIF(bool enable)
{
    this->m_audioRenderer.useSPDIF(enable);
    this->resetAudioStream();
}

bool MediaPresenter::isUseSPDIF() const
{
    return this->m_audioRenderer.isUseSPDIF();
}

void MediaPresenter::setSPDIFEncodingMethod(AnyVODEnums::SPDIFEncodingMethod method)
{
    this->m_audioRenderer.setSPDIFEncodingMethod(method);

    if (SPDIF::getInstance().isOpened())
        this->resetAudioStream();
}

AnyVODEnums::SPDIFEncodingMethod MediaPresenter::getSPDIFEncodingMethod() const
{
    return this->m_audioRenderer.getSPDIFEncodingMethod();
}

void MediaPresenter::setScreenRotationDegree(AnyVODEnums::ScreenRotationDegree degree)
{
    this->m_videoRenderer.setScreenRotationDegree(degree);
}

AnyVODEnums::ScreenRotationDegree MediaPresenter::getScreenRotationDegree() const
{
    return this->m_videoRenderer.getScreenRotationDegree();
}

void MediaPresenter::use3DFull(bool enable)
{
    this->m_videoRenderer.use3DFull(enable);
}

bool MediaPresenter::isUse3DFull() const
{
    return this->m_videoRenderer.isUse3DFull();
}

void MediaPresenter::usePBO(bool enable)
{
    this->m_videoRenderer.usePBO(enable);
}

bool MediaPresenter::isUsePBO() const
{
    return this->m_videoRenderer.isUsePBO();
}

bool MediaPresenter::isUsingPBO() const
{
    return this->isUsePBO() && this->isUsablePBO();
}

bool MediaPresenter::isUsablePBO() const
{
    return this->m_videoRenderer.isUsablePBO();
}

QSize MediaPresenter::getSize() const
{
    return this->m_videoRenderer.getSize();
}

void MediaPresenter::resetSPDIF()
{
    if (this->m_audioRenderer.isUseSPDIF())
        this->useSPDIF(this->m_audioRenderer.isUseSPDIF());
}

void MediaPresenter::releaseSubtitles()
{
    MediaState *ms = this->m_state;
    SubtitleFrames &frames = ms->subtitleFrames;

    frames.lock.lock();

    for (int i = 0; i < frames.items.count(); i++)
        avsubtitle_free(&frames.items[i].subtitle);

    frames.items.clear();

    frames.lock.unlock();
}

void MediaPresenter::releasePictures()
{
    MediaState *ms = this->m_state;

    if (!ms)
        return;

    ms->videoFrames.releaseFrames();
}

SyncType MediaPresenter::getRecommandSyncType() const
{
    if (this->m_state->audio.stream.index >= 0)
        return SYNC_AUDIO_MASTER;
    else if (this->m_state->video.stream.index >= 0)
        return SYNC_VIDEO_MASTER;
    else
        return SYNC_EXTERNAL_MASTER;
}

bool MediaPresenter::openAudioStream(int *ret)
{
    AVFormatContext *format = nullptr;
    MediaState *ms = this->m_state;
    QString filePath = this->m_audioPath;
    int audioIndex = -1;

    format = avformat_alloc_context();

    if (!format)
        return false;

    format->interrupt_callback.callback = MediaPresenter::decodingInterruptCallback;
    format->interrupt_callback.opaque = ms;

    if (avformat_open_input(&format, PathUtils::convertPathToFileSystemRepresentation(filePath, false), nullptr, nullptr) != 0)
    {
        avformat_close_input(&format);
        return false;
    }

    if (!this->initIOOptions(filePath, format))
        return false;

    ms->audioFormat = format;

    if (avformat_find_stream_info(format, nullptr) < 0)
        return false;

    this->m_audioStreamInfo.clear();

    for (unsigned int i = 0; i < format->nb_streams; i++)
    {
        AVCodecParameters *context = format->streams[i]->codecpar;
        AVDictionary *meta = format->streams[i]->metadata;
        AVDictionaryEntry *entry;
        QString desc;
        QString lang;

        entry = av_dict_get(meta, "language", nullptr, AV_DICT_IGNORE_SUFFIX);

        if (entry)
            lang = QString::fromLocal8Bit(entry->value).toUpper();

        AVCodec *codec = avcodec_find_decoder(context->codec_id);

        if (codec)
            desc = QString("%1 %2").arg(codec->long_name, this->findProfileName(codec->profiles, context->profile));

        if (!lang.isEmpty())
            desc = lang + ", " + desc;

        switch (context->codec_type)
        {
            case AVMEDIA_TYPE_AUDIO:
            {
                AudioStreamInfo info;

                info.index = i;
                info.name = QString("%1, %2 Channels").arg(desc).arg(context->channels);

                this->m_audioStreamInfo.append(info);

                break;
            }
            default:
            {
                break;
            }
        }
    }

    if (this->m_audioStreamInfo.count() > 0)
    {
        if (this->m_lastAudioStream == -1 || this->m_lastAudioStream >= (int)format->nb_streams ||
                format->streams[this->m_lastAudioStream]->codecpar->codec_type != AVMEDIA_TYPE_AUDIO)
        {
            audioIndex = this->m_audioStreamInfo[0].index;
            this->m_lastAudioStream = audioIndex;
        }
        else
        {
            audioIndex = this->m_lastAudioStream;
        }
    }

    *ret = audioIndex;

    return true;
}

bool MediaPresenter::initIOOptions(const QString &filePath, AVFormatContext *context) const
{
    bool success = true;

    if (context->pb && context->pb->av_class)
    {
        const AVClass *&avClass = context->pb->av_class;
        int flags = AV_OPT_SEARCH_CHILDREN;

        if (filePath.contains("http://", Qt::CaseInsensitive) ||
            filePath.contains("https://", Qt::CaseInsensitive))
        {
            if (!this->m_isLive)
                success = av_opt_set_int(&avClass, "reconnect", 1, flags) == 0;
        }
    }

    return success;
}

QString MediaPresenter::adjustProtocol(QString url) const
{
    url.replace("mms://", "mmst://", Qt::CaseInsensitive);

    if (url.startsWith(LIVE))
        url.remove(0, LIVE.length());

    return url;
}

bool MediaPresenter::openStream()
{
    MediaState *ms = new MediaState;
    int videoIndex = -1;
    int audioIndex = -1;
    int subtitleIndex = -1;
    AVFormatContext *format = nullptr;
    QString filePath;
    QFileInfo fileInfo(this->m_filePath);

    this->m_state = ms;
    this->m_videoRenderer.setup();

    ms->video.stream.index = -1;
    ms->audio.stream.index = -1;
    ms->subtitle.stream.index = -1;

    this->m_assParser.init();

    if (fileInfo.suffix().toLower() == "cue")
    {
        CueParser parser;

        if (parser.open(this->m_filePath))
        {
            parser.getFilePath(&filePath);
            parser.getChapters(&this->m_chapters);
            parser.close();

            filePath = fileInfo.absolutePath() + QDir::separator() + filePath;
        }
    }
    else
    {
        filePath = this->m_filePath;
    }

    filePath = this->adjustProtocol(filePath);

    AVInputFormat *inputFormat = nullptr;
    bool isDevice = false;

#if !defined Q_OS_ANDROID && !defined Q_OS_IOS
    if (this->m_isDevice || DeviceUtils::determinIsDeviceHaveDuration(filePath))
    {
        QString format = DeviceUtils::getDeviceType(filePath);

        inputFormat = av_find_input_format(format.toUtf8());

        if (inputFormat)
            filePath = DeviceUtils::getDevicePath(filePath);

        isDevice = true;
    }
#endif

    format = avformat_alloc_context();

    if (!format)
        return false;

    format->interrupt_callback.callback = MediaPresenter::decodingInterruptCallback;
    format->interrupt_callback.opaque = ms;

    QByteArray fileSystemPath = PathUtils::convertPathToFileSystemRepresentation(filePath, isDevice);
    char *inputFilePath = nullptr;

#ifdef Q_OS_MACOS
    if (!DeviceUtils::determinIsDeviceHaveDuration(this->m_filePath) && DeviceUtils::getDeviceType(this->m_filePath) != DeviceUtils::CDIO_PROTOCOL)
        inputFilePath = fileSystemPath.data();
#else
    inputFilePath = fileSystemPath.data();
#endif

    AVDictionary *options = nullptr;

    if (!this->m_playData.query.isEmpty())
    {
        QByteArray query = this->m_playData.query.toString(QUrl::FullyEncoded).prepend("?").toLatin1();

        av_dict_set(&options, "query_string", query.data(), 0);
    }

    if (avformat_open_input(&format, inputFilePath, inputFormat, &options) != 0)
    {
        avformat_close_input(&format);

        if (options)
            av_dict_free(&options);

        return false;
    }

    if (options)
        av_dict_free(&options);

    if (!this->initIOOptions(filePath, format))
        return false;

    ms->format = format;

    if (avformat_find_stream_info(format, nullptr) < 0)
        return false;

    AVInputFormat *iformat = format->iformat;

    if (iformat)
        this->m_detail.fileFormat = QString("%1 (%2)").arg(QString(iformat->name).toUpper(), iformat->long_name);

    for (unsigned int i = 0; i < format->nb_chapters; i++)
    {
        AVChapter *chapter = format->chapters[i];
        double base = av_q2d(chapter->time_base);
        ChapterInfo info;
        AVDictionaryEntry *entry = av_dict_get(chapter->metadata, "title", nullptr, AV_DICT_IGNORE_SUFFIX);

        info.start = base * chapter->start;
        info.end = base * chapter->end;

        if (entry)
            info.desc = QString::fromUtf8(entry->value);

        this->m_chapters.push_back(info);
    }

    int videoNum = 1;
    int subtitleNum = 1;

    for (unsigned int i = 0; i < format->nb_streams; i++)
    {
        AVCodecParameters *context = format->streams[i]->codecpar;
        AVDictionary *meta = format->streams[i]->metadata;
        AVDictionaryEntry *entry;
        QString desc;
        QString lang;

        entry = av_dict_get(meta, "language", nullptr, AV_DICT_IGNORE_SUFFIX);

        if (entry)
            lang = QString::fromLocal8Bit(entry->value).toUpper();

        AVCodec *codec = avcodec_find_decoder(context->codec_id);

        if (codec)
            desc = QString("%1 %2").arg(codec->long_name, this->findProfileName(codec->profiles, context->profile));

        if (!lang.isEmpty())
            desc = lang + ", " + desc;

        switch (context->codec_type)
        {
            case AVMEDIA_TYPE_VIDEO:
            {
                if (videoIndex < 0)
                    videoIndex = i;

                if (this->isAudio() && !this->m_showAlbumJacket)
                    videoIndex = -1;

                if (FloatPointUtils::zeroDouble(this->m_videoRenderer.getRotation()) <= 0.0)
                {
                    QString rotation;

                    entry = av_dict_get(meta, "rotate", nullptr, AV_DICT_MATCH_CASE);

                    if (entry)
                        rotation = QString::fromLocal8Bit(entry->value);

                    this->m_videoRenderer.setRotation(rotation.toDouble());
                }

                if (videoIndex != -1)
                {
                    VideoStreamInfo info;

                    info.index = i;
                    info.name = QString("Track (%1)").arg(videoNum);
                    info.size = QSize(context->width, context->height);

                    videoNum++;

                    this->m_videoStreamInfo.append(info);
                }

                break;
            }
            case AVMEDIA_TYPE_AUDIO:
            {
                AudioStreamInfo info;

                info.index = i;
                info.name = QString("%1, %2 Channels").arg(desc).arg(context->channels);

                this->m_audioStreamInfo.append(info);

                break;
            }
            case AVMEDIA_TYPE_SUBTITLE:
            {
                SubtitleStreamInfo info;

                info.index = i;
                info.name = QString("%1 (%2)").arg(desc).arg(subtitleNum);
                info.size = QSize(context->width, context->height);

                subtitleNum++;

                this->m_subtitleStreamInfo.append(info);

                break;
            }
            case AVMEDIA_TYPE_ATTACHMENT:
            {
                switch (context->codec_id)
                {
                    case AV_CODEC_ID_OTF:
                    case AV_CODEC_ID_TTF:
                    {
                        AVDictionaryEntry *fileName = av_dict_get(meta, "filename", nullptr, 0);

                        if (fileName && fileName->value)
                            this->m_assParser.addFont(fileName->value, context->extradata, context->extradata_size);

                        break;
                    }
                    default:
                    {
                        break;
                    }
                }

                break;
            }
            default:
            {
                break;
            }
        }
    }

    if (this->m_videoStreamInfo.count() > 0)
    {
        if (this->m_lastVideoStream == -1 || this->m_lastVideoStream >= (int)format->nb_streams ||
                format->streams[this->m_lastVideoStream]->codecpar->codec_type != AVMEDIA_TYPE_VIDEO)
        {
            videoIndex = this->m_videoStreamInfo[0].index;
            this->m_lastVideoStream = videoIndex;
        }
        else
        {
            videoIndex = this->m_lastVideoStream;
        }
    }

    if (this->m_audioStreamInfo.count() > 0)
    {
        if (this->m_lastAudioStream == -1 || this->m_lastAudioStream >= (int)format->nb_streams ||
                format->streams[this->m_lastAudioStream]->codecpar->codec_type != AVMEDIA_TYPE_AUDIO)
        {
            audioIndex = this->m_audioStreamInfo[0].index;
            this->m_lastAudioStream = audioIndex;
        }
        else
        {
            audioIndex = this->m_lastAudioStream;
        }
    }

    if (this->m_subtitleStreamInfo.count() > 0)
    {
        if (this->m_lastSubtitleStream == -1 || this->m_lastSubtitleStream >= (int)format->nb_streams ||
                format->streams[this->m_lastSubtitleStream]->codecpar->codec_type != AVMEDIA_TYPE_SUBTITLE)
        {
            subtitleIndex = this->m_subtitleStreamInfo[0].index;
            this->m_lastSubtitleStream = subtitleIndex;
        }
        else
        {
            subtitleIndex = this->m_lastSubtitleStream;
        }
    }

    if (this->isUseAudioPath())
    {
        int retry = 3;
        bool success = false;

        while (retry --> 0)
        {
            if (this->openAudioStream(&audioIndex))
            {
                success = true;
                break;
            }
        }

        if (!success)
            return false;
    }

    if (this->isRemoteFile() && this->isAudio())
        videoIndex = -1;

    if (videoIndex >= 0)
    {
        if (!this->openStreamComponent(videoIndex, false))
            return false;
    }

    if (audioIndex >= 0)
    {
        bool success = this->openStreamComponent(audioIndex, this->isUseAudioPath());

        if (!success)
        {
            audioIndex = -1;

            if (this->isAudio())
                return false;
        }
    }

    if (ms->video.stream.index < 0 && ms->audio.stream.index < 0)
        return false;

    if (subtitleIndex >= 0)
        this->openStreamComponent(subtitleIndex, false);

    if (this->m_assParser.isExist() && !this->m_subtitleFilePath.isEmpty())
    {
        QFileInfo f(this->m_subtitleFilePath);

        if (f.exists())
            this->m_assParser.open(this->m_subtitleFilePath);
    }

    this->m_assParser.setDefaultFont(this->m_assFontFamily);

    ms->externalClock.base = this->getAbsoluteClock();
    ms->syncType = this->getRecommandSyncType();

    bool calDuration = true;

    if (this->isRemoteProtocol())
    {
        char path[MAX_FILEPATH_CHAR_SIZE];
        QString pathPart;

        av_url_split(nullptr, 0, nullptr, 0, nullptr, 0, nullptr, path, sizeof(path), this->m_realFilePath.toUtf8().constData());
        pathPart = QString::fromUtf8(path);

        calDuration = !QFileInfo(pathPart).fileName().isEmpty();
    }

    if (calDuration && this->m_playData.duration == 0.0)
        this->m_playData.duration = format->duration / (double)AV_TIME_BASE;

    if (this->m_playData.duration < 0.0)
        this->m_playData.duration = 0.0;

    if (this->m_playData.userData.isEmpty())
        this->m_detail.fileName = StringUtils::normalizedString(QFileInfo(this->m_realFilePath).fileName());
    else
        this->m_detail.fileName = this->m_title;

    this->m_detail.totalTime = this->getDuration();

    if (this->m_playData.totalFrame <= 0 && videoIndex >= 0)
    {
        double total = av_q2d(format->streams[videoIndex]->avg_frame_rate);

        this->m_playData.totalFrame = this->getDuration() * total;
    }

    this->m_detail.videoTotalFrame = this->m_playData.totalFrame;

    if (this->m_artist.isEmpty())
    {
        AVDictionary *meta = format->metadata;
        AVDictionaryEntry *entry;

        entry = av_dict_get(meta, "artist", nullptr, AV_DICT_IGNORE_SUFFIX);

        if (entry)
            this->m_artist = QString::fromUtf8(entry->value);
    }

    if (audioIndex >= 0)
    {
        if (!this->m_audioRenderer.play())
            return false;
    }

    this->startReadThread();

    this->refreshSchedule(FIRST_REFRESH_DELAY);

    this->m_refreshThread.setVideoEnabled(this->isEnabledVideo());
    this->m_refreshThread.start();

    return true;
}

void MediaPresenter::startReadThread()
{
    this->m_state->readThreadQuit = false;

    this->m_readThread.setup(this->m_state, this);
    this->m_readThread.start();
}

void MediaPresenter::seekStream(double pos, double dir, int flag)
{
    MediaState *ms = this->m_state;
    Seek &seek = ms->seek;

    if (seek.discard || seek.request || (!seek.readable && !ms->pause.pause))
        return;

    seek.flags = dir < 0.0 ? AVSEEK_FLAG_BACKWARD : 0;
    seek.flags |= flag;

    double dur = this->getDuration();

    if (pos > dur)
    {
        pos = dur;
        seek.flags |= AVSEEK_FLAG_BACKWARD;
    }

    if (IS_BIT_SET(seek.flags, AVSEEK_FLAG_BACKWARD))
        ms->willBeEnd = false;

    seek.time = pos + this->getAudioClockOffset();
    seek.pos = (int64_t)(seek.time * AV_TIME_BASE);

    if (ms->pause.pause)
    {
        if (this->isAudio())
        {
            seek.pauseSeeking = false;
            seek.inc += dir;
        }
        else
        {
            seek.pauseSeeking = true;
            seek.flushed = false;
        }
    }

    bool discard = IS_BIT_SET(seek.flags, AVSEEK_FLAG_ANY);

    if (discard)
    {
        if (ms->video.stream.index != -1)
        {
            if (!this->isAudio())
                ms->video.stream.discard = true;

            ms->video.stream.discardCount = 0;
        }

        if (ms->audio.stream.index != -1)
        {
            ms->audio.stream.discard = true;
            ms->audio.stream.discardCount = 0;

            this->m_audioRenderer.pause();
        }

        if (ms->subtitle.stream.index != -1)
        {
            ms->subtitle.stream.discard = true;
            ms->subtitle.stream.discardCount = 0;
        }

        seek.flags &= ~AVSEEK_FLAG_ANY;
        seek.discardTime = pos;
        seek.readDiscardStartTime = this->getAbsoluteClock();
    }

    seek.request = true;

    if (discard)
        seek.discard = true;

    ms->seek.readable = false;
    ms->subtitle.seekFlags = seek.flags;
}

void MediaPresenter::processSkipRange()
{
    if (!this->m_state || !this->m_state->seek.readable || this->m_state->seek.discard || !this->hasDuration())
        return;

    for (int i = 0; i < this->m_skipRanges.count(); i++)
    {
        Range &range = this->m_skipRanges[i];

        if (range.end <= range.start)
        {
            if (range.start > 0.0 && range.end > 0.0)
            {
                range.enable = false;
                continue;
            }
        }

        double curTime = this->getCurrentPosition();
        QString startTime;
        QString endTime;
        double destTime = -1.0;
        QString desc;

        if (range.start < 0.0 && curTime < range.end)
        {
            if (!this->m_skipOpening || range.end <= 0.0)
                continue;

            destTime = range.end;
            ConvertingUtils::getTimeString(range.end, ConvertingUtils::TIME_HH_MM_SS, &endTime);

            desc = tr("오프닝 스킵 : %1").arg(endTime);
        }
        else if (this->m_playData.duration - range.start <= curTime && range.end < 0.0)
        {
            if (!this->m_skipEnding)
                continue;

            if (this->m_ended.callback)
                this->m_ended.callback(this->m_ended.userData);

            this->pause();
        }
        else if (range.start <= curTime && curTime < range.end)
        {
            if (!range.enable || !this->m_useSkipRange)
                continue;

            destTime = range.end;
            ConvertingUtils::getTimeString(range.start, ConvertingUtils::TIME_HH_MM_SS, &startTime);
            ConvertingUtils::getTimeString(range.end, ConvertingUtils::TIME_HH_MM_SS, &endTime);

            desc = tr("재생 스킵 : %1 ~ %2").arg(startTime, endTime);
        }

        if (destTime >= 0.0 && destTime < this->getDuration())
        {
            const double offset = destTime > 0.0 ? 0.5 : -0.5;
            QMutexLocker locker(&this->m_controlLocker);

            this->seekStream(destTime + offset, destTime - curTime, AVSEEK_FLAG_ANY | AVSEEK_FLAG_BACKWARD);

            if (!desc.isEmpty())
                this->showOptionDesc(desc);

            break;
        }
    }
}

void MediaPresenter::callEmptyCallbackInternal(bool show)
{
    if (this->m_emptyBufferCallback.callback)
        this->m_emptyBufferCallback.callback(this->m_emptyBufferCallback.userData, show);
}

void MediaPresenter::run()
{
    const int playTime = 100;
    const int detailTime = 1000;
    const int cpuUsageTime = 1000;
    const int usingMemTime = 1000;
    const int showOptionDescTime = OPTION_DESC_TIME;
    QElapsedTimer playingTimer;
    QElapsedTimer showOptionDescTimer;
    QElapsedTimer detailTimer;
    QElapsedTimer cpuUsageTimer;
    QElapsedTimer usingMemTimer;
    MediaState *ms = this->m_state;

    playingTimer.start();
    showOptionDescTimer.start();
    detailTimer.start();
    cpuUsageTimer.start();
    usingMemTimer.start();

    while (!this->m_forceExit)
    {
        this->m_detail.currentTime = this->getCurrentPosition();
        this->m_detail.timePercentage = (this->m_detail.currentTime / this->m_detail.totalTime) * 100.0;

        if (std::isnan(this->m_detail.timePercentage))
            this->m_detail.timePercentage = 0.0;

        this->m_detail.videoCurrentFrame.fetchAndStoreOrdered((this->m_detail.timePercentage * this->m_detail.videoTotalFrame) / 100.0);

        if (detailTimer.elapsed() >= detailTime)
        {
            float elapsed = detailTimer.elapsed() / 1000.0f;

            detailTimer.restart();
            this->m_detail.videoFPS = this->m_detail.videoFrameCount.fetchAndStoreOrdered(0) / elapsed;
            this->m_detail.videoInputByteRate = this->m_detail.videoInputByteCount.fetchAndStoreOrdered(0) / elapsed;
            this->m_detail.videoOutputByteRate = this->m_detail.videoOutputByteCount.fetchAndStoreOrdered(0) / elapsed;
            this->m_detail.audioInputByteRate = this->m_detail.audioInputByteCount.fetchAndStoreOrdered(0) / elapsed;
            this->m_detail.audioOutputByteRate = this->m_detail.audioOutputByteCount.fetchAndStoreOrdered(0) / elapsed;
            this->m_detail.dtvSignal = true;
        }

        if (cpuUsageTimer.elapsed() >= cpuUsageTime)
        {
            cpuUsageTimer.restart();
            this->m_detail.cpuUsage = SystemResourceUsage::getInstance().getCPUUsage();
        }

        if (usingMemTimer.elapsed() >= usingMemTime)
        {
            usingMemTimer.restart();
            this->m_detail.usingMemSize = SystemResourceUsage::getInstance().getUsingMemSize();
        }

        if (playingTimer.elapsed() >= playTime)
        {
            if (ms && this->m_readThread.isRunning() && ms->seek.readable)
            {
                if (this->m_playing.callback)
                    this->m_playing.callback(this->m_playing.userData);
            }

            playingTimer.restart();
        }

        if (this->m_videoRenderer.isShowOptionDesc())
        {
            this->m_videoRenderer.displayOptionDesc();
            showOptionDescTimer.restart();
        }

        if (this->m_videoRenderer.isShowingOptionDesc() && showOptionDescTimer.elapsed() > showOptionDescTime)
        {
            this->m_videoRenderer.hideOptionDesc();

            if (this->m_showAudioOptionDescCallback.callback && this->isAudio())
                this->m_showAudioOptionDescCallback.callback(this->m_showAudioOptionDescCallback.userData, QString(), false);
        }

        if (this->m_repeatRange.enable && this->m_state->seek.readable && !this->m_state->seek.discard)
        {
            if (this->m_repeatRange.end <= this->m_repeatRange.start)
            {
                this->m_repeatRange.enable = false;
            }
            else
            {
                double curTime = this->getCurrentPosition();

                if (curTime > this->m_repeatRange.end)
                {
                    QString startTime;
                    QString endTime;
                    QMutexLocker locker(&this->m_controlLocker);

                    ConvertingUtils::getTimeString(this->getRepeatStart(), ConvertingUtils::TIME_HH_MM_SS_ZZZ, &startTime);
                    ConvertingUtils::getTimeString(this->getRepeatEnd(), ConvertingUtils::TIME_HH_MM_SS_ZZZ, &endTime);

                    this->seekStream(this->m_repeatRange.start, this->m_repeatRange.start - curTime, AVSEEK_FLAG_ANY);
                    this->showOptionDesc(tr("구간 반복 : %1 ~ %2").arg(startTime, endTime));
                }
            }
        }

        if (SPDIF::getInstance().isOpened() && SPDIF::getInstance().isExceededFailCount())
        {
            this->useSPDIF(false);
            this->showOptionDesc(tr("S/PDIF 출력을 지원하지 않은 포맷이므로 PCM 출력으로 전환합니다."));
        }

        if (this->m_hwDecoder.isOpened() && this->m_hwDecoder.getShouldStop())
        {
            this->useHWDecoder(false);
            this->showOptionDesc(tr("하드웨어 디코딩을 지원하지 않는 코덱이므로 하드웨어 디코딩을 끕니다."));
        }

        this->processSkipRange();

        this->msleep(WATCHER_DELAY);
    }
}
