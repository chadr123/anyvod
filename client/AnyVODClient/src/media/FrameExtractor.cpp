﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "FrameExtractor.h"
#include "utils/PixelUtils.h"
#include "utils/PathUtils.h"
#include "utils/FloatPointUtils.h"
#include "utils/MediaTypeUtils.h"
#include "utils/RemoteFileUtils.h"
#include "utils/DeviceUtils.h"
#include "utils/FrameUtils.h"

#include <QFileInfo>
#include <QDebug>

#ifndef UNICODE
# define UNICODE
#endif

#ifndef _UNICODE
# define _UNICODE
#endif

#if !defined Q_OS_MOBILE
# include <MediaInfoDLL/MediaInfo.h>
#endif

extern "C"
{
# include <libavformat/avformat.h>
# include <libswscale/swscale.h>

# include <libavutil/opt.h>
# include <libavutil/imgutils.h>

# include <libavfilter/buffersrc.h>
# include <libavfilter/buffersink.h>
}

#if !defined Q_OS_MOBILE
using namespace MediaInfoDLL;
#endif

const int FrameExtractor::MAX_READ_RETRY_COUNT = 5;

FrameExtractor::FrameExtractor() :
    m_duration(0.0),
    m_width(0),
    m_height(0),
    m_format(nullptr),
    m_ctx(nullptr),
    m_imageConverter(nullptr),
    m_index(-1),
    m_rotation(0.0),
    m_pixFormat(AV_PIX_FMT_RGB32),
    m_useAutoColorConversion(false),
    m_in(nullptr),
    m_colorConversion(nullptr),
    m_eq(nullptr),
    m_out(nullptr),
    m_graph(nullptr)
{

}

FrameExtractor::~FrameExtractor()
{
    this->close();
}

bool FrameExtractor::openWithType(const QString &filePath, bool autoColorConversion, bool *isVideo)
{
    if (this->m_format)
        return false;

    QString path = filePath;

    path = path.replace("mms://", "mmst://", Qt::CaseInsensitive);

    if (avformat_open_input(&this->m_format, PathUtils::convertPathToFileSystemRepresentation(path, false), nullptr, nullptr) != 0)
    {
        this->close();
        return false;
    }

    if (avformat_find_stream_info(this->m_format, nullptr) < 0)
    {
        this->close();
        return false;
    }

    int videoIndex = -1;

    for (unsigned int i = 0; i < this->m_format->nb_streams; i++)
    {
        AVCodecParameters *context = this->m_format->streams[i]->codecpar;

        if (context->codec_type == AVMEDIA_TYPE_VIDEO)
        {
            videoIndex = i;
            break;
        }
    }

    if (videoIndex < 0)
    {
        this->close();
        return false;
    }

    this->m_index = videoIndex;

    AVStream *stream = this->m_format->streams[videoIndex];
    AVCodecContext *context = avcodec_alloc_context3(nullptr);

    if (avcodec_parameters_to_context(context, stream->codecpar) < 0)
    {
        avcodec_free_context(&context);
        return false;
    }

    context->pkt_timebase = stream->time_base;

    this->m_ctx = context;

    AVCodec *codec = nullptr;
    AVCodecID codecID = context->codec_id;

    codec = avcodec_find_decoder(codecID);

    if (!codec)
    {
        this->close();
        return false;
    }

    if (avcodec_open2(context, codec, nullptr) < 0)
    {
        this->close();
        return false;
    }

#if !defined Q_OS_MOBILE
    MediaInfo mi;
    bool isDevice = false;
    bool isRemoteProtocol = false;

    isDevice = DeviceUtils::determinDevice(path);
    isRemoteProtocol = RemoteFileUtils::determinRemoteProtocol(path);

    if (!isDevice && !isRemoteProtocol && mi.Open(path.toStdWString()) == 1)
    {
        QString totalTime = QString::fromStdWString(mi.Get(Stream_General, 0, __T("Duration")));

        if (totalTime.toDouble() < 0.0)
            this->m_duration = this->m_format->duration / (double)AV_TIME_BASE;
        else
            this->m_duration = totalTime.toDouble() / 1000.0;

        QString rotation = QString::fromStdWString(mi.Get(Stream_Video, 0, __T("Rotation")));

        this->m_rotation = rotation.toDouble();

        mi.Close();
    }
    else
#endif
    {
        this->m_duration = this->m_format->duration / (double)AV_TIME_BASE;
    }

    if (this->m_duration < 0.0)
        this->m_duration = 0.0;

    if (FloatPointUtils::zeroDouble(this->m_rotation) <= 0.0)
    {
        AVDictionary *meta = stream->metadata;
        AVDictionaryEntry *entry;
        QString rotation;

        entry = av_dict_get(meta, "rotate", nullptr, AV_DICT_MATCH_CASE);

        if (entry)
            rotation = QString::fromLocal8Bit(entry->value);

        this->m_rotation = rotation.toDouble();
    }

    if (isVideo)
    {
        int64_t frames = stream->nb_frames;

        if (frames <= 0)
            frames = av_q2d(stream->avg_frame_rate) * this->m_duration;

        *isVideo = !(frames <= 0 && MediaTypeUtils::isExtension(QFileInfo(path).suffix(), MT_AUDIO));
    }

    this->m_width = context->width;
    this->m_height = context->height;

#ifndef Q_OS_RASPBERRY_PI
    if (autoColorConversion && context->color_primaries == AVCOL_PRI_BT2020)
    {
        if (!this->initFilter())
        {
            this->close();
            return false;
        }
    }

    this->m_useAutoColorConversion = autoColorConversion;
#else
    (void)autoColorConversion;
#endif

    return true;
}

void FrameExtractor::close()
{
    if (this->m_ctx)
        avcodec_free_context(&this->m_ctx);

    if (this->m_format)
        avformat_close_input(&this->m_format);

    if (this->m_imageConverter)
    {
        sws_freeContext(this->m_imageConverter);
        this->m_imageConverter = nullptr;
    }

    this->unInitFilter();

    this->m_index = -1;
    this->m_duration = 0.0;
    this->m_width = 0;
    this->m_height = 0;
    this->m_rotation = 0.0;
    this->m_useAutoColorConversion = false;
}

bool FrameExtractor::isVideo(const QString &filePath)
{
    bool isVideo = false;

    if (this->openWithType(filePath, this->m_useAutoColorConversion, &isVideo))
        this->close();

    return isVideo;
}

void FrameExtractor::setPixFormat(AVPixelFormat pixFormat)
{
    this->m_pixFormat = pixFormat;
}

AVPixelFormat FrameExtractor::getPixFormat() const
{
    return this->m_pixFormat;
}

bool FrameExtractor::getFrame(double time, bool ignoreTime, FrameExtractor::FrameItem *ret)
{
    if (!this->m_format)
        return false;

    AVRational r = {1, AV_TIME_BASE};
    AVStream *stream = this->m_format->streams[this->m_index];

    if (!ignoreTime)
    {
        int64_t target = (int64_t)(time * AV_TIME_BASE);

        target = av_rescale_q(target, r, stream->time_base);

        if (av_seek_frame(this->m_format, this->m_index, target, AVSEEK_FLAG_BACKWARD) < 0)
            return false;

        avcodec_flush_buffers(this->m_ctx);
    }

    bool ok = false;
    AVPacket *packet = av_packet_alloc();

    while (true)
    {
        double pts = -1.0;

        if (!this->readPacket(&pts, packet))
            break;

        if (this->decodePacket(time, ignoreTime, *packet, ret))
        {
            ok = true;
            av_packet_unref(packet);

            break;
        }
        else
        {
            av_packet_unref(packet);

            if (pts > 0.0)
                continue;
        }
    }

    av_packet_free(&packet);

    return ok;
}

bool FrameExtractor::getFramesByPeriod(double period, QVector<FrameExtractor::FrameItem> *ret)
{
    if (!this->m_format)
        return false;

    if (this->m_duration <= 0.0)
        return false;

    for (double time = 0.0; time <= this->m_duration; time += period)
    {
        FrameItem item;

        if (this->getFrame(time, false, &item))
            ret->append(item);
    }

    return true;
}

double FrameExtractor::getDuration() const
{
    return this->m_duration;
}

int FrameExtractor::getWidth() const
{
    return this->m_width;
}

int FrameExtractor::getHeight() const
{
    return this->m_height;
}

bool FrameExtractor::initFilter()
{
    this->m_graph = avfilter_graph_alloc();

    if (!this->m_graph)
        return false;

    this->m_graph->scale_sws_opts = av_strdup("flags=neighbor");

    const AVFilter *in = avfilter_get_by_name("buffer");
    const AVFilter *out = avfilter_get_by_name("buffersink");
    QString inParam;
    AVPixelFormat pixFormats[] = {this->m_pixFormat, AV_PIX_FMT_NONE};
    AVStream *stream = this->m_format->streams[this->m_index];

    inParam = QString("video_size=%1x%2:pix_fmt=%3:time_base=%4/%5:pixel_aspect=%6/%7")
            .arg(this->m_ctx->width)
            .arg(this->m_ctx->height)
            .arg(this->m_ctx->pix_fmt)
            .arg(stream->time_base.num)
            .arg(stream->time_base.den)
            .arg(this->m_ctx->sample_aspect_ratio.num)
            .arg(this->m_ctx->sample_aspect_ratio.den);

    if (avfilter_graph_create_filter(&this->m_in, in, "in", inParam.toLatin1(), nullptr, this->m_graph) < 0)
    {
        this->unInitFilter();
        return false;
    }

    this->m_filters.append(this->m_in);

    QString param = "all=bt709";
    const AVFilter *filter = avfilter_get_by_name("colorspace");

    if (avfilter_graph_create_filter(&this->m_colorConversion, filter, "colorspace", param.toLatin1(), nullptr, this->m_graph) < 0)
    {
        this->unInitFilter();
        return false;
    }

    this->m_filters.append(this->m_colorConversion);

    param = "gamma=0.78";
    filter = avfilter_get_by_name("eq");

    if (avfilter_graph_create_filter(&this->m_eq, filter, "eq", param.toLatin1(), nullptr, this->m_graph) < 0)
    {
        this->unInitFilter();
        return false;
    }

    this->m_filters.append(this->m_eq);

    if (avfilter_graph_create_filter(&this->m_out, out, "out", nullptr, nullptr, this->m_graph) < 0)
    {
        this->unInitFilter();
        return false;
    }

    if (av_opt_set_int_list(this->m_out, "pix_fmts", pixFormats, AV_PIX_FMT_NONE, AV_OPT_SEARCH_CHILDREN) < 0)
        return false;

    this->m_filters.append(this->m_out);

    if (!this->linkFilter())
    {
        this->unInitFilter();
        return false;
    }

    if (avfilter_graph_config(this->m_graph, nullptr) < 0)
    {
        this->unInitFilter();
        return false;
    }

    return true;
}

void FrameExtractor::unInitFilter()
{
    if (this->m_graph)
        avfilter_graph_free(&this->m_graph);

    this->m_in = nullptr;
    this->m_out = nullptr;
    this->m_colorConversion = nullptr;
    this->m_eq = nullptr;
}

bool FrameExtractor::linkFilter()
{
    if (this->m_filters.count() < 3)
        return false;

    AVFilterContext *prev = this->m_filters.first();

    for (int i = 1; i < this->m_filters.count(); i++)
    {
        AVFilterContext *next = this->m_filters[i];

        if (avfilter_link(prev, 0, next, 0) < 0)
            return false;

        prev = next;
    }

    return true;
}

bool FrameExtractor::open(const QString &filePath, bool autoColorConversion)
{
    return this->openWithType(filePath, autoColorConversion, nullptr);
}

bool FrameExtractor::readPacket(double *retPTS, AVPacket *ret)
{
    AVStream *stream = this->m_format->streams[this->m_index];

    while (true)
    {
        int error;
        int retryCount = MAX_READ_RETRY_COUNT;

        do
        {
            error = av_read_frame(this->m_format, ret);

            if (error < 0)
            {
                if (AVERROR(EAGAIN) != error)
                    return false;
            }
            else
            {
                retryCount = 0;
            }
        }
        while (retryCount --> 0);

        if (ret->stream_index == this->m_index)
        {
            if (ret->dts != AV_NOPTS_VALUE)
            {
                double pts = av_q2d(stream->time_base) * ret->dts;

                *retPTS = pts;
            }

            break;
        }
        else
        {
            av_packet_unref(ret);
            continue;
        }
    }

    return true;
}

bool FrameExtractor::decodePacket(double time, bool ignoreTime, AVPacket &packet, FrameExtractor::FrameItem *ret)
{
    AVFrame *frame = av_frame_alloc();
    AVStream *stream = this->m_format->streams[this->m_index];
    int success;
    int decoded;
    bool ok = false;

    success = FrameUtils::decodeFrame(this->m_ctx, &packet, frame, &decoded);

    if (success > 0 && decoded)
    {
        double pts = frame->best_effort_timestamp;

        pts *= av_q2d(stream->time_base);

        if (pts >= time || ignoreTime)
        {
            AVCodecContext *codec = this->m_ctx;
            int w = codec->width;
            int h = codec->height;
            AVFrame *inFrame;
            AVFrame filterd;
            AVPixelFormat format = codec->pix_fmt;
            bool hasFilter = this->m_filters.count() > 0;

            if (hasFilter)
            {
                if (!this->getFilteredFrame(*frame, &filterd, &format))
                {
                    av_frame_free(&frame);
                    return false;
                }

                inFrame = &filterd;
            }
            else
            {
                inFrame = frame;
            }

            this->m_imageConverter = sws_getCachedContext(
                        this->m_imageConverter,
                        w, h, format,
                        w, h, this->m_pixFormat,
                        SWS_POINT, nullptr, nullptr, nullptr);

            AVFrame pict;

            memset(&pict, 0, sizeof(pict));

            ret->buffer = (uint8_t*)av_malloc(w * h * GET_PIXEL_SIZE(true) * sizeof(uint8_t));

            pict.data[0] = ret->buffer;
            pict.linesize[0] = w * GET_PIXEL_SIZE(true);

            if (this->m_imageConverter)
                sws_scale(this->m_imageConverter, inFrame->data, inFrame->linesize, 0, h, pict.data, pict.linesize);

            ret->time = time;
            ret->rotation = this->m_rotation;
            ret->realTime = pts;
            ret->frame = QImage(ret->buffer, w, h, QImage::Format_ARGB32, av_free, ret->buffer);

            if (hasFilter)
                av_freep(&filterd.data[0]);

            ok = true;
        }
    }

    av_frame_free(&frame);

    return ok;
}

bool FrameExtractor::getFilteredFrame(const AVFrame &in, AVFrame *out, AVPixelFormat *retFormat)
{
    class localPtr
    {
    public:
        localPtr() :
            m_in(av_frame_alloc()),
            m_out(av_frame_alloc())
        {

        }

        ~localPtr()
        {
            if (this->m_in)
                av_frame_free(&this->m_in);

            if (this->m_out)
                av_frame_free(&this->m_out);
        }

        AVFrame *m_in;
        AVFrame *m_out;
    }l;

    if (!this->m_in || !this->m_out)
        return false;

    av_frame_copy_props(l.m_in, &in);

    l.m_in->color_trc = AVCOL_TRC_BT2020_10;
    l.m_in->width  = this->m_ctx->width;
    l.m_in->height = this->m_ctx->height;
    l.m_in->format = this->m_ctx->pix_fmt;

    memcpy(l.m_in->data, in.data, sizeof(l.m_in->data));
    memcpy(l.m_in->linesize, in.linesize, sizeof(l.m_in->linesize));

    if (av_buffersrc_add_frame_flags(this->m_in, l.m_in, AV_BUFFERSRC_FLAG_KEEP_REF) < 0)
        return false;

    if (av_buffersink_get_frame(this->m_out, l.m_out) < 0)
        return false;

    AVPixelFormat outFormat = (AVPixelFormat)l.m_out->format;

    av_image_alloc(out->data, out->linesize, this->m_ctx->width, this->m_ctx->height, outFormat, 1);
    av_image_copy(out->data, out->linesize, (const uint8_t**)l.m_out->data, l.m_out->linesize, outFormat, this->m_ctx->width, this->m_ctx->height);

    *retFormat = outFormat;

    return true;
}
