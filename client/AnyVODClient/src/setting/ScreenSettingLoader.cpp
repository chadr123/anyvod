﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "ScreenSettingLoader.h"
#include "Settings.h"
#include "ui/Screen.h"

#include <QSettings>

ScreenSettingLoader::ScreenSettingLoader(Screen *screen) :
    ScreenSettingBase(screen)
{

}

void ScreenSettingLoader::load() const
{
    bool screenVisible = this->m_settings.value(SETTING_SCREEN_VISIBLE, this->m_screen->getLastVisible()).toBool();
    this->m_screen->setVisible(screenVisible);
    this->m_screen->setLastVisible(screenVisible);

    QSize screenSize = this->m_screen->size();

    if (screenVisible)
    {
        if (screenSize.height() <= 0)
            screenSize.setHeight(Screen::DEFAULT_SCREEN_SIZE.height());

        if (screenSize.width() <= 0)
            screenSize.setWidth(Screen::DEFAULT_SCREEN_SIZE.width());
    }
    else
    {
        if (screenSize.height() <= Screen::DEFAULT_SCREEN_SIZE.height())
            screenSize.setHeight(Screen::DEFAULT_SCREEN_SIZE.height());

        if (screenSize.width() <= Screen::DEFAULT_SCREEN_SIZE.width())
            screenSize.setWidth(Screen::DEFAULT_SCREEN_SIZE.width());
    }

    this->m_screen->setLastSize(screenSize);
    this->m_screen->resize(screenSize);

    ScreenCaptureHelper &capHelper = this->m_screen->getCaptureHelper();
    capHelper.setSavePath(this->m_settings.value(SETTING_SCREEN_CAPTURE_DIR, capHelper.getSavePath()).toString());
    capHelper.setExtension(this->m_settings.value(SETTING_SCREEN_CAPTURE_EXT, capHelper.getExtention()).toString());

    bool useVSync = this->m_settings.value(SETTING_USE_VSYNC, this->m_screen->isUseVSync()).toBool();
    this->m_screen->setUseVSync(useVSync);
}
