﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "ScreenSettingSaver.h"
#include "setting/Settings.h"
#include "ui/Screen.h"

#include <QSettings>

ScreenSettingSaver::ScreenSettingSaver(Screen *screen) :
    ScreenSettingBase(screen)
{

}

void ScreenSettingSaver::save() const
{
    this->m_settings.setValue(SETTING_SCREEN_VISIBLE, this->m_screen->getLastVisible());

    ScreenCaptureHelper &capHelper = this->m_screen->getCaptureHelper();

    this->m_settings.setValue(SETTING_SCREEN_CAPTURE_DIR, capHelper.getSavePath());
    this->m_settings.setValue(SETTING_SCREEN_CAPTURE_EXT, capHelper.getExtention());

    this->m_settings.setValue(SETTING_USE_VSYNC, this->m_screen->isUseVSync());
}
