﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "MediaPlayerSettingLoader.h"
#include "Settings.h"
#include "media/MediaPlayer.h"
#include "ui/Equalizer.h"

#include <QSettings>

MediaPlayerSettingLoader::MediaPlayerSettingLoader(MediaPlayer *player) :
    MediaPlayerSettingBase(player)
{

}

void MediaPlayerSettingLoader::load() const
{
    uint8_t volume = this->m_settings.value(SETTING_VOLUME, this->m_player->getVolume()).toInt();
    this->m_player->volume(volume);

    bool showSubtitle = this->m_settings.value(SETTING_SHOW_SUBTITLE, this->m_player->isShowSubtitle()).toBool();
    this->m_player->showSubtitle(showSubtitle);

    bool useNormalizer = this->m_settings.value(SETTING_USE_NORMALIZER, this->m_player->isUsingNormalizer()).toBool();
    this->m_player->useNormalizer(useNormalizer);

    AnyVODEnums::DeinterlaceMethod deMethod = (AnyVODEnums::DeinterlaceMethod)this->m_settings.value(SETTING_DEINTERLACER_METHOD, this->m_player->getDeinterlaceMethod()).toInt();
    this->m_player->setDeinterlaceMethod(deMethod);

    AnyVODEnums::DeinterlaceAlgorithm deAlgorithm = (AnyVODEnums::DeinterlaceAlgorithm)this->m_settings.value(SETTING_DEINTERLACER_ALGORITHM, this->m_player->getDeinterlaceAlgorithm()).toInt();
    this->m_player->setDeinterlaceAlgorithm(deAlgorithm);

    AnyVODEnums::HAlignMethod halign = (AnyVODEnums::HAlignMethod)this->m_settings.value(SETTING_HALIGN_SUBTITLE, this->m_player->getHAlign()).toInt();
    this->m_player->setHAlign(halign);

    bool seekKeyFrame = this->m_settings.value(SETTING_SEEK_KEYFRAME, this->m_player->isSeekKeyFrame()).toBool();
    this->m_player->setSeekKeyFrame(seekKeyFrame);

    bool searchSubtitle = this->m_settings.value(SETTING_ENABLE_SEARCH_SUBTITLE, this->m_player->isEnableSearchSubtitle()).toBool();
    this->m_player->enableSearchSubtitle(searchSubtitle);

    bool searchLyrics = this->m_settings.value(SETTING_ENABLE_SEARCH_LYRICS, this->m_player->isEnableSearchLyrics()).toBool();
    this->m_player->enableSearchLyrics(searchLyrics);

    AnyVODEnums::VAlignMethod valign = (AnyVODEnums::VAlignMethod)this->m_settings.value(SETTING_VALIGN_SUBTITLE, this->m_player->getVAlign()).toInt();
    this->m_player->setVAlign(valign);

    float subtitleSize = (float)this->m_settings.value(SETTING_SUBTITLE_SIZE, this->m_player->getSubtitleSize()).toFloat();
    this->m_player->setSubtitleSize(subtitleSize);

    bool useHWDecoder = this->m_settings.value(SETTING_USE_HW_DECODER, this->m_player->isUseHWDecoder()).toBool();
    this->m_player->useHWDecoder(useHWDecoder, true);

    bool usePBO = this->m_settings.value(SETTING_USE_PBO, this->m_player->isUsePBO()).toBool();
    this->m_player->usePBO(usePBO);

    bool showAlbumJacket = this->m_settings.value(SETTING_SHOW_ALBUM_JACKET, this->m_player->isShowAlbumJacket()).toBool();
    this->m_player->showAlbumJacket(showAlbumJacket);

    bool enableLastPlay = this->m_settings.value(SETTING_ENABLE_LAST_PLAY, this->m_player->isGotoLastPos()).toBool();
    this->m_player->enableGotoLastPos(enableLastPlay, false);

    bool useFrameDrop = this->m_settings.value(SETTING_USE_FRAME_DROP, this->m_player->isUseFrameDrop()).toBool();
    this->m_player->useFrameDrop(useFrameDrop);

    QStringList subtitleDirectory;
    bool priorSubtitleDirectory;

    this->m_player->getSubtitleDirectory(&subtitleDirectory, &priorSubtitleDirectory);

    subtitleDirectory = this->m_settings.value(SETTING_SUBTITLE_DIRECTORY, subtitleDirectory).toStringList();
    priorSubtitleDirectory = this->m_settings.value(SETTING_PRIOR_SUBTITLE_DIRECTORY, priorSubtitleDirectory).toBool();

    this->m_player->setSubtitleDirectory(subtitleDirectory, priorSubtitleDirectory);

    bool searchSubtitleComplex = this->m_settings.value(SETTING_SEARCH_SUBTITLE_COMPLEX, this->m_player->getSearchSubtitleComplex()).toBool();
    this->m_player->setSearchSubtitleComplex(searchSubtitleComplex);

    int spdifDevice = this->m_settings.value(SETTING_SPDIF_DEVICE, this->m_player->getCurrentSPDIFAudioDevice()).toInt();
    this->m_player->setSPDIFAudioDevice(spdifDevice);

    bool useBufferingMode = this->m_settings.value(SETTING_USE_BUFFERING_MODE, this->m_player->isUseBufferingMode()).toBool();
    this->m_player->useBufferingMode(useBufferingMode);

    bool useDistortion = this->m_settings.value(SETTING_VR_USE_DISTORTION, this->m_player->isUseDistortion()).toBool();
    this->m_player->useDistortion(useDistortion);

    QVector2D barrelCoefficients(this->m_settings.value(SETTING_VR_BARREL_DISTORTION_COEFFICIENTS, this->m_player->getBarrelDistortionCoefficients().toPointF()).toPointF());
    this->m_player->setBarrelDistortionCoefficients(barrelCoefficients);

    QVector2D pincushionCoefficients(this->m_settings.value(SETTING_VR_PINCUSHION_DISTORTION_COEFFICIENTS, this->m_player->getPincushionDistortionCoefficients().toPointF()).toPointF());
    this->m_player->setPincushionDistortionCoefficients(pincushionCoefficients);

    QVector2D lensCenter(this->m_settings.value(SETTING_VR_DISTORTION_LENS_CENTER, this->m_player->getDistortionLensCenter().toPointF()).toPointF());
    this->m_player->setDistortionLensCenter(lensCenter);

    bool useGPUConvert = this->m_settings.value(SETTING_USE_GPU_CONVERT, this->m_player->isUseGPUConvert()).toBool();
    this->m_player->useGPUConvert(useGPUConvert);

    bool autoSaveSearchLyrics = this->m_settings.value(SETTING_AUTO_SAVE_SEARCH_LYRICS, this->m_player->isAutoSaveSearchLyrics()).toBool();
    this->m_player->enableAutoSaveSearchLyrics(autoSaveSearchLyrics);

    bool useHDR = this->m_settings.value(SETTING_USE_HDR, this->m_player->isUseHDR()).toBool();
    this->m_player->useHDR(useHDR);

    int hwDecoder = this->m_settings.value(SETTING_HW_DECODER, this->m_player->getHWDecoderIndex()).toInt();
    this->m_player->setHWDecoderIndex(hwDecoder);

    bool useIVTC = this->m_settings.value(SETTING_USE_IVTC, this->m_player->isUseDeinterlaceIVTC()).toBool();
    this->m_player->useDeinterlaceIVTC(useIVTC);

    bool useAutoColorConversion = this->m_settings.value(SETTING_USE_AUTO_COLOR_CONVERSION, this->m_player->isUsingAutoColorConversion()).toBool();
    this->m_player->useAutoColorConversion(useAutoColorConversion);

    AnyVODEnums::PlayingMethod method = (AnyVODEnums::PlayingMethod)this->m_settings.value(SETTING_PLAYING_METHOD, (int)this->m_player->getPlayingMethod()).toInt();
    this->m_player->setPlayingMethod(method);

    this->loadEqualizer();
    this->loadSkipRange();
}

void MediaPlayerSettingLoader::loadEqualizer() const
{
    bool useEqualizer = this->m_settings.value(SETTING_USE_EQUALIZER, this->m_player->isUsingEqualizer()).toBool();
    this->m_player->useEqualizer(useEqualizer);

    float preamp = this->m_settings.value(SETTING_EQUALIZER_PREAMP, this->m_player->getPreAmp()).toFloat();
    this->m_player->setPreAmp(preamp);

    QVariantList list = this->m_settings.value(SETTING_EQUALIZER_GAINS, QVariantList()).toList();

    for (int i = 0; i < list.count(); i++)
        this->m_player->setEqualizerGain(i, list[i].value<Equalizer::EqualizerItem>().gain);
}

void MediaPlayerSettingLoader::loadSkipRange() const
{
    bool skipOpening = this->m_settings.value(SETTING_SKIP_OPENING, this->m_player->getSkipOpening()).toBool();
    this->m_player->setSkipOpening(skipOpening);

    bool skipEnding = this->m_settings.value(SETTING_SKIP_ENDING, this->m_player->getSkipEnding()).toBool();
    this->m_player->setSkipEnding(skipEnding);

    bool useSkipRange = this->m_settings.value(SETTING_SKIP_USING, this->m_player->getUseSkipRange()).toBool();
    this->m_player->setUseSkipRange(useSkipRange);

    QVariantList list = this->m_settings.value(SETTING_SKIP_RANGE, QVariantList()).toList();
    QVector<MediaPresenter::Range> ranges;

    for (int i = 0; i < list.count(); i++)
        ranges.append(list[i].value<MediaPresenter::Range>());

    this->m_player->setSkipRanges(ranges);
}
