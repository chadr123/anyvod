﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "SettingBackupAndRestore.h"
#include "SettingsSynchronizer.h"
#include "SettingsFactory.h"
#include "Settings.h"

#include <QSettings>
#include <QFileInfo>
#include <QDataStream>

const QString SettingBackupAndRestore::BACKUP_SETTING_POSTFIX = "_temp";
const QString SettingBackupAndRestore::BACKUP_LIST_FILE_NAME = "backuplist.ini";

SettingBackupAndRestore::SettingBackupAndRestore()
{

}

bool SettingBackupAndRestore::saveSettings(const QString &to)
{
    QFile file(to);

    if (!file.open(QFile::WriteOnly))
        return false;

    SettingsSynchronizer::sync();

    QDataStream out(&file);
    QVariantHash hash;
    QStringList list = SettingBackupAndRestore::getSettingFiles();

    for (const QString &path : qAsConst(list))
    {
        QString fileName = QFileInfo(path).fileName();
        QByteArray data = SettingBackupAndRestore::readFile(path);

        if (!data.isEmpty())
            hash[fileName] = data;
    }

    out << hash;

    return true;
}

QByteArray SettingBackupAndRestore::readFile(const QString &path)
{
    QFile f(path);

    if (!f.open(QFile::ReadOnly))
        return QByteArray();

    return f.readAll();
}

bool SettingBackupAndRestore::loadSettings(const QString &from)
{
    QFile file(from);

    if (!file.open(QFile::ReadOnly))
        return false;

    QDataStream in(&file);
    QVariantHash hash;
    const QStringList list = SettingBackupAndRestore::getSettingFiles();
    QFile lists(QFileInfo(SettingsFactory::getSettingPath() + BACKUP_LIST_FILE_NAME).absoluteFilePath());

    if (!lists.open(QFile::WriteOnly))
        return false;

    QDataStream listsOut(&lists);

    in >> hash;
    listsOut << list;

    const auto keys = hash.keys();

    for (const QString &key : keys)
    {
        for (const QString &path : list)
        {
            if (key == QFileInfo(path).fileName())
            {
                if (!SettingBackupAndRestore::writeFile(hash[key].toByteArray(), path + BACKUP_SETTING_POSTFIX))
                    return false;
            }
        }
    }

    return true;
}

bool SettingBackupAndRestore::writeFile(const QByteArray &data, const QString &path)
{
    QFile f(path);

    if (!f.open(QFile::WriteOnly))
        return false;

    return f.write(data) == data.length();
}

bool SettingBackupAndRestore::restoreSettings()
{
    QFile lists(QFileInfo(SettingsFactory::getSettingPath() + BACKUP_LIST_FILE_NAME).absoluteFilePath());

    if (!lists.open(QFile::ReadOnly))
        return false;

    QDataStream listsIn(&lists);
    QStringList list;

    listsIn >> list;

    for (const QString &path : qAsConst(list))
    {
        if (!QFile::exists(path + BACKUP_SETTING_POSTFIX))
            continue;

#ifdef Q_OS_WIN
        if (!SettingBackupAndRestore::writeFile(SettingBackupAndRestore::readFile(path + BACKUP_SETTING_POSTFIX), path))
            return false;

        if (!QFile::remove(path + BACKUP_SETTING_POSTFIX))
            return false;
#else
        if (!QFile::remove(path))
            return false;

        if (!QFile::rename(path + BACKUP_SETTING_POSTFIX, path))
            return false;
#endif
    }

    return lists.remove();
}

QStringList SettingBackupAndRestore::getSettingFiles()
{
    QStringList list;

#ifdef Q_OS_MOBILE
    list.append(SettingsFactory::getSettingPath() + SETTING_FILE_NAME);
    list.append(SettingsFactory::getSettingPath() + SETTING_LAST_PLAY_FILE_NAME);
#else
    list.append(QFileInfo(ENV_FILENAME).absoluteFilePath());
    list.append(QFileInfo(SettingsFactory::getInstance(SettingsFactory::ST_GLOBAL).fileName()).absoluteFilePath());
    list.append(QFileInfo(SettingsFactory::getSettingPath() + SETTING_LAST_PLAY_FILE_NAME).absoluteFilePath());
    list.append(QFileInfo(SettingsFactory::getSettingPath() + SETTING_EQUALIZER_FILE_NAME).absoluteFilePath());
#endif

    return list;
}
