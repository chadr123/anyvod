﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "RadioReaderSettingLoader.h"
#include "Settings.h"
#include "device/RadioReader.h"

#include <QSettings>

RadioReaderSettingLoader::RadioReaderSettingLoader(RadioReader *reader) :
    RadioReaderSettingBase(reader)
{

}

void RadioReaderSettingLoader::load() const
{
    QVariantList list = this->m_settings.value(SETTING_RADIO_SCANNED_CHANNELS, QVariantList()).toList();
    QVector<RadioReader::ChannelInfo> channels;

    for (int i = 0; i < list.count(); i++)
        channels.append(list[i].value<RadioReader::ChannelInfo>());

    this->m_reader->setScannedChannels(channels);

    QLocale::Country country = (QLocale::Country)this->m_settings.value(SETTING_RADIO_CURRENT_COUNTRY, QLocale::SouthKorea).toInt();
    RadioReaderInterface::DemodulatorType type = (RadioReaderInterface::DemodulatorType)this->m_settings.value(SETTING_RADIO_CURRENT_TYPE, RadioReaderInterface::DT_WIDE_FM_MONO).toInt();

    this->m_reader->setChannelCountry(country, type);
}
