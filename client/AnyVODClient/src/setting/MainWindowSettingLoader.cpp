﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "MainWindowSettingLoader.h"
#include "Settings.h"
#include "ui/MainWindow.h"
#include "ui/Spectrum.h"
#include "ui/Slider.h"
#include "ui/ShortcutKey.h"
#include "ui/UserAspectRatios.h"
#include "parsers/subtitle/GlobalSubtitleCodecName.h"

#include <QSettings>
#include <QTextCodec>

MainWindowSettingLoader::MainWindowSettingLoader(MainWindowInterface *mainWindow) :
    MainWindowSettingBase(mainWindow)
{

}

void MainWindowSettingLoader::load() const
{
    QSize minSize = this->m_settings.value(SETTING_MAINWINDOW_MIN_SIZE, this->m_mainWindow->minimumSize()).toSize();
    this->m_mainWindow->setMinimumSize(minSize);

    QSize maxSize = this->m_settings.value(SETTING_MAINWINDOW_MAX_SIZE, this->m_mainWindow->maximumSize()).toSize();
    this->m_mainWindow->setMaximumSize(maxSize);

    QRect geometry = this->m_settings.value(SETTING_MAINWINDOW_GEOMETRY, QRect(MainWindow::DEFAULT_POSITION, MainWindow::DEFAULT_SIZE)).toRect();

    if (geometry.height() < MainWindow::CONTROL_BAR_HEIGHT)
        geometry.setHeight(MainWindow::CONTROL_BAR_HEIGHT);

    this->m_mainWindow->setGeometry(geometry);

    bool isMostTop = this->m_settings.value(SETTING_MAINWINDOW_MOSTTOP, false).toBool();

    if (isMostTop && ShortcutKey::getInstance().isShortcutKeyEnabled(AnyVODEnums::SK_MOST_TOP))
        this->m_mainWindow->mostTop();

    this->m_mainWindow->setLastSelectedDirectory(this->m_settings.value(SETTING_MAINWINDOW_LAST_DIR, QString()).toString());

    Spectrum::SpectrumType type = (Spectrum::SpectrumType)this->m_settings.value(SETTING_SPECTRUM_TYPE, (int)this->m_mainWindow->getSpectrum()->getType()).toInt();
    this->m_mainWindow->getSpectrum()->setType(type);

    qreal opaque = this->m_settings.value(SETTING_MAINWINDOW_OPAQUE, this->m_mainWindow->windowOpacity()).toReal();
    this->m_mainWindow->setWindowOpacity(opaque);
    this->m_mainWindow->getRemoteFileList().setWindowOpacity(opaque);
    this->m_mainWindow->getOpaqueSlider()->setValue(this->m_mainWindow->getOpaqueSlider()->maximum() * opaque);

    UserAspectRatios::Item item = this->m_mainWindow->getUserAspectRatios().getUserRatio();
    QSizeF defaultAspectRatio(item.width, item.height);
    QSizeF aspectRatio = this->m_settings.value(SETTING_USER_ASPECT_RATIO, defaultAspectRatio).toSizeF();
    item.width = aspectRatio.width();
    item.height = aspectRatio.height();
    this->m_mainWindow->getUserAspectRatios().setUserRatio(item);

    QString lang = this->m_settings.value(SETTING_LANGUAGE, QString()).toString();
    this->m_mainWindow->setLanguage(lang);

    GlobalSubtitleCodecName::getInstance().setCodecName(this->m_settings.value(SETTING_TEXT_ENCODING, QTextCodec::codecForLocale()->name()).toString());

    RemoteServerInformation info = this->m_mainWindow->getRemoteServerInformation();
    this->m_mainWindow->setRemoteServerInformation(RemoteServerInformation(this->m_settings.value(SETTING_ADDRESS, info.address).toString(),
                                                                           this->m_settings.value(SETTING_COMMAND_PORT, info.commandPort).toUInt(),
                                                                           this->m_settings.value(SETTING_STREAM_PORT, info.streamPort).toUInt()));
}
