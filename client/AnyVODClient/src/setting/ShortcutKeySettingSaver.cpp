﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "ShortcutKeySettingSaver.h"
#include "Settings.h"
#include "ui/ShortcutKey.h"

#include <QSettings>
#include <QShortcut>

ShortcutKeySettingSaver::ShortcutKeySettingSaver(ShortcutKey *shortcutKey) :
    ShortcutKeySettingBase(shortcutKey)
{

}

void ShortcutKeySettingSaver::save() const
{
    QVariantList list;

    for (int i = 0; i < AnyVODEnums::SK_COUNT; i++)
    {
        QString key;
        QShortcut *shortcut = this->m_shortcutKey->getKey((AnyVODEnums::ShortcutKey)i);

        if (shortcut)
            key = shortcut->key().toString();

        list.append(key);
    }

    this->m_settings.setValue(SETTING_SHORTCUTS, list);
}
