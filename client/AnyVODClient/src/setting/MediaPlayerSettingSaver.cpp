﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "MediaPlayerSettingSaver.h"
#include "Settings.h"
#include "media/MediaPlayer.h"
#include "ui/Equalizer.h"

#include <QSettings>

MediaPlayerSettingSaver::MediaPlayerSettingSaver(MediaPlayer *player) :
    MediaPlayerSettingBase(player)
{

}

void MediaPlayerSettingSaver::save() const
{
    this->m_settings.setValue(SETTING_VOLUME, this->m_player->getVolume());
    this->m_settings.setValue(SETTING_SHOW_SUBTITLE, this->m_player->isShowSubtitle());
    this->m_settings.setValue(SETTING_USE_NORMALIZER, this->m_player->isUsingNormalizer());
    this->m_settings.setValue(SETTING_DEINTERLACER_METHOD, (int)this->m_player->getDeinterlaceMethod());
    this->m_settings.setValue(SETTING_DEINTERLACER_ALGORITHM, (int)this->m_player->getDeinterlaceAlgorithm());
    this->m_settings.setValue(SETTING_HALIGN_SUBTITLE, (int)this->m_player->getHAlign());
    this->m_settings.setValue(SETTING_SEEK_KEYFRAME, this->m_player->isSeekKeyFrame());
    this->m_settings.setValue(SETTING_ENABLE_SEARCH_SUBTITLE, this->m_player->isEnableSearchSubtitle());
    this->m_settings.setValue(SETTING_ENABLE_SEARCH_LYRICS, this->m_player->isEnableSearchLyrics());
    this->m_settings.setValue(SETTING_VALIGN_SUBTITLE, (int)this->m_player->getVAlign());
    this->m_settings.setValue(SETTING_SUBTITLE_SIZE, this->m_player->getSubtitleSize());
    this->m_settings.setValue(SETTING_USE_HW_DECODER, this->m_player->isUseHWDecoder());
    this->m_settings.setValue(SETTING_USE_PBO, this->m_player->isUsePBO());
    this->m_settings.setValue(SETTING_SHOW_ALBUM_JACKET, this->m_player->isShowAlbumJacket());
    this->m_settings.setValue(SETTING_ENABLE_LAST_PLAY, this->m_player->isGotoLastPos());
    this->m_settings.setValue(SETTING_USE_FRAME_DROP, this->m_player->isUseFrameDrop());

    QStringList subtitleDirectory;
    bool priorSubtitleDirectory;

    this->m_player->getSubtitleDirectory(&subtitleDirectory, &priorSubtitleDirectory);
    this->m_settings.setValue(SETTING_SUBTITLE_DIRECTORY, subtitleDirectory);
    this->m_settings.setValue(SETTING_PRIOR_SUBTITLE_DIRECTORY, priorSubtitleDirectory);

    this->m_settings.setValue(SETTING_SEARCH_SUBTITLE_COMPLEX, this->m_player->getSearchSubtitleComplex());
    this->m_settings.setValue(SETTING_SPDIF_DEVICE, this->m_player->getCurrentSPDIFAudioDevice());
    this->m_settings.setValue(SETTING_USE_BUFFERING_MODE, this->m_player->isUseBufferingMode());

    this->m_settings.setValue(SETTING_VR_USE_DISTORTION, this->m_player->isUseDistortion());
    this->m_settings.setValue(SETTING_VR_BARREL_DISTORTION_COEFFICIENTS, this->m_player->getBarrelDistortionCoefficients().toPointF());
    this->m_settings.setValue(SETTING_VR_PINCUSHION_DISTORTION_COEFFICIENTS, this->m_player->getPincushionDistortionCoefficients().toPointF());
    this->m_settings.setValue(SETTING_VR_DISTORTION_LENS_CENTER, this->m_player->getDistortionLensCenter().toPointF());

    this->m_settings.setValue(SETTING_AUTO_SAVE_SEARCH_LYRICS, this->m_player->isAutoSaveSearchLyrics());

    this->m_settings.setValue(SETTING_USE_GPU_CONVERT, this->m_player->isUseGPUConvert());
    this->m_settings.setValue(SETTING_USE_HDR, this->m_player->isUseHDR());

    this->m_settings.setValue(SETTING_HW_DECODER, this->m_player->getHWDecoderIndex());
    this->m_settings.setValue(SETTING_USE_IVTC, this->m_player->isUseDeinterlaceIVTC());
    this->m_settings.setValue(SETTING_USE_AUTO_COLOR_CONVERSION, this->m_player->isUsingAutoColorConversion());
    this->m_settings.setValue(SETTING_PLAYING_METHOD, (int)this->m_player->getPlayingMethod());

    this->saveEqualizer();
    this->saveSkipRange();
}

void MediaPlayerSettingSaver::saveEqualizer() const
{
    this->m_settings.setValue(SETTING_USE_EQUALIZER, this->m_player->isUsingEqualizer());
    this->m_settings.setValue(SETTING_EQUALIZER_PREAMP, this->m_player->getPreAmp());

    QVariantList list;
    QVector<Equalizer::EqualizerItem> eqGains;

    for (int i = 0; i < this->m_player->getBandCount(); i++)
    {
        Equalizer::EqualizerItem item;

        item.gain = this->m_player->getEqualizerGain(i);

        eqGains.append(item);
    }

    for (int i = 0; i < eqGains.count(); i++)
        list.append(QVariant::fromValue(eqGains[i]));

    this->m_settings.setValue(SETTING_EQUALIZER_GAINS, list);
}

void MediaPlayerSettingSaver::saveSkipRange() const
{
    this->m_settings.setValue(SETTING_SKIP_OPENING, this->m_player->getSkipOpening());
    this->m_settings.setValue(SETTING_SKIP_ENDING, this->m_player->getSkipEnding());
    this->m_settings.setValue(SETTING_SKIP_USING, this->m_player->getUseSkipRange());

    QVariantList list;
    QVector<MediaPresenter::Range> ranges;

    this->m_player->getSkipRanges(&ranges);

    for (int i = 0; i < ranges.count(); i++)
        list.append(QVariant::fromValue(ranges[i]));

    this->m_settings.setValue(SETTING_SKIP_RANGE, list);
}
