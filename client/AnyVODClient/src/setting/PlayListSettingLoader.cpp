﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "PlayListSettingLoader.h"
#include "Settings.h"
#include "ui/PlayList.h"

#include <QSettings>

PlayListSettingLoader::PlayListSettingLoader(PlayList *playList) :
    PlayListSettingBase(playList)
{

}

void PlayListSettingLoader::load() const
{
    QVariantList list = this->m_settings.value(SETTING_PLAYLIST, QVariantList()).toList();
    QVector<PlayItem> playList;

    for (int i = 0; i < list.count(); i++)
        playList.append(list[i].value<PlayItem>());

    this->m_playList->setPlayListWithoutParentUpdate(playList);

    bool show = this->m_settings.value(SETTING_PLAYLIST_SHOW, this->m_playList->isVisible()).toBool();

#ifdef Q_OS_WIN
    this->m_playList->setVisible(true); // workaround for weird screen
#endif
    this->m_playList->setVisible(show);

    QRect geometry = this->m_settings.value(SETTING_PLAYLIST_GEOMETRY, this->m_playList->geometry()).toRect();
    this->m_playList->setGeometry(geometry);

    int playing = this->m_settings.value(SETTING_CURRENT_PLAYING, -1).toInt();
    this->m_playList->selectItem(playing);
    this->m_playList->setCurrentPlayingIndex(playing);

    this->m_playList->setClearPlayListOnExit(this->m_settings.value(SETTING_CLEAR_PLAYLIST, this->m_playList->isClearPlayListOnExit()).toBool());

    qreal opaque = this->m_settings.value(SETTING_MAINWINDOW_OPAQUE, this->m_playList->windowOpacity()).toReal();
    this->m_playList->setWindowOpacity(opaque);
}
