﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "PlayListSettingSaver.h"
#include "Settings.h"
#include "ui/PlayList.h"

#include <QSettings>

PlayListSettingSaver::PlayListSettingSaver(PlayList *playList) :
    PlayListSettingBase(playList)
{

}

void PlayListSettingSaver::save() const
{
    QVariantList list;
    QVector<PlayItem> playList;

    this->m_playList->getPlayList(&playList);

    for (int i = 0; i < playList.count(); i++)
        list.append(QVariant::fromValue(playList[i]));

    this->m_settings.setValue(SETTING_PLAYLIST, list);
    this->m_settings.setValue(SETTING_PLAYLIST_SHOW, this->m_playList->isVisible());
    this->m_settings.setValue(SETTING_PLAYLIST_GEOMETRY, this->m_playList->geometry());
    this->m_settings.setValue(SETTING_CLEAR_PLAYLIST, this->m_playList->isClearPlayListOnExit());
    this->m_settings.setValue(SETTING_CURRENT_PLAYING, this->m_playList->getCurrentPlayingIndex());
}
