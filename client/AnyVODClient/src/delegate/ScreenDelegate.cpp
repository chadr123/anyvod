﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "ScreenDelegate.h"
#include "core/FileExtensions.h"
#include "ui/Screen.h"
#include "ui/MultipleCapture.h"
#include "utils/MessageBoxUtils.h"
#include "utils/PathUtils.h"

#include <QFileDialog>
#include <QApplication>
#include <QDesktopServices>

ScreenDelegate::ScreenDelegate() :
    m_screen(Screen::getInstance())
{

}

ScreenDelegate& ScreenDelegate::getInstance()
{
    static ScreenDelegate instance;

    return instance;
}

void ScreenDelegate::useHDR()
{
    if (!this->m_screen->isHDRSupported())
        return;

    this->m_player.useHDR(!this->m_player.isUseHDR());
    MessageBoxUtils::informationMessageBox(QApplication::activeWindow(), tr("HDR 사용 상태를 변경 후 적용 하려면 프로그램을 재 시작 해야 합니다."));
}

void ScreenDelegate::selectCaptureSaveDir()
{
    ScreenCaptureHelper &capHelper = this->m_screen->getCaptureHelper();
    QString path = QFileDialog::getExistingDirectory(QApplication::activeWindow(), QString(), capHelper.getSavePath());

    if (!path.isEmpty())
    {
        QString prevPath = capHelper.getSavePath();

        capHelper.setSavePath(path);

        if (prevPath != path)
            this->m_player.showOptionDesc(tr("캡쳐 저장 경로 변경 : %1").arg(path));
    }
}

void ScreenDelegate::openCaptureSaveDir()
{
    QString path = this->m_screen->getCaptureHelper().getSavePath();

    PathUtils::createDirectory(path);

    if (!QDesktopServices::openUrl(QUrl("file:///" + path)))
        QDesktopServices::openUrl(QUrl(QDir::toNativeSeparators("./")));
}

void ScreenDelegate::useVSync()
{
    this->m_screen->toggleUseVSync();
}

void ScreenDelegate::captureExtOrder()
{
    ScreenCaptureHelper &capHelper = this->m_screen->getCaptureHelper();
    int index = 0;

    for (; index < FileExtensions::CAPTURE_FORMAT_LIST.count(); index++)
    {
        if (FileExtensions::CAPTURE_FORMAT_LIST.at(index).toLower() == capHelper.getExtention())
            break;
    }

    index++;

    if (index >= FileExtensions::CAPTURE_FORMAT_LIST.count())
        index = 0;

    this->selectCaptureExt(FileExtensions::CAPTURE_FORMAT_LIST.at(index));
}

void ScreenDelegate::captureSingle()
{
    if (this->m_player.canCapture())
    {
        ScreenCaptureHelper &capHelper = this->m_screen->getCaptureHelper();

        capHelper.setCaptureByOriginalSize(true);
        capHelper.setTotalCount(1);
        capHelper.start();
    }
}

void ScreenDelegate::captureMultiple()
{
    if (this->m_player.canCapture())
    {
        MultipleCapture dlg(this->m_screen, QApplication::activeWindow());
        bool showingSubtitle = this->m_player.isShowSubtitle();
        bool showingDetail = this->m_player.isShowDetail();

        dlg.exec();

        this->m_player.showSubtitle(showingSubtitle, true);
        this->m_player.showDetail(showingDetail);

        this->m_screen->getCaptureHelper().stop();
    }
}

void ScreenDelegate::selectCaptureExt(const QString &ext)
{
    ScreenCaptureHelper &capHelper = this->m_screen->getCaptureHelper();
    QString prevExt = capHelper.getExtention();

    capHelper.setExtension(ext.toLower());

    if (prevExt != ext)
        this->m_player.showOptionDesc(tr("캡쳐 확장자 변경 : %1").arg(ext.toUpper()));
}
