﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "MediaPlayerDelegate.h"
#include "media/MediaPlayer.h"
#include "media/AudioRenderer.h"
#include "core/EnumsTranslator.h"
#include "utils/ChapterInfoUtils.h"
#include "utils/MessageBoxUtils.h"
#include "utils/FloatPointUtils.h"
#include "factories/MainWindowFactory.h"
#include "ui/MainWindowInterface.h"
#include "audio/SPDIFSampleRates.h"

#include <QFileInfo>
#include <QFileDialog>
#include <QApplication>

MediaPlayerDelegate::MediaPlayerDelegate() :
    m_mainWindow(MainWindowFactory::getInstance()),
    m_spdifSampleRates(SPDIFSampleRates::getInstance())
{

}

MediaPlayerDelegate& MediaPlayerDelegate::getInstance()
{
    static MediaPlayerDelegate instance;

    return instance;
}

void MediaPlayerDelegate::selectAudioDevice(int device)
{
    this->m_player.setAudioDevice(device);
}

void MediaPlayerDelegate::audioDeviceOrder()
{
    QStringList list = AudioRenderer::getDevices();
    int curDevice = this->m_player.getCurrentAudioDevice() + 1;

    if (curDevice >= list.count())
        curDevice = -1;

    this->selectAudioDevice(curDevice);
}

void MediaPlayerDelegate::selectSPDIFAudioDevice(int device)
{
    this->m_player.setSPDIFAudioDevice(device);
    this->m_mainWindow->adjustSPDIFAudioUI();
}

void MediaPlayerDelegate::audioSPDIFDeviceOrder()
{
    QStringList list;
    int curDevice = this->m_player.getCurrentSPDIFAudioDevice() + 1;

    this->m_player.getSPDIFAudioDevices(&list);

    if (curDevice >= list.count())
        curDevice = -1;

    this->selectSPDIFAudioDevice(curDevice);
}

void MediaPlayerDelegate::useFrameDrop()
{
    this->m_player.useFrameDrop(!this->m_player.isUseFrameDrop());
}

void MediaPlayerDelegate::useSPDIFEncodingOrder()
{
    AnyVODEnums::SPDIFEncodingMethod method = this->m_player.getSPDIFEncodingMethod();

    method = (AnyVODEnums::SPDIFEncodingMethod)(method + 1);

    if (method >= AnyVODEnums::SEM_COUNT)
        method = AnyVODEnums::SEM_NONE;

    this->selectSPDIFEncodingMethod(method);
}

void MediaPlayerDelegate::selectSPDIFEncodingMethod(int method)
{
    this->m_player.setSPDIFEncodingMethod((AnyVODEnums::SPDIFEncodingMethod)method);
    this->m_mainWindow->adjustSPDIFAudioUI();
}

void MediaPlayerDelegate::method3DSubtitleOrder()
{
    AnyVODEnums::Subtitle3DMethod method = this->m_player.getSubtitle3DMethod();

    method = (AnyVODEnums::Subtitle3DMethod)(method + 1);

    if (method >= AnyVODEnums::S3M_COUNT)
        method = AnyVODEnums::S3M_NONE;

    this->m_player.setSubtitle3DMethod(method);
}

void MediaPlayerDelegate::select3DSubtitleMethod(int method)
{
    this->m_player.setSubtitle3DMethod((AnyVODEnums::Subtitle3DMethod)method);
}

void MediaPlayerDelegate::use3DFull()
{
    if (this->m_player.get3DMethod() != AnyVODEnums::V3M_NONE)
        this->m_player.use3DFull(!this->m_player.isUse3DFull());
}


void MediaPlayerDelegate::reset3DSubtitleOffset()
{
    if (this->m_player.is3DSubtitleMoveable())
        this->m_player.reset3DSubtitleOffset();
}

void MediaPlayerDelegate::up3DSubtitleOffset()
{
    if (this->m_player.is3DSubtitleMoveable())
        this->m_player.setVertical3DSubtitleOffset(1);
}

void MediaPlayerDelegate::down3DSubtitleOffset()
{
    if (this->m_player.is3DSubtitleMoveable())
        this->m_player.setVertical3DSubtitleOffset(-1);
}

void MediaPlayerDelegate::left3DSubtitleOffset()
{
    if (this->m_player.is3DSubtitleMoveable())
        this->m_player.setHorizontal3DSubtitleOffset(1);
}

void MediaPlayerDelegate::right3DSubtitleOffset()
{
    if (this->m_player.is3DSubtitleMoveable())
        this->m_player.setHorizontal3DSubtitleOffset(-1);
}

void MediaPlayerDelegate::screenRotationDegreeOrder()
{
    AnyVODEnums::ScreenRotationDegree degree = this->m_player.getScreenRotationDegree();

    degree = (AnyVODEnums::ScreenRotationDegree)(degree + 1);

    if (degree >= AnyVODEnums::SRD_COUNT)
        degree = AnyVODEnums::SRD_NONE;

    this->selectScreenRotationDegree(degree);
}

void MediaPlayerDelegate::selectScreenRotationDegree(int degree)
{
    this->m_player.setScreenRotationDegree((AnyVODEnums::ScreenRotationDegree)degree);
}

void MediaPlayerDelegate::histEQ()
{
    if (this->m_player.isEnabledVideo())
    {
        FilterGraph &graph = this->m_player.getFilterGraph();
        bool use = !graph.getEnableHistEQ();

        graph.setEnableHistEQ(use);

        QString desc;

        if (use)
            desc = tr("히스토그램 이퀄라이저 켜짐");
        else
            desc = tr("히스토그램 이퀄라이저 꺼짐");

        this->m_player.showOptionDesc(desc);
        this->m_player.configFilterGraph();
    }
}

void MediaPlayerDelegate::hq3DDenoise()
{
    if (this->m_player.isEnabledVideo())
    {
        FilterGraph &graph = this->m_player.getFilterGraph();
        bool use = !graph.getEnableHighQuality3DDenoise();

        graph.setEnableHighQuality3DDenoise(use);

        QString desc;

        if (use)
            desc = tr("3D 노이즈 제거 켜짐");
        else
            desc = tr("3D 노이즈 제거 꺼짐");

        this->m_player.showOptionDesc(desc);
        this->m_player.configFilterGraph();
    }
}

void MediaPlayerDelegate::ataDenoise()
{
    if (this->m_player.isEnabledVideo())
    {
        FilterGraph &graph = this->m_player.getFilterGraph();
        bool use = !graph.getEnableATADenoise();

        graph.setEnableATADenoise(use);

        QString desc;

        if (use)
            desc = tr("적응 시간 평균 노이즈 제거 켜짐");
        else
            desc = tr("적응 시간 평균 노이즈 제거 꺼짐");

        this->m_player.showOptionDesc(desc);
        this->m_player.configFilterGraph();
    }
}

void MediaPlayerDelegate::owDenoise()
{
    if (this->m_player.isEnabledVideo())
    {
        FilterGraph &graph = this->m_player.getFilterGraph();
        bool use = !graph.getEnableOWDenoise();

        graph.setEnableOWDenoise(use);

        QString desc;

        if (use)
            desc = tr("Overcomplete Wavelet 노이즈 제거 켜짐");
        else
            desc = tr("Overcomplete Wavelet 노이즈 제거 꺼짐");

        this->m_player.showOptionDesc(desc);
        this->m_player.configFilterGraph();
    }
}

void MediaPlayerDelegate::deband()
{
    if (this->m_player.isEnabledVideo())
    {
        FilterGraph &graph = this->m_player.getFilterGraph();
        bool use = !graph.getEnableDeBand();

        graph.setEnableDeBand(use);

        QString desc;

        if (use)
            desc = tr("디밴드 켜짐");
        else
            desc = tr("디밴드 꺼짐");

        this->m_player.showOptionDesc(desc);
        this->m_player.configFilterGraph();
    }
}

void MediaPlayerDelegate::deblock()
{
    if (this->m_player.isEnabledVideo())
    {
        FilterGraph &graph = this->m_player.getFilterGraph();
        bool use = !graph.getEnableDeBlock();

        graph.setEnableDeBlock(use);

        QString desc;

        if (use)
            desc = tr("디블록 켜짐");
        else
            desc = tr("디블록 꺼짐");

        this->m_player.showOptionDesc(desc);
        this->m_player.configFilterGraph();
    }
}

void MediaPlayerDelegate::useSearchSubtitleComplex()
{
    this->m_player.setSearchSubtitleComplex(!this->m_player.getSearchSubtitleComplex());
}

void MediaPlayerDelegate::useBufferingMode()
{
    this->m_player.useBufferingMode(!this->m_player.isUseBufferingMode());
}

void MediaPlayerDelegate::vrInputSourceOrder()
{
    AnyVODEnums::VRInputSource source = this->m_player.getVRInputSource();

    source = (AnyVODEnums::VRInputSource)(source + 1);

    if (source >= AnyVODEnums::VRI_COUNT)
        source = AnyVODEnums::VRI_NONE;

    this->selectVRInputSource(source);
}

void MediaPlayerDelegate::selectVRInputSource(int source)
{
    QString sourceDesc = this->m_enums.getVRInputSourceDesc((AnyVODEnums::VRInputSource)source);

    this->m_player.setVRInputSource((AnyVODEnums::VRInputSource)source);
    this->m_player.showOptionDesc(tr("VR 입력 영상 (%1)").arg(sourceDesc));
}

void MediaPlayerDelegate::upLensCenterX()
{
    QVector2D lensCenter = this->m_player.getDistortionLensCenter();

    lensCenter.setX(lensCenter.x() + 0.001f);
    this->selectLensCenter(lensCenter);
}

void MediaPlayerDelegate::downLensCenterX()
{
    QVector2D lensCenter = this->m_player.getDistortionLensCenter();

    lensCenter.setX(lensCenter.x() - 0.001f);
    this->selectLensCenter(lensCenter);
}

void MediaPlayerDelegate::upLensCenterY()
{
    QVector2D lensCenter = this->m_player.getDistortionLensCenter();

    lensCenter.setY(lensCenter.y() + 0.001f);
    this->selectLensCenter(lensCenter);
}

void MediaPlayerDelegate::downLensCenterY()
{
    QVector2D lensCenter = this->m_player.getDistortionLensCenter();

    lensCenter.setY(lensCenter.y() - 0.001f);
    this->selectLensCenter(lensCenter);
}

void MediaPlayerDelegate::selectLensCenter(const QVector2D &lensCenter)
{
    QString desc = tr("VR 렌즈 센터 설정 (X : %1, Y : %2)");
    QVector2D center = lensCenter;

    center.setX(FloatPointUtils::zeroDouble(center.x()));
    center.setY(FloatPointUtils::zeroDouble(center.y()));

    if (center.x() > 1.0f)
        center.setX(1.0f);
    else if (center.x() < -1.0f)
        center.setX(-1.0f);

    if (center.y() > 1.0f)
        center.setY(1.0f);
    else if (center.y() < -1.0f)
        center.setY(-1.0f);

    this->m_player.setDistortionLensCenter(center);

    desc = desc.arg(center.x(), 0, 'f', 3).arg(center.y(), 0, 'f', 3);

    this->m_player.showOptionDesc(desc);
}

void MediaPlayerDelegate::upVirtual3DDepth()
{
    this->m_player.addVirtual3DDepth(0.01);
}

void MediaPlayerDelegate::downVirtual3DDepth()
{
    this->m_player.addVirtual3DDepth(-0.01);
}

void MediaPlayerDelegate::barrelUpK1()
{
    this->m_player.addBarrelK1(0.001f);
}

void MediaPlayerDelegate::barrelDownK1()
{
    this->m_player.addBarrelK1(-0.001f);
}

void MediaPlayerDelegate::barrelUpK2()
{
    this->m_player.addBarrelK2(0.001f);
}

void MediaPlayerDelegate::barrelDownK2()
{
    this->m_player.addBarrelK2(-0.001f);
}

void MediaPlayerDelegate::distortionAdjustModeOrder()
{
    AnyVODEnums::DistortionAdjustMode mode = this->m_player.getDistortionAdjustMode();

    mode = (AnyVODEnums::DistortionAdjustMode)(mode + 1);

    if (mode >= AnyVODEnums::DAM_COUNT)
        mode = AnyVODEnums::DAM_NONE;

    this->selectDistortionAdjustMode(mode);
}

void MediaPlayerDelegate::selectDistortionAdjustMode(int mode)
{
    QString desc;

    this->m_player.setDistortionAdjustMode((AnyVODEnums::DistortionAdjustMode)mode);

    switch (mode)
    {
        case AnyVODEnums::DAM_NONE:
            desc = tr("왜곡 보정 모드 사용 안 함");
            break;
        case AnyVODEnums::DAM_BARREL:
        case AnyVODEnums::DAM_PINCUSION:
            desc = tr("왜곡 보정 모드 사용 : %1").arg(this->m_enums.getDistortionAdjustModeDesc((AnyVODEnums::DistortionAdjustMode)mode));
            break;
        default:
            break;
    }

    this->m_player.showOptionDesc(desc);
}

void MediaPlayerDelegate::useDistortion()
{
    QString desc;

    this->m_player.useDistortion(!this->m_player.isUseDistortion());

    if (this->m_player.isUseDistortion())
        desc = tr("왜곡 보정 사용 함");
    else
        desc = tr("왜곡 보정 사용 안 함");

    this->m_player.showOptionDesc(desc);
}

void MediaPlayerDelegate::useGPUConvert()
{
    this->m_player.useGPUConvert(!this->m_player.isUseGPUConvert());
}

void MediaPlayerDelegate::autoSaveSearchLyrics()
{
    this->m_player.enableAutoSaveSearchLyrics(!this->m_player.isAutoSaveSearchLyrics());
}

void MediaPlayerDelegate::videoNormalize()
{
    if (this->m_player.isEnabledVideo())
    {
        FilterGraph &graph = this->m_player.getFilterGraph();
        bool use = !graph.getEnableNormalize();

        graph.setEnableNormalize(use);

        QString desc;

        if (use)
            desc = tr("노멀라이즈 켜짐");
        else
            desc = tr("노멀라이즈 꺼짐");

        this->m_player.showOptionDesc(desc);
        this->m_player.configFilterGraph();
    }
}

void MediaPlayerDelegate::nlMeansDenoise()
{
    if (this->m_player.isEnabledVideo())
    {
        FilterGraph &graph = this->m_player.getFilterGraph();
        bool use = !graph.getEnableNLMeansDenoise();

        graph.setEnableNLMeansDenoise(use);

        QString desc;

        if (use)
            desc = tr("Non-local means 노이즈 제거 켜짐");
        else
            desc = tr("Non-local means 노이즈 제거 꺼짐");

        this->m_player.showOptionDesc(desc);
        this->m_player.configFilterGraph();
    }
}

void MediaPlayerDelegate::vagueDenoise()
{
    if (this->m_player.isEnabledVideo())
    {
        FilterGraph &graph = this->m_player.getFilterGraph();
        bool use = !graph.getEnableVagueDenoise();

        graph.setEnableVagueDenoise(use);

        QString desc;

        if (use)
            desc = tr("Vague 노이즈 제거 켜짐");
        else
            desc = tr("Vague 노이즈 제거 꺼짐");

        this->m_player.showOptionDesc(desc);
        this->m_player.configFilterGraph();
    }
}

void MediaPlayerDelegate::hwDecoderOrder()
{
    int index = this->m_player.getHWDecoderIndex();

    index++;

    if (index >= this->m_player.getHWDecoderCount())
        index = 0;

    this->selectHWDecoder(index);
}

void MediaPlayerDelegate::selectHWDecoder(int index)
{
    this->m_player.setHWDecoderIndex(index);
}

void MediaPlayerDelegate::useIVTC()
{
    this->m_player.useDeinterlaceIVTC(!this->m_player.isUseDeinterlaceIVTC());
}

void MediaPlayerDelegate::useAutoColorConversion()
{
    this->m_player.useAutoColorConversion(!this->m_player.isUsingAutoColorConversion());
}

void MediaPlayerDelegate::enableSearchSubtitle()
{
    this->m_player.enableSearchSubtitle(!this->m_player.isEnableSearchSubtitle());

    if (this->m_player.isEnableSearchSubtitle())
    {
        this->m_player.retreiveExternalSubtitle(this->m_player.getFilePath());
        this->m_mainWindow->checkGOMSubtitle();
    }
}

void MediaPlayerDelegate::enableSearchLyrics()
{
    this->m_player.enableSearchLyrics(!this->m_player.isEnableSearchLyrics());

    if (this->m_player.isEnableSearchLyrics())
    {
        this->m_player.retreiveExternalSubtitle(this->m_player.getFilePath());

        if (this->m_player.existAudioSubtitle())
            this->m_mainWindow->resetFirstAudioSubtitle();
    }
}

void MediaPlayerDelegate::subtitleVAlignOrder()
{
    if (this->m_player.isAlignable())
    {
        AnyVODEnums::VAlignMethod align = this->m_player.getVAlign();

        align = (AnyVODEnums::VAlignMethod)(align + 1);

        if (align >= AnyVODEnums::VAM_COUNT)
            align = AnyVODEnums::VAM_NONE;

        this->m_player.setVAlign(align);
    }
}

void MediaPlayerDelegate::selectSubtitleVAlignMethod(int method)
{
    if (this->m_player.isAlignable())
        this->m_player.setVAlign((AnyVODEnums::VAlignMethod)method);
}

void MediaPlayerDelegate::incSubtitleSize()
{
    if (this->m_player.isAlignable())
        this->m_player.addSubtitleSize(0.01f);
}

void MediaPlayerDelegate::decSubtitleSize()
{
    if (this->m_player.isAlignable())
        this->m_player.addSubtitleSize(-0.01f);
}

void MediaPlayerDelegate::resetSubtitleSize()
{
    if (this->m_player.isAlignable())
        this->m_player.resetSubtitleSize();
}

void MediaPlayerDelegate::useHWDecoder()
{
    this->m_player.useHWDecoder(!this->m_player.isUseHWDecoder(), false);
}

void MediaPlayerDelegate::useSPDIF()
{
    this->m_player.useSPDIF(!this->m_player.isUseSPDIF());
    this->m_mainWindow->adjustSPDIFAudioUI();
}

void MediaPlayerDelegate::spdifSampleRateOrder()
{
    this->m_spdifSampleRates.selectNextSampleRate();
    this->selectSPDIFSampleRate(this->m_spdifSampleRates.getSelectedSampleRate());
}

void MediaPlayerDelegate::selectSPDIFSampleRate(int sampleRate)
{
    this->m_spdifSampleRates.selectSampleRate(sampleRate);
    this->m_player.resetSPDIF();

    this->m_mainWindow->adjustSPDIFAudioUI();
}

void MediaPlayerDelegate::usePBO()
{
    if (this->m_player.isUsablePBO())
        this->m_player.usePBO(!this->m_player.isUsePBO());
}

void MediaPlayerDelegate::method3DOrder()
{
    AnyVODEnums::Video3DMethod method = this->m_player.get3DMethod();

    method = (AnyVODEnums::Video3DMethod)(method + 1);

    if (method >= AnyVODEnums::V3M_COUNT)
        method = AnyVODEnums::V3M_NONE;

    this->m_player.set3DMethod(method);
    ShaderCompositer::getInstance().set3DMethod(method);
}

void MediaPlayerDelegate::prevChapter()
{
    if (this->m_player.canMoveChapter())
    {
        double pos = this->m_player.getCurrentPosition();
        QString desc;

        if (this->moveChapter(this->getChapterIndex(pos) - 1, &desc))
            this->m_player.showOptionDesc(tr("이전 챕터로 이동 (%1)").arg(desc));
    }
}

void MediaPlayerDelegate::nextChapter()
{
    if (this->m_player.canMoveChapter())
    {
        double pos = this->m_player.getCurrentPosition();
        QString desc;

        if (this->moveChapter(this->getChapterIndex(pos) + 1, &desc))
            this->m_player.showOptionDesc(tr("다음 챕터로 이동 (%1)").arg(desc));
    }
}

bool MediaPlayerDelegate::moveChapter(int index, QString *desc)
{
    const QVector<ChapterInfo> &list = this->m_player.getChapters();

    if (index < 0 || index >= list.count())
        return false;

    this->m_player.seek(list[index].start, true);
    *desc = list[index].desc;

    return true;
}

int MediaPlayerDelegate::getChapterIndex(double pos)
{
    return ChapterInfoUtils::findChapter(pos, this->m_player.getChapters());
}

void MediaPlayerDelegate::closeExternalSubtitle()
{
    if (this->m_player.existExternalSubtitle())
    {
        this->m_player.closeAllExternalSubtitles();
        this->m_mainWindow->resetAudioSubtitle();
    }
}

void MediaPlayerDelegate::showAlbumJacket()
{
    this->m_player.showAlbumJacket(!this->m_player.isShowAlbumJacket());
}

void MediaPlayerDelegate::lastPlay()
{
    this->m_player.enableGotoLastPos(!this->m_player.isGotoLastPos(), false);
}

void MediaPlayerDelegate::saveSubtitle()
{
    if (this->m_player.existFileSubtitle())
        this->m_player.saveSubtitle();
}

void MediaPlayerDelegate::saveAsSubtitle()
{
    if (this->m_player.existFileSubtitle())
    {
        QFileInfo info(this->m_player.getSubtitlePath());
        QString type = this->m_player.isMovieSubtitleVisiable() ? tr("자막") : tr("가사");
        QString path = QFileDialog::getSaveFileName(QApplication::activeWindow(), QString(), info.absoluteFilePath(), tr("%1 (*.%2);;모든 파일 (*.*)").arg(type, info.suffix()));

        if (!path.isEmpty())
            this->m_player.saveSubtitleAs(path);
    }
}

void MediaPlayerDelegate::select3DMethod(int method)
{
    this->m_player.set3DMethod((AnyVODEnums::Video3DMethod)method);
    ShaderCompositer::getInstance().set3DMethod((AnyVODEnums::Video3DMethod)method);
}

void MediaPlayerDelegate::audioNormalize()
{
    this->m_player.useNormalizer(!this->m_player.isUsingNormalizer());
}

void MediaPlayerDelegate::audioEqualizer()
{
    this->m_player.useEqualizer(!this->m_player.isUsingEqualizer());
}

void MediaPlayerDelegate::selectSubtitleClass(const QString &className)
{
    QString currentName;

    this->m_player.getCurrentSubtitleClass(&currentName);

    if (currentName != className)
    {
        if (!this->m_player.setCurrentSubtitleClass(className))
        {
            QString desc;

            if (this->m_player.isMovieSubtitleVisiable())
                desc = tr("자막을 변경 할 수 없습니다");
            else
                desc = tr("가사를 변경 할 수 없습니다");

            MessageBoxUtils::criticalMessageBox(QApplication::activeWindow(), desc);
        }
    }
}

void MediaPlayerDelegate::selectVideoStream(int index)
{
    if (this->m_player.getCurrentVideoStreamIndex() != index)
    {
        if (!this->m_player.changeVideoStream(index))
            MessageBoxUtils::criticalMessageBox(QApplication::activeWindow(), tr("영상을 변경 할 수 없습니다"));
    }
}

void MediaPlayerDelegate::selectAudioStream(int index)
{
    if (this->m_player.getCurrentAudioStreamIndex() != index)
    {
        if (!this->m_player.changeAudioStream(index))
            MessageBoxUtils::criticalMessageBox(QApplication::activeWindow(), tr("음성을 변경 할 수 없습니다"));
    }
}

void MediaPlayerDelegate::resetSubtitlePosition()
{
    if (this->m_player.isSubtitleMoveable())
        this->m_player.resetSubtitlePosition();
}

void MediaPlayerDelegate::upSubtitlePosition()
{
    if (this->m_player.isSubtitleMoveable())
        this->m_player.setVerticalSubtitlePosition(1);
}

void MediaPlayerDelegate::downSubtitlePosition()
{
    if (this->m_player.isSubtitleMoveable())
        this->m_player.setVerticalSubtitlePosition(-1);
}

void MediaPlayerDelegate::leftSubtitlePosition()
{
    if (this->m_player.isSubtitleMoveable())
        this->m_player.setHorizontalSubtitlePosition(1);
}

void MediaPlayerDelegate::rightSubtitlePosition()
{
    if (this->m_player.isSubtitleMoveable())
        this->m_player.setHorizontalSubtitlePosition(-1);
}

void MediaPlayerDelegate::rewind(double distance)
{
    if (this->m_player.hasDuration())
    {
        this->m_player.rewind(distance);

        this->m_mainWindow->updateTrackBar();
        this->m_mainWindow->setSeekingText();
    }
}

void MediaPlayerDelegate::forward(double distance)
{
    if (this->m_player.hasDuration())
    {
        this->m_player.forward(distance);

        this->m_mainWindow->updateTrackBar();
        this->m_mainWindow->setSeekingText();
    }
}

void MediaPlayerDelegate::rewind5()
{
    this->rewind(5.0);
}

void MediaPlayerDelegate::forward5()
{
    this->forward(5.0);
}

void MediaPlayerDelegate::rewind30()
{
    this->rewind(30.0);
}

void MediaPlayerDelegate::forward30()
{
    this->forward(30.0);
}

void MediaPlayerDelegate::rewind60()
{
    this->rewind(60.0);
}

void MediaPlayerDelegate::forward60()
{
    this->forward(60.0);
}

void MediaPlayerDelegate::gotoBegin()
{
    if (this->m_player.hasDuration())
    {
        this->m_player.seek(0.0, false);
        this->m_player.showOptionDesc(tr("처음으로 이동"));
    }
}

void MediaPlayerDelegate::subtitleToggle()
{
    this->m_player.showSubtitle(!this->m_player.isShowSubtitle());
}

void MediaPlayerDelegate::prevSubtitleSync()
{
    if (this->m_player.existSubtitle())
        this->m_player.prevSubtitleSync(0.5);
}

void MediaPlayerDelegate::nextSubtitleSync()
{
    if (this->m_player.existSubtitle())
        this->m_player.nextSubtitleSync(0.5);
}

void MediaPlayerDelegate::resetSubtitleSync()
{
    if (this->m_player.existSubtitle())
        this->m_player.resetSubtitleSync();
}

void MediaPlayerDelegate::subtitleLanguageOrder()
{
    QStringList classes;
    QString curClass;

    this->m_player.getSubtitleClasses(&classes);
    this->m_player.getCurrentSubtitleClass(&curClass);

    if (curClass.isEmpty() && classes.count() > 0)
    {
        this->selectSubtitleClass(classes[0]);
        return;
    }

    for (int i = 0; i < classes.count(); i++)
    {
        if (curClass == classes[i])
        {
            QString selClass;

            if (i + 1 >= classes.count())
                selClass = classes[0];
            else
                selClass = classes[i + 1];

            this->selectSubtitleClass(selClass);

            break;
        }
    }
}

void MediaPlayerDelegate::videoOrder()
{
    QVector<VideoStreamInfo> info;
    int index = this->m_player.getCurrentVideoStreamIndex();

    this->m_player.getVideoStreamInfo(&info);

    if (index < 0 && info.count() > 0)
    {
        this->selectVideoStream(info[0].index);
        return;
    }

    for (int i = 0; i < info.count(); i++)
    {
        if (index == info[i].index)
        {
            int selIndex;

            if (i + 1 >= info.count())
                selIndex = info[0].index;
            else
                selIndex = info[i + 1].index;

            this->selectVideoStream(selIndex);

            break;
        }
    }
}

void MediaPlayerDelegate::audioOrder()
{
    QVector<AudioStreamInfo> info;
    int index = this->m_player.getCurrentAudioStreamIndex();

    this->m_player.getAudioStreamInfo(&info);

    if (index < 0 && info.count() > 0)
    {
        this->selectAudioStream(info[0].index);
        return;
    }

    for (int i = 0; i < info.count(); i++)
    {
        if (index == info[i].index)
        {
            int selIndex;

            if (i + 1 >= info.count())
                selIndex = info[0].index;
            else
                selIndex = info[i + 1].index;

            this->selectAudioStream(selIndex);

            break;
        }
    }
}

void MediaPlayerDelegate::detail()
{
    if (this->m_player.isEnabledVideo())
        this->m_player.showDetail(!this->m_player.isShowDetail());
}

void MediaPlayerDelegate::deinterlaceMethodOrder()
{
    AnyVODEnums::DeinterlaceMethod method = this->m_player.getDeinterlaceMethod();

    method = (AnyVODEnums::DeinterlaceMethod)(method + 1);

    if (method >= AnyVODEnums::DM_COUNT)
        method = AnyVODEnums::DM_AUTO;

    this->m_player.setDeinterlaceMethod(method);
}

void MediaPlayerDelegate::deinterlaceAlgorithmOrder()
{
    AnyVODEnums::DeinterlaceAlgorithm algorithm = this->m_player.getDeinterlaceAlgorithm();

    algorithm = (AnyVODEnums::DeinterlaceAlgorithm)(algorithm + 1);

    if (algorithm >= AnyVODEnums::DA_COUNT)
        algorithm = AnyVODEnums::DA_BLEND;

    this->m_player.setDeinterlaceAlgorithm(algorithm);
}

void MediaPlayerDelegate::selectDeinterlacerMethod(int method)
{
    this->m_player.setDeinterlaceMethod((AnyVODEnums::DeinterlaceMethod)method);
}

void MediaPlayerDelegate::selectDeinterlacerAlgorithem(int algorithm)
{
    this->m_player.setDeinterlaceAlgorithm((AnyVODEnums::DeinterlaceAlgorithm)algorithm);
}

void MediaPlayerDelegate::prevAudioSync()
{
    this->m_player.prevAudioSync(0.05);
}

void MediaPlayerDelegate::nextAudioSync()
{
    this->m_player.nextAudioSync(0.05);
}

void MediaPlayerDelegate::resetAudioSync()
{
    this->m_player.resetAudioSync();
}

void MediaPlayerDelegate::subtitleHAlignOrder()
{
    if (this->m_player.isAlignable())
    {
        AnyVODEnums::HAlignMethod align = this->m_player.getHAlign();

        align = (AnyVODEnums::HAlignMethod)(align + 1);

        if (align >= AnyVODEnums::HAM_COUNT)
            align = AnyVODEnums::HAM_NONE;

        this->m_player.setHAlign(align);
    }
}

void MediaPlayerDelegate::selectSubtitleHAlignMethod(int method)
{
    if (this->m_player.isAlignable())
        this->m_player.setHAlign((AnyVODEnums::HAlignMethod)method);
}

void MediaPlayerDelegate::repeatRangeStart()
{
    if (this->m_player.hasDuration())
    {
        double pos = this->m_player.getCurrentPosition();

        this->m_player.setRepeatStart(pos);
    }
}

void MediaPlayerDelegate::repeatRangeEnd()
{
    if (this->m_player.hasDuration())
    {
        double pos = this->m_player.getCurrentPosition();

        this->m_player.setRepeatEnd(pos);
    }
}

void MediaPlayerDelegate::repeatRangeEnable()
{
    this->m_player.setRepeatEnable(!this->m_player.getRepeatEnable());
}

void MediaPlayerDelegate::repeatRangeStartBack100MS()
{
    if (this->m_player.hasDuration())
        this->m_player.setRepeatStartMove(-0.1);
}

void MediaPlayerDelegate::repeatRangeStartForw100MS()
{
    if (this->m_player.hasDuration())
        this->m_player.setRepeatStartMove(0.1);
}

void MediaPlayerDelegate::repeatRangeEndBack100MS()
{
    if (this->m_player.hasDuration())
        this->m_player.setRepeatEndMove(-0.1);
}

void MediaPlayerDelegate::repeatRangeEndForw100MS()
{
    if (this->m_player.hasDuration())
        this->m_player.setRepeatEndMove(0.1);
}

void MediaPlayerDelegate::repeatRangeBack100MS()
{
    if (this->m_player.hasDuration())
        this->m_player.setRepeatMove(-0.1);
}

void MediaPlayerDelegate::repeatRangeForw100MS()
{
    if (this->m_player.hasDuration())
        this->m_player.setRepeatMove(0.1);
}

void MediaPlayerDelegate::seekKeyFrame()
{
    this->m_player.setSeekKeyFrame(!this->m_player.isSeekKeyFrame());
}

void MediaPlayerDelegate::skipOpening()
{
    this->m_player.setSkipOpening(!this->m_player.getSkipOpening());
}

void MediaPlayerDelegate::skipEnding()
{
    this->m_player.setSkipEnding(!this->m_player.getSkipEnding());
}

void MediaPlayerDelegate::useSkipRange()
{
    this->m_player.setUseSkipRange(!this->m_player.getUseSkipRange());
}

void MediaPlayerDelegate::prevFrame()
{
    if (this->m_player.canMoveFrame())
        this->m_player.prevFrame(1);
}

void MediaPlayerDelegate::nextFrame()
{
    if (this->m_player.canMoveFrame())
        this->m_player.nextFrame(1);
}

void MediaPlayerDelegate::slowerPlayback()
{
    if (this->m_player.isTempoUsable())
    {
        int cur = (int)this->m_player.getTempo();

        if (cur > -90)
            this->m_player.setTempo(cur - 10.0f);
    }
}

void MediaPlayerDelegate::fasterPlayback()
{
    if (this->m_player.isTempoUsable())
    {
        int cur = (int)this->m_player.getTempo();

        this->m_player.setTempo(cur + 10.0f);
    }
}

void MediaPlayerDelegate::normalPlayback()
{
    if (this->m_player.isTempoUsable())
        this->m_player.setTempo(0.0f);
}

void MediaPlayerDelegate::lowerMusic()
{
    this->m_player.useLowerMusic(!this->m_player.isUsingLowerMusic());
}

void MediaPlayerDelegate::lowerVoice()
{
    this->m_player.useLowerVoice(!this->m_player.isUsingLowerVoice());
}

void MediaPlayerDelegate::higherVoice()
{
    this->m_player.useHigherVoice(!this->m_player.isUsingHigherVoice());
}

void MediaPlayerDelegate::pincushionUpK1()
{
    this->m_player.addPincushionK1(0.001f);
}

void MediaPlayerDelegate::pincushionDownK1()
{
    this->m_player.addPincushionK1(-0.001f);
}

void MediaPlayerDelegate::pincushionUpK2()
{
    this->m_player.addPincushionK2(0.001f);
}

void MediaPlayerDelegate::pincushionDownK2()
{
    this->m_player.addPincushionK2(-0.001f);
}

void MediaPlayerDelegate::incSubtitleOpaque()
{
    if (this->m_player.existSubtitle())
        this->m_player.addSubtitleOpaque(0.05f);
}

void MediaPlayerDelegate::decSubtitleOpaque()
{
    if (this->m_player.existSubtitle())
        this->m_player.addSubtitleOpaque(-0.05f);
}

void MediaPlayerDelegate::resetSubtitleOpaque()
{
    if (this->m_player.existSubtitle())
        this->m_player.resetSubtitleOpaque();
}

void MediaPlayerDelegate::playOrder()
{
    AnyVODEnums::PlayingMethod method = this->m_player.getPlayingMethod();

    method = (AnyVODEnums::PlayingMethod)(method + 1);

    if (method >= AnyVODEnums::PM_COUNT)
        method = AnyVODEnums::PM_TOTAL;

    this->m_player.setPlayingMethod(method);
}

void MediaPlayerDelegate::selectPlayingMethod(int method)
{
    this->m_player.setPlayingMethod((AnyVODEnums::PlayingMethod)method);
}
