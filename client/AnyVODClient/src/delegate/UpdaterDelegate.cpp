﻿#include "UpdaterDelegate.h"
#include "net/Updater.h"
#include "core/EnumsTranslator.h"
#include "media/MediaPlayer.h"

UpdaterDelegate::UpdaterDelegate()
{

}

UpdaterDelegate& UpdaterDelegate::getInstance()
{
    static UpdaterDelegate instance;

    return instance;
}

void UpdaterDelegate::selectUpdateGap(int gap)
{
    AnyVODEnums::CheckUpdateGap updateGap = (AnyVODEnums::CheckUpdateGap)gap;

    this->m_player.showOptionDesc(tr("업데이트 확인 주기 (%1)").arg(this->m_enums.getUpdateGapDesc(updateGap)));
    Updater::getInstance().setUpdateGap(updateGap);
}

void UpdaterDelegate::updateGapOrder()
{
    AnyVODEnums::CheckUpdateGap gap = Updater::getInstance().getUpdateGap();

    gap = (AnyVODEnums::CheckUpdateGap)(gap + 1);

    if (gap >= AnyVODEnums::CUG_COUNT)
        gap = AnyVODEnums::CUG_WEEK;

    this->selectUpdateGap(gap);
}
