﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "Updater.h"
#include "net/HttpDownloader.h"
#include "setting/Settings.h"
#include "setting/SettingsFactory.h"
#include "../../../../common/version.h"

#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QRegularExpressionMatchIterator>
#include <QBuffer>
#include <QSettings>

#include <algorithm>

using namespace std;

#ifdef Q_OS_MOBILE
const QString Updater::PREFIX = "AnyVODMobile-";
#else
const QString Updater::PREFIX = "AnyVOD-";
#endif
const QString Updater::DOWNLOAD_PAGE_URL = "https://bitbucket.org/chadr123/anyvod/downloads/";

Updater::Updater() :
    m_settings(SettingsFactory::getInstance(SettingsFactory::ST_GLOBAL))
{
    connect(this, &Updater::findResult, this, &Updater::updateResult);
}

Updater& Updater::getInstance()
{
    static Updater instance;

    return instance;
}

void Updater::findUpdate()
{
    if (this->isRunning())
        return;

    this->start();
}

void Updater::checkUpdate()
{
    QDate current = QDate::currentDate();
    QDate last = this->m_settings.value(SETTING_CURRENT_UPDATE_DATE, current).toDate();
    bool expired = false;

    switch (this->getUpdateGap())
    {
        case AnyVODEnums::CUG_ALWAYS:
            expired = true;
            break;
        case AnyVODEnums::CUG_EVERY_DAY:
            expired = last.addDays(1) < current;
            break;
        case AnyVODEnums::CUG_WEEK:
            expired = last.addDays(7) < current;
            break;
        case AnyVODEnums::CUG_MONTH:
            expired = last.addMonths(1) < current;
            break;
        case AnyVODEnums::CUG_HALF_YEAR:
            expired = last.addMonths(6) < current;
            break;
        case AnyVODEnums::CUG_YEAR:
            expired = last.addYears(1) < current;
            break;
        default:
            break;
    }

    if (expired)
    {
        this->m_settings.setValue(SETTING_CURRENT_UPDATE_DATE, current);
        this->findUpdate();
    }
}

QString Updater::getUpdateURL()
{
    QString fileName = this->m_settings.value(SETTING_UPDATE_FILENAME, "").toString();
    QString url;

    if (fileName.isEmpty())
        return url;

    if (!this->isUpdatable(fileName))
        return url;

    this->m_settings.setValue(SETTING_UPDATE_FILENAME, "");

    return this->getUpdateURL(fileName);
}

void Updater::updateCurrentDate() const
{
    this->m_settings.setValue(SETTING_CURRENT_UPDATE_DATE, QDate::currentDate());
}

QString Updater::getFileName() const
{
    HttpDownloader downloader;
    QBuffer buf;
    QString fileName;

    if (!downloader.download(DOWNLOAD_PAGE_URL, buf))
        return fileName;

    QString contents = buf.buffer();
    const QStringList archs = this->getArchs();

    for (const QString &arch : archs)
    {
        QRegularExpression exp(">(" + PREFIX + R"(\d\.\d\.\d_)" + arch + R"(\.[a-zA-Z\d]+)<)");
        QRegularExpressionMatchIterator iter = exp.globalMatch(contents);
        QStringList list;

        while (iter.hasNext())
        {
            QRegularExpressionMatch match = iter.next();

            list.append(match.captured(1));
        }

        if (list.isEmpty())
            continue;

        stable_sort(list.begin(), list.end(), greater<QString>());

        int curVersion = this->getCurrnetVersion();

        for (const QString &item : list)
        {
            int version = this->getVersionFromFileName(item);

            if (version > curVersion)
            {
                fileName = item;
                break;
            }
        }

        if (!fileName.isEmpty())
            break;
    }

    return fileName;
}

QString Updater::getUpdateURL(const QString &fileName) const
{
    return DOWNLOAD_PAGE_URL + fileName;
}

AnyVODEnums::CheckUpdateGap Updater::getUpdateGap() const
{
    return (AnyVODEnums::CheckUpdateGap)this->m_settings.value(SETTING_CHECK_UPDATE_GAP, AnyVODEnums::CUG_WEEK).toInt();
}

void Updater::setUpdateGap(AnyVODEnums::CheckUpdateGap gap)
{
    this->m_settings.setValue(SETTING_CHECK_UPDATE_GAP, gap);
    this->m_settings.setValue(SETTING_CURRENT_UPDATE_DATE, QDate::currentDate());
}

void Updater::fixUpdateGap()
{
    QDate updateDate = this->m_settings.value(SETTING_CURRENT_UPDATE_DATE, QDate()).toDate();

    if (updateDate.isValid())
    {
        bool fixed = this->m_settings.value(SETTING_CHECK_UPDATE_GAP_FIX, false).toBool();

        if (!fixed)
        {
            this->m_settings.setValue(SETTING_CHECK_UPDATE_GAP_FIX, true);
            this->m_settings.setValue(SETTING_CHECK_UPDATE_GAP, AnyVODEnums::CUG_WEEK);
        }
    }
    else
    {
        this->m_settings.setValue(SETTING_CHECK_UPDATE_GAP_FIX, true);
        this->m_settings.setValue(SETTING_CHECK_UPDATE_GAP, AnyVODEnums::CUG_WEEK);
        this->m_settings.setValue(SETTING_CURRENT_UPDATE_DATE, QDate::currentDate());
    }
}

void Updater::updateResult(const QString &fileName)
{
    this->m_settings.setValue(SETTING_UPDATE_FILENAME, fileName);
}

bool Updater::isUpdatable(const QString &fileName) const
{
    int curVersion = this->getCurrnetVersion();
    int version = this->getVersionFromFileName(fileName);

    return version > curVersion;
}

int Updater::getCurrnetVersion() const
{
    const QString VERSION_TEMPLATE = "%1%2%3";
    QString version;

    version = VERSION_TEMPLATE.arg(MAJOR).arg(MINOR).arg(PATCH);

    return version.toInt();
}

int Updater::getVersionFromFileName(const QString &fileName) const
{
    QString work = fileName;
    QString version;
    QStringList items;

    work.replace(PREFIX, "");

    items = work.split("_");

    if (items.length() <= 0)
        return -1;

    items = items.first().split(".");

    if (items.length() < 3)
        return -1;

    version = items.join("");

    return version.toInt();
}

QStringList Updater::getArchs() const
{
    QStringList arch;

#ifdef Q_OS_MOBILE
# if defined Q_OS_IOS
    arch.append("ios");
# elif defined Q_OS_ANDROID
    arch.append("android");
# elif defined Q_OS_RASPBERRY_PI
    arch.append("raspi_armhf");
# endif
#else
# if defined Q_OS_WIN
    arch.append("win");
# elif defined Q_OS_MAC
    arch.append("mac");
# elif defined Q_OS_LINUX
#  if defined Q_PROCESSOR_X86_64
    arch.append("linux_amd64");
#  endif
# endif
#endif

    return arch;
}

void Updater::run()
{
    QString fileName = this->getFileName();

    if (!fileName.isEmpty())
        emit this->findResult(fileName);
}
