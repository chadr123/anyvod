﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "SubtitleImpl.h"
#include "SyncHttp.h"

#include <string>
#include <stdint.h>
#include <stdio.h>
#include <fcntl.h>

#include <QFile>
#include <QCryptographicHash>
#include <QBuffer>

#if defined Q_OS_LINUX || defined Q_OS_MAC
# include <unistd.h>
#endif

using namespace std;

SubtitleImpl::SubtitleImpl()
{

}

FILE* SubtitleImpl::openFile(const wstring &filePath)
{
    QFile file(QString::fromStdWString(filePath));

    if (!file.open(QIODevice::ReadOnly))
        return nullptr;

    return fdopen(dup(file.handle()), "rb");
}

bool SubtitleImpl::retreiveFromAlSongServer(const string &data, wstring *ret)
{
    SyncHttp alsong;
    QBuffer result;

    alsong.syncPost(ALSONG_SERVER, ALSONG_URL, QString::fromStdString(data).toUtf8(), &result);

    *ret = QString::fromUtf8(result.buffer()).toStdWString();

    return alsong.error() == QNetworkReply::NoError;
}

bool SubtitleImpl::retreiveFromGOMServer(const string &md5, wstring *ret)
{
    SyncHttp gom;
    QBuffer result;

    gom.syncGet(GOM_SERVER, GOM_URL, QString::fromStdString(md5), &result);

    *ret = QString::fromUtf8(result.buffer()).toStdWString();

    return gom.error() == QNetworkReply::NoError;
}

void SubtitleImpl::getMD5(uint8_t *buf, size_t size, string *ret)
{
    QCryptographicHash md5(QCryptographicHash::Md5);

    md5.addData((const char*)buf, (int)size);
    *ret = QString(md5.result().toHex()).toStdString();
}
