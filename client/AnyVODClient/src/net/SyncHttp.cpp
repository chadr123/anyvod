﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "SyncHttp.h"

#include <QMutexLocker>
#include <QNetworkAccessManager>
#include <QHttpMultiPart>
#include <QDebug>

const QString SyncHttp::PROTOCOL = "http://";

SyncHttp::SyncHttp() :
    m_header(nullptr),
    m_data(nullptr),
    m_multiParts(nullptr),
    m_to(nullptr),
    m_error(QNetworkReply::NoError)
{

}

SyncHttp::~SyncHttp()
{

}

QNetworkReply::NetworkError SyncHttp::error() const
{
    return this->m_error;
}

void SyncHttp::request(QNetworkRequest *header, const QByteArray *data, QHttpMultiPart *multiParts, QIODevice *to)
{
    this->m_header = header;
    this->m_data = data;
    this->m_multiParts = multiParts;
    this->m_to = to;

    this->start();
    this->wait();

    this->m_header = nullptr;
    this->m_data = nullptr;
    this->m_multiParts = nullptr;
    this->m_to = nullptr;
}

QUrl SyncHttp::getUrl(const QString &host, const QString &path) const
{
    QString url;

    if (!host.startsWith(PROTOCOL, Qt::CaseInsensitive))
        url = PROTOCOL;

    if (host.endsWith('/'))
        url += host.midRef(0, host.length() - 1);
    else
        url += host;

    if (path.startsWith('/'))
        url += path;
    else
        url += '/' + path;

    return QUrl(url);
}

void SyncHttp::syncPost(const QString &host, const QString &path, const QByteArray &data, QIODevice *to)
{
    QMutexLocker locker(&this->m_lock);
    QNetworkRequest header(this->getUrl(host, path));

    header.setHeader(QNetworkRequest::ContentTypeHeader, "application/soap+xml; charset=utf-8");
    header.setRawHeader("User-Agent", "gSOAP/2.7");
    header.setRawHeader("SOAPAction", "ALSongWebServer/GetLyric7");

    this->request(&header, &data, nullptr, to);
}

void SyncHttp::syncGet(const QString &host, const QString &path, const QString &md5, QIODevice *to)
{
    QMutexLocker locker(&this->m_lock);
    QNetworkRequest header(this->getUrl(host, path));
    QHttpMultiPart multiParts(QHttpMultiPart::FormDataType);
    QHttpPart movieKey;

    header.setRawHeader("User-Agent", "GenericHTTPClient");

    movieKey.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"moviekey\""));
    movieKey.setBody(md5.toLatin1());
    multiParts.append(movieKey);

    this->request(&header, nullptr, &multiParts, to);
}

void SyncHttp::received(QNetworkReply*)
{
    this->exit();
}

void SyncHttp::run()
{
    QNetworkAccessManager http;
    QNetworkReply *reply;

    connect(&http, &QNetworkAccessManager::finished, this, &SyncHttp::received, Qt::DirectConnection);

    if (this->m_data)
        reply = http.post(*this->m_header, *this->m_data);
    else if (this->m_multiParts)
        reply = http.post(*this->m_header, this->m_multiParts);
    else
        reply = http.get(*this->m_header);

    this->exec();

    bool opened = this->m_to->isOpen();

    if (!opened)
        this->m_to->open(QIODevice::WriteOnly);

    this->m_to->write(reply->readAll());

    if (!opened)
        this->m_to->close();

    this->m_error = reply->error();

    delete reply;
}
