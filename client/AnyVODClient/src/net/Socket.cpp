﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "Socket.h"

#ifdef Q_OS_WIN
# ifdef _WIN32_WINNT
#  undef _WIN32_WINNT
# endif
# define _WIN32_WINNT 0x0501
# define WIN32_LEAN_AND_MEAN

# include <winsock2.h>
# include <ws2tcpip.h>
#else
# include <unistd.h>
# include <netinet/tcp.h>
# include <netdb.h>
#endif

#include <algorithm>

#include "utils/StringUtils.h"

#include "../../../common/network.h"

#include <QMutexLocker>
#include <QByteArray>
#include <QDebug>

const int Socket::DEFAULT_READBUFFER_SIZE = TOTAL_PACKET_SIZE * 20;

Socket::Socket() :
    m_buffer(nullptr),
    m_bufferSize(0),
    m_socket(-1)
{
#ifdef Q_OS_WIN
    WSADATA wsaData;

    WSAStartup(MAKEWORD(2, 2), &wsaData);
#endif

    this->restoreReadBufferSize();
}

Socket::~Socket()
{
    this->disconnect();
    delete[] this->m_buffer;
    this->m_buffer = nullptr;
    this->m_bufferSize = 0;
}

Socket& Socket::getInstance()
{
    static Socket socket;

    return socket;
}

Socket& Socket::getStreamInstance()
{
    static Socket socket;

    return socket;
}

void Socket::setReadBufferSize(int size) const
{
    setsockopt(this->m_socket, SOL_SOCKET, SO_RCVBUF, (char*)&size, sizeof(size));
}

void Socket::restoreReadBufferSize() const
{
    this->setReadBufferSize(DEFAULT_READBUFFER_SIZE);
}

ERROR_TYPE Socket::checkError(ANYVOD_PACKET &packet, QString *error)
{
    if (ntohl(packet.s2c_header.header.type) == PT_S2C_ERROR)
    {
        int errorType = ntohl(packet.s2c_error.type);

        if (error)
            *error = QString::fromWCharArray(ERROR_TYPE_MSG[errorType]);

        return (ERROR_TYPE)errorType;
    }
    else if (packet.s2c_header.success)
    {
        return ET_NONE;
    }
    else
    {
        ANYVOD_PACKET *errorPacket;

        if (this->recv(&errorPacket))
        {
            if (ntohl(errorPacket->s2c_header.header.type) == PT_S2C_ERROR)
            {
                int errorType = ntohl(errorPacket->s2c_error.type);

                if (error)
                    *error = QString::fromWCharArray(ERROR_TYPE_MSG[errorType]);

                return (ERROR_TYPE)errorType;
            }
            else
            {
                if (error)
                    *error = QString::fromWCharArray(ERROR_TYPE_MSG[ET_UNKNOWN]);

                return ET_UNKNOWN;
            }
        }
        else
        {
            if (error)
                *error = QString::fromWCharArray(ERROR_TYPE_MSG[ET_UNKNOWN]);

            return ET_UNKNOWN;
        }
    }
}

bool Socket::connect(const QString &address, uint16_t port)
{
    addrinfo hints;
    addrinfo *resolve;
    char stringPort[10] = {0, };
    int ret;

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    snprintf(stringPort, sizeof(stringPort), "%d", port);

    ret = getaddrinfo(address.toLocal8Bit(), stringPort, &hints, &resolve);

    if (ret)
        return false;

    this->m_socket = socket(resolve->ai_family, resolve->ai_socktype, resolve->ai_protocol);

    if (this->m_socket < 0)
    {
        freeaddrinfo(resolve);
        return false;
    }

    bool option = true;

    setsockopt(this->m_socket, IPPROTO_TCP, TCP_NODELAY, (char*)&option, sizeof(option));
    ret = ::connect(this->m_socket, resolve->ai_addr, resolve->ai_addrlen);

    freeaddrinfo(resolve);

    if (ret)
    {
        this->disconnect();
        return false;
    }

    return true;
}

void Socket::disconnect()
{
    this->m_ticket.clear();
#ifdef Q_OS_WIN
    closesocket(this->m_socket);
#else
    close(this->m_socket);
#endif
    this->m_socket = -1;
}

bool Socket::isConnected() const
{
    return this->m_socket >= 0;
}

bool Socket::send(ANYVOD_PACKET &packet)
{
    QMutexLocker locker(&this->m_lock);
    qint64 rest = packet.c2s_header.header.size;
    qint64 sent = 0;
    char *data = (char*)&packet;

    packet.c2s_header.header.size = htonl(rest);
    packet.c2s_header.header.type = htonl(packet.c2s_header.header.type);

    while (rest)
    {
        sent = ::send(this->m_socket, data, rest, 0);

        if (sent == -1)
        {
            this->disconnect();
            return false;
        }

        rest -= sent;
        data += sent;
    }

    return true;
}

bool Socket::recvSub(char *data, qint64 size)
{
    qint64 rest = size;
    qint64 read = 0;

    while (rest)
    {
        read = ::recv(this->m_socket, data, rest, 0);

        if (read == -1 || read == 0)
        {
            this->disconnect();
            return false;
        }

        rest -= read;
        data += read;
    }

    return true;
}

bool Socket::recv(ANYVOD_PACKET **packet)
{
    QMutexLocker locker(&this->m_lock);
    ANYVOD_PACKET tmpPacket;
    char *data = (char*)&tmpPacket;

    if (this->recvSub(data, S2C_HEADER_SIZE))
    {
        int size = ntohl(tmpPacket.s2c_header.header.size);

        if (this->m_bufferSize < size)
        {
            delete[] this->m_buffer;
            this->m_buffer = new uint8_t[size];
            this->m_bufferSize = size;
        }

        memcpy(this->m_buffer, &tmpPacket, S2C_HEADER_SIZE);

        data = (char*)this->m_buffer;
        data += S2C_HEADER_SIZE;

        if (this->recvSub(data, size - S2C_HEADER_SIZE))
        {
            *packet = (ANYVOD_PACKET*)this->m_buffer;
            return true;
        }
    }

    return false;
}

void Socket::fillTicket(ANYVOD_PACKET *packet) const
{
    strncpy((char*)packet->c2s_header.ticket, this->m_ticket.toLocal8Bit(), MAX_TICKET_CHAR_SIZE);
}

bool Socket::waitResponse(ANYVOD_PACKET &packet, QString *error, ANYVOD_PACKET **recvPacket)
{
    ANYVOD_PACKET *tmpPacket;
    ANYVOD_PACKET **data = recvPacket == nullptr ? &tmpPacket : recvPacket;
    bool success = this->send(packet);

    if (success)
        success = this->recvRequest(data, error);

    if (error && success == false && this->m_socket == -1)
        *error = tr("접속이 끊겼습니다");

    return success;
}

bool Socket::recvRequest(ANYVOD_PACKET **ret, QString *error)
{
    return this->recv(ret) && this->checkError(**ret, error) == ET_NONE;
}

bool Socket::login(const QString &id, const QString &password, QString *error)
{
    ANYVOD_PACKET packet;
    ANYVOD_PACKET *recvPacket;

    packet.c2s_login.header.size = sizeof(packet.c2s_login);
    packet.c2s_login.header.type = PT_C2S_LOGIN;

    QByteArray buf;

    buf = id.toUtf8();
    memset(packet.c2s_login.id, 0x00, sizeof(packet.c2s_login.id));
    memcpy(packet.c2s_login.id, buf.constData(), min(MAX_ID_CHAR_SIZE, buf.size()));

    buf = password.toUtf8();
    memset(packet.c2s_login.pass, 0x00, sizeof(packet.c2s_login.pass));
    memcpy(packet.c2s_login.pass, buf.constData(), min(MAX_PASS_CHAR_SIZE, buf.size()));

    if (this->waitResponse(packet, error, &recvPacket))
    {
        this->m_ticket = QString::fromLocal8Bit((char*)recvPacket->s2c_login.ticket, MAX_TICKET_CHAR_SIZE);
        return true;
    }

    return false;
}

bool Socket::join(const QString &ticket, QString *error)
{
    ANYVOD_PACKET packet;
    ANYVOD_PACKET *recvPacket;

    packet.c2s_join.header.size = sizeof(packet.c2s_join);
    packet.c2s_join.header.type = PT_C2S_JOIN;

    strncpy((char*)packet.c2s_join.ticket, ticket.toLocal8Bit(), MAX_TICKET_CHAR_SIZE);

    if (this->waitResponse(packet, error, &recvPacket))
    {
        this->m_ticket = ticket;
        return true;
    }

    return false;
}

bool Socket::logout(QString *error)
{
    ANYVOD_PACKET packet;

    this->fillTicket(&packet);

    packet.c2s_logout.header.size = sizeof(packet.c2s_logout);
    packet.c2s_logout.header.type = PT_C2S_LOGOUT;

    if (this->waitResponse(packet, error))
    {
        this->disconnect();
        return true;
    }

    return false;
}

bool Socket::requestFilelist(const QString &path, QString *error, QList<ANYVOD_FILE_ITEM> *ret)
{
    ANYVOD_PACKET packet;
    ANYVOD_PACKET *recvPacket;

    this->fillTicket(&packet);

    packet.c2s_filelist.header.size = sizeof(packet.c2s_filelist);
    packet.c2s_filelist.header.type = PT_C2S_FILELIST;

    QByteArray buf = path.toUtf8();
    memset(packet.c2s_filelist.path, 0x00, sizeof(packet.c2s_filelist.path));
    memcpy(packet.c2s_filelist.path, buf.constData(), min(MAX_PATH_CHAR_SIZE, buf.size()));

    if (this->waitResponse(packet, error, &recvPacket))
    {
        for (unsigned int i = 0; i < ntohl(recvPacket->s2c_filelist.count); i++)
        {
            ANYVOD_FILE_ITEM item;
            ANYVOD_PACKET_FILE_ITEM &packetItem = recvPacket->s2c_filelist.Items[i];

            item.bitRate = ntohl(packetItem.bitRate);
            item.permission = packetItem.permission;
            item.totalSize = ntohll(packetItem.totalSize);
            item.totalFrame = ntohl(packetItem.totalFrame);
            item.totalTime = ntohl(packetItem.totalTime);
            item.type = (FILE_ITEM_TYPE)ntohl(packetItem.type);
            item.fileName = QString::fromUtf8((char *)packetItem.fileName).toStdWString();
            item.title = QString::fromUtf8((char *)packetItem.title).toStdWString();
            item.movieType = QString::fromUtf8((char *)packetItem.movieType).toStdWString();

            QString tmp = QString::fromStdWString(item.title);

            std::transform(item.movieType.begin(), item.movieType.end(), item.movieType.begin(), ::towlower);

            if (item.movieType == L"mpeg audio" && StringUtils::isLocal8bit(tmp))
                item.title = QString::fromLocal8Bit(tmp.toLatin1()).toStdWString();

            ret->append(item);
        }

        return true;
    }

    return false;
}

bool Socket::startMovie(const QString &path, QString *error, ANYVOD_MOVIE_INFO *ret)
{
    ANYVOD_PACKET packet;
    ANYVOD_PACKET *recvPacket;

    this->fillTicket(&packet);

    packet.c2s_start_movie.header.size = sizeof(packet.c2s_start_movie);
    packet.c2s_start_movie.header.type = PT_C2S_START_MOVIE;

    QByteArray buf = path.toUtf8();
    memset(packet.c2s_start_movie.filePath, 0x00, sizeof(packet.c2s_start_movie.filePath));
    memcpy(packet.c2s_start_movie.filePath, buf.constData(), min(MAX_FILEPATH_CHAR_SIZE, buf.size()));

    if (this->waitResponse(packet, error, &recvPacket))
    {
        ret->totalSize = ntohll(recvPacket->s2c_start_movie.totalSize);
        ret->bitRate = ntohl(recvPacket->s2c_start_movie.bitRate);
        ret->bitRateMultiplier = recvPacket->s2c_start_movie.bitRateMultiplier;
        ret->movieType = QString::fromUtf8((char *)recvPacket->s2c_start_movie.movieType).toStdWString();
        ret->codecVideo = QString::fromUtf8((char *)recvPacket->s2c_start_movie.codecVideo).toStdWString();

        return true;
    }

    return false;
}

bool Socket::stopMovie(QString *error)
{
    ANYVOD_PACKET packet;
    ANYVOD_PACKET *recvPacket;

    this->fillTicket(&packet);

    packet.c2s_stop_movie.header.size = sizeof(packet.c2s_stop_movie);
    packet.c2s_stop_movie.header.type = PT_C2S_STOP_MOVIE;

    if (this->waitResponse(packet, error, &recvPacket))
        return true;

    return false;
}

bool Socket::streamRequest(uint64_t start, uint32_t size, QString *error)
{
    ANYVOD_PACKET packet;
    ANYVOD_PACKET *recvPacket;

    this->fillTicket(&packet);

    packet.c2s_stream_request.header.size = sizeof(packet.c2s_stream_request);
    packet.c2s_stream_request.header.type = PT_C2S_STREAM_REQUEST;

    packet.c2s_stream_request.startOffset = htonll(start);
    packet.c2s_stream_request.size = htonl(size);

    if (this->waitResponse(packet, error, &recvPacket))
        return true;

    return false;
}

bool Socket::subtitle(const QString &path, bool searchSubtitle, bool searchLyrics, QString *error, QString *subtitleFileName, QByteArray *ret)
{
    ANYVOD_PACKET packet;
    ANYVOD_PACKET *recvPacket;

    this->fillTicket(&packet);

    packet.c2s_subtitle.header.size = sizeof(packet.c2s_subtitle);
    packet.c2s_subtitle.header.type = PT_C2S_SUBTITLE;

    QByteArray buf = path.toUtf8();
    memset(packet.c2s_subtitle.filePath, 0x00, sizeof(packet.c2s_subtitle.filePath));
    memcpy(packet.c2s_subtitle.filePath, buf.constData(), min(MAX_PATH_CHAR_SIZE, buf.size()));

    packet.c2s_subtitle.searchSubtitle = searchSubtitle;
    packet.c2s_subtitle.searchLyrics = searchLyrics;

    if (this->waitResponse(packet, error, &recvPacket))
    {
        if (!this->recvRequest(&recvPacket, error))
            return false;

        *subtitleFileName = QString::fromUtf8((char *)recvPacket->admin_s2c_file_start.path);

        while (true)
        {
            if (!this->recvRequest(&recvPacket, error))
                return false;

            if (ntohl(recvPacket->s2c_header.header.type) == PT_ADMIN_S2C_FILE_END)
                break;

            ANYVOD_PACKET_ADMIN_S2C_FILE_STREAM &item = recvPacket->admin_s2c_file_stream;

            ret->append((char*)item.data, ntohl(item.size));
        }

        return true;
    }

    return false;
}

bool Socket::existSubtitleURL(const QString &path, bool searchSubtitle, QString *error, QString *url)
{
    ANYVOD_PACKET packet;
    ANYVOD_PACKET *recvPacket;

    this->fillTicket(&packet);

    packet.c2s_subtitle_url.header.size = sizeof(packet.c2s_subtitle_url);
    packet.c2s_subtitle_url.header.type = PT_C2S_SUBTITLE_URL;

    QByteArray buf = path.toUtf8();
    memset(packet.c2s_subtitle_url.filePath, 0x00, sizeof(packet.c2s_subtitle_url.filePath));
    memcpy(packet.c2s_subtitle_url.filePath, buf.constData(), min(MAX_PATH_CHAR_SIZE, buf.size()));

    packet.c2s_subtitle_url.searchSubtitle = searchSubtitle;

    if (this->waitResponse(packet, error, &recvPacket))
    {
        *url = QString::fromUtf8((char *)recvPacket->s2c_subtitle_url.url);
        return true;
    }

    return false;
}

bool Socket::isLogined() const
{
    return this->m_ticket.count() != 0;
}

void Socket::getTicket(QString *ret) const
{
    *ret = this->m_ticket;
}
