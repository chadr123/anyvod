﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "NetFile.h"
#include "Socket.h"
#include "utils/SeparatingUtils.h"
#include "media/MediaPresenter.h"

#ifndef Q_OS_WIN
# include <arpa/inet.h>
#endif

#include <QFileInfo>
#include <QByteArray>
#include <QTemporaryFile>
#include <QDir>
#include <QMutexLocker>
#include <QDebug>

#include <zlib.h>

const int NetFile::MAX_BUFFERING_SECOND = 10;

NetFile::NetFile() :
    m_currentPos(0)
{

}

NetFile::~NetFile()
{
    this->close();
}

bool NetFile::readRequested(ANYVOD_PACKET **recv, bool *isError)
{
    if (Socket::getStreamInstance().recvRequest(recv, nullptr))
    {
        int type = ntohl((*recv)->s2c_header.header.type);

        if (type == PT_S2C_STREAM_DATA || type == PT_S2C_STREAM_META_DATA)
        {
            *isError = false;

            return true;
        }
        else if (type == PT_S2C_STREAM_END)
        {
            *isError = false;

            return false;
        }
    }

    *isError = true;

    return false;
}

void NetFile::loadSubtitle(const QString &filePath, MediaPresenter *presenter)
{
    QByteArray buffer;
    QString fileName;

    Socket::getInstance().subtitle(filePath, presenter->isEnableSearchSubtitle(), presenter->isEnableSearchLyrics(), nullptr, &fileName, &buffer);

    if (fileName.isEmpty())
        return;

    QString tmpFilePath = QDir::tempPath();
    QString ext = QFileInfo(fileName).suffix();

    SeparatingUtils::appendDirSeparator(&tmpFilePath);
    tmpFilePath += "XXXXXX.";
    tmpFilePath += ext;

    QTemporaryFile tmpFile(tmpFilePath);

    if (!tmpFile.open())
        return;

    if (!tmpFile.write(buffer))
        return;

    tmpFile.close();

    presenter->closeAllExternalSubtitles();
    presenter->openSubtitle(tmpFile.fileName(), false);
}

bool NetFile::open(const QString &path, QString *error)
{
    bool success = false;

    success = Socket::getStreamInstance().startMovie(path, error, &this->m_info);

    if (success)
    {
        this->m_currentPos = 0;

        Socket::getStreamInstance().setReadBufferSize((this->m_info.bitRate / 8) * MAX_BUFFERING_SECOND);
    }
    else
    {
        Socket::getStreamInstance().stopMovie(nullptr);
    }

    return success;
}

void NetFile::close()
{
    if (Socket::getStreamInstance().isLogined())
        Socket::getStreamInstance().stopMovie(nullptr);

    Socket::getStreamInstance().restoreReadBufferSize();
}

int NetFile::read(uint8_t *buf, int size)
{
    QMutexLocker locker(&this->m_lock);
    int len = 0;
    ANYVOD_PACKET *recv = nullptr;
    bool isError = false;

    if (!Socket::getStreamInstance().streamRequest(this->m_currentPos, size, nullptr))
        return AVERROR(EIO);

    while (true)
    {
        if (this->readRequested(&recv, &isError))
        {
            uint8_t *bufferStart = buf + len;
            int type = ntohl(recv->s2c_header.header.type);

            if (type == PT_S2C_STREAM_DATA)
            {
                int size = ntohl(recv->s2c_stream_data.size);

                if (size <= 0)
                {
                    len = AVERROR_EOF;
                    continue;
                }

                memcpy(bufferStart, recv->s2c_stream_data.data, size);
                len += size;
            }
            else if (type == PT_S2C_STREAM_META_DATA)
            {
               char uncomp[MAX_STREAM_SIZE];
               unsigned long destLen = MAX_STREAM_SIZE;
               int size = ntohl(recv->s2c_stream_meta_data.size);
               int zipError = uncompress((Bytef*)uncomp, &destLen, (Bytef*)recv->s2c_stream_meta_data.data, size);

               if (zipError != Z_OK)
               {
                   len = AVERROR(EIO);
                   break;
               }
               else
               {
                   if (destLen <= 0)
                   {
                       len = AVERROR_EOF;
                       continue;
                   }

                   memcpy(bufferStart, uncomp, destLen);
                   len += destLen;
               }
            }
        }
        else
        {
            if (isError)
                len = AVERROR(EIO);

            break;
        }
    }

    if (len > 0)
        this->m_currentPos += len;

    return len;
}

int64_t NetFile::seek(int64_t pos, int whence)
{
    QMutexLocker locker(&this->m_lock);

    if (whence == AVSEEK_SIZE)
    {
        return this->m_info.totalSize;
    }
    else
    {
        int64_t newPos = this->m_currentPos;

        if (whence == SEEK_SET)
            newPos = pos;
        else if (whence == SEEK_CUR)
            newPos += pos;
        else if (whence == SEEK_END)
            newPos = this->m_info.totalSize + pos;
        else
            return AVERROR(EINVAL);

        if (newPos > (int64_t)this->m_info.totalSize || newPos < 0)
            return AVERROR(EIO);

        this->m_currentPos = newPos;

        return this->m_currentPos;
    }
}
