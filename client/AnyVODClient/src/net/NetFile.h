﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "../../../../common/types.h"

#include <stdint.h>

#include <QMutex>

union ANYVOD_PACKET;

class QString;
class MediaPresenter;

class NetFile
{
public:
    NetFile();
    ~NetFile();

    bool open(const QString &path, QString *error);
    void close();

    int read(uint8_t *buf, int size);
    int64_t seek(int64_t pos, int whence);

    void loadSubtitle(const QString &filePath, MediaPresenter *presenter);

private:
    bool readRequested(ANYVOD_PACKET **recv, bool *isError);

private:
    static const int MAX_BUFFERING_SECOND;

private:
    ANYVOD_MOVIE_INFO m_info;
    int64_t m_currentPos;
    QMutex m_lock;
};
