﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "VirtualFile.h"
#include "utils/SeparatingUtils.h"
#include "utils/RemoteFileUtils.h"
#include "media/MediaPresenter.h"

#include <QDebug>

URLProtocol VirtualFile::PROTOCOL = {
    .name = RemoteFileUtils::ANYVOD_PROTOCOL_NAME,
    .url_open = VirtualFile::movieOpen,
    .url_open2 = nullptr,
    .url_accept = nullptr,
    .url_handshake = nullptr,
    .url_read = VirtualFile::movieRead,
    .url_write = nullptr,
    .url_seek = VirtualFile::movieSeek,
    .url_close = VirtualFile::movieClose,
    .url_read_pause = nullptr,
    .url_read_seek = nullptr,
    .url_get_file_handle = nullptr,
    .url_get_multi_file_handle = nullptr,
    .url_get_short_seek = nullptr,
    .url_shutdown = nullptr,
    .priv_data_class = nullptr,
    .priv_data_size = 0,
    .flags = 0,
    .url_check = nullptr,
    .url_open_dir = nullptr,
    .url_read_dir = nullptr,
    .url_close_dir = nullptr,
    .url_delete = nullptr,
    .url_move = nullptr,
    .default_whitelist = nullptr,
};

VirtualFile::VirtualFile()
{
    memset(&this->m_intCallback, 0, sizeof(this->m_intCallback));
}

VirtualFile::~VirtualFile()
{

}

VirtualFile& VirtualFile::getInstance()
{
    static VirtualFile instance;

    return instance;
}

URLProtocol* VirtualFile::getProtocol()
{
    return &VirtualFile::PROTOCOL;
}

void VirtualFile::loadSubtitle(const QString &filePath, MediaPresenter *presenter)
{
    this->m_file.loadSubtitle(filePath, presenter);
}

int VirtualFile::movieOpen(URLContext *h, const char *uri, int)
{
    QString realUri = QString::fromUtf8(uri);
    int index = realUri.lastIndexOf(SeparatingUtils::FFMPEG_SEPARATOR);

    if (index != -1)
    {
        VirtualFile *me = nullptr;
        uint64_t id = 0;
        QString parentID;

        parentID = realUri.mid(index + 1);
        id = parentID.toULongLong();
        me = (VirtualFile*)id;

        h->priv_data = me;

        realUri = realUri.mid(0, index);

        char proto[strlen(RemoteFileUtils::ANYVOD_PROTOCOL_NAME) + 1];
        char path[MAX_FILEPATH_CHAR_SIZE];

        av_url_split(proto, (int)sizeof(proto), nullptr, 0, nullptr, 0, nullptr, path, (int)sizeof(path), realUri.toUtf8().constData());

        if (strcmp(proto, RemoteFileUtils::ANYVOD_PROTOCOL_NAME))
            return AVERROR(EINVAL);

        if (me->m_file.open(QString::fromUtf8(path), nullptr))
        {
            me->m_intCallback = h->interrupt_callback;

            return 0;
        }
        else
        {
            return AVERROR(ENOENT);
        }
    }
    else
    {
        return AVERROR(EINVAL);
    }
}

int VirtualFile::movieRead(URLContext *h, uint8_t *buf, int size)
{
    VirtualFile *me = (VirtualFile*)h->priv_data;

    if (me->m_intCallback.callback && me->m_intCallback.callback(me->m_intCallback.opaque))
        return AVERROR_EXIT;

    return me->m_file.read(buf, size);
}

int64_t VirtualFile::movieSeek(URLContext *h, int64_t pos, int whence)
{
    VirtualFile *me = (VirtualFile*)h->priv_data;

    return me->m_file.seek(pos, whence);
}

int VirtualFile::movieClose(URLContext *h)
{
    VirtualFile *me = (VirtualFile*)h->priv_data;

    me->m_file.close();

    return 0;
}
