﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "StringUtils.h"

#include <QString>

StringUtils::StringUtils()
{

}

QString StringUtils::normalizedString(const QString &str)
{
#ifdef Q_OS_MAC
    return str.normalized(QString::NormalizationForm_C);
#else
    return str;
#endif
}

bool StringUtils::isLocal8bit(const QString &text)
{
    bool local8 = false;

    for (int i = 0; i < text.size(); i++)
    {
        const QChar c = text.at(i);

        if (c >= 0xa0 && c <= 0xff)
        {
            local8 = true;
            break;
        }
    }

    return local8;
}
