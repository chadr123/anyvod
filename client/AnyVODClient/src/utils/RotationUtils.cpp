﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "RotationUtils.h"

RotationUtils::RotationUtils()
{

}

bool RotationUtils::isPortrait(double degree)
{
    return (degree >= 70.0 && degree <= 110.0) || (degree >= 250.0 && degree <= 290.0);
}

float RotationUtils::convertSRDtoNumeric(AnyVODEnums::ScreenRotationDegree value)
{
    switch (value)
    {
        case AnyVODEnums::SRD_90:
            return 90.0f;
        case AnyVODEnums::SRD_180:
            return 180.0f;
        case AnyVODEnums::SRD_270:
            return 270.0f;
        default:
            return 0.0f;
    }
}
