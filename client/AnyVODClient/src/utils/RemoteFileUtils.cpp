﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "RemoteFileUtils.h"
#include "core/Common.h"
#include "device/DTVReader.h"
#include "device/RadioReader.h"

#include <QString>

const char RemoteFileUtils::RemoteFileUtils::ANYVOD_PROTOCOL_NAME[] = "anyvod";
const char RemoteFileUtils::FILE_PROTOCOL_NAME[] = "file";

const QString RemoteFileUtils::ANYVOD_PROTOCOL = QString(ANYVOD_PROTOCOL_NAME) + PROTOCOL_POSTFIX;
const QString RemoteFileUtils::FILE_PROTOCOL = QString(FILE_PROTOCOL_NAME) + PROTOCOL_POSTFIX;

RemoteFileUtils::RemoteFileUtils()
{

}

bool RemoteFileUtils::determinRemoteFile(const QString &filePath)
{
    return filePath.startsWith(ANYVOD_PROTOCOL);
}

bool RemoteFileUtils::determinRemoteProtocol(const QString &filePath)
{
    return filePath.contains(PROTOCOL_POSTFIX) &&
            !filePath.startsWith(FILE_PROTOCOL) &&
            !filePath.startsWith(DTVReader::DTV_PROTOCOL) &&
            !filePath.startsWith(RadioReader::RADIO_PROTOCOL);
}
