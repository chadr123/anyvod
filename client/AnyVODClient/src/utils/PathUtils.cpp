﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "PathUtils.h"
#include "SeparatingUtils.h"

#include <QDir>

#if defined Q_OS_MAC
# include "utils/MacUtils.h"
#endif

PathUtils::PathUtils()
{

}

bool PathUtils::createDirectory(const QString &path)
{
    QDir dir(path);
    QString parent = dir.absolutePath();
    int index = parent.lastIndexOf(SeparatingUtils::DIR_SEPARATOR);

    if (index == -1)
        return false;

    index++;

    QString subDir = parent.mid(index);

    parent = parent.mid(0, index);

    QDir mkDir(parent);

    if (subDir.isEmpty())
        return true;
    else
        return mkDir.mkpath(subDir);
}

QByteArray PathUtils::convertPathToFileSystemRepresentation(const QString &path, bool isDevice)
{
    QByteArray fileSystemPath;

#if defined Q_OS_MAC
    if (isDevice)
        fileSystemPath = path.toUtf8();
    else
        fileSystemPath = MacUtils::convertPathToFileSystemRepresentation(path);
#else
    (void)isDevice;
    fileSystemPath = path.toUtf8();
#endif

    return fileSystemPath;
}
