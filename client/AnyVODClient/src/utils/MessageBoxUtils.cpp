﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "MessageBoxUtils.h"

#include <QMessageBox>

MessageBoxUtils::MessageBoxUtils()
{

}

void MessageBoxUtils::criticalMessageBox(QWidget *parent, const QString &text)
{
    QMessageBox::critical(parent, tr("오류"), text);
}

void MessageBoxUtils::informationMessageBox(QWidget *parent, const QString &text)
{
    QMessageBox::information(parent, tr("정보"), text);
}

bool MessageBoxUtils::questionMessageBox(QWidget *parent, const QString &text)
{
    return QMessageBox::question(parent, tr("질문"), text, QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes;
}
