﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "MediaTypeUtils.h"

#include "../../../../common/types.h"

#include <QStringList>

MediaTypeUtils::MediaTypeUtils()
{

}

bool MediaTypeUtils::isExtension(const QString &ext, MediaType type)
{
    QString key = ext.toLower();
    QStringList *searchExts = nullptr;
    QStringList *searchCache = nullptr;

    switch (type)
    {
        case MT_VIDEO:
        {
            static QStringList exts = QString::fromStdWString(VIDEO_EXTENSION).split(" ", Qt::SkipEmptyParts);
            static QStringList cache;
            static bool removed = false;

#if defined Q_OS_MOBILE
            if (!removed)
            {
                exts.removeOne("dat");
                exts.removeOne("bin");

                removed = true;
            }
#else
            (void)removed;
#endif
            searchExts = &exts;
            searchCache = &cache;

            break;
        }
        case MT_AUDIO:
        {
            static QStringList exts = QString::fromStdWString(AUDIO_EXTENSION).split(" ", Qt::SkipEmptyParts);
            static QStringList cache;

            searchExts = &exts;
            searchCache = &cache;

            break;
        }
        case MT_SUBTITLE:
        {
            static QStringList exts = QString::fromStdWString(SUBTITLE_EXTENSION).split(" ", Qt::SkipEmptyParts);
            static QStringList cache;

            searchExts = &exts;
            searchCache = &cache;

            break;
        }
        case MT_PLAYLIST:
        {
            static QStringList exts = QString::fromStdWString(PLAYLIST_EXTENSION).split(" ", Qt::SkipEmptyParts);
            static QStringList cache;

            searchExts = &exts;
            searchCache = &cache;

            break;
        }
        case MT_FONT:
        {
            static QStringList exts = QString::fromStdWString(FONT_EXTENSION).split(" ", Qt::SkipEmptyParts);
            static QStringList cache;

            searchExts = &exts;
            searchCache = &cache;

            break;
        }
        case MT_CAPTURE_FORMAT:
        {
            static QStringList exts = QString::fromStdWString(CAPTURE_FORMAT_EXTENSION).split(" ", Qt::SkipEmptyParts);
            static QStringList cache;

            searchExts = &exts;
            searchCache = &cache;

            break;
        }
        default:
        {
            return false;
        }
    }

    if (searchCache->contains(key))
    {
        return true;
    }
    else
    {
        if (searchExts->contains(key))
        {
            searchCache->append(key);
            return true;
        }
        else
        {
            return false;
        }
    }
}

bool MediaTypeUtils::isMediaExtension(const QString &ext)
{
    return MediaTypeUtils::isExtension(ext, MT_VIDEO) ||
           MediaTypeUtils::isExtension(ext, MT_AUDIO) ||
           MediaTypeUtils::isExtension(ext, MT_PLAYLIST);
}

bool MediaTypeUtils::isETCExtension(const QString &ext)
{
    for (int i = MT_START; i < MT_COUNT; i++)
    {
        if (MediaTypeUtils::isExtension(ext, (MediaType)i))
            return false;
    }

    return true;
}
