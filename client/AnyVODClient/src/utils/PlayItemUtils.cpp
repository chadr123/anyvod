﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "PlayItemUtils.h"

#include <QVector>

const QChar PlayItemUtils::URL_PICKER_SEPARATOR = '#';

PlayItemUtils::PlayItemUtils()
{

}

QVector<PlayItem> PlayItemUtils::getPlayItemFromFileNames(const QStringList &list)
{
    QVector<PlayItem> result;

    for (int i = 0; i < list.count(); i++)
    {
        PlayItem item;
        QString path = list[i].trimmed();

        if (!path.isEmpty())
        {
            item.path = path;
            result.append(item);
        }
    }

    return result;
}

QVector<PlayItem> PlayItemUtils::getPlayItemFromURLPicker(const QVector<URLPickerInterface::Item> &list, bool isPlayList)
{
    QVector<PlayItem> result;

    for (int i = 0; i < list.count(); i++)
    {
        PlayItem item;
        const URLPickerInterface::Item &pickerItem = list[i];
        QString path = pickerItem.orgUrl.trimmed();
        QString title = pickerItem.title;

        if (!path.isEmpty())
        {
            item.path = path;
            item.extraData.query = pickerItem.query;
            item.extraData.userData = pickerItem.id.toLatin1() + QString(URL_PICKER_SEPARATOR).toLatin1() + path.toLatin1();

            if (title.isEmpty())
            {
                item.itemUpdated = false;
            }
            else
            {
                item.title = title;
                item.itemUpdated = true;
            }

            if (!isPlayList)
            {
                item.urlPickerIndex = 0;
                item.urlPickerData = list;
            }

            result.append(item);
        }

        if (!isPlayList)
            break;
    }

    return result;
}
