﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <QCoreApplication>

class MessageBoxUtils
{
    Q_DECLARE_TR_FUNCTIONS(MessageBoxUtils)
private:
    MessageBoxUtils();

public:
    static void criticalMessageBox(QWidget *parent, const QString &text);
    static void informationMessageBox(QWidget *parent, const QString &text);
    static bool questionMessageBox(QWidget *parent, const QString &text);
};
