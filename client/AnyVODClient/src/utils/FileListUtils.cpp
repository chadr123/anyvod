﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "FileListUtils.h"
#include "SeparatingUtils.h"
#include "MediaTypeUtils.h"

#include <QDir>
#include <QFileInfo>

FileListUtils::FileListUtils()
{

}

QStringList FileListUtils::getLocalFileList(const QString &path, const QStringList &extList, bool ignoreExtList)
{
    QFileInfo info(path);
    QStringList result;

    if (info.isDir())
    {
        QDir dir(path);
        QStringList filter = QString::fromUtf8("*.%1").arg(extList.join(" *.")).split(" ", Qt::SkipEmptyParts);
        QStringList fileList = dir.entryList(filter, QDir::NoDotAndDotDot | QDir::AllDirs | QDir::Files);
        QString dirPath = path;

        SeparatingUtils::appendDirSeparator(&dirPath);

        for (int i = 0; i < fileList.count(); i++)
        {
            QString filePath = dirPath + fileList[i];

            if (QFileInfo(filePath).isDir())
                result.append(FileListUtils::getLocalFileList(filePath, extList, ignoreExtList));
            else
                result.append(filePath);
        }
    }
    else
    {
        QFileInfo info(path);

        if (extList.contains(info.suffix().toLower()) || ignoreExtList)
            result.append(path);
    }

    return result;
}

QStringList FileListUtils::getOnlyMediaFileList(const QStringList &list)
{
    QStringList filterd;

    for (const QString &item : list)
    {
        if (MediaTypeUtils::isMediaExtension(QFileInfo(item).suffix()))
            filterd.append(item);
    }

    return filterd;
}

QStringList FileListUtils::getOnlyMediaLocalFileList(const QString &path, const QStringList &extList, bool ignoreExtList)
{
    QStringList result = FileListUtils::getLocalFileList(path, extList, ignoreExtList);

    return FileListUtils::getOnlyMediaFileList(result);
}
