﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "MacUtils.h"
#include "utils/RemoteFileUtils.h"

#include <QByteArray>
#include <QString>

#import <Foundation/Foundation.h>

#ifndef Q_OS_IOS
# import <AppKit/NSApplication.h>
#endif

MacUtils::MacUtils()
{

}

#ifndef Q_OS_IOS
void MacUtils::useAquaMode()
{
    NSApplication *app = [NSApplication sharedApplication];

    app.appearance = [NSAppearance appearanceNamed: NSAppearanceNameAqua];
}
#endif

QByteArray MacUtils::convertPathToFileSystemRepresentation(const QString &path)
{
    QByteArray fileSystemPath;

    if (RemoteFileUtils::determinRemoteProtocol(path))
    {
        fileSystemPath = path.toUtf8();
    }
    else
    {
        NSURL *url = [NSURL fileURLWithPath:(__bridge NSString*)path.toCFString()];

        fileSystemPath = QByteArray([url fileSystemRepresentation]);
    }

    return fileSystemPath;
}
