﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "DeviceUtils.h"
#include "device/CaptureInfo.h"
#include "device/DTVReader.h"
#include "device/RadioReader.h"

const QString DeviceUtils::CDIO_PROTOCOL = QString("libcdio:");

DeviceUtils::DeviceUtils()
{

}

QString DeviceUtils::getDevicePath(const QString &path)
{
    int index = path.indexOf(":");
    QString ret;

    if (index < 0)
        return ret;

    ret = path.mid(index + 1);

    return ret;
}

QString DeviceUtils::getDeviceType(const QString &path)
{
    int index = path.indexOf(":");
    QString ret;

    if (index < 0)
        return ret;

    ret = path.mid(0, index);

    return ret;
}

bool DeviceUtils::determinDevice(const QString &path)
{
    QString type = DeviceUtils::getDeviceType(path);

#if defined Q_OS_RASPBERRY_PI
    (void)type;

    return DTVReader::determinDTV(path) || RadioReader::determinRadio(path);
#else
    return type == CaptureInfo::TYPE || path.startsWith(DTVReader::DTV_PROTOCOL) || path.startsWith(RadioReader::RADIO_PROTOCOL);
#endif
}

bool DeviceUtils::determinIsDeviceHaveDuration(const QString &path)
{
    return path.startsWith(CDIO_PROTOCOL);
}
