﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "SeparatingUtils.h"
#include "device/DTVReader.h"
#include "device/RadioReader.h"
#include "utils/RemoteFileUtils.h"

#include <QString>

const QString SeparatingUtils::NETWORK_PATH_PREFIX = "//";

const QChar SeparatingUtils::WINDOWS_DIR_SEPARATOR = '\\';
const QChar SeparatingUtils::DIR_SEPARATOR = '/';
const QChar SeparatingUtils::FFMPEG_SEPARATOR = '#';

SeparatingUtils::SeparatingUtils()
{

}

void SeparatingUtils::appendDirSeparator(QString *path)
{
    if (path->length() == 0 || (*path)[path->length() - 1] != DIR_SEPARATOR)
        path->append(DIR_SEPARATOR);
}

QString SeparatingUtils::adjustNetworkPath(const QString &path)
{
    QString ret = path;

#ifdef Q_OS_WIN
    if (ret.startsWith(NETWORK_PATH_PREFIX))
        return ret.replace(DIR_SEPARATOR, WINDOWS_DIR_SEPARATOR);
#endif

    return ret;
}

QString SeparatingUtils::removeFFMpegSeparator(const QString &org)
{
    QString ret = org;

    if (ret.startsWith(RemoteFileUtils::ANYVOD_PROTOCOL) ||
        ret.startsWith(DTVReader::DTV_PROTOCOL) ||
        ret.startsWith(RadioReader::RADIO_PROTOCOL))
    {
        int ffmepgSepIndex = ret.lastIndexOf(FFMPEG_SEPARATOR);

        if (ffmepgSepIndex != -1)
            ret.remove(ffmepgSepIndex, ret.length() - ffmepgSepIndex);
    }

    return ret;
}
