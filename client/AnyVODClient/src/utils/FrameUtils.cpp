﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "FrameUtils.h"

FrameUtils::FrameUtils()
{

}

int FrameUtils::decodeFrame(AVCodecContext *ctx, AVPacket *packet, AVFrame *frame, int *decoded)
{
    int success = avcodec_send_packet(ctx, packet);

    if (success == AVERROR(EAGAIN))
    {
        int ret = avcodec_receive_frame(ctx, frame);

        if (ret < 0)
        {
            *decoded = 0;
            return ret;
        }
        else
        {
            *decoded = 1;

            success = avcodec_send_packet(ctx, packet);

            if (success < 0)
                return 0;
            else
                return packet->size;
        }
    }
    else if (success < 0)
    {
        *decoded = 0;
        return success;
    }

    success = avcodec_receive_frame(ctx, frame);

    if (success >= 0)
    {
        *decoded = 1;
        return packet->size;
    }
    else if (success == AVERROR(EAGAIN) || success == AVERROR_EOF)
    {
        *decoded = 0;
        return packet->size;
    }
    else
    {
        *decoded = 0;
        return success;
    }
}

int FrameUtils::encodeFrame(AVCodecContext *ctx, AVPacket *packet, AVFrame *frame, int *encoded)
{
    int success = avcodec_send_frame(ctx, frame);

    if (success == AVERROR(EAGAIN))
    {
        int ret = avcodec_receive_packet(ctx, packet);

        if (ret < 0)
        {
            *encoded = 0;
            return ret;
        }
        else
        {
            *encoded = 1;

            success = avcodec_send_frame(ctx, frame);

            if (success < 0)
                return 0;
            else
                return packet->size;
        }
    }
    else if (success < 0)
    {
        *encoded = 0;
        return success;
    }

    success = avcodec_receive_packet(ctx, packet);

    if (success >= 0)
    {
        *encoded = 1;
        return packet->size;
    }
    else if (success == AVERROR(EAGAIN) || success == AVERROR_EOF)
    {
        *encoded = 0;
        return packet->size;
    }
    else
    {
        *encoded = 0;
        return success;
    }
}
