﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "LRCParser.h"
#include "core/Common.h"
#include "core/Lyrics.h"
#include "core/TextEncodingDetector.h"
#include "utils/TimeUtils.h"
#include "utils/ConvertingUtils.h"

#include <limits>
#include <QFile>
#include <QMutexLocker>
#include <QTextStream>
#include <QTime>

using namespace std;

LRCParser::LRCParser() :
    m_curIndex(0),
    m_lastColor(Qt::black),
    m_genderExist(false)
{

}

bool LRCParser::open(const QString &path)
{
    this->close();

    QFile file(path);
    QString codecName;
    TextEncodingDetector detector;

    if (!detector.getCodecName(path, &codecName))
        return false;

    if (!file.open(QIODevice::ReadOnly))
        return false;

    this->parse(file, codecName);
    stable_sort(this->m_lines.begin(), this->m_lines.end());
    this->fillEndTime();

    return this->isExist();
}

bool LRCParser::save(const QString &path, double sync)
{
    QMutexLocker locker(&this->m_lock);
    QFile file(path);

    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return false;

    QTextStream stream(&file);

    stream.setCodec("UTF-8");
    stream.setGenerateByteOrderMark(true);

    for (int i = 0; i < this->m_header.count(); i++)
        stream << this->m_header[i] << Qt::endl;

    if (!this->m_header.isEmpty())
        stream << Qt::endl;

    for (int i = 0; i < this->m_lines.count(); i++)
    {
        const Line &item = this->m_lines[i];
        QTime time = QTime::fromMSecsSinceStartOfDay(item.startTime + int(sync * 1000));
        QStringList texts = item.subLines[0].text.split(NEW_LINE);

        for (int j = 0; j < texts.count(); j++)
        {
            const QString &text = texts[j];

            stream << "[";

            if (text.isEmpty())
                stream << "00:00.000";
            else
                stream << time.toString(ConvertingUtils::TIME_MM_SS_Z);

            stream << "]";

            if (item.color == Qt::red)
                stream << "F: ";
            else if (item.color == Qt::blue)
                stream << "M: ";
            else if (item.color == Qt::magenta)
                stream << "D: ";

            stream << text << Qt::endl;
        }

        if (stream.status() != QTextStream::Ok)
            return false;
    }

    stream.flush();

    if (stream.status() != QTextStream::Ok)
        return false;

    return true;
}

void LRCParser::close()
{
    QMutexLocker locker(&this->m_lock);

    this->m_lines.clear();
    this->m_genderExist = false;
    this->m_lastColor = Qt::black;
    this->m_header.clear();
}

bool LRCParser::isGenderExist() const
{
    return this->m_genderExist;
}

bool LRCParser::get(int32_t millisecond, Lyrics *ret)
{
    QMutexLocker locker(&this->m_lock);

    return this->find(millisecond, ret) >= 0;
}

bool LRCParser::getNext(int32_t millisecond, int step, Lyrics *ret)
{
    QMutexLocker locker(&this->m_lock);
    int index = this->find(millisecond, ret);

    if (index >= 0)
    {
        int next = index + step;

        if (next > 0 && next < this->m_lines.count())
        {
            this->convert(this->m_lines[next], ret);
            return true;
        }
    }

    return false;
}

void LRCParser::convert(const Line &line, Lyrics *ret) const
{
    ret->color = line.color;
    ret->text = line.subLines[0].text;
}

void LRCParser::fillEndTime()
{
    int count = this->m_lines.count();

    if (count <= 0)
        return;

    for (int i = 0; i < count - 1; i++)
        this->m_lines[i].endTime = this->m_lines[i + 1].startTime;

    this->m_lines.last().endTime = numeric_limits<int32_t>::max();
}

int LRCParser::find(int32_t millisecond, Lyrics *ret)
{
    int count = this->m_lines.count();
    int oldIndex = this->m_curIndex;
    int findIndex = -1;

    if (count > 0)
    {
        for (int i = 0; i < count; i++)
        {
            const Line &line = this->m_lines[this->m_curIndex];

            if (line.startTime <= millisecond && millisecond < line.endTime)
            {
                this->convert(line, ret);
                findIndex = this->m_curIndex;

                break;
            }
            else
            {
                this->m_curIndex++;

                if (this->m_curIndex >= count)
                    this->m_curIndex = 0;
            }
        }

        this->m_curIndex = oldIndex;
    }

    return findIndex;
}

bool LRCParser::isExist()
{
    QMutexLocker locker(&this->m_lock);

    return this->m_lines.count() > 0;
}

void LRCParser::parse(QFile &file, const QString &codecName)
{
    QMutexLocker locker(&this->m_lock);
    QTextStream stream(&file);

    stream.setCodec(codecName.toLatin1());

    while (true)
    {
        QString line = stream.readLine();

        line = line.trimmed();

        if (line.isNull())
            break;

        if (line.count() == 0)
            continue;

        this->parseLine(line);
    }
}

void LRCParser::parseLine(const QString &line)
{
    if (line[0] != '[')
        return;

    bool ok = false;

    line.midRef(1, 2).toInt(&ok);

    if (ok)
    {
        Line lyrics;
        Line *prevLyrics = nullptr;

        this->parseTime(line, &lyrics);

        if (this->m_lines.count() > 0)
            prevLyrics = &this->m_lines.last();

        if (prevLyrics && (prevLyrics->startTime == lyrics.startTime || lyrics.startTime == 0))
        {
            prevLyrics->subLines[0].text.append(NEW_LINE);
            prevLyrics->subLines[0].text.append(lyrics.subLines[0].text);
        }
        else
        {
            this->m_lines.append(lyrics);
        }
    }
    else
    {
        this->m_header.append(line);
    }
}

void LRCParser::parseTime(const QString &line, Line *ret)
{
    int index = line.indexOf(']');

    if (index < 0)
        return;

    QString time = line.mid(1, index - 1);

    ret->startTime = -QTime::fromString(time, ConvertingUtils::TIME_MM_SS_Z).msecsTo(TimeUtils::ZERO_TIME);

    this->parseGender(line.mid(index + 1).trimmed(), ret);
}

void LRCParser::parseGender(const QString &line, Line *ret)
{
    int index = line.indexOf(':');

    if (index > 0)
    {
        QString gender = line.mid(0, index).toLower();

        this->m_genderExist = true;

        if (gender == "f")
        {
            ret->color = Qt::red;
        }
        else if (gender == "m")
        {
            ret->color = Qt::blue;
        }
        else if (gender == "d")
        {
            ret->color = Qt::magenta;
        }
        else
        {
            ret->color = Qt::black;
            this->m_genderExist = false;
            index = -1;
        }

        this->m_lastColor = ret->color;
    }
    else
    {
        if (this->m_genderExist)
            ret->color = this->m_lastColor;
        else
            ret->color = Qt::black;
    }

    this->parseLyrics(line.mid(index + 1).trimmed(), ret);
}

void LRCParser::parseLyrics(const QString &lyrics, Line *ret) const
{
    SubLine subline;

    subline.text = lyrics.trimmed();

    ret->subLines.append(subline);
}
