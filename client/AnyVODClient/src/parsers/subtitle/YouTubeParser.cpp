﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "YouTubeParser.h"
#include "net/HttpDownloader.h"
#include "pickers/YouTubeURLPicker.h"
#include "language/GlobalLanguage.h"
#include "utils/PlayItemUtils.h"
#include "utils/SeparatingUtils.h"

#include <QTemporaryFile>
#include <QXmlStreamReader>
#include <QBuffer>
#include <QUrlQuery>
#include <QDir>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QDebug>

const QString YouTubeParser::SUBTITLE_INFO_URL = QString("https://video.google.com/timedtext?hl=%1&type=list&v=%2");
const QString YouTubeParser::SUBTITLE_URL = "https://www.youtube.com/api/timedtext?";

YouTubeParser::YouTubeParser()
{

}

YouTubeParser::~YouTubeParser()
{
    this->close();
}

bool YouTubeParser::open(const QString &path)
{
    bool ok = false;
    YouTubeURLPicker picker;
    QStringList list = path.split(PlayItemUtils::URL_PICKER_SEPARATOR, Qt::SkipEmptyParts);

    if (list.length() != 2)
        return false;

    this->close();

    ok = picker.isSupported(list.last());

    if (!ok)
        return false;

    QString id = list.first();

    ok = this->getManualSubtitles(id);

    if (!ok)
    {
        this->close();
        ok = this->getAutoSubtitles(id);
    }

    if (ok)
        this->setDefaultLanguage(this->m_curLanguage);

    return this->isExist();
}

void YouTubeParser::close()
{
    QMutexLocker locker(&this->m_lock);

    for (const Item &item : qAsConst(this->m_subtitles))
    {
        if (item.parser)
        {
            item.parser->close();
            delete item.parser;
        }
    }

    this->m_subtitles.clear();
    this->m_curLanguage.clear();
}

bool YouTubeParser::save(const QString &path, double sync)
{
    QMutexLocker locker(&this->m_lock);

    if (this->m_curLanguage.isEmpty())
        return false;

    if (this->m_subtitles.contains(this->m_curLanguage))
    {
        Item item = this->m_subtitles[this->m_curLanguage];

        if (item.parser)
            return item.parser->save(path, sync);
    }

    return false;
}

bool YouTubeParser::get(int32_t millisecond, QVector<SRTParser::Item> *ret)
{
    QMutexLocker locker(&this->m_lock);

    if (this->m_curLanguage.isEmpty())
        return false;

    if (this->m_subtitles.contains(this->m_curLanguage))
    {
        Item item = this->m_subtitles[this->m_curLanguage];

        if (item.parser)
            return item.parser->get(millisecond, ret);
    }

    return false;
}

bool YouTubeParser::isExist()
{
    QMutexLocker locker(&this->m_lock);

    return !this->m_subtitles.isEmpty();
}

void YouTubeParser::getDefaultLanguage(QString *ret)
{
    QMutexLocker locker(&this->m_lock);

    *ret = this->m_curLanguage;
}

void YouTubeParser::setDefaultLanguage(const QString &lang)
{
    QMutexLocker locker(&this->m_lock);
    QBuffer subtitle;
    HttpDownloader d;

    if (lang.isEmpty())
        return;

    Item item = this->m_subtitles[lang];

    this->m_curLanguage = lang;

    if (!item.parser)
    {
        if (!d.download(item.url, subtitle))
            return;

        item.parser = this->getSRTParser(subtitle);

        if (item.parser)
            this->m_subtitles[lang] = item;
    }
}

void YouTubeParser::getLanguages(QStringList *ret)
{
    QMutexLocker locker(&this->m_lock);

    *ret = this->m_subtitles.keys();
}

bool YouTubeParser::getTTSUrl(const QString &content, QString *url, QString *listUrl, QString *timestamp) const
{
    QString ret;
    QRegularExpression exp;
    QRegularExpressionMatch match;
    QStringList patterns;

    patterns.append(R"(["']?TTS_URL["']?\s*:\s*["']?([\w\d\/\\\.\-:%&\?=]+)["']?,?)");
    patterns.append(R"(["']?ttsurl["']?\s*:\s*["']?([\w\d\/\\\.\-:%&\?=]+)["']?,?)");

    for (const QString &pattern : patterns)
    {
        exp.setPattern(pattern);
        match = exp.match(content);

        if (!match.hasMatch())
            continue;

        ret = match.captured(1);

        break;
    }

    if (ret.isEmpty())
        return false;

    ret = ret.replace("\\/", "/").replace("\\u0026", "&");

    *url = ret;

    ret += "&type=list&tlangs=1&asrs=1";

    *listUrl = ret;

    exp.setPattern(R"d("timestamp"\s*:\s*"?(\d+)"?,?)d");
    match = exp.match(content);

    if (!match.hasMatch())
        return false;

    ret = match.captured(1);

    *timestamp = ret;

    return true;
}

bool YouTubeParser::getAutoSubtitles(const QString &id)
{
    HttpDownloader d;
    QBuffer data;

    if (!d.download(YouTubeURLPicker::BASE_URL + id, data))
        return false;

    QString content(data.buffer());

    if (this->getTTSSubtitlesFromTTSURL(content))
        return true;

    if (this->getTTSSubtitlesFromJson(content, id))
        return true;

    return false;
}

bool YouTubeParser::getTTSSubtitlesFromTTSURL(const QString &content)
{
    //https://www.youtube.com/watch?v=llM9MIM_9U4
    HttpDownloader d;
    QBuffer data;
    QXmlStreamReader xml;
    QString url;
    QString listUrl;
    QString timestamp;

    if (!this->getTTSUrl(content, &url, &listUrl, &timestamp))
        return false;

    if (!data.open(QIODevice::ReadWrite))
        return false;

    if (!d.download(listUrl, data))
        return false;

    if (!data.reset())
        return false;

    xml.setDevice(&data);

    //<target id="9" urlfrag="&lang=ko" lang_code="ko" lang_original="한국어" lang_translated="한국어" lang_default="true"/>
    while (!xml.atEnd())
    {
        if (!xml.readNextStartElement())
            continue;

        QString elem = xml.name().toString();

        if (elem != "track")
            continue;

        QXmlStreamAttributes attr = xml.attributes();
        QString langCode = attr.value("lang_code").toString();
        QString kind = attr.value("kind").toString();

        while (!xml.atEnd())
        {
            if (!xml.readNextStartElement())
                continue;

            elem = xml.name().toString();

            if (elem != "target")
                continue;

            QXmlStreamAttributes attrSub = xml.attributes();
            QString langSubCode = attrSub.value("lang_code").toString();
            QString langOriginal = attrSub.value("lang_original").toString();
            QString langDefault = attrSub.value("lang_default").toString();
            QUrlQuery query;

            if (this->m_subtitles.contains(langOriginal))
                continue;

            query.addQueryItem("lang", langCode);
            query.addQueryItem("tlang", langSubCode);
            query.addQueryItem("fmt", "vtt");
            query.addQueryItem("ts", timestamp);
            query.addQueryItem("kind", kind);

            Item item;

            item.url = url + "&" + query.query();
            item.code = langSubCode;
            item.parser = nullptr;

            this->m_subtitles[langOriginal] = item;

            if (langDefault.toLower() == "true")
                this->m_curLanguage = langOriginal;
        }
    }

    data.close();

    return this->isExist();
}

bool YouTubeParser::getTTSSubtitlesFromJson(const QString &content, const QString &id)
{
    YouTubeURLPicker picker;
    QJsonObject json;
    QJsonObject args;
    QJsonDocument doc;

    if (!picker.getJSon(content, id, &json))
        return false;

    args = json["args"].toObject();

    doc = QJsonDocument::fromJson(args["player_response"].toString().toUtf8());

    if (doc.isObject())
    {
        //https://www.youtube.com/watch?v=XnNymEksrTw
        QJsonObject playerResponse = doc.object();
        QJsonObject captions = playerResponse["captions"].toObject();
        QJsonObject renderer = captions["playerCaptionsTracklistRenderer"].toObject();
        QJsonArray tracks = renderer["captionTracks"].toArray();

        if (tracks.isEmpty())
            return false;

        QJsonObject track = tracks[0].toObject();
        QString baseUrl = track["baseUrl"].toString();
        const QVariantList langs = renderer["translationLanguages"].toArray().toVariantList();
        QString currentLangCode = track["languageCode"].toString();

        for (const QVariant &var : langs)
        {
            QVariantMap map = var.toMap();
            QString code = map["languageCode"].toString();
            QString name = map["languageName"].toMap()["simpleText"].toString();
            bool nextItem = false;

            for (const Item &existItem : qAsConst(this->m_subtitles))
            {
                nextItem = existItem.code == code;

                if (nextItem)
                    break;
            }

            if (nextItem)
                continue;

            if (currentLangCode == code)
                continue;

            QUrlQuery query;

            query.addQueryItem("tlang", code);
            query.addQueryItem("fmt", "vtt");

            Item item;

            item.url = baseUrl + "&" + query.query();
            item.code = code;
            item.parser = nullptr;

            this->m_subtitles[name] = item;
        }

        QString localCode = QLocale::system().name().split("_").first();
        const auto keys = this->m_subtitles.keys();

        for (const QString &key : keys)
        {
            const Item &existItem = this->m_subtitles[key];

            if (existItem.code == localCode)
            {
                this->m_curLanguage = key;
                break;
            }
        }
    }

    return this->isExist();
}

bool YouTubeParser::getManualSubtitles(const QString &id)
{
    //https://www.youtube.com/watch?v=XJGiS83eQLk
    HttpDownloader d;
    QBuffer data;
    QXmlStreamReader xml;
    QString lang = GlobalLanguage::getInstance().getLanguage();
    QString url = SUBTITLE_INFO_URL.arg(lang.isEmpty() ? "en" : lang, id);

    if (!data.open(QIODevice::ReadWrite))
        return false;

    if (!d.download(url, data))
        return false;

    if (!data.reset())
        return false;

    xml.setDevice(&data);

    //<track id="0" name="" lang_code="en" lang_original="English" lang_translated="English" lang_default="true"/>
    while (!xml.atEnd())
    {
        if (!xml.readNextStartElement())
            continue;

        QString elem = xml.name().toString();

        if (elem != "track")
            continue;

        QXmlStreamAttributes attr = xml.attributes();
        QString name = attr.value("name").toString();
        QString langCode = attr.value("lang_code").toString();
        QString langOriginal = attr.value("lang_original").toString();
        QString langDefault = attr.value("lang_default").toString();
        QUrlQuery query;

        if (this->m_subtitles.contains(langOriginal))
            continue;

        query.addQueryItem("lang", langCode);
        query.addQueryItem("v", id);
        query.addQueryItem("fmt", "vtt");
        query.addQueryItem("name", name);

        Item item;

        item.url = SUBTITLE_URL + query.query();
        item.code = langCode;
        item.parser = nullptr;

        this->m_subtitles[langOriginal] = item;

        if (langDefault.toLower() == "true")
            this->m_curLanguage = langOriginal;
    }

    data.close();

    return this->isExist();
}

SRTParser* YouTubeParser::getSRTParser(QBuffer &subtitle) const
{
    QString tmpFilePath = QDir::tempPath();

    SeparatingUtils::appendDirSeparator(&tmpFilePath);
    tmpFilePath += "XXXXXX";
    tmpFilePath += ".srt";

    QTemporaryFile tmpFile(tmpFilePath);

    if (!tmpFile.open())
        return nullptr;

    QFile file(tmpFile.fileName());

    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return nullptr;

    QTextStream stream(&file);

    stream.setCodec("UTF-8");
    stream.setGenerateByteOrderMark(true);

    if (!subtitle.open(QIODevice::ReadWrite))
        return nullptr;

    stream << subtitle.buffer();

    subtitle.close();
    file.close();
    tmpFile.close();

    SRTParser *parser = new SRTParser();

    if (!parser->open(tmpFile.fileName()))
    {
        delete parser;
        return nullptr;
    }

    return parser;
}
