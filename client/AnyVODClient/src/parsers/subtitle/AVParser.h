﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "ASSParser.h"
#include "core/StreamInfo.h"
#include "media/MediaState.h"

#include <stdint.h>
#include <QVector>
#include <QHash>
#include <QSize>

class QString;

struct AVSubtitle;
struct AVCodecContext;

class AVParser
{
public:
    AVParser();

    bool open(const QString &path, const QString &fontPath, const QString &fontFamily, int index);
    void close();

    void setFrameSize(const QSize &size);

    bool get(const uint32_t millisecond, AVSubtitle **ret);
    bool getASSImage(const uint32_t millisecond, ASS_Image **ret, bool *changed);

    void getStreamInfos(QVector<SubtitleStreamInfo> *ret) const;

    unsigned int getCurrentIndex() const;
    bool setCurrentIndex(unsigned int index);

    bool getCurrentName(QString *ret) const;
    bool getDesc(QString *ret) const;
    bool getSize(QSize *ret) const;

    bool isExist();

private:
    void fillStreamInfos(const AVFormatContext *format);
    bool fillItems(AVFormatContext *format, int index);

    void closeInternal();

    void adjustSubtitleTimes();
    bool rebuildSubtitleTime(const SubtitleElement &second, SubtitleElement *first) const;

    void closeStream(AVFormatContext *format);
    void addToASSParser(unsigned int index);

private:
    QVector<SubtitleStreamInfo> m_streamInfos;
    QVector<SubtitleElement> m_items;
    ASSParser m_assParser;
    QString m_assHeader;
    QSize m_frameSize;
    QString m_filePath;
    QString m_fontPath;
    QString m_fontFamily;
    bool m_isUTF8;
    unsigned int m_curIndex;
    int m_searchIndex;
};
