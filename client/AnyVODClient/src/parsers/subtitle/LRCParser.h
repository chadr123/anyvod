﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <stdint.h>
#include <QMutex>
#include <QVector>
#include <QColor>

class QFile;
struct Lyrics;

class LRCParser
{
public:
    LRCParser();

    bool open(const QString &path);
    bool save(const QString &path, double sync);
    void close();

    bool get(int32_t millisecond, Lyrics *ret);
    bool getNext(int32_t millisecond, int step, Lyrics *ret);

    bool isExist();
    bool isGenderExist() const;

private:
    struct SubLine
    {
        int32_t time;
        QString text;
    };

    struct Line
    {
        int32_t startTime;
        int32_t endTime;
        QColor color;
        QVector<SubLine> subLines;

        bool operator < (const Line &rhs) const
        {
            return startTime < rhs.startTime;
        }
    };

private:
    void parse(QFile &file, const QString &codecName);
    void parseLine(const QString &line);
    void parseTime(const QString &line, Line *ret);
    void parseGender(const QString &line, Line *ret);
    void parseLyrics(const QString &lyrics, Line *ret) const;

    void fillEndTime();
    int find(int32_t millisecond, Lyrics *ret);
    void convert(const Line &line, Lyrics *ret) const;

private:
    int m_curIndex;
    QVector<Line> m_lines;
    QMutex m_lock;
    QColor m_lastColor;
    bool m_genderExist;
    QStringList m_header;
};
