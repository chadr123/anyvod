﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "AVParser.h"
#include "core/TextEncodingDetector.h"
#include "utils/PathUtils.h"

#include <QString>
#include <QDebug>

extern "C"
{
# include <libavcodec/avcodec.h>
# include <libavformat/avformat.h>

#if defined Q_OS_MOBILE
    int avio_feof(AVIOContext *s);
#endif
}

AVParser::AVParser() :
    m_isUTF8(false),
    m_curIndex(0),
    m_searchIndex(0)
{

}

bool AVParser::open(const QString &path, const QString &fontPath, const QString &fontFamily, int index)
{
    AVFormatContext *format = nullptr;
    QString codecName;
    TextEncodingDetector detector;

    if (!detector.getCodecName(path, &codecName))
        return false;

    this->m_isUTF8 = codecName.toLower() == "utf-8";

    if (avformat_open_input(&format, PathUtils::convertPathToFileSystemRepresentation(path, false), nullptr, nullptr) != 0)
        return false;

    this->m_curIndex = index;

    if (avformat_find_stream_info(format, nullptr) < 0)
    {
        this->close();
        this->closeStream(format);

        return false;
    }

    this->fillStreamInfos(format);

    if (!this->fillItems(format, this->m_curIndex))
    {
        this->close();
        this->closeStream(format);

        return false;
    }

    this->adjustSubtitleTimes();
    this->closeStream(format);

    this->m_assParser.init();
    this->m_assParser.setFontPath(fontPath);
    this->m_assParser.setDefaultFont(fontFamily);
    this->m_assParser.setFrameSize(this->m_frameSize.width(), this->m_frameSize.height());

    this->addToASSParser(this->m_curIndex);

    this->m_filePath = path;
    this->m_fontPath = fontPath;
    this->m_fontFamily = fontFamily;

    return this->isExist();
}

void AVParser::closeStream(AVFormatContext *format)
{
    if (format)
        avformat_close_input(&format);
}

void AVParser::close()
{
    this->closeInternal();

    this->m_filePath.clear();
    this->m_fontPath.clear();
    this->m_fontFamily.clear();
}

void AVParser::setFrameSize(const QSize &size)
{
    this->m_frameSize = size;
    this->m_assParser.setFrameSize(this->m_frameSize.width(), this->m_frameSize.height());
}

bool AVParser::get(const uint32_t millisecond, AVSubtitle **ret)
{
    int oldIndex = this->m_searchIndex;
    int count = this->m_items.count();

    for (int i = 0; i < count; i++)
    {
        SubtitleElement &elem = this->m_items[this->m_searchIndex];
        AVSubtitle &spItem = elem.subtitle;
        uint64_t base = (uint64_t)(elem.pts * 1000);

        if (base + spItem.start_display_time <= millisecond && millisecond <= base + spItem.end_display_time)
        {
            *ret = &spItem;
            return true;
        }

        this->m_searchIndex++;

        if (this->m_searchIndex >= count)
            this->m_searchIndex = 0;
    }

    this->m_searchIndex = oldIndex;

    return false;
}

bool AVParser::getASSImage(const uint32_t millisecond, ASS_Image **ret, bool *changed)
{
    return this->m_assParser.getSingle(millisecond, ret, changed);
}

void AVParser::getStreamInfos(QVector<SubtitleStreamInfo> *ret) const
{
    *ret = this->m_streamInfos;
}

unsigned int AVParser::getCurrentIndex() const
{
    return this->m_curIndex;
}

bool AVParser::setCurrentIndex(unsigned int index)
{
    if (index < (unsigned int)this->m_streamInfos.count())
    {
        if (this->m_curIndex != index)
        {
            this->closeInternal();
            return this->open(this->m_filePath, this->m_fontPath, this->m_fontFamily, index);
        }

        return true;
    }

    return false;
}

bool AVParser::getCurrentName(QString *ret) const
{
    if (this->m_curIndex < (unsigned int)this->m_streamInfos.count())
    {
        *ret = this->m_streamInfos[this->m_curIndex].name;
        return true;
    }

    return false;
}

bool AVParser::getDesc(QString *ret) const
{
    if (this->m_curIndex < (unsigned int)this->m_streamInfos.count())
    {
        *ret = this->m_streamInfos[this->m_curIndex].desc;
        return true;
    }

    return false;
}

bool AVParser::getSize(QSize *ret) const
{
    if (this->m_curIndex < (unsigned int)this->m_streamInfos.count())
    {
        *ret = this->m_streamInfos[this->m_curIndex].size;
        return true;
    }

    return false;
}

bool AVParser::isExist()
{
    return this->m_items.count() > 0;
}

void AVParser::addToASSParser(unsigned int index)
{
    if (index < (unsigned int)this->m_streamInfos.count())
    {
        this->m_assParser.setHeader(this->m_assHeader);

        for (int i = 0; i < this->m_items.count(); i++)
        {
            AVSubtitle &sub = this->m_items[i].subtitle;

            for (unsigned int j = 0; j < sub.num_rects; j++)
            {
                AVSubtitleRect *rect = sub.rects[j];

                if (rect->type == SUBTITLE_ASS)
                {
                    QString ass;

                    if (this->m_isUTF8)
                        ass = QString::fromUtf8(rect->ass);
                    else
                        ass = QString::fromLocal8Bit(rect->ass);

                    this->m_assParser.parseSingle(ass.toUtf8());
                }
            }
        }
    }
}

void AVParser::adjustSubtitleTimes()
{
    for (int i = 0; i < this->m_items.count() - 1; i++)
        this->rebuildSubtitleTime(this->m_items[i + 1], &this->m_items[i]);

    if (this->m_items.count() > 0)
    {
        SubtitleElement &last = this->m_items.last();

        if (last.subtitle.start_display_time == last.subtitle.end_display_time)
            last.subtitle.end_display_time = 5000;
    }
}

bool AVParser::fillItems(AVFormatContext *format, int index)
{
    AVCodecContext *context = avcodec_alloc_context3(nullptr);

    if (avcodec_parameters_to_context(context, format->streams[index]->codecpar) < 0)
    {
        avcodec_free_context(&context);
        return false;
    }

    context->pkt_timebase = format->streams[index]->time_base;

    AVCodec *codec = avcodec_find_decoder(context->codec_id);

    if (avcodec_open2(context, codec, nullptr) < 0)
    {
        avcodec_free_context(&context);
        return false;
    }

    if (context->subtitle_header_size)
        this->m_assHeader = QString::fromUtf8((char*)context->subtitle_header, context->subtitle_header_size);

    AVPacket *packet = av_packet_alloc();

    while (true)
    {
        int ret = av_read_frame(format, packet);

        if (ret < 0)
        {
            if (ret == AVERROR_EOF || avio_feof(format->pb))
            {
                break;
            }
            else
            {
                if (AVERROR(EAGAIN) != ret)
                {
                    avcodec_free_context(&context);
                    return false;
                }
            }
        }

        double pts = 0.0;
        SubtitleElement sub;
        int decoded = 0;
        int success = 0;

        if (index == packet->stream_index)
        {
            AVStream *stream = format->streams[packet->stream_index];

            if (packet->pts != AV_NOPTS_VALUE)
                pts = av_q2d(stream->time_base) * packet->pts;

            sub.pts = pts;

            success = avcodec_decode_subtitle2(context, &sub.subtitle, &decoded, packet);

            if (success && decoded)
                this->m_items.append(sub);
        }

        av_packet_unref(packet);
    }

    av_packet_free(&packet);
    avcodec_free_context(&context);

    return true;
}

void AVParser::closeInternal()
{
    this->m_searchIndex = 0;
    this->m_curIndex = 0;
    this->m_streamInfos.clear();
    this->m_assHeader.clear();

    for (int i = 0; i < this->m_items.count(); i++)
        avsubtitle_free(&this->m_items[i].subtitle);

    this->m_items.clear();
    this->m_assParser.deInit();
}

void AVParser::fillStreamInfos(const AVFormatContext *format)
{
    int subtitleNum = 1;

    for (unsigned int i = 0; i < format->nb_streams; i++)
    {
        AVCodecParameters *context = format->streams[i]->codecpar;
        AVDictionary *meta = format->streams[i]->metadata;
        AVDictionaryEntry *entry;
        QString desc;
        QString lang;

        if (context->codec_type != AVMEDIA_TYPE_SUBTITLE)
            continue;

        entry = av_dict_get(meta, "language", nullptr, AV_DICT_IGNORE_SUFFIX);

        if (entry)
            lang = QString::fromLocal8Bit(entry->value).toUpper();

        AVCodec *codec = avcodec_find_decoder(context->codec_id);

        if (codec)
            desc = codec->long_name;

        if (!lang.isEmpty())
            desc = lang + ", " + desc;

        SubtitleStreamInfo info;

        info.index = i;
        info.name = QString("%1 (%2)").arg(desc).arg(subtitleNum);

        if (codec && codec->long_name)
            info.desc = codec->long_name;

        info.size = QSize(context->width, context->height);

        subtitleNum++;

        this->m_streamInfos.append(info);
    }
}

bool AVParser::rebuildSubtitleTime(const SubtitleElement &second, SubtitleElement *first) const
{
    if (first->subtitle.start_display_time == first->subtitle.end_display_time)
    {
        double diff = second.pts - first->pts;

        if (diff < 0.0)
            return false;

        first->subtitle.end_display_time = diff * 1000.0;
    }

    return true;
}
