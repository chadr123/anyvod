﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "ASSParser.h"
#include "core/TextEncodingDetector.h"

#include <QMutexLocker>
#include <QFile>
#include <QTextStream>
#include <QFileInfo>
#include <QDebug>

ASSParser::ASSParser() :
    m_lock(QMutex::Recursive),
    m_init(false),
    m_lib(nullptr),
    m_renderer(nullptr),
    m_singleTrack(nullptr),
    m_fileTrack(nullptr)
{

}

void ASSParser::messageCallback(int, const char *, va_list, void *)
{

}

void ASSParser::init()
{
    QMutexLocker locker(&this->m_lock);

    this->deInit();

    this->m_lib = ass_library_init();
    this->m_renderer = ass_renderer_init(this->m_lib);
    this->m_singleTrack = ass_new_track(this->m_lib);

    ass_set_extract_fonts(this->m_lib, 1);
    ass_set_message_cb(this->m_lib, ASSParser::messageCallback, nullptr);
    ass_set_shaper(this->m_renderer, ASS_SHAPING_SIMPLE);

    this->m_init = true;
}

void ASSParser::deInit()
{
    QMutexLocker locker(&this->m_lock);

    if (this->m_singleTrack)
    {
        ass_free_track(this->m_singleTrack);
        this->m_singleTrack = nullptr;
    }

    if (this->m_renderer)
    {
        ass_renderer_done(this->m_renderer);
        this->m_renderer = nullptr;
    }

    if (this->m_lib)
    {
        ass_library_done(this->m_lib);
        this->m_lib = nullptr;
    }

    this->m_init = false;
}

bool ASSParser::open(const QString &path)
{
    QMutexLocker locker(&this->m_lock);
    QFile file(path);
    QString codecName;
    TextEncodingDetector detector;

    this->close();

    if (!detector.getCodecName(path, &codecName))
        return false;

    if (!file.open(QIODevice::ReadOnly))
        return false;

    QTextStream stream(&file);
    QByteArray contents;

    stream.setCodec(codecName.toLatin1());
    contents = stream.readAll().toUtf8();

    if (!this->m_init)
        this->init();

    this->m_fileTrack = ass_read_memory(this->m_lib, contents.data(), contents.size(), nullptr);

    return this->m_fileTrack != nullptr;
}

void ASSParser::close()
{
    QMutexLocker locker(&this->m_lock);

    if (this->m_fileTrack)
    {
        ass_free_track(this->m_fileTrack);
        this->m_fileTrack = nullptr;
    }

    if (!this->m_singleTrack)
        this->clearFonts();
}

void ASSParser::clearFonts()
{
    QMutexLocker locker(&this->m_lock);

    if (this->m_lib)
        ass_clear_fonts(this->m_lib);
}

void ASSParser::setHeader(const QString &header)
{
    QMutexLocker locker(&this->m_lock);
    QByteArray content = header.toUtf8();

    if (!this->m_singleTrack)
        return;

    this->freeStyle(this->m_singleTrack);

    ass_flush_events(this->m_singleTrack);
    ass_process_codec_private(this->m_singleTrack, content.data(), content.size());

    this->m_singleLines.clear();
}

void ASSParser::setFrameSize(const int width, const int height)
{
    QMutexLocker locker(&this->m_lock);

    if (!this->m_renderer)
        return;

    ass_set_frame_size(this->m_renderer, width, height);
}

void ASSParser::setDefaultFont(const QString &family)
{
    QMutexLocker locker(&this->m_lock);
    QFileInfo info(this->m_fontPath);

    if (!this->m_lib || !this->m_renderer)
        return;

    ass_set_fonts_dir(this->m_lib, info.absolutePath().toUtf8());
    ass_set_fonts(this->m_renderer, info.filePath().toUtf8(), family.toUtf8(), ASS_FONTPROVIDER_AUTODETECT, nullptr, 1);
}

void ASSParser::setFontPath(const QString &path)
{
    QMutexLocker locker(&this->m_lock);

    this->m_fontPath = path;
}

void ASSParser::getFontPath(QString *ret)
{
    QMutexLocker locker(&this->m_lock);

    *ret = this->m_fontPath;
}

void ASSParser::addFont(const QString &name, uint8_t *data, int size)
{
    QMutexLocker locker(&this->m_lock);

    if (!this->m_lib)
        return;

    ass_add_font(this->m_lib, name.toUtf8().data(), (const char*)data, size);
}

bool ASSParser::get(const int32_t millisecond, ASS_Image **ret, bool *changed)
{
    return this->getImage(this->m_fileTrack, millisecond, ret, changed);
}

void ASSParser::parseSingle(const char *ass)
{
    QMutexLocker locker(&this->m_lock);
    QString dialogue = QString::fromUtf8(ass);

    if (!this->m_singleTrack)
        return;

    if (this->m_singleLines.find(dialogue) == this->m_singleLines.end())
    {
        QByteArray content = dialogue.toUtf8();

        ass_process_data(this->m_singleTrack, content.data(), content.size());
        this->m_singleLines.insert(dialogue, dialogue);
    }
}

bool ASSParser::getSingle(const int32_t millisecond, ASS_Image **ret, bool *changed)
{
    return this->getImage(this->m_singleTrack, millisecond, ret, changed);
}

bool ASSParser::getImage(ASS_Track *track, const int32_t millisecond, ASS_Image **ret, bool *changed)
{
    QMutexLocker locker(&this->m_lock);
    int detectChange = 0;

    if (!this->m_renderer)
        return false;

    *ret = ass_render_frame(this->m_renderer, track, millisecond, &detectChange);
    *changed = detectChange != 0;

    return *ret != nullptr;
}

void ASSParser::freeStyle(ASS_Track *track)
{
    for (int i = 0; i < track->n_styles; i++)
        ass_free_style(track, i);

    track->n_styles = 0;
}

bool ASSParser::isExist()
{
    QMutexLocker locker(&this->m_lock);

    return this->m_fileTrack != nullptr;
}
