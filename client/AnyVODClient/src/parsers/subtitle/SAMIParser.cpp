﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "SAMIParser.h"
#include "core/Common.h"
#include "core/TextEncodingDetector.h"

#include <limits>
#include <QRegularExpression>
#include <QTextStream>
#include <QMap>
#include <QMutexLocker>
#include <QFile>
#include <QDebug>

using namespace std;

SAMIParser::SAMIParser() :
    m_curIndex(0),
    m_processNewLine(false),
    m_lock(QMutex::Recursive)
{

}

bool SAMIParser::open(const QString &path)
{
    QFile file(path);
    QString codecName;
    TextEncodingDetector detector;

    if (!detector.getCodecName(path, &codecName))
        return false;

    return this->open(file, codecName);
}

bool SAMIParser::open(QIODevice &data, const QString &codecName)
{
    this->close();

    if (!data.open(QIODevice::ReadOnly))
        return false;

    this->parse(data, codecName);
    this->determinDefaultClassName();
    this->fillEndTime();

    return this->isExist();
}

void SAMIParser::addTimeToContent(const QString &content, int sync, QString *ret)
{
    QString result = content;
    int pos = 0;

    while (true)
    {
        int endPos = 0;
        int attStart = 0;

        pos = result.indexOf("<SYNC", pos, Qt::CaseInsensitive);

        if (pos == -1)
            break;

        pos += 5;

        endPos = result.indexOf(">", pos);

        if (endPos == -1)
            break;

        pos = result.indexOf(QRegularExpression("\\s"), pos);

        if (pos >= endPos)
            continue;

        pos++;
        attStart = pos;

        while (true)
        {
            int attPos = attStart;
            int attEnd = 0;

            attPos = result.indexOf("=", attPos);

            if (attPos >= endPos)
                break;

            attEnd = result.indexOf(QRegularExpression("\\s"), attPos);

            if (attEnd >= endPos)
                attEnd = endPos;

            QString attr = result.mid(attStart, attPos - attStart).trimmed().toLower();

            attPos++;

            if (attr == "start" || attr == "end")
            {
                QString value = result.mid(attPos, attEnd - attPos);
                bool ok = false;
                int time = value.toInt(&ok);

                if (ok)
                {
                    QString newValue = QString::number(time + sync);
                    int countDiff = newValue.count() - value.count();

                    result.replace(attPos, value.count(), newValue);

                    attEnd += countDiff;
                    endPos += countDiff;
                }
            }

            attStart = attEnd + 1;

            if (attStart >= endPos)
                break;
        }

        pos = endPos;
    }

    *ret = result;
}

bool SAMIParser::save(const QString &path, double sync)
{
    QMutexLocker locker(&this->m_lock);
    QFile file(path);
    QTextStream stream;
    QString contents;

    this->addTimeToContent(this->m_contents, int(sync * 1000), &contents);

    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return false;

    stream.setDevice(&file);
    stream.setCodec("UTF-8");
    stream.setGenerateByteOrderMark(true);

    stream << contents;

    stream.flush();

    if (stream.status() != QTextStream::Ok)
        return false;

    return true;
}

void SAMIParser::close()
{
    QMutexLocker locker(&this->m_lock);

    this->m_curIndex = 0;
    this->m_contents.clear();
    this->m_sami.syncs.clear();
    this->m_sami.classNames.clear();
    this->m_sami.defaultClassName.clear();
}

bool SAMIParser::isExist()
{
    QMutexLocker locker(&this->m_lock);

    return this->m_sami.syncs.count() > 0;
}

void SAMIParser::determinDefaultClassName()
{
    QMap<QString, int> table;
    QString defaultClassName;

    if (this->m_sami.classNames.count() > 0)
        defaultClassName = this->m_sami.classNames[0];

    for (int i = 0; i < this->m_sami.syncs.count(); i++)
    {
        Sync &sync = this->m_sami.syncs[i];

        for (int j = 0; j < sync.paragraphs.count(); j++)
        {
            const Paragraph &para = sync.paragraphs[j];

            if (table.contains(para.className))
                table[para.className] += 1;
            else
                table[para.className] = 1;
        }
    }

    QMapIterator<QString, int> i(table);

    while (i.hasNext())
    {
        int defaultClassCount = table[defaultClassName];

        i.next();

        if (defaultClassName != i.key())
        {
            if (defaultClassCount / 2 < i.value())
                defaultClassName = i.key();
        }
    }

    this->m_sami.defaultClassName = defaultClassName;
}

void SAMIParser::fillEndTime()
{
    int count = this->m_sami.syncs.count();

    if (count <= 0)
        return;

    for (int i = 0; i < count - 1; i++)
    {
        Sync &first = this->m_sami.syncs[i];
        int paraCount = first.paragraphs.count();

        if (paraCount > 0)
        {
            bool found = false;

            for (int j = 0; j < paraCount; j++)
            {
                Sync *second;

                if (this->findSecond(first.paragraphs[j].className, i + 1, &second))
                {
                    if (first.end == -1)
                        first.end = second->start;

                    found = true;
                    break;
                }
            }

            if (!found)
                first.end = this->m_sami.syncs.last().start;
        }
        else
        {
            first.end = first.start;
        }
    }

    Sync &sync = this->m_sami.syncs.last();

    if (sync.end == -1)
        sync.end = numeric_limits<int32_t>::max();
}

bool SAMIParser::findSecond(const QString &className, int index, Sync **ret)
{
    int count = this->m_sami.syncs.count();

    for (int i = index; i < count; i++)
    {
        Sync &sync = this->m_sami.syncs[i];

        for (int j = 0; j < sync.paragraphs.count(); j++)
        {
            const Paragraph &para = sync.paragraphs[j];

            if (para.className == className)
            {
                *ret = &sync;
                return true;
            }
        }
    }

    return false;
}

bool SAMIParser::get(const QString &className, int32_t millisecond, Paragraph *ret)
{
    QMutexLocker locker(&this->m_lock);
    int count = this->m_sami.syncs.count();
    int oldIndex = this->m_curIndex;

    if (count > 0)
    {
        for (int i = 0; i < count; i++)
        {
            const Sync &sync = this->m_sami.syncs[this->m_curIndex];

            if (sync.start <= millisecond && millisecond < sync.end)
            {
                for (int j = 0; j < sync.paragraphs.count(); j++)
                {
                    const Paragraph &para = sync.paragraphs[j];

                    if (para.className == className)
                    {
                        *ret = para;
                        return true;
                    }
                }
            }

            this->m_curIndex++;

            if (this->m_curIndex >= count)
                this->m_curIndex = 0;
        }

        this->m_curIndex = oldIndex;
    }

    return false;
}

void SAMIParser::removeComment(QString *ret) const
{
    int pos = 0;
    int endPos = 0;

    while (true)
    {
        pos = ret->indexOf("<!--", pos);

        if (pos == -1)
            break;

        endPos = ret->indexOf("-->", pos);

        if (endPos == -1)
            endPos = ret->count() - 1;

        endPos = ret->indexOf(">", endPos);

        endPos++;
        ret->remove(pos, endPos - pos);
    }
}

void SAMIParser::getDefaultClassName(QString *ret)
{
    QMutexLocker locker(&this->m_lock);

    if (this->m_sami.defaultClassName.isEmpty())
    {
        QStringList names;

        this->getClassNames(&names);

        if (names.count() > 0)
            *ret = names[0];
        else
            *ret = "default";
    }
    else
    {
        *ret = this->m_sami.defaultClassName;
    }
}

void SAMIParser::setDefaultClassName(const QString &className)
{
    QMutexLocker locker(&this->m_lock);

    this->m_sami.defaultClassName = className;
}

void SAMIParser::getClassNames(QStringList *ret)
{
    QMutexLocker locker(&this->m_lock);

    if (this->m_sami.classNames.count() == 0)
    {
        QMap<QString, QString> directory;

        for (int i = 0; i < this->m_sami.syncs.count(); i++)
        {
            const Sync &sync = this->m_sami.syncs[i];

            for (int j = 0; j < sync.paragraphs.count(); j++)
            {
                const Paragraph &para = sync.paragraphs[j];

                if (!directory.contains(para.className))
                    directory.insert(para.className, para.className);
            }
        }

        QMap<QString, QString>::iterator i = directory.begin();

        for (; i != directory.end(); i++)
            ret->append(i.key());

        this->m_sami.classNames = *ret;
    }
    else
    {
        *ret = this->m_sami.classNames;
    }
}

void SAMIParser::setProcessNewLine(bool value)
{
    this->m_processNewLine = value;
}

bool SAMIParser::getProcessNewLine() const
{
    return this->m_processNewLine;
}

bool SAMIParser::getColor(const QString &color, QColor *ret) const
{
    QString tmp = color.toLower();

    tmp = tmp.remove("=").trimmed();
    tmp.remove("\"");
    tmp.remove("'");

    if (tmp.count() <= 0)
        return false;

    if (tmp[0] == '#')
    {
        bool ok;
        int rgb;

        tmp = tmp.remove('#');
        rgb = tmp.toInt(&ok, 16);

        if (ok)
            *ret = QColor::fromRgb(rgb);
        else
            *ret = Qt::white;
    }
    else if (tmp.startsWith("rgb"))
    {
        int index = tmp.indexOf('(');

        if (index == -1)
            return false;

        QString rgb = tmp.mid(index + 1).remove(')');
        QStringList rgbNumbers = rgb.split(',');

        if (rgbNumbers.length() != 3)
            return false;

        bool ok;
        int r;
        int g;
        int b;

        r = rgbNumbers[0].toInt(&ok);

        if (!ok)
            return false;

        g = rgbNumbers[1].toInt(&ok);

        if (!ok)
            return false;

        b = rgbNumbers[2].toInt(&ok);

        if (!ok)
            return false;

        *ret = QColor(r, g, b);
    }
    else
    {
        ret->setNamedColor(tmp);

        if (!ret->isValid())
        {
            bool ok;
            int rgb;

            rgb = tmp.toInt(&ok, 16);

            if (ok)
                *ret = QColor::fromRgb(rgb);
            else
                *ret = Qt::white;
        }
    }

    return true;
}

void SAMIParser::removeTags(QString *ret) const
{
    ret->remove(QRegularExpression("<[^>]*>"));

    int count = ret->count();

    for (int i = 0; i < count; i++)
    {
        if (i < count - 2 && (*ret)[i] == '&' && (*ret)[i + 1] == '#')
        {
            int index = ret->indexOf(';', i + 1);

            if (index == -1)
            {
                for (int j = i + 2; j < count; j++)
                {
                    QChar c = (*ret)[j];

                    if (!c.isNumber() && !c.isLower() && !c.isUpper())
                    {
                        index = j;
                        break;
                    }
                }
            }
            else
            {
                index++;
            }

            if (index == -1)
                continue;

            QString escape = ret->mid(i, index - i);
            QString number = escape;

            number.remove('&');
            number.remove('#');
            number.remove(';');

            if (number.length() > 0)
            {
                number = number.toLower();

                if (number[0] == 'x')
                    number.insert(0, '0');
            }

            ret->replace(escape, QChar(number.toInt(nullptr, 0)));
        }
    }

    ret->replace("&nbsp;", " ", Qt::CaseInsensitive);
    ret->replace("&nbsp", " ", Qt::CaseInsensitive);

    ret->replace("&quot;", "\"", Qt::CaseInsensitive);
    ret->replace("&quot", "\"", Qt::CaseInsensitive);

    ret->replace("&amp;", "&", Qt::CaseInsensitive);
    ret->replace("&amp", "&", Qt::CaseInsensitive);

    ret->replace("&lt;", "<", Qt::CaseInsensitive);
    ret->replace("&lt", "<", Qt::CaseInsensitive);

    ret->replace("&gt;", ">", Qt::CaseInsensitive);
    ret->replace("&gt", ">", Qt::CaseInsensitive);

    ret->replace('\t', TABS);
}

int SAMIParser::getTag(const QString &text, int pos, QString *ret) const
{
    QString tag = text;
    int start = tag.indexOf("<", pos);
    int end;

    if (start == -1)
        return pos;

    int i = start + 1;

    while (!tag.isEmpty())
    {
        if (tag[i].isSpace())
            tag.remove(i, 1);
        else
            break;
    }

    end = tag.indexOf(">", start);

    if (end == -1)
    {
        end = tag.length();
    }
    else
    {
        i = end - 1;

        while (!tag.isEmpty())
        {
            if (tag[i].isSpace())
                tag.remove(i--, 1);
            else
                break;
        }

        end = tag.indexOf(">", start);
        end++;
    }

    *ret = tag.mid(start, end - start).toLower();

    return end;
}

void SAMIParser::parseLine(const QString &data, Context *context, QVector<Text> *ret) const
{
    Text text;
    int i = 0;
    QString plainText;

    while (i < data.count())
    {
        if (data[i] == '<')
        {
            if (!plainText.isEmpty())
            {
                this->removeTags(&plainText);

                text.bold = context->bold;
                text.italic = context->italic;
                text.underline = context->underline;
                text.strike = context->strike;
                text.color = context->color;
                text.text = plainText;

                plainText.clear();

                ret->append(text);
            }

            QString tag;
            int pos = this->getTag(data, i, &tag);
            int tagPos = 0;

            if (tag.startsWith("</"))
            {
                if (tag == "</font>")
                {
                    if (!context->colorStack.isEmpty())
                        context->color = context->colorStack.pop();
                }
                else if (tag == "</b>")
                {
                    if (!context->boldStack.isEmpty())
                        context->bold = context->boldStack.pop();
                }
                else if (tag == "</u>")
                {
                    if (!context->underlineStack.isEmpty())
                        context->underline = context->underlineStack.pop();
                }
                else if (tag == "</i>")
                {
                    if (!context->italicStack.isEmpty())
                        context->italic = context->italicStack.pop();
                }
                else if (tag == "</s>" || tag == "</strike>")
                {
                    if (!context->strikeStack.isEmpty())
                        context->strike = context->strikeStack.pop();
                }
            }
            else
            {
                if (tag.startsWith("<font"))
                {
                    context->colorStack.push(context->color);

                    tagPos = tag.indexOf("Color", tagPos, Qt::CaseInsensitive);

                    if (tagPos != -1)
                    {
                        tagPos = tag.indexOf("=", tagPos);

                        if (tagPos != -1)
                        {
                            int endPos = tag.indexOf(QRegularExpression("\\s|>"), tagPos);
                            QColor color = context->color;

                            if (endPos != -1)
                                this->getColor(tag.mid(tagPos, endPos - tagPos), &color);

                            context->color = color;
                        }
                    }
                    else
                    {
                        context->color = Qt::white;
                    }
                }
                else if (tag == "<b>")
                {
                    context->boldStack.push(context->bold);
                    context->bold = true;
                }
                else if (tag == "<u>")
                {
                    context->underlineStack.push(context->underline);
                    context->underline = true;
                }
                else if (tag == "<i>")
                {
                    context->italicStack.push(context->italic);
                    context->italic = true;
                }
                else if (tag == "<s>" || tag == "<strike>")
                {
                    context->strikeStack.push(context->strike);
                    context->strike = true;
                }
            }

            i = pos;
        }
        else
        {
            plainText.append(data[i]);
            i++;
        }
    }

    if (!plainText.isEmpty())
    {
        this->removeTags(&plainText);

        text.bold = context->bold;
        text.italic = context->italic;
        text.underline = context->underline;
        text.strike = context->strike;
        text.color = context->color;
        text.text = plainText;

        ret->append(text);
    }
}

void SAMIParser::parseParagraph(const QString &data, QVector<Line> *ret) const
{
    QString para = data;
    QStringList lines;
    Context context;

    context.bold = false;
    context.italic = false;
    context.underline = false;
    context.strike = false;
    context.color = Qt::white;

    if (!this->m_processNewLine)
        para.remove(NEW_LINE);

    para.remove("\r");
    para.replace("<br>", NEW_LINE, Qt::CaseInsensitive);
    lines = para.split(NEW_LINE);

    for (int i = 0; i < lines.count(); i++)
    {
        Line line;

        this->parseLine(lines[i], &context, &line.subtitles);
        ret->append(line);
    }
}

void SAMIParser::parseSync(const QString &data, QVector<Paragraph> *ret)
{
    int pos = 0;

    while (true)
    {
        Paragraph para;
        int endPos = 0;
        int prevPos = 0;

        pos = data.indexOf("<P", pos, Qt::CaseInsensitive);

        if (pos == -1)
        {
            if (ret->count() == 0)
            {
                this->getDefaultClassName(&para.className);
                this->parseParagraph(data, &para.lines);

                ret->append(para);
            }

            break;
        }

        prevPos = pos;
        pos = data.indexOf("Class", pos, Qt::CaseInsensitive);

        if (pos == -1)
        {
            prevPos++;
            endPos = data.indexOf(">", prevPos, Qt::CaseInsensitive);

            if (endPos == -1)
                break;

            this->getDefaultClassName(&para.className);
        }
        else
        {
            pos = data.indexOf("=", pos, Qt::CaseInsensitive);

            if (pos == -1)
                break;

            pos++;
            endPos = data.indexOf(">", pos, Qt::CaseInsensitive);

            if (endPos == -1)
                break;

            para.className = data.mid(pos, endPos - pos);
            para.className.remove("'");
        }

        para.className = para.className.trimmed();

        pos = endPos + 1;
        endPos = data.indexOf("<P", pos, Qt::CaseInsensitive);

        if (endPos == -1)
            endPos = data.length();

        QString content = data.mid(pos, endPos - pos);

        this->parseParagraph(content, &para.lines);

        ret->append(para);
    }
}

void SAMIParser::parseClassName(const QString &data)
{
    int pos = 0;
    int endPos = 0;

    pos = data.indexOf("<STYLE", pos, Qt::CaseInsensitive);

    if (pos == -1)
        return;

    pos = data.indexOf(">", pos);

    if (pos == -1)
        return;

    pos++;
    endPos = data.indexOf(">", pos);

    QStringList style = data.mid(pos, endPos - pos).split(NEW_LINE, Qt::SkipEmptyParts);

    for (int i = 0; i < style.count(); i++)
    {
        QString line = style[i].trimmed();

        if (line.startsWith("."))
        {
            endPos = line.indexOf(QRegularExpression("\\s"));

            this->m_sami.classNames.append(line.mid(1, endPos - 1));
        }
    }
}

void SAMIParser::parse(QIODevice &file, const QString &codecName)
{
    QMutexLocker locker(&this->m_lock);
    QTextStream stream(&file);

    stream.setCodec(codecName.toLatin1());

    QString sami(stream.readAll());
    int pos = 0;

    this->m_contents = sami;
    this->parseClassName(sami);
    this->removeComment(&sami);

    while (true)
    {
        int endPos = 0;
        Sync sync;
        QString attr;
        QStringList attrs;

        pos = sami.indexOf("<SYNC", pos, Qt::CaseInsensitive);

        if (pos == -1)
            break;

        pos += 5;

        endPos = sami.indexOf(">", pos);

        attr = sami.mid(pos, endPos - pos).trimmed();
        attrs = attr.split(QRegularExpression("\\s"));

        sync.start = 0;
        sync.end = -1;

        for (int i = 0; i < attrs.count(); i++)
        {
            QStringList items = attrs[i].split("=");

            if (items[0].toLower() == "start")
                sync.start = items[1].toInt();
            else if (items[0].toLower() == "end")
                sync.end = items[1].toInt();
        }

        pos = endPos + 1;
        endPos = sami.indexOf("<SYNC", pos, Qt::CaseInsensitive);

        if (endPos == -1)
            endPos = sami.length();

        this->parseSync(sami.mid(pos, endPos - pos), &sync.paragraphs);

        this->m_sami.syncs.append(sync);
    }
}
