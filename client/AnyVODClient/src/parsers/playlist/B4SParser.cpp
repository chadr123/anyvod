﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "B4SParser.h"

#include <QFile>
#include <QDomDocument>

B4SParser::B4SParser()
{

}

bool B4SParser::parse(QFile &file)
{
    QDomDocument doc;

    if (!doc.setContent(&file))
        return false;

    QDomElement root = doc.documentElement();
    QDomElement playlist = root.firstChildElement("playlist");
    QDomNodeList entries = playlist.elementsByTagName("entry");

    for (int i = 0; i < entries.count(); i++)
    {
        QDomElement entry = entries.item(i).toElement();
        QString value = entry.attribute("Playstring");
        int index = value.indexOf(':');

        if (index >= 0)
            value = value.mid(index + 1);

        QString title = entry.firstChildElement("Name").text();
        PlayListParserInterface::PlayListItem item;

        item.path = value;
        item.title = title;

        this->appendPlayList(item);
    }

    return true;
}
