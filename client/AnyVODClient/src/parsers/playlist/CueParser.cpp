﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "CueParser.h"
#include "core/TextEncodingDetector.h"
#include "utils/TimeUtils.h"

#include <limits>
#include <QFile>
#include <QTextStream>
#include <QTime>
#include <QRegularExpression>

using namespace std;

CueParser::CueParser()
{

}

CueParser::~CueParser()
{
    this->close();
}

bool CueParser::open(const QString &filePath)
{
    this->close();

    QFile file(filePath);
    QString codecName;
    TextEncodingDetector detector;

    if (!detector.getCodecName(filePath, &codecName))
        return false;

    if (!file.open(QIODevice::ReadOnly))
        return false;

    this->parse(file, codecName);

    return this->isExist();
}

void CueParser::close()
{
    this->m_chapters.clear();
    this->m_filePath.clear();
}

void CueParser::getChapters(QVector<ChapterInfo> *ret) const
{
    *ret = this->m_chapters;
}

void CueParser::getFilePath(QString *ret) const
{
    *ret = this->m_filePath;
}

bool CueParser::isExist() const
{
    return this->m_chapters.count() > 0;
}

bool CueParser::parse(QFile &file, const QString &codecName)
{
    QTextStream stream(&file);

    stream.setCodec(codecName.toLatin1());

    while (true)
    {
        QString line = stream.readLine().trimmed();

        if (line.isNull())
            break;

        if (line.isEmpty())
            continue;

        if (line.startsWith("file", Qt::CaseInsensitive))
        {
            QStringList list = line.split('\"', Qt::SkipEmptyParts);

            if (list.count() > 1)
            {
                this->m_filePath = list[1].trimmed();
                this->parseFile(stream);

                return true;
            }
        }
    }

    return false;
}

void CueParser::parseFile(QTextStream &stream)
{
    QStringList lines;

    while (true)
    {
        QString line = stream.readLine().trimmed();

        if (line.isNull())
            break;

        if (line.isEmpty())
            continue;

        lines.append(line);
    }

    for (int i = 0; i < lines.count(); i++)
    {
        if (lines[i].startsWith("track", Qt::CaseInsensitive))
            i = this->parseTrack(lines, i + 1);
    }

    if (this->m_chapters.count() > 0)
        this->m_chapters.last().end = numeric_limits<int>::max();
}

int CueParser::parseTrack(QStringList &lines, int index)
{
    ChapterInfo info;
    int i = index;

    for (; i < lines.count(); i++)
    {
        QString &line = lines[i];
        QStringList list;

        if (line.startsWith("title", Qt::CaseInsensitive))
        {
            list = line.split('\"', Qt::SkipEmptyParts);

            if (list.count() > 1)
                info.desc = list[1].trimmed();
        }
        else if (line.startsWith("index", Qt::CaseInsensitive))
        {
            list = line.split(QRegularExpression("\\s"), Qt::SkipEmptyParts);

            if (list.count() > 2)
            {
                int index = list[1].toInt();
                QString &timeString = list[2];
                bool ok = true;
                int minute;
                int second;

                minute = timeString.midRef(0, 2).toInt(&ok);

                if (!ok)
                    continue;

                second = timeString.midRef(3, 2).toInt(&ok);

                if (!ok)
                    continue;

                QTime time = QTime(TimeUtils::ZERO_TIME).addSecs(minute * 60 + second);
                int value = -time.secsTo(TimeUtils::ZERO_TIME);

                if (index == 1)
                {
                    info.start = value;
                }
                else if (index == 0)
                {
                    if (this->m_chapters.count() > 0)
                        this->m_chapters.last().end = value;
                }
            }
        }
        else if (line.startsWith("track", Qt::CaseInsensitive))
        {
            break;
        }
    }

    if (this->m_chapters.count() > 0)
    {
        ChapterInfo &last = this->m_chapters.last();

        if (last.end == 0)
            last.end = info.start;
    }

    this->m_chapters.push_back(info);

    return i - 1;
}
