﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "core/ChapterInfo.h"

#include <QVector>

class QFile;
class QTextStream;

class CueParser
{
public:
    CueParser();
    ~CueParser();

    bool open(const QString &filePath);
    void close();

    void getChapters(QVector<ChapterInfo> *ret) const;
    void getFilePath(QString *ret) const;

    bool isExist() const;

private:
    bool parse(QFile &file, const QString &codecName);
    void parseFile(QTextStream &stream);
    int parseTrack(QStringList &lines, int index);

private:
    QString m_filePath;
    QVector<ChapterInfo> m_chapters;
};
