﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "PlayListParserInterface.h"
#include "PlayListParserGenerator.h"
#include "net/HttpDownloader.h"
#include "utils/SeparatingUtils.h"
#include "utils/RemoteFileUtils.h"

#include "../../../../common/types.h"

#include <QFileInfo>
#include <QDir>
#include <QTemporaryFile>

PlayListParserInterface::PlayListParserInterface() :
    m_success(false)
{

}

PlayListParserInterface::~PlayListParserInterface()
{

}

void PlayListParserInterface::setRoot(const QString &root)
{
    this->m_root = root;
}

void PlayListParserInterface::getRoot(QString *ret) const
{
    *ret = this->m_root;
}

void PlayListParserInterface::clear()
{
    this->m_list.clear();
    this->m_loadedPlayList.clear();
}

void PlayListParserInterface::getFileList(QVector<PlayListItem> *ret) const
{
    *ret = this->m_list;
}

void PlayListParserInterface::getLoadedPlayList(QStringList *ret) const
{
    *ret = this->m_loadedPlayList;
}

bool PlayListParserInterface::isValidPlayListExt(const QString &ext)
{
    if (ext.toLower() == "cue")
        return false;

    static QStringList exts = QString::fromStdWString(PLAYLIST_EXTENSION).split(" ", Qt::SkipEmptyParts);

    return exts.contains(ext, Qt::CaseInsensitive);
}

bool PlayListParserInterface::isExist() const
{
    return !this->m_list.isEmpty();
}

bool PlayListParserInterface::isSuccess() const
{
    return this->m_success;
}

bool PlayListParserInterface::open(const QString &filePath)
{
    this->close();

    if (RemoteFileUtils::determinRemoteProtocol(filePath))
    {
        this->m_success = this->openFromRemote(filePath);
    }
    else
    {
        QString path = QDir::toNativeSeparators(filePath);
        QFile file(path);

        if (!file.open(QIODevice::ReadOnly))
            return false;

        this->m_loadedPlayList.append(path);
        this->m_success = this->parse(file);
    }

    return this->isExist();
}

bool PlayListParserInterface::openFromRemote(const QString &url)
{
    if (this->m_loadedPlayList.contains(url))
        return true;

    QString tmpFilePath = QDir::tempPath();
    QFileInfo info(url);

    SeparatingUtils::appendDirSeparator(&tmpFilePath);
    tmpFilePath += "XXXXXX.";
    tmpFilePath += info.suffix();

    QTemporaryFile tmpFile(tmpFilePath);

    if (!tmpFile.open())
        return false;

    this->m_loadedPlayList.append(url);

    HttpDownloader downloader;

    if (downloader.download(url, tmpFile))
    {
        tmpFile.close();

        return this->loadPlayListFromFile(tmpFile.fileName());
    }

    return false;
}

void PlayListParserInterface::close()
{
    this->clear();
}

bool PlayListParserInterface::loadPlayListFromFile(const QString &filePath)
{
    QString path = QDir::toNativeSeparators(filePath);

    if (this->m_loadedPlayList.contains(path))
        return true;

    bool success = false;
    PlayListParserGenerator gen;
    QFileInfo info(path);
    PlayListParserInterface *parser = gen.getParser(info.suffix());

    if (parser == nullptr)
        return false;

    parser->setRoot(info.absolutePath() + QDir::separator());

    if (parser->open(path))
    {
        QVector<PlayListItem> fileList;
        QStringList loadedPlayList;

        parser->getFileList(&fileList);
        parser->getLoadedPlayList(&loadedPlayList);

        parser->close();

        for (const PlayListItem &item : qAsConst(fileList))
            this->m_list.append(item);

        this->m_loadedPlayList.append(loadedPlayList);

        success = true;
    }

    this->m_loadedPlayList.append(path);

    delete parser;

    return success;
}

bool PlayListParserInterface::loadPlayList(const QString &filePath)
{
    if (RemoteFileUtils::determinRemoteProtocol(filePath))
        return this->openFromRemote(filePath);
    else
        return this->loadPlayListFromFile(filePath);
}

void PlayListParserInterface::appendPlayList(const PlayListItem &item)
{
    QString value = item.path;
    QFileInfo info(value);
    QString rootPath;

    if (!RemoteFileUtils::determinRemoteProtocol(value) && !info.isAbsolute())
        rootPath = this->m_root;

    value = rootPath + value;

    if (this->isValidPlayListExt(info.suffix()))
    {
        this->loadPlayList(value);
    }
    else
    {
        PlayListItem tmp = item;

        tmp.path = value;

        this->m_list.append(tmp);
    }
}
