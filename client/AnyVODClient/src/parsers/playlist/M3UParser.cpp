﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "M3UParser.h"

#include <QFile>
#include <QFileInfo>
#include <QTextStream>

M3UParser::M3UParser()
{

}

bool M3UParser::parse(QFile &file)
{
    QFileInfo fileInfo(file);
    QTextStream stream(&file);

    if (fileInfo.suffix().toLower() == "m3u8")
        stream.setCodec("utf-8");

    while (true)
    {
        QString line = stream.readLine().trimmed();

        if (line.isNull())
            break;

        if (line.isEmpty())
            continue;

        if (line == "#EXTM3U")
            break;
        else
            return false;
    }

    while (true)
    {
        QString line = stream.readLine().trimmed();

        if (line.isNull())
            break;

        if (line.isEmpty())
            continue;

        if (line.startsWith("#EXTINF"))
        {
            PlayListParserInterface::PlayListItem item;
            int pos = line.indexOf(',');

            if (pos == -1)
                continue;

            item.title = line.mid(pos + 1).trimmed();
            item.path = stream.readLine().trimmed();

            this->appendPlayList(item);
        }
    }

    return true;
}
