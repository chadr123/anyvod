﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "PlayListParserGenerator.h"
#include "M3UParser.h"
#include "PLSParser.h"
#include "ASXParser.h"
#include "WPLParser.h"
#include "B4SParser.h"

PlayListParserGenerator::PlayListParserGenerator()
{

}

PlayListParserInterface* PlayListParserGenerator::getParser(const QString &ext) const
{
    PlayListParserInterface *parser = nullptr;
    QString tmp = ext.toLower();

    if (tmp == "m3u" || tmp == "m3u8")
        parser = new M3UParser;
    else if (tmp == "pls")
        parser = new PLSParser;
    else if (tmp == "asx" || tmp == "wvx" || tmp == "wax" || tmp == "wmx")
        parser = new ASXParser;
    else if (tmp == "wpl")
        parser = new WPLParser;
    else if (tmp == "b4s")
        parser = new B4SParser;

    return parser;
}
