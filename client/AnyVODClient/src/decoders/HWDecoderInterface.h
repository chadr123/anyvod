﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#if !defined Q_OS_MOBILE
# include "video/USWCMemCopy.h"
#endif

class QString;

extern "C"
{
# include <libavcodec/avcodec.h>
# include <libavutil/pixfmt.h>
}

class HWDecoderInterface
{
public:
    HWDecoderInterface();
    virtual ~HWDecoderInterface();

    virtual bool open(AVCodecContext *codec) = 0;
    virtual void close() = 0;

    virtual bool prepare(AVCodecContext *codec) = 0;

    virtual bool getBuffer(AVFrame *ret) = 0;
    virtual void releaseBuffer(uint8_t *data[AV_NUM_DATA_POINTERS]) = 0;

    virtual AVPixelFormat getFormat() const = 0;
    virtual QString getName() const = 0;

    virtual bool decodePicture(const AVPacket &packet, AVFrame *ret) = 0;
    virtual bool copyPicture(const AVFrame &src, AVFrame *ret) = 0;

    virtual bool isDecodable(AVPixelFormat format) const = 0;
    virtual void getDecoderDesc(QString *ret) const = 0;

    virtual void flushSurfaceQueue() = 0;
    virtual int getSurfaceQueueCount() const = 0;

    virtual bool isUseDefaultGetBuffer() const = 0;

    virtual bool isSuitable(AVCodecID codecID, int profile) const;

    void resetShouldStop();
    bool getShouldStop() const;

#if !defined Q_OS_MOBILE
protected:
    USWCMemCopy& getMemCopy();

private:
    USWCMemCopy m_memCopy;
#endif

protected:
    bool m_shouldStop;
};
