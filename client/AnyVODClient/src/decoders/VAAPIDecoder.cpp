﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "VAAPIDecoder.h"

#include <QDebug>

extern "C"
{
# include <libavutil/hwcontext.h>
# include <libavutil/opt.h>
# include <libavutil/buffer.h>
}

VAAPIDecoder::VAAPIDecoder()
{

}

VAAPIDecoder::~VAAPIDecoder()
{
    this->closeInternal();
}

bool VAAPIDecoder::open(AVCodecContext *codec)
{
    if (av_hwdevice_ctx_create(&this->m_context.deviceCtx, AV_HWDEVICE_TYPE_VAAPI, nullptr, nullptr, 0) < 0)
    {
        this->close();
        return false;
    }

    codec->hw_device_ctx = av_buffer_ref(this->m_context.deviceCtx);

    this->m_context.codec = codec;

    return true;
}

void VAAPIDecoder::close()
{
    this->closeInternal();
}

bool VAAPIDecoder::prepare(AVCodecContext *codec)
{
    (void)codec;

    return true;
}

bool VAAPIDecoder::getBuffer(AVFrame *ret)
{
    return avcodec_default_get_buffer2(this->m_context.codec, ret, 0) == 0;
}

void VAAPIDecoder::releaseBuffer(uint8_t *data[])
{
    (void)data;
}

AVPixelFormat VAAPIDecoder::getFormat() const
{
    AVCodecContext *codec = this->m_context.codec;

    if (codec->hw_frames_ctx == nullptr)
        return AV_PIX_FMT_NONE;

    AVPixelFormat *formats = nullptr;
    AVPixelFormat format;

    if (av_hwframe_transfer_get_formats(codec->hw_frames_ctx, AV_HWFRAME_TRANSFER_DIRECTION_FROM, &formats, 0) < 0)
        return AV_PIX_FMT_NONE;

    format = formats[0];
    av_free(formats);

    return format;
}

QString VAAPIDecoder::getName() const
{
    return "VA-API";
}

bool VAAPIDecoder::decodePicture(const AVPacket &packet, AVFrame *ret)
{
    (void)packet;
    (void)ret;

    return false;
}

bool VAAPIDecoder::copyPicture(const AVFrame &src, AVFrame *ret)
{
    int result = av_hwframe_transfer_data(ret, &src, 0);

    if (result != 0)
        this->m_shouldStop = true;

    return result == 0;
}

bool VAAPIDecoder::isDecodable(AVPixelFormat format) const
{
    return format == AV_PIX_FMT_VAAPI;
}

void VAAPIDecoder::getDecoderDesc(QString *ret) const
{
    switch (this->m_context.codec->codec_id)
    {
        case AV_CODEC_ID_H264:
            *ret = "H264";
            break;
        case AV_CODEC_ID_MJPEG:
            *ret = "MJPEG";
            break;
        case AV_CODEC_ID_MPEG2VIDEO:
            *ret = "MPEG2 Video";
            break;
        case AV_CODEC_ID_MPEG4:
            *ret = "MPEG4";
            break;
        case AV_CODEC_ID_VC1:
            *ret = "VC1";
            break;
        case AV_CODEC_ID_WMV3:
            *ret = "WMV3";
            break;
        case AV_CODEC_ID_VP8:
            *ret = "VP8";
            break;
        case AV_CODEC_ID_VP9:
            *ret = "VP9";
            break;
        case AV_CODEC_ID_HEVC:
            *ret = "HEVC";
            break;
        case AV_CODEC_ID_AV1:
            *ret = "AV1";
            break;
        default:
            break;
    }
}

void VAAPIDecoder::flushSurfaceQueue()
{

}

int VAAPIDecoder::getSurfaceQueueCount() const
{
    return 0;
}

bool VAAPIDecoder::isUseDefaultGetBuffer() const
{
    return true;
}

void VAAPIDecoder::closeInternal()
{
    if (this->m_context.deviceCtx)
    {
        av_buffer_unref(&this->m_context.deviceCtx);
        this->m_context.deviceCtx = nullptr;
    }

    this->m_context.codec = nullptr;
}
