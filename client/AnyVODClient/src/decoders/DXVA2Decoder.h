﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "HWDecoderInterface.h"

#ifdef _WIN32_WINNT
# undef _WIN32_WINNT
#endif

#define _WIN32_WINNT 0x0600
#define COBJMACROS
#define CINTERFACE
#define INITGUID
#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <d3d9.h>

extern "C"
{
# include <libavcodec/dxva2.h>
}

const int DXVA2_MAX_SURFACE_COUNT = 64;
const int DXVA2_MAX_SURFACE_QUEUE = 2;

class DXVA2Decoder : public HWDecoderInterface
{
public:
    DXVA2Decoder();
    virtual ~DXVA2Decoder();

    virtual bool open(AVCodecContext *codec);
    virtual void close();

    virtual bool prepare(AVCodecContext *codec);

    virtual bool getBuffer(AVFrame *ret);
    virtual void releaseBuffer(uint8_t *data[AV_NUM_DATA_POINTERS]);

    virtual AVPixelFormat getFormat() const;
    virtual QString getName() const;

    virtual bool decodePicture(const AVPacket &packet, AVFrame *ret);
    virtual bool copyPicture(const AVFrame &src, AVFrame *ret);

    virtual bool isDecodable(AVPixelFormat format) const;
    virtual void getDecoderDesc(QString *ret) const;

    virtual void flushSurfaceQueue();
    virtual int getSurfaceQueueCount() const;

    virtual bool isUseDefaultGetBuffer() const;

private:
    struct Surface
    {
        Surface()
        {
            d3dSurface = nullptr;
            ref = 0;
            order = 0;
        }

        LPDIRECT3DSURFACE9 d3dSurface;
        int ref;
        unsigned int order;
    };

    struct Context
    {
        Context()
        {
            memset(&hw, 0, sizeof(hw));

            d3d9 = nullptr;
            dxva2 = nullptr;

            d3d = nullptr;
            d3dDev = nullptr;
            memset(&d3dParam, 0, sizeof(d3dParam));

            devManager = nullptr;

            device = nullptr;
            videoService = nullptr;

            memset(&input, 0, sizeof(input));
            render = D3DFMT_UNKNOWN;

            width = 0;
            height = 0;
            memset(&cfg, 0, sizeof(cfg));
            decoder = nullptr;

            surfaceWidth = 0;
            surfaceHeight = 0;
            surfaceCount = 0;
            surfaceOrder = 0;

            surfaceQueueIndex = 0;
            surfaceQueueCount = DXVA2_MAX_SURFACE_QUEUE;
            surfaceQueueInit = 0;
            memset(surfaceQueue, 0, sizeof(surfaceQueue));

            memset(&hwSurface, 0, sizeof(hwSurface));
        }

        dxva_context hw;

        HINSTANCE d3d9;
        HINSTANCE dxva2;

        LPDIRECT3D9 d3d;
        LPDIRECT3DDEVICE9 d3dDev;
        D3DPRESENT_PARAMETERS d3dParam;

        IDirect3DDeviceManager9 *devManager;

        HANDLE device;
        IDirectXVideoDecoderService *videoService;

        GUID input;
        D3DFORMAT render;

        int width;
        int height;
        DXVA2_ConfigPictureDecode cfg;
        IDirectXVideoDecoder *decoder;

        int surfaceWidth;
        int surfaceHeight;
        int surfaceCount;
        unsigned int surfaceOrder;

        int surfaceQueueIndex;
        int surfaceQueueCount;
        int surfaceQueueInit;
        LPDIRECT3DSURFACE9 surfaceQueue[DXVA2_MAX_SURFACE_QUEUE];

        Surface surface[DXVA2_MAX_SURFACE_COUNT];
        LPDIRECT3DSURFACE9 hwSurface[DXVA2_MAX_SURFACE_COUNT];
    };

    struct DXVA2Mode
    {
        const char *name;
        const GUID *guid;
        AVCodecID codec;
        const int *profiles;
    };

    struct D3DFormat
    {
        const char *name;
        D3DFORMAT format;
        AVPixelFormat pixFormat;
    };

    typedef LPDIRECT3D9 (WINAPI *CreateD3D9)(UINT);
    typedef HRESULT (WINAPI *CreateD3D9Ex)(UINT, IDirect3D9Ex **);
    typedef HRESULT (WINAPI *CreateDeviceManager9)(UINT *, IDirect3DDeviceManager9 **);
    typedef HRESULT (WINAPI *CreateVideoService)(IDirect3DDevice9 *, REFIID, void **);

private:
    bool loadDLLs();
    void unloadDLLs();

    void closeInternal();

    bool createDevice();
    bool createDeviceEx();
    void deleteDevice();

    bool createDeviceManager();
    void deleteDeviceManager();

    bool createVideoService();
    void deleteVideoService();

    bool findVideoServiceConversion(AVCodecID codecID, int profile);

    bool createDecoder(AVCodecID codecID, int width, int height);
    void deleteDecoder();

    Surface* findSurface(LPDIRECT3DSURFACE9 d3dSurface);
    bool isProfileSupported(const DXVA2Mode &mode, int profile) const;

    template <typename T>
    T getProcAddress(HMODULE hModule, LPCSTR lpProcName) const;

private:
    Context m_context;
    int m_initialRefCount;

private:
    static const DXVA2Mode MODES[];
    static const D3DFormat D3D_FORMATS[];
    static const int PROFILE_MPEG2_SIMPLE[];
    static const int PROFILE_MPEG2_MAIN[];
    static const int PROFILE_H264_HIGH[];
    static const int PROFILE_HEVC_MAIN[];
    static const int PROFILE_HEVC_MAIN10[];
    static const int PROFILE_VP9_0[];
    static const int PROFILE_VP9_1[];
    static const int PROFILE_VP9_2[];
    static const int PROFILE_VP9_3[];
    static const int PROFILE_AV1_MAIN[];
};
