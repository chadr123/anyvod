﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <QString>
#include <QDateTime>
#include <QLocale>

#include <stdint.h>

#define DTV_FEC(a, b)   (((a) << 16u) | (b))
#define DTV_FEC_AUTO    0xFFFFFFFF
#define DTV_GUARD(a, b) (((a) << 16u) | (b))
#define DTV_GUARD_AUTO  0xFFFFFFFF

#define ATSC_VCT_TERR_TID (0xC8)
#define ATSC_VCT_CABL_TID (0xC9)
#define ATSC_VCT_PID (0x1FFB)

#define ATSC_MGT_TID (0xC7)
#define ATSC_MGT_PID ATSC_VCT_PID
#define ATSC_MGT_TYPE_START (0x0100)
#define ATSC_MGT_TYPE_END (0x017F)

#define ATSC_STT_TID (0xCD)
#define ATSC_STT_PID ATSC_VCT_PID

#define ATSC_EIT_TID (0xCB)

#define DVB_SDT_ACTUAL_TID (0x42)
#define DVB_SDT_OTHER_TID (0x46)
#define DVB_SDT_PID (0x11)
#define DVB_SERVICE_TAG (0x48)

#define DVB_EIT_ACTUAL_TID (0x4E)
#define DVB_EIT_OTHER_TID (0x4F)
#define DVB_EIT_PID (0x12)
#define DVB_SHORT_EVENT_TAG (0x4D)

#define DVB_TDT_PID (0x14)
#define DVB_TDT_TID (0x70)

#define DTV_SYNC_BYTE (0x47)
#define DTV_BUDGET_PID (0x2000)

class DTVReaderInterface
{
public:
    struct ISDBTLayer
    {
        ISDBTLayer() :
            codeRate(0),
            segmentCount(0),
            timeInterleaving(0)
        {

        }

        ISDBTLayer(const QString &imod, uint32_t icodeRate, uint8_t isegmentCount, uint8_t itimeInterleaving) :
            mod(imod),
            codeRate(icodeRate),
            segmentCount(isegmentCount),
            timeInterleaving(itimeInterleaving)
        {

        }

        ISDBTLayer(const ISDBTLayer &rhs) :
            mod(rhs.mod),
            codeRate(rhs.codeRate),
            segmentCount(rhs.segmentCount),
            timeInterleaving(rhs.timeInterleaving)
        {

        }

        ISDBTLayer& operator = (const ISDBTLayer& rhs)
        {
            mod = rhs.mod;
            codeRate = rhs.codeRate;
            segmentCount = rhs.segmentCount;
            timeInterleaving = rhs.timeInterleaving;

            return *this;
        }

        QString mod;
        uint32_t codeRate;
        uint8_t segmentCount;
        uint8_t timeInterleaving;
    };

public:
    enum SystemType
    {
        ST_NONE = 0,
        ST_ATSC_T = 0x00000001,
        ST_ATSC_C = 0x00000002,
        ST_CQAM = 0x00000004,

        ST_DVB_C = 0x00000010,
        ST_DVB_C2 = 0x00000020,
        ST_DVB_S = 0x00000040,
        ST_DVB_S2 = 0x00000080,
        ST_DVB_T = 0x00000100,
        ST_DVB_T2 = 0x00000200,

        ST_ISDB_C = 0x00001000,
        ST_ISDB_S = 0x00002000,
        ST_ISDB_T = 0x00004000,
        ST_COUNT
    };

    enum Inversion
    {
        IV_NORMAL,
        IV_INVERTED,
        IV_AUTO
    };

    struct EPG
    {
        bool operator == (const EPG &rhs)
        {
            return title == rhs.title && start == rhs.start && duration == rhs.duration;
        }

        QString title;
        QDateTime start;
        QTime duration;
    };

    struct AdapterInfo
    {
        QString name;
        int index;
    };

    struct SystemTypeInfo
    {
        SystemType type;
        QString name;
    };

    struct CountryInfo
    {
        QLocale::Country country;
        QString name;
    };

public:
    DTVReaderInterface();
    virtual ~DTVReaderInterface();

    virtual bool open(const QString &path) = 0;
    virtual void close() = 0;
    virtual int read(uint8_t *buf, int size) = 0;

    virtual unsigned int enumSystems() = 0;
    virtual float getSignalStrength() const = 0;
    virtual float getSignalNoiseRatio() const = 0;
    virtual QString getChannelName() = 0;
    virtual void getEPG(QVector<EPG> *ret) = 0;
    virtual QDateTime getCurrentDateTime() = 0;
    virtual bool tune() = 0;

    virtual bool getAdapterList(QVector<AdapterInfo> *ret) const = 0;

    virtual bool setInversion(Inversion inversion) = 0;

    /* DVB-C */
    virtual bool setDVBC(uint32_t freq, const QString &mod, uint32_t srate, uint32_t fec) = 0;

    /* DVB-S */
    virtual bool setDVBS(uint64_t freq, uint32_t srate, uint32_t fec) = 0;
    virtual bool setDVBS2(uint64_t freq, const QString &mod, uint32_t srate, uint32_t fec, int pilot, int rolloff, uint8_t sid) = 0;
    virtual bool setSEC(uint64_t freq, uint32_t srate, uint32_t fec, char pol, uint32_t lowf, uint32_t highf, uint32_t switchf,
                        int32_t highv, uint32_t satNo, uint32_t uncommitted) = 0;

    /* DVB-T */
    virtual bool setDVBT(uint32_t freq, const QString &mod, uint32_t fecHP, uint32_t fecLP,
                         uint32_t bandwidth, int transmission, uint32_t guard, int hierarchy) = 0;
    virtual bool setDVBT2(uint32_t freq, const QString &mod, uint32_t fec, uint32_t bandwidth,
                          int transmission, uint32_t guard, uint8_t plp) = 0;

    /* ATSC */
    virtual bool setATSC(uint32_t freq, const QString &mod) = 0;
    virtual bool setCQAM(uint32_t freq, const QString &mod) = 0;

    /* ISDB-C */
    virtual bool setISDBC(uint32_t freq, const QString &mod, uint32_t srate, uint32_t fec) = 0;

    /* ISDB-S */
    virtual bool setISDBS(uint64_t freq, uint16_t tsID) = 0;

    /* ISDB-T */
    virtual bool setISDBT(uint32_t freq, uint32_t bandwidth, int transmission, uint32_t guard, const ISDBTLayer layers[3]) = 0;

public:
    void setAdapter(long adapter, SystemType type);
    void getAdapter(long *adapter, SystemType *type) const;
    void reset();

protected:
    QString decodeMultipleStringStructure(uint8_t *str, int len) const;

protected:
    long m_adapter;
    SystemType m_type;
};
