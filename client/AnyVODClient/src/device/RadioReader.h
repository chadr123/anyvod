﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

extern "C"
{
# include <libavformat/avio.h>
# include <libavformat/url.h>
}

#include "RadioReaderInterface.h"
#include "RadioChannelMap.h"

#include <QMetaType>

class RadioReader
{
public:
    struct ChannelInfo
    {
        ChannelInfo()
        {
            adapter = -1;
            type = RadioReaderInterface::DT_NONE;
            country = QLocale::AnyCountry;
            index = -1;
            channel = 0;
            freq = 0;
            signal = 0.0f;
        }

        long adapter;
        RadioReaderInterface::DemodulatorType type;
        QLocale::Country country;
        int index;
        int channel;
        uint64_t freq;
        float signal;
    };

private:
    RadioReader();
    ~RadioReader();

public:
    static RadioReader& getInstance();

public:
    bool isSupport() const;
    bool isOpened() const;

    bool setChannelCountry(QLocale::Country country, RadioReaderInterface::DemodulatorType type);
    void getChannelCountry(QLocale::Country *country, RadioReaderInterface::DemodulatorType *type) const;
    void getChannels(QVector<RadioChannelMap::Channel> *ret) const;

    float getSignalStrength() const;
    int getSignalStrengthInLinear(float strength) const;
    uint8_t getSignalStrengthInPercentage(float strength) const;

    bool setDecimation(int decimation);
    int getDecimation() const;

    void setAudioGain(float db);
    float getAudioGain() const;

    void setAntenna(const QString &antenna);
    QStringList getAntennas() const;

    void getAdapter(long *adapter, RadioReaderInterface::DemodulatorType *type) const;
    bool getAdapterInfo(long adapter, RadioReaderInterface::AdapterInfo *ret) const;
    bool getAdapterList(QVector<RadioReaderInterface::AdapterInfo> *ret) const;

    bool prepareScanning(long adapter, RadioReaderInterface::DemodulatorType type);
    bool scan(QLocale::Country country, const RadioChannelMap::Channel &channel, int index, ChannelInfo *ret);
    void finishScanning();

    void setScannedChannels(const QVector<ChannelInfo> &list);
    void getScannedChannels(QVector<ChannelInfo> *ret) const;

    QString demodulatorTypeToString(RadioReaderInterface::DemodulatorType type) const;

public:
    static URLProtocol* getProtocol();

#if !defined Q_OS_ANDROID && !defined Q_OS_IOS
    static bool determinRadio(const QString &path);
    static QString makeRadioPath(const RadioReader::ChannelInfo &info);
#endif

public:
    static const QString RADIO_PROTOCOL;

    static const float MAX_SIGNAL_STRENGTH_LINEAR;
    static const float MIN_SIGNAL_STRENGTH_LINEAR;

private:
    void setAdapter(long adapter, RadioReaderInterface::DemodulatorType type);
    bool setTunningParameters(QLocale::Country country, RadioReaderInterface::DemodulatorType type, int index);
    bool tune(QLocale::Country country, RadioReaderInterface::DemodulatorType type, int index);
    float adjustSignalStrength(float strength) const;

private:
    static int open(URLContext *h, const char *uri, int);
    static int read(URLContext *h, uint8_t *buf, int size);
    static int close(URLContext *h);

private:
    static URLProtocol PROTOCOL;
    static const char RADIO_PROTOCOL_NAME[];
    static const float MAX_SIGNAL_STRENGTH;
    static const float MIN_SIGNAL_STRENGTH;
    static const int SIGNAL_STRENGTH_SCALE;

private:
    RadioReaderInterface *m_reader;
    AVIOInterruptCB m_intCallback;
    RadioChannelMap m_channelMap;
    QVector<ChannelInfo> m_scannedChannel;
    bool m_isOpened;
};

Q_DECLARE_METATYPE(RadioReader::ChannelInfo)
QDataStream& operator << (QDataStream &out, const RadioReader::ChannelInfo &item);
QDataStream& operator >> (QDataStream &in, RadioReader::ChannelInfo &item);
