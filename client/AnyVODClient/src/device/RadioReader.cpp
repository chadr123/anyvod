﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "RadioReader.h"
#include "core/Common.h"
#include "utils/SeparatingUtils.h"
#include "setting/RadioReaderSettingLoader.h"
#include "setting/RadioReaderSettingSaver.h"

#include "../../../../common/size.h"

#if defined Q_OS_LINUX && !defined Q_OS_ANDROID
# include "radio/RadioGNURadio.h"
#endif

#include <QUrl>
#include <QUrlQuery>
#include <QDataStream>
#include <QThread>
#include <QDebug>

#include <bass/bass.h>
#include <bass/bass_fx.h>

extern "C"
{
# include <libavformat/avformat.h>
}

QDataStream& operator << (QDataStream &out, const RadioReader::ChannelInfo &item)
{
    out << (int)item.adapter;
    out << (int)item.type;
    out << (int)item.country;
    out << (quint64)item.freq;
    out << item.index;
    out << item.channel;
    out << item.signal;

    return out;
}

QDataStream& operator >> (QDataStream &in, RadioReader::ChannelInfo &item)
{
    int tmp;

    in >> tmp;
    item.adapter = tmp;

    in >> tmp;
    item.type = (RadioReaderInterface::DemodulatorType)tmp;

    in >> tmp;
    item.country = (QLocale::Country)tmp;

    quint64 tmp2;

    in >> tmp2;
    item.freq = tmp2;

    in >> item.index;
    in >> item.channel;
    in >> item.signal;

    return in;
}

const char RadioReader::RADIO_PROTOCOL_NAME[] = "radio";
const int RadioReader::SIGNAL_STRENGTH_SCALE = 10000;
const float RadioReader::MAX_SIGNAL_STRENGTH = 21.0f;
const float RadioReader::MIN_SIGNAL_STRENGTH = -14.0f;
const float RadioReader::MAX_SIGNAL_STRENGTH_LINEAR = BASS_BFX_dB2Linear(RadioReader::MAX_SIGNAL_STRENGTH) * RadioReader::SIGNAL_STRENGTH_SCALE;
const float RadioReader::MIN_SIGNAL_STRENGTH_LINEAR = BASS_BFX_dB2Linear(RadioReader::MIN_SIGNAL_STRENGTH) * RadioReader::SIGNAL_STRENGTH_SCALE;

URLProtocol RadioReader::PROTOCOL = {
    .name = RADIO_PROTOCOL_NAME,
    .url_open = RadioReader::open,
    .url_open2 = nullptr,
    .url_accept = nullptr,
    .url_handshake = nullptr,
    .url_read = RadioReader::read,
    .url_write = nullptr,
    .url_seek = nullptr,
    .url_close = RadioReader::close,
    .url_read_pause = nullptr,
    .url_read_seek = nullptr,
    .url_get_file_handle = nullptr,
    .url_get_multi_file_handle = nullptr,
    .url_get_short_seek = nullptr,
    .url_shutdown = nullptr,
    .priv_data_class = nullptr,
    .priv_data_size = 0,
    .flags = 0,
    .url_check = nullptr,
    .url_open_dir = nullptr,
    .url_read_dir = nullptr,
    .url_close_dir = nullptr,
    .url_delete = nullptr,
    .url_move = nullptr,
    .default_whitelist = nullptr,
};

const QString RadioReader::RADIO_PROTOCOL = QString(RADIO_PROTOCOL_NAME) + PROTOCOL_POSTFIX;

RadioReader::RadioReader() :
    m_reader(nullptr),
    m_isOpened(false)
{
#if defined Q_OS_LINUX && !defined Q_OS_ANDROID
    this->m_reader = new RadioGNURadio();
#endif

    memset(&this->m_intCallback, 0, sizeof(this->m_intCallback));

    RadioReaderSettingLoader(this).load();
}

RadioReader::~RadioReader()
{
    RadioReaderSettingSaver(this).save();

    if (this->m_reader)
    {
        delete this->m_reader;
        this->m_reader = nullptr;
    }
}

RadioReader& RadioReader::getInstance()
{
    static RadioReader instance;

    return instance;
}

URLProtocol* RadioReader::getProtocol()
{
    return &RadioReader::PROTOCOL;
}

#if !defined Q_OS_ANDROID && !defined Q_OS_IOS
QString RadioReader::makeRadioPath(const RadioReader::ChannelInfo &info)
{
    QString url = QString("%1?type=%2&adapter=%3&index=%4&country=%5")
            .arg(RADIO_PROTOCOL)
            .arg((int)info.type)
            .arg(info.adapter)
            .arg(info.index)
            .arg(info.country);

    return url;
}

bool RadioReader::determinRadio(const QString &path)
{
    return path.startsWith(RADIO_PROTOCOL);
}
#endif

bool RadioReader::isSupport() const
{
    return this->m_reader != nullptr;
}

bool RadioReader::isOpened() const
{
    return this->m_isOpened;
}

bool RadioReader::setChannelCountry(QLocale::Country country, RadioReaderInterface::DemodulatorType type)
{
    return this->m_channelMap.setChannelCountry(country, type);
}

void RadioReader::getChannelCountry(QLocale::Country *country, RadioReaderInterface::DemodulatorType *type) const
{
    this->m_channelMap.getChannelCountry(country, type);
}

void RadioReader::getChannels(QVector<RadioChannelMap::Channel> *ret) const
{
    this->m_channelMap.getChannels(ret);
}

float RadioReader::getSignalStrength() const
{
    return this->adjustSignalStrength(this->m_reader->getSignalStrength());
}

int RadioReader::getSignalStrengthInLinear(float strength) const
{
    return BASS_BFX_dB2Linear(strength) * SIGNAL_STRENGTH_SCALE;
}

bool RadioReader::setDecimation(int decimation)
{
    return this->m_reader->setDecimation(decimation);
}

int RadioReader::getDecimation() const
{
    return this->m_reader->getDecimation();
}

void RadioReader::setAudioGain(float db)
{
    this->m_reader->setAudioGain(db);
}

float RadioReader::getAudioGain() const
{
    return this->m_reader->getAudioGain();
}

void RadioReader::setAntenna(const QString &antenna)
{
    this->m_reader->setAntenna(antenna);
}

QStringList RadioReader::getAntennas() const
{
    return this->m_reader->getAntennas();
}

void RadioReader::getAdapter(long *adapter, RadioReaderInterface::DemodulatorType *type) const
{
    this->m_reader->getAdapter(adapter, type);
}

bool RadioReader::getAdapterInfo(long adapter, RadioReaderInterface::AdapterInfo *ret) const
{
    QVector<RadioReaderInterface::AdapterInfo> adapters;

    if (!this->getAdapterList(&adapters))
        return false;

    if (adapters.length() - 1 >= adapter)
    {
        *ret = adapters[(int)adapter];
        return true;
    }

    return false;
}

bool RadioReader::getAdapterList(QVector<RadioReaderInterface::AdapterInfo> *ret) const
{
    return this->m_reader->getAdapterList(ret);
}

bool RadioReader::prepareScanning(long adapter, RadioReaderInterface::DemodulatorType type)
{
    this->m_reader->setAdapter(adapter, type);

    return this->m_reader->open(QString()) && this->m_reader->tune();
}

void RadioReader::finishScanning()
{
    this->m_reader->close();
    this->m_reader->reset();
}

bool RadioReader::scan(QLocale::Country country, const RadioChannelMap::Channel &channel, int index, ChannelInfo *ret)
{
    this->m_reader->getAdapter(&ret->adapter, &ret->type);

    ret->channel = channel.channel;
    ret->index = index;

    if (!this->setTunningParameters(country, ret->type, index))
        return false;

    QThread::msleep(300);
    ret->signal = this->adjustSignalStrength(this->getSignalStrength());

    if (ret->signal < -0.7f)
        return false;

    return true;
}

void RadioReader::setScannedChannels(const QVector<RadioReader::ChannelInfo> &list)
{
    this->m_scannedChannel = list;
}

void RadioReader::getScannedChannels(QVector<RadioReader::ChannelInfo> *ret) const
{
    *ret = this->m_scannedChannel;
}

void RadioReader::setAdapter(long adapter, RadioReaderInterface::DemodulatorType type)
{
    this->m_reader->setAdapter(adapter, type);
}

bool RadioReader::setTunningParameters(QLocale::Country country, RadioReaderInterface::DemodulatorType type, int index)
{
    RadioChannelMap::Channel channelInfo;

    if (!this->m_channelMap.setChannelCountry(country, type))
        return false;

    this->m_channelMap.getChannel(index, &channelInfo);

    this->m_reader->enableAGC(true);
    this->m_reader->setFrequency(channelInfo.freq);
    this->m_reader->setEmphasis(channelInfo.emphasis);

    return true;
}

bool RadioReader::tune(QLocale::Country country, RadioReaderInterface::DemodulatorType type, int index)
{
    return this->setTunningParameters(country, type, index) && this->m_reader->tune();
}

float RadioReader::adjustSignalStrength(float strength) const
{
    if (std::isnan(strength))
        strength = MIN_SIGNAL_STRENGTH;
    else if (strength < MIN_SIGNAL_STRENGTH)
        strength = MIN_SIGNAL_STRENGTH;
    else if (strength > MAX_SIGNAL_STRENGTH)
        strength = MAX_SIGNAL_STRENGTH;

    return strength;
}

uint8_t RadioReader::getSignalStrengthInPercentage(float strength) const
{
    uint8_t value = uint8_t(100.0 * (this->getSignalStrengthInLinear(strength) / (MAX_SIGNAL_STRENGTH_LINEAR - MIN_SIGNAL_STRENGTH_LINEAR)));

    return value > 100 ? 100 : value;
}

int RadioReader::open(URLContext *h, const char *uri, int)
{
    QString realUri = QString::fromUtf8(uri);
    int index = realUri.lastIndexOf(SeparatingUtils::FFMPEG_SEPARATOR);

    if (index != -1)
    {
        RadioReader *me = nullptr;
        uint64_t id = 0;
        QString parentID;

        parentID = realUri.mid(index + 1);
        id = parentID.toULongLong();
        me = (RadioReader*)id;

        h->priv_data = me;

        realUri = realUri.mid(0, index);

        char proto[strlen(RADIO_PROTOCOL_NAME) + 1];
        char path[MAX_FILEPATH_CHAR_SIZE];

        av_url_split(proto, (int)sizeof(proto), nullptr, 0, nullptr, 0, nullptr, path, (int)sizeof(path), realUri.toUtf8().constData());

        if (strcmp(proto, RADIO_PROTOCOL_NAME))
            return AVERROR(EINVAL);

        QUrl url(realUri);
        QUrlQuery query(url.query());
        long adapter;
        RadioReaderInterface::DemodulatorType type;

        adapter = query.queryItemValue("adapter").toInt();
        type = (RadioReaderInterface::DemodulatorType)query.queryItemValue("type").toInt();

        me->setAdapter(adapter, type);

        if (me->m_reader->open(realUri))
        {
            QLocale::Country country;
            int index;

            country = (QLocale::Country)query.queryItemValue("country").toInt();
            index = query.queryItemValue("index").toInt();

            if (!me->tune(country, type, index))
            {
                me->m_reader->close();
                return AVERROR(EFAULT);
            }

            me->m_intCallback = h->interrupt_callback;
            me->m_isOpened = true;

            return 0;
        }
        else
        {
            return AVERROR(ENOENT);
        }
    }
    else
    {
        return AVERROR(EINVAL);
    }
}

int RadioReader::read(URLContext *h, uint8_t *buf, int size)
{
    RadioReader *me = (RadioReader*)h->priv_data;
    int index = 0;

    while (size > 0)
    {
        int ret;

        if (me->m_intCallback.callback && me->m_intCallback.callback(me->m_intCallback.opaque))
            return AVERROR_EXIT;

        ret = me->m_reader->read(buf + index, size);

        index += ret;
        size -= ret;
    }

     return index;
}

int RadioReader::close(URLContext *h)
{
    RadioReader *me = (RadioReader*)h->priv_data;

    me->m_reader->close();
    me->m_isOpened = false;

    return 0;
}

QString RadioReader::demodulatorTypeToString(RadioReaderInterface::DemodulatorType type) const
{
    QString name;

    switch (type)
    {
        case RadioReaderInterface::DT_WIDE_FM_MONO:
            name = "Wide FM Mono";
            break;
        case RadioReaderInterface::DT_WIDE_FM_STEREO:
            name = "Wide FM Stereo";
            break;
        default:
            break;
    }

    return name;
}
