﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#define WIN32_LEAN_AND_MEAN
#include <wtypes.h>
#include <unknwn.h>
#include <ole2.h>
#include <limits.h>

#define NO_DSHOW_STRSAFE
#include <dshow.h>
#include <comcat.h>

class IDigitalCableTuneRequest;
class IDigitalCableLocator;
class IATSCChannelTuneRequest;
class IATSCLocator;
class IBDA_DeviceControl;
class IBDA_FrequencyFilter;
class IBDA_SignalStatistics;
class IBDA_Topology;
class IChannelTuneRequest;
class IComponent;
class IComponents;
class IComponentType;
class IComponentTypes;
class IDVBCLocator;
class IDVBSLocator;
class IDVBSTuningSpace;
class IDVBTLocator;
class IDVBTLocator2;
class IDVBTuneRequest;
class IDVBTuningSpace;
class IDVBTuningSpace2;
class IEnumComponents;
class IEnumComponentTypes;
class IEnumTuningSpaces;
class ILocator;
class ISampleGrabber;
class ISampleGrabberCB;
class IScanningTuner;
class ITuner;
class ITunerCap;
class ITuneRequest;
class ITuningSpace;
class ITuningSpaceContainer;
class ITuningSpaces;
class IMpeg2Data;
class IGuideData;
class IGuideDataEvent;
class ISectionList;
class IEnumTuneRequests;
class IEnumGuideDataProperties;
class IGuideDataProperty;
class IMpeg2Stream;

typedef enum BinaryConvolutionCodeRate
{
    BDA_BCC_RATE_NOT_SET = -1,
    BDA_BCC_RATE_NOT_DEFINED=0,
    BDA_BCC_RATE_1_2 = 1,
    BDA_BCC_RATE_2_3,
    BDA_BCC_RATE_3_4,
    BDA_BCC_RATE_3_5,
    BDA_BCC_RATE_4_5,
    BDA_BCC_RATE_5_6,
    BDA_BCC_RATE_5_11,
    BDA_BCC_RATE_7_8,
    BDA_BCC_RATE_MAX,
} BinaryConvolutionCodeRate;

typedef enum ComponentCategory
{
    CategoryNotSet = -1,
    CategoryOther=0,
    CategoryVideo,
    CategoryAudio,
    CategoryText,
    CategoryData,
} ComponentCategory;

typedef enum ComponentStatus
{
    StatusActive,
    StatusInactive,
    StatusUnavailable,
} ComponentStatus;

typedef enum DVBSystemType
{
    DVB_Cable,
    DVB_Terrestrial,
    DVB_Satellite,
} DVBSystemType;

typedef enum FECMethod
{
    BDA_FEC_METHOD_NOT_SET = -1,
    BDA_FEC_METHOD_NOT_DEFINED=0,
    BDA_FEC_VITERBI = 1,
    BDA_FEC_RS_204_188,
    BDA_FEC_MAX,
} FECMethod;

typedef enum GuardInterval
{
    BDA_GUARD_NOT_SET = -1,
    BDA_GUARD_NOT_DEFINED=0,
    BDA_GUARD_1_32 = 1,
    BDA_GUARD_1_16,
    BDA_GUARD_1_8,
    BDA_GUARD_1_4,
    BDA_GUARD_MAX,
} GuardInterval;

typedef enum HierarchyAlpha
{
    BDA_HALPHA_NOT_SET = -1,
    BDA_HALPHA_NOT_DEFINED=0,
    BDA_HALPHA_1 = 1,
    BDA_HALPHA_2,
    BDA_HALPHA_4,
    BDA_HALPHA_MAX,
} HierarchyAlpha;

typedef enum ModulationType
{
    BDA_MOD_NOT_SET = -1,
    BDA_MOD_NOT_DEFINED=0,
    BDA_MOD_16QAM = 1,
    BDA_MOD_32QAM,
    BDA_MOD_64QAM,
    BDA_MOD_80QAM,
    BDA_MOD_96QAM,
    BDA_MOD_112QAM,
    BDA_MOD_128QAM,
    BDA_MOD_160QAM,
    BDA_MOD_192QAM,
    BDA_MOD_224QAM,
    BDA_MOD_256QAM,
    BDA_MOD_320QAM,
    BDA_MOD_384QAM,
    BDA_MOD_448QAM,
    BDA_MOD_512QAM,
    BDA_MOD_640QAM,
    BDA_MOD_768QAM,
    BDA_MOD_896QAM,
    BDA_MOD_1024QAM,
    BDA_MOD_QPSK,
    BDA_MOD_BPSK,
    BDA_MOD_OQPSK,
    BDA_MOD_8VSB,
    BDA_MOD_16VSB,
    BDA_MOD_ANALOG_AMPLITUDE,
    BDA_MOD_ANALOG_FREQUENCY,
    BDA_MOD_MAX,
} ModulationType;

typedef enum Polarisation
{
    BDA_POLARISATION_NOT_SET     = -1,
    BDA_POLARISATION_NOT_DEFINED = 0,
    BDA_POLARISATION_LINEAR_H    = 1,
    BDA_POLARISATION_LINEAR_V    = 2,
    BDA_POLARISATION_CIRCULAR_L  = 3,
    BDA_POLARISATION_CIRCULAR_R  = 4,
    BDA_POLARISATION_MAX         = 5
} Polarisation;

typedef enum SpectralInversion
{
    BDA_SPECTRAL_INVERSION_NOT_SET = -1,
    BDA_SPECTRAL_INVERSION_NOT_DEFINED = 0,
    BDA_SPECTRAL_INVERSION_AUTOMATIC = 1,
    BDA_SPECTRAL_INVERSION_NORMAL,
    BDA_SPECTRAL_INVERSION_INVERTED,
    BDA_SPECTRAL_INVERSION_MAX
} SpectralInversion;

typedef enum TransmissionMode
{
    BDA_XMIT_MODE_NOT_SET = -1,
    BDA_XMIT_MODE_NOT_DEFINED=0,
    BDA_XMIT_MODE_2K = 1,
    BDA_XMIT_MODE_8K,
    BDA_XMIT_MODE_MAX,
} TransmissionMode;

typedef struct _BDANODE_DESCRIPTOR
{
    ULONG               ulBdaNodeType;
    GUID                guidFunction;
    GUID                guidName;
} BDANODE_DESCRIPTOR, *PBDANODE_DESCRIPTOR;

typedef struct _BDA_TEMPLATE_CONNECTION
{
    ULONG   FromNodeType;
    ULONG   FromNodePinType;
    ULONG   ToNodeType;
    ULONG   ToNodePinType;
} BDA_TEMPLATE_CONNECTION, *PBDA_TEMPLATE_CONNECTION;

typedef struct _BDA_TEMPLATE_PIN_JOINT
{
    ULONG   uliTemplateConnection;
    ULONG   ulcInstancesMax;
} BDA_TEMPLATE_PIN_JOINT, *PBDA_TEMPLATE_PIN_JOINT;

class IComponent : public IDispatch
{
public:
    virtual HRESULT __stdcall get_Type( IComponentType** p_p_cpt_type )=0;
    virtual HRESULT __stdcall put_Type( IComponentType* p_cpt_type )=0;
    virtual HRESULT __stdcall get_DescLangID( long* p_l_language )=0;
    virtual HRESULT __stdcall put_DescLangID( long l_language )=0;
    virtual HRESULT __stdcall get_Status( ComponentStatus* p_status )=0;
    virtual HRESULT __stdcall put_Status( ComponentStatus status )=0;
    virtual HRESULT __stdcall get_Description( BSTR* p_bstr_desc )=0;
    virtual HRESULT __stdcall put_Description( BSTR bstr_desc )=0;
    virtual HRESULT __stdcall Clone( IComponent** p_p_component )=0;
};

class IComponents : public IDispatch
{
public:
    virtual HRESULT __stdcall get_Count( long* pl_count )=0;
    virtual HRESULT __stdcall get__NewEnum( IEnumVARIANT** p_p_enum )=0;
    virtual HRESULT __stdcall EnumComponents( IEnumComponents** p_p_enum )=0;
    virtual HRESULT __stdcall get_Item( VARIANT Index,
        IComponent** p_p_component )=0;
    virtual HRESULT __stdcall Add( IComponent* p_component,
        VARIANT* v_index )=0;
    virtual HRESULT __stdcall Remove( VARIANT v_index )=0;
    virtual HRESULT __stdcall Clone( IComponents** p_p_cpts )=0;
};

class IComponentType : public IDispatch
{
public:
    virtual HRESULT __stdcall get_Category( ComponentCategory* p_category )=0;
    virtual HRESULT __stdcall put_Category( ComponentCategory category )=0;
    virtual HRESULT __stdcall get_MediaMajorType( BSTR* p_bstr_major_type )=0;
    virtual HRESULT __stdcall put_MediaMajorType( BSTR bstr_major_type )=0;
    virtual HRESULT __stdcall get__MediaMajorType( GUID* p_guid_major_type )=0;
    virtual HRESULT __stdcall put__MediaMajorType( REFCLSID guid_major_type )=0;
    virtual HRESULT __stdcall get_MediaSubType( BSTR* p_bstr_sub_type )=0;
    virtual HRESULT __stdcall put_MediaSubType( BSTR bstr_sub_type )=0;
    virtual HRESULT __stdcall get__MediaSubType( GUID* p_guid_sub_type )=0;
    virtual HRESULT __stdcall put__MediaSubType( REFCLSID guid_sub_type )=0;
    virtual HRESULT __stdcall get_MediaFormatType( BSTR* p_bstr_format_type )=0;
    virtual HRESULT __stdcall put_MediaFormatType( BSTR bstr_format_type )=0;
    virtual HRESULT __stdcall get__MediaFormatType(
        GUID* p_guid_format_type )=0;
    virtual HRESULT __stdcall put__MediaFormatType(
        REFCLSID guid_format_type )=0;
    virtual HRESULT __stdcall get_MediaType( AM_MEDIA_TYPE* p_media_type )=0;
    virtual HRESULT __stdcall put_MediaType( AM_MEDIA_TYPE* p_media_type )=0;
    virtual HRESULT __stdcall Clone( IComponentType** p_p_cpt_type )=0;
};

class IComponentTypes : public IDispatch
{
public:
    virtual HRESULT __stdcall get_Count( long* l_count )=0;
    virtual HRESULT __stdcall get__NewEnum( IEnumVARIANT** p_p_enum )=0;
    virtual HRESULT __stdcall EnumComponentTypes(
        IEnumComponentTypes** p_p_enum )=0;
    virtual HRESULT __stdcall get_Item( VARIANT v_index,
        IComponentType** p_p_cpt_type )=0;
    virtual HRESULT __stdcall put_Item( VARIANT v_index,
        IComponentType* p_cpt_type )=0;
    virtual HRESULT __stdcall Add( IComponentType* p_cpt_type,
        VARIANT* v_index )=0;
    virtual HRESULT __stdcall Remove( VARIANT v_index )=0;
    virtual HRESULT __stdcall Clone( IComponentTypes** p_p_cpt_types )=0;
};

class IEnumComponents : public IUnknown
{
public:
    virtual HRESULT __stdcall Next( ULONG num_elem, IComponent** p_p_elem,
        ULONG* p_num_elem_fetch )=0;
    virtual HRESULT __stdcall Skip( ULONG num_elem )=0;
    virtual HRESULT __stdcall Reset( void )=0;
    virtual HRESULT __stdcall Clone( IEnumComponents** p_p_enum )=0;
};

class IEnumComponentTypes : public IUnknown
{
public:
    virtual HRESULT __stdcall Next( ULONG num_elem, IComponentType** p_p_elem,
        ULONG* p_num_elem_fetch )=0;
    virtual HRESULT __stdcall Skip( ULONG num_elem )=0;
    virtual HRESULT __stdcall Reset( void )=0;
    virtual HRESULT __stdcall Clone( IEnumComponentTypes** p_p_enum )=0;
};

class IEnumTuningSpaces : public IUnknown
{
public:
    virtual HRESULT __stdcall Next( ULONG l_num_elem,
        ITuningSpace** p_p_tuning_space, ULONG* pl_num_elem_fetched )=0;
    virtual HRESULT __stdcall Skip( ULONG l_num_elem )=0;
    virtual HRESULT __stdcall Reset( void )=0;
    virtual HRESULT __stdcall Clone( IEnumTuningSpaces** p_p_enum )=0;
};

class ITunerCap : public IUnknown
{
public:
    virtual HRESULT __stdcall get_AuxInputCount( ULONG* pulCompositeCount,
        ULONG* pulSvideoCount )=0;
    virtual HRESULT __stdcall get_SupportedNetworkTypes(
        ULONG ulcNetworkTypesMax, ULONG* pulcNetworkTypes,
        GUID* pguidNetworkTypes )=0;
    virtual HRESULT __stdcall get_SupportedVideoFormats(
        ULONG* pulAMTunerModeType, ULONG* pulAnalogVideoStandard )=0;
};


class ITuner : public IUnknown
{
public:
    virtual HRESULT __stdcall get_TuningSpace(
        ITuningSpace** p_p_tuning_space )=0;
    virtual HRESULT __stdcall put_TuningSpace( ITuningSpace* p_tuning_space )=0;
    virtual HRESULT __stdcall EnumTuningSpaces(
       IEnumTuningSpaces** p_p_enum )=0;
    virtual HRESULT __stdcall get_TuneRequest(
        ITuneRequest** p_p_tune_request )=0;
    virtual HRESULT __stdcall put_TuneRequest( ITuneRequest* p_tune_request )=0;
    virtual HRESULT __stdcall Validate( ITuneRequest* p_tune_request )=0;
    virtual HRESULT __stdcall get_PreferredComponentTypes(
        IComponentTypes** p_p_cpt_types )=0;
    virtual HRESULT __stdcall put_PreferredComponentTypes(
        IComponentTypes* p_cpt_types )=0;
    virtual HRESULT __stdcall get_SignalStrength( long* l_sig_strength )=0;
    virtual HRESULT __stdcall TriggerSignalEvents( long l_interval )=0;
};

class ISampleGrabber : public IUnknown
{
public:
    virtual HRESULT __stdcall SetOneShot( BOOL b_one_shot )=0;
    virtual HRESULT __stdcall SetMediaType(
        const AM_MEDIA_TYPE* p_media_type )=0;
    virtual HRESULT __stdcall GetConnectedMediaType(
        AM_MEDIA_TYPE* p_media_type )=0;
    virtual HRESULT __stdcall SetBufferSamples( BOOL b_buffer_samples )=0;
    virtual HRESULT __stdcall GetCurrentBuffer( long* p_buff_size,
        long* p_buffer )=0;
    virtual HRESULT __stdcall GetCurrentSample( IMediaSample** p_p_sample )=0;
    virtual HRESULT __stdcall SetCallback( ISampleGrabberCB* pf_callback,
        long l_callback_type )=0;
};

class ISampleGrabberCB : public IUnknown
{
public:
    virtual HRESULT __stdcall SampleCB( double d_sample_time,
        IMediaSample* p_sample )=0;
    virtual HRESULT __stdcall BufferCB( double d_sample_time, BYTE *p_buffer,
        long l_bufferLen )=0;
};

class IScanningTuner : public ITuner
{
public:
    virtual HRESULT __stdcall SeekUp( void )=0;
    virtual HRESULT __stdcall SeekDown( void )=0;
    virtual HRESULT __stdcall ScanDown( long l_pause )=0;
    virtual HRESULT __stdcall ScanUp( long l_pause )=0;
    virtual HRESULT __stdcall AutoProgram( void )=0;
};

class ITuneRequest : public IDispatch
{
public:
    virtual HRESULT __stdcall get_TuningSpace(
        ITuningSpace** p_p_tuning_space )=0;
    virtual HRESULT __stdcall get_Components( IComponents** p_p_components )=0;
    virtual HRESULT __stdcall Clone( ITuneRequest** p_p_tune_request )=0;
    virtual HRESULT __stdcall get_Locator( ILocator** p_p_locator )=0;
    virtual HRESULT __stdcall put_Locator( ILocator* p_locator )=0;
};

class IChannelTuneRequest : public ITuneRequest
{
public:
    virtual HRESULT __stdcall get_Channel( long* pl_channel )=0;
    virtual HRESULT __stdcall put_Channel( long l_channel )=0;
};

class IATSCChannelTuneRequest : public IChannelTuneRequest
{
public:
    virtual HRESULT __stdcall get_MinorChannel( long* pl_minor_channel )=0;
    virtual HRESULT __stdcall put_MinorChannel( long l_minor_channel )=0;
};

class IDigitalCableTuneRequest : public IATSCChannelTuneRequest
{
public:
    virtual HRESULT __stdcall get_MajorChannel( long* pl_major_channel )=0;
    virtual HRESULT __stdcall put_MajorChannel( long l_major_channel )=0;
    virtual HRESULT __stdcall get_SourceID( long* pl_source_id )=0;
    virtual HRESULT __stdcall put_SourceID( long l_source_id )=0;
};

class IDVBTuneRequest : public ITuneRequest
{
public:
    virtual HRESULT __stdcall get_ONID( long* pl_onid )=0;
    virtual HRESULT __stdcall put_ONID( long l_onid )=0;
    virtual HRESULT __stdcall get_TSID( long* pl_tsid )=0;
    virtual HRESULT __stdcall put_TSID( long l_tsid )=0;
    virtual HRESULT __stdcall get_SID( long* pl_sid )=0;
    virtual HRESULT __stdcall put_SID( long l_sid )=0;
};

class ILocator : public IDispatch
{
public:
    virtual HRESULT __stdcall get_CarrierFrequency( long* pl_frequency )=0;
    virtual HRESULT __stdcall put_CarrierFrequency( long l_frequency )=0;
    virtual HRESULT __stdcall get_InnerFEC( FECMethod* FEC )=0;
    virtual HRESULT __stdcall put_InnerFEC( FECMethod FEC )=0;
    virtual HRESULT __stdcall get_InnerFECRate(
        BinaryConvolutionCodeRate* FEC )=0;
    virtual HRESULT __stdcall put_InnerFECRate(
        BinaryConvolutionCodeRate FEC )=0;
    virtual HRESULT __stdcall get_OuterFEC( FECMethod* FEC )=0;
    virtual HRESULT __stdcall put_OuterFEC( FECMethod FEC )=0;
    virtual HRESULT __stdcall get_OuterFECRate(
        BinaryConvolutionCodeRate* FEC )=0;
    virtual HRESULT __stdcall put_OuterFECRate(
        BinaryConvolutionCodeRate FEC )=0;
    virtual HRESULT __stdcall get_Modulation( ModulationType* p_modulation )=0;
    virtual HRESULT __stdcall put_Modulation( ModulationType modulation )=0;
    virtual HRESULT __stdcall get_SymbolRate( long* pl_rate )=0;
    virtual HRESULT __stdcall put_SymbolRate( long l_rate )=0;
    virtual HRESULT __stdcall Clone( ILocator** p_p_locator )=0;
};

class IATSCLocator : public ILocator
{
public:
    virtual HRESULT __stdcall get_PhysicalChannel( long* pl_phys_channel )=0;
    virtual HRESULT __stdcall put_PhysicalChannel( long l_phys_channel )=0;
    virtual HRESULT __stdcall get_TSID( long* pl_tsid )=0;
    virtual HRESULT __stdcall put_TSID( long l_tsid )=0;
};

class IATSCLocator2 : public IATSCLocator
{
public:
    virtual HRESULT __stdcall get_ProgramNumber( long* pl_prog_number )=0;
    virtual HRESULT __stdcall put_ProgramNumber( long l_prog_number )=0;
};

class IDigitalCableLocator : public IATSCLocator2
{
public:
};

class IDVBCLocator : public ILocator
{
public:
};

class IDVBSLocator : public ILocator
{
public:
    virtual HRESULT __stdcall get_SignalPolarisation(
        Polarisation* p_polarisation )=0;
    virtual HRESULT __stdcall put_SignalPolarisation(
        Polarisation polarisation )=0;
    virtual HRESULT __stdcall get_WestPosition( VARIANT_BOOL* pb_west )=0;
    virtual HRESULT __stdcall put_WestPosition( VARIANT_BOOL b_west )=0;
    virtual HRESULT __stdcall get_OrbitalPosition( long* pl_longitude )=0;
    virtual HRESULT __stdcall put_OrbitalPosition( long l_longitude )=0;
    virtual HRESULT __stdcall get_Azimuth( long* pl_azimuth )=0;
    virtual HRESULT __stdcall put_Azimuth( long l_azimuth )=0;
    virtual HRESULT __stdcall get_Elevation( long* pl_elevation )=0;
    virtual HRESULT __stdcall put_Elevation( long l_elevation )=0;
};

class IDVBTLocator : public ILocator
{
public:
    virtual HRESULT __stdcall get_Bandwidth( long* pl_bandwidth )=0;
    virtual HRESULT __stdcall put_Bandwidth( long l_bandwidth )=0;
    virtual HRESULT __stdcall get_LPInnerFEC( FECMethod* FEC )=0;
    virtual HRESULT __stdcall put_LPInnerFEC( FECMethod FEC )=0;
    virtual HRESULT __stdcall get_LPInnerFECRate(
        BinaryConvolutionCodeRate* FEC )=0;
    virtual HRESULT __stdcall put_LPInnerFECRate(
        BinaryConvolutionCodeRate FEC )=0;
    virtual HRESULT __stdcall get_HAlpha( HierarchyAlpha* Alpha )=0;
    virtual HRESULT __stdcall put_HAlpha( HierarchyAlpha Alpha )=0;
    virtual HRESULT __stdcall get_Guard( GuardInterval* GI )=0;
    virtual HRESULT __stdcall put_Guard( GuardInterval GI )=0;
    virtual HRESULT __stdcall get_Mode( TransmissionMode* mode )=0;
    virtual HRESULT __stdcall put_Mode( TransmissionMode mode )=0;
    virtual HRESULT __stdcall get_OtherFrequencyInUse(
        VARIANT_BOOL* OtherFrequencyInUseVal )=0;
    virtual HRESULT __stdcall put_OtherFrequencyInUse(
        VARIANT_BOOL OtherFrequencyInUseVal )=0;
};

class IDVBTLocator2 : public IDVBTLocator
{
public:
    virtual HRESULT __stdcall get_PhysicalLayerPipeId(
            LONG *PhysicalLayerPipeIdVal )=0;
    virtual HRESULT __stdcall put_PhysicalLayerPipeId(
            LONG PhysicalLayerPipeIdVal )=0;
};

class ITuningSpace : public IDispatch
{
public:
    virtual HRESULT __stdcall get_UniqueName( BSTR* p_bstr_name )=0;
    virtual HRESULT __stdcall put_UniqueName( BSTR Name )=0;
    virtual HRESULT __stdcall get_FriendlyName( BSTR* p_bstr_name )=0;
    virtual HRESULT __stdcall put_FriendlyName( BSTR bstr_name )=0;
    virtual HRESULT __stdcall get_CLSID( BSTR* bstr_clsid )=0;
    virtual HRESULT __stdcall get_NetworkType( BSTR* p_bstr_network_guid )=0;
    virtual HRESULT __stdcall put_NetworkType( BSTR bstr_network_guid )=0;
    virtual HRESULT __stdcall get__NetworkType( GUID* p_guid_network_guid )=0;
    virtual HRESULT __stdcall put__NetworkType( REFCLSID clsid_network_guid )=0;
    virtual HRESULT __stdcall CreateTuneRequest(
        ITuneRequest** p_p_tune_request )=0;
    virtual HRESULT __stdcall EnumCategoryGUIDs( IEnumGUID** p_p_enum )=0;
    virtual HRESULT __stdcall EnumDeviceMonikers( IEnumMoniker** p_p_enum )=0;
    virtual HRESULT __stdcall get_DefaultPreferredComponentTypes(
        IComponentTypes** p_p_cpt_types )=0;
    virtual HRESULT __stdcall put_DefaultPreferredComponentTypes(
        IComponentTypes* p_cpt_types )=0;
    virtual HRESULT __stdcall get_FrequencyMapping( BSTR* p_bstr_mapping )=0;
    virtual HRESULT __stdcall put_FrequencyMapping( BSTR bstr_mapping )=0;
    virtual HRESULT __stdcall get_DefaultLocator( ILocator** p_p_locator )=0;
    virtual HRESULT __stdcall put_DefaultLocator( ILocator* p_locator )=0;
    virtual HRESULT __stdcall Clone( ITuningSpace** p_p_tuning_space )=0;
};

class IDVBTuningSpace : public ITuningSpace
{
public:
    virtual HRESULT __stdcall get_SystemType( DVBSystemType* p_sys_type )=0;
    virtual HRESULT __stdcall put_SystemType( DVBSystemType sys_type )=0;
};

class IDVBTuningSpace2 : public IDVBTuningSpace
{
public:
    virtual HRESULT __stdcall get_NetworkID( long* p_l_network_id )=0;
    virtual HRESULT __stdcall put_NetworkID( long l_network_id )=0;
};

class IDVBSTuningSpace : public IDVBTuningSpace2
{
public:
    virtual HRESULT __stdcall get_LowOscillator( long* p_l_low_osc )=0;
    virtual HRESULT __stdcall put_LowOscillator( long l_low_osc )=0;
    virtual HRESULT __stdcall get_HighOscillator( long* p_l_high_osc )=0;
    virtual HRESULT __stdcall put_HighOscillator( long l_high_osc )=0;
    virtual HRESULT __stdcall get_LNBSwitch( long* p_l_lnb_switch )=0;
    virtual HRESULT __stdcall put_LNBSwitch( long l_lnb_switch )=0;
    virtual HRESULT __stdcall get_InputRange( BSTR* p_bstr_input_range )=0;
    virtual HRESULT __stdcall put_InputRange( BSTR bstr_input_range )=0;
    virtual HRESULT __stdcall get_SpectralInversion(
        SpectralInversion* p_spectral_inv )=0;
    virtual HRESULT __stdcall put_SpectralInversion(
        SpectralInversion spectral_inv )=0;
};

class ITuningSpaceContainer : public IDispatch
{
public:
    virtual HRESULT __stdcall get_Count( long* l_count )=0;
    virtual HRESULT __stdcall get__NewEnum( IEnumVARIANT** p_p_enum )=0;
    virtual HRESULT __stdcall get_Item( VARIANT v_index,
        ITuningSpace** p_p_tuning_space )=0;
    virtual HRESULT __stdcall put_Item( VARIANT v_index,
        ITuningSpace* p_tuning_space )=0;
    virtual HRESULT __stdcall TuningSpacesForCLSID( BSTR bstr_clsid,
        ITuningSpaces** p_p_tuning_spaces )=0;
    virtual HRESULT __stdcall _TuningSpacesForCLSID( REFCLSID clsid,
        ITuningSpaces** p_p_tuning_spaces )=0;
    virtual HRESULT __stdcall TuningSpacesForName( BSTR bstr_name,
        ITuningSpaces** p_p_tuning_spaces )=0;
    virtual HRESULT __stdcall FindID( ITuningSpace* p_tuning_space,
        long* l_id )=0;
    virtual HRESULT __stdcall Add( ITuningSpace* p_tuning_space,
        VARIANT* v_index )=0;
    virtual HRESULT __stdcall get_EnumTuningSpaces(
        IEnumTuningSpaces** p_p_enum )=0;
    virtual HRESULT __stdcall Remove( VARIANT v_index )=0;
    virtual HRESULT __stdcall get_MaxCount( long* l_maxcount )=0;
    virtual HRESULT __stdcall put_MaxCount( long l_maxcount )=0;
};

class ITuningSpaces : public IDispatch
{
public:
    virtual HRESULT __stdcall get_Count( long* l_count )=0;
    virtual HRESULT __stdcall get__NewEnum( IEnumVARIANT** p_p_enum )=0;
    virtual HRESULT __stdcall get_Item( VARIANT v_index,
        ITuningSpace** p_p_tuning_space )=0;
    virtual HRESULT __stdcall get_EnumTuningSpaces(
        IEnumTuningSpaces** p_p_enum )=0;
};

class IBDA_DeviceControl : public IUnknown
{
public:
    virtual HRESULT __stdcall StartChanges( void )=0;
    virtual HRESULT __stdcall CheckChanges( void )=0;
    virtual HRESULT __stdcall CommitChanges( void )=0;
    virtual HRESULT __stdcall GetChangeState( ULONG *pState )=0;
};

class IBDA_FrequencyFilter : public IUnknown
{
public:
    virtual HRESULT __stdcall put_Autotune( ULONG ulTransponder )=0;
    virtual HRESULT __stdcall get_Autotune( ULONG *pulTransponder )=0;
    virtual HRESULT __stdcall put_Frequency( ULONG ulFrequency )=0;
    virtual HRESULT __stdcall get_Frequency( ULONG *pulFrequency )=0;
    virtual HRESULT __stdcall put_Polarity( Polarisation Polarity )=0;
    virtual HRESULT __stdcall get_Polarity( Polarisation *pPolarity )=0;
    virtual HRESULT __stdcall put_Range( ULONG ulRange )=0;
    virtual HRESULT __stdcall get_Range( ULONG *pulRange )=0;
    virtual HRESULT __stdcall put_Bandwidth( ULONG ulBandwidth )=0;
    virtual HRESULT __stdcall get_Bandwidth( ULONG *pulBandwidth )=0;
    virtual HRESULT __stdcall put_FrequencyMultiplier( ULONG ulMultiplier )=0;
    virtual HRESULT __stdcall get_FrequencyMultiplier(
        ULONG *pulMultiplier )=0;
};

class IBDA_SignalStatistics : public IUnknown
{
public:
    virtual HRESULT __stdcall put_SignalStrength( LONG lDbStrength )=0;
    virtual HRESULT __stdcall get_SignalStrength( LONG *plDbStrength )=0;
    virtual HRESULT __stdcall put_SignalQuality( LONG lPercentQuality )=0;
    virtual HRESULT __stdcall get_SignalQuality( LONG *plPercentQuality )=0;
    virtual HRESULT __stdcall put_SignalPresent( BOOLEAN fPresent )=0;
    virtual HRESULT __stdcall get_SignalPresent( BOOLEAN *pfPresent )=0;
    virtual HRESULT __stdcall put_SignalLocked( BOOLEAN fLocked )=0;
    virtual HRESULT __stdcall get_SignalLocked( BOOLEAN *pfLocked )=0;
    virtual HRESULT __stdcall put_SampleTime( LONG lmsSampleTime )=0;
    virtual HRESULT __stdcall get_SampleTime( LONG *plmsSampleTime )=0;
};

class IBDA_Topology : public IUnknown
{
public:
    virtual HRESULT __stdcall GetNodeTypes( ULONG *pulcNodeTypes,
        ULONG ulcNodeTypesMax, ULONG rgulNodeTypes[] )=0;
    virtual HRESULT __stdcall GetNodeDescriptors( ULONG *ulcNodeDescriptors,
        ULONG ulcNodeDescriptorsMax,
        BDANODE_DESCRIPTOR rgNodeDescriptors[] )=0;
    virtual HRESULT __stdcall GetNodeInterfaces( ULONG ulNodeType,
        ULONG *pulcInterfaces, ULONG ulcInterfacesMax,
        GUID rgguidInterfaces[] )=0;
    virtual HRESULT __stdcall GetPinTypes( ULONG *pulcPinTypes,
        ULONG ulcPinTypesMax, ULONG rgulPinTypes[] )=0;
    virtual HRESULT __stdcall GetTemplateConnections( ULONG *pulcConnections,
        ULONG ulcConnectionsMax, BDA_TEMPLATE_CONNECTION rgConnections[] )=0;
    virtual HRESULT __stdcall CreatePin( ULONG ulPinType, ULONG *pulPinId )=0;
    virtual HRESULT __stdcall DeletePin( ULONG ulPinId )=0;
    virtual HRESULT __stdcall SetMediaType( ULONG ulPinId,
       AM_MEDIA_TYPE *pMediaType )=0;
    virtual HRESULT __stdcall SetMedium( ULONG ulPinId,
       REGPINMEDIUM *pMedium )=0;
    virtual HRESULT __stdcall CreateTopology( ULONG ulInputPinId,
       ULONG ulOutputPinId )=0;
    virtual HRESULT __stdcall GetControlNode( ULONG ulInputPinId,
       ULONG ulOutputPinId, ULONG ulNodeType, IUnknown **ppControlNode )=0;
};

typedef struct _MPEG_HEADER_BITS_MIDL
{
    WORD Bits;
} MPEG_HEADER_BITS_MIDL;

typedef struct _MPEG_HEADER_VERSION_BITS_MIDL
{
    BYTE Bits;
} MPEG_HEADER_VERSION_BITS_MIDL;

typedef WORD PID;

typedef BYTE TID;

typedef struct _SECTION
{
    TID TableId;
    union
    {
        MPEG_HEADER_BITS_MIDL S;
        WORD W;
    } Header;
    BYTE SectionData[ 1 ];
} SECTION, *PSECTION;

typedef struct _LONG_SECTION
{
    TID TableId;
    union
    {
        MPEG_HEADER_BITS_MIDL S;
        WORD W;
    } Header;
    WORD TableIdExtension;
    union
    {
        MPEG_HEADER_VERSION_BITS_MIDL S;
        BYTE B;
        } Version;
    BYTE SectionNumber;
    BYTE LastSectionNumber;
    BYTE RemainingData[ 1 ];
} LONG_SECTION;

typedef struct _MPEG_BCS_DEMUX
{
    DWORD AVMGraphId;
} MPEG_BCS_DEMUX;

typedef struct _MPEG_WINSOC
{
    DWORD AVMGraphId;
} MPEG_WINSOCK;

typedef enum
{
    MPEG_CONTEXT_BCS_DEMUX = 0,
    MPEG_CONTEXT_WINSOCK = MPEG_CONTEXT_BCS_DEMUX + 1
} MPEG_CONTEXT_TYPE;

typedef struct _MPEG_RQST_PACKET
{
    DWORD dwLength;
    PSECTION pSection;
} MPEG_RQST_PACKET, *PMPEG_RQST_PACKET;

typedef struct _MPEG_PACKET_LIST
{
    WORD wPacketCount;
    PMPEG_RQST_PACKET PacketList[ 1 ];
} MPEG_PACKET_LIST, *PMPEG_PACKET_LIST;

typedef struct _DSMCC_FILTER_OPTIONS
{
    BOOL fSpecifyProtocol;
    BYTE Protocol;
    BOOL fSpecifyType;
    BYTE Type;
    BOOL fSpecifyMessageId;
    WORD MessageId;
    BOOL fSpecifyTransactionId;
    BOOL fUseTrxIdMessageIdMask;
    DWORD TransactionId;
    BOOL fSpecifyModuleVersion;
    BYTE ModuleVersion;
    BOOL fSpecifyBlockNumber;
    WORD BlockNumber;
    BOOL fGetModuleCall;
    WORD NumberOfBlocksInModule;
} DSMCC_FILTER_OPTIONS;

typedef struct _ATSC_FILTER_OPTIONS
{
    BOOL fSpecifyEtmId;
    DWORD EtmId;
} ATSC_FILTER_OPTIONS;

typedef struct _MPEG_STREAM_BUFFER
{
    HRESULT hr;
    DWORD dwDataBufferSize;
    DWORD dwSizeOfDataRead;
    BYTE *pDataBuffer;
} MPEG_STREAM_BUFFER, *PMPEG_STREAM_BUFFER;

typedef struct _MPEG_CONTEXT
{
    MPEG_CONTEXT_TYPE Type;
    union
    {
        MPEG_BCS_DEMUX Demux;
        MPEG_WINSOCK Winsock;
    } U;
} MPEG_CONTEXT, *PMPEG_CONTEXT;

typedef enum
{
   MPEG_RQST_UNKNOWN = 0,
   MPEG_RQST_GET_SECTION = MPEG_RQST_UNKNOWN + 1,
   MPEG_RQST_GET_SECTION_ASYNC = MPEG_RQST_GET_SECTION + 1,
   MPEG_RQST_GET_TABLE = MPEG_RQST_GET_SECTION_ASYNC + 1,
   MPEG_RQST_GET_TABLE_ASYNC = MPEG_RQST_GET_TABLE + 1,
   MPEG_RQST_GET_SECTIONS_STREAM = MPEG_RQST_GET_TABLE_ASYNC + 1,
   MPEG_RQST_GET_PES_STREAM = MPEG_RQST_GET_SECTIONS_STREAM + 1,
   MPEG_RQST_GET_TS_STREAM = MPEG_RQST_GET_PES_STREAM + 1,
   MPEG_RQST_START_MPE_STREAM = MPEG_RQST_GET_TS_STREAM + 1
} MPEG_REQUEST_TYPE;

typedef struct _MPEG2_FILTER
{
    BYTE bVersionNumber;
    WORD wFilterSize;
    BOOL fUseRawFilteringBits;
    BYTE Filter[ 16 ];
    BYTE Mask[ 16 ];
    BOOL fSpecifyTableIdExtension;
    WORD TableIdExtension;
    BOOL fSpecifyVersion;
    BYTE Version;
    BOOL fSpecifySectionNumber;
    BYTE SectionNumber;
    BOOL fSpecifyCurrentNext;
    BOOL fNext;
    BOOL fSpecifyDsmccOptions;
    DSMCC_FILTER_OPTIONS Dsmcc;
    BOOL fSpecifyAtscOptions;
    ATSC_FILTER_OPTIONS Atsc;
} MPEG2_FILTER, *PMPEG2_FILTER;

typedef struct _MPEG_HEADER_BITS
{
    WORD SectionLength          : 12;
    WORD Reserved               :  2;
    WORD PrivateIndicator       :  1;
    WORD SectionSyntaxIndicator :  1;
} MPEG_HEADER_BITS, *PMPEG_HEADER_BITS;

typedef struct _MPEG_HEADER_VERSION_BITS
{
    BYTE CurrentNextIndicator : 1;
    BYTE VersionNumber        : 5;
    BYTE Reserved             : 2;
} MPEG_HEADER_VERSION_BITS, *PMPEG_HEADER_VERSION_BITS;

class IMpeg2Data : public IUnknown
{
public:
    virtual HRESULT __stdcall GetSection( PID pid, TID tid,
        PMPEG2_FILTER pFilter, DWORD dwTimeout,
        ISectionList **ppSectionList )=0;
    virtual HRESULT __stdcall GetTable( PID pid, TID tid, PMPEG2_FILTER pFilter,
        DWORD dwTimeout, ISectionList **ppSectionList )=0;
    virtual HRESULT __stdcall GetStreamOfSections( PID pid, TID tid,
        PMPEG2_FILTER pFilter, HANDLE hDataReadyEvent,
        IMpeg2Stream **ppMpegStream )=0;
};

class IGuideData : public IUnknown
{
public:
    virtual HRESULT __stdcall GetServices(
        IEnumTuneRequests **ppEnumTuneRequestslass )=0;
    virtual HRESULT __stdcall GetServiceProperties(
        ITuneRequest *pTuneRequest,
        IEnumGuideDataProperties **ppEnumProperties )=0;
    virtual HRESULT __stdcall GetGuideProgramIDs(
        IEnumVARIANT **pEnumPrograms )=0;
    virtual HRESULT __stdcall GetProgramProperties(
        VARIANT varProgramDescriptionID,
        IEnumGuideDataProperties **ppEnumProperties )=0;
    virtual HRESULT __stdcall GetScheduleEntryIDs(
        IEnumVARIANT **pEnumScheduleEntries )=0;
    virtual HRESULT __stdcall GetScheduleEntryProperties(
        VARIANT varScheduleEntryDescriptionID,
        IEnumGuideDataProperties **ppEnumProperties )=0;
};

class IGuideDataEvent : public IUnknown
{
public:
    virtual HRESULT __stdcall GuideDataAcquired( void )=0;
    virtual HRESULT __stdcall ProgramChanged(
        VARIANT varProgramDescriptionID )=0;
    virtual HRESULT __stdcall ServiceChanged(
        VARIANT varServiceDescriptionID )=0;
    virtual HRESULT __stdcall ScheduleEntryChanged(
        VARIANT varScheduleEntryDescriptionID )=0;
    virtual HRESULT __stdcall ProgramDeleted(
        VARIANT varProgramDescriptionID )=0;
    virtual HRESULT __stdcall ServiceDeleted(
        VARIANT varServiceDescriptionID )=0;
    virtual HRESULT __stdcall ScheduleDeleted(
            VARIANT varScheduleEntryDescriptionID )=0;
};

class IGuideDataProperty : public IUnknown
{
public:
    virtual  HRESULT __stdcall get_Name( BSTR *pbstrName )=0;
    virtual  HRESULT __stdcall get_Language( long *idLang )=0;
    virtual  HRESULT __stdcall get_Value( VARIANT *pvar )=0;
};

class IMpeg2Stream : public IUnknown
{
public:
    virtual HRESULT __stdcall Initialize( MPEG_REQUEST_TYPE requestType,
        IMpeg2Data *pMpeg2Data, PMPEG_CONTEXT pContext, PID pid, TID tid,
        PMPEG2_FILTER pFilter, HANDLE hDataReadyEvent )=0;
    virtual HRESULT __stdcall SupplyDataBuffer(
        PMPEG_STREAM_BUFFER pStreamBuffer )=0;
};

class ISectionList : public IUnknown
{
public:
    virtual HRESULT __stdcall Initialize( MPEG_REQUEST_TYPE requestType,
        IMpeg2Data *pMpeg2Data, PMPEG_CONTEXT pContext, PID pid, TID tid,
        PMPEG2_FILTER pFilter, DWORD timeout, HANDLE hDoneEvent )=0;
    virtual HRESULT __stdcall InitializeWithRawSections(
        PMPEG_PACKET_LIST pmplSections )=0;
    virtual HRESULT __stdcall CancelPendingRequest( void )=0;
    virtual HRESULT __stdcall GetNumberOfSections( WORD *pCount )=0;
    virtual HRESULT __stdcall GetSectionData( WORD sectionNumber,
        DWORD *pdwRawPacketLength, PSECTION *ppSection )=0;
    virtual HRESULT __stdcall GetProgramIdentifier( PID *pPid )=0;
    virtual HRESULT __stdcall GetTableIdentifier( TID *pTableId )=0;
};

class IEnumGuideDataProperties : public IUnknown
{
public:
    virtual HRESULT __stdcall Next( unsigned long celt,
        IGuideDataProperty **ppprop, unsigned long *pcelt )=0;
    virtual HRESULT __stdcall Skip( unsigned long celt )=0;
    virtual HRESULT __stdcall Reset( void )=0;
    virtual HRESULT __stdcall Clone( IEnumGuideDataProperties **ppenum )=0;
};

class IEnumTuneRequests : public IUnknown
{
public:
    virtual HRESULT __stdcall Next( unsigned long celt, ITuneRequest **ppprop,
        unsigned long *pcelt )=0;
    virtual HRESULT __stdcall Skip( unsigned long celt )=0;
    virtual HRESULT __stdcall Reset( void )=0;
    virtual HRESULT __stdcall Clone( IEnumTuneRequests **ppenum )=0;
};

typedef struct __MIDL___MIDL_itf_mpeg2structs_0000_0000_0024
{
    BYTE Hours;
    BYTE Minutes;
    BYTE Seconds;
} MPEG_TIME;

typedef MPEG_TIME MPEG_DURATION;

typedef struct __MIDL___MIDL_itf_mpeg2structs_0000_0000_0025
{
    BYTE Date;
    BYTE Month;
    WORD Year;
} MPEG_DATE;

typedef struct __MIDL___MIDL_itf_mpeg2structs_0000_0000_0026
{
    MPEG_DATE D;
    MPEG_TIME T;
} MPEG_DATE_AND_TIME;

class IGenericDescriptor : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE Initialize(
        /* [in] */ BYTE *pbDesc,
        /* [in] */ INT bCount) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTag(
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetLength(
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetBody(
        /* [out] */ BYTE **ppbVal) = 0;

};

class IDVB_EIT : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE Initialize(
        /* [in] */ ISectionList *pSectionList,
        /* [in] */ IMpeg2Data *pMPEGData) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetVersionNumber(
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetServiceId(
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTransportStreamId(
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetOriginalNetworkId(
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetSegmentLastSectionNumber(
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetLastTableId(
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetCountOfRecords(
        /* [out] */ DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordEventId(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordStartTime(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ MPEG_DATE_AND_TIME *pmdtVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordDuration(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ MPEG_DURATION *pmdVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordRunningStatus(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordFreeCAMode(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ BOOL *pfVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordCountOfDescriptors(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordDescriptorByIndex(
        /* [in] */ DWORD dwRecordIndex,
        /* [in] */ DWORD dwIndex,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordDescriptorByTag(
        /* [in] */ DWORD dwRecordIndex,
        /* [in] */ BYTE bTag,
        /* [annotation][out][in] */
          DWORD *pdwCookie,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE RegisterForNextTable(
        /* [in] */ HANDLE hNextTableAvailable) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetNextTable(
        /* [out] */ IDVB_EIT **ppEIT) = 0;

    virtual HRESULT STDMETHODCALLTYPE RegisterForWhenCurrent(
        /* [in] */ HANDLE hNextTableIsCurrent) = 0;

    virtual HRESULT STDMETHODCALLTYPE ConvertNextToCurrent( void) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetVersionHash(
        /* [out] */ DWORD *pdwVersionHash) = 0;

};

class IDVB_EIT2 : public IDVB_EIT
{
public:
    virtual HRESULT STDMETHODCALLTYPE GetSegmentInfo(
        /* [out] */ BYTE *pbTid,
        /* [out] */ BYTE *pbSegment) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordSection(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ BYTE *pbVal) = 0;

};

class IPAT : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE Initialize(
        /* [in] */ ISectionList *pSectionList,
        /* [in] */ IMpeg2Data *pMPEGData) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTransportStreamId(
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetVersionNumber(
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetCountOfRecords(
        /* [out] */ DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordProgramNumber(
        /* [in] */ DWORD dwIndex,
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordProgramMapPid(
        /* [in] */ DWORD dwIndex,
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE FindRecordProgramMapPid(
        /* [in] */ WORD wProgramNumber,
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE RegisterForNextTable(
        /* [in] */ HANDLE hNextTableAvailable) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetNextTable(
        /* [out] */ IPAT **ppPAT) = 0;

    virtual HRESULT STDMETHODCALLTYPE RegisterForWhenCurrent(
        /* [in] */ HANDLE hNextTableIsCurrent) = 0;

    virtual HRESULT STDMETHODCALLTYPE ConvertNextToCurrent( void) = 0;

};

typedef struct _DSMCC_ELEMENT
{
    PID pid;
    BYTE bComponentTag;
    DWORD dwCarouselId;
    DWORD dwTransactionId;
    struct _DSMCC_ELEMENT *pNext;
} DSMCC_ELEMENT;

typedef struct _MPE_ELEMENT
{
    PID pid;
    BYTE bComponentTag;
    struct _MPE_ELEMENT *pNext;
} MPE_ELEMENT;

class IPMT : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE Initialize(
        /* [in] */ ISectionList *pSectionList,
        /* [in] */ IMpeg2Data *pMPEGData) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetProgramNumber(
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetVersionNumber(
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetPcrPid(
        /* [out] */ PID *pPidVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetCountOfTableDescriptors(
        /* [out] */ DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTableDescriptorByIndex(
        /* [in] */ DWORD dwIndex,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTableDescriptorByTag(
        /* [in] */ BYTE bTag,
        /* [out][in] */ DWORD *pdwCookie,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetCountOfRecords(
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordStreamType(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordElementaryPid(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ PID *pPidVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordCountOfDescriptors(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordDescriptorByIndex(
        /* [in] */ DWORD dwRecordIndex,
        /* [in] */ DWORD dwDescIndex,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordDescriptorByTag(
        /* [in] */ DWORD dwRecordIndex,
        /* [in] */ BYTE bTag,
        /* [out][in] */ DWORD *pdwCookie,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE QueryServiceGatewayInfo(
        /* [out] */ DSMCC_ELEMENT **ppDSMCCList,
        /* [out] */ UINT *puiCount) = 0;

    virtual HRESULT STDMETHODCALLTYPE QueryMPEInfo(
        /* [out] */ MPE_ELEMENT **ppMPEList,
        /* [out] */ UINT *puiCount) = 0;

    virtual HRESULT STDMETHODCALLTYPE RegisterForNextTable(
        /* [in] */ HANDLE hNextTableAvailable) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetNextTable(
        /* [out] */ IPMT **ppPMT) = 0;

    virtual HRESULT STDMETHODCALLTYPE RegisterForWhenCurrent(
        /* [in] */ HANDLE hNextTableIsCurrent) = 0;

    virtual HRESULT STDMETHODCALLTYPE ConvertNextToCurrent( void) = 0;

};

class ICAT : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE Initialize(
        /* [in] */ ISectionList *pSectionList,
        /* [in] */ IMpeg2Data *pMPEGData) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetVersionNumber(
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetCountOfTableDescriptors(
        /* [out] */ DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTableDescriptorByIndex(
        /* [in] */ DWORD dwIndex,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTableDescriptorByTag(
        /* [in] */ BYTE bTag,
        /* [out][in] */ DWORD *pdwCookie,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE RegisterForNextTable(
        /* [in] */ HANDLE hNextTableAvailable) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetNextTable(
        /* [in] */ DWORD dwTimeout,
        /* [out] */ ICAT **ppCAT) = 0;

    virtual HRESULT STDMETHODCALLTYPE RegisterForWhenCurrent(
        /* [in] */ HANDLE hNextTableIsCurrent) = 0;

    virtual HRESULT STDMETHODCALLTYPE ConvertNextToCurrent( void) = 0;

};

class IDVB_NIT : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE Initialize(
        /* [in] */ ISectionList *pSectionList,
        /* [in] */ IMpeg2Data *pMPEGData) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetVersionNumber(
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetNetworkId(
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetCountOfTableDescriptors(
        /* [out] */ DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTableDescriptorByIndex(
        /* [in] */ DWORD dwIndex,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTableDescriptorByTag(
        /* [in] */ BYTE bTag,
        /* [annotation][out][in] */
          DWORD *pdwCookie,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetCountOfRecords(
        /* [out] */ DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordTransportStreamId(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordOriginalNetworkId(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordCountOfDescriptors(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordDescriptorByIndex(
        /* [in] */ DWORD dwRecordIndex,
        /* [in] */ DWORD dwIndex,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordDescriptorByTag(
        /* [in] */ DWORD dwRecordIndex,
        /* [in] */ BYTE bTag,
        /* [annotation][out][in] */
          DWORD *pdwCookie,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE RegisterForNextTable(
        /* [in] */ HANDLE hNextTableAvailable) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetNextTable(
        /* [out] */ IDVB_NIT **ppNIT) = 0;

    virtual HRESULT STDMETHODCALLTYPE RegisterForWhenCurrent(
        /* [in] */ HANDLE hNextTableIsCurrent) = 0;

    virtual HRESULT STDMETHODCALLTYPE ConvertNextToCurrent( void) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetVersionHash(
        /* [out] */ DWORD *pdwVersionHash) = 0;

};

class IDVB_SDT : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE Initialize(
        /* [in] */ ISectionList *pSectionList,
        /* [in] */ IMpeg2Data *pMPEGData) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetVersionNumber(
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTransportStreamId(
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetOriginalNetworkId(
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetCountOfRecords(
        /* [out] */ DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordServiceId(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordEITScheduleFlag(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ BOOL *pfVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordEITPresentFollowingFlag(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ BOOL *pfVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordRunningStatus(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordFreeCAMode(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ BOOL *pfVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordCountOfDescriptors(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordDescriptorByIndex(
        /* [in] */ DWORD dwRecordIndex,
        /* [in] */ DWORD dwIndex,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordDescriptorByTag(
        /* [in] */ DWORD dwRecordIndex,
        /* [in] */ BYTE bTag,
        /* [annotation][out][in] */
         DWORD *pdwCookie,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE RegisterForNextTable(
        /* [in] */ HANDLE hNextTableAvailable) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetNextTable(
        /* [out] */ IDVB_SDT **ppSDT) = 0;

    virtual HRESULT STDMETHODCALLTYPE RegisterForWhenCurrent(
        /* [in] */ HANDLE hNextTableIsCurrent) = 0;

    virtual HRESULT STDMETHODCALLTYPE ConvertNextToCurrent( void) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetVersionHash(
        /* [out] */ DWORD *pdwVersionHash) = 0;

};

class IDVB_BAT : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE Initialize(
        /* [in] */ ISectionList *pSectionList,
        /* [in] */ IMpeg2Data *pMPEGData) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetVersionNumber(
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetBouquetId(
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetCountOfTableDescriptors(
        /* [out] */ DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTableDescriptorByIndex(
        /* [in] */ DWORD dwIndex,
        /* [in] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTableDescriptorByTag(
        /* [in] */ BYTE bTag,
        /* [annotation][out][in] */
         DWORD *pdwCookie,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetCountOfRecords(
        /* [out] */ DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordTransportStreamId(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordOriginalNetworkId(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordCountOfDescriptors(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordDescriptorByIndex(
        /* [in] */ DWORD dwRecordIndex,
        /* [in] */ DWORD dwIndex,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordDescriptorByTag(
        /* [in] */ DWORD dwRecordIndex,
        /* [in] */ BYTE bTag,
        /* [annotation][out][in] */
         DWORD *pdwCookie,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE RegisterForNextTable(
        /* [in] */ HANDLE hNextTableAvailable) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetNextTable(
        /* [out] */ IDVB_BAT **ppBAT) = 0;

    virtual HRESULT STDMETHODCALLTYPE RegisterForWhenCurrent(
        /* [in] */ HANDLE hNextTableIsCurrent) = 0;

    virtual HRESULT STDMETHODCALLTYPE ConvertNextToCurrent( void) = 0;

};

class IDVB_RST : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE Initialize(
        /* [in] */ ISectionList *pSectionList) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetCountOfRecords(
        /* [out] */ DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordTransportStreamId(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordOriginalNetworkId(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordServiceId(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordEventId(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordRunningStatus(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ BYTE *pbVal) = 0;

};

class IDVB_ST : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE Initialize(
        /* [in] */ ISectionList *pSectionList) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetDataLength(
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetData(
        /* [out] */ BYTE **ppData) = 0;

};

class IDVB_TDT : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE Initialize(
        /* [in] */ ISectionList *pSectionList) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetUTCTime(
        /* [out] */ MPEG_DATE_AND_TIME *pmdtVal) = 0;

};

class IDVB_TOT : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE Initialize(
        /* [in] */ ISectionList *pSectionList) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetUTCTime(
        /* [out] */ MPEG_DATE_AND_TIME *pmdtVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetCountOfTableDescriptors(
        /* [out] */ DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTableDescriptorByIndex(
        /* [in] */ DWORD dwIndex,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTableDescriptorByTag(
        /* [in] */ BYTE bTag,
        /* [annotation][out][in] */
         DWORD *pdwCookie,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

};

class IDVB_DIT : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE Initialize(
        /* [in] */ ISectionList *pSectionList) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTransitionFlag(
        /* [out] */ BOOL *pfVal) = 0;

};

class IDVB_SIT : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE Initialize(
        /* [in] */ ISectionList *pSectionList,
        /* [in] */ IMpeg2Data *pMPEGData) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetVersionNumber(
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetCountOfTableDescriptors(
        /* [out] */ DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTableDescriptorByIndex(
        /* [in] */ DWORD dwIndex,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTableDescriptorByTag(
        /* [in] */ BYTE bTag,
        /* [annotation][out][in] */
         DWORD *pdwCookie,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetCountOfRecords(
        /* [out] */ DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordServiceId(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordRunningStatus(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordCountOfDescriptors(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordDescriptorByIndex(
        /* [in] */ DWORD dwRecordIndex,
        /* [in] */ DWORD dwIndex,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordDescriptorByTag(
        /* [in] */ DWORD dwRecordIndex,
        /* [in] */ BYTE bTag,
        /* [annotation][out][in] */
         DWORD *pdwCookie,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE RegisterForNextTable(
        /* [in] */ HANDLE hNextTableAvailable) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetNextTable(
        /* [in] */ DWORD dwTimeout,
        /* [out] */ IDVB_SIT **ppSIT) = 0;

    virtual HRESULT STDMETHODCALLTYPE RegisterForWhenCurrent(
        /* [in] */ HANDLE hNextTableIsCurrent) = 0;

    virtual HRESULT STDMETHODCALLTYPE ConvertNextToCurrent( void) = 0;

};

class ITSDT : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE Initialize(
        /* [in] */ ISectionList *pSectionList,
        /* [in] */ IMpeg2Data *pMPEGData) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetVersionNumber(
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetCountOfTableDescriptors(
        /* [out] */ DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTableDescriptorByIndex(
        /* [in] */ DWORD dwIndex,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTableDescriptorByTag(
        /* [in] */ BYTE bTag,
        /* [out][in] */ DWORD *pdwCookie,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE RegisterForNextTable(
        /* [in] */ HANDLE hNextTableAvailable) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetNextTable(
        /* [out] */ ITSDT **ppTSDT) = 0;

    virtual HRESULT STDMETHODCALLTYPE RegisterForWhenCurrent(
        /* [in] */ HANDLE hNextTableIsCurrent) = 0;

    virtual HRESULT STDMETHODCALLTYPE ConvertNextToCurrent( void) = 0;

};

class IDvbSiParser : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE Initialize(
        /* [in] */ IUnknown *punkMpeg2Data) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetPAT(
        /* [out] */ IPAT **ppPAT) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetCAT(
        /* [in] */ DWORD dwTimeout,
        /* [out] */ ICAT **ppCAT) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetPMT(
        /* [in] */ PID pid,
        /* [annotation][in] */
        _In_opt_  WORD *pwProgramNumber,
        /* [out] */ IPMT **ppPMT) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTSDT(
        /* [out] */ ITSDT **ppTSDT) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetNIT(
        /* [in] */ TID tableId,
        /* [annotation][in] */
        _In_opt_  WORD *pwNetworkId,
        /* [out] */ IDVB_NIT **ppNIT) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetSDT(
        /* [in] */ TID tableId,
        /* [annotation][in] */
        _In_opt_  WORD *pwTransportStreamId,
        /* [out] */ IDVB_SDT **ppSDT) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetEIT(
        /* [in] */ TID tableId,
        /* [annotation][in] */
        _In_opt_  WORD *pwServiceId,
        /* [out] */ IDVB_EIT **ppEIT) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetBAT(
        /* [annotation][in] */
        _In_opt_  WORD *pwBouquetId,
        /* [out] */ IDVB_BAT **ppBAT) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRST(
        /* [in] */ DWORD dwTimeout,
        /* [out] */ IDVB_RST **ppRST) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetST(
        /* [in] */ PID pid,
        /* [in] */ DWORD dwTimeout,
        /* [out] */ IDVB_ST **ppST) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTDT(
        /* [out] */ IDVB_TDT **ppTDT) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTOT(
        /* [out] */ IDVB_TOT **ppTOT) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetDIT(
        /* [in] */ DWORD dwTimeout,
        /* [out] */ IDVB_DIT **ppDIT) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetSIT(
        /* [in] */ DWORD dwTimeout,
        /* [out] */ IDVB_SIT **ppSIT) = 0;

};

class IDvbSiParser2 : public IDvbSiParser
{
public:
    virtual HRESULT STDMETHODCALLTYPE GetEIT2(
        /* [in] */ TID tableId,
        /* [annotation][in] */
        _In_opt_  WORD *pwServiceId,
        /* [annotation][in] */
        _In_opt_  BYTE *pbSegment,
        /* [out] */ IDVB_EIT2 **ppEIT) = 0;

};

class IDvbServiceDescriptor : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE GetTag(
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetLength(
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetServiceType(
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetServiceProviderName(
        /* [annotation][out] */
         char **pszName) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetServiceProviderNameW(
        /* [annotation][out] */
         BSTR *pbstrName) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetServiceName(
        /* [annotation][out] */
         char **pszName) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetProcessedServiceName(
        /* [out] */ BSTR *pbstrName) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetServiceNameEmphasized(
        /* [out] */ BSTR *pbstrName) = 0;

};

typedef enum __MIDL___MIDL_itf_dvbsiparser_0000_0000_0001
{
    STRCONV_MODE_DVB	= 0,
    STRCONV_MODE_DVB_EMPHASIS	= ( STRCONV_MODE_DVB + 1 ) ,
    STRCONV_MODE_DVB_WITHOUT_EMPHASIS	= ( STRCONV_MODE_DVB_EMPHASIS + 1 ) ,
    STRCONV_MODE_ISDB	= ( STRCONV_MODE_DVB_WITHOUT_EMPHASIS + 1 )
} DVB_STRCONV_MODE;

class IDvbShortEventDescriptor : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE GetTag(
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetLength(
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetLanguageCode(
        /* [annotation][out] */
        char *pszCode) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetEventNameW(
        /* [in] */ DVB_STRCONV_MODE convMode,
        /* [out] */ BSTR *pbstrName) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTextW(
        /* [in] */ DVB_STRCONV_MODE convMode,
        /* [out] */ BSTR *pbstrText) = 0;

};

class IDvbServiceDescriptor2 : public IDvbServiceDescriptor
{
public:
    virtual HRESULT STDMETHODCALLTYPE GetServiceProviderNameW(
        /* [in] */ DVB_STRCONV_MODE convMode,
        /* [out] */ BSTR *pbstrName) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetServiceNameW(
        /* [in] */ DVB_STRCONV_MODE convMode,
        /* [out] */ BSTR *pbstrName) = 0;

};

class IISDB_SDT : public IDVB_SDT
{
public:
    virtual HRESULT STDMETHODCALLTYPE GetRecordEITUserDefinedFlags(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ BYTE *pbVal) = 0;

};

class IISDB_BIT : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE Initialize(
        /* [in] */ ISectionList *pSectionList,
        /* [in] */ IMpeg2Data *pMPEGData) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetVersionNumber(
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetOriginalNetworkId(
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetBroadcastViewPropriety(
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetCountOfTableDescriptors(
        /* [out] */ DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTableDescriptorByIndex(
        /* [in] */ DWORD dwIndex,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTableDescriptorByTag(
        /* [in] */ BYTE bTag,
        /* [annotation][out][in] */
         DWORD *pdwCookie,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetCountOfRecords(
        /* [out] */ DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordBroadcasterId(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordCountOfDescriptors(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordDescriptorByIndex(
        /* [in] */ DWORD dwRecordIndex,
        /* [in] */ DWORD dwIndex,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordDescriptorByTag(
        /* [in] */ DWORD dwRecordIndex,
        /* [in] */ BYTE bTag,
        /* [annotation][out][in] */
         DWORD *pdwCookie,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetVersionHash(
        /* [out] */ DWORD *pdwVersionHash) = 0;

};

class IISDB_NBIT : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE Initialize(
        /* [in] */ ISectionList *pSectionList,
        /* [in] */ IMpeg2Data *pMPEGData) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetVersionNumber(
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetOriginalNetworkId(
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetCountOfRecords(
        /* [out] */ DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordInformationId(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordInformationType(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordDescriptionBodyLocation(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordMessageSectionNumber(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordUserDefined(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordNumberOfKeys(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordKeys(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ BYTE **pbKeys) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordCountOfDescriptors(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordDescriptorByIndex(
        /* [in] */ DWORD dwRecordIndex,
        /* [in] */ DWORD dwIndex,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordDescriptorByTag(
        /* [in] */ DWORD dwRecordIndex,
        /* [in] */ BYTE bTag,
        /* [annotation][out][in] */
         DWORD *pdwCookie,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetVersionHash(
        /* [out] */ DWORD *pdwVersionHash) = 0;

};

class IISDB_LDT : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE Initialize(
        /* [in] */ ISectionList *pSectionList,
        /* [in] */ IMpeg2Data *pMPEGData) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetVersionNumber(
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetOriginalServiceId(
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTransportStreamId(
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetOriginalNetworkId(
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetCountOfRecords(
        /* [out] */ DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordDescriptionId(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordCountOfDescriptors(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordDescriptorByIndex(
        /* [in] */ DWORD dwRecordIndex,
        /* [in] */ DWORD dwIndex,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordDescriptorByTag(
        /* [in] */ DWORD dwRecordIndex,
        /* [in] */ BYTE bTag,
        /* [annotation][out][in] */
         DWORD *pdwCookie,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetVersionHash(
        /* [out] */ DWORD *pdwVersionHash) = 0;

};

class IISDB_SDTT : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE Initialize(
        /* [in] */ ISectionList *pSectionList,
        /* [in] */ IMpeg2Data *pMPEGData) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetVersionNumber(
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTableIdExt(
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTransportStreamId(
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetOriginalNetworkId(
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetServiceId(
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetCountOfRecords(
        /* [out] */ DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordGroup(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordTargetVersion(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordNewVersion(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordDownloadLevel(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordVersionIndicator(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordScheduleTimeShiftInformation(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordCountOfSchedules(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordStartTimeByIndex(
        /* [in] */ DWORD dwRecordIndex,
        /* [in] */ DWORD dwIndex,
        /* [out] */ MPEG_DATE_AND_TIME *pmdtVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordDurationByIndex(
        /* [in] */ DWORD dwRecordIndex,
        /* [in] */ DWORD dwIndex,
        /* [out] */ MPEG_DURATION *pmdVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordCountOfDescriptors(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordDescriptorByIndex(
        /* [in] */ DWORD dwRecordIndex,
        /* [in] */ DWORD dwIndex,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordDescriptorByTag(
        /* [in] */ DWORD dwRecordIndex,
        /* [in] */ BYTE bTag,
        /* [annotation][out][in] */
         DWORD *pdwCookie,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetVersionHash(
        /* [out] */ DWORD *pdwVersionHash) = 0;

};

class IISDB_CDT : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE Initialize(
        /* [in] */ ISectionList *pSectionList,
        /* [in] */ IMpeg2Data *pMPEGData,
        /* [in] */ BYTE bSectionNumber) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetVersionNumber(
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetDownloadDataId(
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetSectionNumber(
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetOriginalNetworkId(
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetDataType(
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetCountOfTableDescriptors(
        /* [out] */ DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTableDescriptorByIndex(
        /* [in] */ DWORD dwIndex,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTableDescriptorByTag(
        /* [in] */ BYTE bTag,
        /* [annotation][out][in] */
         DWORD *pdwCookie,
        /* [out] */ IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetSizeOfDataModule(
        /* [out] */ DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetDataModule(
        /* [out] */ BYTE **pbData) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetVersionHash(
        /* [out] */ DWORD *pdwVersionHash) = 0;

};

class IISDB_EMM : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE Initialize(
        /* [in] */ ISectionList *pSectionList,
        /* [in] */ IMpeg2Data *pMPEGData) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetVersionNumber(
        /* [out] */ BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTableIdExtension(
        /* [out] */ WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetDataBytes(
        /* [out][in] */ WORD *pwBufferLength,
        /* [out] */ BYTE *pbBuffer) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetSharedEmmMessage(
        WORD *pwLength,
        BYTE **ppbMessage) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetIndividualEmmMessage(
        IUnknown *pUnknown,
        WORD *pwLength,
        BYTE **ppbMessage) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetVersionHash(
        /* [out] */ DWORD *pdwVersionHash) = 0;

};

class IIsdbSiParser2 : public IDvbSiParser2
{
public:
    virtual HRESULT STDMETHODCALLTYPE GetSDT(
        /* [in] */ TID tableId,
        /* [annotation][in] */
        _In_opt_  WORD *pwTransportStreamId,
        /* [out] */ IISDB_SDT **ppSDT) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetBIT(
        /* [in] */ TID tableId,
        /* [annotation][in] */
        _In_opt_  WORD *pwOriginalNetworkId,
        /* [out] */ IISDB_BIT **ppBIT) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetNBIT(
        /* [in] */ TID tableId,
        /* [annotation][in] */
        _In_opt_  WORD *pwOriginalNetworkId,
        /* [out] */ IISDB_NBIT **ppNBIT) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetLDT(
        /* [in] */ TID tableId,
        /* [annotation][in] */
        _In_opt_  WORD *pwOriginalServiceId,
        /* [out] */ IISDB_LDT **ppLDT) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetSDTT(
        /* [in] */ TID tableId,
        /* [annotation][in] */
        _In_opt_  WORD *pwTableIdExt,
        /* [out] */ IISDB_SDTT **ppSDTT) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetCDT(
        /* [in] */ TID tableId,
        /* [in] */ BYTE bSectionNumber,
        /* [annotation][in] */
        _In_opt_  WORD *pwDownloadDataId,
        /* [out] */ IISDB_CDT **ppCDT) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetEMM(
        /* [in] */ PID pid,
        /* [in] */ WORD wTableIdExt,
        /* [out] */ IISDB_EMM **ppEMM) = 0;

};

class IATSC_MGT : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE Initialize(
        /* [in] */ __RPC__in_opt ISectionList *pSectionList,
        /* [in] */ __RPC__in_opt IMpeg2Data *pMPEGData) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetVersionNumber(
        /* [out] */ __RPC__out BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetProtocolVersion(
        /* [out] */ __RPC__out BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetCountOfRecords(
        /* [out] */ __RPC__out DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordType(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ __RPC__out WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordTypePid(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ __RPC__out PID *ppidVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordVersionNumber(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ __RPC__out BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordCountOfDescriptors(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ __RPC__out DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordDescriptorByIndex(
        /* [in] */ DWORD dwRecordIndex,
        /* [in] */ DWORD dwIndex,
        /* [out] */ __RPC__deref_out_opt IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordDescriptorByTag(
        /* [in] */ DWORD dwRecordIndex,
        /* [in] */ BYTE bTag,
        /* [out][in] */ __RPC__inout DWORD *pdwCookie,
        /* [out] */ __RPC__deref_out_opt IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetCountOfTableDescriptors(
        /* [in] */ __RPC__in DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTableDescriptorByIndex(
        /* [in] */ DWORD dwIndex,
        /* [out] */ __RPC__deref_out_opt IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTableDescriptorByTag(
        /* [in] */ BYTE bTag,
        /* [out][in] */ __RPC__inout DWORD *pdwCookie,
        /* [out] */ __RPC__deref_out_opt IGenericDescriptor **ppDescriptor) = 0;

};

class IATSC_VCT : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE Initialize(
        /* [in] */ __RPC__in_opt ISectionList *pSectionList,
        /* [in] */ __RPC__in_opt IMpeg2Data *pMPEGData) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetVersionNumber(
        /* [out] */ __RPC__out BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTransportStreamId(
        /* [out] */ __RPC__out WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetProtocolVersion(
        /* [out] */ __RPC__out BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetCountOfRecords(
        /* [out] */ __RPC__out DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordName(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ __RPC__deref_out_opt LPWSTR *pwsName) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordMajorChannelNumber(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ __RPC__out WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordMinorChannelNumber(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ __RPC__out WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordModulationMode(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ __RPC__out BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordCarrierFrequency(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ __RPC__out DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordTransportStreamId(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ __RPC__out WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordProgramNumber(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ __RPC__out WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordEtmLocation(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ __RPC__out BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordIsAccessControlledBitSet(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ __RPC__out BOOL *pfVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordIsHiddenBitSet(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ __RPC__out BOOL *pfVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordIsPathSelectBitSet(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ __RPC__out BOOL *pfVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordIsOutOfBandBitSet(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ __RPC__out BOOL *pfVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordIsHideGuideBitSet(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ __RPC__out BOOL *pfVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordServiceType(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ __RPC__out BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordSourceId(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ __RPC__out WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordCountOfDescriptors(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ __RPC__out DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordDescriptorByIndex(
        /* [in] */ DWORD dwRecordIndex,
        /* [in] */ DWORD dwIndex,
        /* [out] */ __RPC__deref_out_opt IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordDescriptorByTag(
        /* [in] */ DWORD dwRecordIndex,
        /* [in] */ BYTE bTag,
        /* [out][in] */ __RPC__inout DWORD *pdwCookie,
        /* [out] */ __RPC__deref_out_opt IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetCountOfTableDescriptors(
        /* [in] */ __RPC__in DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTableDescriptorByIndex(
        /* [in] */ DWORD dwIndex,
        /* [out] */ __RPC__deref_out_opt IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTableDescriptorByTag(
        /* [in] */ BYTE bTag,
        /* [out][in] */ __RPC__inout DWORD *pdwCookie,
        /* [out] */ __RPC__deref_out_opt IGenericDescriptor **ppDescriptor) = 0;

};

class IATSC_EIT : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE Initialize(
        /* [in] */ __RPC__in_opt ISectionList *pSectionList,
        /* [in] */ __RPC__in_opt IMpeg2Data *pMPEGData) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetVersionNumber(
        /* [out] */ __RPC__out BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetSourceId(
        /* [out] */ __RPC__out WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetProtocolVersion(
        /* [out] */ __RPC__out BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetCountOfRecords(
        /* [out] */ __RPC__out DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordEventId(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ __RPC__out WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordStartTime(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ __RPC__out MPEG_DATE_AND_TIME *pmdtVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordEtmLocation(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ __RPC__out BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordDuration(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ __RPC__out MPEG_DURATION *pmdVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordTitleText(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ __RPC__out DWORD *pdwLength,
        /* [out] */ __RPC__deref_out_opt BYTE **ppText) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordCountOfDescriptors(
        /* [in] */ DWORD dwRecordIndex,
        /* [out] */ __RPC__out DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordDescriptorByIndex(
        /* [in] */ DWORD dwRecordIndex,
        /* [in] */ DWORD dwIndex,
        /* [out] */ __RPC__deref_out_opt IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRecordDescriptorByTag(
        /* [in] */ DWORD dwRecordIndex,
        /* [in] */ BYTE bTag,
        /* [out][in] */ __RPC__inout DWORD *pdwCookie,
        /* [out] */ __RPC__deref_out_opt IGenericDescriptor **ppDescriptor) = 0;

};

class IATSC_ETT : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE Initialize(
        /* [in] */ __RPC__in_opt ISectionList *pSectionList,
        /* [in] */ __RPC__in_opt IMpeg2Data *pMPEGData) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetVersionNumber(
        /* [out] */ __RPC__out BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetProtocolVersion(
        /* [out] */ __RPC__out BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetEtmId(
        /* [out] */ __RPC__out DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetExtendedMessageText(
        /* [out] */ __RPC__out DWORD *pdwLength,
        /* [out] */ __RPC__deref_out_opt BYTE **ppText) = 0;

};

class IATSC_STT : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE Initialize(
        /* [in] */ __RPC__in_opt ISectionList *pSectionList,
        /* [in] */ __RPC__in_opt IMpeg2Data *pMPEGData) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetProtocolVersion(
        /* [out] */ __RPC__out BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetSystemTime(
        /* [out] */ __RPC__out MPEG_DATE_AND_TIME *pmdtSystemTime) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetGpsUtcOffset(
        /* [out] */ __RPC__out BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetDaylightSavings(
        /* [out] */ __RPC__out WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetCountOfTableDescriptors(
        /* [out] */ __RPC__out DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTableDescriptorByIndex(
        /* [in] */ DWORD dwIndex,
        /* [out] */ __RPC__deref_out_opt IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTableDescriptorByTag(
        /* [in] */ BYTE bTag,
        /* [out][in] */ __RPC__inout DWORD *pdwCookie,
        /* [out] */ __RPC__deref_out_opt IGenericDescriptor **ppDescriptor) = 0;

};

class ISCTE_EAS : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE Initialize(
        /* [in] */ __RPC__in_opt ISectionList *pSectionList,
        /* [in] */ __RPC__in_opt IMpeg2Data *pMPEGData) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetVersionNumber(
        /* [out] */ __RPC__out BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetSequencyNumber(
        /* [out] */ __RPC__out BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetProtocolVersion(
        /* [out] */ __RPC__out BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetEASEventID(
        /* [out] */ __RPC__out WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetOriginatorCode(
        /* [out] */ __RPC__out BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetEASEventCodeLen(
        /* [out] */ __RPC__out BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetEASEventCode(
        /* [out] */ __RPC__out BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRawNatureOfActivationTextLen(
        /* [out] */ __RPC__out BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRawNatureOfActivationText(
        /* [out] */ __RPC__out BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetNatureOfActivationText(
        /* [in] */ __RPC__in BSTR bstrIS0639code,
        /* [out] */ __RPC__deref_out_opt BSTR *pbstrString) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTimeRemaining(
        /* [out] */ __RPC__out BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetStartTime(
        /* [out] */ __RPC__out DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetDuration(
        /* [out] */ __RPC__out WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetAlertPriority(
        /* [out] */ __RPC__out BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetDetailsOOBSourceID(
        /* [out] */ __RPC__out WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetDetailsMajor(
        /* [out] */ __RPC__out WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetDetailsMinor(
        /* [out] */ __RPC__out WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetDetailsAudioOOBSourceID(
        /* [out] */ __RPC__out WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetAlertText(
        /* [in] */ __RPC__in BSTR bstrIS0639code,
        /* [out] */ __RPC__deref_out_opt BSTR *pbstrString) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRawAlertTextLen(
        /* [out] */ __RPC__out WORD *pwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetRawAlertText(
        /* [out] */ __RPC__out BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetLocationCount(
        /* [out] */ __RPC__out BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetLocationCodes(
        /* [in] */ BYTE bIndex,
        /* [out] */ __RPC__out BYTE *pbState,
        /* [out] */ __RPC__out BYTE *pbCountySubdivision,
        /* [out] */ __RPC__out WORD *pwCounty) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetExceptionCount(
        /* [out] */ __RPC__out BYTE *pbVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetExceptionService(
        /* [in] */ BYTE bIndex,
        /* [out] */ __RPC__out BYTE *pbIBRef,
        /* [out] */ __RPC__out WORD *pwFirst,
        /* [out] */ __RPC__out WORD *pwSecond) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetCountOfTableDescriptors(
        /* [out] */ __RPC__out DWORD *pdwVal) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTableDescriptorByIndex(
        /* [in] */ DWORD dwIndex,
        /* [out] */ __RPC__deref_out_opt IGenericDescriptor **ppDescriptor) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTableDescriptorByTag(
        /* [in] */ BYTE bTag,
        /* [out][in] */ __RPC__inout DWORD *pdwCookie,
        /* [out] */ __RPC__deref_out_opt IGenericDescriptor **ppDescriptor) = 0;

};

class IAtscPsipParser : public IUnknown
{
public:
    virtual HRESULT STDMETHODCALLTYPE Initialize(
        /* [in] */ __RPC__in_opt IUnknown *punkMpeg2Data) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetPAT(
        /* [out] */ __RPC__deref_out_opt IPAT **ppPAT) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetCAT(
        /* [in] */ DWORD dwTimeout,
        /* [out] */ __RPC__deref_out_opt ICAT **ppCAT) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetPMT(
        /* [in] */ PID pid,
        /* [in] */ __RPC__in WORD *pwProgramNumber,
        /* [out] */ __RPC__deref_out_opt IPMT **ppPMT) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetTSDT(
        /* [out] */ __RPC__deref_out_opt ITSDT **ppTSDT) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetMGT(
        /* [out] */ __RPC__deref_out_opt IATSC_MGT **ppMGT) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetVCT(
        /* [in] */ TID tableId,
        /* [in] */ BOOL fGetNextTable,
        /* [out] */ __RPC__deref_out_opt IATSC_VCT **ppVCT) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetEIT(
        /* [in] */ PID pid,
        /* [in] */ __RPC__in WORD *pwSourceId,
        /* [in] */ DWORD dwTimeout,
        /* [out] */ __RPC__deref_out_opt IATSC_EIT **ppEIT) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetETT(
        /* [in] */ PID pid,
        /* [in] */ __RPC__in WORD *wSourceId,
        /* [in] */ __RPC__in WORD *pwEventId,
        /* [out] */ __RPC__deref_out_opt IATSC_ETT **ppETT) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetSTT(
        /* [out] */ __RPC__deref_out_opt IATSC_STT **ppSTT) = 0;

    virtual HRESULT STDMETHODCALLTYPE GetEAS(
        /* [in] */ PID pid,
        /* [out] */ __RPC__deref_out_opt ISCTE_EAS **ppEAS) = 0;

};

class IISDBSLocator : public IDVBSLocator
{
public:
};

extern "C"
{
    /* Following GUIDs are for the new windows 7 interfaces  */
    /* windows 7 universal provider applies to all networks */
    const CLSID CLSID_NetworkProvider =
        {0xB2F3A67C,0x29DA,0x4C78,{0x88,0x31,0x09,0x1E,0xD5,0x09,0xA4,0x75}};

    /* Win 7 - Digital Cable - North America Clear QAM */
    const CLSID CLSID_DigitalCableTuningSpace =
        {0xD9BB4CEE,0xB87A,0x47F1,{0xAC,0xF1,0xB0,0x8D,0x9C,0x78,0x13,0xFC}};
    const CLSID CLSID_DigitalCableLocator =
        {0x03C06416,0xD127,0x407A,{0xAB,0x4C,0xFD,0xD2,0x79,0xAB,0xBE,0x5D}};
    const CLSID CLSID_DigitalCableNetworkType =
        {0x143827AB,0xF77B,0x498d,{0x81,0xCA,0x5A,0x00,0x7A,0xEC,0x28,0xBF}};
    const IID IID_IDigitalCableTuneRequest =
        {0xBAD7753B,0x6B37,0x4810,{0xAE,0x57,0x3C,0xE0,0xC4,0xA9,0xE6,0xCB}};
    const IID IID_IDigitalCableLocator =
        {0x48F66A11,0x171A,0x419A,{0x95,0x25,0xBE,0xEE,0xCD,0x51,0x58,0x4C}};

    /* ISDB */
    const CLSID CLSID_ISDBTNetworkType =
        {0x95037f6f,0x3ac7,0x4452,{0xb6,0xc4,0x45,0xa9,0xce,0x92,0x92,0xa2}};
    const CLSID CLSID_ISDBSNetworkType =
        {0xb0a4e6a0,0x6a1a,0x4b83,{0xbb,0x5b,0x90,0x3e,0x1d,0x90,0xe6,0xb6}};
    const CLSID CLSID_ISDBCNetworkType =
        {0xc974ddb5,0x41fe,0x4b25,{0x97,0x41,0x92,0xf0,0x49,0xf1,0xd5,0xd1}};
    const CLSID CLSID_ISDBTLocator =
        {0x9CD64701,0xBDF3,0x4D14,{0x8E,0x03,0xF1,0x29,0x83,0xD8,0x66,0x64}};
    const CLSID CLSID_ISDBSLocator =
        {0x6504AFED,0xA629,0x455c,{0xA7,0xF1,0x04,0x96,0x4D,0xEA,0x5C,0xC4}};
    const CLSID CLSID_ISDBTTuningSpace =
        {0xC6B14B32,0x76AA,0x4A86,{0xA7,0xAC,0x5C,0x79,0xAA,0xF5,0x8D,0xA7}};
    const CLSID CLSID_ISDBSTuningSpace =
        {0xB64016F3,0xC9A2,0x4066,{0x96,0xF0,0xBD,0x95,0x63,0x31,0x47,0x26}};
    const IID IID_IISDBSLocator =
        {0xC9897087,0xE29C,0x473f,{0x9E,0x4B,0x70,0x72,0x12,0x3D,0xEA,0x14}};

    /* KSCATEGORY_BDA */
    const GUID KSCATEGORY_BDA_NETWORK_PROVIDER =
        {0x71985F4B,0x1CA1,0x11d3,{0x9C,0xC8,0x00,0xC0,0x4F,0x79,0x71,0xE0}};
    const GUID KSCATEGORY_BDA_TRANSPORT_INFORMATION =
        {0xa2e3074f,0x6c3d,0x11d3,{0xb6,0x53,0x00,0xc0,0x4f,0x79,0x49,0x8e}};
    const GUID KSCATEGORY_BDA_RECEIVER_COMPONENT    =
        {0xFD0A5AF4,0xB41D,0x11d2,{0x9c,0x95,0x00,0xc0,0x4f,0x79,0x71,0xe0}};
    const GUID KSCATEGORY_BDA_NETWORK_TUNER         =
        {0x71985f48,0x1ca1,0x11d3,{0x9c,0xc8,0x00,0xc0,0x4f,0x79,0x71,0xe0}};
    const GUID KSDATAFORMAT_SUBTYPE_BDA_MPEG2_TRANSPORT =
        {0xF4AEB342,0x0329,0x4fdd,{0xA8,0xFD,0x4A,0xFF,0x49,0x26,0xC9,0x78}};

    const CLSID CLSID_IDvbSiParser2 =
        {0xF6B96EDA,0x1A94,0x4476,{0xA8,0x5F,0x4D,0x3D,0xC7,0xB3,0x9C,0x3F}};
    const CLSID CLSID_IAtscPsipParser =
        {0x3508C064,0xB94E,0x420b,{0xA8,0x21,0x20,0xC8,0x09,0x6F,0xAA,0xDC}};
    const CLSID CLSID_Mpeg2Data =
        {0xC666E115,0xBB62,0x4027,{0xA1,0x13,0x82,0xD6,0x43,0xFE,0x2D,0x99}};

    extern const CLSID CLSID_ATSCLocator;
    extern const CLSID CLSID_ATSCNetworkProvider;
    extern const CLSID CLSID_ATSCTuningSpace;
    extern const CLSID CLSID_DVBCLocator;
    extern const CLSID CLSID_DVBCNetworkProvider;
    extern const CLSID CLSID_DVBSLocator;
    extern const CLSID CLSID_DVBSNetworkProvider;
    extern const CLSID CLSID_DVBSTuningSpace;
    extern const CLSID CLSID_DVBTuningSpace;
    extern const CLSID CLSID_DVBTLocator;
    const CLSID CLSID_DVBTLocator2 =
        {0xefe3fa02, 0x45d7, 0x4920,{0xbe,0x96,0x53,0xfa,0x7f,0x35,0xb0,0xe6}};
    extern const CLSID CLSID_DVBTNetworkProvider;
    extern const CLSID CLSID_FilterGraph;
    extern const CLSID CLSID_InfTee;
    extern const CLSID CLSID_MPEG2Demultiplexer;
    extern const CLSID CLSID_NullRenderer;
    extern const CLSID CLSID_SampleGrabber;
    extern const CLSID CLSID_SystemDeviceEnum;
    extern const CLSID CLSID_SystemTuningSpaces;

    extern const IID IID_IATSCChannelTuneRequest;
    extern const IID IID_IATSCLocator;
    extern const IID IID_IBaseFilter;
    extern const IID IID_IBDA_DeviceControl;
    extern const IID IID_IBDA_FrequencyFilter;
    extern const IID IID_IBDA_SignalStatistics;
    /* Following symbol does not exist in library
    extern const IID IID_IBDA_Topology; */
    const IID IID_IBDA_Topology =
        {0x79B56888,0x7FEA,0x4690,{0xB4,0x5D,0x38,0xFD,0x3C,0x78,0x49,0xBE}};
    extern const IID IID_ICreateDevEnum;
    extern const IID IID_IDVBTLocator;
    const IID IID_IDVBTLocator2 =
        {0x448a2edf, 0xae95, 0x4b43,{0xa3,0xcc,0x74,0x78,0x43,0xc4,0x53,0xd4}};
    extern const IID IID_IDVBCLocator;
    extern const IID IID_IDVBSLocator;
    extern const IID IID_IDVBSTuningSpace;
    extern const IID IID_IDVBTuneRequest;
    extern const IID IID_IDVBTuningSpace;
    extern const IID IID_IDVBTuningSpace2;
    extern const IID IID_IGraphBuilder;
    extern const IID IID_IMediaControl;
    extern const IID IID_IMpeg2Demultiplexer;
    extern const IID IID_ISampleGrabber;
    extern const IID IID_IScanningTuner;
    extern const IID IID_ITuner;
    /* Following symbol does not exist in library
    extern const IID IID_ITunerCap; */
    const IID IID_ITunerCap =
        {0xE60DFA45,0x8D56,0x4e65,{0xA8,0xAB,0xD6,0xBE,0x94,0x12,0xC2,0x49}};
    extern const IID IID_ITuningSpace;
    extern const IID IID_ITuningSpaceContainer;
    /* Following symbol does not exist in library
    extern const IID IID_IMpeg2Data; */
    const IID IID_IMpeg2Data =
        {0x9B396D40,0xF380,0x4e3c,{0xA5,0x14,0x1A,0x82,0xBF,0x6E,0xBF,0xE6}};
    extern const IID IID_IGuideData;
    extern const IID IID_ISectionList;
    extern const IID IID_IEnumTuneRequests;
    extern const IID IID_IEnumGuideDataProperties;
    extern const IID IID_IGuideDataProperty;
    extern const IID IID_IMpeg2Stream;
    extern const IID IID_IGuideDataEvent;
    const IID IID_IDvbSiParser =
        {0xB758A7BD,0x14DC,0x449d,{0xB8,0x28,0x35,0x90,0x9A,0xCB,0x3B,0x1E}};
    const IID IID_IDvbSiParser2 =
        {0x0AC5525F,0xF816,0x42F4,{0x93,0xBA,0x4C,0x0F,0x32,0xF4,0x6E,0x54}};
    const IID IID_IIsdbSiParser2 =
        {0x900E4BB7,0x18CD,0x453F,{0x98,0xBE,0x3B,0xE6,0xAA,0x21,0x17,0x72}};
    const IID IID_IAtscPsipParser =
        {0xB2C98995,0x5EB2,0x4fb1,{0xB4,0x06,0xF3,0xE8,0xE2,0x02,0x6A,0x9A}};
    extern const GUID MEDIATYPE_MPEG2_SECTIONS;
    extern const GUID MEDIASUBTYPE_MPEG2_TRANSPORT;
    extern const GUID MEDIASUBTYPE_None;
    extern const GUID FORMAT_None;
    const GUID KSPROPSETID_BdaTunerExtensionProperties =
        {0xfaa8f3e5, 0x31d4, 0x4e41, {0x88, 0xef, 0xd9, 0xeb, 0x71, 0x6f, 0x6e, 0xc9}};
}

const DWORD KSPROPERTY_BDA_PLPINFO = 22;
