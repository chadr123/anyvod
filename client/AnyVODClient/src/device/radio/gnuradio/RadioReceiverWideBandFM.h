﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "RadioReceiverInterface.h"
#include "blocks/RadioComplexResampler.h"
#include "blocks/RadioFloatPointResampler.h"
#include "blocks/RadioBandPassFilter.h"
#include "blocks/RadioFMDemodulator.h"
#include "blocks/RadioStereoDemodulator.h"

#include <gnuradio/digital/probe_mpsk_snr_est_c.h>

class RadioReceiverWideBandFM : public RadioReceiverInterface
{
public:
    RadioReceiverWideBandFM();
    virtual ~RadioReceiverWideBandFM();

    virtual bool configure();

    virtual void setQuadRate(float rate);
    virtual void setAudioSampleRate(float rate);

    virtual void setEmphasis(double emphasis);

    virtual void setFilterParams(double low, double high, double transWidth);
    virtual void setDemodulator(RadioReaderInterface::DemodulatorType type);

    virtual float getSignalLevel();

private:
    static const double PREF_QUAD_RATE;
    static const double PREF_MIDLE_RATE;

private:
    float m_quadRate;
    int m_audioRate;
    RadioReaderInterface::DemodulatorType m_demodType;
    double m_lowFilter;
    double m_highFilter;
    double m_transWidthFilter;
    double m_emphasis;

    boost::shared_ptr<RadioComplexResampler> m_iqResampler;
    boost::shared_ptr<RadioFloatPointResampler> m_midleResampler;
    boost::shared_ptr<RadioBandPassFilter> m_bandPassFilter;
    gr::digital::probe_mpsk_snr_est_c::sptr m_signalMeter;
    boost::shared_ptr<RadioFMDemodulator> m_demodulator;
    boost::shared_ptr<RadioStereoDemodulator> m_stereo;
    boost::shared_ptr<RadioStereoDemodulator> m_mono;
};
