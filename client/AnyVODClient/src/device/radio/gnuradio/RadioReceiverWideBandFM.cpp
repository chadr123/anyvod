﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "RadioReceiverWideBandFM.h"

#include <QDebug>

const double RadioReceiverWideBandFM::PREF_QUAD_RATE = 240e3;
const double RadioReceiverWideBandFM::PREF_MIDLE_RATE = 120e3;

RadioReceiverWideBandFM::RadioReceiverWideBandFM() :
    RadioReceiverInterface("RadioReceiverWideBandFM"),
    m_quadRate(0.0f),
    m_audioRate(0),
    m_demodType(RadioReaderInterface::DT_NONE),
    m_lowFilter(0.0),
    m_highFilter(0.0),
    m_transWidthFilter(0.0)
{

}

RadioReceiverWideBandFM::~RadioReceiverWideBandFM()
{

}

bool RadioReceiverWideBandFM::configure()
{
    this->disconnect_all();

    this->m_iqResampler.reset();
    this->m_midleResampler.reset();
    this->m_bandPassFilter.reset();
    this->m_signalMeter.reset();
    this->m_demodulator.reset();
    this->m_stereo.reset();
    this->m_mono.reset();

    this->m_iqResampler = gnuradio::get_initial_sptr(new RadioComplexResampler());
    this->m_midleResampler = gnuradio::get_initial_sptr(new RadioFloatPointResampler());
    this->m_bandPassFilter = gnuradio::get_initial_sptr(new RadioBandPassFilter());
    this->m_signalMeter = gr::digital::probe_mpsk_snr_est_c::make(gr::digital::SNR_EST_M2M4, PREF_QUAD_RATE);
    this->m_demodulator = gnuradio::get_initial_sptr(new RadioFMDemodulator());
    this->m_stereo = gnuradio::get_initial_sptr(new RadioStereoDemodulator());
    this->m_mono = gnuradio::get_initial_sptr(new RadioStereoDemodulator());

    this->m_iqResampler->setRate(PREF_QUAD_RATE / this->m_quadRate);
    this->m_midleResampler->setRate(PREF_MIDLE_RATE / PREF_QUAD_RATE);
    this->m_bandPassFilter->setParameters(PREF_QUAD_RATE, this->m_lowFilter, this->m_highFilter, this->m_transWidthFilter);
    this->m_demodulator->setParameters(PREF_QUAD_RATE, 75000.0, 0.0);
    this->m_stereo->setParameters(PREF_MIDLE_RATE, this->m_audioRate, this->m_emphasis, true);
    this->m_mono->setParameters(PREF_MIDLE_RATE, this->m_audioRate, this->m_emphasis, false);

    this->connect(this->self(), 0, this->m_iqResampler, 0);
    this->connect(this->m_iqResampler, 0, this->m_bandPassFilter, 0);
    this->connect(this->m_bandPassFilter, 0, this->m_signalMeter, 0);
    this->connect(this->m_bandPassFilter, 0, this->m_demodulator, 0);
    this->connect(this->m_demodulator, 0, this->m_midleResampler, 0);

    switch (this->m_demodType)
    {
        case RadioReaderInterface::DT_WIDE_FM_MONO:
        {
            this->connect(this->m_midleResampler, 0, this->m_mono, 0);
            this->connect(this->m_mono, 0, this->self(), 0);
            this->connect(this->m_mono, 1, this->self(), 1);

            break;
        }
        case RadioReaderInterface::DT_WIDE_FM_STEREO:
        {
            this->connect(this->m_midleResampler, 0, this->m_stereo, 0);
            this->connect(this->m_stereo, 0, this->self(), 0);
            this->connect(this->m_stereo, 1, this->self(), 1);

            break;
        }
        default:
        {
            return false;
        }
    }

    return true;
}

void RadioReceiverWideBandFM::setQuadRate(float rate)
{
    this->m_quadRate = rate;
}

void RadioReceiverWideBandFM::setAudioSampleRate(float rate)
{
    this->m_audioRate = rate;
}

void RadioReceiverWideBandFM::setEmphasis(double emphasis)
{
    this->m_emphasis = emphasis;
}

void RadioReceiverWideBandFM::setFilterParams(double low, double high, double transWidth)
{
    this->m_lowFilter = low;
    this->m_highFilter = high;
    this->m_transWidthFilter = transWidth;
}

void RadioReceiverWideBandFM::setDemodulator(RadioReaderInterface::DemodulatorType type)
{
    this->m_demodType = type;
}

float RadioReceiverWideBandFM::getSignalLevel()
{
    return this->m_signalMeter->snr();
}
