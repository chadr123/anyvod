﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "RadioFloatPointResampler.h"
#include "RadioFMDeemphasis.h"
#include "RadioLowPassFilter.h"

#include <gnuradio/hier_block2.h>
#include <gnuradio/analog/pll_refout_cc.h>
#include <gnuradio/blocks/complex_to_imag.h>

#if __has_include(<gnuradio/filter/fir_filter_fcc.h>)
#include <gnuradio/filter/fir_filter_fcc.h>
#include <gnuradio/filter/fir_filter_fff.h>
#include <gnuradio/blocks/multiply_cc.h>
#include <gnuradio/blocks/multiply_ff.h>
#include <gnuradio/blocks/multiply_const_ff.h>
#include <gnuradio/blocks/add_ff.h>
#else
#include <gnuradio/filter/fir_filter_blk.h>
#include <gnuradio/blocks/multiply.h>
#include <gnuradio/blocks/multiply_const.h>
#include <gnuradio/blocks/add_blk.h>
#endif

class RadioStereoDemodulator : public gr::hier_block2
{
public:
    RadioStereoDemodulator();
    virtual ~RadioStereoDemodulator();

    void setParameters(float inputRate, float audioRate, double emphasis, bool stereo);

private:
    gr::filter::fir_filter_fcc::sptr m_pilotTone;
    gr::analog::pll_refout_cc::sptr m_pilotTonePLL;
    gr::blocks::multiply_cc::sptr m_subtone;
    gr::blocks::complex_to_imag::sptr m_complexTone;
    gr::filter::fir_filter_fff::sptr m_subtoneBPF;
    gr::blocks::multiply_ff::sptr m_balanceMixer;
    boost::shared_ptr<RadioLowPassFilter> m_lowPassFilterLeft;
    boost::shared_ptr<RadioLowPassFilter> m_lowPassFilterRight;
    boost::shared_ptr<RadioFloatPointResampler> m_audioResamplerLeft;
    boost::shared_ptr<RadioFloatPointResampler> m_audioResamplerRight;
    boost::shared_ptr<RadioFMDeemphasis> m_deemphasisLeft;
    boost::shared_ptr<RadioFMDeemphasis> m_deemphasisRight;
    gr::blocks::multiply_const_ff::sptr m_channelDeltaPlus;
    gr::blocks::multiply_const_ff::sptr m_channelDeltaMinus;
    gr::blocks::add_ff::sptr m_addLeft;
    gr::blocks::add_ff::sptr m_addRight;
};
