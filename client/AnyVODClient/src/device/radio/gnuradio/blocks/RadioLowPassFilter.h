﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <gnuradio/hier_block2.h>

#if __has_include(<gnuradio/filter/fir_filter_fff.h>)
#include <gnuradio/filter/fir_filter_fff.h>
#else
#include <gnuradio/filter/fir_filter_blk.h>
#endif

class RadioLowPassFilter : public gr::hier_block2
{
public:
    RadioLowPassFilter();
    virtual ~RadioLowPassFilter();

    void setParameters(double sampleRate, double cutoffFreq, double transWidth, double gain);

private:
    gr::filter::fir_filter_fff::sptr m_filter;
};
