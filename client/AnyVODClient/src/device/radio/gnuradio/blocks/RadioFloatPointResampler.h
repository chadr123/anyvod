﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "RadioResamplerBase.h"

#include <gnuradio/filter/pfb_arb_resampler_fff.h>

class RadioFloatPointResampler : public RadioResamplerBase
{
public:
    RadioFloatPointResampler();
    virtual ~RadioFloatPointResampler();

protected:
    virtual gr::basic_block_sptr prepareFilter(float rate, const std::vector<float> &taps, unsigned int filterSize);

private:
    gr::filter::pfb_arb_resampler_fff::sptr m_filter;
};
