﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "RadioFMDeemphasis.h"

#include <gnuradio/analog/quadrature_demod_cf.h>
#include <gnuradio/hier_block2.h>

class RadioFMDemodulator : public gr::hier_block2
{
public:
    RadioFMDemodulator();
    virtual ~RadioFMDemodulator();

    void setParameters(float quadRate, float maxDev, double tau);

private:
    gr::analog::quadrature_demod_cf::sptr m_demodulator;
    boost::shared_ptr<RadioFMDeemphasis> m_deemphasis;

    std::vector<float> m_taps;
};
