﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "RadioStereoDemodulator.h"

#include <gnuradio/filter/firdes.h>

RadioStereoDemodulator::RadioStereoDemodulator() :
    gr::hier_block2("stereo_demod",
                    gr::io_signature::make(1, 1, sizeof(float)),
                    gr::io_signature::make(2, 2, sizeof(float)))
{

}

RadioStereoDemodulator::~RadioStereoDemodulator()
{

}

void RadioStereoDemodulator::setParameters(float inputRate, float audioRate, double emphasis, bool stereo)
{
    const double cutoffFreq = 17e3;

    this->disconnect_all();

    this->m_pilotTone.reset();
    this->m_pilotTonePLL.reset();
    this->m_subtone.reset();
    this->m_complexTone.reset();
    this->m_subtoneBPF.reset();
    this->m_balanceMixer.reset();
    this->m_lowPassFilterLeft.reset();
    this->m_lowPassFilterRight.reset();
    this->m_audioResamplerLeft.reset();
    this->m_audioResamplerRight.reset();
    this->m_deemphasisLeft.reset();
    this->m_deemphasisRight.reset();
    this->m_channelDeltaPlus.reset();
    this->m_channelDeltaMinus.reset();
    this->m_addLeft.reset();
    this->m_addRight.reset();

    this->m_lowPassFilterLeft = gnuradio::get_initial_sptr(new RadioLowPassFilter());
    this->m_audioResamplerLeft = gnuradio::get_initial_sptr(new RadioFloatPointResampler());
    this->m_deemphasisLeft = gnuradio::get_initial_sptr(new RadioFMDeemphasis());

    this->m_lowPassFilterLeft->setParameters(inputRate, cutoffFreq, 2e3, 1.0);
    this->m_audioResamplerLeft->setRate(audioRate / inputRate);
    this->m_deemphasisLeft->setParameters(audioRate, emphasis);

    if (stereo)
    {
      this->m_lowPassFilterRight = gnuradio::get_initial_sptr(new RadioLowPassFilter());
      this->m_audioResamplerRight = gnuradio::get_initial_sptr(new RadioFloatPointResampler());
      this->m_deemphasisRight = gnuradio::get_initial_sptr(new RadioFMDeemphasis());

      this->m_lowPassFilterRight->setParameters(inputRate, cutoffFreq, 2e3, 1.0);
      this->m_audioResamplerRight->setRate(audioRate / inputRate);
      this->m_deemphasisRight->setParameters(audioRate, emphasis);

      std::vector<gr_complex> toneTaps = gr::filter::firdes::complex_band_pass(1.0, inputRate, 18800.0, 19200.0, 300.0);

      this->m_pilotTone = gr::filter::fir_filter_fcc::make(1, toneTaps);
      this->m_pilotTonePLL = gr::analog::pll_refout_cc::make(0.001, float(2.0 * M_PI * 19200 / inputRate), float(2 * M_PI * 18800 / inputRate));
      this->m_subtone = gr::blocks::multiply_cc::make();
      this->m_complexTone = gr::blocks::complex_to_imag::make();

      std::vector<float> pllTaps = gr::filter::firdes::band_pass(1.0, inputRate, 37600.0, 38400.0, 400.0);

      this->m_subtoneBPF = gr::filter::fir_filter_fff::make(1, pllTaps);

      this->m_balanceMixer = gr::blocks::multiply_ff::make();

      this->m_channelDeltaPlus = gr::blocks::multiply_const_ff::make(3.61);
      this->m_channelDeltaMinus = gr::blocks::multiply_const_ff::make(-3.61);

      this->m_addLeft = gr::blocks::add_ff::make();
      this->m_addRight = gr::blocks::add_ff::make();

      this->connect(this->self(), 0, this->m_pilotTone, 0);
      this->connect(this->m_pilotTone, 0, this->m_pilotTonePLL, 0);
      this->connect(this->m_pilotTonePLL, 0, this->m_subtone, 0);
      this->connect(this->m_pilotTonePLL, 0, this->m_subtone, 1);
      this->connect(this->m_subtone, 0, this->m_complexTone, 0);

      this->connect(this->m_complexTone,  0, this->m_subtoneBPF, 0);
      this->connect(this->m_subtoneBPF, 0, this->m_balanceMixer, 0);

      this->connect(this->self(), 0, this->m_balanceMixer, 1);

      this->connect(this->self(), 0, this->m_lowPassFilterLeft, 0);
      this->connect(this->m_balanceMixer,  0, this->m_lowPassFilterRight, 0);

      this->connect(this->m_lowPassFilterLeft, 0, this->m_audioResamplerLeft, 0);
      this->connect(this->m_lowPassFilterRight, 0, this->m_audioResamplerRight, 0);

      this->connect(this->m_audioResamplerRight, 0, this->m_channelDeltaPlus, 0);
      this->connect(this->m_audioResamplerRight, 0, this->m_channelDeltaMinus, 0);

      this->connect(this->m_audioResamplerLeft, 0, this->m_addLeft, 0);
      this->connect(this->m_channelDeltaPlus, 0, this->m_addLeft, 1);
      this->connect(this->m_addLeft, 0, this->m_deemphasisLeft, 0);
      this->connect(this->m_deemphasisLeft, 0, this->self(), 0);

      this->connect(this->m_audioResamplerLeft, 0, this->m_addRight, 0);
      this->connect(this->m_channelDeltaMinus, 0, this->m_addRight, 1);
      this->connect(this->m_addRight, 0, this->m_deemphasisRight, 0);
      this->connect(this->m_deemphasisRight, 0, this->self(), 1);
    }
    else
    {
      this->connect(this->self(), 0, this->m_lowPassFilterLeft, 0);
      this->connect(this->m_lowPassFilterLeft, 0, this->m_audioResamplerLeft, 0);
      this->connect(this->m_audioResamplerLeft, 0, this->m_deemphasisLeft, 0);
      this->connect(this->m_deemphasisLeft, 0, this->self(), 0);
      this->connect(this->m_deemphasisLeft, 0, this->self(), 1);
    }
}
