﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "RadioBandPassFilter.h"

#include <gnuradio/filter/firdes.h>

RadioBandPassFilter::RadioBandPassFilter() :
    gr::hier_block2("RadioBandPassFilter",
                    gr::io_signature::make(1, 1, sizeof(gr_complex)),
                    gr::io_signature::make(1, 1, sizeof (gr_complex)))
{

}

RadioBandPassFilter::~RadioBandPassFilter()
{

}

void RadioBandPassFilter::setParameters(double sampleRate, double low, double high, double transWith)
{
    const double limit = 0.95 * sampleRate / 2.0;

    this->disconnect_all();

    if (low < -limit)
        low = -limit;

    if (high > limit)
        high = limit;

    auto taps = gr::filter::firdes::complex_band_pass(1.0, sampleRate, low, high, transWith);

    this->m_filter.reset();
    this->m_filter = gr::filter::fir_filter_ccc::make(1, taps);

    this->connect(this->self(), 0, this->m_filter, 0);
    this->connect(this->m_filter, 0, this->self(), 0);
}
