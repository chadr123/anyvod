﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "RadioResamplerBase.h"

#include <gnuradio/filter/firdes.h>

RadioResamplerBase::RadioResamplerBase(const std::string &name, size_t inputSize, size_t outputSize) :
    gr::hier_block2(name,
                    gr::io_signature::make(1, 1, inputSize),
                    gr::io_signature::make(1, 1, outputSize))
{

}

RadioResamplerBase::~RadioResamplerBase()
{

}

void RadioResamplerBase::setRate(float rate)
{
    double cutoff = rate > 1.0 ? 0.4 : 0.4 * rate;
    double transWidth = rate > 1.0 ? 0.2 : 0.2 * rate;
    unsigned int filterSize = 32;

    this->disconnect_all();

    std::vector<float> taps = gr::filter::firdes::low_pass(filterSize, filterSize, cutoff, transWidth);
    gr::basic_block_sptr filter = this->prepareFilter(rate, taps, filterSize);

    this->connect(this->self(), 0, filter, 0);
    this->connect(filter, 0, this->self(), 0);
}
