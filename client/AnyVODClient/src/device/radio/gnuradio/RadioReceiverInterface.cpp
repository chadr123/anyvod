﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "RadioReceiverInterface.h"

RadioReceiverInterface::RadioReceiverInterface(const std::string &name) :
    gr::hier_block2(name,
                    gr::io_signature::make(1, 1, sizeof(gr_complex)),
                    gr::io_signature::make(2, 2, sizeof(float)))
{

}

RadioReceiverInterface::~RadioReceiverInterface()
{

}
