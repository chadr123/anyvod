﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "../RadioReaderInterface.h"
#include "gnuradio/blocks/RadioFIRDecimator.h"
#include "gnuradio/blocks/RadioDownConverter.h"
#include "gnuradio/blocks/RadioAudioSink.h"
#include "gnuradio/RadioReceiverInterface.h"

#if __has_include(<gnuradio/blocks/multiply_const_ff.h>)
# include <gnuradio/blocks/multiply_const_ff.h>
#else
# include <gnuradio/blocks/multiply_const.h>
#endif

#include <gnuradio/top_block.h>
#include <osmosdr/source.h>

class RadioGNURadio : public RadioReaderInterface
{
public:
    RadioGNURadio();
    virtual ~RadioGNURadio();

    virtual bool open(const QString &path);
    virtual void close();
    virtual int read(uint8_t *buf, int size);

    virtual void setFrequency(double freqHz);
    virtual double getFrequency();

    virtual float getSignalStrength() const;

    virtual bool setDecimation(int decimation);
    virtual int getDecimation() const;

    virtual void setAudioGain(float db);
    virtual float getAudioGain() const;

    virtual void enableAGC(bool enable);
    virtual bool isEnabledAGC() const;

    virtual void setEmphasis(double emphasis);

    virtual void setAntenna(const QString &antenna);
    virtual QStringList getAntennas() const;

    virtual bool tune();
    virtual bool getAdapterList(QVector<AdapterInfo> *ret) const;

private:
    struct RIFFHeader
    {
        quint32 chunkID;
        quint32 chunkSize;
        quint32 format;
        quint32 subChunk1ID;
        quint32 subChunk1Size;
        quint16 audioFormat;
        quint16 numChannels;
        quint32 sampleRate;
        quint32 byteRate;
        quint16 blockAlign;
        quint16 bitPerSample;
        quint32 subChunk2ID;
        quint32 subChunk2Size;
    };

    struct FilterParams
    {
        double low;
        double high;
    };

    enum Phase
    {
        PH_NONE,
        PH_HEADER,
        PH_BODY,
        PH_TRAILER
    };

private:
    void start();
    void stop();
    void closeInternal();

    void connectAll();
    void selectInputRate(const QString &devArgs);

private:
    static const float DEFAULT_AUDIO_GAIN;
    static const QPair<QString, uint64_t> DEFAULT_INPUT_RATES[];
    static const FilterParams FILTER_PARAMS[RadioReaderInterface::DT_COUNT];

private:
    gr::top_block_sptr m_topBlock;
    osmosdr::source::sptr m_source;

    gr::blocks::multiply_const_ff::sptr m_audioGainLeft;
    gr::blocks::multiply_const_ff::sptr m_audioGainRight;

    boost::shared_ptr<RadioFIRDecimator> m_firDecimator;
    boost::shared_ptr<RadioDownConverter> m_downConverter;
    boost::shared_ptr<RadioAudioSink> m_audioSink;
    boost::shared_ptr<RadioReceiverInterface> m_rx;

    uint64_t m_inputSampleRate;
    uint32_t m_audioSampleRate;
    int m_decimation;
    double m_emphasis;
    Phase m_phase;
    int m_channelCount;
};
