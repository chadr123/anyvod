﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "AppleRemote.h"
#include "ui/MainWindowInterface.h"
#include "delegate/MediaPlayerDelegate.h"
#include "factories/MainWindowFactory.h"

#include <QDebug>

const NSTimeInterval AppleRemote::DEFAULT_MAXIMUM_CLICK_TIME_DIFFERENCE = 0.35;
const NSTimeInterval AppleRemote::HOLD_RECOGNITION_TIME_INTERVAL = 0.4;
const int AppleRemote::REMOTE_SWITCH_COOKIE = 19;

AppleRemote::AppleRemote() :
    m_hidDeviceInterface(nullptr),
    m_queue(nullptr),
    m_cookieToButtonMapping(nil),
    m_allCookies(nil),
    m_openInExclusiveMode(false),
    m_simulatePlusMinusHold(false),
    m_processesBacklog(false),
    m_maxClickTimeDifference(0.0),
    m_eventSource(nullptr),
    m_clickCountEnabledButtons(kRemoteButtonPlay),
    m_eventClickCount(0),
    m_remoteButtonHold(false)
{

}

AppleRemote::~AppleRemote()
{
    this->deInit();
}

void AppleRemote::init()
{
    NSMutableDictionary *buttonMapping = [[NSMutableDictionary alloc] init];

    if (@available(macOS 10.15, *))
    {
        [buttonMapping setObject:[NSNumber numberWithInt:kRemoteButtonVolume_Plus]    forKey:@"35_23_22_17_14_4_3_35_23_22_4_3_"];
        [buttonMapping setObject:[NSNumber numberWithInt:kRemoteButtonVolume_Minus]   forKey:@"35_23_22_18_14_4_3_35_23_22_4_3_"];
        [buttonMapping setObject:[NSNumber numberWithInt:kRemoteButtonMenu]           forKey:@"35_24_23_22_4_3_35_24_23_22_4_3_"];
        [buttonMapping setObject:[NSNumber numberWithInt:kRemoteButtonPlay]           forKey:@"35_25_23_22_4_3_35_25_23_22_4_3_"];
        [buttonMapping setObject:[NSNumber numberWithInt:kRemoteButtonRight]          forKey:@"35_26_23_22_4_3_35_26_23_22_4_3_"];
        [buttonMapping setObject:[NSNumber numberWithInt:kRemoteButtonLeft]           forKey:@"35_27_23_22_4_3_35_27_23_22_4_3_"];
        [buttonMapping setObject:[NSNumber numberWithInt:kRemoteButtonRight_Hold]     forKey:@"35_23_22_16_14_4_3_"];
        [buttonMapping setObject:[NSNumber numberWithInt:kRemoteButtonLeft_Hold]      forKey:@"35_23_22_15_14_4_3_"];
        [buttonMapping setObject:[NSNumber numberWithInt:kRemoteButtonMenu_Hold]      forKey:@"35_23_22_4_3_35_23_22_4_3_"];
        [buttonMapping setObject:[NSNumber numberWithInt:kRemoteButtonPlay_Sleep]     forKey:@"39_35_23_22_4_3_39_35_23_22_4_3_"];
        [buttonMapping setObject:[NSNumber numberWithInt:k2009RemoteButtonPlay]       forKey:@"35_23_22_10_4_3_35_23_22_10_4_3_"];
        [buttonMapping setObject:[NSNumber numberWithInt:k2009RemoteButtonFullscreen] forKey:@"35_23_22_4_3_35_23_22_4_3_"];
        [buttonMapping setObject:[NSNumber numberWithInt:kRemoteControl_Switched]     forKey:@"44_35_23_22_4_3_35_23_22_4_3_"];
    }
    else
    {
        [buttonMapping setObject:[NSNumber numberWithInt:kRemoteButtonVolume_Plus]    forKey:@"33_31_30_21_20_2_"];
        [buttonMapping setObject:[NSNumber numberWithInt:kRemoteButtonVolume_Minus]   forKey:@"33_32_30_21_20_2_"];
        [buttonMapping setObject:[NSNumber numberWithInt:kRemoteButtonMenu]           forKey:@"33_22_21_20_2_33_22_21_20_2_"];
        [buttonMapping setObject:[NSNumber numberWithInt:kRemoteButtonPlay]           forKey:@"33_23_21_20_2_33_23_21_20_2_"];
        [buttonMapping setObject:[NSNumber numberWithInt:kRemoteButtonRight]          forKey:@"33_24_21_20_2_33_24_21_20_2_"];
        [buttonMapping setObject:[NSNumber numberWithInt:kRemoteButtonLeft]           forKey:@"33_25_21_20_2_33_25_21_20_2_"];
        [buttonMapping setObject:[NSNumber numberWithInt:kRemoteButtonRight_Hold]     forKey:@"33_21_20_14_12_2_"];
        [buttonMapping setObject:[NSNumber numberWithInt:kRemoteButtonLeft_Hold]      forKey:@"33_21_20_13_12_2_"];
        [buttonMapping setObject:[NSNumber numberWithInt:kRemoteButtonMenu_Hold]      forKey:@"33_21_20_2_33_21_20_2_"];
        [buttonMapping setObject:[NSNumber numberWithInt:kRemoteButtonPlay_Sleep]     forKey:@"37_33_21_20_2_37_33_21_20_2_"];
        [buttonMapping setObject:[NSNumber numberWithInt:k2009RemoteButtonPlay]       forKey:@"33_21_20_8_2_33_21_20_8_2_"];
        [buttonMapping setObject:[NSNumber numberWithInt:k2009RemoteButtonFullscreen] forKey:@"33_21_20_3_2_33_21_20_3_2_"];
        [buttonMapping setObject:[NSNumber numberWithInt:kRemoteControl_Switched]     forKey:@"42_33_23_21_20_2_33_23_21_20_2_"];

        if (@available(macOS 10.13, *)) {
            [buttonMapping setObject:[NSNumber numberWithInt:kRemoteButtonVolume_Plus]    forKey:@"33_21_20_15_12_2_"];
            [buttonMapping setObject:[NSNumber numberWithInt:kRemoteButtonVolume_Minus]   forKey:@"33_21_20_16_12_2_"];
        }
    }

    this->m_cookieToButtonMapping = [[NSDictionary alloc] initWithDictionary: buttonMapping];
    [buttonMapping release];

    this->m_simulatePlusMinusHold = true;
    this->m_maxClickTimeDifference = DEFAULT_MAXIMUM_CLICK_TIME_DIFFERENCE;
    this->m_openInExclusiveMode = true;
}

void AppleRemote::deInit()
{
    this->stop();

    if (this->m_cookieToButtonMapping != nil)
    {
        [this->m_cookieToButtonMapping release];
        this->m_cookieToButtonMapping = nil;
    }

    this->m_openInExclusiveMode = false;
    this->m_simulatePlusMinusHold = false;
    this->m_maxClickTimeDifference = 0.0;
}

bool AppleRemote::start()
{
    if (this->isStarted())
        return false;

    io_object_t hidDevice = this->findAppleRemoteDevice();

    if (hidDevice == 0)
        return false;

    if (!this->createInterfaceForDevice(hidDevice))
    {
        this->stop();
        IOObjectRelease(hidDevice);

        return false;
    }

    if (!this->initializeCookies())
    {
        this->stop();
        IOObjectRelease(hidDevice);

        return false;
    }

    if (!this->openDevice())
    {
        this->stop();
        IOObjectRelease(hidDevice);

        return false;
    }

    IOObjectRelease(hidDevice);

    return true;
}

void AppleRemote::stop()
{
    if (this->m_eventSource)
    {
        CFRunLoopRemoveSource(CFRunLoopGetCurrent(), this->m_eventSource, kCFRunLoopDefaultMode);
        CFRelease(this->m_eventSource);

        this->m_eventSource = nullptr;
    }

    if (this->m_queue)
    {
        (*this->m_queue)->stop(this->m_queue);
        (*this->m_queue)->dispose(this->m_queue);
        (*this->m_queue)->Release(this->m_queue);

        this->m_queue = nullptr;
    }

    if (this->m_allCookies != nil)
    {
        [this->m_allCookies autorelease];
        this->m_allCookies = nil;
    }

    if (this->m_hidDeviceInterface)
    {
        (*this->m_hidDeviceInterface)->close(this->m_hidDeviceInterface);
        (*this->m_hidDeviceInterface)->Release(this->m_hidDeviceInterface);

        this->m_hidDeviceInterface = nullptr;
    }
}

bool AppleRemote::isStarted() const
{
    return this->m_hidDeviceInterface && this->m_allCookies && this->m_queue;
}

io_object_t AppleRemote::findAppleRemoteDevice() const
{
    CFMutableDictionaryRef hid;
    IOReturn ret;
    io_iterator_t iterator = 0;
    io_object_t device = 0;

    hid = IOServiceMatching("AppleIRController");
    ret = IOServiceGetMatchingServices(kIOMasterPortDefault, hid, &iterator);

    if (ret == kIOReturnSuccess && iterator != 0)
        device = IOIteratorNext(iterator);

    IOObjectRelease(iterator);

    return device;
}

bool AppleRemote::createInterfaceForDevice(io_object_t hidDevice)
{
    io_name_t className;
    IOCFPlugInInterface **interface = nullptr;
    HRESULT result = S_OK;
    SInt32 score = 0;
    IOReturn ret;

    ret = IOObjectGetClass(hidDevice, className);

    if (ret != kIOReturnSuccess)
        return false;

    ret = IOCreatePlugInInterfaceForService(hidDevice, kIOHIDDeviceUserClientTypeID, kIOCFPlugInInterfaceID,
                                            &interface, &score);
    if (ret == kIOReturnSuccess)
    {
        result = (*interface)->QueryInterface(interface, CFUUIDGetUUIDBytes(kIOHIDDeviceInterfaceID),
                                              (LPVOID*)&this->m_hidDeviceInterface);

        if (interface)
            (*interface)->Release(interface);

        return result == S_OK;
    }

    return false;
}

bool AppleRemote::initializeCookies()
{
    IOHIDDeviceInterface122 **handle = (IOHIDDeviceInterface122**)this->m_hidDeviceInterface;
    IOHIDElementCookie cookie;
    long usage;
    long usagePage;
    id object;
    NSArray *elements = nil;
    NSDictionary *element = nil;
    IOReturn success;

    if (!handle || !(*handle))
        return false;

    success = (*handle)->copyMatchingElements(handle, nullptr, (CFArrayRef*)&elements);

    if (success != kIOReturnSuccess)
    {
        if (elements)
            [elements release];

        return false;
    }

    NSMutableArray *mutableAllCookies = [[NSMutableArray alloc] init];
    NSUInteger elementCount = [elements count];

    for (NSUInteger i = 0; i < elementCount; i++)
    {
        element = [elements objectAtIndex:i];

        object = [element valueForKey: (NSString*)CFSTR(kIOHIDElementCookieKey) ];

        if (object == nil || ![object isKindOfClass:[NSNumber class]])
            continue;

        if (object == 0 || CFGetTypeID(object) != CFNumberGetTypeID())
            continue;

        cookie = (IOHIDElementCookie)[object longValue];

        object = [element valueForKey: (NSString*)CFSTR(kIOHIDElementUsageKey) ];

        if (object == nil || ![object isKindOfClass:[NSNumber class]])
            continue;

        usage = [object longValue];

        object = [element valueForKey: (NSString*)CFSTR(kIOHIDElementUsagePageKey) ];

        if (object == nil || ![object isKindOfClass:[NSNumber class]])
            continue;

        usagePage = [object longValue];

        [mutableAllCookies addObject: [NSNumber numberWithInt:(int)cookie]];
    }

    this->m_allCookies = [[NSArray alloc] initWithArray: mutableAllCookies];

    [mutableAllCookies release];
    [elements release];

    return true;
}

bool AppleRemote::openDevice()
{
    IOHIDOptionsType openMode = kIOHIDOptionsTypeNone;

    if (this->m_openInExclusiveMode)
        openMode = kIOHIDOptionsTypeSeizeDevice;

    IOReturn ioReturnValue = (*this->m_hidDeviceInterface)->open(this->m_hidDeviceInterface, openMode);

    if (ioReturnValue != KERN_SUCCESS)
        return false;

    this->m_queue = (*this->m_hidDeviceInterface)->allocQueue(this->m_hidDeviceInterface);

    if (!this->m_queue)
        return false;

    (*this->m_queue)->create(this->m_queue, 0, 12);

    NSUInteger cookieCount = [this->m_allCookies count];

    for (NSUInteger i = 0; i < cookieCount; i++)
    {
        IOHIDElementCookie cookie = (IOHIDElementCookie)[[this->m_allCookies objectAtIndex:i] intValue];

        (*this->m_queue)->addElement(this->m_queue, cookie, 0);
    }

    ioReturnValue = (*this->m_queue)->createAsyncEventSource(this->m_queue, &this->m_eventSource);

    if (ioReturnValue != KERN_SUCCESS)
        return false;

    ioReturnValue = (*this->m_queue)->setEventCallout(this->m_queue, AppleRemote::queueCallbackFunction, this, nullptr);

    if (ioReturnValue != KERN_SUCCESS)
        return false;

    CFRunLoopAddSource(CFRunLoopGetCurrent(), this->m_eventSource, kCFRunLoopDefaultMode);

    (*this->m_queue)->start(this->m_queue);

    return true;
}

void AppleRemote::handleEventWithCookieString(NSString *cookieString, SInt32 sumOfValues)
{
    if (cookieString == nil || [cookieString length] == 0)
        return;

    NSNumber *buttonID = [this->m_cookieToButtonMapping objectForKey: cookieString];

    if (buttonID != nil)
    {
        this->sendRemoteButtonEvent((AppleRemoteEventIdentifier)[buttonID intValue], sumOfValues > 0);
    }
    else
    {
        NSString *subCookieString;
        NSString *lastSubCookieString = nil;

        while ((subCookieString = this->validCookieSubstring(cookieString)))
        {
            cookieString = [cookieString substringFromIndex: [subCookieString length]];
            lastSubCookieString = subCookieString;

            if (this->m_processesBacklog)
                this->handleEventWithCookieString(subCookieString, sumOfValues);
        }

        if (!this->m_processesBacklog && lastSubCookieString != nil)
            this->handleEventWithCookieString(lastSubCookieString, 0);
    }
}

void AppleRemote::sendRemoteButtonEvent(AppleRemoteEventIdentifier event, bool pressedDown)
{
    if (this->m_simulatePlusMinusHold)
    {
        if (event == kRemoteButtonVolume_Plus || event == kRemoteButtonVolume_Minus)
        {
            if (pressedDown)
            {
                this->m_lastPlusMinusEvent = event;
                this->m_lastPlusMinusEventTime = [NSDate timeIntervalSinceReferenceDate];

                double time = this->m_lastPlusMinusEventTime;
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(HOLD_RECOGNITION_TIME_INTERVAL * NSEC_PER_SEC)),
                               dispatch_get_main_queue(),
                ^{
                    bool startSimulateHold = false;
                    AppleRemoteEventIdentifier event = this->m_lastPlusMinusEvent;

                    this->m_lock.lock();
                    startSimulateHold = (this->m_lastPlusMinusEvent > 0 && this->m_lastPlusMinusEventTime == time);
                    this->m_lock.unlock();

                    if (startSimulateHold)
                    {
                        this->m_lastEventSimulatedHold = true;
                        event = (event == kRemoteButtonVolume_Plus) ? kRemoteButtonVolume_Plus_Hold : kRemoteButtonVolume_Minus_Hold;

                        this->appleRemoteButton(event, pressedDown, 1);
                    }
                });

                return;
            }
            else
            {
                if (this->m_lastEventSimulatedHold)
                {
                    event = (event == kRemoteButtonVolume_Plus) ? kRemoteButtonVolume_Plus_Hold : kRemoteButtonVolume_Minus_Hold;

                    this->m_lastPlusMinusEvent = (AppleRemoteEventIdentifier)0;
                    this->m_lastEventSimulatedHold = false;
                }
                else
                {
                    this->m_lock.lock();
                    this->m_lastPlusMinusEvent = (AppleRemoteEventIdentifier)0;
                    this->m_lock.unlock();

                    pressedDown = true;
                }
            }
        }
    }

    if ((this->m_clickCountEnabledButtons & event) == event)
    {
        if (!pressedDown && (event == kRemoteButtonVolume_Minus || event == kRemoteButtonVolume_Plus))
            return;

        AppleRemoteEventIdentifier eventNumber;
        double timeNumber;

        this->m_lock.lock();

        this->m_lastClickCountEventTime = [NSDate timeIntervalSinceReferenceDate];

        if (this->m_lastClickCountEvent == event)
            this->m_eventClickCount++;
        else
            this->m_eventClickCount = 1;

        this->m_lastClickCountEvent = event;

        timeNumber = this->m_lastClickCountEventTime;
        eventNumber = event;

        this->m_lock.unlock();

        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(this->m_maxClickTimeDifference * NSEC_PER_SEC)),
                       dispatch_get_main_queue(),
        ^{
            AppleRemoteEventIdentifier event = eventNumber;
            NSTimeInterval eventTimePoint = timeNumber;
            BOOL finishedClicking = false;
            int finalClickCount = this->m_eventClickCount;

            this->m_lock.lock();

            finishedClicking = (event != this->m_lastClickCountEvent || eventTimePoint == this->m_lastClickCountEventTime);

            if (finishedClicking)
                this->m_eventClickCount = 0;

            this->m_lock.unlock();

            if (finishedClicking)
            {
                this->appleRemoteButton(event, true, finalClickCount);

                if (!this->m_simulatePlusMinusHold && (event == kRemoteButtonVolume_Minus || event == kRemoteButtonVolume_Plus))
                {
                    [NSThread sleepUntilDate: [NSDate dateWithTimeIntervalSinceNow:0.1]];
                    this->appleRemoteButton(event, false, finalClickCount);
                }
            }
        });
    }
    else
    {
        this->appleRemoteButton(event, pressedDown, 1);
    }
}

NSString* AppleRemote::validCookieSubstring(NSString *cookieString)
{
    if (cookieString == nil || [cookieString length] == 0)
        return nil;

    NSEnumerator *keyEnum = [this->m_cookieToButtonMapping keyEnumerator];
    NSString *key;

    while ((key = [keyEnum nextObject]))
    {
        NSRange range = [cookieString rangeOfString:key];

        if (range.location == 0)
            return key;
    }

    return nil;
}

void AppleRemote::appleRemoteButton(AppleRemote::AppleRemoteEventIdentifier button, bool pressedDown, unsigned int count)
{
    MainWindowInterface *window = MainWindowFactory::getInstance();
    MediaPlayerDelegate &playerDelegate = MediaPlayerDelegate::getInstance();

    if (!window)
        return;

    switch (button)
    {
        case k2009RemoteButtonFullscreen:
        {
            window->fullScreen();
            break;
        }
        case k2009RemoteButtonPlay:
        {
            window->toggle();
            break;
        }
        case kRemoteButtonPlay:
        {
            if (count >= 2)
                window->fullScreen();
            else
                window->toggle();

            break;
        }
        case kRemoteButtonVolume_Plus:
        {
            window->volumeUp();
            break;
        }
        case kRemoteButtonVolume_Minus:
        {
            window->volumeDown();
            break;
        }
        case kRemoteButtonRight:
        {
            playerDelegate.forward5();
            break;
        }
        case kRemoteButtonLeft:
        {
            playerDelegate.rewind5();
            break;
        }
        case kRemoteButtonRight_Hold:
        case kRemoteButtonLeft_Hold:
        case kRemoteButtonVolume_Plus_Hold:
        case kRemoteButtonVolume_Minus_Hold:
        {
            this->m_remoteButtonHold = pressedDown;

            if (pressedDown)
                this->executeHoldActionForRemoteButton(button);

            break;
        }
        case kRemoteButtonMenu:
        {
            playerDelegate.detail();
            break;
        }
        case kRemoteButtonPlay_Sleep:
        {
            NSAppleScript *script = [[NSAppleScript alloc] initWithSource:@"tell application \"System Events\" to sleep"];

            [script executeAndReturnError:nil];
            [script release];

            break;
        }
        default:
        {
            break;
        }
    }
}

void AppleRemote::executeHoldActionForRemoteButton(AppleRemote::AppleRemoteEventIdentifier button)
{
    MainWindowInterface *window = MainWindowFactory::getInstance();
    MediaPlayerDelegate &playerDelegate = MediaPlayerDelegate::getInstance();

    if (!window)
        return;

    if (this->m_remoteButtonHold)
    {
        switch (button)
        {
            case kRemoteButtonRight_Hold:
            {
                playerDelegate.forward30();
                break;
            }
            case kRemoteButtonLeft_Hold:
            {
                playerDelegate.rewind30();
                break;
            }
            case kRemoteButtonVolume_Plus_Hold:
            {
                window->volumeUp();
                break;
            }
            case kRemoteButtonVolume_Minus_Hold:
            {
                window->volumeDown();
                break;
            }
            default:
            {
                break;
            }
        }

        if (this->m_remoteButtonHold)
        {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)),
                           dispatch_get_main_queue(),
            ^{
                this->executeHoldActionForRemoteButton(button);
            });
        }
    }
}

void AppleRemote::queueCallbackFunction(void *target, IOReturn result, void *refcon, void *sender)
{
    (void)refcon;
    (void)sender;
    AppleRemote *remote = (AppleRemote*)target;
    IOHIDEventStruct event;
    AbsoluteTime zeroTime = {0, 0};
    NSMutableString *cookieString = [NSMutableString string];
    SInt32 sumOfValues = 0;

    while (result == kIOReturnSuccess)
    {
        result = (*(remote->m_queue))->getNextEvent(remote->m_queue, &event, zeroTime, 0);

        if (result != kIOReturnSuccess)
            continue;

        if (REMOTE_SWITCH_COOKIE == (int)event.elementCookie)
        {
            remote->handleEventWithCookieString(@"19_", 0);
        }
        else
        {
            if (((int)event.elementCookie) != 5)
            {
                sumOfValues += event.value;
                [cookieString appendString:[NSString stringWithFormat:@"%d_", event.elementCookie]];
            }
        }
    }

    remote->handleEventWithCookieString(cookieString, sumOfValues);
}
