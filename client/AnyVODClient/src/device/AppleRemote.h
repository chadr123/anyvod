﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#import <Cocoa/Cocoa.h>
#import <mach/mach.h>
#import <mach/mach_error.h>
#import <IOKit/IOKitLib.h>
#import <IOKit/IOCFPlugIn.h>
#import <IOKit/hid/IOHIDLib.h>
#import <IOKit/hid/IOHIDKeys.h>

#include <QMutex>

class AppleRemote
{
public:
    AppleRemote();
    ~AppleRemote();

    void init();
    void deInit();

    bool start();
    void stop();

private:
    enum AppleRemoteEventIdentifier
    {
        kRemoteButtonVolume_Plus        = 1 << 1,
        kRemoteButtonVolume_Minus       = 1 << 2,
        kRemoteButtonMenu               = 1 << 3,
        kRemoteButtonPlay               = 1 << 4,
        kRemoteButtonRight              = 1 << 5,
        kRemoteButtonLeft               = 1 << 6,
        kRemoteButtonRight_Hold         = 1 << 7,
        kRemoteButtonLeft_Hold          = 1 << 8,
        kRemoteButtonMenu_Hold          = 1 << 9,
        kRemoteButtonPlay_Sleep         = 1 << 10,
        kRemoteControl_Switched         = 1 << 11,
        kRemoteButtonVolume_Plus_Hold   = 1 << 12,
        kRemoteButtonVolume_Minus_Hold  = 1 << 13,
        k2009RemoteButtonPlay           = 1 << 14,
        k2009RemoteButtonFullscreen     = 1 << 15
    };

private:
    bool isStarted() const;
    io_object_t findAppleRemoteDevice() const;
    bool createInterfaceForDevice(io_object_t hidDevice);
    bool initializeCookies();
    bool openDevice();
    void handleEventWithCookieString(NSString *cookieString, SInt32 sumOfValues);
    void sendRemoteButtonEvent(AppleRemoteEventIdentifier event, bool pressedDown);
    NSString* validCookieSubstring(NSString *cookieString);
    void appleRemoteButton(AppleRemoteEventIdentifier button, bool pressedDown, unsigned int count);
    void executeHoldActionForRemoteButton(AppleRemoteEventIdentifier button);

private:
    static const NSTimeInterval DEFAULT_MAXIMUM_CLICK_TIME_DIFFERENCE;
    static const NSTimeInterval HOLD_RECOGNITION_TIME_INTERVAL;
    static const int REMOTE_SWITCH_COOKIE;

private:
    static void queueCallbackFunction(void* target, IOReturn result, void* refcon, void* sender);

private:
    QMutex m_lock;
    IOHIDDeviceInterface **m_hidDeviceInterface;
    IOHIDQueueInterface **m_queue;
    NSDictionary *m_cookieToButtonMapping;
    NSArray *m_allCookies;
    bool m_openInExclusiveMode;
    bool m_simulatePlusMinusHold;
    bool m_processesBacklog;
    NSTimeInterval m_maxClickTimeDifference;
    CFRunLoopSourceRef m_eventSource;
    AppleRemoteEventIdentifier m_lastPlusMinusEvent;
    NSTimeInterval m_lastPlusMinusEventTime;
    bool m_lastEventSimulatedHold;
    unsigned int m_clickCountEnabledButtons;
    NSTimeInterval m_lastClickCountEventTime;
    AppleRemoteEventIdentifier m_lastClickCountEvent;
    unsigned int m_eventClickCount;
    bool m_remoteButtonHold;
};
