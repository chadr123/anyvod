﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "DTVReaderInterface.h"
#include "DTVDShowDefs.h"

#include "../../../common/MemBuffer.h"

#include <QMutex>
#include <QWaitCondition>

class DTVDShow : public DTVReaderInterface, ISampleGrabberCB
{
public:
    DTVDShow();
    virtual ~DTVDShow();

protected:
    virtual bool open(const QString &path);
    virtual void close();
    virtual int read(uint8_t *buf, int size);

    virtual unsigned int enumSystems();
    virtual float getSignalStrength() const;
    virtual float getSignalNoiseRatio() const;
    virtual QString getChannelName();
    virtual void getEPG(QVector<DTVReaderInterface::EPG> *ret);
    virtual QDateTime getCurrentDateTime();
    virtual bool tune();

    virtual bool getAdapterList(QVector<AdapterInfo> *ret) const;

    virtual bool setInversion(DTVReaderInterface::Inversion inversion);

    /* DVB-C */
    virtual bool setDVBC(uint32_t freq, const QString &mod, uint32_t srate, uint32_t fec);

    /* DVB-S */
    virtual bool setDVBS(uint64_t freq, uint32_t srate, uint32_t fec);
    virtual bool setDVBS2(uint64_t freq, const QString &mod, uint32_t srate, uint32_t fec, int pilot, int rolloff, uint8_t sid);
    virtual bool setSEC(uint64_t freq, uint32_t srate, uint32_t fec, char pol, uint32_t lowf, uint32_t highf, uint32_t switchf,
                        int32_t highv, uint32_t satNo, uint32_t uncommitted);

    /* DVB-T */
    virtual bool setDVBT(uint32_t freq, const QString &mod, uint32_t fecHP, uint32_t fecLP,
                         uint32_t bandwidth, int transmission, uint32_t guard, int hierarchy);
    virtual bool setDVBT2(uint32_t freq, const QString &mod, uint32_t fec, uint32_t bandwidth,
                          int transmission, uint32_t guard, uint8_t plp);

    /* ATSC */
    virtual bool setATSC(uint32_t freq, const QString &mod);
    virtual bool setCQAM(uint32_t freq, const QString &mod);

    /* ISDB-C */
    virtual bool setISDBC(uint32_t freq, const QString &mod, uint32_t srate, uint32_t fec);

    /* ISDB-S */
    virtual bool setISDBS(uint64_t freq, uint16_t tsID);

    /* ISDB-T */
    virtual bool setISDBT(uint32_t freq, uint32_t bandwidth, int transmission, uint32_t guard, const ISDBTLayer layers[3]);

private:
    bool submitTuneRequest();
    bool setUpTuner(REFCLSID networkType);
    bool getNextNetworkType(CLSID *networkType) const;
    bool build();
    bool check(REFCLSID networkType);
    bool getFilterName(IBaseFilter *filter, QString *ret) const;
    unsigned int getSystems(REFCLSID clsid) const;
    bool findFilter(REFCLSID clsid, long *monikerUsed, IBaseFilter *upStream, IBaseFilter **downStream);
    bool listAdapters(REFCLSID clsid, QVector<AdapterInfo> *ret) const;
    bool connect(IBaseFilter *filterUpStream, IBaseFilter *filterDownStream) const;
    bool start();
    void destroy();
    bool registerGraph();
    void deregisterGraph();
    bool getDTVParser(IUnknown **parser) const;
    QString getATSCChannelName(IUnknown *parser) const;
    QString getDVBChannelName(IUnknown *parser) const;
    QString getISDBChannelName(IUnknown *parser) const;

    QDateTime getATSCCurrentDateTime(IUnknown *parser) const;
    QDateTime getDVBCurrentDateTime(IUnknown *parser) const;
    QDateTime getISDBCurrentDateTime(IUnknown *parser) const;

    void getATSCEPG(IUnknown *parser, QVector<DTVReaderInterface::EPG> *ret) const;
    void getDVBEPG(IUnknown *parser, QVector<DTVReaderInterface::EPG> *ret) const;
    void getISDBEPG(IUnknown *parser, QVector<DTVReaderInterface::EPG> *ret) const;

    ModulationType getModulation(const QString &mod) const;
    BinaryConvolutionCodeRate getFEC(uint32_t fec) const;
    GuardInterval getGuard(uint32_t guard) const;
    TransmissionMode getTransmission(int transmission) const;
    HierarchyAlpha getHierarchy(int hierarchy) const;
    Polarisation getPolarization(char pol) const;

    IPin* findPinOnFilter(IBaseFilter *baseFilter, const char *expectedPinName) const;

private:
    STDMETHODIMP_(ULONG) AddRef();
    STDMETHODIMP_(ULONG) Release();
    STDMETHODIMP QueryInterface(REFIID riid, void **object);
    STDMETHODIMP SampleCB(double time, IMediaSample *sample);
    STDMETHODIMP BufferCB(double time, BYTE *buffer, long len);

private:
    struct TBSPLPInfo
    {
        unsigned char id;
        unsigned char ount;
        unsigned char resered1;
        unsigned char resered2;
        unsigned char idList[256];
    };

private:
    static const int MAX_QUEUE_SIZE;

private:
    ULONG m_cbrc;
    CLSID m_networkType;
    SpectralInversion m_inversion;
    long m_tunerUsed;
    unsigned int m_systems;
    QMutex m_dataLock;
    QWaitCondition m_dataCond;
    MemBuffer m_data;
    DWORD m_graphRegister;
    IMediaControl *m_mediaControl;
    IGraphBuilder *m_filterGraph;
    ITuningSpace *m_tuningSpace;
    ICreateDevEnum *m_systemDevEnum;
    IBaseFilter *m_networkProvider;
    IBaseFilter *m_tunerDevice;
    IBaseFilter *m_captureDevice;
    IBaseFilter *m_sampleGrabber;
    IBaseFilter *m_mpegDemux;
    IBaseFilter *m_transportInfo;
    IBaseFilter *m_mpeg2Data;
    IScanningTuner *m_scanningTuner;
    ISampleGrabber *m_grabber;
    IMpeg2Data *m_mpeg2DataParser;
};
