﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "AppleMediaKey.h"
#include "factories/MainWindowFactory.h"
#include "delegate/MediaPlayerDelegate.h"
#include "ui/MainWindowInterface.h"

#include <QtConcurrent>
#include <QDebug>

const int AppleMediaKey::SYSTEM_DEFINED_EVENT_MEDIA_KEYS = 8;

AppleMediaKey::AppleMediaKey() :
    m_eventPort(nil),
    m_eventPortSource(nil),
    m_tapThreadRunLoop(nil),
    m_mediaKeyAppList(nil),
    m_interceptMediaKeyEvents(false),
    m_mediaKeyJustJumped(false),
    m_appActivatedObj(nil),
    m_appTerminatedObj(nil)
{
    this->m_eventThreadPool.setMaxThreadCount(1);
}

AppleMediaKey::~AppleMediaKey()
{
    this->deInit();
}

bool AppleMediaKey::init()
{
    this->m_mediaKeyAppList = [NSMutableArray new];
    return this->start();
}

void AppleMediaKey::deInit()
{
    this->stop();

    if (this->m_mediaKeyAppList != nil)
    {
        [this->m_mediaKeyAppList autorelease];
        this->m_mediaKeyAppList = nil;
    }
}

bool AppleMediaKey::start()
{
    this->getMediaKeyUserBundleIdentifiers();
    this->startWatchingAppSwitching();

    return this->startWatchingMediaKeys();
}

void AppleMediaKey::stop()
{
    this->stopWatchingMediaKeys();
    this->stopWatchingAppSwitching();
}

void AppleMediaKey::startWatchingAppSwitching()
{
    NSNotificationCenter *center = [[NSWorkspace sharedWorkspace] notificationCenter];

    this->m_appActivatedObj = [center addObserverForName: NSWorkspaceDidActivateApplicationNotification
                                                  object: nil
                                                   queue: nil
                                              usingBlock: ^(NSNotification* notification) {
                                                              this->handleFrontMostAppChanged(notification.userInfo);
                                                          }
                              ];

    this->m_appTerminatedObj = [center addObserverForName: NSWorkspaceDidTerminateApplicationNotification
                                                   object: nil
                                                    queue: nil
                                               usingBlock: ^(NSNotification* notification) {
                                                               this->handleAppTerminated(notification.userInfo);
                                                           }
                               ];
}

void AppleMediaKey::stopWatchingAppSwitching() const
{
    NSNotificationCenter *center = [[NSWorkspace sharedWorkspace] notificationCenter];

    [center removeObserver: this->m_appActivatedObj];
    [center removeObserver: this->m_appTerminatedObj];
}

bool AppleMediaKey::startWatchingMediaKeys()
{
    this->stopWatchingMediaKeys();
    this->interceptMediaKeyEvents(true);

    this->m_eventPort = CGEventTapCreate(kCGSessionEventTap,
                                         kCGHeadInsertEventTap,
                                         kCGEventTapOptionDefault,
                                         CGEventMaskBit(NX_SYSDEFINED),
                                         AppleMediaKey::tapEventCallback,
                                         (__bridge void * __nullable)this);

    if (!this->m_eventPort)
        return false;

    this->m_eventPortSource = CFMachPortCreateRunLoopSource(kCFAllocatorSystemDefault, this->m_eventPort, 0);

    if (!this->m_eventPortSource)
        return false;

    QThread::start();
    return true;
}

void AppleMediaKey::stopWatchingMediaKeys()
{
    if (this->m_tapThreadRunLoop)
    {
        CFRunLoopStop(this->m_tapThreadRunLoop);
        this->m_tapThreadRunLoop = nil;
    }

    if (this->m_eventPort)
    {
        CFMachPortInvalidate(this->m_eventPort);
        CFRelease(this->m_eventPort);
        this->m_eventPort = nil;
    }

    if (this->m_eventPortSource)
    {
        CFRelease(this->m_eventPortSource);
        this->m_eventPortSource = nil;
    }

    this->wait();
}

void AppleMediaKey::interceptMediaKeyEvents(bool onoff)
{
    this->m_interceptMediaKeyEvents = onoff;

    if (this->m_tapThreadRunLoop && this->m_interceptMediaKeyEvents != onoff)
        QtConcurrent::run(&this->m_eventThreadPool, [this, onoff]() { CGEventTapEnable(this->m_eventPort, onoff); });
}

void AppleMediaKey::resetMediaKeyJumpLater()
{
    dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25f * NSEC_PER_SEC));
    dispatch_after(time, dispatch_get_main_queue(), ^{
        this->m_mediaKeyJustJumped = false;
    });
}

CGEventRef AppleMediaKey::processTapEvent(CGEventTapProxy proxy, CGEventType type, CGEventRef event)
{
    (void)proxy;

    switch (type)
    {
        case kCGEventTapDisabledByTimeout:
            CGEventTapEnable(this->m_eventPort, true);
            return event;
        case kCGEventTapDisabledByUserInput:
            return event;
        default:
            break;
    }

    NSEvent *nsEvent = [NSEvent eventWithCGEvent:event];

    if (type != NX_SYSDEFINED || [nsEvent subtype] != SYSTEM_DEFINED_EVENT_MEDIA_KEYS)
        return event;

    int keyCode = (([nsEvent data1] & 0xFFFF0000) >> 16);

    if (keyCode != NX_KEYTYPE_PLAY && keyCode != NX_KEYTYPE_FAST &&
        keyCode != NX_KEYTYPE_REWIND && keyCode != NX_KEYTYPE_PREVIOUS &&
        keyCode != NX_KEYTYPE_NEXT)
    {
        return event;
    }

    if (!this->m_interceptMediaKeyEvents)
        return event;

    dispatch_async(dispatch_get_main_queue(), ^{
        this->handleMediaKeyEvent(nsEvent);
    });

    return nullptr;
}

void AppleMediaKey::handleMediaKeyEvent(NSEvent *event)
{
    MainWindowInterface *window = MainWindowFactory::getInstance();
    MediaPlayerDelegate &player = MediaPlayerDelegate::getInstance();
    int keyCode = (([event data1] & 0xFFFF0000) >> 16);
    int keyFlags = ([event data1] & 0x0000FFFF);
    int keyState = (((keyFlags & 0xFF00) >> 8)) == 0xA;
    int keyRepeat = (keyFlags & 0x1);

    if (keyCode == NX_KEYTYPE_PLAY && keyState == 0)
    {
        window->toggle();
    }
    else
    {
        if (this->m_mediaKeyJustJumped)
            return;

        if (keyCode == NX_KEYTYPE_FAST || keyCode == NX_KEYTYPE_NEXT)
        {
            if (keyState == 0 && keyRepeat == 0)
            {
                window->next();
            }
            else if (keyRepeat == 1)
            {
                this->m_mediaKeyJustJumped = true;
                this->resetMediaKeyJumpLater();

                player.forward5();
            }
        }
        else if (keyCode == NX_KEYTYPE_REWIND || keyCode == NX_KEYTYPE_PREVIOUS)
        {
            if (keyState == 0 && keyRepeat == 0)
            {
                window->prev();
            }
            else if (keyRepeat == 1)
            {
                this->m_mediaKeyJustJumped = true;
                this->resetMediaKeyJumpLater();

                player.rewind5();
            }
        }
    }
}

void AppleMediaKey::handleMediaKeyAppListChanged()
{
    if ([this->m_mediaKeyAppList count] == 0)
        return;

    NSRunningApplication *thisApp = [NSRunningApplication currentApplication];
    NSRunningApplication *otherApp = [this->m_mediaKeyAppList firstObject];

    this->interceptMediaKeyEvents([thisApp isEqual: otherApp]);
}

NSArray* AppleMediaKey::getMediaKeyUserBundleIdentifiers() const
{
    static NSArray *bundleIdentifiers = nil;
    static dispatch_once_t onceToken;

    dispatch_once(&onceToken, ^{
        bundleIdentifiers = @[
            [[NSBundle mainBundle] bundleIdentifier],
            @"com.spotify.client",
            @"com.apple.iTunes",
            @"com.apple.Music",
            @"com.apple.QuickTimePlayerX",
            @"com.apple.quicktimeplayer",
            @"com.apple.iWork.Keynote",
            @"com.apple.iPhoto",
            @"org.videolan.vlc",
            @"com.apple.Aperture",
            @"com.plexsquared.Plex",
            @"com.soundcloud.desktop",
            @"org.niltsh.MPlayerX",
            @"com.ilabs.PandorasHelper",
            @"com.mahasoftware.pandabar",
            @"com.bitcartel.pandorajam",
            @"org.clementine-player.clementine",
            @"fm.last.Last.fm",
            @"fm.last.Scrobbler",
            @"com.beatport.BeatportPro",
            @"com.Timenut.SongKey",
            @"com.macromedia.fireworks",
            @"at.justp.Theremin",
            @"ru.ya.themblsha.YandexMusic",
            @"com.jriver.MediaCenter18",
            @"com.jriver.MediaCenter19",
            @"com.jriver.MediaCenter20",
            @"co.rackit.mate",
            @"com.ttitt.b-music",
            @"com.beardedspice.BeardedSpice",
            @"com.plug.Plug",
            @"com.netease.163music",
        ];
    });

    return bundleIdentifiers;
}

CGEventRef AppleMediaKey::tapEventCallback(CGEventTapProxy proxy, CGEventType type, CGEventRef event, void *refcon)
{
    @autoreleasepool
    {
        AppleMediaKey *self = (__bridge AppleMediaKey*)refcon;

        return self->processTapEvent(proxy, type, event);
    }
}

void AppleMediaKey::handleFrontMostAppChanged(NSDictionary *userInfo)
{
    NSRunningApplication *app = [userInfo objectForKey: NSWorkspaceApplicationKey];

    if (app.bundleIdentifier == nil)
        return;

    NSArray *bundleIdentifiers = this->getMediaKeyUserBundleIdentifiers();

    if (![bundleIdentifiers containsObject: app.bundleIdentifier])
        return;

    [this->m_mediaKeyAppList removeObject: app];
    [this->m_mediaKeyAppList insertObject: app atIndex: 0];
    this->handleMediaKeyAppListChanged();
}

void AppleMediaKey::handleAppTerminated(NSDictionary *userInfo)
{
    NSRunningApplication *app = [userInfo objectForKey: NSWorkspaceApplicationKey];

    [this->m_mediaKeyAppList removeObject: app];
    this->handleMediaKeyAppListChanged();
}

void AppleMediaKey::run()
{
    this->m_tapThreadRunLoop = CFRunLoopGetCurrent();

    CFRunLoopAddSource(this->m_tapThreadRunLoop, this->m_eventPortSource, kCFRunLoopCommonModes);
    CFRunLoopRun();
}
