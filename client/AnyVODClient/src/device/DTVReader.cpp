﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "DTVReader.h"
#include "core/Common.h"
#include "utils/SeparatingUtils.h"
#include "setting/DTVReaderSettingLoader.h"
#include "setting/DTVReaderSettingSaver.h"

#include "../../../../common/size.h"

#ifdef Q_OS_WIN
# include "DTVDShow.h"
#elif defined Q_OS_LINUX && !defined Q_OS_ANDROID
# include "DTVLinuxDVB.h"
#endif

#include <QUrl>
#include <QUrlQuery>
#include <QThread>
#include <QDataStream>
#include <QDebug>

extern "C"
{
# include <libavformat/avformat.h>
}

QDataStream& operator << (QDataStream &out, const DTVReader::ChannelInfo &item)
{
    out << (int)item.adapter;
    out << (int)item.type;
    out << (int)item.country;
    out << item.index;
    out << item.channel;
    out << item.signal;
    out << item.name;

    return out;
}

QDataStream& operator >> (QDataStream &in, DTVReader::ChannelInfo &item)
{
    int tmp;

    in >> tmp;
    item.adapter = tmp;

    in >> tmp;
    item.type = (DTVReaderInterface::SystemType)tmp;

    in >> tmp;
    item.country = (QLocale::Country)tmp;

    in >> item.index;
    in >> item.channel;
    in >> item.signal;
    in >> item.name;

    return in;
}

const char DTVReader::DTV_PROTOCOL_NAME[] = "dtv";

URLProtocol DTVReader::PROTOCOL = {
    .name = DTV_PROTOCOL_NAME,
    .url_open = DTVReader::open,
    .url_open2 = nullptr,
    .url_accept = nullptr,
    .url_handshake = nullptr,
    .url_read = DTVReader::read,
    .url_write = nullptr,
    .url_seek = nullptr,
    .url_close = DTVReader::close,
    .url_read_pause = nullptr,
    .url_read_seek = nullptr,
    .url_get_file_handle = nullptr,
    .url_get_multi_file_handle = nullptr,
    .url_get_short_seek = nullptr,
    .url_shutdown = nullptr,
    .priv_data_class = nullptr,
    .priv_data_size = 0,
    .flags = 0,
    .url_check = nullptr,
    .url_open_dir = nullptr,
    .url_read_dir = nullptr,
    .url_close_dir = nullptr,
    .url_delete = nullptr,
    .url_move = nullptr,
    .default_whitelist = nullptr,
};

const QString DTVReader::DTV_PROTOCOL = QString(DTV_PROTOCOL_NAME) + PROTOCOL_POSTFIX;

DTVReader::DTVReader() :
    m_reader(nullptr),
    m_isOpened(false)
{
#ifdef Q_OS_WIN
    this->m_reader = new DTVDShow;
#elif defined Q_OS_LINUX && !defined Q_OS_ANDROID
    this->m_reader = new DTVLinuxDVB;
#endif

    memset(&this->m_intCallback, 0, sizeof(this->m_intCallback));

    DTVReaderSettingLoader(this).load();
}

DTVReader::~DTVReader()
{
    DTVReaderSettingSaver(this).save();

    if (this->m_reader)
    {
        delete this->m_reader;
        this->m_reader = nullptr;
    }
}

DTVReader& DTVReader::getInstance()
{
    static DTVReader instance;

    return instance;
}

URLProtocol* DTVReader::getProtocol()
{
    return &DTVReader::PROTOCOL;
}

#if !defined Q_OS_ANDROID && !defined Q_OS_IOS
QString DTVReader::makeDTVPath(const DTVReader::ChannelInfo &info)
{
    QString url = QString("%1?type=%2&adapter=%3&index=%4&country=%5")
            .arg(DTV_PROTOCOL)
            .arg((int)info.type)
            .arg(info.adapter)
            .arg(info.index)
            .arg(info.country);

    return url;
}

bool DTVReader::determinDTV(const QString &path)
{
    return path.startsWith(DTV_PROTOCOL);
}
#endif

bool DTVReader::isSupport() const
{
    return this->m_reader != nullptr;
}

bool DTVReader::setChannelCountry(QLocale::Country country, DTVReaderInterface::SystemType type)
{
    return this->m_channelMap.setChannelCountry(country, type);
}

void DTVReader::getChannelCountry(QLocale::Country *country, DTVReaderInterface::SystemType *type) const
{
    this->m_channelMap.getChannelCountry(country, type);
}

void DTVReader::getChannels(QVector<DTVChannelMap::Channel> *ret) const
{
    this->m_channelMap.getChannels(ret);
}

bool DTVReader::isOpened() const
{
    return this->m_isOpened;
}

unsigned int DTVReader::enumSystems()
{
    return this->m_reader->enumSystems();
}

float DTVReader::getSignalStrength() const
{
    float signal = this->m_reader->getSignalStrength();

    if (signal > 100.0f)
        signal = 100.0f;

    return signal;
}

float DTVReader::getSignalNoiseRatio() const
{
    return this->m_reader->getSignalNoiseRatio();
}

QString DTVReader::getChannelName()
{
    return this->m_reader->getChannelName();
}

void DTVReader::getEPG(QVector<DTVReaderInterface::EPG> *ret)
{
    this->m_reader->getEPG(ret);

    if (ret->count() < 2)
        return;

    DTVReaderInterface::EPG prev = ret->first();

    for (int i = 1; i < ret->count(); i++)
    {
        DTVReaderInterface::EPG cur = ret->at(i);

        if (prev.title == cur.title && prev.start == cur.start)
        {
            ret->remove(i--);
        }
        else if (prev.title == cur.title)
        {
            prev.duration = prev.duration.addSecs(QTime(0, 0).secsTo(cur.duration));

            ret->remove(i);
            ret->replace(--i, prev);

            cur = ret->at(i);
        }

        prev = cur;
    }
}

QDateTime DTVReader::getCurrentDateTime()
{
    return this->m_reader->getCurrentDateTime();
}

void DTVReader::getAdapter(long *adapter, DTVReaderInterface::SystemType *type) const
{
    this->m_reader->getAdapter(adapter, type);
}

bool DTVReader::getAdapterInfo(long adapter, DTVReaderInterface::AdapterInfo *ret) const
{
    QVector<DTVReaderInterface::AdapterInfo> adapters;

    if (!this->getAdapterList(&adapters))
        return false;

    if (adapters.length() - 1 >= adapter)
    {
        *ret = adapters[(int)adapter];
        return true;
    }

    return false;
}

bool DTVReader::getAdapterList(QVector<DTVReaderInterface::AdapterInfo> *ret) const
{
    return this->m_reader->getAdapterList(ret);
}

bool DTVReader::scan(long adapter, QLocale::Country country, DTVReaderInterface::SystemType type,
                     const DTVChannelMap::Channel &channel, int index, ChannelInfo *ret)
{
    ret->adapter = adapter;
    ret->type = type;
    ret->channel = channel.channel;
    ret->index = index;

    this->m_reader->setAdapter(adapter, type);

    if (this->m_reader->open(QString()))
    {
        if (!this->tune(country, type, index))
        {
            this->m_reader->close();
            return false;
        }

        bool ok;

        ok = (ret->signal = this->getSignalStrength()) >= 45.0f;

        if (ok)
        {
            if (ret->signal > 100.0f)
                ret->signal = 100.0f;

            for (int i = 0; i < 3; i++)
            {
                ret->name = this->getChannelName();

                if (ret->name.isEmpty())
                    QThread::msleep(500);
                else
                    break;
            }
        }

        this->m_reader->close();
        this->m_reader->reset();

        return ok;
    }

    return false;
}

void DTVReader::setScannedChannels(const QVector<DTVReader::ChannelInfo> &list)
{
    this->m_scannedChannel = list;
}

void DTVReader::getScannedChannels(QVector<DTVReader::ChannelInfo> *ret) const
{
    *ret = this->m_scannedChannel;
}

void DTVReader::setAdapter(long adapter, DTVReaderInterface::SystemType type)
{
    this->m_reader->setAdapter(adapter, type);
}

bool DTVReader::tune(QLocale::Country country, DTVReaderInterface::SystemType type, int index)
{
    bool ok = false;
    DTVChannelMap::Channel channelInfo;

    if (!this->m_channelMap.setChannelCountry(country, type))
        return false;

    this->m_channelMap.getChannel(index, &channelInfo);

    switch (type)
    {
        case DTVReaderInterface::ST_ATSC_T:
        case DTVReaderInterface::ST_ATSC_C:
            ok = this->setDefaultATSC(channelInfo);
            break;
        case DTVReaderInterface::ST_CQAM:
            ok = this->setDefaultCQAM(channelInfo);
            break;
        case DTVReaderInterface::ST_DVB_C:
            ok = this->setDefaultDVBC(channelInfo);
            break;
        case DTVReaderInterface::ST_DVB_C2:
            ok = this->setDefaultDVBC2(channelInfo);
            break;
        case DTVReaderInterface::ST_DVB_S:
            ok = this->setDefaultDVBS(channelInfo);
            break;
        case DTVReaderInterface::ST_DVB_S2:
            ok = this->setDefaultDVBS2(channelInfo);
            break;
        case DTVReaderInterface::ST_DVB_T:
            ok = this->setDefaultDVBT(channelInfo);
            break;
        case DTVReaderInterface::ST_DVB_T2:
            ok = this->setDefaultDVBT2(channelInfo);
            break;
        case DTVReaderInterface::ST_ISDB_C:
            ok = this->setDefaultISDBC(channelInfo);
            break;
        case DTVReaderInterface::ST_ISDB_S:
            ok = this->setDefaultISDBS(channelInfo);
            break;
        case DTVReaderInterface::ST_ISDB_T:
            ok = this->setDefaultISDBT(channelInfo);
            break;
        default:
            return false;
    }

    if (!ok)
        return false;

    if (!this->setDefaultInversion())
        return false;

    return this->m_reader->tune();
}

bool DTVReader::setDefaultInversion()
{
    return this->m_reader->setInversion(DTVReaderInterface::IV_AUTO);
}

bool DTVReader::setDefaultDVBC(const DTVChannelMap::Channel &channelInfo)
{
    return this->m_reader->setDVBC((uint32_t)channelInfo.freq, channelInfo.mod, channelInfo.srate, channelInfo.hpFEC);
}

bool DTVReader::setDefaultDVBC2(const DTVChannelMap::Channel &channelInfo)
{
    (void)channelInfo;

    return false;
}

bool DTVReader::setDefaultDVBS(const DTVChannelMap::Channel &channelInfo)
{
    bool ok = this->m_reader->setDVBS(channelInfo.freq, channelInfo.srate, channelInfo.hpFEC);

    if (ok)
        this->m_reader->setSEC(channelInfo.freq, channelInfo.srate, channelInfo.hpFEC, channelInfo.pol,
                               channelInfo.lowf, channelInfo.highf, channelInfo.switchf, channelInfo.highv,
                               channelInfo.satNo, channelInfo.uncommitted);

    return ok;
}

bool DTVReader::setDefaultDVBS2(const DTVChannelMap::Channel &channelInfo)
{
    bool ok = this->m_reader->setDVBS2(channelInfo.freq, channelInfo.mod, channelInfo.srate,
                                       channelInfo.hpFEC, channelInfo.pilot, channelInfo.rolloff, channelInfo.sid);
    if (ok)
        this->m_reader->setSEC(channelInfo.freq, channelInfo.srate, channelInfo.hpFEC, channelInfo.pol,
                               channelInfo.lowf, channelInfo.highf, channelInfo.switchf, channelInfo.highv,
                               channelInfo.satNo, channelInfo.uncommitted);

    return ok;
}

bool DTVReader::setDefaultSEC(const DTVChannelMap::Channel &channelInfo)
{
    return this->m_reader->setSEC(channelInfo.freq, channelInfo.srate, channelInfo.hpFEC, channelInfo.pol,
                                  channelInfo.lowf, channelInfo.highf, channelInfo.switchf, channelInfo.highv,
                                  channelInfo.satNo, channelInfo.uncommitted);
}

bool DTVReader::setDefaultDVBT(const DTVChannelMap::Channel &channelInfo)
{
    return this->m_reader->setDVBT((uint32_t)channelInfo.freq, channelInfo.mod, channelInfo.hpFEC, channelInfo.lpFEC,
                                   channelInfo.bandwidth, channelInfo.transmission, channelInfo.guard, channelInfo.hierarchy);
}

bool DTVReader::setDefaultDVBT2(const DTVChannelMap::Channel &channelInfo)
{
    return this->m_reader->setDVBT2((uint32_t)channelInfo.freq, channelInfo.mod, channelInfo.hpFEC, channelInfo.bandwidth,
                                    channelInfo.transmission, channelInfo.guard, channelInfo.plp);
}

bool DTVReader::setDefaultATSC(const DTVChannelMap::Channel &channelInfo)
{
    return this->m_reader->setATSC((uint32_t)channelInfo.freq, channelInfo.mod);
}

bool DTVReader::setDefaultCQAM(const DTVChannelMap::Channel &channelInfo)
{
    return this->m_reader->setCQAM((uint32_t)channelInfo.freq, channelInfo.mod);
}

bool DTVReader::setDefaultISDBC(const DTVChannelMap::Channel &channelInfo)
{
    return this->m_reader->setISDBC((uint32_t)channelInfo.freq, channelInfo.mod, channelInfo.srate, channelInfo.hpFEC);
}

bool DTVReader::setDefaultISDBS(const DTVChannelMap::Channel &channelInfo)
{
    bool ok = this->m_reader->setISDBS(channelInfo.freq, channelInfo.tsID);

    if (ok)
        this->m_reader->setSEC(channelInfo.freq, channelInfo.srate, channelInfo.hpFEC, channelInfo.pol,
                               channelInfo.lowf, channelInfo.highf, channelInfo.switchf, channelInfo.highv,
                               channelInfo.satNo, channelInfo.uncommitted);

    return false;
}

bool DTVReader::setDefaultISDBT(const DTVChannelMap::Channel &channelInfo)
{
    return this->m_reader->setISDBT((uint32_t)channelInfo.freq, channelInfo.bandwidth, channelInfo.transmission, channelInfo.guard, channelInfo.layers);
}

int DTVReader::open(URLContext *h, const char *uri, int)
{
    QString realUri = QString::fromUtf8(uri);
    int index = realUri.lastIndexOf(SeparatingUtils::FFMPEG_SEPARATOR);

    if (index != -1)
    {
        DTVReader *me = nullptr;
        uint64_t id = 0;
        QString parentID;

        parentID = realUri.mid(index + 1);
        id = parentID.toULongLong();
        me = (DTVReader*)id;

        h->priv_data = me;

        realUri = realUri.mid(0, index);

        char proto[strlen(DTV_PROTOCOL_NAME) + 1];
        char path[MAX_FILEPATH_CHAR_SIZE];

        av_url_split(proto, (int)sizeof(proto), nullptr, 0, nullptr, 0, nullptr, path, (int)sizeof(path), realUri.toUtf8().constData());

        if (strcmp(proto, DTV_PROTOCOL_NAME))
            return AVERROR(EINVAL);

        QUrl url(realUri);
        QUrlQuery query(url.query());
        long adapter;
        DTVReaderInterface::SystemType type;

        adapter = query.queryItemValue("adapter").toInt();
        type = (DTVReaderInterface::SystemType)query.queryItemValue("type").toInt();

        me->setAdapter(adapter, type);

        if (me->m_reader->open(realUri))
        {
            QLocale::Country country;
            int index;

            country = (QLocale::Country)query.queryItemValue("country").toInt();
            index = query.queryItemValue("index").toInt();

            if (!me->tune(country, type, index))
            {
                me->m_reader->close();
                return AVERROR(EFAULT);
            }

            me->m_intCallback = h->interrupt_callback;
            me->m_isOpened = true;

            return 0;
        }
        else
        {
            return AVERROR(ENOENT);
        }
    }
    else
    {
        return AVERROR(EINVAL);
    }
}

int DTVReader::read(URLContext *h, uint8_t *buf, int size)
{
    DTVReader *me = (DTVReader*)h->priv_data;
    int index = 0;

    while (size > 0)
    {
        int ret;

        if (me->m_intCallback.callback && me->m_intCallback.callback(me->m_intCallback.opaque))
            return AVERROR_EXIT;

        ret = me->m_reader->read(buf + index, size);

        index += ret;
        size -= ret;
    }

     return index;
}

int DTVReader::close(URLContext *h)
{
    DTVReader *me = (DTVReader*)h->priv_data;

    me->m_reader->close();
    me->m_reader->reset();
    me->m_isOpened = false;

    return 0;
}

QString DTVReader::systemTypeToString(DTVReaderInterface::SystemType type) const
{
    QString name;

    switch (type)
    {
        case DTVReaderInterface::ST_ATSC_C:
            name = "ATSC_C";
            break;
        case DTVReaderInterface::ST_ATSC_T:
            name = "ATSC_T";
            break;
        case DTVReaderInterface::ST_CQAM:
            name = "CQAM";
            break;
        case DTVReaderInterface::ST_DVB_C:
            name = "DVB_C";
            break;
        case DTVReaderInterface::ST_DVB_C2:
            name = "DVB_C2";
            break;
        case DTVReaderInterface::ST_DVB_S:
            name = "DVB_S";
            break;
        case DTVReaderInterface::ST_DVB_S2:
            name = "DVB_S2";
            break;
        case DTVReaderInterface::ST_DVB_T:
            name = "DVB_T";
            break;
        case DTVReaderInterface::ST_DVB_T2:
            name = "DVB_T2";
            break;
        case DTVReaderInterface::ST_ISDB_C:
            name = "ISDB_C";
            break;
        case DTVReaderInterface::ST_ISDB_S:
            name = "ISDB_S";
            break;
        case DTVReaderInterface::ST_ISDB_T:
            name = "ISDB_T";
            break;
        default:
            break;
    }

    return name;
}
