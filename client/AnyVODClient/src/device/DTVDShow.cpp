﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "DTVDShow.h"
#include "core/Common.h"
#include "utils/COMUtils.h"

#include <QUrl>
#include <QUrlQuery>
#include <QVector>
#include <QDeadlineTimer>
#include <QCoreApplication>

const int DTVDShow::MAX_QUEUE_SIZE = 1024 * 1024 * 2;

DTVDShow::DTVDShow() :
    m_cbrc(0),
    m_networkType(GUID_NULL),
    m_inversion(BDA_SPECTRAL_INVERSION_NOT_SET),
    m_tunerUsed(-1),
    m_systems(0),
    m_data(MAX_QUEUE_SIZE),
    m_graphRegister(0),
    m_mediaControl(nullptr),
    m_filterGraph(nullptr),
    m_tuningSpace(nullptr),
    m_systemDevEnum(nullptr),
    m_networkProvider(nullptr),
    m_tunerDevice(nullptr),
    m_captureDevice(nullptr),
    m_sampleGrabber(nullptr),
    m_mpegDemux(nullptr),
    m_transportInfo(nullptr),
    m_mpeg2Data(nullptr),
    m_scanningTuner(nullptr),
    m_grabber(nullptr),
    m_mpeg2DataParser(nullptr)
{
    COMUtils::initCOM();
}

DTVDShow::~DTVDShow()
{
    COMUtils::unInitCOM();
}

bool DTVDShow::open(const QString &path)
{
    (void)path;

    return true;
}

void DTVDShow::close()
{
    this->destroy();

    if (this->m_tuningSpace)
        this->m_tuningSpace->Release();

    this->m_tuningSpace = nullptr;
    this->m_systems = 0;
    this->m_inversion = BDA_SPECTRAL_INVERSION_NOT_SET;
}

int DTVDShow::read(uint8_t *buf, int size)
{
    this->m_dataLock.lock();

    while (this->m_data.IsEmpty())
    {
        if (!this->m_dataCond.wait(&this->m_dataLock, QDeadlineTimer(250)))
        {
            this->m_dataLock.unlock();
            return 0;
        }
    }

    int read = this->m_data.Read((char*)buf, size);

    this->m_dataLock.unlock();

    return read;
}

unsigned int DTVDShow::enumSystems()
{
    GUID networkProvider = GUID_NULL;
    bool ok = false;

    do
    {
        if (!this->getNextNetworkType(&networkProvider))
            break;

        ok |= this->check(networkProvider);

    }
    while (true);

    if (this->m_filterGraph)
        this->destroy();

    return this->m_systems;
}

float DTVDShow::getSignalStrength() const
{
    HRESULT hr;
    long strength = 0;

    if (!this->m_scanningTuner)
        return 0.0f;

    hr = this->m_scanningTuner->get_SignalStrength(&strength);

    if (FAILED(hr))
        return 0.0f;

    if (strength == -1)
        return 0.0f;

    return strength / 65535.0f;
}

float DTVDShow::getSignalNoiseRatio() const
{
    return 0.0f;
}

QString DTVDShow::getChannelName()
{
    class localComPtr
    {
    public:
        localComPtr() :
            m_parser(nullptr)
        {

        }

        ~localComPtr()
        {
            if (this->m_parser)
                this->m_parser->Release();
        }

        IUnknown *m_parser;
    }l;

    QString name;

    if (!this->m_mpeg2DataParser)
        return name;

    if (!this->getDTVParser(&l.m_parser))
        return name;

    switch (this->m_type)
    {
        case ST_ATSC_C:
        case ST_ATSC_T:
        case ST_CQAM:
            name = this->getATSCChannelName(l.m_parser);
            break;
        case ST_DVB_C:
        case ST_DVB_C2:
        case ST_DVB_S:
        case ST_DVB_S2:
        case ST_DVB_T:
        case ST_DVB_T2:
            name = this->getDVBChannelName(l.m_parser);
            break;
        case ST_ISDB_C:
        case ST_ISDB_T:
        case ST_ISDB_S:
            name = this->getISDBChannelName(l.m_parser);
            break;
        default:
            break;
    }

    return name;
}

void DTVDShow::getEPG(QVector<DTVReaderInterface::EPG> *ret)
{
    class localComPtr
    {
    public:
        localComPtr() :
            m_parser(nullptr)
        {

        }

        ~localComPtr()
        {
            if (this->m_parser)
                this->m_parser->Release();
        }

        IUnknown *m_parser;
    }l;

    if (!this->m_mpeg2DataParser)
        return;

    if (!this->getDTVParser(&l.m_parser))
        return;

    switch (this->m_type)
    {
        case ST_ATSC_C:
        case ST_ATSC_T:
        case ST_CQAM:
            this->getATSCEPG(l.m_parser, ret);
            break;
        case ST_DVB_C:
        case ST_DVB_C2:
        case ST_DVB_S:
        case ST_DVB_S2:
        case ST_DVB_T:
        case ST_DVB_T2:
            this->getDVBEPG(l.m_parser, ret);
            break;
        case ST_ISDB_C:
        case ST_ISDB_T:
        case ST_ISDB_S:
            this->getISDBEPG(l.m_parser, ret);
            break;
        default:
            break;
    }
}

QDateTime DTVDShow::getCurrentDateTime()
{
    class localComPtr
    {
    public:
        localComPtr() :
            m_parser(nullptr)
        {

        }

        ~localComPtr()
        {
            if (this->m_parser)
                this->m_parser->Release();
        }

        IUnknown *m_parser;
    }l;

    QDateTime time;

    if (!this->m_mpeg2DataParser)
        return time;

    if (!this->getDTVParser(&l.m_parser))
        return time;

    switch (this->m_type)
    {
        case ST_ATSC_C:
        case ST_ATSC_T:
        case ST_CQAM:
            time = this->getATSCCurrentDateTime(l.m_parser);
            break;
        case ST_DVB_C:
        case ST_DVB_C2:
        case ST_DVB_S:
        case ST_DVB_S2:
        case ST_DVB_T:
        case ST_DVB_T2:
            time = this->getDVBCurrentDateTime(l.m_parser);
            break;
        case ST_ISDB_C:
        case ST_ISDB_T:
        case ST_ISDB_S:
            time = this->getISDBCurrentDateTime(l.m_parser);
            break;
        default:
            break;
    }

    return time;
}

bool DTVDShow::tune()
{
    return this->submitTuneRequest();
}

bool DTVDShow::getAdapterList(QVector<DTVReaderInterface::AdapterInfo> *ret) const
{
    return this->listAdapters(KSCATEGORY_BDA_NETWORK_TUNER, ret);
}

bool DTVDShow::setInversion(DTVReaderInterface::Inversion inversion)
{
    class localComPtr
    {
    public:
        localComPtr() :
            m_dvbsTuningSpace(nullptr)
        {

        }

        ~localComPtr()
        {
            if (this->m_dvbsTuningSpace)
                this->m_dvbsTuningSpace->Release();
        }

        IDVBSTuningSpace *m_dvbsTuningSpace;
    }l;

    HRESULT hr;

    if (!(this->m_type == DTVReaderInterface::ST_DVB_S ||
            this->m_type == DTVReaderInterface::ST_DVB_S2 ||
            this->m_type == DTVReaderInterface::ST_ISDB_S))
        return true;

    switch (inversion)
    {
        case IV_NORMAL:
            this->m_inversion = BDA_SPECTRAL_INVERSION_NORMAL;
            break;
        case IV_INVERTED:
            this->m_inversion = BDA_SPECTRAL_INVERSION_INVERTED;
            break;
        case IV_AUTO:
            this->m_inversion = BDA_SPECTRAL_INVERSION_AUTOMATIC;
            break;
        default:
            this->m_inversion = BDA_SPECTRAL_INVERSION_NOT_DEFINED;
            break;
    }

    if (!this->m_scanningTuner)
        return false;

    hr = this->m_scanningTuner->get_TuningSpace(&this->m_tuningSpace);

    if (FAILED(hr))
        return false;

    hr = this->m_tuningSpace->QueryInterface(IID_IDVBSTuningSpace, (void**)&l.m_dvbsTuningSpace);

    if (FAILED(hr))
        return false;

    hr = l.m_dvbsTuningSpace->put_SpectralInversion(this->m_inversion);

    if (FAILED(hr))
        return false;

    return true;
}

bool DTVDShow::setDVBC(uint32_t freq, const QString &mod, uint32_t srate, uint32_t fec)
{
    (void)fec;

    class localComPtr
    {
    public:
        localComPtr():
            m_tuneRequest(nullptr),
            m_dvbTuneRequest(nullptr),
            m_dvbcLocator(nullptr),
            m_dvbTuningSpace(nullptr)
        {

        }

        ~localComPtr()
        {
            if (this->m_tuneRequest)
                this->m_tuneRequest->Release();

            if (this->m_dvbTuneRequest)
                this->m_dvbTuneRequest->Release();

            if (this->m_dvbcLocator)
                this->m_dvbcLocator->Release();

            if (this->m_dvbTuningSpace)
                this->m_dvbTuningSpace->Release();
        }

        ITuneRequest *m_tuneRequest;
        IDVBTuneRequest *m_dvbTuneRequest;
        IDVBCLocator *m_dvbcLocator;
        IDVBTuningSpace2 *m_dvbTuningSpace;
    }l;

    HRESULT hr ;
    ModulationType qamMod = this->getModulation(mod);

    hr = this->check(CLSID_DVBCNetworkProvider);

    if (FAILED(hr))
        return false;

    if(!this->m_scanningTuner)
        return false;

    hr = this->m_scanningTuner->get_TuneRequest(&l.m_tuneRequest);

    if (FAILED(hr))
        return false;

    hr = l.m_tuneRequest->QueryInterface(IID_IDVBTuneRequest, (void**)&l.m_dvbTuneRequest);

    if (FAILED(hr))
        return false;

    l.m_dvbTuneRequest->put_ONID(-1);
    l.m_dvbTuneRequest->put_SID(-1);
    l.m_dvbTuneRequest->put_TSID(-1);

    hr = CoCreateInstance(CLSID_DVBCLocator, nullptr, CLSCTX_INPROC, IID_IDVBCLocator, (void**)&l.m_dvbcLocator);

    if (FAILED(hr))
        return false;

    hr = this->m_scanningTuner->get_TuningSpace(&this->m_tuningSpace);

    if (FAILED(hr))
        return false;

    hr = this->m_tuningSpace->QueryInterface(IID_IDVBTuningSpace2, (void**)&l.m_dvbTuningSpace);

    if (FAILED(hr))
        return false;

    hr = l.m_dvbTuningSpace->put_SystemType(DVB_Cable);

    if (SUCCEEDED(hr))
        hr = l.m_dvbcLocator->put_CarrierFrequency(freq / 1000);

    if (SUCCEEDED(hr) && srate > 0)
        hr = l.m_dvbcLocator->put_SymbolRate(srate);

    if (SUCCEEDED(hr) && qamMod != BDA_MOD_NOT_SET)
        hr = l.m_dvbcLocator->put_Modulation(qamMod);

    if (FAILED(hr))
        return false;

    hr = l.m_dvbTuneRequest->put_Locator(l.m_dvbcLocator);

    if (FAILED(hr))
        return false;

    hr = this->m_scanningTuner->Validate(l.m_dvbTuneRequest);

    if (FAILED(hr))
        return false;

    hr = this->m_scanningTuner->put_TuneRequest(l.m_dvbTuneRequest);

    if (FAILED(hr))
        return false;

    return true;
}

bool DTVDShow::setDVBS(uint64_t freq, uint32_t srate, uint32_t fec)
{
    return this->setSEC(freq, srate, fec, ' ', 0, 0, 0, 0, 0, 0);
}

bool DTVDShow::setDVBS2(uint64_t freq, const QString &mod, uint32_t srate, uint32_t fec, int pilot, int rolloff, uint8_t sid)
{
    (void)freq;
    (void)mod;
    (void)srate;
    (void)fec;
    (void)pilot;
    (void)rolloff;
    (void)sid;

    return false;
}

bool DTVDShow::setSEC(uint64_t freq, uint32_t srate, uint32_t fec, char pol, uint32_t lowf, uint32_t highf, uint32_t switchf,
                      int32_t highv, uint32_t satNo, uint32_t uncommitted)
{
    (void)highv;
    (void)satNo;
    (void)uncommitted;

    class localComPtr
    {
    public:
        localComPtr() :
            m_tuneRequest(nullptr),
            m_dvbTuneRequest(nullptr),
            m_dvbsLocator(nullptr),
            m_dvbsTuningSpace(nullptr)
        {

        }

        ~localComPtr()
        {
            if (this->m_tuneRequest)
                this->m_tuneRequest->Release();

            if (this->m_dvbTuneRequest)
                this->m_dvbTuneRequest->Release();

            if (this->m_dvbsLocator)
                this->m_dvbsLocator->Release();

            if (this->m_dvbsTuningSpace)
                this->m_dvbsTuningSpace->Release();
        }

        ITuneRequest *m_tuneRequest;
        IDVBTuneRequest *m_dvbTuneRequest;
        IDVBSLocator *m_dvbsLocator;
        IDVBSTuningSpace *m_dvbsTuningSpace;
    }l;

    HRESULT hr;
    BinaryConvolutionCodeRate hpFEC = this->getFEC(fec);
    Polarisation polar = this->getPolarization(pol);
    SpectralInversion inversion = this->m_inversion;

    hr = this->check(CLSID_DVBSNetworkProvider);

    if (FAILED(hr))
        return false;

    if (!this->m_scanningTuner)
        return false;

    hr = this->m_scanningTuner->get_TuneRequest(&l.m_tuneRequest);

    if (FAILED(hr))
        return false;

    hr = l.m_tuneRequest->QueryInterface(IID_IDVBTuneRequest, (void**)&l.m_dvbTuneRequest);

    if (FAILED(hr))
        return false;

    l.m_dvbTuneRequest->put_ONID(-1);
    l.m_dvbTuneRequest->put_SID(-1);
    l.m_dvbTuneRequest->put_TSID(-1);

    hr = CoCreateInstance(CLSID_DVBSLocator, nullptr, CLSCTX_INPROC, IID_IDVBSLocator, (void**)&l.m_dvbsLocator);

    if (FAILED(hr))
        return false;

    hr = this->m_scanningTuner->get_TuningSpace(&this->m_tuningSpace);

    if (FAILED(hr))
        return false;

    hr = this->m_tuningSpace->QueryInterface(IID_IDVBSTuningSpace, (void**)&l.m_dvbsTuningSpace);

    if (FAILED(hr))
        return false;

    hr = l.m_dvbsTuningSpace->put_SystemType(DVB_Satellite);

    if (SUCCEEDED(hr) && lowf > 0)
        hr = l.m_dvbsTuningSpace->put_LowOscillator(lowf);

    if (SUCCEEDED(hr) && switchf > 0)
        hr = l.m_dvbsTuningSpace->put_LNBSwitch(switchf);

    if (SUCCEEDED(hr) && highf > 0)
        hr = l.m_dvbsTuningSpace->put_HighOscillator(highf);

    if (SUCCEEDED(hr) && inversion != BDA_SPECTRAL_INVERSION_NOT_SET)
        hr = l.m_dvbsTuningSpace->put_SpectralInversion(inversion);

    if (SUCCEEDED(hr))
        hr = l.m_dvbsLocator->put_CarrierFrequency(freq / 1000);

    if (SUCCEEDED(hr) && srate > 0)
        hr = l.m_dvbsLocator->put_SymbolRate(srate);

    if (SUCCEEDED(hr) && polar != BDA_POLARISATION_NOT_SET)
        hr = l.m_dvbsLocator->put_SignalPolarisation(polar);

    if (SUCCEEDED(hr))
        hr = l.m_dvbsLocator->put_Modulation(BDA_MOD_QPSK);

    if (SUCCEEDED(hr) && hpFEC != BDA_BCC_RATE_NOT_SET)
        hr = l.m_dvbsLocator->put_InnerFECRate(hpFEC);

    if (FAILED(hr))
        return false;

    hr = l.m_dvbTuneRequest->put_Locator(l.m_dvbsLocator);

    if (FAILED(hr))
        return false;

    hr = this->m_scanningTuner->Validate(l.m_dvbTuneRequest);

    if (FAILED(hr))
        return false;

    hr = this->m_scanningTuner->put_TuneRequest(l.m_dvbTuneRequest);

    if (FAILED(hr))
        return false;

    return true;
}

bool DTVDShow::setDVBT(uint32_t freq, const QString &mod, uint32_t fecHP, uint32_t fecLP, uint32_t bandwidth, int transmission, uint32_t guard, int hierarchy)
{
    (void)mod;

    class localComPtr
    {
    public:
        localComPtr():
            m_tuneRequest(nullptr),
            m_dvbTuneRequest(nullptr),
            m_dvbtLocator(nullptr),
            m_dvbTuningSpace(nullptr)
        {

        }

        ~localComPtr()
        {
            if (this->m_tuneRequest)
                this->m_tuneRequest->Release();

            if (this->m_dvbTuneRequest)
                this->m_dvbTuneRequest->Release();

            if (this->m_dvbtLocator)
                this->m_dvbtLocator->Release();

            if (this->m_dvbTuningSpace)
                this->m_dvbTuningSpace->Release();
        }

        ITuneRequest *m_tuneRequest;
        IDVBTuneRequest *m_dvbTuneRequest;
        IDVBTLocator *m_dvbtLocator;
        IDVBTuningSpace2 *m_dvbTuningSpace;
    }l;

    HRESULT hr;
    BinaryConvolutionCodeRate hpFEC = this->getFEC(fecHP);
    BinaryConvolutionCodeRate lpFEC = this->getFEC(fecLP);
    GuardInterval guardInterval = this->getGuard(guard);
    TransmissionMode transmit = this->getTransmission(transmission);
    HierarchyAlpha hierarchyAlpha = this->getHierarchy(hierarchy);

    hr = this->check(CLSID_DVBTNetworkProvider);

    if (FAILED(hr))
        return false;

    if (!this->m_scanningTuner)
        return false;

    hr = this->m_scanningTuner->get_TuneRequest(&l.m_tuneRequest);

    if (FAILED(hr))
        return false;

    hr = l.m_tuneRequest->QueryInterface(IID_IDVBTuneRequest, (void**)&l.m_dvbTuneRequest);

    if (FAILED(hr))
        return false;

    l.m_dvbTuneRequest->put_ONID(-1);
    l.m_dvbTuneRequest->put_SID(-1);
    l.m_dvbTuneRequest->put_TSID(-1);

    hr = this->m_scanningTuner->get_TuningSpace(&this->m_tuningSpace);

    if (FAILED(hr))
        return false;

    hr = this->m_tuningSpace->QueryInterface(IID_IDVBTuningSpace2, (void**)&l.m_dvbTuningSpace);

    if (FAILED(hr))
        return false;

    hr = CoCreateInstance(CLSID_DVBTLocator, nullptr, CLSCTX_INPROC, IID_IDVBTLocator, (void**)&l.m_dvbtLocator);

    if (FAILED(hr))
        return false;

    hr = l.m_dvbTuningSpace->put_SystemType(DVB_Terrestrial);

    if (SUCCEEDED(hr))
        hr = l.m_dvbtLocator->put_CarrierFrequency(freq / 1000);

    if (SUCCEEDED(hr) && bandwidth > 0)
        hr = l.m_dvbtLocator->put_Bandwidth(bandwidth);

    if (SUCCEEDED(hr) && hpFEC != BDA_BCC_RATE_NOT_SET)
        hr = l.m_dvbtLocator->put_InnerFECRate(hpFEC);

    if (SUCCEEDED(hr) && lpFEC != BDA_BCC_RATE_NOT_SET)
        hr = l.m_dvbtLocator->put_LPInnerFECRate(lpFEC);

    if (SUCCEEDED(hr) && guardInterval != BDA_GUARD_NOT_SET)
        hr = l.m_dvbtLocator->put_Guard(guardInterval);

    if (SUCCEEDED(hr) && transmit != BDA_XMIT_MODE_NOT_SET)
        hr = l.m_dvbtLocator->put_Mode(transmit);

    if (SUCCEEDED(hr) && hierarchyAlpha != BDA_HALPHA_NOT_SET)
        hr = l.m_dvbtLocator->put_HAlpha(hierarchyAlpha);

    if (FAILED(hr))
        return false;

    hr = l.m_dvbTuneRequest->put_Locator(l.m_dvbtLocator);

    if (FAILED(hr))
        return false;

    hr = this->m_scanningTuner->Validate(l.m_dvbTuneRequest);

    if (FAILED(hr))
        return false;

    hr = this->m_scanningTuner->put_TuneRequest(l.m_dvbTuneRequest);

    if (FAILED(hr))
        return false;

    return true;
}

bool DTVDShow::setDVBT2(uint32_t freq, const QString &mod, uint32_t fec, uint32_t bandwidth, int transmission, uint32_t guard, uint8_t plp)
{
    (void)mod;

    class localComPtr
    {
    public:
        localComPtr():
            m_tuneRequest(nullptr),
            m_dvbTuneRequest(nullptr),
            m_dvbtLocator(nullptr),
            m_dvbTuningSpace(nullptr)
        {

        }

        ~localComPtr()
        {
            if (this->m_tuneRequest)
                this->m_tuneRequest->Release();

            if (this->m_dvbTuneRequest)
                this->m_dvbTuneRequest->Release();

            if (this->m_dvbtLocator)
                this->m_dvbtLocator->Release();

            if (this->m_dvbTuningSpace)
                this->m_dvbTuningSpace->Release();
        }

        ITuneRequest* m_tuneRequest;
        IDVBTuneRequest* m_dvbTuneRequest;
        IDVBTLocator2* m_dvbtLocator;
        IDVBTuningSpace2* m_dvbTuningSpace;
    }l;

    HRESULT hr;
    BinaryConvolutionCodeRate codeRate = this->getFEC(fec);
    GuardInterval guardInterval = this->getGuard(guard);
    TransmissionMode transmit = this->getTransmission(transmission);

    hr = this->check(CLSID_DVBTNetworkProvider);

    if (FAILED(hr))
        return false;

    if (!this->m_scanningTuner)
        return false;

    hr = this->m_scanningTuner->get_TuneRequest(&l.m_tuneRequest);

    if (FAILED(hr))
        return false;

    hr = l.m_tuneRequest->QueryInterface(IID_IDVBTuneRequest, (void**)&l.m_dvbTuneRequest);

    if (FAILED(hr))
        return false;

    l.m_dvbTuneRequest->put_ONID(-1);
    l.m_dvbTuneRequest->put_SID(-1);
    l.m_dvbTuneRequest->put_TSID(-1);

    hr = this->m_scanningTuner->get_TuningSpace(&this->m_tuningSpace);

    if (FAILED(hr))
        return false;

    hr = this->m_tuningSpace->QueryInterface(IID_IDVBTuningSpace2, (void**)&l.m_dvbTuningSpace);

    if (FAILED(hr))
        return false;

    hr = ::CoCreateInstance(CLSID_DVBTLocator2, 0, CLSCTX_INPROC, IID_IDVBTLocator2, (void**)&l.m_dvbtLocator);

    if (FAILED(hr))
        return false;

    hr = l.m_dvbTuningSpace->put_SystemType(DVB_Terrestrial);

    if (SUCCEEDED(hr) && freq > 0)
        hr = l.m_dvbtLocator->put_CarrierFrequency(freq);

    if (SUCCEEDED(hr) && bandwidth > 0)
        hr = l.m_dvbtLocator->put_Bandwidth(bandwidth);

    if (SUCCEEDED(hr) && codeRate != BDA_BCC_RATE_NOT_SET)
        hr = l.m_dvbtLocator->put_InnerFECRate(codeRate);

    if (SUCCEEDED(hr) && codeRate != BDA_BCC_RATE_NOT_SET)
        hr = l.m_dvbtLocator->put_LPInnerFECRate(codeRate);

    if (SUCCEEDED(hr) && guardInterval != BDA_GUARD_NOT_SET)
        hr = l.m_dvbtLocator->put_Guard(guardInterval);

    if (SUCCEEDED(hr) && transmit != BDA_XMIT_MODE_NOT_SET)
        hr = l.m_dvbtLocator->put_Mode(transmit);

    if (SUCCEEDED(hr) && plp > 0)
        hr = l.m_dvbtLocator->put_PhysicalLayerPipeId(plp);

    if (FAILED(hr))
        return false;

    hr = l.m_dvbTuneRequest->put_Locator(l.m_dvbtLocator);

    if (FAILED(hr))
        return false;

    hr = this->m_scanningTuner->Validate(l.m_dvbTuneRequest);

    if (FAILED(hr))
        return false;

    hr = this->m_scanningTuner->put_TuneRequest(l.m_dvbTuneRequest);

    if (FAILED(hr))
        return false;

    IPin *pinInput0 = this->findPinOnFilter(this->m_tunerDevice, "Input0");

    if (pinInput0)
    {
        IKsPropertySet* ksPropertySet;

        hr = pinInput0->QueryInterface(IID_IKsPropertySet, (void**)&ksPropertySet);

        if (SUCCEEDED(hr))
        {
            TBSPLPInfo plpInfo;

            ZeroMemory(&plpInfo, sizeof(plpInfo));
            plpInfo.id = plp;

            ksPropertySet->Set(KSPROPSETID_BdaTunerExtensionProperties, KSPROPERTY_BDA_PLPINFO, nullptr, 0, &plpInfo, sizeof(plpInfo));
            ksPropertySet->Release();
        }

        pinInput0->Release();
    }

    return true;
}

bool DTVDShow::setATSC(uint32_t freq, const QString &mod)
{
    (void)mod;

    class localComPtr
    {
    public:
        localComPtr():
            m_tuneRequest(nullptr),
            m_atscTuneRequest(nullptr),
            m_atscLocator(nullptr)
        {

        }

        ~localComPtr()
        {
            if (this->m_tuneRequest)
                this->m_tuneRequest->Release();

            if (this->m_atscTuneRequest)
                this->m_atscTuneRequest->Release();

            if (this->m_atscLocator)
                this->m_atscLocator->Release();
        }

        ITuneRequest *m_tuneRequest;
        IATSCChannelTuneRequest *m_atscTuneRequest;
        IATSCLocator *m_atscLocator;
    }l;

    HRESULT hr;

    hr = this->check(CLSID_ATSCNetworkProvider);

    if (FAILED(hr))
        return false;

    if (this->m_scanningTuner == nullptr)
        return false;

    hr = this->m_scanningTuner->get_TuneRequest(&l.m_tuneRequest);

    if (FAILED(hr))
        return false;

    hr = l.m_tuneRequest->QueryInterface(IID_IATSCChannelTuneRequest, (void**)&l.m_atscTuneRequest);

    if (FAILED(hr))
        return false;

    hr = CoCreateInstance(CLSID_ATSCLocator, nullptr, CLSCTX_INPROC, IID_IATSCLocator, (void**)&l.m_atscLocator);

    if (FAILED(hr))
        return false;

    hr = l.m_atscLocator->put_CarrierFrequency(freq / 1000);

    if (FAILED(hr))
        return false;

    hr = l.m_atscTuneRequest->put_Locator(l.m_atscLocator);

    if (FAILED(hr))
        return false;

    hr = this->m_scanningTuner->Validate(l.m_atscTuneRequest);

    if (FAILED(hr))
        return false;

    hr = this->m_scanningTuner->put_TuneRequest(l.m_atscTuneRequest);

    if (FAILED(hr))
        return false;

    return true;
}

bool DTVDShow::setCQAM(uint32_t freq, const QString &mod)
{
    (void)mod;

    class localComPtr
    {
    public:
        localComPtr():
            m_tuneRequest(nullptr),
            m_cqamTuneRequest(nullptr),
            m_cqamLocator(nullptr)
        {

        }

        ~localComPtr()
        {
            if (this->m_tuneRequest)
                this->m_tuneRequest->Release();

            if (this->m_cqamTuneRequest)
                this->m_cqamTuneRequest->Release();

            if (this->m_cqamLocator)
                this->m_cqamLocator->Release();
        }

        ITuneRequest *m_tuneRequest;
        IDigitalCableTuneRequest *m_cqamTuneRequest;
        IDigitalCableLocator *m_cqamLocator;
    }l;

    HRESULT hr;

    hr = this->check(CLSID_DigitalCableNetworkType);

    if (FAILED(hr))
        return false;

    if (!this->m_scanningTuner)
        return false;

    hr = this->m_scanningTuner->get_TuneRequest(&l.m_tuneRequest);

    if (FAILED(hr))
        return false;

    hr = l.m_tuneRequest->QueryInterface(IID_IDigitalCableTuneRequest, (void**)&l.m_cqamTuneRequest);

    if (FAILED(hr))
        return false;

    hr = CoCreateInstance(CLSID_DigitalCableLocator, nullptr, CLSCTX_INPROC, IID_IDigitalCableLocator, (void**)&l.m_cqamLocator);

    if (FAILED(hr))
        return false;

    hr = l.m_cqamLocator->put_CarrierFrequency(freq / 1000);

    if (FAILED(hr))
        return false;

    hr = l.m_cqamTuneRequest->put_Locator(l.m_cqamLocator);

    if (FAILED(hr))
        return false;

    hr = this->m_scanningTuner->Validate(l.m_cqamTuneRequest);

    if (FAILED(hr))
        return false;

    hr = this->m_scanningTuner->put_TuneRequest(l.m_cqamTuneRequest);

    if (FAILED(hr))
        return false;

    return true;
}

bool DTVDShow::setISDBC(uint32_t freq, const QString &mod, uint32_t srate, uint32_t fec)
{
    (void)freq;
    (void)mod;
    (void)fec;
    (void)srate;

    return false;
}

bool DTVDShow::setISDBS(uint64_t freq, uint16_t tsID)
{
    (void)freq;
    (void)tsID;

    return false;
}

bool DTVDShow::setISDBT(uint32_t freq, uint32_t bandwidth, int transmission, uint32_t guard,
                        const DTVReaderInterface::ISDBTLayer layers[3])
{
    (void)freq;
    (void)bandwidth;
    (void)transmission;
    (void)guard;
    (void)layers;

    return false;
}

bool DTVDShow::submitTuneRequest()
{
    bool ok;
    int i = 0;

    do
    {
        ok = this->build();

        if (!ok)
            return false;

        ok = this->start();

        if (!ok)
            ++i;
    }
    while (!ok && i < 10);

    return ok;
}

bool DTVDShow::setUpTuner(const IID &networkType)
{
    class localComPtr
    {
    public:
        localComPtr():
            m_tuningSpaceEnum(nullptr),
            m_testTuningSpace(nullptr),
            m_tuneRequest(nullptr),
            m_locator(nullptr),
            m_testNetworkType(GUID_NULL)
        {

        }

        ~localComPtr()
        {
            if (this->m_tuningSpaceEnum)
                this->m_tuningSpaceEnum->Release();

            if (this->m_testTuningSpace)
                this->m_testTuningSpace->Release();

            if (this->m_tuneRequest)
                this->m_tuneRequest->Release();

            if (this->m_locator)
                this->m_locator->Release();
        }

        IEnumTuningSpaces *m_tuningSpaceEnum;
        ITuningSpace *m_testTuningSpace;
        ITuneRequest *m_tuneRequest;
        ILocator *m_locator;
        CLSID m_testNetworkType;
    }l;

    HRESULT hr;

    if (!this->m_scanningTuner)
        return false;

    if (this->m_tuningSpace)
    {
        hr = this->m_tuningSpace->get__NetworkType(&l.m_testNetworkType);

        if (FAILED(hr))
            l.m_testNetworkType = GUID_NULL;

        if (l.m_testNetworkType == networkType)
        {
            hr = this->m_scanningTuner->get_TuneRequest(&l.m_tuneRequest);

            if (SUCCEEDED(hr))
                return true;

            hr = this->m_tuningSpace->CreateTuneRequest(&l.m_tuneRequest);

            if (SUCCEEDED(hr))
                return true;
        }

        if (l.m_tuneRequest)
            l.m_tuneRequest->Release();

        l.m_tuneRequest = nullptr;
    }

    hr = this->m_scanningTuner->EnumTuningSpaces(&l.m_tuningSpaceEnum);

    if (FAILED(hr))
        return false;

    do
    {
        l.m_testNetworkType = GUID_NULL;

        if (l.m_testTuningSpace)
            l.m_testTuningSpace->Release();

        l.m_testTuningSpace = nullptr;

        if (this->m_tuningSpace)
            this->m_tuningSpace->Release();

        this->m_tuningSpace = nullptr;

        if (!l.m_tuningSpaceEnum)
            break;

        hr = l.m_tuningSpaceEnum->Next(1, &l.m_testTuningSpace, nullptr);

        if (hr != S_OK)
            break;

        hr = l.m_testTuningSpace->get__NetworkType(&l.m_testNetworkType);

        if (FAILED(hr))
            l.m_testNetworkType = GUID_NULL;

        if (l.m_testNetworkType == networkType)
            break;
    }
    while (true);

    if (l.m_testNetworkType == GUID_NULL)
        return false;

    if (l.m_testTuningSpace == nullptr)
        return false;

    hr = this->m_scanningTuner->put_TuningSpace(l.m_testTuningSpace);

    if (FAILED(hr))
        return false;

    hr = l.m_testTuningSpace->get_DefaultLocator(&l.m_locator);

    if (FAILED(hr))
        return false;

    hr = l.m_testTuningSpace->CreateTuneRequest(&l.m_tuneRequest);

    if (FAILED(hr))
        return false;

    hr = l.m_tuneRequest->put_Locator(l.m_locator);

    if (FAILED(hr))
        return false;

    hr = this->m_scanningTuner->Validate(l.m_tuneRequest);

    if (FAILED(hr))
        return false;

    hr = this->m_scanningTuner->put_TuneRequest(l.m_tuneRequest);

    if (FAILED(hr))
        return false;

    return true;
}

bool DTVDShow::getNextNetworkType(CLSID *networkType) const
{
    if (*networkType == GUID_NULL)
    {
        *networkType = CLSID_DVBCNetworkProvider;
        return true;
    }
    else if (*networkType == CLSID_DVBCNetworkProvider)
    {
        *networkType = CLSID_DVBTNetworkProvider;
        return true;
    }
    else if (*networkType == CLSID_DVBTNetworkProvider)
    {
        *networkType = CLSID_DVBSNetworkProvider;
        return true;
    }
    else if (*networkType == CLSID_DVBSNetworkProvider)
    {
        *networkType = CLSID_ATSCNetworkProvider;
        return true;
    }

    *networkType = GUID_NULL;

    return false;
}

bool DTVDShow::build()
{
    class localComPtr
    {
    public:
        localComPtr():
            m_tuningSpaceContainer(nullptr)
        {

        }

        ~localComPtr()
        {
            if (this->m_tuningSpaceContainer)
                this->m_tuningSpaceContainer->Release();
        }

        ITuningSpaceContainer *m_tuningSpaceContainer;
    }l;

    HRESULT hr;
    long captureUsed = -1;
    long tifUsed = -1;
    AM_MEDIA_TYPE grabberMediaType;

    if (!this->m_scanningTuner || !this->m_tunerDevice)
        return false;

    hr = this->m_scanningTuner->get_TuningSpace(&this->m_tuningSpace);

    if (FAILED(hr))
        return false;

    hr = this->m_tuningSpace->get__NetworkType(&this->m_networkType);

    if (FAILED(hr))
        return false;

    if (!this->findFilter(KSCATEGORY_BDA_RECEIVER_COMPONENT, &captureUsed, this->m_tunerDevice, &this->m_captureDevice))
    {
        this->m_captureDevice = this->m_tunerDevice;
        this->m_captureDevice->AddRef();
    }

    if (this->m_sampleGrabber)
        this->m_sampleGrabber->Release();

    this->m_sampleGrabber = nullptr;

    hr = CoCreateInstance(CLSID_SampleGrabber, nullptr, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (void**)&this->m_sampleGrabber);

    if (FAILED(hr))
        return false;

    hr = this->m_filterGraph->AddFilter(this->m_sampleGrabber, L"Sample Grabber");

    if (FAILED(hr))
        return false;

    if (this->m_grabber)
        this->m_grabber->Release();

    this->m_grabber = nullptr;

    hr = this->m_sampleGrabber->QueryInterface(IID_ISampleGrabber, (void**)&this->m_grabber);

    if (FAILED(hr))
        return false;

    for (int i = 0; i < 2; i++)
    {
        ZeroMemory(&grabberMediaType, sizeof(AM_MEDIA_TYPE));

        grabberMediaType.majortype = MEDIATYPE_Stream;
        grabberMediaType.subtype = i == 0 ? MEDIASUBTYPE_MPEG2_TRANSPORT : KSDATAFORMAT_SUBTYPE_BDA_MPEG2_TRANSPORT;

        hr = this->m_grabber->SetMediaType(&grabberMediaType);

        if (SUCCEEDED(hr))
        {
            if (this->connect(this->m_captureDevice, this->m_sampleGrabber))
                break;
        }
    }

    if (FAILED(hr))
        return false;

    if (this->m_mpegDemux)
        this->m_mpegDemux->Release();

    this->m_mpegDemux = nullptr;

    hr = CoCreateInstance(CLSID_MPEG2Demultiplexer, nullptr, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (void**)&this->m_mpegDemux);

    if (FAILED(hr))
        return false;

    hr = this->m_filterGraph->AddFilter(this->m_mpegDemux, L"Demux");

    if (FAILED(hr))
        return false;

    if (!this->connect(this->m_sampleGrabber, this->m_mpegDemux))
        return false;

    if (!this->findFilter(KSCATEGORY_BDA_TRANSPORT_INFORMATION, &tifUsed, this->m_mpegDemux, &this->m_transportInfo))
        return false;

    hr = CoCreateInstance(CLSID_Mpeg2Data, nullptr, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (void**)&this->m_mpeg2Data);

    if (FAILED(hr))
        return false;

    hr = this->m_filterGraph->AddFilter(this->m_mpeg2Data, L"Mpeg2Data");

    if (FAILED(hr))
        return false;

    if (!this->connect(this->m_mpegDemux, this->m_mpeg2Data))
        return false;

    hr = this->m_mpeg2Data->QueryInterface(IID_IMpeg2Data, (void**)&this->m_mpeg2DataParser);

    if (FAILED(hr))
        return false;

    hr = this->m_grabber->SetBufferSamples(true);

    if (FAILED(hr))
        return false;

    hr = this->m_grabber->SetOneShot(false);

    if (FAILED(hr))
        return false;

    hr = this->m_grabber->SetCallback(this, 0);

    if (FAILED(hr))
        return false;

    if (!this->registerGraph())
        this->m_graphRegister = 0;

    if (this->m_mediaControl)
        this->m_mediaControl->Release();

    this->m_mediaControl = nullptr;

    hr = this->m_filterGraph->QueryInterface(IID_IMediaControl, (void**)&this->m_mediaControl);

    if (FAILED(hr))
        return false;

    return true;
}

bool DTVDShow::check(const IID &networkType)
{
    class localComPtr
    {
    public:
        localComPtr():
            m_tuningSpaceContainer(nullptr)
        {

        }

        ~localComPtr()
        {
            if (this->m_tuningSpaceContainer)
                this->m_tuningSpaceContainer->Release();
        }

        ITuningSpaceContainer *m_tuningSpaceContainer;
    }l;

    HRESULT hr;

    this->m_systems &= ~this->getSystems(networkType);

    if (this->m_filterGraph)
        this->destroy();

    this->m_filterGraph = nullptr;

    hr = CoCreateInstance(CLSID_FilterGraph, nullptr, CLSCTX_INPROC, IID_IGraphBuilder, (void**)&this->m_filterGraph);

    if (FAILED(hr))
        return false;

    if (this->m_networkProvider)
        this->m_networkProvider->Release();

    this->m_networkProvider = nullptr;

    hr = CoCreateInstance(networkType, nullptr, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (void**)&this->m_networkProvider);

    if (FAILED(hr))
        return false;

    hr = this->m_filterGraph->AddFilter(this->m_networkProvider, L"Network Provider");

    if (FAILED(hr))
        return false;

    if (this->m_tunerUsed < 0 && this->m_adapter >= 0)
        this->m_tunerUsed = this->m_adapter - 1;

    if (this->m_scanningTuner)
        this->m_scanningTuner->Release();

    this->m_scanningTuner = nullptr;

    hr = this->m_networkProvider->QueryInterface(IID_IScanningTuner, (void**)&this->m_scanningTuner);

    if (FAILED(hr))
        return false;

    if (!this->setUpTuner(networkType))
        return false;

    if (!this->findFilter(KSCATEGORY_BDA_NETWORK_TUNER, &this->m_tunerUsed, this->m_networkProvider, &this->m_tunerDevice))
        return false;

    if (this->m_adapter > 0 && this->m_tunerUsed != this->m_adapter)
        return false;

    this->m_systems |= this->getSystems(networkType);

    return true;
}

bool DTVDShow::getFilterName(IBaseFilter *filter, QString *ret) const
{
    FILTER_INFO filterInfo;
    HRESULT hr;

    hr = filter->QueryFilterInfo(&filterInfo);

    if (FAILED(hr))
        return hr;

    *ret = QString::fromWCharArray(filterInfo.achName);

    if (filterInfo.pGraph)
        filterInfo.pGraph->Release();

    return true;
}

unsigned int DTVDShow::getSystems(const IID &clsid) const
{
    unsigned int sys = 0;

    if (clsid == CLSID_DVBTNetworkProvider)
        sys = ST_DVB_T;
    else if (clsid == CLSID_DVBCNetworkProvider)
        sys = ST_DVB_C;
    else if (clsid == CLSID_DVBSNetworkProvider)
        sys = ST_DVB_S;
    else if (clsid == CLSID_ATSCNetworkProvider)
        sys = ST_ATSC_T | ST_ATSC_C;
    else if (clsid == CLSID_DigitalCableNetworkType)
        sys = ST_CQAM;

    return sys;
}

bool DTVDShow::findFilter(const IID &clsid, long *monikerUsed, IBaseFilter *upStream, IBaseFilter **downStream)
{
    class localComPtr
    {
    public:
        localComPtr():
            m_monikerEnum(nullptr),
            m_moniker(nullptr),
            m_bindContext(nullptr),
            m_propertyBag(nullptr)
        {
            VariantInit(&this->m_bstr);
        }

        ~localComPtr()
        {
            if (this->m_monikerEnum)
                this->m_monikerEnum->Release();

            if (this->m_moniker)
                this->m_moniker->Release();

            if (this->m_bindContext)
                this->m_bindContext->Release();

            if (this->m_propertyBag)
                this->m_propertyBag->Release();

            VariantClear(&this->m_bstr);
        }

        IEnumMoniker *m_monikerEnum;
        IMoniker *m_moniker;
        IBindCtx *m_bindContext;
        IPropertyBag *m_propertyBag;
        VARIANT m_bstr;
    }l;

    HRESULT hr;
    int monikerIndex = -1;

    if (!this->m_systemDevEnum)
    {
        hr = CoCreateInstance(CLSID_SystemDeviceEnum, nullptr, CLSCTX_INPROC, IID_ICreateDevEnum, (void**)&this->m_systemDevEnum);

        if (FAILED(hr))
            return false;
    }

    hr = this->m_systemDevEnum->CreateClassEnumerator(clsid, &l.m_monikerEnum, 0);

    if (FAILED(hr))
        return false;

    do
    {
        if (l.m_bstr.vt != VT_EMPTY)
            VariantClear(&l.m_bstr);

        if (*downStream)
            (*downStream)->Release();

        *downStream = nullptr;

        if (l.m_propertyBag)
            l.m_propertyBag->Release();

        l.m_propertyBag = nullptr;

        if (l.m_bindContext)
            l.m_bindContext->Release();

        l.m_bindContext = nullptr;

        if (l.m_moniker)
            l.m_moniker->Release();

        l.m_moniker = nullptr;

        if (!l.m_monikerEnum)
            break;

        hr = l.m_monikerEnum->Next(1, &l.m_moniker, 0);

        if (hr != S_OK)
            break;

        monikerIndex++;

        if (monikerIndex <= *monikerUsed)
            continue;

        *monikerUsed = monikerIndex;

        hr = CreateBindCtx(0, &l.m_bindContext);

        if (FAILED(hr))
            continue;

        hr = l.m_moniker->BindToObject(l.m_bindContext, nullptr, IID_IBaseFilter, (void**)downStream);

        if (FAILED(hr))
            continue;

        hr = l.m_moniker->BindToStorage(l.m_bindContext, nullptr, IID_IPropertyBag, (void**)&l.m_propertyBag);

        if (FAILED(hr))
            continue;

        hr = l.m_propertyBag->Read(L"FriendlyName", &l.m_bstr, nullptr);

        if (FAILED(hr))
            continue;

        hr = this->m_filterGraph->AddFilter(*downStream, l.m_bstr.bstrVal);

        if (FAILED(hr))
            continue;

        if (this->connect(upStream, *downStream))
            return true;

        hr = this->m_filterGraph->RemoveFilter(*downStream);

        if (FAILED(hr))
            continue;
    }
    while (true);

    return false;
}

bool DTVDShow::listAdapters(const IID &clsid, QVector<AdapterInfo> *ret) const
{
    class localComPtr
    {
    public:
        localComPtr():
            m_systemDevEnum(nullptr),
            m_monikerEnum(nullptr),
            m_moniker(nullptr),
            m_filter(nullptr),
            m_bindContext(nullptr),
            m_propertyBag(nullptr)
        {
            VariantInit(&this->m_name);
        }

        ~localComPtr()
        {
            if (this->m_propertyBag)
                this->m_propertyBag->Release();

            if (this->m_bindContext)
                this->m_bindContext->Release();

            if (this->m_filter)
                this->m_filter->Release();

            if (this->m_moniker)
                this->m_moniker->Release();

            if (this->m_monikerEnum)
                this->m_monikerEnum->Release();

            if (this->m_systemDevEnum)
                this->m_systemDevEnum->Release();

            VariantClear(&this->m_name);
        }

        ICreateDevEnum *m_systemDevEnum;
        IEnumMoniker *m_monikerEnum;
        IMoniker *m_moniker;
        IBaseFilter *m_filter;
        IBindCtx *m_bindContext;
        IPropertyBag *m_propertyBag;
        VARIANT m_name;
    }l;

    HRESULT hr;
    int monikerIndex = -1;

    if (l.m_systemDevEnum)
        l.m_systemDevEnum->Release();

    l.m_systemDevEnum = nullptr;

    hr = CoCreateInstance(CLSID_SystemDeviceEnum, nullptr, CLSCTX_INPROC, IID_ICreateDevEnum, (void**)&l.m_systemDevEnum);

    if (FAILED(hr))
        return false;

    if (l.m_monikerEnum)
        l.m_monikerEnum->Release();

    l.m_monikerEnum = nullptr;

    hr = l.m_systemDevEnum->CreateClassEnumerator(clsid, &l.m_monikerEnum, 0);

    if (FAILED(hr))
        return false;

    do
    {
        if (l.m_name.vt != VT_EMPTY)
            VariantClear(&l.m_name);

        if (l.m_propertyBag)
            l.m_propertyBag->Release();

        l.m_propertyBag = nullptr;

        if (l.m_filter)
            l.m_filter->Release();

        l.m_filter = nullptr;

        if (l.m_bindContext)
            l.m_bindContext->Release();

        l.m_bindContext = nullptr;

        if (l.m_moniker)
            l.m_moniker->Release();

        l.m_moniker = nullptr;

        if (!l.m_monikerEnum)
            break;

        hr = l.m_monikerEnum->Next(1, &l.m_moniker, 0);

        if (hr != S_OK)
            break;

        monikerIndex++;

        hr = CreateBindCtx(0, &l.m_bindContext);

        if (FAILED(hr))
            continue;

        hr = l.m_moniker->BindToObject(l.m_bindContext, nullptr, IID_IBaseFilter, (void**)&l.m_filter);

        if (FAILED(hr))
            continue;

        hr = l.m_moniker->BindToStorage(nullptr, nullptr, IID_IPropertyBag, (void**)&l.m_propertyBag);

        if (FAILED(hr))
            continue;

        hr = l.m_propertyBag->Read(L"FriendlyName", &l.m_name, nullptr);

        if (FAILED(hr))
            continue;

        AdapterInfo info;

        info.index = monikerIndex;
        info.name = QString::fromWCharArray(l.m_name.bstrVal);

        ret->append(info);
    }
    while (true);

    return true;
}

bool DTVDShow::connect(IBaseFilter *filterUpStream, IBaseFilter *filterDownStream) const
{
    class localComPtr
    {
    public:
        localComPtr():
            m_upStream(nullptr),
            m_downStream(nullptr),
            m_upStreamEnum(nullptr),
            m_downStreamEnum(nullptr),
            m_temp(nullptr)
        {

        }

        ~localComPtr()
        {
            if (this->m_temp)
                this->m_temp->Release();

            if (this->m_downStream)
                this->m_downStream->Release();

            if (this->m_upStream)
                this->m_upStream->Release();

            if (this->m_upStreamEnum)
                this->m_upStreamEnum->Release();

            if (this->m_downStreamEnum)
                this->m_downStreamEnum->Release();
        }

        IPin *m_upStream;
        IPin *m_downStream;
        IEnumPins *m_upStreamEnum;
        IEnumPins *m_downStreamEnum;
        IPin *m_temp;
    } l;

    HRESULT hr;
    PIN_DIRECTION dir;

    hr = filterUpStream->EnumPins(&l.m_upStreamEnum);

    if (FAILED(hr))
        return false;

    do
    {
        if (l.m_upStream)
            l.m_upStream ->Release();

        l.m_upStream = nullptr;

        if (!l.m_upStreamEnum)
            break;

        hr = l.m_upStreamEnum->Next(1, &l.m_upStream, 0);

        if (hr != S_OK)
            break;

        hr = l.m_upStream->QueryDirection(&dir);

        if (FAILED(hr))
            return false;

        hr = l.m_upStream->ConnectedTo(&l.m_downStream);

        if (SUCCEEDED(hr))
        {
            l.m_downStream->Release();
            l.m_downStream = nullptr;
        }

        if (FAILED(hr) && hr != VFW_E_NOT_CONNECTED)
            return false;

        if (dir == PINDIR_OUTPUT && hr == VFW_E_NOT_CONNECTED)
        {
            hr = filterDownStream->EnumPins(&l.m_downStreamEnum);

            if (FAILED(hr))
                return false;

            do
            {
                if (l.m_downStream)
                    l.m_downStream->Release();

                l.m_downStream = nullptr;

                if (!l.m_downStreamEnum)
                    break;

                hr = l.m_downStreamEnum->Next(1, &l.m_downStream, 0);

                if (hr != S_OK)
                    break;

                hr = l.m_downStream->QueryDirection(&dir);

                if (FAILED(hr))
                    return false;

                hr = l.m_downStream->ConnectedTo(&l.m_temp);

                if (SUCCEEDED(hr))
                {
                    l.m_temp->Release();
                    l.m_temp = nullptr;
                }

                if (FAILED(hr) && hr != VFW_E_NOT_CONNECTED)
                    return false;

                if (dir == PINDIR_INPUT && hr == VFW_E_NOT_CONNECTED)
                {
                    hr = this->m_filterGraph->ConnectDirect(l.m_upStream, l.m_downStream, nullptr);

                    if (SUCCEEDED(hr))
                        return true;
                }
            }
            while (true);

            if (l.m_downStreamEnum)
                l.m_downStreamEnum->Release();

            l.m_downStreamEnum = nullptr;

            if (l.m_downStream)
                l.m_downStream->Release();

            l.m_downStream = nullptr;
        }
    }
    while (true);

    return false;
}

bool DTVDShow::start()
{
    HRESULT hr;
    OAFilterState state;

    if (!this->m_mediaControl)
        return false;

    hr = this->m_mediaControl->Run();

    if (hr == S_OK)
        return true;

    while ((hr = this->m_mediaControl->GetState(100, &state)) != S_OK)
    {
        if (hr == E_FAIL)
            return false;
    }

    if (state == State_Running)
        return true;

    this->m_mediaControl->StopWhenReady();

    return false;
}

void DTVDShow::destroy()
{
    if (this->m_mediaControl)
        this->m_mediaControl->StopWhenReady();

    if (this->m_graphRegister)
        this->deregisterGraph();

    this->m_dataLock.lock();
    this->m_data.Clear();
    this->m_dataCond.wakeOne();
    this->m_dataLock.unlock();

    if (this->m_transportInfo)
    {
        this->m_filterGraph->RemoveFilter(this->m_transportInfo);

        this->m_transportInfo->Release();
        this->m_transportInfo = nullptr;
    }

    if (this->m_mpeg2DataParser)
    {
        this->m_mpeg2DataParser->Release();
        this->m_mpeg2DataParser = nullptr;
    }

    if (this->m_mpeg2Data)
    {
        this->m_filterGraph->RemoveFilter(this->m_mpeg2Data);

        this->m_mpeg2Data->Release();
        this->m_mpeg2Data = nullptr;
    }

    if (this->m_mpegDemux)
    {
        this->m_filterGraph->RemoveFilter(this->m_mpegDemux);

        this->m_mpegDemux->Release();
        this->m_mpegDemux = nullptr;
    }

    if (this->m_grabber)
    {
        this->m_grabber->Release();
        this->m_grabber = nullptr;
    }

    if (this->m_sampleGrabber)
    {
        this->m_filterGraph->RemoveFilter(this->m_sampleGrabber);

        this->m_sampleGrabber->Release();
        this->m_sampleGrabber = nullptr;
    }

    if (this->m_captureDevice)
    {
        this->m_filterGraph->RemoveFilter(this->m_captureDevice);

        this->m_captureDevice->Release();
        this->m_captureDevice = nullptr;
    }

    if (this->m_tunerDevice)
    {
        this->m_filterGraph->RemoveFilter(this->m_tunerDevice);

        this->m_tunerDevice->Release();
        this->m_tunerDevice = nullptr;
    }

    if (this->m_systemDevEnum)
    {
        this->m_systemDevEnum->Release();
        this->m_systemDevEnum = nullptr;
    }

    if (this->m_scanningTuner)
    {
        this->m_scanningTuner->Release();
        this->m_scanningTuner = nullptr;
    }

    if (this->m_networkProvider)
    {
        this->m_filterGraph->RemoveFilter(this->m_networkProvider);

        this->m_networkProvider->Release();
        this->m_networkProvider = nullptr;
    }

    if (this->m_mediaControl)
    {
        this->m_mediaControl->Release();
        this->m_mediaControl = nullptr;
    }

    if (this->m_filterGraph)
    {
        this->m_filterGraph->Release();
        this->m_filterGraph = nullptr;
    }

    this->m_graphRegister = 0;
    this->m_tunerUsed = -1;
    this->m_networkType = GUID_NULL;
}

bool DTVDShow::registerGraph()
{
    class localComPtr
    {
    public:
        localComPtr():
            m_moniker(nullptr),
            m_roTable(nullptr)
        {

        }

        ~localComPtr()
        {
            if (this->m_moniker)
                this->m_moniker->Release();

            if (this->m_roTable)
                this->m_roTable->Release();
        }

        IMoniker *m_moniker;
        IRunningObjectTable *m_roTable;

    } l;

    QString graphName;
    HRESULT hr;

    hr = GetRunningObjectTable(0, &l.m_roTable);

    if (FAILED(hr))
        return false;

    graphName = QString("%1 BDA Graph %2 PID %3")
            .arg(APP_NAME)
            .arg((qulonglong)&graphName)
            .arg(QCoreApplication::applicationPid());

    WCHAR name[graphName.length() + 1];

    memset(name, 0, sizeof(name));
    graphName.toWCharArray(name);

    hr = CreateItemMoniker(L"!", name, &l.m_moniker);

    if (FAILED(hr))
        return false;

    hr = l.m_roTable->Register(ROTFLAGS_REGISTRATIONKEEPSALIVE, this->m_filterGraph, l.m_moniker, &this->m_graphRegister);

    if (FAILED(hr))
        return false;

    return true;
}

void DTVDShow::deregisterGraph()
{
    HRESULT hr;
    IRunningObjectTable *roTable;

    hr = GetRunningObjectTable(0, &roTable);

    if (SUCCEEDED(hr))
        roTable->Revoke(this->m_graphRegister);

    this->m_graphRegister = 0;
    roTable->Release();
}

bool DTVDShow::getDTVParser(IUnknown **parser) const
{
    HRESULT hr;
    CLSID clsParser = GUID_NULL;
    IID iidParser = GUID_NULL;

    switch (this->m_type)
    {
        case ST_ATSC_C:
        case ST_ATSC_T:
        case ST_CQAM:
        {
            clsParser = CLSID_IAtscPsipParser;
            iidParser = IID_IAtscPsipParser;

            break;
        }
        case ST_DVB_C:
        case ST_DVB_C2:
        case ST_DVB_S:
        case ST_DVB_S2:
        case ST_DVB_T:
        case ST_DVB_T2:
        {
            clsParser = CLSID_IDvbSiParser2;
            iidParser = IID_IDvbSiParser2;

            break;
        }
        case ST_ISDB_C:
        case ST_ISDB_T:
        case ST_ISDB_S:
        {
            clsParser = CLSID_IDvbSiParser2;
            iidParser = IID_IIsdbSiParser2;

            break;
        }
        default:
        {
            break;
        }
    }

    hr = CoCreateInstance(clsParser, nullptr, CLSCTX_INPROC, iidParser, (void**)parser);

    return FAILED(hr) ? false : true;
}

QString DTVDShow::getATSCChannelName(IUnknown *parser) const
{
    class localComPtr
    {
    public:
        localComPtr() :
            m_vct(nullptr)
        {

        }

        ~localComPtr()
        {
            if (this->m_vct)
                this->m_vct->Release();
        }

        IATSC_VCT *m_vct;
    }l;

    HRESULT hr;
    QString name;
    DWORD count = 0;
    IAtscPsipParser *atsc = (IAtscPsipParser*)parser;

    hr = atsc->Initialize(this->m_mpeg2DataParser);

    if (FAILED(hr))
        return name;

    hr = atsc->GetVCT(ATSC_VCT_TERR_TID, FALSE, &l.m_vct);

    if (FAILED(hr))
        hr = atsc->GetVCT(ATSC_VCT_TERR_TID, TRUE, &l.m_vct);

    if (FAILED(hr))
        hr = atsc->GetVCT(ATSC_VCT_CABL_TID, FALSE, &l.m_vct);

    if (FAILED(hr))
        hr = atsc->GetVCT(ATSC_VCT_CABL_TID, TRUE, &l.m_vct);

    if (FAILED(hr))
        return name;

    hr = l.m_vct->GetCountOfRecords(&count);

    if (FAILED(hr))
        return name;

    for (DWORD i = 0; i < count; i++)
    {
        WORD majorChannel = -1;
        WORD minorChannel = -1;
        WCHAR *channelName = nullptr;
        bool ok = true;

        hr = l.m_vct->GetRecordMajorChannelNumber(i, &majorChannel);
        ok &= SUCCEEDED(hr);

        hr = l.m_vct->GetRecordMinorChannelNumber(i, &minorChannel);
        ok &= SUCCEEDED(hr);

        hr = l.m_vct->GetRecordName(i, &channelName);
        ok &= SUCCEEDED(hr);

        name = QString("%1 [%2-%3]")
                .arg(QString::fromWCharArray(channelName).trimmed())
                .arg(majorChannel)
                .arg(minorChannel);

        CoTaskMemFree(channelName);

        if (ok)
            break;
    }

    return name;
}

QString DTVDShow::getDVBChannelName(IUnknown *parser) const
{
    class localComPtr
    {
    public:
        localComPtr() :
            m_sdt(nullptr)
        {

        }

        ~localComPtr()
        {
            if (this->m_sdt)
                this->m_sdt->Release();
        }

        IDVB_SDT *m_sdt;
    }l;

    HRESULT hr;
    QString name;
    DWORD count = 0;
    IDvbSiParser2 *dvb = (IDvbSiParser2*)parser;

    hr = dvb->Initialize(this->m_mpeg2DataParser);

    if (FAILED(hr))
        return name;

    hr = dvb->GetSDT(DVB_SDT_ACTUAL_TID, nullptr, &l.m_sdt);

    if (FAILED(hr))
        hr = dvb->GetSDT(DVB_SDT_OTHER_TID, nullptr, &l.m_sdt);

    if (FAILED(hr))
        return name;

    hr = l.m_sdt->GetCountOfRecords(&count);

    if (FAILED(hr))
        return name;

    for (DWORD i = 0; i < count; i++)
    {
        IDvbServiceDescriptor *dvbDesc = nullptr;
        BSTR channelName = nullptr;
        bool ok = true;

        hr = l.m_sdt->GetRecordDescriptorByTag(i, DVB_SERVICE_TAG, nullptr, (IGenericDescriptor**)&dvbDesc);

        if (FAILED(hr))
            continue;

        hr = dvbDesc->GetServiceNameEmphasized(&channelName);
        ok &= SUCCEEDED(hr);

        if (dvbDesc)
            dvbDesc->Release();

        name = QString::fromWCharArray(channelName).trimmed();

        SysFreeString(channelName);

        if (ok)
            break;
    }

    return name;
}

QString DTVDShow::getISDBChannelName(IUnknown *parser) const
{
    class localComPtr
    {
    public:
        localComPtr() :
            m_sdt(nullptr)
        {

        }

        ~localComPtr()
        {
            if (this->m_sdt)
                this->m_sdt->Release();
        }

        IISDB_SDT *m_sdt;
    }l;

    HRESULT hr;
    QString name;
    DWORD count = 0;
    IIsdbSiParser2 *isdb = (IIsdbSiParser2*)parser;

    hr = isdb->Initialize(this->m_mpeg2DataParser);

    if (FAILED(hr))
        return name;

    hr = isdb->GetSDT(DVB_SDT_ACTUAL_TID, nullptr, &l.m_sdt);

    if (FAILED(hr))
        hr = isdb->GetSDT(DVB_SDT_OTHER_TID, nullptr, &l.m_sdt);

    if (FAILED(hr))
        return name;

    hr = l.m_sdt->GetCountOfRecords(&count);

    if (FAILED(hr))
        return name;

    for (DWORD i = 0; i < count; i++)
    {
        IDvbServiceDescriptor *isdbDesc = nullptr;
        BSTR channelName = nullptr;
        bool ok = true;

        hr = l.m_sdt->GetRecordDescriptorByTag(i, DVB_SERVICE_TAG, nullptr, (IGenericDescriptor**)&isdbDesc);

        if (FAILED(hr))
            continue;

        hr = isdbDesc->GetServiceNameEmphasized(&channelName);
        ok &= SUCCEEDED(hr);

        if (isdbDesc)
            isdbDesc->Release();

        name = QString::fromWCharArray(channelName).trimmed();

        SysFreeString(channelName);

        if (ok)
            break;
    }

    return name;
}

QDateTime DTVDShow::getATSCCurrentDateTime(IUnknown *parser) const
{
    class localComPtr
    {
    public:
        localComPtr() :
            m_stt(nullptr)
        {

        }

        ~localComPtr()
        {
            if (this->m_stt)
                this->m_stt->Release();
        }

        IATSC_STT *m_stt;
    }l;

    HRESULT hr;
    QDateTime time;
    MPEG_DATE_AND_TIME t;
    IAtscPsipParser *atsc = (IAtscPsipParser*)parser;

    hr = atsc->Initialize(this->m_mpeg2DataParser);

    if (FAILED(hr))
        return time;

    hr = atsc->GetSTT(&l.m_stt);

    if (FAILED(hr))
        return time;

    hr = l.m_stt->GetSystemTime(&t);

    if (FAILED(hr))
        return time;

    time = QDateTime(QDate(t.D.Year, t.D.Month, t.D.Date),
                     QTime(t.T.Hours, t.T.Minutes, t.T.Seconds),
                     Qt::UTC);

    return time;
}

QDateTime DTVDShow::getDVBCurrentDateTime(IUnknown *parser) const
{
    class localComPtr
    {
    public:
        localComPtr() :
            m_tdt(nullptr)
        {

        }

        ~localComPtr()
        {
            if (this->m_tdt)
                this->m_tdt->Release();
        }

        IDVB_TDT *m_tdt;
    }l;

    HRESULT hr;
    QDateTime time;
    MPEG_DATE_AND_TIME t;
    IDvbSiParser2 *dvb = (IDvbSiParser2*)parser;

    hr = dvb->Initialize(this->m_mpeg2DataParser);

    if (FAILED(hr))
        return time;

    hr = dvb->GetTDT(&l.m_tdt);

    if (FAILED(hr))
        return time;

    hr = l.m_tdt->GetUTCTime(&t);

    if (FAILED(hr))
        return time;

    time = QDateTime(QDate(t.D.Year, t.D.Month, t.D.Date),
                     QTime(t.T.Hours, t.T.Minutes, t.T.Seconds),
                     Qt::UTC);

    return time;
}

QDateTime DTVDShow::getISDBCurrentDateTime(IUnknown *parser) const
{
    return this->getDVBCurrentDateTime(parser);
}

void DTVDShow::getATSCEPG(IUnknown *parser, QVector<DTVReaderInterface::EPG> *ret) const
{
    class localComPtr
    {
    public:
        localComPtr() :
            m_mgt(nullptr),
            m_eit(nullptr),
            m_title(nullptr)
        {

        }

        ~localComPtr()
        {
            if (this->m_eit)
                this->m_eit->Release();

            if (this->m_mgt)
                this->m_mgt->Release();

            if (this->m_title)
                CoTaskMemFree(this->m_title);
        }

        IATSC_MGT *m_mgt;
        IATSC_EIT *m_eit;
        BYTE *m_title;
    }l;

    HRESULT hr;
    DWORD count = 0;
    IAtscPsipParser *atsc = (IAtscPsipParser*)parser;

    hr = atsc->Initialize(this->m_mpeg2DataParser);

    if (FAILED(hr))
        return;

    hr = atsc->GetMGT(&l.m_mgt);

    if (FAILED(hr))
        return;

    hr = l.m_mgt->GetCountOfRecords(&count);

    if (FAILED(hr))
        return;

    QVector<PID> pids;

    for (DWORD i = 0; i < count; i++)
    {
        WORD type;
        PID pid;

        hr = l.m_mgt->GetRecordType(i, &type);

        if (FAILED(hr))
            continue;

        if (type < ATSC_MGT_TYPE_START || type > ATSC_MGT_TYPE_END)
            continue;

        hr = l.m_mgt->GetRecordTypePid(i, &pid);

        if (FAILED(hr))
            continue;

        pids.append(pid);
    }

    for (PID pid : pids)
    {
        localComPtr eit;
        DWORD eitCount;

        hr = atsc->GetEIT(pid, nullptr, 5000, &eit.m_eit);

        if (FAILED(hr))
            continue;

        hr = eit.m_eit->GetCountOfRecords(&eitCount);

        if (FAILED(hr))
            continue;

        for (DWORD i = 0; i < eitCount; i++)
        {
            MPEG_DURATION dur;
            WORD eventID;
            MPEG_DATE_AND_TIME start;
            DWORD titleLen;

            hr = eit.m_eit->GetRecordDuration(i, &dur);

            if (FAILED(hr))
                continue;

            hr = eit.m_eit->GetRecordEventId(i, &eventID);

            if (FAILED(hr))
                continue;

            hr = eit.m_eit->GetRecordStartTime(i, &start);

            if (FAILED(hr))
                continue;

            hr = eit.m_eit->GetRecordTitleText(i, &titleLen, &eit.m_title);

            if (FAILED(hr))
                continue;

            DTVReaderInterface::EPG epg;

            epg.title = this->decodeMultipleStringStructure(eit.m_title, titleLen);
            epg.start = QDateTime(QDate(start.D.Year, start.D.Month, start.D.Date),
                                  QTime(start.T.Hours, start.T.Minutes, start.T.Seconds),
                                  Qt::UTC);
            epg.duration = QTime(dur.Hours, dur.Minutes, dur.Seconds);

            ret->append(epg);
        }
    }
}

void DTVDShow::getDVBEPG(IUnknown *parser, QVector<DTVReaderInterface::EPG> *ret) const
{
    class localComPtr
    {
    public:
        localComPtr() :
            m_eit(nullptr)
        {

        }

        ~localComPtr()
        {
            if (this->m_eit)
                this->m_eit->Release();
        }

        IDVB_EIT2 *m_eit;
    }l;

    HRESULT hr;
    DWORD count = 0;
    IDvbSiParser2 *dvb = (IDvbSiParser2*)parser;

    hr = dvb->Initialize(this->m_mpeg2DataParser);

    if (FAILED(hr))
        return;

    hr = dvb->GetEIT2(DVB_EIT_ACTUAL_TID, nullptr, nullptr, &l.m_eit);

    if (FAILED(hr))
        hr = dvb->GetEIT2(DVB_EIT_OTHER_TID, nullptr, nullptr, &l.m_eit);

    if (FAILED(hr))
        return;

    hr = l.m_eit->GetCountOfRecords(&count);

    if (FAILED(hr))
        return;

    for (DWORD i = 0; i < count; i++)
    {
        IDvbShortEventDescriptor *dvbDesc = nullptr;
        MPEG_DURATION dur;
        MPEG_DATE_AND_TIME start;
        BSTR title;
        bool ok = true;

        hr = l.m_eit->GetRecordDuration(i, &dur);

        if (FAILED(hr))
            continue;

        hr = l.m_eit->GetRecordStartTime(i, &start);

        if (FAILED(hr))
            continue;

        hr = l.m_eit->GetRecordDescriptorByTag(i, DVB_SHORT_EVENT_TAG, nullptr, (IGenericDescriptor**)&dvbDesc);

        if (FAILED(hr))
            continue;

        hr = dvbDesc->GetEventNameW(STRCONV_MODE_DVB, &title);
        ok &= SUCCEEDED(hr);

        if (ok)
        {
            DTVReaderInterface::EPG epg;

            epg.title = QString::fromWCharArray(title).trimmed();
            epg.start = QDateTime(QDate(start.D.Year, start.D.Month, start.D.Date),
                                  QTime(start.T.Hours, start.T.Minutes, start.T.Seconds),
                                  Qt::UTC);
            epg.duration = QTime(dur.Hours, dur.Minutes, dur.Seconds);

            ret->append(epg);
        }

        if (dvbDesc)
            dvbDesc->Release();

        SysFreeString(title);

        if (ok)
            break;
    }
}

void DTVDShow::getISDBEPG(IUnknown *parser, QVector<DTVReaderInterface::EPG> *ret) const
{
    class localComPtr
    {
    public:
        localComPtr() :
            m_eit(nullptr)
        {

        }

        ~localComPtr()
        {
            if (this->m_eit)
                this->m_eit->Release();
        }

        IDVB_EIT2 *m_eit;
    }l;

    HRESULT hr;
    DWORD count = 0;
    IIsdbSiParser2 *isdb = (IIsdbSiParser2*)parser;

    hr = isdb->Initialize(this->m_mpeg2DataParser);

    if (FAILED(hr))
        return;

    hr = isdb->GetEIT2(DVB_EIT_ACTUAL_TID, nullptr, nullptr, &l.m_eit);

    if (FAILED(hr))
        hr = isdb->GetEIT2(DVB_EIT_OTHER_TID, nullptr, nullptr, &l.m_eit);

    if (FAILED(hr))
        return;

    hr = l.m_eit->GetCountOfRecords(&count);

    if (FAILED(hr))
        return;

    for (DWORD i = 0; i < count; i++)
    {
        IDvbShortEventDescriptor *isdbDesc = nullptr;
        MPEG_DURATION dur;
        MPEG_DATE_AND_TIME start;
        BSTR title;
        bool ok = true;

        hr = l.m_eit->GetRecordDuration(i, &dur);

        if (FAILED(hr))
            continue;

        hr = l.m_eit->GetRecordStartTime(i, &start);

        if (FAILED(hr))
            continue;

        hr = l.m_eit->GetRecordDescriptorByTag(i, DVB_SHORT_EVENT_TAG, nullptr, (IGenericDescriptor**)&isdbDesc);

        if (FAILED(hr))
            continue;

        hr = isdbDesc->GetEventNameW(STRCONV_MODE_ISDB, &title);
        ok &= SUCCEEDED(hr);

        if (ok)
        {
            DTVReaderInterface::EPG epg;

            epg.title = QString::fromWCharArray(title).trimmed();
            epg.start = QDateTime(QDate(start.D.Year, start.D.Month, start.D.Date),
                                  QTime(start.T.Hours, start.T.Minutes, start.T.Seconds),
                                  Qt::UTC);
            epg.duration = QTime(dur.Hours, dur.Minutes, dur.Seconds);

            ret->append(epg);
        }

        if (isdbDesc)
            isdbDesc->Release();

        SysFreeString(title);

        if (ok)
            break;
    }
}

ModulationType DTVDShow::getModulation(const QString &mod) const
{
    if (mod == "16QAM")
        return BDA_MOD_16QAM;
    else if (mod == "32QAM")
        return BDA_MOD_32QAM;
    else if (mod == "64QAM")
        return BDA_MOD_64QAM;
    else if (mod == "128QAM")
        return BDA_MOD_128QAM;
    else if (mod == "256QAM")
        return BDA_MOD_256QAM;

    return BDA_MOD_NOT_SET;
}

BinaryConvolutionCodeRate DTVDShow::getFEC(uint32_t fec) const
{
    switch (fec)
    {
        case DTV_FEC(1, 2):
            return BDA_BCC_RATE_1_2;
        case DTV_FEC(2, 3):
            return BDA_BCC_RATE_2_3;
        case DTV_FEC(3, 4):
            return BDA_BCC_RATE_3_4;
        case DTV_FEC(5, 6):
            return BDA_BCC_RATE_5_6;
        case DTV_FEC(7, 8):
            return BDA_BCC_RATE_7_8;
    }

    return BDA_BCC_RATE_NOT_SET;
}

GuardInterval DTVDShow::getGuard(uint32_t guard) const
{
    switch (guard)
    {
        case DTV_GUARD(1, 4):
            return BDA_GUARD_1_4;
        case DTV_GUARD(1, 8):
            return BDA_GUARD_1_8;
        case DTV_GUARD(1, 16):
            return BDA_GUARD_1_16;
        case DTV_GUARD(1, 32):
            return BDA_GUARD_1_32;
    }

    return BDA_GUARD_NOT_SET;
}

TransmissionMode DTVDShow::getTransmission(int transmission) const
{
    switch (transmission)
    {
        case 2:
            return BDA_XMIT_MODE_2K;
        case 8:
            return BDA_XMIT_MODE_8K;
    }

    return BDA_XMIT_MODE_NOT_SET;
}

HierarchyAlpha DTVDShow::getHierarchy(int hierarchy) const
{
    switch (hierarchy)
    {
        case 1:
            return BDA_HALPHA_1;
        case 2:
            return BDA_HALPHA_2;
        case 4:
            return BDA_HALPHA_4;
    }

    return BDA_HALPHA_NOT_SET;
}

Polarisation DTVDShow::getPolarization(char pol) const
{
    switch (pol)
    {
        case 'H':
            return BDA_POLARISATION_LINEAR_H;
        case 'V':
            return BDA_POLARISATION_LINEAR_V;
        case 'L':
            return BDA_POLARISATION_CIRCULAR_L;
        case 'R':
            return BDA_POLARISATION_CIRCULAR_R;
    }

    return BDA_POLARISATION_NOT_SET;
}

IPin* DTVDShow::findPinOnFilter(IBaseFilter *baseFilter, const char *expectedPinName) const
{
    HRESULT hr;
    IEnumPins *enumPin = nullptr;
    IPin *retPin = nullptr;

    if (!baseFilter || !expectedPinName)
        return nullptr;

    hr = baseFilter->EnumPins(&enumPin);

    if (SUCCEEDED(hr) && enumPin)
    {
        IPin *pin = nullptr;
        ULONG countReceived = 0;

        enumPin->Reset();

        while (SUCCEEDED(enumPin->Next(1, &pin, &countReceived)) && pin)
        {
            PIN_INFO pinInfo;
            char pinName[80];

            ZeroMemory(pinName, sizeof(pinName));

            hr = pin->QueryPinInfo(&pinInfo);

            if (SUCCEEDED(hr))
            {
                int length = wcslen(pinInfo.achName) + 1;
                char *pinSubName = new char[length];

                WideCharToMultiByte(CP_ACP, 0, pinInfo.achName, -1, pinSubName, length, nullptr, nullptr);
                snprintf(pinName, strlen(pinName) + strlen(pinSubName) + 1, "%s%s", pinName, pinSubName);

                if (strstr(pinName, expectedPinName))
                {
                    retPin = pin;
                    delete[] pinSubName;

                    break;
                }
                else
                {
                    pin = nullptr;
                    delete[] pinSubName;
                }
            }
            else
            {
                pin->Release();
            }
        }

        enumPin->Release();
    }

    return retPin;
}

STDMETHODIMP_(ULONG) DTVDShow::AddRef()
{
    return ++this->m_cbrc;
}

STDMETHODIMP_(ULONG) DTVDShow::Release()
{
    return --this->m_cbrc;
}

STDMETHODIMP DTVDShow::QueryInterface(REFIID riid, void **object)
{
    (void)riid;
    (void)object;

    return E_NOTIMPL;
}

STDMETHODIMP DTVDShow::SampleCB(double time, IMediaSample *sample)
{
    (void)time;

    const size_t sampleSize = sample->GetActualDataLength();
    BYTE *sampleData = nullptr;
    HRESULT hr;

    hr = sample->GetPointer(&sampleData);

    if (SUCCEEDED(hr) && sampleSize > 0 && sampleData)
    {
        this->m_dataLock.lock();

        if (this->m_data.GetWritableSize() >= sampleSize)
            this->m_data.Write((char*)sampleData, sampleSize);

        this->m_dataCond.wakeOne();
        this->m_dataLock.unlock();
    }

    return S_OK;
}

STDMETHODIMP DTVDShow::BufferCB(double time, BYTE *buffer, long len)
{
    (void)time;
    (void)buffer;
    (void)len;

    return E_FAIL;
}
