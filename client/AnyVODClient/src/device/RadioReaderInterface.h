﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <QString>
#include <QMetaType>
#include <QLocale>

#include <stdint.h>

class RadioReaderInterface
{
public:
    enum DemodulatorType
    {
        DT_NONE = -1,
        DT_WIDE_FM_MONO = 0,
        DT_WIDE_FM_STEREO,
        DT_COUNT,
    };

    struct AdapterData
    {
        int index;
        QString args;
    };

    struct AdapterInfo
    {
        QString name;
        AdapterData data;
    };

    struct DemodulatorTypeInfo
    {
        DemodulatorType type;
        QString name;
    };

    struct CountryInfo
    {
        QLocale::Country country;
        QString name;
    };

public:
    RadioReaderInterface();
    virtual ~RadioReaderInterface();

    virtual bool open(const QString &path) = 0;
    virtual void close() = 0;
    virtual int read(uint8_t *buf, int size) = 0;

    virtual void setFrequency(double freqHz) = 0;
    virtual double getFrequency() = 0;

    virtual float getSignalStrength() const = 0;

    virtual bool setDecimation(int decimation) = 0;
    virtual int getDecimation() const = 0;

    virtual void setAudioGain(float db) = 0;
    virtual float getAudioGain() const = 0;

    virtual void enableAGC(bool enable) = 0;
    virtual bool isEnabledAGC() const = 0;

    virtual void setEmphasis(double emphasis) = 0;

    virtual void setAntenna(const QString &antenna) = 0;
    virtual QStringList getAntennas() const = 0;

    virtual bool tune() = 0;
    virtual bool getAdapterList(QVector<AdapterInfo> *ret) const = 0;

public:
    void setAdapter(long adapter, DemodulatorType type);
    void getAdapter(long *adapter, DemodulatorType *type) const;
    void reset();

protected:
    long m_adapter;
    DemodulatorType m_type;
};

Q_DECLARE_METATYPE(RadioReaderInterface::AdapterData)
QDataStream& operator << (QDataStream &out, const RadioReaderInterface::AdapterData &data);
QDataStream& operator >> (QDataStream &in, RadioReaderInterface::AdapterData &data);
