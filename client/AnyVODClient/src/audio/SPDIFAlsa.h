﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "SPDIFInterface.h"

#include <QVector>

#include <alsa/asoundlib.h>

class SPDIFAlsa : public SPDIFInterface
{
public:
    SPDIFAlsa();
    virtual ~SPDIFAlsa();

    virtual bool open();
    virtual void close();
    virtual bool checkSupport();

    virtual bool play();
    virtual bool pause();
    virtual bool resume();
    virtual bool stop();

    virtual bool fillBuffer(void *buffer, int size);
    virtual unsigned int getNeedBufferSize() const;

    virtual void getDeviceList(QStringList *ret);
    virtual int getDeviceCount() const;

    virtual double getLatency();
    virtual bool isSupportPull() const;
    virtual int getAlignSize() const;

    virtual bool canFillBufferBeforeStart() const;

private:
    bool updateDeviceList();
    bool setDeviceParams(uint bufferTime, uint periodTime);

    void closeInternal();

private:
    struct DeviceItem
    {
        QString name;
        QString desc;
    };

private:
    snd_pcm_t *m_handle;
    QString m_deviceName;
    QVector<DeviceItem> m_deviceList;
};
