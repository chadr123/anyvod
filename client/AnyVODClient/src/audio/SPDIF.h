﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "SPDIFEncoder.h"
#include "SPDIFInterface.h"

#include <QThread>

class SPDIF : public QThread
{
	Q_OBJECT
private:
    SPDIF();
    ~SPDIF();

public:
    static SPDIF& getInstance();

public:
    bool open(AVCodecID codecID, SPDIFInterface::Callback callback, int encodingSampleRate, int sampleRate, int channelCount,
              unsigned int bufferLen, void *user);
    void close();

    bool isOpened() const;
    bool isAvailable() const;

    bool play();
    bool pause();
    bool resume();
    bool stop();

    void setInterval(unsigned int msec);
    unsigned int getInterval() const;

    bool setBufferLength(unsigned int msec);
    unsigned int getBufferLength() const;

    void getErrorString(QString *ret) const;

    void getParams(const AVCodecContext *context, int *sampleRate, int *channelCount, AVSampleFormat *format) const;
    void setAudioBuffer(uint8_t *buf, int size);
    void setHDRate(int rate);
    int writePacket(AVPacket &packet);

    int getFailCount() const;
    bool isExceededFailCount() const;

    void getDeviceList(QStringList *ret);
    int getDeviceCount() const;

    bool setDevice(int device);
    int getDevice() const;

    bool getDeviceName(int device, QString *ret);

    double getLatency();

private:
    bool requestFillBuffer();
    int alignSize(int size) const;
    SPDIFInterface* checkSupport(SPDIFInterface *output) const;

private:
    virtual void run();

private:
    static const int MAX_FAIL_COUNT;

private:
    volatile bool m_quit;

private:
    SPDIFInterface *m_output;
    SPDIFInterface::Callback m_callback;
    bool m_isOpened;
    unsigned int m_interval;
    void *m_user;
    SPDIFEncoder m_spdifEncoder;
};
