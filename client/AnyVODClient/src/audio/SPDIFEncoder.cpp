﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "SPDIFEncoder.h"

extern "C"
{
# include <libavformat/avformat.h>
# include <libavutil/opt.h>
}

SPDIFEncoder::SPDIFEncoder() :
    m_format(nullptr),
    m_audioBuffer(nullptr),
    m_audioBufferSize(0),
    m_wroteSize(0),
    m_opened(false),
    m_failCount(0)
{

}

bool SPDIFEncoder::open(AVCodecID codecID)
{
    if (avformat_alloc_output_context2(&this->m_format, nullptr, "spdif", nullptr) < 0)
        return false;

    this->m_format->pb = avio_alloc_context(this->m_spdifBuffer, SPDIF_BUFFER_SIZE, 1, this,
                                            nullptr, write, nullptr);
    if (!this->m_format->pb)
        return false;

    AVStream *stream = avformat_new_stream(this->m_format, nullptr);

    if (!stream)
        return false;

    stream->codecpar->codec_id = codecID;

    if (AVERROR_PATCHWELCOME == avformat_write_header(this->m_format, nullptr))
        return false;

    this->m_opened = true;

    return true;
}

void SPDIFEncoder::close()
{
    if (this->m_format)
    {
        av_write_trailer(this->m_format);

        if (this->m_format->pb)
            av_freep(&this->m_format->pb);

        avformat_free_context(this->m_format);
    }

    this->m_format = nullptr;
    this->m_audioBuffer = nullptr;
    this->m_audioBufferSize = 0;
    this->m_wroteSize = 0;
    this->m_opened = false;
    this->m_failCount = 0;
}

void SPDIFEncoder::getParams(const AVCodecContext *context, int *sampleRate, int *channelCount, AVSampleFormat *format) const
{
    if (context->sample_rate < 44100)
    {
        *sampleRate = 48000;
    }
    else
    {
        switch (context->codec_id)
        {
            case AV_CODEC_ID_DTS:
            {
                switch (context->profile)
                {
                    case FF_PROFILE_DTS_HD_HRA:
                    case FF_PROFILE_DTS_HD_MA:
                        *sampleRate = 192000;
                        break;
                    default:
                        *sampleRate = context->sample_rate;
                        break;
                }

                break;
            }
            case AV_CODEC_ID_TRUEHD:
            case AV_CODEC_ID_MLP:
            {
                switch (context->sample_rate)
                {
                    case 48000:
                    case 96000:
                    case 192000:
                        *sampleRate = 192000;
                        break;
                    case 44100:
                    case 88200:
                    case 176400:
                        *sampleRate = 176400;
                        break;
                    default:
                        *sampleRate = context->sample_rate;
                        break;
                }

                break;
            }
            case AV_CODEC_ID_EAC3:
            {
                *sampleRate = context->sample_rate * 4;
                break;
            }
            default:
            {
                *sampleRate = context->sample_rate;
                break;
            }
        }
    }

    switch (context->codec_id)
    {
        case AV_CODEC_ID_DTS:
        {
            switch (context->profile)
            {
                case FF_PROFILE_DTS_HD_MA:
                    *channelCount = 8;
                    break;
                default:
                    *channelCount = 2;
                    break;
            }

            break;
        }
        case AV_CODEC_ID_TRUEHD:
        case AV_CODEC_ID_MLP:
        {
            *channelCount = 8;
            break;
        }
        default:
        {
            *channelCount = 2;
            break;
        }
    }

    *format = AV_SAMPLE_FMT_S16;
}

bool SPDIFEncoder::isOpened() const
{
    return this->m_opened;
}

void SPDIFEncoder::setAudioBuffer(uint8_t *buf, int size)
{
    this->m_audioBuffer = buf;
    this->m_audioBufferSize = size;
}

void SPDIFEncoder::setHDRate(int rate)
{
    av_opt_set_int(this->m_format->priv_data, "dtshd_rate", rate, 0);
}

int SPDIFEncoder::writePacket(AVPacket &packet)
{
    static int pts = 1;
    AVPacket pack;

    memcpy(&pack, &packet, sizeof(pack));

    pack.pts = pts++;
    this->m_wroteSize = 0;

    if (av_write_frame(this->m_format, &pack) < 0)
    {
        this->m_failCount++;
        return -1;
    }
    else
    {
        this->m_failCount = 0;
    }

    return this->m_wroteSize;
}

int SPDIFEncoder::getFailCount() const
{
    return this->m_failCount;
}

int SPDIFEncoder::write(void *opaque, uint8_t *buf, int bufSize)
{
    SPDIFEncoder *spdif = (SPDIFEncoder*)opaque;
    int len = FFMIN(bufSize, spdif->m_audioBufferSize);

    memcpy(spdif->m_audioBuffer, buf, len);
    spdif->m_wroteSize = len;

    return len;
}
