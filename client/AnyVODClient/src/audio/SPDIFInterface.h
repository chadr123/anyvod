﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <QString>
#include <QCoreApplication>

#define ERROR_TO_STR(err)      case err: { this->m_errorString = #err; break; }
#define UNKNOWN_ERROR_TO_STR   default:  { this->m_errorString = "unknown"; break; }

extern "C"
{
# include <libavcodec/avcodec.h>
}

class SPDIFInterface
{
    Q_DECLARE_TR_FUNCTIONS(SPDIFInterface)
public:
    typedef int (*Callback) (void *buffer, int length, void *user);

    SPDIFInterface();
    virtual ~SPDIFInterface();

    virtual bool open() = 0;
    virtual void close() = 0;
    virtual bool checkSupport() = 0;

    virtual bool play() = 0;
    virtual bool pause() = 0;
    virtual bool resume() = 0;
    virtual bool stop() = 0;

    virtual bool fillBuffer(void *buffer, int size) = 0;
    virtual unsigned int getNeedBufferSize() const = 0;

    virtual void getDeviceList(QStringList *ret) = 0;
    virtual int getDeviceCount() const = 0;

    virtual double getLatency() = 0;
    virtual bool isSupportPull() const = 0;
    virtual int getAlignSize() const = 0;

    virtual bool canFillBufferBeforeStart() const = 0;

    void setParams(AVCodecID codecID, int encodingSampleRate, int sampleRate, int channelCount);
    AVCodecID getCodecID() const;
    int getEncodingSampleRate() const;
    int getSampleRate() const;
    int getChannelCount() const;

    void setBufferLength(unsigned int msec);
    unsigned int getBufferLength() const;

    bool setDevice(int device);
    int getDevice() const;

    bool getDeviceName(int device, QString *ret);
    void setCallback(Callback callback, void *user);
    void getErrorString(QString *ret) const;
    unsigned int getBytesPerSecond() const;

    void setQuit(bool quit);

public:
    static const double PUSH_MULTIPLIER;

protected:
    volatile bool m_quit;

protected:
    int m_encodingSampleRate;
    int m_sampleRate;
    int m_channelCount;
    int m_bytesPerSample;
    QString m_errorString;
    unsigned int m_bufferLength;
    int m_device;
    Callback m_callback;
    void *m_user;
    AVCodecID m_codecID;
};
