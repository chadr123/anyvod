﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

extern "C"
{
# include <libavcodec/avcodec.h>
# include <libavutil/mem.h>
}

const int SPDIF_BUFFER_SIZE = 65536;

struct AVFormatContext;

class SPDIFEncoder
{
public:
    SPDIFEncoder();

    bool open(AVCodecID codecID);
    void close();

    void getParams(const AVCodecContext *context, int *sampleRate, int *channelCount, AVSampleFormat *format) const;

    bool isOpened() const;

    void setAudioBuffer(uint8_t *buf, int size);
    void setHDRate(int rate);
    int writePacket(AVPacket &packet);
    int getFailCount() const;

private:
    static int write(void *opaque, uint8_t *buf, int bufSize);

private:
    AVFormatContext *m_format;
    uint8_t *m_audioBuffer;
    int m_audioBufferSize;
    int m_wroteSize;
    DECLARE_ALIGNED(16, uint8_t, m_spdifBuffer[SPDIF_BUFFER_SIZE]);
    bool m_opened;
    int m_failCount;
};
