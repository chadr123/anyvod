﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "SPDIFWasAPI.h"
#include "utils/COMUtils.h"

#include <QDebug>

#include <mmdeviceapi.h>
#include <mmreg.h>
#include <ksmedia.h>

DEFINE_PROPERTYKEY(PKEY_Device_FriendlyName, 0xa45c254e, 0xdf1c, 0x4efd, 0x80, 0x20, 0x67, 0xd1, 0x46, 0xa8, 0x50, 0xe0, 14);
DEFINE_PROPERTYKEY(PKEY_Device_EnumeratorName, 0xa45c254e, 0xdf1c, 0x4efd, 0x80, 0x20, 0x67, 0xd1, 0x46, 0xa8, 0x50, 0xe0, 24);

DEFINE_GUID(KSDATAFORMAT_SUBTYPE_IEC61937_DOLBY_DIGITAL, WAVE_FORMAT_DOLBY_AC3_SPDIF, 0x0000, 0x0010, 0x80, 0x00, 0x00, 0xaa, 0x00, 0x38, 0x9b, 0x71);
DEFINE_GUID(KSDATAFORMAT_SUBTYPE_IEC61937_DTS, WAVE_FORMAT_DTS, 0x0000, 0x0010, 0x80, 0x00, 0x00, 0xaa, 0x00, 0x38, 0x9b, 0x71);
DEFINE_GUID(KSDATAFORMAT_SUBTYPE_IEC61937_DOLBY_DIGITAL_PLUS, 0x0000000aL, 0x0cea, 0x0010, 0x80, 0x00, 0x00, 0xaa, 0x00, 0x38, 0x9b, 0x71);
DEFINE_GUID(KSDATAFORMAT_SUBTYPE_IEC61937_DTS_HD, 0x0000000bL, 0x0cea, 0x0010, 0x80, 0x00, 0x00, 0xaa, 0x00, 0x38, 0x9b, 0x71);
DEFINE_GUID(KSDATAFORMAT_SUBTYPE_IEC61937_DOLBY_MLP, 0x0000000cL, 0x0cea, 0x0010, 0x80, 0x00, 0x00, 0xaa, 0x00, 0x38, 0x9b, 0x71);

SPDIFWasAPI::SPDIFWasAPI() :
    m_wasDevice(nullptr),
    m_audioClient(nullptr),
    m_renderClient(nullptr),
    m_needDataEvent(NULL),
    m_latency(0.0),
    m_bufferSizeInSamples(0)
{
    COMUtils::initCOM();
}

SPDIFWasAPI::~SPDIFWasAPI()
{
    this->closeInternal();
    COMUtils::unInitCOM();
}

bool SPDIFWasAPI::open()
{
    if (!this->createDevice())
    {
        this->close();
        return false;
    }

    if (!this->createAudioClient())
    {
        this->close();
        return false;
    }

    return true;
}

void SPDIFWasAPI::close()
{
    this->closeInternal();
}

bool SPDIFWasAPI::checkSupport()
{
    IMMDeviceEnumerator *enumerator = nullptr;
    HRESULT hr;

    hr = CoCreateInstance(CLSID_MMDeviceEnumerator, nullptr, CLSCTX_ALL, IID_IMMDeviceEnumerator, (void**)&enumerator);

    if (enumerator)
        enumerator->Release();

    return SUCCEEDED(hr);
}

bool SPDIFWasAPI::play()
{
    if (this->m_audioClient)
    {
        HRESULT hr;

        hr = this->m_audioClient->Start();

        return SUCCEEDED(hr);
    }

    return false;
}

bool SPDIFWasAPI::pause()
{
    if (this->m_audioClient)
    {
        HRESULT hr;

        hr = this->m_audioClient->Stop();

        return SUCCEEDED(hr);
    }

    return false;
}

bool SPDIFWasAPI::resume()
{
    return this->play();
}

bool SPDIFWasAPI::stop()
{
    return this->pause();
}

bool SPDIFWasAPI::fillBuffer(void *buffer, int size)
{
    HRESULT hr;
    BYTE *buf = nullptr;
    UINT32 samples = size / this->m_bytesPerSample;

    if (WaitForSingleObject(this->m_needDataEvent, 1100) != WAIT_OBJECT_0)
        return false;

    hr = this->m_renderClient->GetBuffer(samples, &buf);

    if (FAILED(hr))
        return false;

    memcpy(buf, buffer, size);

    hr = this->m_renderClient->ReleaseBuffer(samples, 0);

    if (FAILED(hr))
        return false;

    return true;
}

unsigned int SPDIFWasAPI::getNeedBufferSize() const
{
    return this->m_bufferSizeInSamples * this->m_bytesPerSample;
}

void SPDIFWasAPI::getDeviceList(QStringList *ret)
{
    QStringList list;

    this->updateDeviceList();

    for (const DeviceItem &item : qAsConst(this->m_deviceList))
        list.append(item.name);

    *ret = list;
}

int SPDIFWasAPI::getDeviceCount() const
{
    return this->m_deviceList.count();
}

double SPDIFWasAPI::getLatency()
{
    return this->m_latency;
}

bool SPDIFWasAPI::isSupportPull() const
{
    return false;
}

int SPDIFWasAPI::getAlignSize() const
{
    return 0;
}

bool SPDIFWasAPI::canFillBufferBeforeStart() const
{
    return false;
}

bool SPDIFWasAPI::updateDeviceList()
{
    class localComPtr
    {
    public:
        localComPtr() :
            m_enumerator(nullptr),
            m_enumDevices(nullptr)
        {

        }

        ~localComPtr()
        {
            if (this->m_enumDevices)
                this->m_enumDevices->Release();

            if (this->m_enumerator)
                this->m_enumerator->Release();
        }

        IMMDeviceEnumerator *m_enumerator;
        IMMDeviceCollection *m_enumDevices;
    }l;

    HRESULT hr;
    UINT count = 0;

    this->m_deviceList.clear();

    hr = CoCreateInstance(CLSID_MMDeviceEnumerator, nullptr, CLSCTX_ALL, IID_IMMDeviceEnumerator, (void**)&l.m_enumerator);

    if (FAILED(hr))
        return false;

    hr = l.m_enumerator->EnumAudioEndpoints(eRender, DEVICE_STATE_ACTIVE, &l.m_enumDevices);

    if (FAILED(hr))
        return false;

    hr = l.m_enumDevices->GetCount(&count);

    if (FAILED(hr))
        return false;

    for (UINT i = 0; i < count; i++)
    {
        class localComPtr
        {
        public:
            localComPtr() :
                m_device(nullptr),
                m_property(nullptr)
            {

            }

            ~localComPtr()
            {
                if (this->m_device)
                    this->m_device->Release();

                if (this->m_property)
                    this->m_property->Release();
            }

            IMMDevice *m_device;
            IPropertyStore *m_property;
        }ll;

        DeviceItem item;
        PROPVARIANT varName;

        PropVariantInit(&varName);

        hr = l.m_enumDevices->Item(i, &ll.m_device);

        if (FAILED(hr))
            return false;

        hr = ll.m_device->OpenPropertyStore(STGM_READ, &ll.m_property);

        if (FAILED(hr))
            return false;

        hr = ll.m_property->GetValue(PKEY_Device_FriendlyName, &varName);

        if (FAILED(hr))
            return false;

        item.name = QString::fromWCharArray(varName.pwszVal);
        PropVariantClear(&varName);

        hr = ll.m_property->GetValue(PKEY_AudioEndpoint_GUID, &varName);

        if (FAILED(hr))
            return false;

        item.guid = QString::fromWCharArray(varName.pwszVal);
        PropVariantClear(&varName);

        this->m_deviceList.append(item);
    }

    return true;
}

IMMDevice* SPDIFWasAPI::getDefaultDevice() const
{
    class localComPtr
    {
    public:
        localComPtr() :
            m_enumerator(nullptr)
        {

        }

        ~localComPtr()
        {
            if (this->m_enumerator)
                this->m_enumerator->Release();
        }

        IMMDeviceEnumerator *m_enumerator;
    }l;

    HRESULT hr;
    IMMDevice *device = nullptr;

    hr = CoCreateInstance(CLSID_MMDeviceEnumerator, nullptr, CLSCTX_ALL, IID_IMMDeviceEnumerator, (void**)&l.m_enumerator);

    if (FAILED(hr))
        return nullptr;

    hr = l.m_enumerator->GetDefaultAudioEndpoint(eRender, eConsole, &device);

    if (FAILED(hr))
        return nullptr;

    return device;
}

IMMDevice* SPDIFWasAPI::findDevice(const QString &guid) const
{
    class localComPtr
    {
    public:
        localComPtr() :
            m_enumerator(nullptr),
            m_enumDevices(nullptr)
        {

        }

        ~localComPtr()
        {
            if (this->m_enumDevices)
                this->m_enumDevices->Release();

            if (this->m_enumerator)
                this->m_enumerator->Release();
        }

        IMMDeviceEnumerator *m_enumerator;
        IMMDeviceCollection *m_enumDevices;
    }l;

    HRESULT hr;
    UINT count = 0;

    hr = CoCreateInstance(CLSID_MMDeviceEnumerator, nullptr, CLSCTX_ALL, IID_IMMDeviceEnumerator, (void**)&l.m_enumerator);

    if (FAILED(hr))
        return nullptr;

    hr = l.m_enumerator->EnumAudioEndpoints(eRender, DEVICE_STATE_ACTIVE, &l.m_enumDevices);

    if (FAILED(hr))
        return nullptr;

    hr = l.m_enumDevices->GetCount(&count);

    if (FAILED(hr))
        return nullptr;

    for (UINT i = 0; i < count; i++)
    {
        class localComPtr
        {
        public:
            localComPtr() :
                m_device(nullptr),
                m_property(nullptr)
            {

            }

            ~localComPtr()
            {
                if (this->m_device)
                    this->m_device->Release();

                if (this->m_property)
                    this->m_property->Release();
            }

            IMMDevice *m_device;
            IPropertyStore *m_property;
        }ll;

        PROPVARIANT varName;

        PropVariantInit(&varName);

        hr = l.m_enumDevices->Item(i, &ll.m_device);

        if (FAILED(hr))
            return nullptr;

        hr = ll.m_device->OpenPropertyStore(STGM_READ, &ll.m_property);

        if (FAILED(hr))
            return nullptr;

        hr = ll.m_property->GetValue(PKEY_AudioEndpoint_GUID, &varName);

        if (FAILED(hr))
            return nullptr;

        QString foundGuid = QString::fromWCharArray(varName.pwszVal);

        PropVariantClear(&varName);

        if (foundGuid == guid)
        {
            IMMDevice *foundDevice = ll.m_device;

            ll.m_device = nullptr;

            return foundDevice;
        }
    }

    return nullptr;
}

void SPDIFWasAPI::makeWaveFormat(WAVEFORMATEXTENSIBLE_IEC61937 *ret) const
{
    WAVEFORMATEXTENSIBLE &wfxex = ret->FormatExt;

    wfxex.Format.wFormatTag = WAVE_FORMAT_EXTENSIBLE;
    wfxex.Format.cbSize = sizeof(WAVEFORMATEXTENSIBLE) - sizeof(WAVEFORMATEX);
    wfxex.Format.wBitsPerSample = 16;
    wfxex.Format.nChannels = this->m_channelCount;
    wfxex.Format.nSamplesPerSec = this->m_sampleRate;
    wfxex.Format.nBlockAlign = wfxex.Format.nChannels * (wfxex.Format.wBitsPerSample >> 3);
    wfxex.Format.nAvgBytesPerSec = wfxex.Format.nSamplesPerSec * wfxex.Format.nBlockAlign;

    switch (this->m_codecID)
    {
        case AV_CODEC_ID_EAC3:
        {
            wfxex.SubFormat = KSDATAFORMAT_SUBTYPE_IEC61937_DOLBY_DIGITAL_PLUS;
            break;
        }
        case AV_CODEC_ID_DTS:
        {
            if (this->m_channelCount == 8)
                wfxex.SubFormat = KSDATAFORMAT_SUBTYPE_IEC61937_DTS_HD;
            else
                wfxex.SubFormat = KSDATAFORMAT_SUBTYPE_IEC61937_DTS;

            break;
        }
        case AV_CODEC_ID_AC3:
        {
            wfxex.SubFormat = KSDATAFORMAT_SUBTYPE_IEC61937_DOLBY_DIGITAL;
            break;
        }
        case AV_CODEC_ID_TRUEHD:
        case AV_CODEC_ID_MLP:
        {
            wfxex.SubFormat = KSDATAFORMAT_SUBTYPE_IEC61937_DOLBY_MLP;
            break;
        }
        default:
        {
            wfxex.SubFormat = KSDATAFORMAT_SUBTYPE_IEC61937_DOLBY_DIGITAL;
            break;
        }
    }

    if (this->m_channelCount == 2)
        wfxex.dwChannelMask = KSAUDIO_SPEAKER_STEREO;
    else if (this->m_channelCount == 8)
        wfxex.dwChannelMask = KSAUDIO_SPEAKER_7POINT1_SURROUND;
    else
        wfxex.dwChannelMask = KSAUDIO_SPEAKER_5POINT1;

    wfxex.Samples.wValidBitsPerSample = wfxex.Format.wBitsPerSample;

    ret->dwEncodedChannelCount = wfxex.Format.nChannels;
    ret->dwEncodedSamplesPerSec = this->m_encodingSampleRate;
}

bool SPDIFWasAPI::isUSBDevice() const
{
    IPropertyStore *property = nullptr;
    PROPVARIANT varName;
    HRESULT hr;
    bool ret = false;

    PropVariantInit(&varName);

    hr = this->m_wasDevice->OpenPropertyStore(STGM_READ, &property);

    if (FAILED(hr))
        return false;

    hr = property->GetValue(PKEY_Device_EnumeratorName, &varName);

    if (FAILED(hr))
        return false;

    if (QString::fromWCharArray(varName.pwszVal).toUpper() == "USB")
        ret = true;
    else
        ret = false;

    PropVariantClear(&varName);

    if (property)
        property->Release();

    return ret;
}

bool SPDIFWasAPI::createDevice()
{
    if (this->m_device == -1)
        this->m_wasDevice = this->getDefaultDevice();
    else
        this->m_wasDevice = this->findDevice(this->m_deviceList[this->m_device].guid);

    return this->m_wasDevice != nullptr;
}

void SPDIFWasAPI::deleteDevice()
{
    if (this->m_wasDevice)
    {
        this->m_wasDevice->Release();
        this->m_wasDevice = nullptr;
    }
}

bool SPDIFWasAPI::createAudioClient()
{
    HRESULT hr;

    hr = this->m_wasDevice->Activate(IID_IAudioClient, CLSCTX_ALL, nullptr, (void**)&this->m_audioClient);

    if (FAILED(hr))
        return false;

    WAVEFORMATEXTENSIBLE_IEC61937 format;

    this->makeWaveFormat(&format);
    this->m_bytesPerSample = format.FormatExt.Format.nBlockAlign;

    hr = this->m_audioClient->IsFormatSupported(AUDCLNT_SHAREMODE_EXCLUSIVE, &format.FormatExt.Format, nullptr);

    if (FAILED(hr))
        return false;

    REFERENCE_TIME audioBufferDuration = this->m_bufferLength * 1000ll;

    if (this->isUSBDevice())
        audioBufferDuration *= 2;

    audioBufferDuration = (REFERENCE_TIME)((audioBufferDuration / this->m_bytesPerSample) * this->m_bytesPerSample);

    hr = this->m_audioClient->Initialize(AUDCLNT_SHAREMODE_EXCLUSIVE, AUDCLNT_STREAMFLAGS_EVENTCALLBACK | AUDCLNT_STREAMFLAGS_NOPERSIST,
                                         audioBufferDuration, audioBufferDuration, &format.FormatExt.Format, nullptr);

    if (hr == AUDCLNT_E_BUFFER_SIZE_NOT_ALIGNED)
    {
        hr = this->m_audioClient->GetBufferSize(&this->m_bufferSizeInSamples);

        if (FAILED(hr))
            return false;

        this->deleteAudioClient();

        hr = this->m_wasDevice->Activate(IID_IAudioClient, CLSCTX_ALL, nullptr, (void**)&this->m_audioClient);

        if (FAILED(hr))
            return false;

        audioBufferDuration = (REFERENCE_TIME) ((10000.0 * 1000 / format.FormatExt.Format.nSamplesPerSec * this->m_bufferSizeInSamples) + 0.5);
        hr = this->m_audioClient->Initialize(AUDCLNT_SHAREMODE_EXCLUSIVE, AUDCLNT_STREAMFLAGS_EVENTCALLBACK | AUDCLNT_STREAMFLAGS_NOPERSIST,
                                             audioBufferDuration, audioBufferDuration, &format.FormatExt.Format, nullptr);

        if (FAILED(hr))
            return false;

        this->m_bufferLength = audioBufferDuration / 1000ll;
    }

    this->m_latency = -audioBufferDuration * 0.000001;

    hr = this->m_audioClient->GetBufferSize(&this->m_bufferSizeInSamples);

    if (FAILED(hr))
        return false;

    hr = this->m_audioClient->GetService(IID_IAudioRenderClient, (void**)&this->m_renderClient);

    if (FAILED(hr))
        return false;

    this->m_needDataEvent = CreateEvent(nullptr, FALSE, FALSE, nullptr);
    hr = this->m_audioClient->SetEventHandle(this->m_needDataEvent);

    if (FAILED(hr))
        return false;

    hr = this->m_audioClient->Reset();

    if (FAILED(hr))
        return false;

    return true;
}

void SPDIFWasAPI::deleteAudioClient()
{
    if (this->m_renderClient)
    {
        this->m_renderClient->Release();
        this->m_renderClient = nullptr;
    }

    if (this->m_audioClient)
    {
        this->m_audioClient->Stop();
        this->m_audioClient->Reset();
        this->m_audioClient->Release();

        this->m_audioClient = nullptr;
    }

    if(this->m_needDataEvent != NULL)
    {
        CloseHandle(this->m_needDataEvent);
        this->m_needDataEvent = NULL;
    }
}

void SPDIFWasAPI::closeInternal()
{
    this->deleteAudioClient();
    this->deleteDevice();

    this->m_latency = 0.0;
    this->m_bufferSizeInSamples = 0;
}

void SPDIFWasAPI::errorToString(const HRESULT error)
{
    switch (error)
    {
        ERROR_TO_STR(AUDCLNT_E_NOT_INITIALIZED);
        ERROR_TO_STR(AUDCLNT_E_ALREADY_INITIALIZED);
        ERROR_TO_STR(AUDCLNT_E_WRONG_ENDPOINT_TYPE);
        ERROR_TO_STR(AUDCLNT_E_DEVICE_INVALIDATED);
        ERROR_TO_STR(AUDCLNT_E_NOT_STOPPED);
        ERROR_TO_STR(AUDCLNT_E_BUFFER_TOO_LARGE);
        ERROR_TO_STR(AUDCLNT_E_OUT_OF_ORDER);
        ERROR_TO_STR(AUDCLNT_E_UNSUPPORTED_FORMAT);
        ERROR_TO_STR(AUDCLNT_E_INVALID_SIZE);
        ERROR_TO_STR(AUDCLNT_E_DEVICE_IN_USE);
        ERROR_TO_STR(AUDCLNT_E_BUFFER_OPERATION_PENDING);
        ERROR_TO_STR(AUDCLNT_E_THREAD_NOT_REGISTERED);
        ERROR_TO_STR(AUDCLNT_E_EXCLUSIVE_MODE_NOT_ALLOWED);
        ERROR_TO_STR(AUDCLNT_E_ENDPOINT_CREATE_FAILED);
        ERROR_TO_STR(AUDCLNT_E_SERVICE_NOT_RUNNING);
        ERROR_TO_STR(AUDCLNT_E_EVENTHANDLE_NOT_EXPECTED);
        ERROR_TO_STR(AUDCLNT_E_EXCLUSIVE_MODE_ONLY);
        ERROR_TO_STR(AUDCLNT_E_BUFDURATION_PERIOD_NOT_EQUAL);
        ERROR_TO_STR(AUDCLNT_E_EVENTHANDLE_NOT_SET);
        ERROR_TO_STR(AUDCLNT_E_INCORRECT_BUFFER_SIZE);
        ERROR_TO_STR(AUDCLNT_E_BUFFER_SIZE_ERROR);
        ERROR_TO_STR(AUDCLNT_E_CPUUSAGE_EXCEEDED);
        ERROR_TO_STR(AUDCLNT_E_BUFFER_ERROR);
        ERROR_TO_STR(AUDCLNT_E_BUFFER_SIZE_NOT_ALIGNED);
        ERROR_TO_STR(AUDCLNT_E_INVALID_DEVICE_PERIOD);
        ERROR_TO_STR(E_POINTER);
        ERROR_TO_STR(E_INVALIDARG);
        ERROR_TO_STR(E_OUTOFMEMORY);
        UNKNOWN_ERROR_TO_STR
    }
}
