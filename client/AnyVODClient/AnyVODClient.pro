#-------------------------------------------------
#
# Project created by QtCreator 2011-03-21T11:00:54
#
#-------------------------------------------------

QT += core gui concurrent widgets opengl network xml qml

win32: QT += winextras

mac: CONFIG += sdk_no_version_check

TARGET = AnyVODClient
TEMPLATE = app

CODECFORTR = UTF-8

cache()

SOURCES += \
    ../../common/SubtitleFileNameGenerator.cpp \
    ../../common/RetreiveSubtitle.cpp \
    ../../common/MemBuffer.cpp \
    src/audio/SPDIFSampleRates.cpp \
    src/core/EnumsTranslator.cpp \
    src/core/Environment.cpp \
    src/core/FileAssociationInstaller.cpp \
    src/core/FileIcons.cpp \
    src/core/PlayListInjector.cpp \
    src/core/SystemResourceUsage.cpp \
    src/core/Version.cpp \
    src/core/VersionDescription.cpp \
    src/core/main.cpp \
    src/core/Common.cpp \
    src/core/AnyVOD.cpp \
    src/core/TextEncodingDetector.cpp \
    src/core/FileExtensions.cpp \
    src/delegate/DelegateBase.cpp \
    src/delegate/MediaPlayerDelegate.cpp \
    src/delegate/ScreenDelegate.cpp \
    src/delegate/ShaderCompositerDelegate.cpp \
    src/delegate/UpdaterDelegate.cpp \
    src/factories/MainWindowFactory.cpp \
    src/language/GlobalLanguage.cpp \
    src/language/LanguageInstaller.cpp \
    src/media/AudioRenderer.cpp \
    src/media/MediaPresenterInterface.cpp \
    src/media/MediaThreadBase.cpp \
    src/media/Surface.cpp \
    src/media/VideoFrameQueue.cpp \
    src/media/VideoPicture.cpp \
    src/media/VideoRenderer.cpp \
    src/net/NetworkProxy.cpp \
    src/parsers/subtitle/GlobalSubtitleCodecName.cpp \
    src/screensaver/ScreenSaverPreventer.cpp \
    src/screensaver/ScreenSaverPreventerInterface.cpp \
    src/setting/DTVReaderSettingBase.cpp \
    src/setting/DTVReaderSettingLoader.cpp \
    src/setting/DTVReaderSettingSaver.cpp \
    src/setting/GlobalSettingBase.cpp \
    src/setting/MainWindowSettingBase.cpp \
    src/setting/MainWindowSettingLoader.cpp \
    src/setting/MainWindowSettingSaver.cpp \
    src/setting/MediaPlayerSettingBase.cpp \
    src/setting/MediaPlayerSettingLoader.cpp \
    src/setting/MediaPlayerSettingSaver.cpp \
    src/setting/PlayListSettingBase.cpp \
    src/setting/PlayListSettingLoader.cpp \
    src/setting/PlayListSettingSaver.cpp \
    src/setting/RadioReaderSettingBase.cpp \
    src/setting/RadioReaderSettingLoader.cpp \
    src/setting/RadioReaderSettingSaver.cpp \
    src/setting/ScreenSettingBase.cpp \
    src/setting/ScreenSettingLoader.cpp \
    src/setting/ScreenSettingSaver.cpp \
    src/setting/SettingBackupAndRestore.cpp \
    src/setting/Settings.cpp \
    src/setting/SettingsFactory.cpp \
    src/setting/SettingsSynchronizer.cpp \
    src/setting/ShortcutKeySettingBase.cpp \
    src/setting/ShortcutKeySettingLoader.cpp \
    src/setting/ShortcutKeySettingSaver.cpp \
    src/decoders/HWDecoderInterface.cpp \
    src/decoders/HWDecoder.cpp \
    src/media/VideoThread.cpp \
    src/media/SubtitleThread.cpp \
    src/media/ReadThread.cpp \
    src/media/PacketQueue.cpp \
    src/media/MediaPresenter.cpp \
    src/media/MediaPlayer.cpp \
    src/media/LastPlay.cpp \
    src/media/FrameExtractor.cpp \
    src/media/RefreshThread.cpp \
    src/net/VirtualFile.cpp \
    src/net/SyncHttp.cpp \
    src/net/SubtitleImpl.cpp \
    src/net/Socket.cpp \
    src/net/NetFile.cpp \
    src/net/HttpDownloader.cpp \
    src/net/Updater.cpp \
    src/parsers/playlist/WPLParser.cpp \
    src/parsers/playlist/PLSParser.cpp \
    src/parsers/playlist/PlayListParserInterface.cpp \
    src/parsers/playlist/PlayListParserGenerator.cpp \
    src/parsers/playlist/M3UParser.cpp \
    src/parsers/playlist/CueParser.cpp \
    src/parsers/playlist/B4SParser.cpp \
    src/parsers/playlist/ASXParser.cpp \
    src/parsers/subtitle/SRTParser.cpp \
    src/parsers/subtitle/SAMIParser.cpp \
    src/parsers/subtitle/LRCParser.cpp \
    src/parsers/subtitle/AVParser.cpp \
    src/parsers/subtitle/ASSParser.cpp \
    src/parsers/subtitle/YouTubeParser.cpp \
    src/parsers/subtitle/NaverTVParser.cpp \
    src/ui/ListWidgetDialog.cpp \
    src/ui/MainWindowInterface.cpp \
    src/ui/OpenRadioScanChannel.cpp \
    src/ui/ScreenContextMenu.cpp \
    src/ui/UserAspectRatio.cpp \
    src/ui/Trackbar.cpp \
    src/ui/TimeEdit.cpp \
    src/ui/Spectrum.cpp \
    src/ui/Slider.cpp \
    src/ui/SkipRange.cpp \
    src/ui/ShortcutEdit.cpp \
    src/ui/Shader.cpp \
    src/ui/Screen.cpp \
    src/ui/RemoteFileList.cpp \
    src/ui/Popup.cpp \
    src/ui/PlayListUpdater.cpp \
    src/ui/PlayList.cpp \
    src/ui/OpenExternal.cpp \
    src/ui/MultipleCapture.cpp \
    src/ui/MainWindow.cpp \
    src/ui/Login.cpp \
    src/ui/FileAssociation.cpp \
    src/ui/Equalizer.cpp \
    src/ui/CustomShortcut.cpp \
    src/ui/TextEncodeSetting.cpp \
    src/ui/ScreenExplorer.cpp \
    src/ui/ScreenExplorerItem.cpp \
    src/ui/ScreenExplorerUpdater.cpp \
    src/ui/ServerSetting.cpp \
    src/ui/SubtitleDirectory.cpp \
    src/ui/OpenDevice.cpp \
    src/ui/OpenDTVScanChannel.cpp \
    src/ui/UserAspectRatios.cpp \
    src/ui/ViewEPG.cpp \
    src/ui/EPGItem.cpp \
    src/ui/ElidedLabel.cpp \
    src/ui/ShortcutKey.cpp \
    src/utils/ChapterInfoUtils.cpp \
    src/utils/ConvertingUtils.cpp \
    src/utils/DeviceUtils.cpp \
    src/utils/FileListUtils.cpp \
    src/utils/FloatPointUtils.cpp \
    src/utils/FrameUtils.cpp \
    src/utils/MathUtils.cpp \
    src/utils/MediaTypeUtils.cpp \
    src/utils/MessageBoxUtils.cpp \
    src/utils/PathUtils.cpp \
    src/utils/PixelUtils.cpp \
    src/utils/PlayItemUtils.cpp \
    src/utils/RemoteFileUtils.cpp \
    src/utils/RotationUtils.cpp \
    src/utils/SeparatingUtils.cpp \
    src/utils/StringUtils.cpp \
    src/utils/TimeUtils.cpp \
    src/video/AnaglyphMatrix.cpp \
    src/video/BaseShaderPrograms.cpp \
    src/video/ColorConversion.cpp \
    src/video/ScreenCaptureHelper.cpp \
    src/video/USWCMemCopy.cpp \
    src/video/Font.cpp \
    src/video/Deinterlacer.cpp \
    src/video/ShaderCompositer.cpp \
    src/video/FilterGraph.cpp \
    src/video/Camera.cpp \
    src/video/Sphere.cpp \
    src/video/Cube.cpp \
    src/video/PrimaryMatrix.cpp \
    src/audio/SPDIF.cpp \
    src/audio/SPDIFEncoder.cpp \
    src/audio/SPDIFInterface.cpp \
    src/device/CaptureInfo.cpp \
    src/device/CaptureInfoInterface.cpp \
    src/device/DTVReader.cpp \
    src/device/DTVReaderInterface.cpp \
    src/device/DTVChannelMap.cpp \
    src/device/RadioReader.cpp \
    src/device/RadioReaderInterface.cpp \
    src/device/RadioChannelMap.cpp \
    src/pickers/URLPicker.cpp \
    src/pickers/URLPickerInterface.cpp \
    src/pickers/YouTubeURLPicker.cpp \
    src/pickers/KakaoTVURLPicker.cpp \
    src/pickers/NaverTVURLPicker.cpp \
    src/pickers/TwitchURLPicker.cpp

win32: SOURCES += \
    src/decoders/DXVA2Decoder.cpp \
    src/decoders/NVDecoder.cpp \
    src/audio/SPDIFWasAPI.cpp \
    src/audio/SPDIFDSound.cpp \
    src/device/CaptureInfoDShow.cpp \
    src/device/DTVDShow.cpp \
    src/utils/COMUtils.cpp \
    src/screensaver/WinPreventer.cpp

mac: SOURCES += \
    src/decoders/VideoToolBoxDecoder.cpp \
    src/audio/SPDIFCoreAudio.cpp \
    src/screensaver/MacPreventer.cpp

OBJECTIVE_SOURCES += \
    src/utils/MacUtils.mm \
    src/device/AppleMediaKey.mm \
    src/device/AppleMediaKeyWrapper.mm \
    src/device/AppleRemote.mm \
    src/device/AppleRemoteWrapper.mm \
    src/device/CaptureInfoAVFoundation.mm

linux-*: SOURCES += \
    src/decoders/VAAPIDecoder.cpp \
    src/decoders/VDPAUDecoder.cpp \
    src/decoders/NVDecoder.cpp \
    src/audio/SPDIFAlsa.cpp \
    src/device/CaptureInfoV4L2.cpp \
    src/device/DTVLinuxDVB.cpp \
    src/device/radio/RadioGNURadio.cpp \
    src/device/radio/gnuradio/RadioReceiverInterface.cpp \
    src/device/radio/gnuradio/RadioReceiverWideBandFM.cpp \
    src/device/radio/gnuradio/blocks/RadioAudioSink.cpp \
    src/device/radio/gnuradio/blocks/RadioFIRDecimator.cpp \
    src/device/radio/gnuradio/blocks/RadioDownConverter.cpp \
    src/device/radio/gnuradio/blocks/RadioResamplerBase.cpp \
    src/device/radio/gnuradio/blocks/RadioComplexResampler.cpp \
    src/device/radio/gnuradio/blocks/RadioFloatPointResampler.cpp \
    src/device/radio/gnuradio/blocks/RadioBandPassFilter.cpp \
    src/device/radio/gnuradio/blocks/RadioFMDemodulator.cpp \
    src/device/radio/gnuradio/blocks/RadioFMDeemphasis.cpp \
    src/device/radio/gnuradio/blocks/RadioStereoDemodulator.cpp \
    src/device/radio/gnuradio/blocks/RadioLowPassFilter.cpp \
    src/screensaver/LinuxPreventer.cpp

HEADERS += \
    ../../common/SubtitleFileNameGenerator.h \
    ../../common/types.h \
    ../../common/RetreiveSubtitle.h \
    ../../common/packets.h \
    ../../common/packet_types.h \
    ../../common/size.h \
    ../../common/network.h \
    ../../common/size_types.h \
    ../../common/MemBuffer.h \
    src/audio/SPDIFSampleRates.h \
    src/core/AnyVODEnums.h \
    src/core/ChapterInfo.h \
    src/core/EnumsTranslator.h \
    src/core/Environment.h \
    src/core/ExtraPlayData.h \
    src/core/FileAssociationInstaller.h \
    src/core/FileIcons.h \
    src/core/FrameMetaData.h \
    src/core/Lyrics.h \
    src/core/MediaType.h \
    src/core/PlayItem.h \
    src/core/PlayListInjector.h \
    src/core/RemoteServerInformation.h \
    src/core/StreamInfo.h \
    src/core/SystemResourceUsage.h \
    src/core/TextureInfo.h \
    src/core/Common.h \
    src/core/BuildNumber.h \
    src/core/AnyVOD.h \
    src/core/TextEncodingDetector.h \
    src/core/UserAspectRatioInfo.h \
    src/core/Version.h \
    src/core/VersionDescription.h \
    src/core/FileExtensions.h \
    src/delegate/DelegateBase.h \
    src/delegate/MediaPlayerDelegate.h \
    src/delegate/ScreenDelegate.h \
    src/delegate/ShaderCompositerDelegate.h \
    src/delegate/UpdaterDelegate.h \
    src/factories/MainWindowFactory.h \
    src/language/GlobalLanguage.h \
    src/language/LanguageInstaller.h \
    src/media/AudioRenderer.h \
    src/media/Callbacks.h \
    src/media/Concurrent.h \
    src/media/Detail.h \
    src/media/MediaPresenterInterface.h \
    src/media/MediaThreadBase.h \
    src/media/Surface.h \
    src/media/VideoFrameQueue.h \
    src/media/VideoPicture.h \
    src/media/VideoRenderer.h \
    src/net/NetworkProxy.h \
    src/parsers/subtitle/GlobalSubtitleCodecName.h \
    src/screensaver/ScreenSaverPreventer.h \
    src/screensaver/ScreenSaverPreventerInterface.h \
    src/setting/DTVReaderSettingBase.h \
    src/setting/DTVReaderSettingLoader.h \
    src/setting/DTVReaderSettingSaver.h \
    src/setting/GlobalSettingBase.h \
    src/setting/MainWindowSettingBase.h \
    src/setting/MainWindowSettingLoader.h \
    src/setting/MainWindowSettingSaver.h \
    src/setting/MediaPlayerSettingBase.h \
    src/setting/MediaPlayerSettingLoader.h \
    src/setting/MediaPlayerSettingSaver.h \
    src/setting/PlayListSettingBase.h \
    src/setting/PlayListSettingLoader.h \
    src/setting/PlayListSettingSaver.h \
    src/setting/RadioReaderSettingBase.h \
    src/setting/RadioReaderSettingLoader.h \
    src/setting/RadioReaderSettingSaver.h \
    src/setting/ScreenSettingBase.h \
    src/setting/ScreenSettingLoader.h \
    src/setting/ScreenSettingSaver.h \
    src/setting/SettingBackupAndRestore.h \
    src/setting/Settings.h \
    src/setting/SettingsFactory.h \
    src/setting/SettingsSynchronizer.h \
    src/setting/ShortcutKeySettingBase.h \
    src/setting/ShortcutKeySettingLoader.h \
    src/setting/ShortcutKeySettingSaver.h \
    src/decoders/HWDecoderInterface.h \
    src/decoders/HWDecoder.h \
    src/media/VideoThread.h \
    src/media/SyncType.h \
    src/media/SubtitleThread.h \
    src/media/ReadThread.h \
    src/media/PacketQueue.h \
    src/media/MediaState.h \
    src/media/MediaPresenter.h \
    src/media/MediaPlayer.h \
    src/media/LastPlay.h \
    src/media/FrameExtractor.h \
    src/media/RefreshThread.h \
    src/net/VirtualFile.h \
    src/net/SyncHttp.h \
    src/net/SubtitleImpl.h \
    src/net/Socket.h \
    src/net/NetFile.h \
    src/net/HttpDownloader.h \
    src/net/Updater.h \
    src/parsers/playlist/WPLParser.h \
    src/parsers/playlist/PLSParser.h \
    src/parsers/playlist/PlayListParserInterface.h \
    src/parsers/playlist/PlayListParserGenerator.h \
    src/parsers/playlist/M3UParser.h \
    src/parsers/playlist/CueParser.h \
    src/parsers/playlist/B4SParser.h \
    src/parsers/playlist/ASXParser.h \
    src/parsers/subtitle/SRTParser.h \
    src/parsers/subtitle/SAMIParser.h \
    src/parsers/subtitle/LRCParser.h \
    src/parsers/subtitle/AVParser.h \
    src/parsers/subtitle/ASSParser.h \
    src/parsers/subtitle/YouTubeParser.h \
    src/parsers/subtitle/NaverTVParser.h \
    src/ui/ListWidgetDialog.h \
    src/ui/MainWindowInterface.h \
    src/ui/OpenRadioScanChannel.h \
    src/ui/ScreenContextMenu.h \
    src/ui/UserAspectRatio.h \
    src/ui/Trackbar.h \
    src/ui/TimeEdit.h \
    src/ui/Spectrum.h \
    src/ui/Slider.h \
    src/ui/SkipRange.h \
    src/ui/ShortcutEdit.h \
    src/ui/Shader.h \
    src/ui/Screen.h \
    src/ui/RemoteFileList.h \
    src/ui/Popup.h \
    src/ui/PlayListUpdater.h \
    src/ui/PlayList.h \
    src/ui/OpenExternal.h \
    src/ui/MultipleCapture.h \
    src/ui/MainWindow.h \
    src/ui/Login.h \
    src/ui/FileAssociation.h \
    src/ui/Equalizer.h \
    src/ui/CustomShortcut.h \
    src/ui/TextEncodeSetting.h \
    src/ui/ScreenExplorer.h \
    src/ui/ScreenExplorerItem.h \
    src/ui/ScreenExplorerUpdater.h \
    src/ui/ServerSetting.h \
    src/ui/SubtitleDirectory.h \
    src/ui/OpenDevice.h \
    src/ui/OpenDTVScanChannel.h \
    src/ui/UserAspectRatios.h \
    src/ui/ViewEPG.h \
    src/ui/EPGItem.h \
    src/ui/ElidedLabel.h \
    src/ui/ShortcutKey.h \
    src/utils/ChapterInfoUtils.h \
    src/utils/ConvertingUtils.h \
    src/utils/DeviceUtils.h \
    src/utils/FileListUtils.h \
    src/utils/FloatPointUtils.h \
    src/utils/FrameUtils.h \
    src/utils/MathUtils.h \
    src/utils/MediaTypeUtils.h \
    src/utils/MessageBoxUtils.h \
    src/utils/PathUtils.h \
    src/utils/PixelUtils.h \
    src/utils/PlayItemUtils.h \
    src/utils/RemoteFileUtils.h \
    src/utils/RotationUtils.h \
    src/utils/SeparatingUtils.h \
    src/utils/StringUtils.h \
    src/utils/TimeUtils.h \
    src/video/AnaglyphMatrix.h \
    src/video/BaseShaderPrograms.h \
    src/video/ColorConversion.h \
    src/video/ScreenCaptureHelper.h \
    src/video/USWCMemCopy.h \
    src/video/Font.h \
    src/video/Deinterlacer.h \
    src/video/ShaderCompositer.h \
    src/video/FilterGraph.h \
    src/video/Camera.h \
    src/video/Sphere.h \
    src/video/Cube.h \
    src/video/PrimaryMatrix.h \
    src/audio/SPDIF.h \
    src/audio/SPDIFEncoder.h \
    src/audio/SPDIFInterface.h \
    src/device/CaptureInfo.h \
    src/device/CaptureInfoInterface.h \
    src/device/DTVReader.h \
    src/device/DTVReaderInterface.h \
    src/device/DTVChannelMap.h \
    src/device/RadioReader.h \
    src/device/RadioReaderInterface.h \
    src/device/RadioChannelMap.h \
    src/pickers/URLPicker.h \
    src/pickers/URLPickerInterface.h \
    src/pickers/YouTubeURLPicker.h \
    src/pickers/KakaoTVURLPicker.h \
    src/pickers/NaverTVURLPicker.h \
    src/pickers/TwitchURLPicker.h \

win32: HEADERS += \
    src/decoders/DXVA2Decoder.h \
    src/decoders/NVDecoder.h \
    src/audio/SPDIFWasAPIDefs.h \
    src/audio/SPDIFWasAPI.h \
    src/audio/SPDIFDSound.h \
    src/device/CaptureInfoDShow.h \
    src/device/DTVDShow.h \
    src/device/DTVDShowDefs.h \
    src/utils/COMUtils.h \
    src/screensaver/WinPreventer.h

mac: HEADERS += \
    src/utils/MacUtils.h \
    src/decoders/VideoToolBoxDecoder.h \
    src/audio/SPDIFCoreAudio.h \
    src/device/AppleMediaKey.h \
    src/device/AppleMediaKeyWrapper.h \
    src/device/AppleRemote.h \
    src/device/AppleRemoteWrapper.h \
    src/device/CaptureInfoAVFoundation.h \
    src/screensaver/MacPreventer.h

linux-*: HEADERS += \
    src/decoders/VAAPIDecoder.h \
    src/decoders/VDPAUDecoder.h \
    src/decoders/NVDecoder.h \
    src/audio/SPDIFAlsa.h \
    src/device/CaptureInfoV4L2.h \
    src/device/DTVLinuxDVB.h \
    src/device/radio/RadioGNURadio.h \
    src/device/radio/gnuradio/RadioReceiverInterface.h \
    src/device/radio/gnuradio/RadioReceiverWideBandFM.h \
    src/device/radio/gnuradio/blocks/RadioAudioSink.h \
    src/device/radio/gnuradio/blocks/RadioFIRDecimator.h \
    src/device/radio/gnuradio/blocks/RadioDownConverter.h \
    src/device/radio/gnuradio/blocks/RadioResamplerBase.h \
    src/device/radio/gnuradio/blocks/RadioComplexResampler.h \
    src/device/radio/gnuradio/blocks/RadioFloatPointResampler.h \
    src/device/radio/gnuradio/blocks/RadioBandPassFilter.h \
    src/device/radio/gnuradio/blocks/RadioFMDemodulator.h \
    src/device/radio/gnuradio/blocks/RadioFMDeemphasis.h \
    src/device/radio/gnuradio/blocks/RadioStereoDemodulator.h \
    src/device/radio/gnuradio/blocks/RadioLowPassFilter.h \
    src/screensaver/LinuxPreventer.h

FORMS += \
    forms/openradioscanchannel.ui \
    forms/useraspectratio.ui \
    forms/skiprange.ui \
    forms/shader.ui \
    forms/remotefilelist.ui \
    forms/popup.ui \
    forms/playlist.ui \
    forms/openexternal.ui \
    forms/multiplecapture.ui \
    forms/mainwindow.ui \
    forms/login.ui \
    forms/fileassociation.ui \
    forms/equalizer.ui \
    forms/customshortcut.ui \
    forms/textencodesetting.ui \
    forms/serversetting.ui \
    forms/screenexplorer.ui \
    forms/screenexploreritem.ui \
    forms/subtitledirectory.ui \
    forms/opendevice.ui \
    forms/opendtvscanchannel.ui \
    forms/viewepg.ui \
    forms/epgitem.ui

TRANSLATIONS += \
    languages/anyvod_ko.ts \
    languages/anyvod_en.ts

DEFINES += QT_DEPRECATED_WARNINGS __STDC_CONSTANT_MACROS

win32: DEFINES += PSAPI_VERSION=1

win32: QMAKE_POST_LINK += ..\\AnyVODClient\\scripts\\win\\postlink.bat
linux-*: QMAKE_POST_LINK += ../AnyVODClient/scripts/linux/postlink.sh
mac: QMAKE_POST_LINK += ../AnyVODClient/scripts/mac/postlink.sh

QMAKE_LFLAGS += -L$$PWD

linux-*: QMAKE_LFLAGS += -fuse-ld=gold

QMAKE_CXXFLAGS += -fdiagnostics-show-option

win32 {
    contains(QT_ARCH, x86_64) {
        QMAKE_CXXFLAGS += -fopenmp -m64
    } else {
        QMAKE_CXXFLAGS += -fopenmp -m32
    }
}

linux-*: QMAKE_CXXFLAGS += -fopenmp

win32 {
    contains(QT_ARCH, x86_64) {
        QMAKE_CFLAGS += -fopenmp -m64
    } else {
        QMAKE_CFLAGS += -fopenmp -m32
    }
}

linux-*: QMAKE_CFLAGS += -fopenmp

win32 {
    contains(QT_ARCH, x86_64) {
        LIBS += -L$$PWD\\libs\\win\\x64
    } else {
        LIBS += -L$$PWD\\libs\\win\\x86
    }
}

linux-*: LIBS += -L$$PWD/libs/linux
mac: LIBS += -L$$PWD/libs/mac

LIBS += -lmediainfo -lbass -lbass_fx -lbassmix -lavcodec -lavformat -lavutil -lavdevice \
        -lavfilter -lswscale -lswresample -lass

win32: LIBS += -lws2_32 -lImm32 -lShell32 -lAdvapi32 -lOle32 -lzdll -lgomp -ldsound -lStrmiids -luuid \
               -lOleAut32 -lPsapi -lopengl32
linux-*: LIBS += -ldl -lX11 -lz -lgomp -lasound -ldvbpsi -ldbus-1 -lvdpau -llog4cpp \
                 -lgnuradio-runtime -lgnuradio-pmt -lgnuradio-osmosdr -lgnuradio-filter -lgnuradio-blocks -lgnuradio-analog -lgnuradio-digital
mac: LIBS += -framework CoreFoundation -framework ApplicationServices -framework IOKit -framework CoreVideo \
             -framework CoreAudio -framework Foundation -framework AVFoundation -framework AppKit -lz

linux-*: INCLUDEPATH += /usr/include/dbus-1.0 /usr/lib/x86_64-linux-gnu/dbus-1.0/include
linux-*: DEPENDPATH += /usr/include/dbus-1.0 /usr/lib/x86_64-linux-gnu/dbus-1.0/include

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

INCLUDEPATH += $$PWD/include/ffmpeg
DEPENDPATH += $$PWD/include/ffmpeg

INCLUDEPATH += $$PWD/src
DEPENDPATH += $$PWD/src

RESOURCES += \
    forms/resource.qrc

OTHER_FILES += \
    settings.ini \
    skins/default.skin \
    shaders/sample.glsl \
    settings_template.ini \
    equalizer_template.ini \
    languages/anyvod_en.ts \
    languages/anyvod_ko.ts

mac: ICON = ../../images/icons/app.icns

mac {
    PRIVATE_LIBS.files += \
        libs/mac/libass.9.dylib \
        libs/mac/libbass.dylib \
        libs/mac/libbass_fx.dylib \
        libs/mac/libbassmix.dylib \
        libs/mac/libmediainfo.0.dylib \
        libs/mac/libavcodec.58.dylib \
        libs/mac/libavformat.58.dylib \
        libs/mac/libavutil.56.dylib \
        libs/mac/libswresample.3.dylib \
        libs/mac/libswscale.5.dylib \
        libs/mac/libavdevice.58.dylib \
        libs/mac/libavfilter.7.dylib
    PRIVATE_LIBS.path = Contents/MacOS

    QMAKE_BUNDLE_DATA += PRIVATE_LIBS
    QMAKE_MAC_SDK = macosx11.1
}

mac: QMAKE_INFO_PLIST = resources/mac.plist
win32: RC_FILE = resources/win.rc

win32 {
    contains(QT_ARCH, x86_64) {
        QMAKE_RC += -F pe-x86-64
    } else {
        QMAKE_RC += -F pe-i386
    }
}
